import sys
import os
import json
import getpass
import zipfile
import re
import copy
import ctypes
import pathlib

# import traceback
from json_tricks import dumps, load
import numpy as np
from datetime import datetime
from PyQt5 import QtWidgets, QtCore, QtGui
from contextlib import contextmanager
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.ticker import AutoLocator
from PyQt5.QtCore import QRegExp
from Classes.Measurement import Measurement
from Classes.Sensors import Sensors
from Modules.common_functions import (
    units_conversion,
    convert_temperature,
    cosd,
    sfrnd,
    dateformat,
)
from Modules.local_time_utilities import tz_formatted_string
import UI.QRevMS_gui as QRevMS_gui
from UI.StickySettings import StickySettings as SSet
from UI.OpenMeasurementDialog import OpenMeasurementDialog
from UI.SaveDialog import SaveDialog
from UI.MplCanvas import MplCanvas
from UI.Graphics import Graphics
from UI.Comment import Comment
from UI.InsertVertical import InsertVertical
from UI.ShipTrack import Shiptrack
from UI.ComboEdGrp import ComboEdGrp
from UI.ComboGrp import ComboGrp
from UI.EdGrp import EdGrp
from UI.Options import Options
from UI.Rating import Rating
from UI.Disclaimer import Disclaimer
from UI.VelocityRef import VelocityRef
from UI.VelocityMethod import VelocityMethod
from UI.Azimuth import Azimuth
from UI.QMethod import QMethod
from UI.AxesScale import AxesScale
from UI.PDFReport import Report
from UI.errorlog import ErrorLog

# QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling, True) #enable highdpi scaling
# QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_UseHighDpiPixmaps, True) #use highdpi icons

# if there is a splash screen close it
try:
    import pyi_splash

    pyi_splash.close()
except:
    pass


class QRevMS(QtWidgets.QMainWindow, QRevMS_gui.Ui_MainWindow):
    """This the primary class controlling the user interface which then
    controls the computational code.

    Attributes
    ----------
    QRevMS_version: str
        Text specifying current version of code
    agreement: bool
        Indicates if user has accepted the use agreement and disclosure
    main_initialized: bool
        Indicates if main tab connections have been initialized
    meas: Measurement
        Object of class Measurement which contains all data and computations
    change: bool
        Indicates if a change has been made and clicking on main requires an update
    current_tab: str
        Name of current tab
    vertical_idx: int
        Index in meas.verticals of vertical selected for display or processing
    stations_row: int
        Index of row selected in the stations tab table
    agency_options: dict
        Dictionary of agency options to display and default settings
    units: dict
        Dictionary of unit conversions and labels
    rating_prompt: bool
        Indicates if the user should be prompted to enter a rating when saving a file
    color_map: str
        Identifies color map to be used for color contour plots
    main_row_idx: int
        Index of row selected in the table on the main tab
    first_idx: int
        Index of first measured vertical
    sticky_settings: StickySettings
        Used to store various settings (folder, units, etc) for use the next time
        QRevMS is run
    compass_display_idx_checked: list
        List indicating which verticals to display in compass and pitch and roll plots
    save_4_reset: Measurement
        Copy of object of class Measurement used in the extrap tab to allow resetting
    vertical_changed: list
        List of bool indicating if a vertical extrapolation was changed
    compass_magvar_grp: EdGrp
        User interface controls for magvar edits using EdGrp class
    compass_heading_source_grp: ComboGrp
        User interface controls for heading edits using ComboGrp class
    tempsal_temp_source_grp: ComboEdGrp
        User interface controls for temperature source edits using ComboEdGrp class
    tempsal_salinity_grp: EdGrp
        User interface controls for salinity edits using EdGrp class
    tempsal_sos_source_grp: ComboEdGrp
        User interface controls for speed of sound edits using ComboEdGrp
    stations_location_grp: EdGrp
        User interface controls for station location edits usind EdGrp class
    stations_stage_grp: EdGrp
        User interface controls for station stage edits using EdGrp class
    stations_ws_condition_grp: ComboGrp
        User interface controls for water surface condition edits using ComboGrp class
    stations_vel_method_grp: ComboGrp
        User interface controls for velocity method edits using CombGrp class
    stations_vel_ref_grp: ComboGrp
        User interface controls for velocity reference edits using ComboGrp class
    bt_3beam_grp: ComboGrp
        User interface controls for bottom track beam filter edits using ComboGrp class
    bt_error_vel_grp: ComboEdGrp
        User interface controls for bottom track error velocity filter edits using
        ComboEdGrp class
    bt_vert_vel_grp: ComboEdGrp
        User interface controls for bottom track vertical velocity filter edits using
        ComboEdGrp class
    depth_source_grp: ComboEdGrp
        User interface controls for depth source edits using ComboEdGrp class
    depth_avg_grp: ComboGrp
        User interface controls for depth averaging method edits using ComboGrp class
    depth_filter_grp: ComboGrp
        User interface controls for depth filter edits using ComboGrp class
    depth_draft_grp: EdGrp
        User interface controls for adcp depth below water surface edits using EdGrp class
    depth_draft_ice_grp: EdGrp
        User interface controls for adcp depth below ice/slush edits using EdGrp class
    depth_ice_thickness_grp: EdGrp
        User interface controls for ice thickness edits using EdGrp class
    depth_ice_bottom_grp: EdGrp
        User interface controls for depth to bottom of ice edits using EdGrp class
    depth_slush_bottom_grp: EdGrp
        User interface controls for depth to bottom of slush edits using EdGrp class
    wt_vel_source_grp: ComboEdGrp
        User interface controls for water track source edits using ComboEdGrp class
    wt_min_ens_grp: EdGrp
        User interface controls for minimum ensembles required edits using EdGrp class
    wt_excluded_above_grp: ComboEdGrp
        User interface controls for excluded distance above sidelobe edits using
        ComboEdGrp class
    wt_excluded_below_grp: ComboEdGrp
        User interface controls for excluded distance below ADCP edits using
        ComboEdGrp class
    wt_3beam_grp: ComboGrp
        User interface controls for water track beam filter edits using ComboGrp class
    wt_error_vel_grp: ComboEdGrp
        User interface controls for water track error velocity filter edits using
        ComboEdGrp class
    wt_vert_vel_grp: ComboEdGrp
        User interface controls for water track vertical velocity filter edits using
        ComboEdGrp class
    wt_snr_grp: ComboGrp
        User interface controls for water track signal to noise ratio edits using
        ComboGrp class
    wt_flow_angle_grp: ComboEdGrp
        User interface controls for flow angle or coefficient edits using ComboEdGrp
    extrap_ice_exponent_grp: EdGrp
        User interface controls for ice exponent edits using EdGrp class
    extrap_exponent_grp: EdGrp
        User interface controls for exponent edit using EdGrp class
    extrap_min_ens_grp: EdGrp
        User interface controls for minimum ensembles required edits using EdGrp class
    icon_caution: QIcon
        Icon used to indicate a caution status
    icon_good: QIcon
        Icon used to indicate a good status
    icon_warning: QIcon
        Icon used to indicate a warning status
    sc_open: QShortcut
        Ctrl+F to open file
    sc_save: QShortcut
        Ctrl+S to save file
    sc_options: QShortcut
        Ctrl+O to open options dialog
    sc_comment: QShortcut
        Ctrl+N to open comments dialog
    figs: list
        List of figures on current tab
    canvases: list
        List of canvases on current tab
    toolbars: list
        List of toolbars for figures on current tab
    main_percent_discharge_canvas: MplCanvas
        Canvas for percent discharge bar chart on main tab
    main_percent_discharge_toolbar: NavigationToolbar2QT
        Toolbar for percent discharge bar chart on main tab
    main_percent_discharge_fig: Graphics
        Percent discharge bar chart on main tab created in Graphics class
    main_velocity_canvas: MplCanvas
        Canvas for velocity graphs on main tab
    main_velocity_toolbar: NavigationToolbar2QT
        Toolbar for velocity grpahs on main tab
    main_velocity_fig: Graphics
        Velocity graphs on main tab created in Graphics class
    main_extrap_canvas: MplCanvas
        Canvas for extrapolation graph on main tab
    main_extrap_toolbar: NavigationToolbar2QT
        Toolbar for extrapolation graph on main tab
    main_extrap_fig: Graphics
        Extrapolation graph on main tab created in Graphics class
    main_uncertainty_canvas: MplCanvas
        Canvas for signal strength graph on main tab
    main_uncertainty_toolbar: NavigationToolbar2QT
        Toolbar for signal strength graph on main tab
    main_uncertainty_fig: Graphics
        Signal strength graph on main tab created in Graphics class
    compass_pr_canvas: MplCanvas
        Canvas for graphs on compass tab
    compass_pr_fig: Graphics
        Heading, pitch, and roll graphs on compass tab created in Graphics class
    compass_pr_toolbar: NavigationToolbar2QT
        Toolbar for graphs on compass tab
    tts_canvas: MplCanvas
        Canvas for temperature time series graph on TempSal tab
    tts_fig: Graphics
        Temperature time series graph on TempSal tab created in Graphics class
    tts_toolbar: NavigationToolbar2QT
        Toolbar for temperature time series graph on TempSal tab
    stations_percent_discharge_toolbar: NavigationToolbar2QT
        Toolbar for percent discharge bar graph on Stations tab
    stations_percent_discharge_fig: Graphics
        Percent discharge bar graph on Stations tab created in Graphics class
    stations_percent_discharge_canvas: MplCanvas
        Canvas for percent discharge bar graph on Stations tab
    stations_velocity_toolbar: NavigationToolbar2QT
        Toolbar for velocity graphs on Stations tab
    stations_velocity_fig: Graphics
        Velocity graphs on Stations tab created in Graphics class
    stations_velocity_canvas: MplCanvas
        Canvas for velocity graphs on Stations tab
    bt_shiptrack_fig: Shiptrack
        Shiptrack graph on BT tab created in Shiptrack class
    bt_shiptrack_canvas: MplCanvas
        Canvas for shiptrack graph on BT tab
    bt_shiptrack_toolbar: NavigationToolbar2QT
        Toolbar for shiptrack graph on BT tab
    bt_ts_canvas: MplCanvas
        Canvas for time series and contour graphs on BT tab
    bt_ts_fig: Graphics
        Time series and contour graphs on BT tab created in Graphics class
    bt_ts_toolbar: NavigationToolbar2QT
        Toolbar for time series and contour graphs on BT tab
    depth_canvas: MplCanvas
        Canvas for depth graphs on Depth tab
    depth_fig: Graphics
        Depth graphs on Depth tab created in Graphics class
    depth_toolbar: NavigationToolbar2QT
        Toolbar for depth graphs on Depth tab
    wt_filter_canvas: MplCanvas
        Canvas for water track filter and contour graphs on WT tab
    wt_filter_fig: Graphics
        Water track filter and contour graphs on WT tab created in Graphics class
    wt_filter_toolbar: NavigationToolbar2QT
        Toolbar for water track filter and contour graphs on WT tab
    wt_profile_canvas: MplCanvas
        Canvas for water track magnitude and direction and RSSI graph on WT tab
    wt_profile_fig: Graphics
        Water track magnitude and direction  and RSSI graph on WT tab created in Graphics class
    wt_profile_toolbar: NavigationToolbar2QT
        Toolbar for water track magnitude and direction and RSSI graph on WT tab
    extrap_canvas: MplCanvas
        Canvas for extrapolation graph on Extrap tab
    extrap_fig: Graphics
        Extrapolation graph on Extrap tab created in Graphics class
    extrap_toolbar: NavigationToolbar2QT
        Toolbar for extrapolation graph on Extrap tab
    edges_contour_canvas: MplCanvas
        Canvas for contour graph on Edges tab
    edges_contour_fig: Graphics
        Contour graph on edges tab created in Graphics class
    edges_contour_toolbar: NavigationToolbar2QT
        Toolbar for contour graph on Edges tab
    edges_bar_canvas: MplCanvas
        Canvas for percent discharge bar graph on Edges tab
    edges_bar_fig: Graphics
        Percent discharge bar graph on Edges tab created in Graphics class
    edges_bar_toolbar: NavigationToolbar2QT
        Toolbar for percent discharge bar graph on Edges tab
    plt_color_dict: dict
        Dictionary of colors for various status for percent discharge bar graph
    main_initialized: bool
        Indicates if Main tab connections have been initialized
    systest_initialized: bool
        Indicates if SysTest tab connections have been initialized
    compass_pr_initialized: bool
        Indicates if Compass/P/R tab connections have been initialized
    tempsal_initialized: bool
        Indicates if Temp/Sal tab connections have been initialized
    stations_initialized: bool
        Indicates if Stations tab connections have been initialized
    bt_initialized: bool
        Indicates if BT tab connections have been initialized
    depth_initialized: bool
        Indicates if Depth tab connections have been initialized
    wt_initialized: bool
        Indicates if WT tab connections have been initialized
    extrap_initialized: bool
        Indicates if Extrap tab connections have been initialized
    edges_initialized: bool
        Indicates if Edges tab connections have been initialized
    bt_always_active: bool
        Indicates if the BT tab is always active or only when used as reference
    uncertainty: float
        Computed expanded uncertianty
    uncertainty_results_idx: int
        Index used to identify user uncertainty input
    uncertainty_settings_idx: int
        Index used ot identify user uncertainty setting input
    """

    def __init__(self):
        """Initializes the QRevMS class and sets up the user interface."""
        super(QRevMS, self).__init__()
        self.setupUi(self)

        # Set version of QRev
        self.QRevMS_version = "QRevMS 1.20"
        self.setWindowTitle(self.QRevMS_version)
        self.setWindowIcon(QtGui.QIcon("QRevMS.ico"))
        myappid = "QRevMS"  # arbitrary string
        ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
        show_disclaimer = True

        # Disable ability to hide toolbar
        self.toolBar.toggleViewAction().setEnabled(False)

        self.top_tab.setStyleSheet(
            ":tab:selected { font-size:11pt; font-weight:bold;}"
            ":tab:!selected { font-size:11pt; font-weight:normal;}"
        )
        # Initialize attributes
        # ---------------------
        self.agreement = True
        self.meas = None
        self.change = False
        self.current_tab = "Main"
        self.vertical_idx = 1
        self.stations_row = 1
        self.agency_options = None
        self.units = None
        self.rating_prompt = None
        self.color_map = None
        self.main_row_idx = None
        self.first_idx = None
        self.compass_display_idx_checked = None
        self.save_4_reset = None
        self.vertical_changed = None
        self.application_path = None
        self.bt_always_active = False

        # Setup input groups
        self.compass_magvar_grp = None
        self.compass_heading_source_grp = None
        self.tempsal_temp_source_grp = None
        self.tempsal_salinity_grp = None
        self.tempsal_sos_source_grp = None
        self.stations_location_grp = None
        self.stations_stage_grp = None
        self.stations_ws_condition_grp = None
        self.stations_vel_method_grp = None
        self.stations_vel_ref_grp = None
        self.bt_3beam_grp = None
        self.bt_error_vel_grp = None
        self.bt_vert_vel_grp = None
        self.depth_source_grp = None
        self.depth_avg_grp = None
        self.depth_filter_grp = None
        self.depth_draft_grp = None
        self.depth_draft_ice_grp = None
        self.depth_ice_thickness_grp = None
        self.depth_ice_bottom_grp = None
        self.depth_slush_bottom_grp = None
        self.wt_vel_source_grp = None
        self.wt_min_ens_grp = None
        self.wt_excluded_above_grp = None
        self.wt_excluded_below_grp = None
        self.wt_3beam_grp = None
        self.wt_error_vel_grp = None
        self.wt_vert_vel_grp = None
        self.wt_snr_grp = None
        self.wt_flow_angle_grp = None
        self.extrap_ice_exponent_grp = None
        self.extrap_exponent_grp = None
        self.extrap_min_ens_grp = None
        self.compass_beam_rotation_grp = None
        self.uncertainty = np.nan
        self.uncertainty_results_idx = 0
        self.uncertainty_settings_idx = 0

        # Configure bold and normal fonts
        self.font_bold = QtGui.QFont()
        self.font_bold.setBold(True)
        self.font_normal = QtGui.QFont()
        self.font_normal.setBold(False)

        # Tab initialization tracking setup
        self.main_initialized = False
        self.systest_initialized = False
        self.compass_pr_initialized = False
        self.tempsal_initialized = False
        self.stations_initialized = False
        self.bt_initialized = False
        self.depth_initialized = False
        self.wt_initialized = False
        self.extrap_initialized = False
        self.edges_initialized = False

        # Graphics
        self.figs = []
        self.fig_calls = []
        self.canvases = []
        self.toolbars = []
        self.ui_parents = []
        self.main_percent_discharge_canvas = None
        self.main_percent_discharge_toolbar = None
        self.main_percent_discharge_fig = None
        self.main_velocity_canvas = None
        self.main_velocity_toolbar = None
        self.main_velocity_fig = None
        self.main_extrap_canvas = None
        self.main_extrap_toolbar = None
        self.main_extrap_fig = None
        self.main_uncertainty_canvas = None
        self.main_uncertainty_toolbar = None
        self.main_uncertainty_fig = None
        self.compass_pr_canvas = None
        self.compass_pr_fig = None
        self.compass_pr_toolbar = None
        self.tts_canvas = None
        self.tts_fig = None
        self.tts_toolbar = None
        self.stations_percent_discharge_toolbar = None
        self.stations_percent_discharge_fig = None
        self.stations_percent_discharge_canvas = None
        self.stations_velocity_toolbar = None
        self.stations_velocity_fig = None
        self.stations_velocity_canvas = None
        self.bt_shiptrack_fig = None
        self.bt_shiptrack_canvas = None
        self.bt_shiptrack_toolbar = None
        self.bt_ts_canvas = None
        self.bt_ts_fig = None
        self.bt_ts_toolbar = None
        self.depth_canvas = None
        self.depth_fig = None
        self.depth_toolbar = None
        self.wt_filter_canvas = None
        self.wt_filter_fig = None
        self.wt_filter_toolbar = None
        self.wt_profile_canvas = None
        self.wt_profile_fig = None
        self.wt_profile_toolbar = None
        self.extrap_canvas = None
        self.extrap_fig = None
        self.extrap_toolbar = None
        self.edges_contour_canvas = None
        self.edges_contour_fig = None
        self.edges_contour_toolbar = None
        self.edges_bar_canvas = None
        self.edges_bar_fig = None
        self.edges_bar_toolbar = None
        self.uncertainty_canvas = None
        self.uncertainty_toolbar = None
        self.uncertainty_fig = None

        # Setup indicator icons
        self.icon_caution = QtGui.QIcon()
        self.icon_caution.addPixmap(
            QtGui.QPixmap(":/Icons/Icons/Warning.png"),
            QtGui.QIcon.Normal,
            QtGui.QIcon.Off,
        )

        self.icon_warning = QtGui.QIcon()
        self.icon_warning.addPixmap(
            QtGui.QPixmap(":/Icons/Icons/Alert.png"),
            QtGui.QIcon.Normal,
            QtGui.QIcon.Off,
        )

        self.icon_good = QtGui.QIcon()
        self.icon_good.addPixmap(
            QtGui.QPixmap(":/Icons/Icons/Yes.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off
        )

        self.plt_color_dict = {
            "Good": (0, 153 / 255, 0),
            "Caution": (230 / 255, 138 / 255, 0),
            "Warning": (255 / 255, 77 / 255, 77 / 255),
            "Changed": (0, 0, 255 / 255),
            "N/A": (140 / 255, 140 / 255, 140 / 255),
        }
        # Initial settings
        # Setting file for settings to carry over from one session to the next
        self.settingsFile = "QRevMS_Settings"
        
        # Create settings object which contains the default values from
        # previous use
        self.sticky_settings = SSet(self.settingsFile)

        self.get_settings()
        self.setup_toolbar()
        self.top_tab.setEnabled(False)
        self.setMouseTracking(True)
        self.windows10_settings()

        # Connect a change in selected tab to the tab manager
        self.top_tab.currentChanged.connect(self.tab_manager)
        self.main_subtabs_top.currentChanged.connect(self.main_subtabs_manager)
        
        # Save figure menu with Right click
        self.figsMenu = QtWidgets.QMenu(self)
        self.figsMenu.addAction("Save figure", self.save_fig)
        self.figsMenu.addAction("Set Axes Limits", self.set_axes)

        # Show QRev maximized on the display
        self.showMaximized()

        if show_disclaimer:
            try:
                self.agreement = self.sticky_settings.get("Agreement")
                if not self.agreement:
                    self.close()
            except KeyError:
                # Open disclaimer and license
                disclaimer = Disclaimer(self)
                disclaimer_exec = disclaimer.exec_()
                if disclaimer_exec:
                    self.sticky_settings.new("Agreement", True)
                    self.agreement = True
                else:
                    self.agreement = False
                    self.close()
        else:
            self.agreement = True

    def get_settings(self):
        """Reads and sets QRevMS settings specified by the user and
        previous use of the software.
        """

        # Locate cfg file
        config_name = "QRevMS.cfg"

        # determine if application is a script file or frozen exe
        if getattr(sys, "frozen", False):
            self.application_path = os.path.dirname(sys.executable)
        elif __file__:
            self.application_path = os.path.dirname(__file__)

        options_file = os.path.join(self.application_path, config_name)

        if os.path.exists(options_file):
            if os.path.isfile(options_file):
                # Read json into dictionary
                try:
                    with open(options_file, "r") as f:
                        self.agency_options = json.load(f)
                except json.decoder.JSONDecodeError:
                    self.popup_message(
                        self.tr(
                            "QRevMS.cfg could not be read due a formatting "
                            "error. "
                            "QRevMS cannot continue."
                        )
                    )
                    sys.exit()
        else:
            # Report no file found
            self.popup_message(
                self.tr(
                    "QRevMS.cfg could not be found. QRevMS cannot "
                    "continue. {}".format(options_file)
                )
            )
            sys.exit()

        # Units
        if "Units" not in self.agency_options.keys():
            self.popup_message(self.tr("QRevMS.cfg: Units parameter not found."))
            sys.exit()

        if "show" not in self.agency_options["Units"].keys():
            self.popup_message(self.tr("QRevMS.cfg Units: show parameter not found."))
            sys.exit()

        if "default" not in self.agency_options["Units"].keys():
            self.popup_message(
                self.tr("QRevMS.cfg Units: default parameter not found.")
            )
            sys.exit()

        try:
            if self.agency_options["Units"]["show"]:
                units_id = self.sticky_settings.get("UnitsID")
            else:
                units_id = self.agency_options["Units"]["default"]
            if not units_id:
                self.sticky_settings.set(
                    "UnitsID", self.agency_options["Units"]["default"]
                )
        except KeyError:
            self.sticky_settings.new("UnitsID", self.agency_options["Units"]["default"])
        self.units = units_conversion(units_id=self.sticky_settings.get("UnitsID"))

        # User rating
        if "RatingPrompt" not in self.agency_options.keys():
            self.popup_message(self.tr("QRevMS.cfg: RatingPrompt parameter not found."))
            sys.exit()
        if "show" not in self.agency_options["RatingPrompt"].keys():
            self.popup_message(
                self.tr("QRevMS.cfg RatingPrompt: show parameter not found.")
            )
            sys.exit()
        if "default" not in self.agency_options["RatingPrompt"].keys():
            self.popup_message(
                self.tr("QRevMS.cfg RatingPrompt: default parameter not found.")
            )
            sys.exit()
        try:
            if self.agency_options["RatingPrompt"]["show"]:
                ss = self.sticky_settings.get("UserRating")
                self.rating_prompt = ss
            else:
                self.rating_prompt = self.agency_options["RatingPrompt"]["default"]
        except KeyError:
            self.sticky_settings.new(
                "UserRating", self.agency_options["RatingPrompt"]["default"]
            )
            self.rating_prompt = self.agency_options["RatingPrompt"]["default"]

        # Color map
        if "ColorMap" not in self.agency_options.keys():
            self.popup_message(self.tr("QRevMS.cfg: ColorMap parameter not found."))
            sys.exit()
        if "show" not in self.agency_options["ColorMap"].keys():
            self.popup_message(
                self.tr("QRevMS.cfg ColorMap: show parameter not found.")
            )
            sys.exit()
        if "default" not in self.agency_options["ColorMap"].keys():
            self.popup_message(
                self.tr("QRevMS.cfg ColorMap: default parameter not found.")
            )
            sys.exit()
        try:
            if self.agency_options["ColorMap"]["show"]:
                ss = self.sticky_settings.get("ColorMap")
                self.color_map = ss
            else:
                self.color_map = self.agency_options["ColorMap"]["default"]
        except KeyError:
            self.sticky_settings.new(
                "ColorMap", self.agency_options["ColorMap"]["default"]
            )
            self.color_map = self.agency_options["ColorMap"]["default"]

        # BT
        if "BTActive" not in self.agency_options.keys():
            self.popup_message(self.tr("QRevMS.cfg: BTActive parameter not found."))
            sys.exit()
        if "show" not in self.agency_options["BTActive"].keys():
            self.popup_message(
                self.tr("QRevMS.cfg BTActive: show parameter not found.")
            )
            sys.exit()
        if "default" not in self.agency_options["BTActive"].keys():
            self.popup_message(
                self.tr("QRevMS.cfg BTActive: default parameter not found.")
            )
            sys.exit()
        try:
            if self.agency_options["BTActive"]["show"]:
                ss = self.sticky_settings.get("BTActive")
                self.bt_always_active = ss
            else:
                self.bt_always_active = self.agency_options["BTActive"]["default"]
        except KeyError:
            self.sticky_settings.new(
                "BTActive", self.agency_options["BTActive"]["default"]
            )
            self.bt_always_active = self.agency_options["BTActive"]["default"]

        # Uncertainty
        if "Uncertainty" not in self.agency_options.keys():
            self.popup_message(self.tr("QRevMS.cfg: Uncertainty parameter not found."))
            sys.exit()
        if "show" not in self.agency_options["Uncertainty"].keys():
            self.popup_message(
                self.tr("QRevMS.cfg Uncertainty: show parameter not found.")
            )
            sys.exit()
        if "default" not in self.agency_options["Uncertainty"].keys():
            self.popup_message(
                self.tr("QRevMS.cfg Uncertainty: default parameter not found.")
            )
            sys.exit()
        try:
            if self.agency_options["Uncertainty"]["show"]:
                ss = self.sticky_settings.get("Uncertainty")
                self.uncertainty = ss
            else:
                self.uncertainty = self.agency_options["Uncertainty"]["default"]
        except KeyError:
            self.sticky_settings.new(
                "Uncertainty", self.agency_options["Uncertainty"]["default"]
            )
            self.uncertainty = self.agency_options["Uncertainty"]["default"]

        # Excluded
        if "Excluded" not in self.agency_options.keys():
            self.popup_message(self.tr("QRevMS.cfg: Excluded parameter not found."))
            sys.exit()
        if "RioPro" not in self.agency_options["Excluded"].keys():
            self.popup_message(
                self.tr("QRevMS.cfg Excluded: RioPro parameter not found.")
            )
            sys.exit()
        if "M9" not in self.agency_options["Excluded"].keys():
            self.popup_message(self.tr("QRevMS.cfg Excluded: M9 parameter not found."))
            sys.exit()

        # StationQ
        if "StationQ" not in self.agency_options.keys():
            self.popup_message(self.tr("QRevMS.cfg: StationQ parameter not found."))
            sys.exit()
        if "caution" not in self.agency_options["StationQ"].keys():
            self.popup_message(
                self.tr("QRevMS.cfg StationQ: caution parameter not found.")
            )
            sys.exit()
        if "warning" not in self.agency_options["StationQ"].keys():
            self.popup_message(
                self.tr("QRevMS.cfg StationQ: warning parameter not found.")
            )
            sys.exit()

        # SNR 3-beam computations setting
        if "SNR" not in self.agency_options.keys():
            self.popup_message(self.tr("QRevMS.cfg: SNR parameter not found."))
            sys.exit()
        if "Use3Beam" not in self.agency_options["SNR"].keys():
            self.popup_message(
                self.tr("QRevMS.cfg: SNR Use3Beam parameter " "not found.")
            )
            sys.exit()

        # PDF Summary: Use sticky setting, then agency option, then prompt
        self.pdf_setting = "Prompt"
        if "PDFSummary" in self.sticky_settings.settings:
            self.pdf_setting = self.sticky_settings.get("PDFSummary")
        else:
            if (
                "PDFSummary" in self.agency_options.keys()
                and "default" in self.agency_options["PDFSummary"].keys()
            ):
                self.pdf_setting = self.agency_options["PDFSummary"]["default"]
            self.sticky_settings.new("PDFSummary", self.pdf_setting)

        # Date format
        if "DateFormat" not in self.agency_options.keys():
            self.popup_message(self.tr("QRevMS.cfg: DateFormat parameter not found."))
            sys.exit()
        if "format" not in self.agency_options["DateFormat"].keys():
            self.popup_message(
                self.tr("QRevMS.cfg: DateFormat format parameter not found.")
            )
            sys.exit()
        else:
            datedict = {"m": "%m", "d": "%d", "y": "%Y"}
            self.date_format = dateformat(self.agency_options["DateFormat"]["format"])

        # Slush filter
        if "SlushFilter" not in self.agency_options.keys():
            self.popup_message(self.tr("QRevMS.cfg: SlushFilter parameter not found."))
            sys.exit()
        if "show" not in self.agency_options["SlushFilter"].keys():
            self.popup_message(
                self.tr("QRevMS.cfg SlushFilter: show parameter not found.")
            )
            sys.exit()
        if "default" not in self.agency_options["SlushFilter"].keys():
            self.popup_message(
                self.tr("QRevMS.cfg SlushFilter: default parameter not found.")
            )
            sys.exit()
        try:
            if self.agency_options["SlushFilter"]["show"]:
                ss = self.sticky_settings.get("SlushFilter")
                self.kmeans = ss
            else:
                self.kmeans = self.agency_options["SlushFilter"]["default"]
        except KeyError:
            self.sticky_settings.new(
                "SlushFilter", self.agency_options["SlushFilter"]["default"]
            )
            self.kmeans = self.agency_options["SlushFilter"]["default"]

        # Time zone
        if "TimeZone" not in self.agency_options.keys():
            self.popup_message(self.tr(
                "QRev.cfg: TimeZone parameter not found. Time zone not required."))
            self.time_zone_required = False
        elif "required" in self.agency_options["TimeZone"].keys():
            self.time_zone_required = self.agency_options["TimeZone"]["required"]
        else:
            self.time_zone_required = False

        # Discharge display digits
        if "QDigits" not in self.agency_options.keys():
            self.popup_message(self.tr(
                "QRev.cfg: QDigits " "parameter not found. Setting to 3 significant figures"))
            self.q_digits_method = "sigfig"
            self.q_digits_digits = 3

        if "method" not in self.agency_options["QDigits"].keys():
            self.popup_message(self.tr(
                "QRev.cfg QDigits: " "method parameter not found. Setting to significant figures"))
            self.q_digits_method = "sigfig"
        else:
            self.q_digits_method = self.agency_options["QDigits"]["method"]

        if "digits" not in self.agency_options["QDigits"].keys():
            self.popup_message(self.tr(
                "QRev.cfg QDigits: " "digits parameter not found. Setting to default of 3"))
            self.q_digits_digits = 3
        else:
            self.q_digits_method = self.agency_options["QDigits"]["digits"]

        try:
            ss = self.sticky_settings.get("QDigitsMethod")
            self.q_digits_method = ss
        except KeyError:
            self.sticky_settings.new("QDigitsMethod",
                                     self.agency_options["QDigits"]["method"])
            self.q_digits_method = self.agency_options["QDigits"]["method"]

        try:
            ss = self.sticky_settings.get("QDigitsDigits")
            self.q_digits_digits = ss
        except KeyError:
            self.sticky_settings.new("QDigitsDigits",
                                     self.agency_options["QDigits"]["digits"])
            self.q_digits_digits = self.agency_options["QDigits"]["digits"]

    def setup_toolbar(self):
        """Setup connections for toolbar."""

        # Connect toolbar button to methods
        self.actionOpen.triggered.connect(self.select_measurement)
        self.actionSave.triggered.connect(self.save_measurement)
        self.actionOptions.triggered.connect(self.change_options)
        self.actionNotes.triggered.connect(self.add_comment)
        self.actionHome.triggered.connect(self.home)
        self.actionZoom.triggered.connect(self.zoom)
        self.actionPan.triggered.connect(self.pan)
        self.actionData_Cursor.triggered.connect(self.data_cursor)
        self.actionHelp.triggered.connect(self.help)
        self.actionVelRef.triggered.connect(self.set_vel_ref_all)
        self.actionVelRefSet.triggered.connect(self.set_vel_ref_all)
        self.actionQMethod.triggered.connect(self.set_q_method_all)
        self.actionQMethodSet.triggered.connect(self.set_q_method_all)
        self.actionVelMethod.triggered.connect(self.set_vel_method_all)
        self.actionVelMethodSet.triggered.connect(self.set_vel_method_all)
        self.actionAz.triggered.connect(self.set_azimuth)
        self.actionAzSet.triggered.connect(self.set_azimuth)
        self.actionUserRating.triggered.connect(self.set_user_rating)
        self.actionUserRatingSet.triggered.connect(self.set_user_rating)

        # Disable toolbar icons until data are loaded
        self.actionSave.setDisabled(True)
        self.actionNotes.setDisabled(True)
        self.actionHome.setDisabled(True)
        self.actionZoom.setDisabled(True)
        self.actionPan.setDisabled(True)
        self.actionData_Cursor.setDisabled(True)
        self.actionVelRef.setDisabled(True)
        self.actionVelRefSet.setDisabled(True)
        self.actionQMethod.setDisabled(True)
        self.actionQMethodSet.setDisabled(True)
        self.actionVelMethod.setDisabled(True)
        self.actionVelMethodSet.setDisabled(True)
        self.actionAz.setDisabled(True)
        self.actionAzSet.setDisabled(True)
        self.actionUserRating.setDisabled(True)
        self.actionUserRatingSet.setDisabled(True)

    def windows10_settings(self):
        # Special commands to ensure proper operation on Windows 10
        if QtCore.QSysInfo.windowsVersion() == QtCore.QSysInfo.WV_WINDOWS10:
            self.setStyleSheet(
                "QHeaderView::section{"
                "border-top: 0px solid #D8D8D8;"
                "border-left: 0px solid #D8D8D8;"
                "border-right: 1px solid #D8D8D8;"
                "border-bottom: 1px solid #D8D8D8;"
                "background-color: white;"
                "padding:4px;"
                "}"
                "QTableCornerButton::section{"
                "border-top: 0px solid #D8D8D8;"
                "border-left: 0px solid #D8D8D8;"
                "border-right: 1px solid #D8D8D8;"
                "border-bottom: 1px solid #D8D8D8;"
                "background-color: white;"
                "}"
                "QToolTip{font: 12pt}"
            )

    def config_gui(self):
        """Configure the user interface based on the available data."""

        # After data is loaded enable GUI and buttons on toolbar
        self.top_tab.setEnabled(True)
        self.actionSave.setEnabled(True)
        self.actionOptions.setEnabled(True)
        self.actionNotes.setEnabled(True)
        self.actionHome.setEnabled(True)
        self.actionZoom.setEnabled(True)
        self.actionPan.setEnabled(True)
        self.actionData_Cursor.setEnabled(True)
        self.actionVelRef.setEnabled(True)
        self.actionVelRefSet.setEnabled(True)
        self.actionQMethod.setEnabled(True)
        self.actionQMethodSet.setEnabled(True)
        self.actionVelMethod.setEnabled(True)
        self.actionVelMethodSet.setEnabled(True)
        self.actionAz.setEnabled(True)
        self.actionAzSet.setEnabled(True)
        self.actionUserRating.setEnabled(True)
        self.actionUserRatingSet.setEnabled(True)

        # Set tab text and icons to default
        for tab_idx in range(self.top_tab.count()):
            self.top_tab.setTabIcon(tab_idx, QtGui.QIcon())
            self.top_tab.tabBar().setTabTextColor(tab_idx, QtGui.QColor(191, 191, 191))

        # Configure tabs for the presence or absence of a compass
        compass_present = False
        if len(self.meas.verticals_used_idx) > 0:
            for idx in self.meas.verticals_used_idx:
                if self.meas.verticals[idx].data.sensors is not None:
                    heading = np.unique(
                        self.meas.verticals[idx].data.sensors.heading_deg.internal.data
                    )
                    if len(heading) > 1 and np.any(np.not_equal(heading, 0)):
                        compass_present = True
                        break
        #     else:
        #         heading = np.array([0])
        # else:
        #     heading = np.array([0])

        if compass_present:
            self.top_tab.setTabEnabled(2, True)
        else:
            self.top_tab.setTabEnabled(2, False)

        tab_idx = self.top_tab.indexOf(
            self.top_tab.findChild(QtWidgets.QWidget, "uncertainty_tab")
        )

        self.top_tab.tabBar().setTabTextColor(
            tab_idx,
            QtGui.QColor(0, 0, 0),
        )

        self.top_tab.setCurrentIndex(0)

    def main_subtabs_manager(self, tab_idx=None):
        if tab_idx == 0:
            self.main_summary_tab.setFocus()
        elif tab_idx == 1:
            self.main_details_tab.setFocus()
        elif tab_idx == 2:
            self.main_premeasurement_tab.setFocus()
        elif tab_idx == 3:
            self.main_settings_tab.setFocus()
        elif tab_idx == 4:
            self.main_inst_tab.setFocus()

    def tab_manager(self, tab_idx=None, old_discharge=None, subtab_idx=None):
        """Manages the initialization of content for each tab and updates
        that information as necessary.

        Parameters
        ----------
        tab_idx: int, str
            Index of tab clicked by user
        old_discharge: dataframe
            Pandas dataframe containing measurement summary prior to changes
        subtab_idx: int
            Index of subtab of tab_summary, used to force tab_idx to main,
            discharge
        """

        # Clear zoom, pan, home, data_cursor
        self.clear_zphd()

        # Determine the selected tab
        if tab_idx is None:
            tab_idx = self.current_tab
        elif type(tab_idx) != str:
            tab_idx = self.top_tab.tabText(tab_idx)

        self.current_tab = tab_idx

        # Main tab
        if tab_idx.strip() == "Main":
            if subtab_idx is not None:
                self.main_subtabs_top.setCurrentIndex(0)
                self.main_summary_table.setFocus()
                
            if self.change:
                # If data has changed update main tab display
                self.main_update()
                self.change = False
            else:
                # Setup list for use by graphics controls
                self.canvases = [
                    self.main_velocity_canvas,
                    self.main_extrap_canvas,
                    self.main_percent_discharge_canvas,
                    self.main_uncertainty_canvas,
                ]
                self.figs = [
                    self.main_velocity_fig,
                    self.main_extrap_fig,
                    self.main_percent_discharge_fig,
                    self.main_uncertainty_fig,
                ]
                self.toolbars = [
                    self.main_velocity_toolbar,
                    self.main_extrap_toolbar,
                    self.main_percent_discharge_toolbar,
                    self.main_uncertainty_toolbar,
                ]
                self.fig_calls = [
                    self.main_graphics_velocity,
                    self.main_graphics_extrap,
                    self.main_graphics_percent_discharge,
                    self.main_graphics_uncertainty,
                ]
                self.ui_parents = [i.parent() for i in self.canvases]
                self.figs_menu_connection()
                self.main_tab.show()

        # System tab
        elif tab_idx.strip() == "SysTest":
            self.systest_tab_update()

        # Compass/PR tab
        elif tab_idx.strip() == "Compass/P/R":
            self.compass_tab(old_discharge=old_discharge)

        # Temp/Sal tab
        elif tab_idx.strip() == "Temp/Sal":
            self.tempsal_tab_update(old_discharge=old_discharge)

        # Moving-bed test tab
        elif tab_idx.strip() == "Stations":
            self.stations_update_tab()

        # Bottom track tab
        elif tab_idx.strip() == "BT":
            self.bt_update_tab(old_discharge=old_discharge)

        # Depth tab
        elif tab_idx.strip() == "Depth":
            self.depth_tab_initial(old_discharge=old_discharge)

        # Water track tab
        elif tab_idx.strip() == "WT":
            self.wt_tab_initial(old_discharge=old_discharge)

        # Extrapolation tab
        elif tab_idx.strip() == "Extrap":
            self.extrap_tab_initial(old_discharge=old_discharge)

        # Edges tab
        elif tab_idx.strip() == "Edges":
            self.edges_tab_initial(old_discharge=old_discharge)

        elif tab_idx.strip() == "Uncertainty":
            self.uncertainty_tab_initial()

        self.set_tab_color()
        self.display_bt_tab()

    def update_tab_icons(self):
        """Update tab icons base on results of QA analysis."""
        qa = self.meas.qa
        qa_check_keys = [
            "verticals",
            "user",
            "system_tst",
            "temperature",
            "compass",
            "stations",
            "bt_vel",
            "depths",
            "w_vel",
            "extrapolation",
            "edges",
            "main",
            "rating"
        ]

        if not self.display_bt_tab():
            qa_check_keys.remove("bt_vel")

        for key in qa_check_keys:
            qa_type = getattr(qa, key)
            self.set_icon(key, qa_type["status"])
            self.set_tab_color()

    def set_icon(self, key, status):
        """Set tab icon based on qa check status.

        Parameters
        ----------
        key: str
            Identifies target tab
        status: str
            Quality status for data in that tab
        """
        tab_base = self.top_tab
        tab = ""
        # Identify tab name based on key
        if key == "bt_vel":
            tab = "bt_tab"
        elif key == "compass":
            tab = "compasspr_tab"
        elif key == "depths":
            tab = "depth_tab"
        elif key == "edges":
            tab = "edges_tab"
        elif key == "extrapolation":
            tab = "extrap_tab"
        elif key == "system_tst":
            tab = "systest_tab"
        elif key == "temperature":
            tab = "tempsal_tab"
        elif key == "verticals":
            tab = "main_tab"
            status_list = [
                self.meas.qa.verticals["status"], 
                self.meas.qa.user["status"], 
                self.meas.qa.rating["status"], 
                self.meas.qa.main["status"]
            ]
            status = "good"
            if "warning" in status_list:
                status = "warning"
            elif "caution" in status_list:
                status = "caution"
        elif key == "w_vel":
            tab = "wt_tab"
        elif key == "user":
            tab = "main_premeasurement_tab"
            tab_base = self.main_subtabs_top
        elif key == "stations":
            tab = "stations_tab"
            if len(self.meas.qa.verticals["duration_caution"]) or len(
                self.meas.qa.verticals["valid_caution"]
            ):
                if status != "warning":
                    status = "caution"
            if (
                len(self.meas.qa.verticals["duration_warning"])
                or len(self.meas.qa.verticals["valid_warning"])
                or len(self.meas.qa.verticals["no_valid_profile"])
            ):
                status = "warning"

        self.set_tab_icon(tab, status, tab_base=tab_base)

    def set_tab_icon(self, tab, status, tab_base=None):
        """Sets the text color and icon for the tab based on the quality
        status.

        Parameters
        ----------
        tab: str
            Tab identifier
        status: str
            Quality status
        tab_base: object
            Object of tab interface
        """

        if tab_base is None:
            tab_base = self.top_tab

        tab_idx = tab_base.indexOf(tab_base.findChild(QtWidgets.QWidget, tab))

        tab_base.tabBar().setTabTextColor(
            tab_idx,
            QtGui.QColor(140, 140, 140),
        )
        tab_base.setTabIcon(tab_idx, QtGui.QIcon())
        # Set appropriate icon
        if status == "good":
            tab_base.setTabIcon(
                tab_idx,
                self.icon_good,
            )
            tab_base.tabBar().setTabTextColor(
                tab_idx,
                QtGui.QColor(0, 153, 0),
            )
        elif status == "caution":
            tab_base.setTabIcon(
                tab_idx,
                self.icon_caution,
            )
            tab_base.tabBar().setTabTextColor(
                tab_idx,
                QtGui.QColor(230, 138, 0),
            )
        elif status == "warning":
            tab_base.setTabIcon(
                tab_idx,
                self.icon_warning,
            )
            tab_base.tabBar().setTabTextColor(
                tab_idx,
                QtGui.QColor(255, 77, 77),
            )

    def set_tab_color(self):
        """Updates tab font to Blue if a setting was changed from the
        default settings."""

        if self.meas.qa is not None:
            for tab in self.meas.qa.settings_dict:
                if self.meas.qa.settings_dict[tab] == "Custom":
                    if tab == "main_premeasurement_tab":
                        self.main_subtabs_top.tabBar().setTabTextColor(
                            self.main_subtabs_top.indexOf(
                                self.main_subtabs_top.findChild(QtWidgets.QWidget, tab)
                            ),
                            QtGui.QColor(0, 0, 255),
                        )
                    else:
                        self.top_tab.tabBar().setTabTextColor(
                            self.top_tab.indexOf(
                                self.top_tab.findChild(QtWidgets.QWidget, tab)
                            ),
                            QtGui.QColor(0, 0, 255),
                        )

    def highlight_row(self, tbl, row):
        """Highlights the selected row in the specified table.

        Parameters
        ----------
        tbl: QTableWidget
            Object of QTableWidget
        row: int
            Row selected
        """

        if self.current_tab == "Stations":
            ncol = 1
        else:
            ncol = 0

        # Set all backround to white
        for nrow in range(tbl.rowCount()):
            tbl.item(nrow, ncol).setBackground(QtGui.QColor(255, 255, 255))
            # Set selected row background to gray
            tbl.item(row, ncol).setBackground(QtGui.QColor(191, 191, 191))
        tbl.scrollToItem(tbl.item(row, 0))

    def valid_row(self, row, header=0):
        """Determines if the selected row is a measured vertical and adjusts
        the main_row_idx and vertical_idx
        appropriately.

        Parameters
        ----------
        row: int
            Row from selected table
        header: int
            Number of summary rows at top of table
        """
        if header > 0:
            row_check = header
        else:
            row_check = 0
        row_max = len(self.meas.summary.index) - 3
        if (
            (row > row_check)
            and (row < row_max)
            and (
                self.meas.summary["Station Number"][row]
                == np.floor(self.meas.summary["Station Number"][row])
            )
        ):
            self.main_row_idx = row + 1 - header
            self.vertical_idx = self.meas.verticals_used_idx[
                int(self.meas.summary["Station Number"][row - header] - 1)
            ]
            self.stations_row = np.where(
                self.meas.verticals_sorted_idx == self.vertical_idx
            )[0][0]
            return True
        else:
            return False

    # Toolbar functions
    # =================
    def select_measurement(self):
        """Opens a dialog to allow the user to load measurement file(s) for
        viewing or processing.
        """
        # Open dialog
        select = OpenMeasurementDialog(parent=self)

        # Update sticky_settings
        self.sticky_settings = SSet(self.settingsFile)

        response = ""
        # If a selection is made begin loading
        if len(select.type) > 0:
            # Create measurement object
            self.meas = Measurement(
                uncertainty=self.uncertainty,
                agency_options=self.agency_options,
                kmeans=self.kmeans,
                qt_tr=self.tr,
                time_zone_required=self.time_zone_required
            )

            # Load Sontek data
            if select.type == "SonTek":
                with self.wait_cursor():
                    # Show folder name in GUI header
                    self.setWindowTitle(self.QRevMS_version + ": " + select.fullName[0])
                    response = self.meas.from_sontek(select.fullName[0])
                    if len(response) > 0:
                        self.popup_message(response)

            # Load and process TRDI data
            elif select.type == "TRDI":
                with self.wait_cursor():
                    # Show mmt filename in GUI header
                    self.setWindowTitle(self.QRevMS_version + ": " + select.fullName[0])
                    response = self.meas.from_trdi(filename=select.fullName[0])
                    if len(response) > 0:
                        self.popup_message(response)

            # Load QRev data
            elif select.type == "QRevMS":
                # Show QRev filename in GUI header
                self.setWindowTitle(self.QRevMS_version + ": " + select.fullName[0])

                # Create zipfile object
                with zipfile.ZipFile(select.fullName[0], mode="r") as z:
                    # Check for presence of json file
                    z_list = z.namelist()
                    json_files = [match for match in z_list if ".json" in match]

                    # Always load first match
                    if len(json_files) > 0:
                        self.meas = load(
                            z.open(json_files[0], mode="r"), conv_str_byte=True
                        )
                        self.meas.from_qrevms(qt_tr=self.tr)
                    else:
                        response = self.tr("The selected file in not a QRevMS file.")
                        self.popup_message(response)

            if len(response) == 0:
                self.actionSave.setDisabled(False)
                self.find_first_measured_vertical()
                self.check_magnitude_method()
                self.config_gui()
                self.change = True
                self.tab_manager(tab_idx="Main", subtab_idx=0)

    def save_measurement(self):
        """Save measurement in Matlab format."""

        if self.rating_prompt:
            self.set_user_rating()

        # Get filename for saved files
        save_file = SaveDialog(parent=self)

        # Find extension length
        n_ext = -1 * len(pathlib.Path(save_file.full_Name).suffix)

        if len(save_file.full_Name) > 0:
            # Add comment when saving file
            time_stamp = datetime.now().strftime(self.date_format + " %H:%M:%S")
            user_name = getpass.getuser()
            discharge = self.meas.summary.loc["Total"]["Q"]
            if self.meas.uncertainty.method == "IVE":
                uncertainty = self.meas.uncertainty.u_ive["u95_q"] * 100
            elif self.meas.uncertainty.method == "ISO":
                uncertainty = self.meas.uncertainty.u_iso["u95_q"] * 100
            else:
                uncertainty = self.meas.uncertainty.u_user["u95_q"] * 100
            text = (
                "["
                + time_stamp
                + ", "
                + user_name
                + "]:  "
                + "File Saved.  (Q = "
                + "{} ".format(self.q_digits(discharge * self.units["Q"]))
                + self.units["label_Q"][1:-1]
                + ",   Uncertainty: {:4.1f} %)".format(uncertainty)
            )
            self.meas.comments.append(text)
            self.main_tab_comments()

            # Save PDF Summary Report
            create_pdf = False
            if self.pdf_setting == "Always":
                create_pdf = True

            elif self.pdf_setting == "Prompt":
                reply = QtWidgets.QMessageBox.question(
                    self,
                    self.tr("PDF Summary"),
                    self.tr("Would you like to save a PDF Report Summary"),
                    QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                    QtWidgets.QMessageBox.No,
                )
                if reply == QtWidgets.QMessageBox.Yes:
                    create_pdf = True
            with self.wait_cursor():
                if create_pdf:
                    pdf_fullName = save_file.full_Name[:n_ext] + ".pdf"
                    pdf = Report(pdf_fullName, self)
                    pdf.create()

                # Save data
                json_file = save_file.full_Name.split("/")[-1][:n_ext] + ".json"
                if os.path.exists(save_file.full_Name):
                    os.remove(save_file.full_Name)

                # Remove qt.tr from Measurement, QAData
                meas_save = copy.deepcopy(self.meas)
                meas_save.tr = None
                meas_save.qa.tr = None
                with zipfile.ZipFile(
                    save_file.full_Name,
                    mode="x",
                    compression=zipfile.ZIP_DEFLATED,
                    compresslevel=-1,
                ) as z:
                    z.writestr(
                        json_file, dumps(meas_save, save_file.full_Name, allow_nan=True)
                    )
                del meas_save

                # Save Excel file
                excel_file = save_file.full_Name[:n_ext] + ".xlsx"
                self.meas.summary.to_excel(excel_file)

                # Save xml file
                self.meas.xml_output(
                    self.QRevMS_version, save_file.full_Name[:n_ext] + ".xml"
                )

            # Notify user save is complete
            QtWidgets.QMessageBox.about(
                self,
                self.tr("Save"),
                self.tr("Files have been saved."),
            )
            _ = self.display_bt_tab()

    def change_options(self):
        """Change user options"""

        # Initialize options dialog
        options = Options()

        # Set dialog to current settings
        if not self.agency_options["Units"]["show"]:
            options.gb_units.hide()
        if self.units["ID"] == "SI":
            options.rb_si.setChecked(True)
        else:
            options.rb_english.setChecked(True)

        if not self.agency_options["ColorMap"]["show"]:
            options.gb_color_map.hide()
        if self.color_map == "viridis":
            options.rb_viridis.setChecked(True)
        else:
            options.rb_jet.setChecked(True)

        if not self.agency_options["RatingPrompt"]["show"]:
            options.gb_rating.hide()
        if self.rating_prompt:
            options.cb_rating.setChecked(True)
        else:
            options.cb_rating.setChecked(False)

        if not self.agency_options["BTActive"]["show"]:
            options.gb_bt.hide()
        if self.bt_always_active:
            options.rb_bt_always.setChecked(True)
        else:
            options.rb_bt_only.setChecked(True)

        if not self.agency_options["Uncertainty"]["show"]:
            options.gb_uncertainty_options.hide()
        if self.uncertainty[0:3] == "IVE":
            options.rb_ive.setChecked(True)
        else:
            options.rb_iso.setChecked(True)

        if self.pdf_setting == "No":
            options.rb_pdf_no.setChecked(True)
        elif self.pdf_setting == "Always":
            options.rb_pdf_always.setChecked(True)
        else:
            options.rb_pdf_prompt.setChecked(True)

        if not self.agency_options["SlushFilter"]["show"]:
            options.gb_slush_filter.hide()
        if self.kmeans:
            options.cb_slush_filter.setChecked(True)
        else:
            options.cb_slush_filter.setChecked(False)

        if self.q_digits_method == "sigfig":
            options.rb_sigfig.setChecked(True)
        else:
            options.rb_decimal.setChecked(True)

        options.ed_digits.setText("{:d}".format(self.q_digits_digits))

        # Execute the options window
        rsp = options.exec_()

        with self.wait_cursor():
            # Apply settings from options window
            if rsp == QtWidgets.QDialog.Accepted:
                self.change = False

                # Units options
                if options.rb_english.isChecked():
                    if self.units["ID"] == "SI":
                        self.units = units_conversion(units_id="English")
                        self.sticky_settings.set("UnitsID", "English")
                        if self.meas is not None:
                            self.change = True
                            # self.tab_manager()

                else:
                    if self.units["ID"] == "English":
                        self.units = units_conversion(units_id="SI")
                        self.sticky_settings.set("UnitsID", "SI")
                        if self.meas is not None:
                            self.change = True
                            # self.tab_manager()

                # Color map
                if options.rb_viridis.isChecked():
                    if self.color_map != "viridis":
                        self.color_map = "viridis"
                        self.sticky_settings.set("ColorMap", "viridis")
                        if self.meas is not None:
                            self.change = True
                            # self.tab_manager()

                else:
                    if self.color_map != "jet":
                        self.color_map = "jet"
                        self.sticky_settings.set("ColorMap", "jet")
                        if self.meas is not None:
                            self.change = True
                            # self.tab_manager()

                # Discharge display units options
                if options.rb_sigfig.isChecked() and self.q_digits_method != "sigfig":
                    self.q_digits_method = "sigfig"
                    self.sticky_settings.set("QDigitsMethod",
                                             self.q_digits_method)
                    if self.meas is not None:
                        self.main_update()
                        self.change = True
                        self.map_change = True
                elif options.rb_decimal.isChecked() and self.q_digits_method != "fixed":
                    self.q_digits_method = "fixed"
                    self.sticky_settings.set("QDigitsMethod",
                                             self.q_digits_method)
                    if self.meas is not None:
                        self.main_update()
                        self.change = True
                        self.map_change = True

                if options.cb_rating.isChecked():
                    if self.rating_prompt is False:
                        self.rating_prompt = True
                        self.sticky_settings.set("UserRating", True)
                        if self.meas is not None:
                            self.change = True
                            # self.tab_manager()

                else:
                    if self.rating_prompt:
                        self.rating_prompt = False
                        self.sticky_settings.set("UserRating", False)
                        if self.meas is not None:
                            self.change = True
                            # self.tab_manager()

                if options.rb_bt_always.isChecked():
                    if self.bt_always_active is False:
                        self.bt_always_active = True
                        # self.display_bt_tab()
                        self.sticky_settings.set("BTActive", True)
                        if self.meas is not None:
                            self.change = True
                            # self.tab_manager()

                else:
                    if self.bt_always_active:
                        self.bt_always_active = False
                        # self.display_bt_tab()
                        self.sticky_settings.set("BTActive", False)
                        if self.meas is not None:
                            self.change = True
                            # self.tab_manager()

                if options.rb_ive.isChecked():
                    self.sticky_settings.set("Uncertainty", "IVE")
                    if self.uncertainty[0:3] != "IVE":
                        self.uncertainty = "IVE"
                        if self.meas is not None:
                            self.meas.change_uncertainty("IVE")
                            self.change = True
                elif options.rb_iso.isChecked():
                    self.sticky_settings.set("Uncertainty", "ISO")
                    if self.uncertainty[0:3] != "ISO":
                        self.uncertainty = "ISO"
                        if self.meas is not None:
                            self.meas.change_uncertainty("ISO")
                            self.change = True

                if options.rb_pdf_no.isChecked():
                    self.pdf_setting = "No"
                elif options.rb_pdf_prompt.isChecked():
                    self.pdf_setting = "Prompt"
                elif options.rb_pdf_always.isChecked():
                    self.pdf_setting = "Always"

                slush_filter = options.cb_slush_filter.isChecked()
                if slush_filter != self.kmeans:
                    self.sticky_settings.set("SlushFilter", slush_filter)
                    self.kmeans = slush_filter
                    if self.meas is not None:
                        self.meas.change_kmeans(self.kmeans)
                        self.change = True

                if self.change:
                    self.tab_manager()

    def add_comment(self):
        """Add comment triggered by actionComment"""

        if self.meas is not None:
            # Initialize comment dialog
            tab_name = self.top_tab.tabText(self.top_tab.currentIndex())
            comment = Comment(tab_name)
            comment_entered = comment.exec_()

            # If comment entered and measurement open, save comment,
            # and update comments tab.
            if comment_entered:
                if self.meas is not None:
                    self.meas.comments.append(comment.text_edit_comment.toPlainText())
                self.change = True
                self.update_comments()

    def update_comments(self, tab_idx=None):
        """Manages the initialization of content for each tab and updates
        that information as necessary.

        Parameters
        ----------
        tab_idx: int
            Index of tab clicked by user
        """

        # Determine selected tab
        if tab_idx is None:
            tab_idx = self.current_tab
        else:
            self.current_tab = tab_idx

        # Main
        if tab_idx == "Main":
            self.main_tab_comments()

        # System Test
        elif tab_idx == "SysTest":
            self.systest_comments_messages()

        # Compass/PR
        elif tab_idx == "Compass/P/R":
            self.compass_comments_messages()

        # Temp/Sal
        elif tab_idx == "Temp/Sal":
            self.tempsal_comments_messages()

        # Moving-bed test
        elif tab_idx == "Stations":
            self.stations_comments_messages()

        # Bottom track
        elif tab_idx == "BT":
            self.bt_comments_messages()

        # Depth
        elif tab_idx == "Depth":
            self.depth_comments_messages()

        # WT
        elif tab_idx == "WT":
            self.wt_comments_messages()

        # Extrapolation
        elif tab_idx == "Extrap":
            self.extrap_comments_messages()

        # Edges
        elif tab_idx == "Edges":
            self.edges_comments_messages()

    def set_vel_ref_all(self):
        """Allows changing of velocity reference from the toolbar."""

        # Initialize dialog
        dialog = VelocityRef()
        tab_idx = self.current_tab
        if self.meas.discharge_method == "None":
            dialog.combo_vel_ref.setCurrentIndex(1)
            if self.current_tab == "BT":
                tab_idx = "Main"
        else:
            dialog.combo_vel_ref.setCurrentIndex(0)
        rsp = dialog.exec_()

        with self.wait_cursor():
            # If user clicks apply all, apply change
            if rsp == QtWidgets.QDialog.Accepted:
                ref = dialog.combo_vel_ref.currentText()
                old_discharge = self.meas.summary.copy()
                self.meas.change_vel_reference(idx=None, ref=ref)
                _ = self.display_bt_tab()
                self.change = True
                self.update_toolbar_info()
                self.tab_manager(tab_idx=tab_idx, old_discharge=old_discharge)

    def set_q_method_all(self):
        """Allows changing the discharge method from the toolbar."""

        # Initialize dialog
        dialog = QMethod()
        if self.meas.discharge_method == "Mid":
            dialog.combo_discharge_method.setCurrentIndex(0)
        else:
            dialog.combo_discharge_method.setCurrentIndex(1)
        rsp = dialog.exec_()

        with self.wait_cursor():
            # If user clicks OK, apply change
            if rsp == QtWidgets.QDialog.Accepted:
                text = dialog.combo_discharge_method.currentText()
                # Compute discharge using the specified method
                if text == "Mid-Section":
                    if self.meas.discharge_method == "Mean":
                        self.meas.change_discharge_method(new_method="Mid")
                else:
                    if self.meas.discharge_method == "Mid":
                        self.meas.change_discharge_method(new_method="Mean")
                # Update GUI
                self.change = True
                self.update_toolbar_info()
                self.find_first_measured_vertical()
                self.tab_manager()

    def set_vel_method_all(self):
        """Allows changing the velocity method from the toolbar."""

        # Initialize dialog
        dialog = VelocityMethod()
        vel_method_dict = {"Fixed": 0, "Azimuth": 1, "Magnitude": 2}
        dialog.combo_vel_method.setCurrentIndex(
            vel_method_dict[self.meas.verticals[self.vertical_idx].velocity_method]
        )
        rsp = dialog.exec_()

        with self.wait_cursor():
            old_discharge = self.meas.summary.copy()
            # If user clicks apply all, apply change
            if rsp == QtWidgets.QDialog.Accepted:
                method = dialog.combo_vel_method.currentText()
                result = self.meas.change_vel_method(idx=None, method=method)
                if len(result) > 0:
                    self.popup_message(result)
                    return
                self.change = True
                self.check_magnitude_method()
                self.update_toolbar_info()
                self.tab_manager(old_discharge=old_discharge)

    def set_azimuth(self):
        """Allows changing the velocity method from the toolbar."""

        # Initialize dialog
        dialog = Azimuth()
        dialog.ed_azimuth.setText(f"{self.meas.tagline_azimuth_deg:.2f}")
        rsp = dialog.exec_()

        with self.wait_cursor():
            # If user clicks OK, apply change
            if rsp == QtWidgets.QDialog.Accepted:
                az = self.check_numeric_input(dialog.ed_azimuth, block=False)
                if az is not None:
                    old_discharge = self.meas.summary.copy()
                    self.meas.change_azimuth(az)
                    # Update GUI
                    self.change = True
                    self.update_toolbar_info()
                    self.tab_manager(old_discharge=old_discharge)

    def set_user_rating(self):
        """Allow user to set the measurement rating."""

        # Intialize dialog
        rating_dialog = Rating(self)

        uncertainty = np.nan

        # Get computed uncertainty
        if (
            self.meas.uncertainty.method == "IVE"
            or self.meas.uncertainty.method == "IVE_User"
        ):
            if self.meas.uncertainty.method == "IVE":
                uncertainty = self.meas.uncertainty.u_ive["u95_q"] * 100
            else:
                uncertainty = self.meas.uncertainty.u_user["u95_q"] * 100
        elif (
            self.meas.uncertainty.method == "ISO"
            or self.meas.uncertainty.method == "ISO_User"
        ):
            if self.meas.uncertainty.method == "ISO":
                uncertainty = self.meas.uncertainty.u_iso["u95_q"] * 100
            else:
                uncertainty = self.meas.uncertainty.u_user["u95_q"] * 100

        rating_dialog.uncertainty_value.setText(f"{uncertainty:.1f}")

        # If user hasn't previously rated the measurement set default rating to
        # rating based on uncertainty
        if self.meas.user_rating in ["Not Rated", ""]:
            if uncertainty < 3:
                rating_dialog.rb_excellent.setChecked(True)
            elif uncertainty < 5.01:
                rating_dialog.rb_good.setChecked(True)
            elif uncertainty < 8.01:
                rating_dialog.rb_fair.setChecked(True)
            else:
                rating_dialog.rb_poor.setChecked(True)
        else:
            if "Excellent" in self.meas.user_rating:
                rating_dialog.rb_excellent.setChecked(True)
            elif "Good" in self.meas.user_rating:
                rating_dialog.rb_good.setChecked(True)
            elif "Fair" in self.meas.user_rating:
                rating_dialog.rb_fair.setChecked(True)
            elif "Poor" in self.meas.user_rating:
                rating_dialog.rb_poor.setChecked(True)

        rsp = rating_dialog.exec_()

        # If data entered.
        if rsp == QtWidgets.QDialog.Accepted:
            if rating_dialog.rb_excellent.isChecked():
                rating = self.tr("Excellent")
            elif rating_dialog.rb_good.isChecked():
                rating = self.tr("Good")
            elif rating_dialog.rb_fair.isChecked():
                rating = self.tr("Fair")
            else:
                rating = self.tr("Poor")

            # Store user rating
            self.meas.user_rating = rating
            self.meas.qa.rating_qa(self.meas)

        # If user rating is better than rating suggested by uncertainty require a comment
        if self.meas.qa.rating["status"] == "caution":
            comment = Comment(tab_name="User Rating Justification")
            comment_entered = comment.exec_()

            # If comment entered and measurement open, save comment,
            # and update comments tab.
            if comment_entered:
                if self.meas is not None:
                    self.meas.comments.append(comment.text_edit_comment.toPlainText())
                self.change = True
                self.update_comments()

        # Update toolbar
        self.meas.qa.rating_qa(self.meas)
        self.update_toolbar_info()
        self.main_tab_messages()
        self.main_messages()

    def home(self):
        """Reset graphics to default."""

        for tb in self.toolbars:
            tb.home()

    def zoom(self):
        """Zoom graphics using window."""

        for tb in self.toolbars:
            tb.zoom()
        self.actionPan.setChecked(False)
        self.actionData_Cursor.setChecked(False)
        self.data_cursor()

    def pan(self):
        """Pan graph."""

        for tb in self.toolbars:
            tb.pan()
        self.actionZoom.setChecked(False)
        self.actionData_Cursor.setChecked(False)
        self.data_cursor()

    def data_cursor(self):
        """Apply the data cursor."""

        if self.actionData_Cursor.isChecked():
            for fig in self.figs:
                if self.actionPan.isChecked():
                    self.actionPan.trigger()
                if self.actionZoom.isChecked():
                    self.actionZoom.trigger()
                self.actionData_Cursor.setChecked(True)
                if fig is not None:
                    fig.set_hover_connection(True)
        else:
            for fig in self.figs:
                fig.set_hover_connection(False)

    def help(self):
        """Opens pdf help file user's default pdf viewer."""
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Question)
        msg.addButton(self.tr("Users Manual"), msg.ActionRole)
        msg.addButton(self.tr("Technical Manual"), msg.ActionRole)
        msg.addButton(self.tr("About"), msg.ActionRole)
        msg.addButton(self.tr("Cancel"), msg.ActionRole)
        msg.setWindowTitle("Help Documents")
        msg.setWindowIcon(QtGui.QIcon("QRevMS.ico"))
        msg.exec_()

        help_file = os.path.join(self.application_path, "Help")
        if msg.clickedButton().text() == "Users Manual":
            help_file = os.path.join(help_file, "QRevMS_Users.pdf")
            os.startfile(help_file)
        elif msg.clickedButton().text() == "Technical Manual":
            help_file = os.path.join(help_file, "QRevMS_Tech.pdf")
            os.startfile(help_file)
        elif msg.clickedButton().text() == "About":
            help_file = os.path.join(help_file, "QRevMS_About.pdf")
            os.startfile(help_file)

    def update_toolbar_info(self):
        """Updates the information presented in the toolbar.py"""

        # Disable toolbar buttons
        self.actionVelRef.setDisabled(True)
        self.actionVelRefSet.setDisabled(True)
        self.actionQMethod.setDisabled(True)
        self.actionQMethodSet.setDisabled(True)
        self.actionVelMethod.setDisabled(True)
        self.actionVelMethodSet.setDisabled(True)
        self.actionAz.setDisabled(True)
        self.actionAzSet.setDisabled(True)
        self.actionUserRating.setDisabled(True)
        self.actionUserRatingSet.setDisabled(True)

        # Prep data
        nav_ref = []
        vel_method = []
        for idx in self.meas.verticals_used_idx:
            nav_ref.append(self.meas.verticals[idx].velocity_reference)
            vel_method.append(self.meas.verticals[idx].velocity_method)

        # Velocity or navigation reference
        ref = np.unique(nav_ref[1:-1])
        if len(ref) > 1:
            ref = self.tr("Mixed")
        elif ref[0] == "None":
            ref = self.tr("None")
        elif ref[0] == "BT":
            ref = self.tr("BT")
        elif ref[0] == "bt_vel":
            ref = self.tr("BT")

        self.actionVelRefSet.setText(ref)

        # Discharge method
        if self.meas.discharge_method == "Mid":
            q_method = self.tr("Mid")
        else:
            q_method = self.tr("Mean")
        self.actionQMethodSet.setText(q_method)

        # Velocity Method
        method = np.unique(vel_method)[1:]
        if len(method) > 1:
            method = "Mixed"
        elif method == "Fixed":
            method = self.tr("Fixed")
        elif method == "Azimuth":
            method = self.tr("Azimuth")
        else:
            method = self.tr("Magnitude")
        self.actionVelMethodSet.setText(method)

        # Azimuth
        if method == "Fixed" or method == "Magnitude":
            self.actionAzSet.setText("N/A")
        elif not np.isnan(self.meas.tagline_azimuth_deg):
            self.actionAzSet.setText("{:3.1f}".format(self.meas.tagline_azimuth_deg))
        else:
            self.actionAzSet.setText("N/A")

        # User Rating
        self.actionUserRatingSet.setText(self.meas.user_rating)

        # Enable buttons on toolbar
        self.actionVelRef.setEnabled(True)
        self.actionVelRefSet.setEnabled(True)
        self.actionQMethod.setEnabled(True)
        self.actionQMethodSet.setEnabled(True)
        self.actionVelMethod.setEnabled(True)
        self.actionVelMethodSet.setEnabled(True)
        self.actionAz.setEnabled(True)
        self.actionAzSet.setEnabled(True)
        self.actionUserRating.setEnabled(True)
        self.actionUserRatingSet.setEnabled(True)

    # Main Tab
    # ========
    def main_update(self):
        """Update main tab."""

        # Update only if at least one vertical is used
        if len(self.meas.verticals_used_idx) > 0:
            with self.wait_cursor():
                # If this is the first time this tab is used setup interface
                # connections
                if not self.main_initialized:
                    self.main_summary_table.cellClicked.connect(
                        self.main_select_vertical
                    )
                    self.main_details_table.cellClicked.connect(
                        self.main_select_vertical
                    )
                    self.main_settings_table.cellClicked.connect(
                        self.main_select_vertical_settings_table
                    )
                    self.ed_site_name.editingFinished.connect(
                        self.main_update_site_name
                    )
                    self.ed_site_number.editingFinished.connect(
                        self.main_update_site_number
                    )
                    self.ed_persons.editingFinished.connect(self.main_update_persons)
                    self.ed_measurement_number.editingFinished.connect(
                        self.main_update_meas_number
                    )
                    self.ed_stage_start.editingFinished.connect(
                        self.main_update_stage_start
                    )
                    self.ed_stage_end.editingFinished.connect(
                        self.main_update_stage_end
                    )
                    self.ed_stage_meas.editingFinished.connect(
                        self.main_update_stage_meas
                    )
                    self.ed_beam_misalignment.editingFinished.connect(
                        self.main_update_beam_misalignment
                    )
                    self.combo_timezone.currentIndexChanged[str].connect(
                        self.update_time_zone)
                    
                    # Main tab has been initialized
                    self.main_initialized = True

                # Get index of first vertical with measured data
                for idx in range(len(self.meas.verticals)):
                    if (
                        self.meas.verticals[idx].use
                        and self.meas.verticals[idx].sampling_duration_sec > 0
                    ):
                        self.first_idx = idx
                        break

                # Set toolbar navigation reference
                self.update_toolbar_info()

                self.main_table_summary()
                self.main_table_details()
                self.main_tab_premeasurement()
                self.main_table_settings()
                self.main_table_adcp()
                self.main_graphics_percent_discharge()
                self.main_graphics_velocity()
                self.main_graphics_extrap()
                self.main_graphics_uncertainty()
                self.main_tab_messages()
                self.main_tab_comments()
                self.main_messages()

                # Setup list for use by graphics controls
                self.canvases = [
                    self.main_velocity_canvas,
                    self.main_extrap_canvas,
                    self.main_percent_discharge_canvas,
                    self.main_uncertainty_canvas,
                    self.main_uncertainty_canvas,
                ]
                self.figs = [
                    self.main_velocity_fig,
                    self.main_extrap_fig,
                    self.main_percent_discharge_fig,
                    self.main_uncertainty_fig,
                    self.main_uncertainty_fig,
                ]
                self.toolbars = [
                    self.main_velocity_toolbar,
                    self.main_extrap_toolbar,
                    self.main_percent_discharge_toolbar,
                    self.main_uncertainty_toolbar,
                    self.main_uncertainty_toolbar,
                ]
                self.fig_calls = [
                    self.main_graphics_velocity,
                    self.main_graphics_extrap,
                    self.main_graphics_percent_discharge,
                    self.main_graphics_uncertainty,
                ]
                self.ui_parents = [i.parent() for i in self.canvases]
                self.figs_menu_connection()

    def main_table_summary(self):
        """Populates a summary table based on the mid-section discharge
        method results.
        """

        tbl = self.main_summary_table
        tbl.clear()
        
        # Define headers
        summary_header = [
            self.tr("Station" + "\n" + " Number"),
            self.tr("Location" + "\n") + self.units["label_L"],
            self.tr("Condition"),
            self.tr("Duration" + "\n") + self.tr("(s)"),
            self.tr("Number" + "\n" + "Valid" + "\n" + "Ensembles"),
            self.tr("Number" + "\n" + "Valid" + "\n" + "Cells"),
            self.tr("Ice" + "\n" + "Depth" + "\n") + self.units["label_L"],
            self.tr("Slush" + "\n" + "Depth" + "\n") + self.units["label_L"],
            self.tr("ADCP" + "\n" + "Depth" + "\n") + self.units["label_L"],
            self.tr("Depth" + "\n") + self.units["label_L"],
            self.tr("Width" + "\n") + self.units["label_L"],
            self.tr("Area" + "\n") + self.units["label_A"],
            self.tr("Velocity" + "\n" + "Method"),
            self.tr("Measured" + "\n" + "Normal" + "\n" + "Velocity" + "\n")
            + self.units["label_V"],
            self.tr("Measured" + "\n" + "Normal" + "\n" + "Velocity" + "\n" + "CV %"),
            self.tr("Normal" + "\n" + "Velocity" + "\n") + self.units["label_V"],
            self.tr("Velocity" + "\n" + "Angle" + "\n" + "(deg)"),
            self.tr("Flow Angle" + "\n" + "Coefficient"),
            self.tr("Q" + "\n") + self.units["label_Q"],
            self.tr("%Q"),
        ]

        # Setup table
        ncols = len(summary_header)
        nrows = self.meas.summary.shape[0] - 2
        tbl.setRowCount(nrows + 1)
        tbl.setColumnCount(ncols)
        tbl.setFont(self.font_normal)
        tbl.setHorizontalHeaderLabels(summary_header)
        tbl.horizontalHeader().setFont(self.font_bold)
        tbl.verticalHeader().hide()
        tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)

        # Populate table
        if nrows > 0:
            for row in range(nrows):
                # Populate verticals
                # ------------------

                # Station Number
                col = 0
                if np.isnan(self.meas.summary["Station Number"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                elif np.modf(self.meas.summary["Station Number"][row])[0] == 0:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:3.0f}".format(self.meas.summary["Station Number"][row])
                        ),
                    )
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:3.1f}".format(self.meas.summary["Station Number"][row])
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Location
                col += 1
                if np.isnan(self.meas.summary["Location"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:6.3f}".format(
                                self.meas.summary["Location"][row] * self.units["L"]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Condition
                col += 1
                if not isinstance(self.meas.summary["Condition"][row], str):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(self.meas.summary["Condition"][row]),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Duration
                col += 1
                if np.isnan(self.meas.summary["Duration"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:4.1f}".format(self.meas.summary["Duration"][row])
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Number Valid Profiles
                col += 1
                if np.isnan(self.meas.summary["Number Valid Profiles"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:4.0f}".format(
                                self.meas.summary["Number Valid Profiles"][row]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                if (
                    self.meas.summary["Station Number"][row]
                    in self.meas.qa.verticals["valid_warning"]
                ):
                    tbl.item(row + 1, col).setBackground(QtGui.QColor(255, 77, 77))
                    tbl.item(row + 1, col).setToolTip(
                        "The percent of invalid ensembles exceeds {:2d}%.".format(
                            self.meas.qa.percent_valid_warning_threshold
                        )
                    )
                elif (
                    self.meas.summary["Station Number"][row]
                    in self.meas.qa.verticals["valid_caution"]
                ):
                    tbl.item(row + 1, col).setBackground(QtGui.QColor(255, 204, 0))
                    tbl.item(row + 1, col).setToolTip(
                        "The percent of invalid ensembles exceeds {:2d}%.".format(
                            self.meas.qa.percent_valid_caution_threshold
                        )
                    )
                else:
                    tbl.item(row + 1, col).setBackground(QtGui.QColor(255, 255, 255))

                # Number Valid Cells
                col += 1
                if np.isnan(self.meas.summary["Number Cells"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:4.0f}".format(self.meas.summary["Number Cells"][row])
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Ice Depth
                col += 1
                if np.isnan(self.meas.summary["Depth to Ice Bottom"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:4.2f}".format(
                                self.meas.summary["Depth to Ice Bottom"][row]
                                * self.units["L"]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Slush Depth
                col += 1
                if np.isnan(self.meas.summary["Depth to Slush Bottom"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:4.2f}".format(
                                self.meas.summary["Depth to Slush Bottom"][row]
                                * self.units["L"]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # ADCP Depth
                col += 1
                if np.isnan(self.meas.summary["ADCP Depth"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:4.3f}".format(
                                self.meas.summary["ADCP Depth"][row] * self.units["L"]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Depth
                col += 1
                if np.isnan(self.meas.summary["Depth"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:5.3f}".format(
                                self.meas.summary["Depth"][row] * self.units["L"]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Width
                col += 1
                if np.isnan(self.meas.summary["Width"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:7.3f}".format(
                                self.meas.summary["Width"][row] * self.units["L"]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Area
                col += 1
                if np.isnan(self.meas.summary["Area"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:8.3f}".format(
                                self.meas.summary["Area"][row] * self.units["A"]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Velocity Method
                col += 1
                if not isinstance(self.meas.summary["Velocity Method"][row], str):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            self.meas.summary["Velocity Method"][row]
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Measured Normal Velocity
                col += 1
                if np.isnan(self.meas.summary["Measured Normal Velocity"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:4.3f}".format(
                                self.meas.summary["Measured Normal Velocity"][row]
                                * self.units["V"]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Normal Velocity Measured Speed CV
                col += 1
                if np.isnan(self.meas.summary["Measured Normal Velocity CV"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:4.2f}".format(
                                self.meas.summary["Measured Normal Velocity CV"][row]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Normal Velocity
                col += 1
                if np.isnan(self.meas.summary["Normal Velocity"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:4.3f}".format(
                                self.meas.summary["Normal Velocity"][row]
                                * self.units["V"]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                if (
                    self.meas.summary["Station Number"][row]
                    in self.meas.qa.w_vel["all_invalid"]["All: "]
                ):
                    tbl.item(row + 1, col).setBackground(QtGui.QColor(255, 77, 77))
                elif (
                    self.meas.summary["Station Number"][row]
                    in self.meas.qa.w_vel["total_warning"]["All: "]
                ):
                    tbl.item(row + 1, col).setBackground(QtGui.QColor(255, 77, 77))
                elif (
                    self.meas.summary["Station Number"][row]
                    in self.meas.qa.w_vel["total_caution"]["All: "]
                ):
                    tbl.item(row + 1, col).setBackground(QtGui.QColor(255, 204, 0))
                elif tbl.item(row + 1, col) is not None:
                    tbl.item(row + 1, col).setBackground(QtGui.QColor(255, 255, 255))
                if tbl.item(row + 1, col) is not None:
                    tbl.item(row + 1, col).setToolTip(
                        self.tr(self.wt_create_tooltip(row, 99))
                    )

                # Velocity Angle
                col += 1
                if np.isnan(self.meas.summary["Velocity Angle"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:4.2f}".format(self.meas.summary["Velocity Angle"][row])
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Flow Angle Correction
                col += 1
                if np.isnan(self.meas.summary["Flow Angle Coefficient"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:2.3f}".format(
                                self.meas.summary["Flow Angle Coefficient"][row]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Discharge
                col += 1
                if np.isnan(self.meas.summary["Q"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:8}".format(
                                self.q_digits(
                                    self.meas.summary["Q"][row] * self.units["Q"]
                                )
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Percent Discharge with highlighting based on value
                col += 1
                if np.isnan(self.meas.summary["Q%"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:3.2f}".format(self.meas.summary["Q%"][row])
                        ),
                    )
                    tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)
                    if (
                        self.meas.summary["Q%"][row]
                        > self.agency_options["StationQ"]["warning"]
                    ):
                        tbl.item(row + 1, col).setBackground(QtGui.QColor(255, 77, 77))
                        tbl.item(row + 1, col).setToolTip(
                            self.tr(
                                "Percent of total discharge > {}".format(
                                    self.agency_options["StationQ"]["warning"]
                                )
                            )
                        )
                    elif (
                        self.meas.summary["Q%"][row]
                        > self.agency_options["StationQ"]["caution"]
                    ):
                        tbl.item(row + 1, col).setBackground(QtGui.QColor(255, 204, 0))
                        tbl.item(row + 1, col).setToolTip(
                            self.tr(
                                "Percent of total discharge > {}".format(
                                    self.agency_options["StationQ"]["caution"]
                                )
                            )
                        )

            # Add measurement summaries
            # -------------------------

            # Station Number
            col = 0
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem("All"))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Location
            col += 1
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Condition
            col += 1
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Duration
            col += 1
            tbl.setItem(
                0,
                col,
                QtWidgets.QTableWidgetItem(
                    "{:6.1f}".format(self.meas.summary["Duration"]["Total"])
                ),
            )
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Number valid ensembles
            col += 1
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Number valid cells
            col += 1
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Ice Depth
            col += 1
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Slush Depth
            col += 1
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # ADCP Depth
            col += 1
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Depth
            col += 1
            tbl.setItem(
                0,
                col,
                QtWidgets.QTableWidgetItem(
                    "{:5.3f}".format(
                        self.meas.summary["Depth"]["Mean"] * self.units["L"]
                    )
                ),
            )
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Width
            col += 1
            tbl.setItem(
                0,
                col,
                QtWidgets.QTableWidgetItem(
                    "{:7.3f}".format(
                        self.meas.summary["Width"]["Total"] * self.units["L"]
                    )
                ),
            )
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Area
            col += 1
            tbl.setItem(
                0,
                col,
                QtWidgets.QTableWidgetItem(
                    "{:8.3f}".format(
                        self.meas.summary["Area"]["Total"] * self.units["A"]
                    )
                ),
            )
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Velocity Method
            col += 1
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Measured normal
            col += 1
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Measured normal cv
            col += 1
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Mean Velocity
            col += 1
            tbl.setItem(
                0,
                col,
                QtWidgets.QTableWidgetItem(
                    "{:4.3f}".format(
                        self.meas.summary["Normal Velocity"]["Mean"] * self.units["V"]
                    )
                ),
            )
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Velocity Angle
            col += 1
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Flow Angle Correction
            col += 1
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Discharge
            col += 1
            tbl.setItem(
                0,
                col,
                QtWidgets.QTableWidgetItem(
                    "{:8}".format(
                        self.q_digits(self.meas.summary["Q"]["Total"] * self.units["Q"])
                    )
                ),
            )
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Percent Discharge
            col += 1
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Bold Measurement row
            for col in range(ncols - 1):
                tbl.item(0, col).setFont(self.font_bold)

            # Hide unused columns
            if np.any(self.meas.summary["Condition"].str.contains("Ice")):
                tbl.setColumnHidden(6, False)
                tbl.setColumnHidden(7, False)
            else:
                tbl.setColumnHidden(6, True)
                tbl.setColumnHidden(7, True)

            self.main_shade_mean_method(tbl)

            if np.any(self.meas.summary["Velocity Method"].str.contains("Magnitude")):
                tbl.setColumnHidden(17, False)
            else:
                tbl.setColumnHidden(17, True)

            tbl.item(self.main_row_idx, 0).setBackground(QtGui.QColor(191, 191, 191))
            tbl.scrollToItem(tbl.item(self.main_row_idx, 0))

            tbl.resizeColumnsToContents()
            tbl.resizeRowsToContents()

        else:
            tbl.setRowCount(0)

        self.main_summary_table.setFocus()

    def main_table_details(self):
        """Poplulate the main tab details table."""
        tbl = self.main_details_table
        tbl.clear()
        
        # Find first measured vertical
        start_time_list = []
        end_time_list = []
        tz = None
        for idx in self.meas.verticals_used_idx:
            if self.meas.verticals[idx].data.date_time is not None:
                start_time_list.append(
                    self.meas.verticals[idx].data.date_time.start_serial_time
                )
                end_time_list.append(
                    self.meas.verticals[idx].data.date_time.end_serial_time
                )
                utc_time_offset = self.meas.verticals[idx].data.date_time.utc_time_offset

        start_time = tz_formatted_string(
            serial_time=np.nanmin(start_time_list),
            utc_time_offset=utc_time_offset,
            format="%H:%M:%S.%f")

        end_time = tz_formatted_string(
            serial_time=np.nanmax(end_time_list),
            utc_time_offset=utc_time_offset,
            format="%H:%M:%S.%f")

        start_date = tz_formatted_string(
            serial_time=np.nanmin(start_time_list),
            utc_time_offset=utc_time_offset,
            format=self.date_format)

        end_date = tz_formatted_string(
            serial_time=np.nanmin(end_time_list),
            utc_time_offset=utc_time_offset,
            format=self.date_format
        )

        start_header = "Start" + "\n" + "Time" + "\n" + start_date
        end_header = "End" + "\n" + "Time" + "\n" + end_date

        # Define headers
        summary_header = [
            self.tr("Station" + "\n" + " Number"),
            self.tr("Location" + "\n") + self.units["label_L"],
            self.tr(start_header),
            self.tr(end_header),
            self.tr("Depth" + "\n" + "Std." + "\n" + "Dev." + "\n")
            + self.units["label_L"],
            self.tr("Velocity" + "\n" + "Magnitude" + "\n") + self.units["label_V"],
            self.tr("Velocity" + "\n" + "Magnitude" + "\n" + "CV" + "\n" + "(%)"),
            self.tr("Velocity" + "\n" + "Direction" + "\n" + "(deg)"),
            self.tr(
                "Velocity" + "\n" + "Direction" + "\n" + "Std. Dev." + "\n" + "(deg)"
            ),
            self.tr("Q" + "\n" + "Top" + "\n") + self.units["label_Q"],
            self.tr("Q" + "\n" + "Middle" + "\n") + self.units["label_Q"],
            self.tr("Q" + "\n" + "Bottom" + "\n") + self.units["label_Q"],
            self.tr("Depth %" + "\n" + "Invalid"),
            self.tr("WT %" + "\n" + "Invalid"),
            self.tr("Ice" + "\n" + "Area" + "\n") + self.units["label_A"],
            self.tr("Slush" + "\n" + "Area" + "\n") + self.units["label_A"],
            self.tr("Ice" + "\n" + "Thickness" + "\n") + self.units["label_L"],
            self.tr("Average" + "\n" + "Voltage"),
        ]

        # Setup table
        ncols = len(summary_header)
        nrows = self.meas.summary.shape[0] - 2
        tbl.setRowCount(nrows + 1)
        tbl.setColumnCount(ncols)
        tbl.setFont(self.font_normal)
        tbl.setHorizontalHeaderLabels(summary_header)
        tbl.horizontalHeader().setFont(self.font_bold)
        tbl.verticalHeader().hide()
        tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)

        # Populate table
        if nrows > 0:
            for row in range(nrows):
                # Populate verticals
                # ------------------

                # Station Number
                col = 0
                if np.isnan(self.meas.summary["Station Number"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                elif self.meas.summary["Station Number"][row] == np.floor(
                    self.meas.summary["Station Number"][row]
                ):
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:3.0f}".format(self.meas.summary["Station Number"][row])
                        ),
                    )
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:3.1f}".format(self.meas.summary["Station Number"][row])
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Location
                col += 1
                if np.isnan(self.meas.summary["Location"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:6.2f}".format(
                                self.meas.summary["Location"][row] * self.units["L"]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Start Time
                col += 1
                if len(self.meas.summary["Start Time"][row]) < 1:
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    txt = self.meas.summary["Start Time"][row][
                        0 : self.meas.summary["Start Time"][row].find(".")
                    ]
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(txt))
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # End Time
                col += 1
                if len(self.meas.summary["End Time"][row]) < 1:
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    txt = self.meas.summary["End Time"][row][
                        0 : self.meas.summary["End Time"][row].find(".")
                    ]
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(txt))
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Depth Std Dev
                col += 1
                if np.isnan(self.meas.summary["Depth StdDev"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:6.2f}".format(
                                self.meas.summary["Depth StdDev"][row] * self.units["L"]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Velocity Magnitude
                col += 1
                if np.isnan(self.meas.summary["Velocity Magnitude"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:4.3f}".format(
                                self.meas.summary["Velocity Magnitude"][row]
                                * self.units["V"]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Velocity CV
                col += 1
                if np.isnan(self.meas.summary["Velocity Magnitude CV"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:6.2f}".format(
                                self.meas.summary["Velocity Magnitude CV"][row]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Velocity Direction
                col += 1
                if np.isnan(self.meas.summary["Velocity Direction"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:4.2f}".format(
                                self.meas.summary["Velocity Direction"][row]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Velocity Direction Standard Deviation
                col += 1
                if np.isnan(self.meas.summary["Velocity Direction StdDev"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:4.2f}".format(
                                self.meas.summary["Velocity Direction StdDev"][row]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Q Top
                col += 1
                if np.isnan(self.meas.summary["Q Top"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:6.3f}".format(
                                self.meas.summary["Q Top"][row] * self.units["Q"]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Q Middle
                col += 1
                if np.isnan(self.meas.summary["Q Middle"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:6.3f}".format(
                                self.meas.summary["Q Middle"][row] * self.units["Q"]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Q Bottom
                col += 1
                if np.isnan(self.meas.summary["Q Bottom"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:6.3f}".format(
                                self.meas.summary["Q Bottom"][row] * self.units["Q"]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # BT % Invalid
                # col += 1
                # if np.isnan(self.meas.summary["BT % Invalid"][row]):
                #     tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                # else:
                #     tbl.setItem(
                #         row + 1,
                #         col,
                #         QtWidgets.QTableWidgetItem(
                #             "{:4.1f}".format(self.meas.summary["BT % Invalid"][row])
                #         ),
                #     )
                # tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Depth % Invalid
                col += 1
                if np.isnan(self.meas.summary["Depth % Invalid"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:4.1f}".format(self.meas.summary["Depth % Invalid"][row])
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # WT % Invalid
                col += 1
                if np.isnan(self.meas.summary["WT % Invalid"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:4.1f}".format(self.meas.summary["WT % Invalid"][row])
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Ice Area
                col += 1
                if np.isnan(self.meas.summary["Ice Area"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:8.3f}".format(
                                self.meas.summary["Ice Area"][row] * self.units["A"]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Slush Area
                col += 1
                if np.isnan(self.meas.summary["Slush Area"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:8.3f}".format(
                                self.meas.summary["Slush Area"][row] * self.units["A"]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Ice Thickness
                col += 1
                if np.isnan(self.meas.summary["Ice Thickness"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:4.2f}".format(
                                self.meas.summary["Ice Thickness"][row]
                                * self.units["L"]
                            )
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Average Voltage
                col += 1
                if np.isnan(self.meas.summary["Average Voltage"][row]):
                    tbl.setItem(row + 1, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row + 1,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:4.2f}".format(self.meas.summary["Average Voltage"][row])
                        ),
                    )
                tbl.item(row + 1, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Add measurement summaries
            # -------------------------

            # Station Number
            col = 0
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem("All"))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Location
            col += 1
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Start Time
            col += 1
            if len(start_time) < 1:
                tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            else:
                txt = start_time[0 : start_time.find(".")]
                tbl.setItem(0, col, QtWidgets.QTableWidgetItem(txt))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # End Time
            col += 1
            if len(end_time) < 1:
                tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            else:
                txt = end_time[0 : end_time.find(".")]
                tbl.setItem(0, col, QtWidgets.QTableWidgetItem(txt))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Depth std dev
            col += 1
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Velocity magnitude
            col += 1
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Velocity Magnitude CV
            col += 1
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Velocity Direction
            col += 1
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Velocity Direction Standard Deviation
            col += 1
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Q Top
            col += 1
            tbl.setItem(
                0,
                col,
                QtWidgets.QTableWidgetItem(
                    "{:6.3f}".format(
                        self.meas.summary["Q Top"]["Total"] * self.units["Q"]
                    )
                ),
            )
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Q Middle
            col += 1
            tbl.setItem(
                0,
                col,
                QtWidgets.QTableWidgetItem(
                    "{:6.3f}".format(
                        self.meas.summary["Q Middle"]["Total"] * self.units["Q"]
                    )
                ),
            )
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Q Bottom
            col += 1
            tbl.setItem(
                0,
                col,
                QtWidgets.QTableWidgetItem(
                    "{:6.3f}".format(
                        self.meas.summary["Q Bottom"]["Total"] * self.units["Q"]
                    )
                ),
            )
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # BT % Invalid
            # col += 1
            # tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            # tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Depth % Invalid
            col += 1
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # WT % Invalid
            col += 1
            tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Ice area
            col += 1
            tbl.setItem(
                0,
                col,
                QtWidgets.QTableWidgetItem(
                    "{:8.3f}".format(
                        self.meas.summary["Ice Area"]["Total"] * self.units["A"]
                    )
                ),
            )
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Slush area
            col += 1
            tbl.setItem(
                0,
                col,
                QtWidgets.QTableWidgetItem(
                    "{:8.3f}".format(
                        self.meas.summary["Slush Area"]["Total"] * self.units["A"]
                    )
                ),
            )
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Ice Thickness
            col += 1
            value = self.meas.summary["Ice Thickness"]["Mean"] * self.units["L"]
            if np.isnan(value):
                tbl.setItem(0, col, QtWidgets.QTableWidgetItem(""))
            else:
                tbl.setItem(0, col, QtWidgets.QTableWidgetItem("{:4.1f}".format(value)))
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Average Voltage
            col += 1
            tbl.setItem(
                0,
                col,
                QtWidgets.QTableWidgetItem(
                    "{:4.2f}".format(self.meas.summary["Average Voltage"]["Mean"])
                ),
            )
            tbl.item(0, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Hide unused columns
            if np.any(self.meas.summary["Condition"].str.contains("Ice")):
                tbl.setColumnHidden(14, False)
                tbl.setColumnHidden(15, False)
                tbl.setColumnHidden(16, False)

            else:
                tbl.setColumnHidden(14, True)
                tbl.setColumnHidden(15, True)
                tbl.setColumnHidden(16, True)

            # Bold Measurement row
            for col in range(ncols - 1):
                tbl.item(0, col).setFont(self.font_bold)

            # Highlight selected vertical
            tbl.item(self.main_row_idx, 0).setBackground(QtGui.QColor(191, 191, 191))
            tbl.scrollToItem(tbl.item(self.main_row_idx, 0))

            self.main_shade_mean_method(tbl)
            tbl.scrollToItem(tbl.item(self.main_row_idx, 0))

        else:
            tbl.setRowCount(0)

        tbl.resizeColumnsToContents()
        tbl.resizeRowsToContents()
        tbl.scrollToItem(tbl.item(0, 0))

    def main_tab_premeasurement(self):
        """Populate the fields on the main premeasurement tab."""

        # Turn off signals
        self.ed_site_name.blockSignals(True)
        self.ed_site_number.blockSignals(True)
        self.ed_persons.blockSignals(True)
        self.ed_measurement_number.blockSignals(True)
        self.ed_stage_start.blockSignals(True)
        self.ed_stage_end.blockSignals(True)
        self.ed_stage_meas.blockSignals(True)
        self.ed_beam_misalignment.blockSignals(True)
        self.combo_timezone.blockSignals(True)

        # Initialize and connect the station name and number fields
        font = self.label_site_name.font()
        font.setPointSize(11)
        self.label_site_name.setFont(font)
        self.label_site_number.setFont(font)

        # Site Name
        self.ed_site_name.setText(self.meas.station_name)
        if self.meas.qa.user["sta_name"]:
            self.label_site_name.setStyleSheet(
                "background: #ffcc00; "
                "font: 11pt MS Shell Dlg "
                "2;QToolTip{font: 11pt}"
            )
            self.label_site_name.setToolTip(self.tr("Missing site name."))
        else:
            self.label_site_name.setStyleSheet("background: white")
            self.label_site_name.setToolTip("")

        # Site Number
        try:
            self.ed_site_number.setText(self.meas.station_number)
        except TypeError:
            self.ed_site_number.setText("")
        if self.meas.qa.user["sta_number"]:
            self.label_site_number.setStyleSheet(
                "background: #ffcc00; "
                "font: 11pt MS Shell Dlg "
                "2;QToolTip{font: 11pt}"
            )
            self.label_site_number.setToolTip(self.tr("Missing site number."))
        else:
            self.label_site_number.setStyleSheet("background: white")
            self.label_site_number.setToolTip("")
            self.ed_site_number.setText(self.meas.station_number)

        # Measurement number
        self.ed_measurement_number.setText(self.meas.meas_number)

        # Persons
        self.ed_persons.setText(self.meas.persons)

        # Start stage
        self.label_stage_start.setText("Stage start " + self.units["label_L"] + ":")
        if np.isnan(self.meas.stage_start_m):
            self.ed_stage_start.setText("N/A")
        else:
            self.ed_stage_start.setText(
                "{:3.4f}".format(self.meas.stage_start_m * self.units["L"])
            )

        # End stage
        self.label_stage_end.setText("Stage end " + self.units["label_L"] + ":")
        if np.isnan(self.meas.stage_end_m):
            self.ed_stage_end.setText("N/A")
        else:
            self.ed_stage_end.setText(
                "{:3.4f}".format(self.meas.stage_end_m * self.units["L"])
            )

        # Measurement stage
        self.label_stage_meas.setText("Stage meas " + self.units["label_L"] + ":")
        if np.isnan(self.meas.stage_meas_m):
            self.ed_stage_meas.setText("N/A")
        else:
            self.ed_stage_meas.setText(
                "{:3.4f}".format(self.meas.stage_meas_m * self.units["L"])
            )

        # Beam misalignment
        self.ed_beam_misalignment.setDisabled(True)
        self.txt_beam_misalignment.setDisabled(True)
        for idx in range(len(self.meas.verticals_used_idx)):
            if self.meas.verticals[idx].velocity_method == "Fixed":
                self.ed_beam_misalignment.setEnabled(True)
                self.txt_beam_misalignment.setEnabled(True)
                self.ed_beam_misalignment.setText(
                    "{:4.1f}".format(self.meas.beam_3_misalignment_deg)
                )

        try:
            self.combo_timezone.setCurrentIndex(self.combo_timezone.findText(self.meas.time_zone, QtCore.Qt.MatchFixedString))
            if self.meas.qa.user["time_zone"]:
                self.label_time_zone.setStyleSheet("background-color: #ffcc00")
                self.label_time_zone.setToolTip(self.tr("Missing time zone."))
            else:
                self.label_time_zone.setStyleSheet("background-color: white")
                self.label_time_zone.setToolTip("")
        except (TypeError, KeyError):
            self.combo_timezone.setCurrentIndex(0)
            self.label_time_zone.setStyleSheet("background-color: white")
            self.label_time_zone.setToolTip("")

        self.main_table_premeasurement()

        # Turn on signals
        self.ed_site_name.blockSignals(False)
        self.ed_site_number.blockSignals(False)
        self.ed_persons.blockSignals(False)
        self.ed_measurement_number.blockSignals(False)
        self.ed_stage_start.blockSignals(False)
        self.ed_stage_end.blockSignals(False)
        self.ed_stage_meas.blockSignals(False)
        self.ed_beam_misalignment.blockSignals(False)
        self.combo_timezone.blockSignals(False)

    def main_table_premeasurement(self):
        """Populate the table on the main premeasurement tab."""

        # Setup table
        tbl = self.main_premeasurement_table
        tbl.clear()
        ncols = 4
        nrows = 4
        tbl.setRowCount(nrows)
        tbl.setColumnCount(ncols)
        tbl.horizontalHeader().hide()
        tbl.verticalHeader().hide()
        tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)

        if len(self.meas.verticals_used_idx) > 0:
            # ADCP Test
            tbl.setItem(0, 0, QtWidgets.QTableWidgetItem(self.tr("ADCP Test: ")))
            tbl.item(0, 0).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.item(0, 0).setFont(self.font_bold)
            # Determine is a system test was recorded
            if not self.meas.system_tst:
                tbl.setItem(0, 1, QtWidgets.QTableWidgetItem(self.tr("No")))
            else:
                tbl.setItem(0, 1, QtWidgets.QTableWidgetItem(self.tr("Yes")))
            tbl.item(0, 1).setFlags(QtCore.Qt.ItemIsEnabled)

            # Report test fails
            tbl.setItem(0, 2, QtWidgets.QTableWidgetItem(self.tr("ADCP Test Fails: ")))
            tbl.item(0, 2).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.item(0, 2).setFont(self.font_bold)
            num_tests_with_failure = 0
            for test in self.meas.system_tst:
                if hasattr(test, "result"):
                    if (
                        test.result["sysTest"]["n_failed"] is not None
                        and test.result["sysTest"]["n_failed"] > 0
                    ):
                        num_tests_with_failure += 1
            tbl.setItem(
                0,
                3,
                QtWidgets.QTableWidgetItem("{:2.0f}".format(num_tests_with_failure)),
            )
            tbl.item(0, 3).setFlags(QtCore.Qt.ItemIsEnabled)

            # Compass Calibration
            tbl.setItem(
                1, 0, QtWidgets.QTableWidgetItem(self.tr("Compass Calibration: "))
            )
            tbl.item(1, 0).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.item(1, 0).setFont(self.font_bold)
            if len(self.meas.compass_cal) == 0:
                tbl.setItem(1, 1, QtWidgets.QTableWidgetItem(self.tr("No")))
            else:
                tbl.setItem(1, 1, QtWidgets.QTableWidgetItem(self.tr("Yes")))
            tbl.item(1, 1).setFlags(QtCore.Qt.ItemIsEnabled)

            # Compass Evaluation
            tbl.setItem(
                1, 2, QtWidgets.QTableWidgetItem(self.tr("Compass Evaluation: "))
            )
            tbl.item(1, 2).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.item(1, 2).setFont(self.font_bold)
            if len(self.meas.compass_eval) == 0:
                tbl.setItem(1, 3, QtWidgets.QTableWidgetItem(self.tr("No")))
            else:
                if self.meas.compass_eval[-1].result["compass"]["error"] != "N/A":
                    tbl.setItem(
                        1,
                        3,
                        QtWidgets.QTableWidgetItem(
                            "{:3.1f}".format(
                                self.meas.compass_eval[-1].result["compass"]["error"]
                            )
                        ),
                    )
                else:
                    tbl.setItem(
                        1,
                        3,
                        QtWidgets.QTableWidgetItem(
                            str(self.meas.compass_eval[-1].result["compass"]["error"])
                        ),
                    )
            tbl.item(1, 3).setFlags(QtCore.Qt.ItemIsEnabled)

            # Independent Temperature
            tbl.setItem(
                2,
                0,
                QtWidgets.QTableWidgetItem(self.tr("Independent Temperature (C): ")),
            )
            tbl.item(2, 0).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.item(2, 0).setFont(self.font_bold)
            if type(self.meas.temp_chk["user"]) != float or np.isnan(
                self.meas.temp_chk["user"]
            ):
                tbl.setItem(2, 1, QtWidgets.QTableWidgetItem(self.tr("N/A")))
            else:
                tbl.setItem(
                    2,
                    1,
                    QtWidgets.QTableWidgetItem(
                        "{:4.1f}".format(self.meas.temp_chk["user"])
                    ),
                )
            tbl.item(2, 1).setFlags(QtCore.Qt.ItemIsEnabled)

            # ADCP temperature
            tbl.setItem(
                2, 2, QtWidgets.QTableWidgetItem(self.tr("ADCP Temperature (C): "))
            )
            tbl.item(2, 2).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.item(2, 2).setFont(self.font_bold)
            if type(self.meas.temp_chk["adcp"]) != float or np.isnan(
                self.meas.temp_chk["adcp"]
            ):
                avg_temp = Sensors.avg_temperature(self.meas.verticals)
                tbl.setItem(
                    2, 3, QtWidgets.QTableWidgetItem("{:4.1f}".format(avg_temp))
                )
            else:
                tbl.setItem(
                    2,
                    3,
                    QtWidgets.QTableWidgetItem(
                        "{:4.1f}".format(self.meas.temp_chk["adcp"])
                    ),
                )
            tbl.item(2, 3).setFlags(QtCore.Qt.ItemIsEnabled)

            # Magnetic variation
            tbl.setItem(
                3, 0, QtWidgets.QTableWidgetItem(self.tr("Magnetic Variation: "))
            )
            tbl.item(3, 0).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.item(3, 0).setFont(self.font_bold)
            magvar = []
            for vertical in self.meas.verticals:
                if vertical.use and vertical.data.sensors is not None:
                    magvar.append(
                        vertical.data.sensors.heading_deg.internal.mag_var_deg
                    )
            if len(np.unique(magvar)) > 1:
                tbl.setItem(3, 1, QtWidgets.QTableWidgetItem(self.tr("Varies")))
            else:
                tbl.setItem(
                    3, 1, QtWidgets.QTableWidgetItem("{:4.1f}".format(magvar[0]))
                )
            tbl.item(3, 1).setFlags(QtCore.Qt.ItemIsEnabled)

            # Heading offset
            tbl.setItem(3, 2, QtWidgets.QTableWidgetItem(self.tr("Heading Offset: ")))
            tbl.item(3, 2).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.item(3, 2).setFont(self.font_bold)
            hoffset = []
            for vertical in self.meas.verticals:
                if (
                    vertical.data.sensors is not None
                    and vertical.data.sensors.heading_deg.external is None
                ):
                    tbl.setItem(3, 3, QtWidgets.QTableWidgetItem(self.tr("N/A")))
                if vertical.use and vertical.data.sensors is not None:
                    hoffset.append(
                        vertical.data.sensors.heading_deg.internal.align_correction_deg
                    )
            if len(np.unique(hoffset)) > 1:
                tbl.setItem(3, 3, QtWidgets.QTableWidgetItem(self.tr("Varies")))
            else:
                tbl.setItem(
                    3, 3, QtWidgets.QTableWidgetItem("{:4.1f}".format(hoffset[0]))
                )
            tbl.item(3, 3).setFlags(QtCore.Qt.ItemIsEnabled)

            tbl.resizeColumnsToContents()
            tbl.resizeRowsToContents()

    def update_time_zone(self, text):
        """Records the time zone entered by the user. Value not used in any compuations"""

        self.meas.change_timezone(text)
        self.main_tab_messages()
        self.main_tab_premeasurement()

    def main_messages(self):
        """Displays messages associated with the transects in Messages tab.
        """

        if self.meas is not None:
            qa_check_keys = ["verticals", "user", "main", "rating"]
            self.messages_table(self.table_main_messages, qa_check_keys)

            self.update_tab_icons()

    def main_update_site_name(self):
        """Sets the station name to the name entered by the user."""

        self.ed_site_name.blockSignals(True)
        self.meas.station_name = self.ed_site_name.text()
        if len(self.meas.station_name) > 0:
            self.label_site_name.setStyleSheet("background: white")
        self.meas.qa.user_qa(self.meas)
        self.main_tab_messages()
        self.main_tab_premeasurement()
        self.main_premeasurement_tab.setFocus()
        self.ed_site_name.blockSignals(False)

    def main_update_site_number(self):
        """Sets the station number to the number entered by the user."""

        self.ed_site_number.blockSignals(True)
        self.meas.station_number = self.ed_site_number.text()
        if len(self.meas.station_number) > 0:
            self.label_site_number.setStyleSheet("background: white")
        self.meas.qa.user_qa(self.meas)
        self.main_tab_messages()
        self.main_tab_premeasurement()
        self.main_premeasurement_tab.setFocus()
        self.ed_site_number.blockSignals(False)

    def main_update_persons(self):
        """Sets the person(s) to the information entered by the user."""

        self.ed_persons.blockSignals(True)
        self.meas.persons = self.ed_persons.text()
        self.main_tab_premeasurement()
        self.main_premeasurement_tab.setFocus()
        self.ed_persons.blockSignals(False)

    def main_update_meas_number(self):
        """Sets the measurement number to the information entered by the user."""

        self.ed_measurement_number.blockSignals(True)
        self.meas.meas_number = self.ed_measurement_number.text()
        self.main_tab_premeasurement()
        self.main_premeasurement_tab.setFocus()
        self.ed_measurement_number.blockSignals(False)

    def main_update_stage_start(self):
        """Sets the start stage to the information entered by the user and
        updates the measurement stage.
        """

        self.ed_stage_start.blockSignals(True)
        stage = self.check_numeric_input(self.ed_stage_start, block=False)
        if stage is not None:
            self.meas.stage_start_m = stage / self.units["L"]

            # If there is no end stage assume stage is unchanging and measurement
            # stage is same as start stage, if not, average start and end stages
            # to obtain measurement stage
            if np.isnan(self.meas.stage_end_m):
                self.meas.stage_meas_m = self.meas.stage_start_m
            else:
                self.meas.stage_meas_m = (
                    self.meas.stage_start_m + self.meas.stage_end_m
                ) / 2.0
        else:
            self.meas.stage_start_m = np.nan

        # Update GUI
        self.main_tab_premeasurement()
        self.main_premeasurement_tab.setFocus()
        self.ed_stage_start.blockSignals(False)

    def main_update_stage_end(self):
        """Sets the end stage to the information entered by the user and
        updates the measurement stage.
        """

        self.ed_stage_end.blockSignals(True)
        stage = self.check_numeric_input(self.ed_stage_end, block=False)
        if stage is not None:
            self.meas.stage_end_m = stage / self.units["L"]
            # If there is no start stage assume end stage applies to entire
            # measurement, otherwise, average start and end stage to obtain
            # the stage for the measurement.
            if np.isnan(self.meas.stage_start_m):
                self.meas.stage_meas_m = self.meas.stage_end_m
            else:
                self.meas.stage_meas_m = (
                    self.meas.stage_start_m + self.meas.stage_end_m
                ) / 2.0
        else:
            self.meas.stage_end_m = np.nan

        # Update GUI
        self.main_tab_premeasurement()
        self.main_premeasurement_tab.setFocus()
        self.ed_stage_end.blockSignals(False)

    def main_update_stage_meas(self):
        """Sets the measurement stage to the information entered by the user."""

        self.ed_stage_meas.blockSignals(True)
        stage = self.check_numeric_input(self.ed_stage_meas, block=False)
        if stage is not None:
            self.meas.stage_meas_m = stage / self.units["L"]
        else:
            self.meas.stage_meas_m = np.nan

        # Update GUI
        self.main_tab_premeasurement()
        self.main_premeasurement_tab.setFocus()
        self.ed_stage_meas.blockSignals(False)

    def main_update_beam_misalignment(self):
        """Sets the general beam 3 misalignment to a new value entered by
        the user and calls the method to change and
        recompute the profiles and discharge.
        """

        self.ed_beam_misalignment.blockSignals(True)
        beam_misalignment = self.check_numeric_input(
            self.ed_beam_misalignment, block=False
        )
        if beam_misalignment is not None:
            self.meas.change_beam_3_misalignment(beam_misalignment)
            # Update GUI
            self.change = True
            self.tab_manager()

        self.main_premeasurement_tab.setFocus()
        self.ed_beam_misalignment.blockSignals(False)

    def main_table_settings(self):
        """Populates the main tab settings table."""

        tbl = self.main_settings_table
        tbl.clear()

        # Define headers
        summary_header = [
            self.tr("Station" + "\n" + " Number"),
            self.tr("Location" + "\n") + self.units["label_L"],
            self.tr("Condition"),
            self.tr("Velocity" + "\n" + "Method"),
            self.tr("Velocity" + "\n" + "Reference"),
            self.tr("Depth" + "\n" + "Source"),
            self.tr("Excluded" + "\n" + "Top"),
            self.tr("Excluded" + "\n" + "Top" + "\n" + "Type"),
            self.tr("Excluded" + "\n" + "Bottom"),
            self.tr("Excluded" + "\n" + "Bottom" + "\n" + "Type"),
            self.tr("Top" + "\n" + "Method" + "\n"),
            self.tr("Ice" + "\n" + "Exponent"),
            self.tr("Bottom" + "\n" + "Method"),
            self.tr("Bottom" + "\n" + "Exponent"),
        ]

        # Setup table
        ncols = len(summary_header)
        nrows = len(self.meas.verticals_used_idx)
        tbl.setRowCount(nrows)
        tbl.setColumnCount(ncols)
        tbl.setFont(self.font_normal)
        tbl.setHorizontalHeaderLabels(summary_header)
        tbl.horizontalHeader().setFont(self.font_bold)
        tbl.verticalHeader().hide()
        tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)

        # Populate table
        if nrows > 0:
            row = -1
            for idx in range(len(self.meas.summary.index) - 2):
                # Populate verticals
                # ------------------
                if np.modf(self.meas.summary["Station Number"][idx])[0] == 0:
                    row += 1
                    # Station Number
                    col = 0
                    if np.isnan(self.meas.summary["Station Number"][idx]):
                        tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                    elif self.meas.summary["Station Number"][idx] == np.floor(
                        self.meas.summary["Station Number"][idx]
                    ):
                        tbl.setItem(
                            row,
                            col,
                            QtWidgets.QTableWidgetItem(
                                "{:3.0f}".format(
                                    self.meas.summary["Station Number"][idx]
                                )
                            ),
                        )
                    else:
                        tbl.setItem(
                            row,
                            col,
                            QtWidgets.QTableWidgetItem(
                                "{:3.1f}".format(
                                    self.meas.summary["Station Number"][idx]
                                )
                            ),
                        )
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                    # Location
                    col += 1
                    if np.isnan(self.meas.summary["Location"][idx]):
                        tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                    else:
                        tbl.setItem(
                            row,
                            col,
                            QtWidgets.QTableWidgetItem(
                                "{:6.2f}".format(self.meas.summary["Location"][idx] * self.units["L"])
                            ),
                        )
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                    # Water surface condition
                    col += 1
                    if len(self.meas.summary["Condition"][idx]) < 1:
                        tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                    else:
                        tbl.setItem(
                            row,
                            col,
                            QtWidgets.QTableWidgetItem(
                                self.meas.summary["Condition"][idx]
                            ),
                        )
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                    # Velocity Method
                    col += 1
                    if len(self.meas.summary["Velocity Method"][idx]) < 1:
                        tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                    else:
                        tbl.setItem(
                            row,
                            col,
                            QtWidgets.QTableWidgetItem(
                                self.meas.summary["Velocity Method"][idx]
                            ),
                        )
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                    # Velocity Reference
                    col += 1
                    if len(self.meas.summary["Velocity Reference"][idx]) < 1:
                        tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                    else:
                        tbl.setItem(
                            row,
                            col,
                            QtWidgets.QTableWidgetItem(
                                self.meas.summary["Velocity Reference"][idx]
                            ),
                        )
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                    # Depth Source
                    col += 1
                    if len(self.meas.summary["Depth Source"][idx]) < 1:
                        tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                    else:
                        tbl.setItem(
                            row,
                            col,
                            QtWidgets.QTableWidgetItem(
                                self.meas.summary["Depth Source"][idx]
                            ),
                        )
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                    # Excluded Top
                    col += 1
                    if np.isnan(self.meas.summary["Excluded Top"][idx]):
                        tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                    else:
                        tbl.setItem(
                            row,
                            col,
                            QtWidgets.QTableWidgetItem(
                                "{:4.2f}".format(self.meas.summary["Excluded Top"][idx])
                            ),
                        )
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                    # Excluded Top Type
                    col += 1
                    if len(self.meas.summary["Excluded Top Type"][idx]) < 1:
                        tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                    else:
                        tbl.setItem(
                            row,
                            col,
                            QtWidgets.QTableWidgetItem(
                                self.meas.summary["Excluded Top Type"][idx]
                            ),
                        )
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                    # Excluded Bottom
                    col += 1
                    if np.isnan(self.meas.summary["Excluded Bottom"][idx]):
                        tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                    else:
                        tbl.setItem(
                            row,
                            col,
                            QtWidgets.QTableWidgetItem(
                                "{:4.2f}".format(
                                    self.meas.summary["Excluded Bottom"][idx]
                                )
                            ),
                        )
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                    # Excluded Bottom Type
                    col += 1
                    if len(self.meas.summary["Excluded Bottom Type"][idx]) < 1:
                        tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                    else:
                        tbl.setItem(
                            row,
                            col,
                            QtWidgets.QTableWidgetItem(
                                self.meas.summary["Excluded Bottom Type"][idx]
                            ),
                        )
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                    # Top Method
                    col += 1
                    if len(self.meas.summary["Top Method"][idx]) < 1:
                        tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                    else:
                        tbl.setItem(
                            row,
                            col,
                            QtWidgets.QTableWidgetItem(
                                self.meas.summary["Top Method"][idx]
                            ),
                        )
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                    # Ice Exponent
                    col += 1
                    if np.isnan(self.meas.summary["Ice Exponent"][idx]):
                        tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                    else:
                        tbl.setItem(
                            row,
                            col,
                            QtWidgets.QTableWidgetItem(
                                "{:1.4f}".format(self.meas.summary["Ice Exponent"][idx])
                            ),
                        )
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                    # Bottom Method
                    col += 1
                    if len(self.meas.summary["Bottom Method"][idx]) < 1:
                        tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                    else:
                        tbl.setItem(
                            row,
                            col,
                            QtWidgets.QTableWidgetItem(
                                self.meas.summary["Bottom Method"][idx]
                            ),
                        )
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                    # Bottom Exponent
                    col += 1
                    if np.isnan(self.meas.summary["Bottom Exponent"][idx]):
                        tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                    else:
                        tbl.setItem(
                            row,
                            col,
                            QtWidgets.QTableWidgetItem(
                                "{:1.4f}".format(
                                    self.meas.summary["Bottom Exponent"][idx]
                                )
                            ),
                        )
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Hide unused columns
            if np.all(np.isnan(self.meas.summary["Depth to Ice Bottom"].to_numpy())):
                tbl.setColumnHidden(11, True)
            else:
                tbl.setColumnHidden(11, False)

            # Highlight selected vertical
            settings_row_idx = np.modf(
                self.meas.summary["Station Number"][self.main_row_idx - 1]
            )[1]
            tbl.item(int(settings_row_idx) - 1, 0).setBackground(
                QtGui.QColor(191, 191, 191)
            )
            tbl.scrollToItem(tbl.item(self.main_row_idx, 0))

        else:
            tbl.setRowCount(0)

        tbl.resizeColumnsToContents()
        tbl.resizeRowsToContents()
        tbl.scrollToItem(tbl.item(0, 0))

    def main_table_adcp(self):
        """Display ADCP table."""

        # Setup table
        tbl = self.main_inst_table
        tbl.clear()
        tbl.setRowCount(4)
        tbl.setColumnCount(4)
        tbl.horizontalHeader().hide()
        tbl.verticalHeader().hide()
        tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)

        if len(self.meas.verticals_used_idx) > 0:
            # Serial number
            tbl.setItem(0, 0, QtWidgets.QTableWidgetItem(self.tr("Serial Number: ")))
            tbl.item(0, 0).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.item(0, 0).setFont(self.font_bold)
            # Get serial number from 1st checked transect
            tbl.setItem(
                0,
                1,
                QtWidgets.QTableWidgetItem(
                    self.meas.verticals[self.first_idx].data.inst.serial_num
                ),
            )
            tbl.item(0, 1).setFlags(QtCore.Qt.ItemIsEnabled)

            # Manufacturer
            tbl.setItem(0, 2, QtWidgets.QTableWidgetItem(self.tr("Manufacturer: ")))
            tbl.item(0, 2).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.item(0, 2).setFont(self.font_bold)
            tbl.setItem(
                0,
                3,
                QtWidgets.QTableWidgetItem(
                    self.meas.verticals[self.first_idx].data.inst.manufacturer
                ),
            )
            tbl.item(0, 3).setFlags(QtCore.Qt.ItemIsEnabled)

            # Model
            tbl.setItem(1, 0, QtWidgets.QTableWidgetItem(self.tr("Model: ")))
            tbl.item(1, 0).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.item(1, 0).setFont(self.font_bold)
            tbl.setItem(
                1,
                1,
                QtWidgets.QTableWidgetItem(
                    self.meas.verticals[self.first_idx].data.inst.model
                ),
            )
            tbl.item(1, 1).setFlags(QtCore.Qt.ItemIsEnabled)

            # Firmware
            tbl.setItem(1, 2, QtWidgets.QTableWidgetItem(self.tr("Firmware: ")))
            tbl.item(1, 2).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.item(1, 2).setFont(self.font_bold)
            if type(self.meas.verticals[self.first_idx].data.inst.firmware) == str:
                firmware = self.meas.verticals[
                    self.meas.verticals_used_idx[self.first_idx]
                ].data.inst.firmware
            else:
                firmware = str(self.meas.verticals[self.first_idx].data.inst.firmware)
            tbl.setItem(1, 3, QtWidgets.QTableWidgetItem(firmware))
            tbl.item(1, 3).setFlags(QtCore.Qt.ItemIsEnabled)

            # Frequency
            tbl.setItem(2, 0, QtWidgets.QTableWidgetItem(self.tr("Frequency (kHz): ")))
            tbl.item(2, 0).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.item(2, 0).setFont(self.font_bold)
            if self.meas.verticals[self.first_idx].data.inst.manufacturer == "SonTek":
                item = "Variable"
            elif self.meas.verticals[self.first_idx].data.inst.manufacturer == "Nortek":
                item = "{:4.0f}".format(
                    self.meas.verticals[self.first_idx].data.inst.frequency_khz[0]
                )
            else:
                item = "{:4.0f}".format(
                    self.meas.verticals[self.first_idx].data.inst.frequency_khz
                )
            tbl.setItem(2, 1, QtWidgets.QTableWidgetItem(item))
            tbl.item(2, 1).setFlags(QtCore.Qt.ItemIsEnabled)

            # Depth cell size
            tbl.setItem(
                2, 2, QtWidgets.QTableWidgetItem(self.tr("Depth Cell Size (cm): "))
            )
            tbl.item(2, 2).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.item(2, 2).setFont(self.font_bold)
            # Check for and handle multiple cell sizes
            cell_sizes = np.array([])
            for n in range(len(self.meas.verticals_used_idx)):
                data = self.meas.verticals[n].data
                if data.depths is not None:
                    cell_sizes = np.append(
                        cell_sizes,
                        np.unique(data.depths.bt_depths.depth_cell_size_m)[:],
                    )
            max_cell_size = np.nanmax(cell_sizes) * 100
            min_cell_size = np.nanmin(cell_sizes) * 100
            if max_cell_size - min_cell_size < 1:
                size = "{:3.0f}".format(max_cell_size)
            else:
                size = "{:3.0f} - {:3.0f}".format(min_cell_size, max_cell_size)
            tbl.setItem(2, 3, QtWidgets.QTableWidgetItem(size))
            tbl.item(2, 3).setFlags(QtCore.Qt.ItemIsEnabled)

            # Water mode
            tbl.setItem(3, 0, QtWidgets.QTableWidgetItem(self.tr("Water Mode: ")))
            tbl.item(3, 0).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.item(3, 0).setFont(self.font_bold)
            if self.meas.verticals[self.first_idx].data.inst.manufacturer == "SonTek":
                item = "Variable"
            elif self.meas.verticals[self.first_idx].data.inst.manufacturer == "Nortek":
                item = "Variable"
            else:
                item = "{:2.0f}".format(
                    self.meas.verticals[self.first_idx].data.w_vel.water_mode
                )
            tbl.setItem(3, 1, QtWidgets.QTableWidgetItem(item))
            tbl.item(3, 1).setFlags(QtCore.Qt.ItemIsEnabled)

            # Bottom mode
            tbl.setItem(3, 2, QtWidgets.QTableWidgetItem(self.tr("Bottom Mode: ")))
            tbl.item(3, 2).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.item(3, 2).setFont(self.font_bold)
            if self.meas.verticals[self.first_idx].data.inst.manufacturer == "SonTek":
                item = "Variable"
            elif self.meas.verticals[self.first_idx].data.inst.manufacturer == "Nortek":
                item = "Variable"
            else:
                item = "{:2.0f}".format(
                    self.meas.verticals[self.first_idx].data.boat_vel.bt_vel.bottom_mode
                )
            tbl.setItem(3, 3, QtWidgets.QTableWidgetItem(item))
            tbl.item(3, 3).setFlags(QtCore.Qt.ItemIsEnabled)

            tbl.resizeColumnsToContents()
            tbl.resizeRowsToContents()

    def main_select_vertical_settings_table(self, row):
        """Determine vertical based on row clicked."""

        if self.meas.discharge_method == "Mid":
            self.main_select_vertical(row=row + 1)
        else:
            self.main_select_vertical(row=(row * 2) + 1)

    def main_select_vertical(self, row):
        """Update plots based on transect selected in main_summary_table.

        Parameters
        ----------
        row: int
            Row number selected.
        """

        # Vertical column was selected
        if (
            1 < row < self.main_summary_table.rowCount() - 1
            and np.modf(self.meas.summary["Station Number"][row - 1])[0] == 0
        ):
            with self.wait_cursor():
                # Set all files to normal font
                nrows = self.main_summary_table.rowCount()
                for nrow in range(1, nrows):
                    self.main_summary_table.item(nrow, 0).setBackground(
                        QtGui.QColor(255, 255, 255)
                    )
                    self.main_details_table.item(nrow, 0).setBackground(
                        QtGui.QColor(255, 255, 255)
                    )
                    if nrow < self.main_settings_table.rowCount():
                        self.main_settings_table.item(nrow, 0).setBackground(
                            QtGui.QColor(255, 255, 255)
                        )

                # Set selected file to bold font
                self.main_summary_table.item(row, 0).setBackground(
                    QtGui.QColor(191, 191, 191)
                )
                self.main_details_table.item(row, 0).setBackground(
                    QtGui.QColor(191, 191, 191)
                )
                settings_row_idx = np.modf(
                    self.meas.summary["Station Number"][row - 1]
                )[1]
                self.main_settings_table.item(settings_row_idx - 1, 0).setBackground(
                    QtGui.QColor(191, 191, 191)
                )
                self.main_row_idx = row

                # Determine transect selected
                if self.meas.discharge_method == "Mean":
                    self.vertical_idx = self.meas.verticals_used_idx[
                        int(self.meas.summary["Station Number"][self.main_row_idx] - 1)
                    ]
                else:
                    self.vertical_idx = self.meas.verticals_used_idx[
                        self.main_row_idx - 1
                    ]
                self.stations_row = np.where(
                    self.meas.verticals_sorted_idx == self.vertical_idx
                )[0][0]

                # self.clear_zphd()

                self.main_summary_table.scrollToItem(
                    self.main_summary_table.item(row, 0)
                )
                self.main_details_table.scrollToItem(
                    self.main_details_table.item(row, 0)
                )
                self.main_settings_table.scrollToItem(
                    self.main_details_table.item(settings_row_idx - 1, 0)
                )

                # Update graphics
                self.main_graphics_extrap()
                # self.main_graphics_rssi()
                self.main_graphics_uncertainty()
                self.main_graphics_percent_discharge()
                self.main_velocity_fig.update_contour_selection(
                    self.meas, self.vertical_idx
                )
                self.main_velocity_canvas.draw()

                # Setup list for use by graphics controls
                self.canvases = [
                    self.main_velocity_canvas,
                    self.main_extrap_canvas,
                    self.main_percent_discharge_canvas,
                    self.main_uncertainty_canvas,
                ]
                self.figs = [
                    self.main_velocity_fig,
                    self.main_extrap_fig,
                    self.main_percent_discharge_fig,
                    self.main_uncertainty_fig,
                ]
                self.toolbars = [
                    self.main_velocity_toolbar,
                    self.main_extrap_toolbar,
                    self.main_percent_discharge_toolbar,
                    self.main_uncertainty_toolbar,
                ]
                self.fig_calls = [
                    self.main_graphics_velocity,
                    self.main_graphics_extrap,
                    self.main_graphics_percent_discharge,
                    self.main_graphics_uncertainty,
                ]
                self.ui_parents = [i.parent() for i in self.canvases]
                self.figs_menu_connection()

        self.top_tab.setFocus()

    def main_shade_mean_method(self, tbl):
        """Shades the selected row."""

        for row in range(1, tbl.rowCount()):
            for col in range(tbl.columnCount()):
                if np.modf(self.meas.summary["Station Number"][row - 1])[0] != 0:
                    tbl.item(row, col).setBackground(QtGui.QColor(230, 255, 255))

    def main_graphics_percent_discharge(self):
        """Creates bar graph of percent discharge for each vertical or
        subsection.
        """

        if self.main_percent_discharge_canvas is None:
            # Create the canvas
            self.main_percent_discharge_canvas = MplCanvas(
                parent=self.main_percent_q_graph, width=7, height=4, dpi=80
            )
            # Assign layout to widget to allow auto scaling
            layout = QtWidgets.QVBoxLayout(self.main_percent_q_graph)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(1, 1, 1, 1)
            # Add the canvas
            layout.addWidget(self.main_percent_discharge_canvas)
            # Initialize hidden toolbar for use by graphics controls
            self.main_percent_discharge_toolbar = NavigationToolbar(
                self.main_percent_discharge_canvas, self
            )
            self.main_percent_discharge_toolbar.hide()

        # Initialize the shiptrack figure and assign to the canvas
        self.main_percent_discharge_fig = Graphics(
            canvas=self.main_percent_discharge_canvas
        )
        # Create the figure with the specified data
        self.main_percent_discharge_fig.percent_q_bar(
            meas=self.meas,
            units=self.units,
            color_dict=self.plt_color_dict,
            selected=self.vertical_idx,
            agency_options=self.agency_options,
        )

        # Initialize the ability to click on graph to select vertical
        self.main_percent_discharge_fig.select_vertical(parent=self)

        # Draw canvas
        self.main_percent_discharge_canvas.draw()

    def main_graphics_velocity(self):
        """Creates the velocity quiver and color contour graphs."""

        if self.main_velocity_canvas is None:
            # Create the canvas
            self.main_velocity_canvas = MplCanvas(
                parent=self.main_velocity_graphs, width=7, height=4, dpi=80
            )
            # Assign layout to widget to allow auto scaling
            layout = QtWidgets.QVBoxLayout(self.main_velocity_graphs)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(1, 1, 1, 1)
            # Add the canvas
            layout.addWidget(self.main_velocity_canvas)
            # Initialize hidden toolbar for use by graphics controls
            self.main_velocity_toolbar = NavigationToolbar(
                self.main_velocity_canvas, self
            )
            self.main_velocity_toolbar.hide()

        # Initialize the shiptrack figure and assign to the canvas
        self.main_velocity_fig = Graphics(canvas=self.main_velocity_canvas)
        # Create the figure with the specified data
        self.main_velocity_fig.main_vel_graphs(
            meas=self.meas,
            color_map=self.color_map,
            selected=self.vertical_idx,
            units=self.units,
        )

        # Initialize the ability to click on graph to select vertical
        self.main_velocity_fig.select_vertical(parent=self)

        # Draw canvas
        self.main_velocity_canvas.draw()

    def main_graphics_extrap(self):
        """Creates the extrapolation graph."""

        if self.main_extrap_canvas is None:
            # Create the canvas
            self.main_extrap_canvas = MplCanvas(
                parent=self.main_extrap_graph, width=4, height=4, dpi=80
            )
            # Assign layout to widget to allow auto scaling
            layout = QtWidgets.QVBoxLayout(self.main_extrap_graph)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(1, 1, 1, 1)
            # Add the canvas
            layout.addWidget(self.main_extrap_canvas)
            # Initialize hidden toolbar for use by graphics controls
            self.main_extrap_toolbar = NavigationToolbar(self.main_extrap_canvas, self)
            self.main_extrap_toolbar.hide()

        # Initialize the shiptrack figure and assign to the canvas
        self.main_extrap_fig = Graphics(canvas=self.main_extrap_canvas)
        # Create the figure with the specified data
        if self.meas.verticals[self.vertical_idx].sampling_duration_sec > 0:
            self.main_extrap_fig.main_extrap_graph(
                meas=self.meas, units=self.units, idx=self.vertical_idx
            )
        else:
            self.main_extrap_fig.fig.clear()

        # Draw canvas
        self.main_extrap_canvas.draw()

    def main_graphics_uncertainty(self):
        """Creates the uncertainty graph."""

        if self.main_uncertainty_canvas is None:
            # Create the canvas
            self.main_uncertainty_canvas = MplCanvas(
                parent=self.main_uncertainty_graph, width=4, height=4, dpi=80
            )
            # Assign layout to widget to allow auto scaling
            layout = QtWidgets.QVBoxLayout(self.main_uncertainty_graph)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(1, 1, 1, 1)
            # Add the canvas
            layout.addWidget(self.main_uncertainty_canvas)
            # Initialize hidden toolbar for use by graphics controls
            self.main_uncertainty_toolbar = NavigationToolbar(
                self.main_uncertainty_canvas, self
            )
            self.main_uncertainty_toolbar.hide()

        # Initialize the shiptrack figure and assign to the canvas
        self.main_uncertainty_fig = Graphics(canvas=self.main_uncertainty_canvas)
        # Create the figure with the specified data
        if self.meas.uncertainty is not None:
            self.main_uncertainty_fig.uncertainty_plot(self.meas.uncertainty)
        else:
            self.main_uncertainty_fig.fig.clear()

        # Draw canvas
        self.main_uncertainty_canvas.draw()

    def main_tab_comments(self):
        """Display comments in comments tab."""

        self.main_comment_textedit.clear()
        if self.meas is not None:
            self.main_comment_textedit.moveCursor(QtGui.QTextCursor.Start)
            for comment in self.meas.comments:
                # Display each comment on a new line
                self.main_comment_textedit.textCursor().insertText(comment)
                self.main_comment_textedit.moveCursor(QtGui.QTextCursor.End)
                self.main_comment_textedit.textCursor().insertBlock()

    def main_tab_messages(self):
        """Update messages tab."""

        qa_check_keys = ["bt_vel", "compass", "depths", "edges", "extrapolation",
                         "stations", "verticals", "system_tst", "temperature", 
                         "user", "w_vel", "main", "rating"]
        if not self.display_bt_tab():
            qa_check_keys.remove("bt_vel")
        self.messages_table(self.main_message_table, qa_check_keys)
            
    def display_bt_tab(self):
        """Controls if the BT tab is active or not."""

        self.top_tab.blockSignals(True)
        tab_idx = self.top_tab.indexOf(
            self.top_tab.findChild(QtWidgets.QWidget, "bt_tab")
        )
        if not self.bt_always_active:
            bt_display = False
            for idx in self.meas.verticals_used_idx:
                if self.meas.verticals[idx].velocity_reference == "bt_vel" or self.meas.verticals[idx].velocity_reference == "BT":
                    bt_display = True
                    self.top_tab.setTabEnabled(tab_idx, True)
                    break
            if not bt_display:
                self.top_tab.setTabEnabled(tab_idx, False)
                self.top_tab.tabBar().setTabTextColor(
                    tab_idx, QtGui.QColor(191, 191, 191)
                )
                self.top_tab.setTabIcon(tab_idx, QtGui.QIcon())
        else:
            bt_display = True
            self.top_tab.setTabEnabled(tab_idx, True)
        self.top_tab.blockSignals(False)
        return bt_display

    # System test tab
    # ===============
    def systest_tab_update(self, idx_systest=0):
        """Initialize and display data in the systems tab.

        Parameters
        ----------
        idx_systest: int
            Identifies the system test to display in the text box.
        """

        # Setup table
        tbl = self.table_systest
        nrows = len(self.meas.system_tst)
        tbl.setRowCount(nrows)
        tbl.setColumnCount(4)
        header_text = [
            self.tr("Date/Time"),
            self.tr("No. Tests"),
            self.tr("No. Failed"),
            self.tr("PT3"),
        ]
        tbl.setHorizontalHeaderLabels(header_text)
        tbl.horizontalHeader().setFont(self.font_bold)
        tbl.verticalHeader().hide()
        tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)
        tbl.setStyleSheet("QToolTip{font: 12pt}")
        self.display_systest.clear()

        # Add system tests
        if nrows > 0:
            for row, test in enumerate(self.meas.system_tst):
                # Test identifier
                col = 0
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(test.time_stamp))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Number of subtests run
                col += 1
                if test.result["sysTest"]["n_tests"] is None:
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem("N/A"))
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:2.0f}".format(test.result["sysTest"]["n_tests"])
                        ),
                    )
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Number of subtests failed
                col += 1
                if test.result["sysTest"]["n_failed"] is None:
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem("N/A"))
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))
                    tbl.item(row, col).setToolTip(self.tr("No system test."))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:2.0f}".format(test.result["sysTest"]["n_failed"])
                        ),
                    )
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
                    if test.result["sysTest"]["n_failed"] > 0:
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                        tbl.item(row, col).setToolTip(
                            self.tr(
                                "One or more system test sets have at least one "
                                "test that failed"
                            )
                        )
                    else:
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))
                        tbl.item(row, col).setToolTip(
                            self.tr(
                                "All system test sets have at least one test "
                                "that failed"
                            )
                        )

                # Status of PT3 tests
                col += 1
                if "pt3" in test.result:
                    if "hard_limit" in test.result["pt3"]:
                        if "high_wide" in test.result["pt3"]["hard_limit"]:
                            if (
                                "corr_table"
                                in test.result["pt3"]["hard_limit"]["high_wide"]
                            ):
                                corr_table = test.result["pt3"]["hard_limit"][
                                    "high_wide"
                                ]["corr_table"]
                                if len(corr_table) > 0:
                                    # All lags past lag 2 should be less
                                    # than 50% of lag 0
                                    qa_threshold = corr_table[0, :] * 0.5
                                    all_lag_check = np.greater(
                                        corr_table[3::, :], qa_threshold
                                    )

                                    # Lag 7 should be less than 25% of lag 0
                                    lag_7_check = np.greater(
                                        corr_table[7, :], corr_table[0, :] * 0.25
                                    )

                                    # If either condition is met for any
                                    # beam the test fails
                                    if (
                                        np.sum(np.sum(all_lag_check))
                                        + np.sum(lag_7_check)
                                        > 1
                                    ):
                                        tbl.setItem(
                                            row,
                                            col,
                                            QtWidgets.QTableWidgetItem("Failed"),
                                        )
                                        tbl.item(row, col).setBackground(
                                            QtGui.QColor(255, 204, 0)
                                        )
                                        tbl.item(row, col).setToolTip(
                                            self.tr(
                                                "One or more PT3 tests in the "
                                                "system test indicate potential "
                                                "EMI"
                                            )
                                        )
                                    else:
                                        tbl.setItem(
                                            row, col, QtWidgets.QTableWidgetItem("Pass")
                                        )
                                        tbl.item(row, col).setBackground(
                                            QtGui.QColor(255, 255, 255)
                                        )

                else:
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem("N/A"))
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))

                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Display selected test
            tbl.item(idx_systest, 0).setFont(self.font_bold)
            self.display_systest.clear()
            self.display_systest.textCursor().insertText(
                self.meas.system_tst[idx_systest].data
            )

        tbl.resizeColumnsToContents()
        tbl.resizeRowsToContents()

        if not self.systest_initialized:
            tbl.cellClicked.connect(self.systest_select_test)
            self.systest_initialized = True

        self.systest_comments_messages()

    def systest_comments_messages(self):
        """Display system test comments and messages in system tab."""

        # Clear current contents
        self.display_systest_comments.clear()

        if self.meas is not None:
            # Comments
            self.display_systest_comments.moveCursor(QtGui.QTextCursor.Start)
            for comment in self.meas.comments:
                # Display each comment on a new line
                self.display_systest_comments.textCursor().insertText(comment)
                self.display_systest_comments.moveCursor(QtGui.QTextCursor.End)
                self.display_systest_comments.textCursor().insertBlock()

            # Messages
            self.messages_table(self.table_systest_messages, ["system_tst"])

            self.update_tab_icons()

    def systest_select_test(self, row, column):
        """Displays selected system test in text box.

        Parameters
        ----------
        row: int
            row in table clicked by user
        column: int
            column in table clicked by user
        """

        if column == 0:
            with self.wait_cursor():
                # Set all files to normal font
                nrows = len(self.meas.system_tst)
                for nrow in range(nrows):
                    self.table_systest.item(nrow, 0).setFont(self.font_normal)

                # Set selected file to bold font
                self.table_systest.item(row, 0).setFont(self.font_bold)

                # Update display
                self.systest_tab_update(idx_systest=row)

    # Compass tab
    # ===========
    def compass_tab(self, old_discharge=None):
        """Initialize, setup settings, and display initial data in compass
        tabs.

        Parameters
        ----------
        old_discharge: dataframe
            Pandas dataframe containing measurement summary prior to changes
        """

        # Setup data table
        tbl = self.compass_table
        tbl.clear()
        table_header = [
            self.tr("Plot / Station"),
            self.tr("Location \n") + self.units["label_L"],
            self.tr("Magnetic \n Variation \n(deg)"),
            self.tr("Beam \n Misalignment \n (deg)"),
            self.tr("Heading \n Source"),
            self.tr("Pitch \n Mean \n (deg)"),
            self.tr("Pitch \n Std. Dev. \n (deg)"),
            self.tr("Roll \n Mean \n (deg)"),
            self.tr("Roll \n Std. Dev. \n (deg)"),
            self.tr("Discharge \n Previous \n") + self.units["label_Q"],
            self.tr("Discharge \n Now \n") + self.units["label_Q"],
            self.tr("Discharge \n % Change"),
        ]
        ncols = len(table_header)
        nrows = self.meas.summary.shape[0] - 2
        tbl.setRowCount(nrows)
        tbl.setColumnCount(ncols)
        tbl.setHorizontalHeaderLabels(table_header)
        tbl.horizontalHeader().setFont(self.font_bold)
        tbl.verticalHeader().hide()
        tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)
        tbl.setStyleSheet("QToolTip{font: 12pt}")

        if not self.compass_pr_initialized:
            # Connect table
            tbl.cellClicked.connect(self.compass_table_clicked)
            tbl.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
            tbl.customContextMenuRequested.connect(self.compass_table_right_click)

            # Connect plot variable checkboxes
            self.cb_compass_adcp.stateChanged.connect(self.compass_pr_plot)
            self.cb_compass_mag_field.stateChanged.connect(self.compass_pr_plot)
            self.cb_compass_pitch.stateChanged.connect(self.compass_pr_plot)
            self.cb_compass_roll.stateChanged.connect(self.compass_pr_plot)

            # Magnetic Variation
            self.compass_magvar_grp = EdGrp(
                ed=self.ed_magvar,
                pb_apply=self.pb_magvar_apply,
                pb_apply_all=self.pb_magvar_apply_all,
                change=self.compass_magvar_change,
            )
            self.ed_magvar.textChanged.connect(self.compass_magvar_grp.enable_apply)
            self.pb_magvar_apply.clicked.connect(self.compass_magvar_grp.apply)
            self.pb_magvar_apply_all.clicked.connect(self.compass_magvar_grp.apply_all)

            # Beam rotation
            self.compass_beam_rotation_grp = EdGrp(
                ed=self.ed_beam_rotation,
                pb_apply=self.pb_beam_rotation_apply,
                pb_apply_all=self.pb_beam_rotation_apply_all,
                change=self.compass_beam_rotation_change,
            )
            self.ed_beam_rotation.textChanged.connect(
                self.compass_beam_rotation_grp.enable_apply
            )
            self.pb_beam_rotation_apply.clicked.connect(
                self.compass_beam_rotation_grp.apply
            )
            self.pb_beam_rotation_apply_all.clicked.connect(
                self.compass_beam_rotation_grp.apply_all
            )

            # Heading Source
            self.compass_heading_source_grp = ComboGrp(
                combo=self.combo_heading_source,
                pb_apply=self.pb_heading_source_apply,
                pb_apply_all=self.pb_heading_source_apply_all,
                change=self.compass_heading_source_change,
            )
            self.combo_heading_source.activated.connect(
                self.compass_heading_source_grp.enable_apply
            )
            self.pb_heading_source_apply.clicked.connect(
                self.compass_heading_source_grp.apply
            )
            self.pb_heading_source_apply_all.clicked.connect(
                self.compass_heading_source_grp.apply_all
            )

            self.compass_pr_initialized = True

        # Disable buttons
        self.pb_magvar_apply.setEnabled(False)
        self.pb_magvar_apply_all.setEnabled(False)
        self.pb_heading_source_apply.setEnabled(False)
        self.pb_heading_source_apply_all.setEnabled(False)
        self.pb_beam_rotation_apply.setEnabled(False)
        self.pb_beam_rotation_apply_all.setEnabled(False)

        # Configure mag field error
        self.cb_compass_mag_field.blockSignals(True)
        self.cb_compass_mag_field.setEnabled(False)
        self.cb_compass_mag_field.setChecked(False)
        for idx in self.meas.verticals_used_idx:
            if (
                self.meas.verticals[idx].data.sensors is not None
                and self.meas.verticals[idx].data.sensors.heading_deg.internal.mag_error
                is not None
                and np.logical_not(
                    np.all(
                        np.isnan(
                            self.meas.verticals[
                                idx
                            ].data.sensors.heading_deg.internal.mag_error
                        )
                    )
                )
            ):
                self.cb_compass_mag_field.setEnabled(True)
                self.cb_compass_mag_field.setChecked(True)
                break
        self.cb_compass_mag_field.blockSignals(False)

        # # Configure external heading
        # self.cb_compass_ext.blockSignals(True)
        # self.cb_compass_ext.setEnabled(False)
        # self.cb_compass_ext.setChecked(False)
        # for idx in self.meas.verticals_used_idx:
        #     if (
        #         self.meas.verticals[idx].data.sensors is not None
        #         and self.meas.verticals[idx].data.sensors.heading_deg.external
        #         is not None
        #     ):
        #         self.cb_compass_ext.setEnabled(True)
        #         break
        # self.cb_compass_ext.blockSignals(False)

        # Update table, graphs, messages, and comments
        if old_discharge is None:
            old_discharge = self.meas.summary

        self.compass_display_idx_checked = [
            self.meas.verticals_used_idx[
                int(self.meas.summary["Station Number"][self.main_row_idx - 1] - 1)
            ]
        ]

        self.compass_update_tab(
            tbl=tbl, old_discharge=old_discharge, new_discharge=self.meas.summary
        )

        # Setup list for use by graphics controls
        self.canvases = [self.compass_pr_canvas]
        self.figs = [self.compass_pr_fig]
        self.toolbars = [self.compass_pr_toolbar]
        self.fig_calls = [self.compass_pr_plot]
        self.ui_parents = [i.parent() for i in self.canvases]
        self.figs_menu_connection()

        # Initialize the calibration/evaluation tab
        self.compass_cal_eval(idx_eval=0)

    def compass_cal_eval(self, idx_cal=None, idx_eval=None):
        """Displays data in the calibration / evaluation tab.

        idx_cal: int
            Index of calibration to display in text box
        idx_eval: int
            Index of evaluation to display in text box
        """

        # Setup calibration table
        tblc = self.compass_ce_cal_table
        nrows = len(self.meas.compass_cal)
        tblc.setRowCount(nrows)
        tblc.setColumnCount(1)
        header_text = [self.tr("Date/Time")]
        tblc.setHorizontalHeaderLabels(header_text)
        tblc.horizontalHeader().setFont(self.font_bold)
        tblc.verticalHeader().hide()
        tblc.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)
        tblc.cellClicked.connect(self.compass_select_calibration)

        # Add calibrations
        if nrows > 0:
            for row, test in enumerate(self.meas.compass_cal):
                col = 0
                tblc.setItem(row, col, QtWidgets.QTableWidgetItem(test.time_stamp))
                tblc.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
                tblc.item(row, col).setFont(self.font_normal)

            # Display selected calibration in text box
            if idx_cal is not None:
                self.compass_ce_result.clear()
                tblc.item(idx_cal, 0).setFont(self.font_bold)
                self.compass_ce_result.textCursor().insertText(
                    self.meas.compass_cal[idx_cal].data
                )

            tblc.resizeColumnsToContents()
            tblc.resizeRowsToContents()

        # Setup evaluation table
        tble = self.compass_ce_eval_table

        # SonTek has no independent evaluation. Evaluation results are
        # reported with the calibration.
        if self.meas.verticals[self.vertical_idx].data.inst.manufacturer == "SonTek":
            evals = self.meas.compass_cal
        else:
            evals = self.meas.compass_eval
        nrows = len(evals)
        tble.setRowCount(nrows)
        tble.setColumnCount(2)
        header_text = [self.tr("Date/Time"), self.tr("Error")]
        tble.setHorizontalHeaderLabels(header_text)
        tble.horizontalHeader().setFont(self.font_bold)
        tble.verticalHeader().hide()
        tble.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)
        tble.cellClicked.connect(self.compass_select_evaluation)

        # Add evaluations
        if nrows > 0:
            for row, test in enumerate(evals):
                col = 0
                tble.setItem(row, col, QtWidgets.QTableWidgetItem(test.time_stamp))
                tble.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
                tble.item(row, col).setFont(self.font_normal)

                col = 1
                if type(test.result["compass"]["error"]) == str:
                    item = test.result["compass"]["error"]
                else:
                    item = "{:2.2f}".format(test.result["compass"]["error"])
                tble.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tble.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            if self.meas.qa.compass["status1"] == "warning":
                tble.item(nrows - 1, 1).setBackground(QtGui.QColor(255, 77, 77))
            elif self.meas.qa.compass["status1"] == "caution":
                tble.item(nrows - 1, 1).setBackground(QtGui.QColor(255, 204, 0))
            else:
                tble.item(nrows - 1, 1).setBackground(QtGui.QColor(255, 255, 255))

            # Display selected evaluation in text box
            if idx_eval is not None:
                self.compass_ce_result.clear()
                tble.item(idx_eval, 0).setFont(self.font_bold)
                # self.compass_ce_result.textCursor().insertText(evals[idx_eval].data)
                self.compass_ce_result.insertPlainText(evals[idx_eval].data)
            tble.resizeColumnsToContents()
            tble.resizeRowsToContents()
        if tble.rowCount() == 0 and tblc.rowCount() == 0:
            self.compass_ce_result.clear()

    def compass_comments_messages(self):
        """Displays the messages associated with the compass, pitch,
        and roll and any comments provided by the user.
        """

        # Clear current content
        self.display_compass_comments.clear()

        if self.meas is not None:
            # Comments
            self.display_compass_comments.moveCursor(QtGui.QTextCursor.Start)
            for comment in self.meas.comments:
                # Display each comment on a new line
                self.display_compass_comments.textCursor().insertText(comment)
                self.display_compass_comments.moveCursor(QtGui.QTextCursor.End)
                self.display_compass_comments.textCursor().insertBlock()

            # Messages
            self.messages_table(self.table_compass_messages, ["compass"])

            self.update_tab_icons()
            if self.meas.qa.compass["status1"] != "default":
                self.set_tab_icon(
                    "compasspr_subtab_ce",
                    self.meas.qa.compass["status1"],
                    tab_base=self.compasspr_subtabs,
                )

    def compass_update_tab(self, tbl, old_discharge, new_discharge):
        """Populates the table and draws the graphs with the current data.

        tbl: QTableWidget
            Reference to the QTableWidget
        old_discharge: dataframe
            Pandas dataframe containing measurement summary prior to changes
        new_discharge: dataframe
            Pandas dataframe containing measurement summary from current
            settings
        """

        with self.wait_cursor():
            # Populate each row
            for row in range(tbl.rowCount()):
                # Station
                col = 0
                if np.isnan(self.meas.summary["Station Number"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                elif self.meas.summary["Station Number"][row] == np.floor(
                    self.meas.summary["Station Number"][row]
                ):
                    checked = QtWidgets.QTableWidgetItem(
                        "{:3.0f}".format(self.meas.summary["Station Number"][row])
                    )
                    checked.setFlags(
                        QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled
                    )
                    tbl.setItem(row, col, checked)
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:3.1f}".format(self.meas.summary["Station Number"][row])
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                col += 1
                if np.isnan(self.meas.summary["Location"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:3.2f}".format(
                                self.meas.summary["Location"][row] * self.units["L"]
                            )
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Magnetic variation
                col += 1
                if np.isnan(self.meas.summary["MagVar"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:3.2f}".format(self.meas.summary["MagVar"][row])
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
                # Inconsistent magvar
                if self.meas.qa.compass["magvar"] == 1:
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                    tbl.item(row, col).setToolTip(
                        self.tr(
                            "Magnetic variation is not consistent among " "transects"
                        )
                    )

                # Magvar is zero
                elif self.meas.qa.compass["magvar"] == 2:
                    if (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.compass["magvar_idx"]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                        tbl.item(row, col).setToolTip(
                            self.tr(
                                "Magnetic variation is 0 and GPS data are " "present"
                            )
                        )

                else:
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))

                # Beam Misalignment
                col += 1
                if np.isnan(self.meas.summary["Beam Misalignment"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:3.2f}".format(
                                self.meas.summary["Beam Misalignment"][row]
                            )
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Heading source
                col += 1
                if len(self.meas.summary["Heading Source"][row]) == 0:
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    if self.meas.summary["Heading Source"][row] == "user":
                        text = "None"
                    else:
                        text = self.meas.summary["Heading Source"][row]
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            self.meas.summary["Heading Source"][row]
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                if (
                    self.meas.summary["Station Number"][row]
                    in self.meas.qa.compass["mag_error_idx"]
                ):
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                    tbl.item(row, col).setToolTip(
                        self.tr("Change in mag field exceeds 2%")
                    )
                else:
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))

                # Mean pitch
                col += 1
                item = self.meas.summary["Mean Pitch"][row]
                if np.isnan(item):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row, col, QtWidgets.QTableWidgetItem("{:3.1f}".format(item))
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
                if (
                    self.meas.summary["Station Number"][row]
                    in self.meas.qa.compass["pitch_mean_warning_idx"]
                ):
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                    tbl.item(row, col).setToolTip(
                        self.tr("Mean pitch is greater than 8 deg")
                    )
                elif (
                    self.meas.summary["Station Number"][row]
                    in self.meas.qa.compass["pitch_mean_caution_idx"]
                ):
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                    tbl.item(row, col).setToolTip(
                        self.tr("Mean pitch is greater than 4 deg")
                    )
                else:
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))

                # Pitch standard deviation
                col += 1
                item = self.meas.summary["Pitch StdDev"][row]
                if np.isnan(item):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row, col, QtWidgets.QTableWidgetItem("{:3.1f}".format(item))
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
                if (
                    self.meas.summary["Station Number"][row]
                    in self.meas.qa.compass["pitch_std_caution_idx"]
                ):
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                    tbl.item(row, col).setToolTip(
                        self.tr("Pitch standard deviation is greater than 5 deg")
                    )
                else:
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))

                # Mean roll
                col += 1
                item = self.meas.summary["Mean Roll"][row]
                if np.isnan(item):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row, col, QtWidgets.QTableWidgetItem("{:3.1f}".format(item))
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
                if (
                    self.meas.summary["Station Number"][row]
                    in self.meas.qa.compass["roll_mean_warning_idx"]
                ):
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                    tbl.item(row, col).setToolTip(
                        self.tr("Mean roll is greater than 8 deg")
                    )
                elif (
                    self.meas.summary["Station Number"][row]
                    in self.meas.qa.compass["roll_mean_caution_idx"]
                ):
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                    tbl.item(row, col).setToolTip(
                        self.tr("Mean roll is greater than 4 deg")
                    )
                else:
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))

                # Roll standard deviation
                col += 1
                item = self.meas.summary["Roll StdDev"][row]
                if np.isnan(item):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row, col, QtWidgets.QTableWidgetItem("{:3.1f}".format(item))
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
                if (
                    self.meas.summary["Station Number"][row]
                    in self.meas.qa.compass["roll_std_caution_idx"]
                ):
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                    tbl.item(row, col).setToolTip(
                        self.tr("Roll standard deviation is greater than 5 deg")
                    )
                else:
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))

                # Discharge from previous settings
                col += 1
                if np.isnan(self.meas.summary["Q"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:8}".format(
                                self.q_digits(old_discharge["Q"][row] * self.units["Q"])
                            )
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Discharge from new/current settings
                col += 1
                if np.isnan(self.meas.summary["Q"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:8}".format(
                                self.q_digits(new_discharge["Q"][row] * self.units["Q"])
                            )
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Percent difference in old and new discharges
                col += 1
                if np.isnan(self.meas.summary["Q"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    if np.abs(old_discharge["Q"][row]) > 0:
                        per_change = (
                            (new_discharge["Q"][row] - old_discharge["Q"][row])
                            / old_discharge["Q"][row]
                        ) * 100
                        tbl.setItem(
                            row,
                            col,
                            QtWidgets.QTableWidgetItem("{:3.1f}".format(per_change)),
                        )
                    else:
                        tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # If initial is specified uncheck all rows
                tbl.item(row, 0).setCheckState(QtCore.Qt.Unchecked)

            tbl.item(self.main_row_idx - 1, 0).setCheckState(QtCore.Qt.Checked)

            # Beam rotation only valid for fixed method
            if np.any(self.meas.summary["Velocity Method"].str.contains("Fixed")):
                tbl.setColumnHidden(3, False)
            else:
                tbl.setColumnHidden(3, True)

            # Highlight selected row
            self.highlight_row(tbl, self.main_row_idx - 1)
            self.compass_update_user_input()

            # Automatically resize rows and columns
            tbl.resizeColumnsToContents()
            tbl.resizeRowsToContents()

            # Update graphics, comments, and messages
            self.compass_pr_plot()
            self.compass_comments_messages()

    def compass_update_user_input(self):
        self.ed_magvar.blockSignals(True)
        self.combo_heading_source.blockSignals(True)
        self.ed_beam_rotation.blockSignals(True)
        self.ed_magvar.setText(
            "{:3.1f}".format(self.meas.summary["MagVar"][self.main_row_idx - 1])
        )

        if (
            self.meas.summary["Heading Source"][self.main_row_idx - 1].lower()
            == "internal"
        ):
            self.combo_heading_source.setCurrentIndex(0)
        elif (
            self.meas.summary["Heading Source"][self.main_row_idx - 1].lower() == "none"
        ):
            self.combo_heading_source.setCurrentIndex(1)
        elif (
            self.meas.summary["Heading Source"][self.main_row_idx - 1].lower() == "user"
        ):
            self.combo_heading_source.setCurrentIndex(1)

        # Beam rotation
        if self.meas.summary["Velocity Method"][self.main_row_idx - 1] == "Fixed":
            self.gb_beam_rotation.show()
            self.ed_beam_rotation.setText(
                "{:3.1f}".format(
                    self.meas.summary["Beam Misalignment"][self.main_row_idx - 1]
                )
            )
        else:
            self.gb_beam_rotation.hide()

        self.compasspr_subtab_data.setFocus()
        self.ed_magvar.blockSignals(False)
        self.combo_heading_source.blockSignals(False)
        self.ed_beam_rotation.blockSignals(False)

    @QtCore.pyqtSlot(QtCore.QPoint)
    def compass_table_right_click(self, pos):
        """Manages actions caused by the user right clicking in selected
        columns of the table.

        Parameters
        ==========
        pos: QPoint
            A location in the table
        """

        tbl = self.compass_table
        cell = tbl.itemAt(pos)
        column = cell.column()
        row = cell.row()

        # Change verticals plotted
        if column == 0:
            if row > 1 and self.meas.summary["Station Number"][row] == np.floor(
                self.meas.summary["Station Number"][row]
            ):
                if tbl.item(row, 0).checkState() == QtCore.Qt.Checked:
                    tbl.item(row, 0).setCheckState(QtCore.Qt.Unchecked)
                else:
                    tbl.item(row, 0).setCheckState(QtCore.Qt.Checked)

                self.compass_display_idx_checked = []
                for r in range(tbl.rowCount()):
                    if tbl.item(r, 0).checkState() == QtCore.Qt.Checked:
                        self.compass_display_idx_checked.append(
                            self.meas.verticals_used_idx[
                                int(self.meas.summary["Station Number"][r] - 1)
                            ]
                        )

                self.compass_pr_plot()
                self.canvases = [self.compass_pr_canvas]
                self.figs = [self.compass_pr_fig]
                self.toolbars = [self.compass_pr_toolbar]
                self.fig_calls = [self.compass_pr_plot]
                self.ui_parents = [i.parent() for i in self.canvases]
                self.figs_menu_connection()
            else:
                tbl.item(row, 0).setCheckState(QtCore.Qt.Unchecked)
                self.compasspr_subtab_data.setFocus()

    @QtCore.pyqtSlot(int, int)
    def compass_table_clicked(self, row):
        """Manages actions caused by the user clicking table.

        Parameters
        ==========
        row: int
            row in table clicked by user
        """

        tbl = self.compass_table

        if self.valid_row(row):
            # Change transects plotted
            for nrow in range(tbl.rowCount()):
                tbl.item(nrow, 0).setCheckState(QtCore.Qt.Unchecked)
            self.compass_display_idx_checked = [
                self.meas.verticals_used_idx[
                    int(self.meas.summary["Station Number"][row] - 1)
                ]
            ]
            tbl.item(row, 0).setCheckState(QtCore.Qt.Checked)
            self.highlight_row(tbl, row)
            self.compass_pr_plot()
            self.canvases = [self.compass_pr_canvas]
            self.figs = [self.compass_pr_fig]
            self.toolbars = [self.compass_pr_toolbar]
            self.fig_calls = [self.compass_pr_plot]
            self.ui_parents = [i.parent() for i in self.canvases]
            self.figs_menu_connection()
            self.change = True
            self.compass_update_user_input()
        # else:
        #     tbl.item(row, 0).setCheckState(QtCore.Qt.Unchecked)

        self.compasspr_subtab_data.setFocus()
        self.change = True

    def compass_select_calibration(self, row, column):
        """Displays selected compass calibration.

        Parameters
        ==========
        row: int
            row in table clicked by user
        column: int
            column in table clicked by user
        """
        if column == 0:
            with self.wait_cursor():
                # Set all files to normal font
                for nrow in range(self.compass_ce_cal_table.rowCount()):
                    self.compass_ce_cal_table.item(nrow, 0).setFont(self.font_normal)
                for nrow in range(self.compass_ce_eval_table.rowCount()):
                    self.compass_ce_eval_table.item(nrow, 0).setFont(self.font_normal)

                # Set selected file to bold font
                self.compass_ce_cal_table.item(row, 0).setFont(self.font_bold)

                # Update
                self.compass_ce_result.clear()
                self.compass_ce_result.insertPlainText(self.meas.compass_cal[row].data)

    def compass_select_evaluation(self, row, column):
        """Displays selected compass evaluation.

        Parameters
        ==========
        row: int
            row in table clicked by user
        column: int
            column in table clicked by user
        """

        if column == 0:
            with self.wait_cursor():
                # Set all files to normal font
                for nrow in range(self.compass_ce_cal_table.rowCount()):
                    self.compass_ce_cal_table.item(nrow, 0).setFont(self.font_normal)
                for nrow in range(self.compass_ce_eval_table.rowCount()):
                    self.compass_ce_eval_table.item(nrow, 0).setFont(self.font_normal)

                # Set selected file to bold font
                self.compass_ce_eval_table.item(row, 0).setFont(self.font_bold)

                # Update
                self.compass_ce_result.clear()
                # SonTek has no separate evaluations so the calibration is
                # displayed
                if (
                    self.meas.verticals[self.vertical_idx].data.inst.manufacturer
                    == "SonTek"
                ):
                    self.compass_ce_result.insertPlainText(
                        self.meas.compass_cal[row].data
                    )
                else:
                    self.compass_ce_result.insertPlainText(
                        self.meas.compass_eval[row].data
                    )

    def compass_pr_plot(self):
        """Generates the graph of heading and magnetic change."""

        if self.compass_pr_canvas is None:
            # Create the canvas
            self.compass_pr_canvas = MplCanvas(
                self.compass_pr_graph, width=6, height=2, dpi=80
            )
            # Assign layout to widget to allow auto scaling
            layout = QtWidgets.QVBoxLayout(self.compass_pr_graph)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(1, 1, 1, 1)
            # Add the canvas
            layout.addWidget(self.compass_pr_canvas)
            # Initialize hidden toolbar for use by graphics controls
            self.compass_pr_toolbar = NavigationToolbar(self.compass_pr_canvas, self)
            self.compass_pr_toolbar.hide()

            # Initialize the temperature figure and assign to the canvas
        self.compass_pr_fig = Graphics(canvas=self.compass_pr_canvas)
        # Create the figure with the specified data
        heading_types = []
        if self.cb_compass_adcp.isChecked():
            heading_types.append("Internal")
        # if self.cb_compass_ext.isChecked():
        #     heading_types.append("External")
        if self.cb_compass_mag_field.isChecked():
            heading_types.append("MagChange")
        pr_types = []
        if self.cb_compass_pitch.isChecked():
            pr_types.append("Pitch")
        if self.cb_compass_roll.isChecked():
            pr_types.append("Roll")

        self.compass_pr_fig.compass_pr_graphs(
            meas=self.meas,
            units=self.units,
            heading_types=heading_types,
            pr_types=pr_types,
            idx_list=self.compass_display_idx_checked,
        )

        self.clear_zphd()

        # Draw canvas
        self.compass_pr_canvas.draw()

        self.compasspr_subtab_data.setFocus()

    def compass_magvar_change(self, apply_all=False):
        """Applies the change in magnetic variation to the selected vertical."""

        # Block signals to prevent changes causing two computations
        self.ed_magvar.blockSignals(True)

        # Save old discharge for comparison and computing change
        old_discharge = self.meas.summary

        # Get value for edit box
        magvar = self.check_numeric_input(self.ed_magvar, block=False)

        # Apply the change
        if magvar is not None:
            if apply_all:
                self.meas.change_magvar(magvar)
            else:
                self.meas.change_magvar(magvar, idx=self.vertical_idx)
            self.change = True
            self.tab_manager(old_discharge=old_discharge)

        # Reset the UI
        self.compasspr_tab.setFocus()
        self.pb_magvar_apply.setEnabled(False)
        self.pb_magvar_apply_all.setEnabled(False)
        self.ed_magvar.blockSignals(False)

    def compass_beam_rotation_change(self, apply_all=False):
        """Applies the change in magnetic variation to the selected vertical."""

        # Block signals to prevent changes causing two computations
        self.ed_beam_rotation.blockSignals(True)

        # Save old discharge for comparison and computing change
        old_discharge = self.meas.summary

        # Get value for edit box
        rotation = self.check_numeric_input(self.ed_beam_rotation, block=False)

        # Apply the change
        if rotation is not None:
            if apply_all:
                self.meas.change_station_misalignment(rotation)
            else:
                self.meas.change_station_misalignment(rotation, idx=self.vertical_idx)
            self.change = True
            self.tab_manager(old_discharge=old_discharge)

        # Reset the UI
        self.compasspr_tab.setFocus()
        self.pb_beam_rotation_apply.setEnabled(False)
        self.pb_beam_rotation_apply_all.setEnabled(False)
        self.ed_beam_rotation.blockSignals(False)

    def compass_heading_source_change(self, apply_all=False):
        """Changes the heading source for the selected vertical."""

        # Block signals to prevent changes causing two computations
        self.combo_heading_source.blockSignals(True)

        # Save old discharge for comparison and computing change
        old_discharge = self.meas.summary

        # Get setting from combobox
        text = self.combo_heading_source.currentText()

        if apply_all:
            idx = None
        else:
            idx = self.vertical_idx

        # Apply selected heading source
        if text == "Internal":
            self.meas.change_heading_source("internal", idx=idx)
        elif text == "None":
            self.meas.change_heading_source("user", idx=idx)
        self.change = True
        self.tab_manager(old_discharge=old_discharge)

        # Reset UI
        self.compasspr_tab.setFocus()
        self.pb_heading_source_apply.setEnabled(False)
        self.pb_heading_source_apply_all.setEnabled(False)
        self.combo_heading_source.blockSignals(False)

    # Temperature & Salinity Tab
    # ==========================
    def tempsal_tab_update(self, old_discharge=None):
        """Initialize tempsal tab and associated settings.

        Parameters
        ----------
        old_discharge: dataframe
            Pandas dataframe containing measurement summary prior to changes
        """

        # Setup data table
        tbl = self.table_tempsal
        table_header = [
            self.tr("Station"),
            self.tr("Temperature \n Source"),
            self.tr("Average \n Temperature"),
            self.tr("Average \n Salinity (ppt)"),
            self.tr("Speed of \n Sound Source"),
            self.tr("Speed of \n Sound" + self.units["label_V"]),
            self.tr("Discharge \n Previous \n" + self.units["label_Q"]),
            self.tr("Discharge \n Now \n" + self.units["label_Q"]),
            self.tr("Discharge \n % Change"),
        ]
        ncols = len(table_header)
        nrows = self.meas.summary.shape[0] - 2
        tbl.setRowCount(nrows)
        tbl.setColumnCount(ncols)
        tbl.setHorizontalHeaderLabels(table_header)
        tbl.horizontalHeader().setFont(self.font_bold)
        tbl.verticalHeader().hide()
        tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)

        # Update the table, independent, and user temperatures
        if old_discharge is None:
            old_discharge = self.meas.summary
        self.tempsal_update(
            old_discharge=old_discharge, new_discharge=self.meas.summary
        )

        # Display the times series plot of ADCP temperatures
        self.tempsal_plot_temperature()

        if not self.tempsal_initialized:
            # Temperature Source
            self.tempsal_temp_source_grp = ComboEdGrp(
                combo=self.combo_temp_source,
                ed=self.ed_user_temp_2,
                pb_apply=self.pb_temp_source_apply,
                pb_apply_all=self.pb_temp_source_apply_all,
                change=self.tempsal_temp_source_change,
            )
            self.combo_temp_source.activated.connect(
                self.tempsal_temp_source_grp.enable_apply
            )
            self.pb_temp_source_apply.clicked.connect(
                self.tempsal_temp_source_grp.apply
            )
            self.pb_temp_source_apply_all.clicked.connect(
                self.tempsal_temp_source_grp.apply_all
            )
            self.ed_user_temp_2.textChanged.connect(
                self.tempsal_temp_source_grp.enable_apply
            )

            # Salinity
            self.tempsal_salinity_grp = EdGrp(
                ed=self.ed_salinity,
                pb_apply=self.pb_salinity_apply,
                pb_apply_all=self.pb_salinity_apply_all,
                change=self.tempsal_salinity_change,
                min_value=0,
            )
            self.ed_salinity.textChanged.connect(self.tempsal_salinity_grp.enable_apply)
            self.pb_salinity_apply.clicked.connect(self.tempsal_salinity_grp.apply)
            self.pb_salinity_apply_all.clicked.connect(
                self.tempsal_salinity_grp.apply_all
            )

            # SOS
            self.tempsal_sos_source_grp = ComboEdGrp(
                combo=self.combo_sos_source,
                ed=self.ed_user_sos,
                pb_apply=self.pb_sos_source_apply,
                pb_apply_all=self.pb_sos_source_apply_all,
                change=self.tempsal_sos_source_change,
                min_value=0,
            )
            self.combo_sos_source.activated.connect(
                self.tempsal_sos_source_grp.enable_apply
            )
            self.pb_sos_source_apply.clicked.connect(self.tempsal_sos_source_grp.apply)
            self.pb_sos_source_apply_all.clicked.connect(
                self.tempsal_sos_source_grp.apply_all
            )
            self.ed_user_sos.textChanged.connect(
                self.tempsal_sos_source_grp.enable_apply
            )

            # Connect temperature units radio buttons
            self.rb_c.toggled.connect(self.tempsal_change_temp_units)
            self.rb_f.toggled.connect(self.tempsal_change_temp_units)

            # Setup input validator for independent and adcp user temperature
            reg_ex = QRegExp("^[+ -]?[0-9]{1,3}([.][0-9]{0,2})?$")
            input_validator = QtGui.QRegExpValidator(reg_ex, self)

            # Connect independent and adcp input option
            self.ed_user_temp.setValidator(input_validator)
            self.ed_adcp_temp.setValidator(input_validator)
            self.ed_user_temp.editingFinished.connect(self.tempsal_user_temp_changed)
            self.ed_adcp_temp.editingFinished.connect(self.tempsal_adcp_temp_changed)
            self.table_tempsal.cellClicked.connect(self.tempsal_table_clicked)
            self.tempsal_initialized = True

        self.canvases = [self.tts_canvas]
        self.toolbars = [self.tts_toolbar]
        self.figs = [self.tts_fig]
        self.fig_calls = [self.tempsal_plot_temperature]
        self.ui_parents = [i.parent() for i in self.canvases]
        self.figs_menu_connection()

    def tempsal_update(self, old_discharge, new_discharge):
        """Updates all data displayed on the tempsal tab.

        Parameters
        ==========
        old_discharge: dataframe
            Pandas dataframe containing measurement summary prior to changes
        new_discharge: dataframe
            Pandas dataframe containing measurement summary after change
                applied
        """

        tbl = self.table_tempsal

        # Initialize array to accumalate all temperature data
        temp_all = np.array([])

        for row in range(tbl.rowCount()):
            # Station
            col = 0
            if np.isnan(self.meas.summary["Station Number"][row]):
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            elif self.meas.summary["Station Number"][row] == np.floor(
                self.meas.summary["Station Number"][row]
            ):
                checked = QtWidgets.QTableWidgetItem(
                    "{:3.0f}".format(self.meas.summary["Station Number"][row])
                )
                checked.setFlags(
                    QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled
                )
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(checked))
            else:
                tbl.setItem(
                    row,
                    col,
                    QtWidgets.QTableWidgetItem(
                        "{:3.1f}".format(self.meas.summary["Station Number"][row])
                    ),
                )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Temperature source
            col += 1
            item = self.tr("Internal (ADCP)")
            if len(self.meas.summary["Temperature Source"][row]) > 0:
                if self.meas.summary["Temperature Source"][row] == "user":
                    item = "User"
            else:
                item = ""
            tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Average temperature for transect
            col += 1
            if np.isnan(self.meas.summary["Mean Temperature"][row]):
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            else:
                temp_converted = self.meas.summary["Mean Temperature"][row]
                if self.rb_f.isChecked():
                    temp_converted = convert_temperature(
                        temp_in=temp_converted, units_in="C", units_out="F"
                    )
                tbl.setItem(
                    row,
                    col,
                    QtWidgets.QTableWidgetItem("{:3.1f}".format(temp_converted)),
                )

            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Salinity for transect
            col += 1
            if np.isnan(self.meas.summary["Salinity"][row]):
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            else:
                tbl.setItem(
                    row,
                    col,
                    QtWidgets.QTableWidgetItem(
                        "{:3.1f}".format(self.meas.summary["Salinity"][row])
                    ),
                )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Speed of sound source
            col += 1
            sos_source = self.meas.summary["SOS Source"][row]
            if len(sos_source) > 0:
                if sos_source == "Internal":
                    item = self.tr("Internal (ADCP)")
                else:
                    item = self.tr("Computed")
            else:
                item = ""

            tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Average speed of sound
            col += 1
            if np.isnan(self.meas.summary["SOS"][row]):
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            else:
                tbl.setItem(
                    row,
                    col,
                    QtWidgets.QTableWidgetItem(
                        "{:3.1f}".format(
                            self.meas.summary["SOS"][row] * self.units["V"]
                        )
                    ),
                )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Discharge before changes
            col += 1
            if np.isnan(old_discharge["Q"][row]):
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            else:
                tbl.setItem(
                    row,
                    col,
                    QtWidgets.QTableWidgetItem(
                        "{:8}".format(
                            self.q_digits(old_discharge["Q"][row] * self.units["Q"])
                        )
                    ),
                )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Discharge after changes
            col += 1
            if np.isnan(new_discharge["Q"][row]):
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            else:
                tbl.setItem(
                    row,
                    col,
                    QtWidgets.QTableWidgetItem(
                        "{:8}".format(
                            self.q_digits(new_discharge["Q"][row] * self.units["Q"])
                        )
                    ),
                )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Percent change in discharge
            col += 1
            if np.isnan(self.meas.summary["Q"][row]):
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            else:
                if np.abs(old_discharge["Q"][row]) > 0:
                    per_change = (
                        (new_discharge["Q"][row] - old_discharge["Q"][row])
                        / old_discharge["Q"][row]
                    ) * 100
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem("{:3.1f}".format(per_change)),
                    )
                else:
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Accumulate all temperature data in a single array used to
            # compute mean temperature
            if np.logical_not(np.isnan(self.meas.summary["Mean Temperature"][row])):
                vertical_id = self.meas.verticals_used_idx[
                    self.meas.summary["Station Number"][row] - 1
                ]
                temp_all = np.append(
                    temp_all,
                    self.meas.verticals[
                        vertical_id
                    ].data.sensors.temperature_deg_c.internal.data,
                )

        self.highlight_row(tbl, self.main_row_idx - 1)
        tbl.resizeColumnsToContents()
        tbl.resizeRowsToContents()

        # Display independent temperature reading, if available
        try:
            if np.isnan(self.meas.temp_chk["user"]):
                self.ed_user_temp.setText("")
                self.label_independent.setStyleSheet(
                    "background: #ffcc00; "
                    "font: 12pt MS Shell "
                    "Dlg 2;QToolTip{font: "
                    "12pt}"
                )
                self.label_independent.setToolTip(
                    self.tr("No user supplied temperature.")
                )
            else:
                temp = float(self.meas.temp_chk["user"])
                if self.rb_f.isChecked():
                    temp = convert_temperature(
                        self.meas.temp_chk["user"], units_in="C", units_out="F"
                    )
                self.ed_user_temp.setText("{:3.1f}".format(temp))
                self.label_independent.setStyleSheet(
                    "background: white; font: 12pt MS Shell Dlg 2"
                )
        except (ValueError, TypeError):
            self.ed_user_temp.setText("")

        # Display user provided adcp temperature reading if available
        if np.isnan(self.meas.temp_chk["adcp"]):
            self.ed_adcp_temp.setText("")
        else:
            try:
                temp = float(self.meas.temp_chk["adcp"])
                if self.rb_f.isChecked():
                    temp = convert_temperature(
                        self.meas.temp_chk["adcp"], units_in="C", units_out="F"
                    )
                self.ed_adcp_temp.setText("{:3.1f}".format(temp))
            except (ValueError, TypeError):
                self.ed_adcp_temp.setText("")

        # Display mean temperature measured by ADCP
        temp = np.nanmean(temp_all)
        if self.rb_f.isChecked():
            temp = convert_temperature(temp, units_in="C", units_out="F")
        self.txt_adcp_avg.setText("{:3.1f}".format(temp))

        # Set initial state of temp, sal, and sos user change ui
        self.pb_sos_source_apply.setEnabled(False)
        self.pb_sos_source_apply_all.setEnabled(False)
        self.ed_user_sos.setEnabled(False)
        self.pb_salinity_apply.setEnabled(False)
        self.pb_salinity_apply_all.setEnabled(False)
        self.pb_temp_source_apply.setEnabled(False)
        self.pb_temp_source_apply_all.setEnabled(False)
        self.ed_user_temp_2.setEnabled(False)

        if self.meas.verticals[self.vertical_idx].sampling_duration_sec > 0:
            if (
                self.meas.verticals[
                    self.vertical_idx
                ].data.sensors.temperature_deg_c.selected.lower()
                == "user"
            ):
                self.ed_user_temp_2.setEnabled(True)
                user_temp = self.meas.verticals[
                    self.vertical_idx
                ].data.sensors.temperature_deg_c.user.data[0]
                if self.rb_f.isChecked():
                    user_temp = convert_temperature(
                        user_temp, units_in="C", units_out="F"
                    )
                self.ed_user_temp_2.setText("{:4.2f}".format(user_temp))
                self.combo_temp_source.setCurrentIndex(1)
            else:
                self.combo_temp_source.setCurrentIndex(0)
                self.ed_user_temp_2.setText("")
                self.ed_user_temp_2.setEnabled(False)

            if (
                self.meas.verticals[
                    self.vertical_idx
                ].data.sensors.speed_of_sound_mps.selected.lower()
                == "user"
            ):
                self.ed_user_sos.setEnabled(True)
                user_sos = self.meas.verticals[
                    self.vertical_idx
                ].data.sensors.speed_of_sound_mps.user.data[0]
                self.ed_user_sos.setText("{:4.2f}".format(user_sos) * self.units["V"])
                self.combo_temp_source.setCurrentIndex(1)
            else:
                self.combo_sos_source.setCurrentIndex(0)
                self.ed_user_sos.setText("")
                self.ed_user_sos.setEnabled(False)

        if self.rb_f.isChecked():
            self.txt_user_temp.setText("User Temperature (deg F)")
        else:
            self.txt_user_temp.setText("User Temperature (deg C)")

        # Display comments and messages in messages tab
        self.tempsal_comments_messages()

    def tempsal_comments_messages(self):
        """Displays comments and messages associated with temperature,
        salinity, and speed of sound in Messages tab.
        """

        # Clear comments and messages
        self.display_tempsal_comments.clear()

        if self.meas is not None:
            # Display each comment on a new line
            self.display_tempsal_comments.moveCursor(QtGui.QTextCursor.Start)
            for comment in self.meas.comments:
                self.display_tempsal_comments.textCursor().insertText(comment)
                self.display_tempsal_comments.moveCursor(QtGui.QTextCursor.End)
                self.display_tempsal_comments.textCursor().insertBlock()

            # Display each message on a new line
            self.messages_table(self.table_tempsal_messages, ["temperature"])

        self.update_tab_icons()

    def tempsal_plot_temperature(self):
        """Generates the graph of temperature."""

        if self.tts_canvas is None:
            # Create the canvas
            self.tts_canvas = MplCanvas(
                self.graph_temperature, width=4, height=2, dpi=80
            )
            # Assign layout to widget to allow auto scaling
            layout = QtWidgets.QVBoxLayout(self.graph_temperature)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(1, 1, 1, 1)
            # Add the canvas
            layout.addWidget(self.tts_canvas)
            # Initialize hidden toolbar for use by graphics controls
            self.tts_toolbar = NavigationToolbar(self.tts_canvas, self)
            self.tts_toolbar.hide()

        # Initialize the temperature figure and assign to the canvas
        self.tts_fig = Graphics(canvas=self.tts_canvas)
        # Create the figure with the specified data
        if self.rb_f.isChecked():
            temp_units = "F"
        else:
            temp_units = "C"
        self.tts_fig.temperature_ts_graph(
            meas=self.meas, temp_units=temp_units, idx=self.vertical_idx
        )

        # Draw canvas
        self.tts_canvas.draw()

    def tempsal_table_clicked(self, row):
        """Changes selected vertical with tempsal table clicked and updates
            display.

        Parameters
        ----------
        row: int
            Index of row clicked
        """

        if self.valid_row(row):
            self.highlight_row(self.table_tempsal, row)
            self.tempsal_update(
                old_discharge=self.meas.summary, new_discharge=self.meas.summary
            )
            self.tempsal_plot_temperature()
            self.change = True

    def tempsal_change_temp_units(self):
        """Updates the display when the user changes the temperature units.
        Note: changing the units does not
        change the actual data only the units used to display the data.
        """

        self.tempsal_update(
            old_discharge=self.meas.summary, new_discharge=self.meas.summary
        )

        self.tempsal_plot_temperature()

    def tempsal_user_temp_changed(self):
        """Applies a user entered value for the independent temperature.
        This change does not affect the measured
        discharge but could change the automatic QA/QC messages.
        """

        # Set cursor focus onto the table to avoid multiple calls the the
        # adcp_temp_changed function
        self.table_tempsal.setFocus()

        # If data has been entered, convert the data to Celsius if necessary
        if len(self.ed_user_temp.text()) > 0:
            temp = float(self.ed_user_temp.text())
            if self.rb_f.isChecked():
                temp = convert_temperature(temp, "F", "C")
            self.label_independent.setStyleSheet(
                "background: white; font: 12pt MS Shell Dlg 2"
            )
        else:
            temp = np.nan

        # Update the measurement with the new ADCP temperature
        self.meas.temp_chk["user"] = temp

        # Apply qa checks
        self.meas.qa.temperature_qa(self.meas)
        self.meas.qa.check_tempsal_settings(self.meas)

        # Update GUI
        try:
            if np.isnan(self.meas.temp_chk["user"]):
                self.ed_user_temp.setText("")
                self.label_independent.setStyleSheet(
                    "background: #ffcc00; "
                    "font: 12pt MS Shell "
                    "Dlg 2;QToolTip{font: "
                    "12pt}"
                )
                self.label_independent.setToolTip(
                    self.tr("No user supplied temperature.")
                )
            else:
                temp = float(self.meas.temp_chk["user"])
                if self.rb_f.isChecked():
                    temp = convert_temperature(
                        self.meas.temp_chk["user"], units_in="C", units_out="F"
                    )
                self.ed_user_temp.setText("{:3.1f}".format(temp))
                self.label_independent.setStyleSheet(
                    "background: white; font: 11pt MS Shell Dlg 2"
                )
        except (ValueError, TypeError):
            self.ed_user_temp.setText("")
        self.tempsal_comments_messages()
        self.change = True

    def tempsal_adcp_temp_changed(self):
        """Enables the apply button if the user enters a valid value in the
        ADCP temp box.

        NOTE: Appling a user entered value for the ADCP temperature does not
        affect the measured discharge but could change the automatic QA/QC messages.
        """

        # Set cursor focus onto the table to avoid multiple calls the the
        # adcp_temp_changed funtion
        self.table_tempsal.setFocus()

        # If data has been entered, convert the data to Celsius if necessary
        if len(self.ed_adcp_temp.text()) > 0:
            temp = float(self.ed_adcp_temp.text())
            if self.rb_f.isChecked():
                temp = convert_temperature(temp, "F", "C")
        else:
            temp = np.nan

        # Update the measurement with the new ADCP temperature
        self.meas.temp_chk["adcp"] = temp

        # Apply qa checks
        self.meas.qa.temperature_qa(self.meas)
        self.meas.qa.check_tempsal_settings(self.meas)

        # Update GUI
        self.tempsal_comments_messages()
        self.change = True

    # Tempsal User Changes to Data
    # ----------------------------
    def tempsal_temp_source_change(self, apply_all=False):
        """Changes the temperature source and associated sos for selected
        vertical(s).
        """

        with self.wait_cursor():
            # Block signals
            self.combo_temp_source.blockSignals(True)
            self.ed_user_temp_2.blockSignals(True)

            # Assign data based on change made by user
            old_discharge = self.meas.summary
            t_source = self.combo_temp_source.currentText()
            if t_source == "Internal":
                t_source = "internal"
                user_temp = None
                self.change = True
                self.ed_user_temp_2.setText("")

            elif t_source == "User":
                t_source = "user"
                # Check for valid input
                user_temp = self.check_numeric_input(self.ed_user_temp_2, block=False)
                if user_temp is not None:
                    # Convert to deg C if necessary
                    if self.rb_f.isChecked():
                        user_temp = convert_temperature(
                            user_temp, units_in="F", units_out="C"
                        )
                    self.change = True
                else:
                    self.ed_user_temp_2.setText("")

            if self.change:
                if apply_all:
                    # Apply change
                    self.meas.change_sos(
                        parameter="temperatureSrc",
                        temperature=user_temp,
                        selected=t_source,
                    )
                else:
                    # Apply change
                    self.meas.change_sos(
                        vertical_idx=self.vertical_idx,
                        parameter="temperatureSrc",
                        temperature=user_temp,
                        selected=t_source,
                    )
                # Update the tempsal tab
                self.meas.qa.check_tempsal_settings(self.meas)
                self.tempsal_update(
                    old_discharge=old_discharge, new_discharge=self.meas.summary
                )
                self.combo_temp_source.blockSignals(False)
                self.ed_user_temp_2.blockSignals(False)
                self.pb_temp_source_apply.setEnabled(False)
                self.pb_temp_source_apply_all.setEnabled(False)

    def tempsal_salinity_change(self, apply_all):
        """Applies change to salinity and resulting change in sos to
        selected vertical.
        """

        with self.wait_cursor():
            # Assign data based on change made by user
            old_discharge = self.meas.summary
            salinity = self.check_numeric_input(self.ed_salinity, block=False)
            if salinity is not None:
                if apply_all:
                    # Apply change
                    self.meas.change_sos(parameter="salinity", salinity=salinity)
                else:
                    # Apply change
                    self.meas.change_sos(
                        vertical_idx=self.vertical_idx,
                        parameter="salinity",
                        salinity=salinity,
                    )
                # Update the tempsal tab
                self.meas.qa.check_tempsal_settings(self.meas)
                self.tempsal_update(
                    old_discharge=old_discharge, new_discharge=self.meas.summary
                )
                self.change = True
                self.pb_salinity_apply.setEnabled(False)
                self.pb_salinity_apply_all.setEnabled(False)
            else:
                self.ed_salinity.setText("")

    def tempsal_sos_source_change(self, apply_all):
        """Changes the speed of sound for the selected vertical."""

        with self.wait_cursor():
            # Block signals
            self.combo_sos_source.blockSignals(True)
            self.ed_user_sos.blockSignals(True)

            # Get discharge prior to change
            old_discharge = self.meas.summary

            # Get parameters to be changed
            if self.combo_sos_source.currentText() == "Internal":
                sos_source = "internal"
                user_sos = None
                self.ed_user_sos.setText("")
                self.ed_user_sos.setEnabled(False)
                self.change = True

            elif self.combo_sos_source.currentText() == "User":
                sos_source = "user"
                user_sos = self.check_numeric_input(self.ed_user_sos, block=False)
                if user_sos is not None:
                    user_sos = user_sos / self.units["V"]
                    self.change = True

            # Apply change
            if self.change:
                if apply_all:
                    # Apply change to all
                    self.meas.change_sos(
                        parameter="sosSrc", speed=user_sos, selected=sos_source
                    )
                else:
                    # Apply change to selected vertical
                    self.meas.change_sos(
                        vertical_idx=self.vertical_idx,
                        parameter="sosSrc",
                        speed=user_sos,
                        selected=sos_source,
                    )

                # Update the tempsal tab
                self.meas.qa.check_tempsal_settings(self.meas)
                self.tempsal_update(
                    old_discharge=old_discharge, new_discharge=self.meas.summary
                )
                self.change = True

                # Unblock signals and disable apply buttons
                self.combo_sos_source.blockSignals(False)
                self.ed_user_sos.blockSignals(False)
                self.pb_sos_source_apply.setEnabled(False)
                self.pb_sos_source_apply_all.setEnabled(False)

    # Stations Tab
    # ============
    def stations_update_tab(self):
        """Initializes the stations tab and updates the values, tables,
        and graphics.
        """

        # Initialize the connections
        if not self.stations_initialized:
            # Table Clicked
            self.stations_table.cellClicked.connect(self.stations_table_clicked)

            # Location
            self.stations_location_grp = EdGrp(
                ed=self.ed_sta_location,
                pb_apply=self.pb_location_apply,
                pb_apply_all=self.pb_location_apply,
                change=self.stations_location_change,
            )
            self.ed_sta_location.textChanged.connect(
                self.stations_location_grp.enable_apply
            )
            self.pb_location_apply.clicked.connect(self.stations_location_grp.apply)

            # Stage
            self.stations_stage_grp = EdGrp(
                ed=self.ed_sta_stage,
                pb_apply=self.pb_stage_apply,
                pb_apply_all=self.pb_stage_apply,
                change=self.stations_stage_time_change,
            )
            self.ed_sta_stage.textChanged.connect(self.stations_stage_grp.enable_apply)
            self.ed_sta_stage_time.textChanged.connect(self.stations_time_enable_apply)
            self.pb_stage_apply.clicked.connect(self.stations_stage_grp.apply)

            # Manual Station
            self.pb_man_insert.clicked.connect(self.stations_insert_vertical)
            self.pb_man_edit.clicked.connect(self.stations_edit_vertical)

            # Water Surface Condition
            self.stations_ws_condition_grp = ComboGrp(
                combo=self.combo_ws_cond,
                pb_apply=self.pb_ws_cond_apply,
                pb_apply_all=self.pb_ws_cond_apply_all,
                change=self.stations_ws_condition_change,
            )
            self.combo_ws_cond.activated.connect(
                self.stations_ws_condition_grp.enable_apply
            )
            self.pb_ws_cond_apply.clicked.connect(self.stations_ws_condition_grp.apply)
            self.pb_ws_cond_apply_all.clicked.connect(
                self.stations_ws_condition_grp.apply_all
            )

            # Velocity Method
            self.stations_vel_method_grp = ComboGrp(
                combo=self.combo_vel_method,
                pb_apply=self.pb_vel_method_apply,
                pb_apply_all=self.pb_vel_method_apply_all,
                change=self.stations_vel_method_change,
            )
            self.combo_vel_method.activated.connect(
                self.stations_vel_method_grp.enable_apply
            )
            self.pb_vel_method_apply.clicked.connect(self.stations_vel_method_grp.apply)
            self.pb_vel_method_apply_all.clicked.connect(
                self.stations_vel_method_grp.apply_all
            )

            # Velocity Reference
            self.stations_vel_ref_grp = ComboGrp(
                combo=self.combo_vel_ref,
                pb_apply=self.pb_vel_ref_apply,
                pb_apply_all=self.pb_vel_ref_apply_all,
                change=self.stations_vel_ref_change,
            )
            self.combo_vel_ref.activated.connect(self.stations_vel_ref_grp.enable_apply)
            self.pb_vel_ref_apply.clicked.connect(self.stations_vel_ref_grp.apply)
            self.pb_vel_ref_apply_all.clicked.connect(
                self.stations_vel_ref_grp.apply_all
            )

            self.stations_initialized = True

        # Disable apply buttons
        self.pb_location_apply.setEnabled(False)
        self.pb_stage_apply.setEnabled(False)
        self.pb_vel_ref_apply.setEnabled(False)
        self.pb_vel_ref_apply_all.setEnabled(False)
        self.pb_vel_method_apply.setEnabled(False)
        self.pb_vel_method_apply_all.setEnabled(False)
        self.pb_ws_cond_apply.setEnabled(False)
        self.pb_ws_cond_apply_all.setEnabled(False)

        # Update editable fields
        self.ed_sta_location.blockSignals(True)
        self.ed_sta_stage.blockSignals(True)
        self.ed_sta_stage_time.blockSignals(True)
        self.combo_ws_cond.blockSignals(True)
        self.combo_vel_ref.blockSignals(True)
        self.combo_vel_method.blockSignals(True)

        vertical = self.meas.verticals[
            self.meas.verticals_sorted_idx[self.stations_row]
        ]

        # Change selected vertical to a used vertical
        if not vertical.use:
            new_idx = self.vertical_idx + 1
            while not new_idx in self.meas.verticals_used_idx:
                new_idx = new_idx + 1
                if new_idx > max(self.meas.verticals_used_idx):
                    new_idx = self.meas.verticals_used_idx[1]
            self.vertical_idx = new_idx
            self.stations_row = \
            np.where(self.meas.verticals_sorted_idx == self.vertical_idx)[0][0]
            
        if vertical.velocity_method == "Manual" and vertical.depth_source == "Manual":
            self.pb_man_edit.setEnabled(True)
        else:
            self.pb_man_edit.setEnabled(False)

        # Get the vertical index for the stations tab data, which includes
        # all verticals
        vert_idx = self.vertical_idx

        # Set units
        self.gb_location.setTitle(self.tr("Location " + self.units["label_L"]))
        self.gb_stage.setTitle(self.tr("Stage " + self.units["label_L"]))

        # Update the location data
        self.ed_sta_location.setText(
            "{:6.2f}".format(self.meas.verticals[vert_idx].location_m * self.units["L"])
        )

        # WS Condition
        if self.meas.verticals[vert_idx].ws_condition == "":
            self.combo_ws_cond.setEnabled(False)
        elif self.meas.verticals[vert_idx].ws_condition == "Open":
            self.combo_ws_cond.setEnabled(True)
            self.combo_ws_cond.setCurrentIndex(0)
        else:
            self.combo_ws_cond.setEnabled(True)
            self.combo_ws_cond.setCurrentIndex(1)

        # Velocity Method
        if self.meas.verticals[vert_idx].velocity_method == "Manual":
            self.combo_vel_method.setEnabled(False)
        else:
            self.combo_vel_method.setEnabled(True)
            vel_method_dict = {"Fixed": 0, "Azimuth": 1, "Magnitude": 2}
            self.combo_vel_method.setCurrentIndex(
                vel_method_dict[self.meas.verticals[vert_idx].velocity_method]
            )

        # Velocity Reference
        if self.meas.verticals[vert_idx].velocity_reference == "":
            self.combo_vel_ref.setEnabled(False)
        elif self.meas.verticals[vert_idx].velocity_reference == "BT":
            self.combo_vel_ref.setEnabled(True)
            self.combo_vel_ref.setCurrentIndex(1)
        elif self.meas.verticals[vert_idx].velocity_reference == "bt_vel":
            self.combo_vel_ref.setEnabled(True)
            self.combo_vel_ref.setCurrentIndex(1)
        else:
            self.combo_vel_ref.setEnabled(True)
            self.combo_vel_ref.setCurrentIndex(0)

        # Stage
        gh_m = self.meas.verticals[vert_idx].gh_m
        if np.isnan(gh_m):
            self.ed_sta_stage.setText("")
        else:
            self.ed_sta_stage.setText("{:3.3f}".format(gh_m * self.units["L"]))

        # Stage time
        self.ed_sta_stage_time.setText(self.meas.verticals[vert_idx].gh_obs_time)

        # Hide options that do not apply to Manual verticals
        if self.meas.verticals[vert_idx].mean_speed_source == "Manual":
            self.combo_vel_method.hide()
            self.pb_vel_method_apply.hide()
            self.pb_vel_method_apply_all.hide()
            self.combo_vel_ref.hide()
            self.pb_vel_ref_apply.hide()
            self.pb_vel_ref_apply_all.hide()
        else:
            self.combo_vel_method.show()
            self.pb_vel_method_apply.show()
            self.pb_vel_method_apply_all.show()
            self.combo_vel_ref.show()
            self.pb_vel_ref_apply.show()
            self.pb_vel_ref_apply_all.show()

        # Unblock signals
        self.ed_sta_location.blockSignals(False)
        self.ed_sta_stage.blockSignals(False)
        self.ed_sta_stage_time.blockSignals(False)
        self.combo_ws_cond.blockSignals(False)
        self.combo_vel_ref.blockSignals(False)
        self.combo_vel_method.blockSignals(False)

        # Update table and graphics
        self.stations_update_table()
        self.stations_update_graphs()

        self.stations_comments_messages()

        # Setup list for use by graphics controls
        self.canvases = [
            self.stations_percent_discharge_canvas,
            self.stations_velocity_canvas,
        ]
        self.figs = [self.stations_percent_discharge_fig, self.stations_velocity_fig]
        self.toolbars = [
            self.stations_percent_discharge_toolbar,
            self.stations_velocity_toolbar,
        ]
        self.fig_calls = [
            self.stations_graphics_percent_discharge,
            self.stations_graphics_velocity,
        ]
        self.ui_parents = [i.parent() for i in self.canvases]
        self.figs_menu_connection()
        self.update_toolbar_info()

    def stations_update_table(self):
        """Updates the header and data in the stations table."""

        # Set table
        tbl = self.stations_table
        tbl.clear()

        # Define header
        summary_header = [
            self.tr("Use"),
            self.tr("Station" + "\n" + "Number"),
            self.tr("Location" + "\n") + self.units["label_L"],
            self.tr("Start \n Time"),
            self.tr("Condition"),
            self.tr("Velocity" + "\n" + "Method"),
            self.tr("Velocity" + "\n" + "Reference"),
            self.tr("Stage" + "\n") + self.units["label_L"],
            self.tr("Stage" + "\n" + "Time"),
            self.tr("Depth" + "\n") + self.units["label_L"],
            self.tr("Normal" + "\n" + "Velocity" + "\n") + self.units["label_V"],
            self.tr("Duration" + "\n") + self.tr("(s)"),
            self.tr("Number" + "\n" + "Valid" + "\n" + "Ens."),
            self.tr("Number" + "\n" + "Valid" + "\n" + "Cells"),
            self.tr("Depth" + "\n" + "Number" + "\n" + "Valid"),
            self.tr("WT %" + "\n" + "Valid"),
        ]

        # Setup table
        ncols = len(summary_header)
        nrows = len(self.meas.verticals)
        tbl.setRowCount(nrows)
        tbl.setColumnCount(ncols)
        tbl.setFont(self.font_normal)
        tbl.setHorizontalHeaderLabels(summary_header)
        tbl.horizontalHeader().setFont(self.font_bold)
        tbl.verticalHeader().hide()
        tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)

        # Initialize counter
        sta = 0

        for row, idx in enumerate(self.meas.verticals_sorted_idx):
            # Get vertical
            vertical = self.meas.verticals[idx]

            # Only verticals that are used for discharge computations are
            # given a station number
            if vertical.use:
                sta += 1
                station = sta
            else:
                station = np.nan

            # User, checkable cells
            col = 0
            item = QtWidgets.QTableWidgetItem(" ")
            item.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
            tbl.setItem(row, col, item)

            if vertical.use:
                tbl.item(row, col).setCheckState(QtCore.Qt.Checked)
            else:
                tbl.item(row, col).setCheckState(QtCore.Qt.Unchecked)

            # Station Number
            col += 1
            if np.isnan(station):
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
            else:
                tbl.setItem(
                    row, col, QtWidgets.QTableWidgetItem("{:3.0f}".format(station))
                )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
            if vertical.velocity_method == "Manual":
                tbl.item(row, col).setForeground(QtGui.QColor(230, 138, 0))
                tbl.item(row, col).setFont(self.font_bold)

            # Location
            col += 1
            if np.isnan(vertical.location_m):
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            else:
                tbl.setItem(
                    row,
                    col,
                    QtWidgets.QTableWidgetItem(
                        "{:6.2f}".format(vertical.location_m * self.units["L"])
                    ),
                )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
            if vertical.velocity_method == "Manual":
                tbl.item(row, col).setForeground(QtGui.QColor(230, 138, 0))
                tbl.item(row, col).setFont(self.font_bold)

            # Start time
            col += 1
            tbl.setItem(row, col, QtWidgets.QTableWidgetItem(vertical.time_start[0:8]))
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Condition
            col += 1
            tbl.setItem(row, col, QtWidgets.QTableWidgetItem(vertical.ws_condition))
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
            if vertical.velocity_method == "Manual":
                tbl.item(row, col).setForeground(QtGui.QColor(230, 138, 0))
                tbl.item(row, col).setFont(self.font_bold)

            # Velocity Method
            col += 1
            tbl.setItem(row, col, QtWidgets.QTableWidgetItem(vertical.velocity_method))
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
            if vertical.velocity_method == "Manual":
                tbl.item(row, col).setForeground(QtGui.QColor(230, 138, 0))
                tbl.item(row, col).setFont(self.font_bold)

            # Velocity Reference
            col += 1
            tbl.setItem(
                row, col, QtWidgets.QTableWidgetItem(vertical.velocity_reference)
            )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Stage
            col += 1
            if np.isnan(vertical.gh_m):
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            else:
                tbl.setItem(
                    row,
                    col,
                    QtWidgets.QTableWidgetItem(
                        "{:3.3f}".format(vertical.gh_m * self.units["L"])
                    ),
                )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
            if vertical.velocity_method == "Manual":
                tbl.item(row, col).setForeground(QtGui.QColor(230, 138, 0))
                tbl.item(row, col).setFont(self.font_bold)

            # Stage time
            col += 1
            tbl.setItem(row, col, QtWidgets.QTableWidgetItem(vertical.gh_obs_time))
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
            if vertical.velocity_method == "Manual":
                tbl.item(row, col).setForeground(QtGui.QColor(230, 138, 0))
                tbl.item(row, col).setFont(self.font_bold)

            # Depth
            col += 1
            if np.isnan(vertical.depth_m):
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            else:
                tbl.setItem(
                    row,
                    col,
                    QtWidgets.QTableWidgetItem(
                        "{:5.3f}".format(vertical.depth_m * self.units["L"])
                    ),
                )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
            if vertical.velocity_method == "Manual":
                tbl.item(row, col).setForeground(QtGui.QColor(230, 138, 0))
                tbl.item(row, col).setFont(self.font_bold)

            # Mean Velocity
            col += 1
            if np.isnan(vertical.mean_speed_mps):
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            else:
                tbl.setItem(
                    row,
                    col,
                    QtWidgets.QTableWidgetItem(
                        "{:4.3f}".format(vertical.mean_speed_mps * self.units["V"])
                    ),
                )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
            if vertical.velocity_method == "Manual":
                tbl.item(row, col).setForeground(QtGui.QColor(230, 138, 0))
                tbl.item(row, col).setFont(self.font_bold)

            try:
                if station in self.meas.qa.w_vel["all_invalid"]["All: "]:
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                elif station in self.meas.qa.w_vel["total_warning"]["All: "]:
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                elif station in self.meas.qa.w_vel["total_caution"]["All: "]:
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                elif tbl.item(row, col) is not None:
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))

                if tbl.item(row, col) is not None:
                    tbl.item(row, col).setToolTip(
                        self.tr(self.wt_create_tooltip(row, col))
                    )
            except KeyError:
                pass

            # Duration
            col += 1
            if np.isnan(vertical.sampling_duration_sec):
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            else:
                tbl.setItem(
                    row,
                    col,
                    QtWidgets.QTableWidgetItem(
                        "{:4.1f}".format(vertical.sampling_duration_sec)
                    ),
                )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
            if vertical.velocity_method == "Manual":
                tbl.item(row, col).setForeground(QtGui.QColor(230, 138, 0))
                tbl.item(row, col).setFont(self.font_bold)

            # Number Valid Ensembles
            col += 1
            if np.isnan(vertical.number_of_valid_ensembles):
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            else:
                tbl.setItem(
                    row,
                    col,
                    QtWidgets.QTableWidgetItem(
                        "{:4.0f}".format(vertical.number_of_valid_ensembles)
                    ),
                )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Try except used to handle situation of unused verticals which have no station number
            try:
                if vertical.velocity_method == "Manual":
                    tbl.item(row, col).setForeground(QtGui.QColor(230, 138, 0))
                    tbl.item(row, col).setFont(self.font_bold)
                elif station in self.meas.qa.verticals["valid_warning"]:
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                elif station in self.meas.qa.verticals["valid_caution"]:
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                else:
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))
                if tbl.item(row, col) is not None:
                    tbl.item(row, col).setToolTip(
                        self.tr(self.wt_create_tooltip(row, col))
                    )
            except KeyError:
                pass

            # Number Valid Cells
            col += 1
            if np.isnan(vertical.number_valid_cells):
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            else:
                tbl.setItem(
                    row,
                    col,
                    QtWidgets.QTableWidgetItem(
                        "{:4.0f}".format(vertical.number_valid_cells)
                    ),
                )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
            if vertical.velocity_method == "Manual":
                tbl.item(row, col).setForeground(QtGui.QColor(230, 138, 0))
                tbl.item(row, col).setFont(self.font_bold)

            # BT Number Valid
            # col += 1
            # if vertical.sampling_duration_sec == 0:
            #     tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            # else:
            #     ens_bt = np.nansum(vertical.data.boat_vel.bt_vel.valid_data[0, :])
            #     tbl.setItem(
            #         row, col, QtWidgets.QTableWidgetItem("{:4.0f}".format(ens_bt))
            #     )
            # tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Depth Number Valid
            col += 1
            if vertical.sampling_duration_sec == 0:
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            else:
                depths = getattr(vertical.data.depths, vertical.data.depths.selected)
                ens_depth = np.nansum(depths.valid_data)
                tbl.setItem(
                    row, col, QtWidgets.QTableWidgetItem("{:4.0f}".format(ens_depth))
                )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # WT % Invalid
            col += 1
            if vertical.sampling_duration_sec == 0:
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            else:
                valid_data = vertical.data.w_vel.valid_data
                useable_cells = vertical.data.w_vel.valid_data[6, :, :]
                num_useable_cells = np.nansum(np.nansum(useable_cells.astype(int)))
                num_valid = np.nansum(np.nansum(valid_data[0, :, :][useable_cells]))
                item = "{:3.2f}".format((num_valid / num_useable_cells) * 100.0)
                if item == "nan":
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
            try:
                if station in self.meas.qa.w_vel["all_invalid"]["All: "]:
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                elif station in self.meas.qa.w_vel["total_warning"]["All: "]:
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                elif station in self.meas.qa.w_vel["total_caution"]["All: "]:
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                elif tbl.item(row, col) is not None:
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))

                if tbl.item(row, col) is not None:
                    tbl.item(row, col).setToolTip(
                        self.tr(self.wt_create_tooltip(row, col))
                    )
            except KeyError:
                pass
        # Highlight selected row
        self.highlight_row(tbl, self.stations_row)

        tbl.resizeRowsToContents()
        tbl.resizeColumnsToContents()

    def stations_create_tooltip(self, row, column):
        """Create a tooltip for the row and column in the table based on the
        QA results.

                Parameters
                ----------
                row: int
                    Row in table
                column: int
                    Column in table

                Returns
                -------
                tt: str
                    Text of tooltip
        """

        # Initialize variables
        cat = None
        tt = ""

        # From column determine category of QA results to use
        if column == 11:
            qa_data = {
                "all_invalid": self.meas.qa.verticals["no_valid_profile"],
                "total_warning": self.meas.qa.verticals["valid_warning"],
                "total_caution": self.meas.qa.verticals["valid_caution"],
            }
            tt = "".join(
                self.tooltip_qa_message(
                    qa_data=qa_data,
                    cat=cat,
                    vertical_id=int(self.meas.summary["Station Number"][row]),
                    total_threshold_warning=self.meas.qa.percent_valid_warning_threshold,
                    total_threshold_caution=self.meas.qa.percent_valid_caution_threshold,
                )
            )
        return tt

    def stations_comments_messages(self):
        """Displays comments and messages associated with stations in
        Messages tab.
        """

        # Clear comments and messages
        self.display_sta_comments.clear()

        if self.meas is not None:
            # Display each comment on a new line
            self.display_sta_comments.moveCursor(QtGui.QTextCursor.Start)
            for comment in self.meas.comments:
                self.display_sta_comments.textCursor().insertText(comment)
                self.display_sta_comments.moveCursor(QtGui.QTextCursor.End)
                self.display_sta_comments.textCursor().insertBlock()

            # Display each message on a new line
            self.messages_table(self.table_sta_messages, ["stations", "verticals"])
            self.update_tab_icons()

    def stations_update_graphs(self):
        """Update stations tab graphics"""
        self.stations_graphics_percent_discharge()
        self.stations_graphics_velocity()

    def stations_graphics_percent_discharge(self):
        """Creates bar graph of percent discharge for each vertical or
        subsection.
        """

        if self.stations_percent_discharge_canvas is None:
            # Create the canvas
            self.stations_percent_discharge_canvas = MplCanvas(
                parent=self.sta_graph_q, width=4, height=2, dpi=80
            )
            # Assign layout to widget to allow auto scaling
            layout = QtWidgets.QVBoxLayout(self.sta_graph_q)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(1, 1, 1, 1)
            # Add the canvas
            layout.addWidget(self.stations_percent_discharge_canvas)
            # Initialize hidden toolbar for use by graphics controls
            self.stations_percent_discharge_toolbar = NavigationToolbar(
                self.stations_percent_discharge_canvas, self
            )
            self.stations_percent_discharge_toolbar.hide()

        # Initialize the figure and assign to the canvas
        self.stations_percent_discharge_fig = Graphics(
            canvas=self.stations_percent_discharge_canvas
        )

        # Create the figure with the specified data
        if self.meas.verticals[self.meas.verticals_sorted_idx[self.stations_row]].use:
            self.stations_percent_discharge_fig.percent_q_bar(
                meas=self.meas,
                units=self.units,
                color_dict=self.plt_color_dict,
                selected=self.vertical_idx,
                agency_options=self.agency_options,
            )
        else:
            # If selected row is not a used vertical no highlighting of
            # vertical in graph
            self.stations_percent_discharge_fig.percent_q_bar(
                meas=self.meas,
                units=self.units,
                color_dict=self.plt_color_dict,
                selected=None,
                agency_options=self.agency_options,
            )
        # Initialize the ability to click on graph to select vertical
        self.stations_percent_discharge_fig.select_vertical(parent=self)

        # Draw canvas
        self.stations_percent_discharge_canvas.draw()

    def stations_graphics_velocity(self):
        """Creates the velocity quiver and color contour graphs."""

        if self.stations_velocity_canvas is None:
            # Create the canvas
            self.stations_velocity_canvas = MplCanvas(
                parent=self.sta_graph_vel, width=8, height=4, dpi=80
            )
            # Assign layout to widget to allow auto scaling
            layout = QtWidgets.QVBoxLayout(self.sta_graph_vel)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(1, 1, 1, 1)
            # Add the canvas
            layout.addWidget(self.stations_velocity_canvas)
            # Initialize hidden toolbar for use by graphics controls
            self.stations_velocity_toolbar = NavigationToolbar(
                self.stations_velocity_canvas, self
            )
            self.stations_velocity_toolbar.hide()

        # Initialize the figure and assign to the canvas
        self.stations_velocity_fig = Graphics(canvas=self.stations_velocity_canvas)

        # Create the figure with the specified data
        if self.meas.verticals[self.meas.verticals_sorted_idx[self.stations_row]].use:
            self.stations_velocity_fig.main_vel_graphs(
                meas=self.meas,
                selected=self.vertical_idx,
                color_map=self.color_map,
                units=self.units,
            )
        else:
            # If selected row is not a used vertical no highlighting of
            # vertical in graph
            self.stations_velocity_fig.main_vel_graphs(
                meas=self.meas,
                selected=None,
                color_map=self.color_map,
                units=self.units,
            )

        # Initialize the ability to click on graph to select vertical
        self.stations_velocity_fig.select_vertical(parent=self)

        # Draw canvas
        self.stations_velocity_canvas.draw()

    def stations_table_clicked(self, row, col):
        """Handles left clicks on stations table.

        Parameters
        ----------
        row: int
            Row clicked
        col: int
            Column clicked
        """

        # Block signals
        self.stations_table.blockSignals(True)

        if 0 < row < self.stations_table.rowCount() - 1:
            # Column zero used to mark verticals for use
            if col == 0:
                self.stations_change_vertical_use(row)

            # Clicking any other column simply changes the selected vertical
            # for display and editing
            else:
                self.stations_row = row
                if self.meas.verticals[self.meas.verticals_sorted_idx[row]].use:
                    self.vertical_idx = self.meas.verticals_sorted_idx[row]
                    self.main_row_idx = (
                        self.meas.summary.index[
                            self.meas.summary["Station Number"]
                            == int(self.stations_table.item(row, 1).text())
                        ].tolist()[0]
                        + 1
                    )
                # Update tab
                self.stations_update_tab()
                self.change = True

        # Unblock signals
        self.stations_table.blockSignals(False)
        self.stations_data_tab.setFocus()

    def stations_change_vertical_use(self, row):
        """Changes the status of a vertical as to whether it should be used
        in discharge compuations.

        Parameters
        ----------
        row: int
            Row selected
        """

        # Block signals
        self.stations_table.blockSignals(True)

        # Request justification from user
        self.add_comment()

        # Avoid duplicate locations by turning off all locations equal to selected
        if not self.meas.verticals[self.meas.verticals_sorted_idx[row]].use:
            loc_use = np.round(
                self.meas.verticals[self.meas.verticals_sorted_idx[row]].location_m, 4
            )
            for idx in self.meas.verticals_used_idx:
                if loc_use == self.meas.verticals[idx].location_m:
                    self.meas.toggle_vertical_use(idx=idx)
        # Toggle vertical
        self.meas.toggle_vertical_use(idx=self.meas.verticals_sorted_idx[row])

        # Update gui
        self.change = True
        self.stations_update_tab()

        # Unblock signals
        self.stations_table.blockSignals(False)

    def stations_location_change(self, **_):
        """Activates the location apply button if valid data are entered."""

        # Block signals
        self.ed_sta_location.blockSignals(True)

        # Enable apply
        self.pb_location_apply.setEnabled(True)
        self.pb_location_apply.blockSignals(True)

        # Get start and end locations
        loc_min_max = [
            self.meas.verticals[self.meas.verticals_used_idx[0]].location_m
            * self.units["L"],
            self.meas.verticals[self.meas.verticals_used_idx[-1]].location_m
            * self.units["L"],
        ]

        # Check for valid value and change
        value = self.check_numeric_input(self.ed_sta_location, block=True, min_value=0)
        if loc_min_max[0] < value < loc_min_max[-1]:
            if value is not None:
                self.meas.change_vertical_location(
                    idx=self.meas.verticals_sorted_idx[self.stations_row],
                    new_location=value / self.units["L"],
                )

            # Update gui
            self.stations_row = np.where(
                self.meas.verticals_sorted_idx == self.vertical_idx
            )[0][0]
            self.change = True
            self.stations_update_tab()
        else:
            self.popup_message(
                "New location must be between start ({:6.2f}) and end ({:6.2f}) ".format(
                    loc_min_max[0], loc_min_max[1]
                )
            )
            self.ed_sta_location.setText(
                "{:6.2f}".format(
                    self.meas.verticals[self.vertical_idx].location_m * self.units["L"]
                )
            )
        # Unblock signals
        self.ed_sta_location.blockSignals(False)
        self.pb_location_apply.blockSignals(False)

    def stations_stage_time_change(self, **_):
        """Applies change in stage or stage time."""

        # Update vertical stage
        stage = self.check_numeric_input(self.ed_sta_stage, block=True)
        stage_time = self.ed_sta_stage_time.text()
        self.meas.update_vertical_stage(
            idx=self.meas.verticals_sorted_idx[self.stations_row],
            stage=stage / self.units["L"],
            stage_time=stage_time,
        )

        # Update gui
        self.change = True
        self.stations_update_tab()

    def stations_time_enable_apply(self):
        """Activates the stage apply button if a valid date is entered."""

        # Check for valid input
        if len(self.ed_sta_stage_time.text()) > 0:
            # If input it should have standard time format
            time_pattern = "^(?:[01]?\d|2[0-3]):[0-5]?\d(?::[0-5]\d)?$"
            if re.match(time_pattern, self.ed_sta_stage_time.text()):
                self.pb_stage_apply.setEnabled(True)
            else:
                self.pb_stage_apply.setEnabled(False)
        else:
            # Blank input is also acceptable
            self.pb_stage_apply.setEnabled(True)

    def stations_ws_condition_change(self, apply_all=False):
        """Change ws condition.

        Parameters
        ----------
        apply_all: bool
            Indicates if change should be applied to selected data or all data.
        """

        condition = self.combo_ws_cond.currentText()
        if apply_all:
            idx = None
        else:
            idx = self.meas.verticals_sorted_idx[self.stations_row]

        self.meas.change_ws_condition(idx=idx, condition=condition)

        # Update gui
        self.change = True
        self.stations_update_tab()

    def stations_vel_method_change(self, apply_all=False):
        """Change velocity method.

        Parameters
        ----------
        apply_all: bool
            Indicates if change should be applied to selected data or all data.
        """

        method = self.combo_vel_method.currentText()
        if apply_all:
            idx = None
        else:
            idx = self.meas.verticals_sorted_idx[self.stations_row]

        result = self.meas.change_vel_method(idx=idx, method=method)
        if len(result) > 0:
            self.popup_message(result)
            if idx == None:
                idx = self.meas.verticals_used_idx[1]
            self.combo_vel_method.setCurrentText(
                self.meas.verticals[idx].velocity_method
            )
            return

        # Update gui
        self.change = True
        self.stations_update_tab()

    def stations_vel_ref_change(self, apply_all=False):
        """Changes velocity reference.

        Parameters
        ----------
        apply_all: bool
            Indicates if change should be applied to selected data or all data.
        """

        ref = self.combo_vel_ref.currentText()

        if apply_all:
            idx = None
        else:
            idx = self.meas.verticals_sorted_idx[self.stations_row]

        self.meas.change_vel_reference(idx=idx, ref=ref)

        # Update gui
        _ = self.display_bt_tab()
        self.change = True
        self.stations_update_tab()

    def stations_insert_vertical(self):
        """Allows the user to enter a manual station anywhere between the
        start and end locations.
        """

        # Get start and end locations
        loc_min_max = [
            self.meas.verticals[self.meas.verticals_used_idx[0]].location_m
            * self.units["L"],
            self.meas.verticals[self.meas.verticals_used_idx[-1]].location_m
            * self.units["L"],
        ]

        all_locations = []
        for vertical in self.meas.verticals:
            all_locations.append(np.round(vertical.location_m * self.units["L"], 2))

        # Use water surface condition from selected vertical
        ws_condition = self.meas.verticals[
            self.meas.verticals_sorted_idx[self.stations_row]
        ].ws_condition

        # Open dialog
        add_vert = InsertVertical(
            units=self.units,
            loc_min_max=loc_min_max,
            all_locations=all_locations,
            ws_condition=ws_condition,
        )
        data_entered = add_vert.exec_()

        # If data entered, process data
        if data_entered:
            # Get data from dialog
            location = self.check_numeric_input(add_vert.ed_man_location, block=False)
            depth = self.check_numeric_input(add_vert.ed_man_depth, block=False)
            vel = self.check_numeric_input(add_vert.ed_man_velocity, block=False)
            if vel is None:
                vel = np.nan
            correction = self.check_numeric_input(
                add_vert.ed_man_correction, block=False
            )
            if correction is None:
                correction = 0.0
            ws_condition = add_vert.combo_man_ws_condition.currentText()
            if ws_condition == "Ice":
                ice_thickness = self.check_numeric_input(
                    add_vert.ed_man_ice_thickness, block=False
                )
                if ice_thickness is None:
                    ice_thickness = 0.0
                ice_bottom = self.check_numeric_input(
                    add_vert.ed_man_ice_bottom, block=False
                )
                if ice_bottom is None:
                    ice_bottom = 0.0
                slush_bottom = self.check_numeric_input(
                    add_vert.ed_man_slush_bottom, block=False
                )
                if slush_bottom is None:
                    slush_bottom = 0.0
            else:
                ice_thickness = 0.0
                ice_bottom = 0.0
                slush_bottom = 0.0

            # Create vertical
            self.meas.insert_manual_vertical(
                location=location / self.units["L"],
                depth=depth / self.units["L"],
                velocity=vel / self.units["V"],
                ws_condition=ws_condition,
                ice_thickness=ice_thickness / self.units["L"],
                ice_bottom=ice_bottom / self.units["L"],
                slush_bottom=slush_bottom / self.units["L"],
                velocity_correction=correction,
                use=True,
            )

            # Save comment
            comment_text = add_vert.ed_sta_comment.toPlainText()
            # Create and add default information
            time_stamp = datetime.now().strftime(self.date_format + " %H:%M:%S")
            user_name = getpass.getuser()
            text = (
                "["
                + "Stations"
                + ", "
                + time_stamp
                + ", "
                + user_name
                + "]:  "
                + comment_text
            )
            self.meas.comments.append(text)
            self.change = True

            # Update selected row
            self.vertical_idx = len(self.meas.verticals) - 1
            self.stations_row = np.where(
                self.meas.verticals_sorted_idx == self.vertical_idx
            )[0][0]

            # Update gui
            self.stations_update_tab()

            self.main_row_idx = (
                self.meas.summary.index[
                    self.meas.summary["Station Number"]
                    == int(self.stations_table.item(self.stations_row, 1).text())
                ].tolist()[0]
                + 1
            )

    def stations_edit_vertical(self):
        # Get start and end locations
        loc_min_max = [
            self.meas.verticals[self.meas.verticals_used_idx[0]].location_m
            * self.units["L"],
            self.meas.verticals[self.meas.verticals_used_idx[-1]].location_m
            * self.units["L"],
        ]

        all_locations = []
        for vertical in self.meas.verticals:
            all_locations.append(np.round(vertical.location_m * self.units["L"], 2))

        vertical = self.meas.verticals[
            self.meas.verticals_sorted_idx[self.stations_row]
        ]

        # Use water surface condition from selected vertical
        ws_condition = self.meas.verticals[
            self.meas.verticals_sorted_idx[self.stations_row]
        ].ws_condition

        # Open dialog
        ed_vert = InsertVertical(
            units=self.units,
            loc_min_max=loc_min_max,
            all_locations=all_locations,
            ws_condition=ws_condition,
            vertical=vertical,
        )

        data_entered = ed_vert.exec_()

        # If data entered, process data
        if data_entered:
            # Get data from dialog
            location = self.check_numeric_input(ed_vert.ed_man_location, block=False)
            depth = self.check_numeric_input(ed_vert.ed_man_depth, block=False)
            velocity_correction = self.check_numeric_input(
                ed_vert.ed_man_correction, block=False
            )
            if velocity_correction == 0:
                velocity = self.check_numeric_input(
                    ed_vert.ed_man_velocity, block=False
                )
                if velocity is None:
                    velocity = np.nan
            else:
                velocity = np.nan
            ws_condition = ed_vert.combo_man_ws_condition.currentText()

            if ws_condition == "Ice":
                ice_thickness = self.check_numeric_input(
                    ed_vert.ed_man_ice_thickness, block=False
                )
                if ice_thickness is None:
                    ice_thickness = 0.0
                ice_bottom = self.check_numeric_input(
                    ed_vert.ed_man_ice_bottom, block=False
                )
                if ice_bottom is None:
                    ice_bottom = 0.0
                slush_bottom = self.check_numeric_input(
                    ed_vert.ed_man_slush_bottom, block=False
                )
                if slush_bottom is None:
                    slush_bottom = 0.0
            else:
                ice_thickness = 0.0
                ice_bottom = 0.0
                slush_bottom = 0.0

            vertical.from_user_input(
                location=location / self.units["L"],
                depth=depth / self.units["L"],
                velocity=velocity / self.units["V"],
                ws_condition=ws_condition,
                ice_thickness_m=ice_thickness / self.units["L"],
                ws_to_ice_bottom_m=ice_bottom / self.units["L"],
                ws_to_slush_bottom_m=slush_bottom / self.units["L"],
                use=True,
                velocity_correction=velocity_correction,
            )

            self.meas.compute_discharge()

            # Save comment
            comment_text = ed_vert.ed_sta_comment.toPlainText()
            # Create and add default information
            time_stamp = datetime.now().strftime(self.date_format + " %H:%M:%S")
            user_name = getpass.getuser()
            text = (
                "["
                + "Stations"
                + ", "
                + time_stamp
                + ", "
                + user_name
                + "]:  "
                + comment_text
            )
            self.meas.comments.append(text)
            self.change = True

            # Update selected row
            self.stations_row = np.where(
                self.meas.verticals_sorted_idx == self.vertical_idx
            )[0][0]

            # Update gui
            self.stations_update_tab()

            self.main_row_idx = (
                self.meas.summary.index[
                    self.meas.summary["Station Number"]
                    == int(self.stations_table.item(self.stations_row, 1).text())
                ].tolist()[0]
                + 1
            )

    # Bottom Track Tab
    # ================
    def bt_update_tab(self, old_discharge=None):
        """Initialize, setup settings, and data display in bottom track tab.

        Parameters
        ----------
        old_discharge: dataframe
            Pandas dataframe of summary data
        """

        # Setup data table
        tbl = self.table_bt
        table_header = [
            self.tr("Station \n Number"),
            self.tr("Location " + self.units["label_L"]),
            self.tr("Number \n Ens."),
            self.tr("Number \n Valid \n Ens."),
            self.tr("Number \n Beam <4"),
            self.tr("Number \n Invalid \n Orig Data "),
            self.tr("Number \n Invalid \n <4 Beam "),
            self.tr("Number \n Invalid \n Error Vel"),
            self.tr("Number \n Invalid \n Vert Vel "),
            self.tr("Discharge \n Previous \n" + self.units["label_Q"]),
            self.tr("Discharge \n Now \n" + self.units["label_Q"]),
            self.tr("Discharge \n % Change"),
        ]
        ncols = len(table_header)
        nrows = self.meas.summary.shape[0] - 2
        tbl.setRowCount(nrows)
        tbl.setColumnCount(ncols)
        tbl.setHorizontalHeaderLabels(table_header)
        tbl.horizontalHeader().setFont(self.font_bold)
        tbl.verticalHeader().hide()
        tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)

        # Automatically resize rows and columns
        tbl.resizeColumnsToContents()
        tbl.resizeRowsToContents()

        # Initialize connections
        if not self.bt_initialized:
            # Table clicked
            tbl.cellClicked.connect(self.bt_table_clicked)

            # 3 Beam Solutions
            self.bt_3beam_grp = ComboGrp(
                combo=self.combo_bt_3beam,
                pb_apply=self.pb_bt_3b_apply,
                pb_apply_all=self.pb_bt_3b_apply_all,
                change=self.bt_beam_change,
            )
            self.combo_bt_3beam.activated.connect(self.bt_3beam_grp.enable_apply)
            self.pb_bt_3b_apply.clicked.connect(self.bt_3beam_grp.apply)
            self.pb_bt_3b_apply_all.clicked.connect(self.bt_3beam_grp.apply_all)

            # Error Velocity
            self.bt_error_vel_grp = ComboEdGrp(
                combo=self.combo_bt_error_velocity,
                ed=self.ed_bt_error_vel_threshold,
                pb_apply=self.pb_bt_error_apply,
                pb_apply_all=self.pb_bt_error_apply_all,
                change=self.bt_error_vel_change,
            )
            self.combo_bt_error_velocity.activated.connect(
                self.bt_error_vel_grp.enable_apply
            )
            self.pb_bt_error_apply.clicked.connect(self.bt_error_vel_grp.apply)
            self.pb_bt_error_apply_all.clicked.connect(self.bt_error_vel_grp.apply_all)
            self.ed_bt_error_vel_threshold.textChanged.connect(
                self.bt_error_vel_grp.enable_apply
            )

            # Vertical Velocity
            self.bt_vert_vel_grp = ComboEdGrp(
                combo=self.combo_bt_vert_velocity,
                ed=self.ed_bt_vert_vel_threshold,
                pb_apply=self.pb_bt_vert_apply,
                pb_apply_all=self.pb_bt_vert_apply_all,
                change=self.bt_vert_vel_change,
            )
            self.combo_bt_vert_velocity.activated.connect(
                self.bt_vert_vel_grp.enable_apply
            )
            self.pb_bt_vert_apply.clicked.connect(self.bt_vert_vel_grp.apply)
            self.pb_bt_vert_apply_all.clicked.connect(self.bt_vert_vel_grp.apply_all)
            self.ed_bt_vert_vel_threshold.textChanged.connect(
                self.bt_vert_vel_grp.enable_apply
            )

            # Connect plot variable checkboxes
            self.cb_bt_vectors.stateChanged.connect(self.bt_plot_change)

            # Connect radio buttons
            self.rb_bt_beam.toggled.connect(self.bt_radiobutton_control)
            self.rb_bt_error.toggled.connect(self.bt_radiobutton_control)
            self.rb_bt_vert.toggled.connect(self.bt_radiobutton_control)
            self.rb_bt_source.toggled.connect(self.bt_radiobutton_control)

            self.bt_initialized = True

        self.pb_bt_3b_apply.setEnabled(False)
        self.pb_bt_3b_apply_all.setEnabled(False)
        self.pb_bt_error_apply.setEnabled(False)
        self.pb_bt_error_apply_all.setEnabled(False)
        self.pb_bt_vert_apply.setEnabled(False)
        self.pb_bt_vert_apply_all.setEnabled(False)

        # Turn signals off
        self.cb_bt_vectors.blockSignals(True)

        self.cb_bt_vectors.setChecked(True)

        # Set units
        self.gb_bt_error_velocity.setTitle(
            self.tr("Error Velocity" + self.units["label_V"])
        )
        self.gb_bt_vertical_velocity.setTitle(
            self.tr("Vertical Velocity" + self.units["label_V"])
        )

        self.bt_update_settings()

        # Display content
        if old_discharge is None:
            old_discharge = self.meas.summary
        self.bt_update_table(old_discharge=old_discharge)
        self.bt_plots()
        self.bt_comments_messages()
        self.wt_subtab_data.setFocus()

        # Setup lists for use by graphics controls
        self.canvases = [self.bt_shiptrack_canvas, self.bt_ts_canvas]
        self.figs = [self.bt_shiptrack_fig, self.bt_ts_fig]
        self.toolbars = [self.bt_shiptrack_toolbar, self.bt_ts_toolbar]
        self.fig_calls = [self.bt_shiptrack, self.bt_ts_plots]
        self.ui_parents = [i.parent() for i in self.canvases]
        self.figs_menu_connection()

        # Turn signals on
        self.cb_bt_vectors.blockSignals(False)

    def bt_update_table(self, old_discharge):
        """Updates the bottom track table with new or reprocessed data.

        Parameters
        ----------
        old_discharge: dataframe
            Dataframe of measurement summary prior to changes
        """

        with self.wait_cursor():
            # Set tbl variable
            tbl = self.table_bt

            # Populate each row
            for row in range(tbl.rowCount()):
                # Identify vertical
                if self.meas.summary["Station Number"][row] == np.floor(
                    self.meas.summary["Station Number"][row]
                ):
                    # Get vertical
                    vertical_id = self.meas.verticals_used_idx[
                        int(self.meas.summary["Station Number"][row] - 1)
                    ]
                    vertical = self.meas.verticals[vertical_id]

                    # Identify a measured vertical
                    if self.meas.summary["Duration"][row] > 0:
                        valid_data = vertical.data.boat_vel.bt_vel.valid_data
                        num_ensembles = len(valid_data[0, :])
                        not_4beam = np.nansum(
                            np.isnan(vertical.data.boat_vel.bt_vel.d_mps)
                        )
                        num_valid = np.nansum(valid_data[0, :])
                        num_orig_invalid = np.nansum(np.logical_not(valid_data[1, :]))
                        num_beam_invalid = np.nansum(np.logical_not(valid_data[5, :]))
                        num_error_invalid = np.nansum(np.logical_not(valid_data[2, :]))
                        num_vert_invalid = np.nansum(np.logical_not(valid_data[3, :]))

                    else:
                        num_ensembles = 0
                        not_4beam = np.nan
                        num_valid = np.nan
                        num_orig_invalid = np.nan
                        num_beam_invalid = np.nan
                        num_error_invalid = np.nan
                        num_vert_invalid = np.nan

                else:
                    num_ensembles = np.nan
                    not_4beam = np.nan
                    num_valid = np.nan
                    num_orig_invalid = np.nan
                    num_beam_invalid = np.nan
                    num_error_invalid = np.nan
                    num_vert_invalid = np.nan

                # Station Number
                col = 0
                if np.isnan(self.meas.summary["Station Number"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                elif np.modf(self.meas.summary["Station Number"][row])[0] == 0:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:3.0f}".format(self.meas.summary["Station Number"][row])
                        ),
                    )
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:3.1f}".format(self.meas.summary["Station Number"][row])
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
                if np.modf(self.meas.summary["Station Number"][row])[0] == 0:
                    if (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.bt_vel["total_warning"]["All: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))

                    elif (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.bt_vel["total_caution"]["All: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))

                    else:
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))

                    tbl.item(row, col).setToolTip(
                        self.tr(self.bt_create_tooltip(row, col))
                    )

                # Location
                col += 1
                if np.isnan(self.meas.summary["Location"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:6.2f}".format(
                                self.meas.summary["Location"][row] * self.units["L"]
                            )
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Total number of ensembles
                col += 1
                if np.isnan(num_ensembles):
                    item = ""
                else:
                    item = "{:5.0f}".format(num_ensembles)
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Valid total
                col += 1
                if num_ensembles == 0 or np.isnan(num_ensembles):
                    item = ""
                else:
                    item = "{:5.0f}".format(num_valid)
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                if item != "":
                    if (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.bt_vel["all_invalid"]["All: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                    elif (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.bt_vel["total_warning"]["All: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                    elif (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.bt_vel["total_caution"]["All: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                    else:
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))

                    tbl.item(row, col).setToolTip(
                        self.tr(self.bt_create_tooltip(row, col))
                    )

                # Less than 4 beams
                col += 1
                if num_ensembles == 0 or np.isnan(num_ensembles):
                    item = ""
                else:
                    item = "{:5.0f}".format(not_4beam)
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Invalid original data
                col += 1
                if num_ensembles == 0 or np.isnan(num_ensembles):
                    item = ""
                else:
                    item = "{:5.0f}".format(num_orig_invalid)
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                if item != "":
                    if (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.bt_vel["total_warning"]["Original: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                    elif (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.bt_vel["total_caution"]["Original: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                    else:
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))

                    if num_valid == 0:
                        tbl.item(row, col).setToolTip(self.tr("All data are invalid."))
                    else:
                        tbl.item(row, col).setToolTip(
                            self.tr(self.bt_create_tooltip(row, col))
                        )

                # Valid 3 beam
                col += 1
                if num_ensembles == 0 or np.isnan(num_ensembles):
                    item = ""
                else:
                    item = "{:5.0f}".format(num_beam_invalid)
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                if item != "":
                    if (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.bt_vel["total_warning"]["3Beams: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                    elif (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.bt_vel["total_caution"]["3Beams: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                    else:
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))

                    if num_valid == 0:
                        tbl.item(row, col).setToolTip(self.tr("All data are invalid."))
                    else:
                        tbl.item(row, col).setToolTip(
                            self.tr(self.bt_create_tooltip(row, col))
                        )

                # Error velocity invalid
                col += 1
                if num_ensembles == 0 or np.isnan(num_ensembles):
                    item = ""
                else:
                    item = "{:5.0f}".format(num_error_invalid)
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                if item != "":
                    if (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.bt_vel["total_warning"]["ErrorVel: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                    elif (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.bt_vel["total_caution"]["ErrorVel: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                    else:
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))

                    if num_valid == 0:
                        tbl.item(row, col).setToolTip(self.tr("All data are invalid."))
                    else:
                        tbl.item(row, col).setToolTip(
                            self.tr(self.bt_create_tooltip(row, col))
                        )

                # Vertical velocity invalid
                col += 1
                if num_ensembles == 0 or np.isnan(num_ensembles):
                    item = ""
                else:
                    item = "{:5.0f}".format(num_vert_invalid)
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                if item != "":
                    if (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.bt_vel["total_warning"]["VertVel: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                    elif (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.bt_vel["total_caution"]["VertVel: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                    else:
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))

                    if num_valid == 0:
                        tbl.item(row, col).setToolTip(self.tr("All data are invalid."))
                    else:
                        tbl.item(row, col).setToolTip(
                            self.tr(self.bt_create_tooltip(row, col))
                        )

                # Discharge before changes
                col += 1
                if np.isnan(old_discharge["Q"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:8}".format(
                                self.q_digits(old_discharge["Q"][row] * self.units["Q"])
                            )
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Discharge after changes
                col += 1
                if np.isnan(self.meas.summary["Q"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:8}".format(
                                self.q_digits(
                                    self.meas.summary["Q"][row] * self.units["Q"]
                                )
                            )
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Percent difference in old and new discharges
                col += 1
                if np.isnan(self.meas.summary["Q"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    if np.abs(old_discharge["Q"][row]) > 0:
                        per_change = (
                            (self.meas.summary["Q"][row] - old_discharge["Q"][row])
                            / old_discharge["Q"][row]
                        ) * 100
                        tbl.setItem(
                            row,
                            col,
                            QtWidgets.QTableWidgetItem("{:3.1f}".format(per_change)),
                        )
                    else:
                        tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Set selected file to bold font
            self.highlight_row(tbl, self.main_row_idx - 1)

            tbl.resizeColumnsToContents()
            tbl.resizeRowsToContents()
            tbl.setFocus()

    def bt_update_settings(self):
        """Updates the user modifiable settings to values for the selected
        vertical.
        """

        # Block signals
        self.combo_bt_3beam.blockSignals(True)
        self.combo_bt_error_velocity.blockSignals(True)
        self.combo_bt_vert_velocity.blockSignals(True)

        # Transect selected for display
        vertical = self.meas.verticals[self.vertical_idx]

        # Set beam filter from transect data
        if vertical.data.boat_vel is not None:
            self.combo_bt_3beam.show()
            self.pb_bt_3b_apply.show()
            self.pb_bt_3b_apply_all.show()
            self.combo_bt_error_velocity.show()
            self.ed_bt_error_vel_threshold.show()
            self.pb_bt_error_apply.show()
            self.pb_bt_error_apply_all.show()
            self.combo_bt_vert_velocity.show()
            self.ed_bt_vert_vel_threshold.show()
            self.pb_bt_vert_apply.show()
            self.pb_bt_vert_apply_all.show()

            if vertical.data.boat_vel.bt_vel.beam_filter < 0:
                self.combo_bt_3beam.setCurrentIndex(0)
            elif vertical.data.boat_vel.bt_vel.beam_filter == 3:
                self.combo_bt_3beam.setCurrentIndex(1)
            elif vertical.data.boat_vel.bt_vel.beam_filter == 4:
                self.combo_bt_3beam.setCurrentIndex(2)
            else:
                self.combo_bt_3beam.setCurrentIndex(0)

            # Set error velocity filter from transect data
            index = self.combo_bt_error_velocity.findText(
                vertical.data.boat_vel.bt_vel.d_filter, QtCore.Qt.MatchFixedString
            )
            self.combo_bt_error_velocity.setCurrentIndex(index)

            if vertical.data.boat_vel.bt_vel.d_filter == "Manual":
                threshold = "{:3.4f}".format(
                    vertical.data.boat_vel.bt_vel.d_filter_thresholds * self.units["V"]
                )
                self.ed_bt_error_vel_threshold.setText(threshold)
                self.ed_bt_error_vel_threshold.setEnabled(True)
            else:
                self.ed_bt_error_vel_threshold.setText("")

            # Set vertical velocity filter from transect data
            index = self.combo_bt_vert_velocity.findText(
                vertical.data.boat_vel.bt_vel.w_filter, QtCore.Qt.MatchFixedString
            )
            self.combo_bt_vert_velocity.setCurrentIndex(index)

            if (
                vertical.data.boat_vel is not None
                and vertical.data.boat_vel.bt_vel.w_filter == "Manual"
            ):
                threshold = "{:3.4f}".format(
                    vertical.data.boat_vel.bt_vel.w_filter_thresholds * self.units["V"]
                )
                self.ed_bt_vert_vel_threshold.setText(threshold)
                self.ed_bt_vert_vel_threshold.setEnabled(True)
            else:
                self.ed_bt_vert_vel_threshold.setText("")
        else:
            self.combo_bt_3beam.hide()
            self.pb_bt_3b_apply.hide()
            self.pb_bt_3b_apply_all.hide()
            self.combo_bt_error_velocity.hide()
            self.ed_bt_error_vel_threshold.hide()
            self.pb_bt_error_apply.hide()
            self.pb_bt_error_apply_all.hide()
            self.combo_bt_vert_velocity.hide()
            self.ed_bt_vert_vel_threshold.hide()
            self.pb_bt_vert_apply.hide()
            self.pb_bt_vert_apply_all.hide()

        # Unblock signals
        self.combo_bt_3beam.blockSignals(False)
        self.combo_bt_error_velocity.blockSignals(False)
        self.combo_bt_vert_velocity.blockSignals(False)

    def bt_table_clicked(self, row):
        """Changes plotted data and user settings to the vertical selected.

        Parameters
        ----------
        row: int
            Row clicked by user
        """

        tbl = self.table_bt

        # Check to see that the row selected is a valid vertical. This
        # avoids the discharge lines for mean section
        if self.valid_row(row):
            self.highlight_row(tbl, row)
            self.bt_update_settings()
            self.bt_plots()
            self.bt_subtab_data.setFocus()
            self.change = True

    def bt_plots(self):
        """Creates graphics for BT tab."""

        with self.wait_cursor():
            # Update plots
            self.bt_shiptrack()
            self.bt_ts_plots()

            # Update list of figs
            self.figs = [self.bt_shiptrack_fig, self.bt_ts_fig]
            self.canvases = [self.bt_shiptrack_canvas, self.bt_ts_canvas]
            self.fig_calls = [self.bt_shiptrack(), self.bt_ts_plots()]
            self.toolbars = [self.bt_shiptrack_toolbar, self.bt_ts_toolbar]
            self.ui_parents = [i.parent() for i in self.canvases]
            self.figs_menu_connection()

            # Reset data cursor to work with new figure
            if self.actionData_Cursor.isChecked():
                self.data_cursor()

    def bt_shiptrack(self):
        """Creates shiptrack plot for data in vertical."""

        # If the canvas has not been previously created, create the canvas
        # and add the widget.
        if self.bt_shiptrack_canvas is None:
            # Create the canvas
            self.bt_shiptrack_canvas = MplCanvas(
                parent=self.graph_bt_st, width=4, height=4, dpi=80
            )
            # Assign layout to widget to allow auto scaling
            layout = QtWidgets.QVBoxLayout(self.graph_bt_st)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(1, 1, 1, 1)
            # Add the canvas
            layout.addWidget(self.bt_shiptrack_canvas)
            # Initialize hidden toolbar for use by graphics controls
            self.bt_shiptrack_toolbar = NavigationToolbar(
                self.bt_shiptrack_canvas, self
            )
            self.bt_shiptrack_toolbar.hide()

        # Initialize the shiptrack figure and assign to the canvas
        self.bt_shiptrack_fig = Shiptrack(canvas=self.bt_shiptrack_canvas)
        if self.meas.verticals[self.vertical_idx].data.boat_vel is not None:
            # Create the figure with the specified data
            self.bt_shiptrack_fig.create(
                transect=self.meas.verticals[self.vertical_idx].data,
                units=self.units,
                cb=True,
                cb_vectors=self.cb_bt_vectors,
            )
        else:
            self.bt_shiptrack_fig.fig.clear()

        # Draw canvas
        self.bt_shiptrack_canvas.draw()

    def bt_ts_plots(self):
        """Creates plots of filter characteristics."""

        # If the canvas has not been previously created, create the canvas
        # and add the widget.
        if self.bt_ts_canvas is None:
            # Create the canvas
            self.bt_ts_canvas = MplCanvas(
                parent=self.graph_bt_ts, width=8, height=2, dpi=80
            )
            # Assign layout to widget to allow auto scaling
            layout = QtWidgets.QVBoxLayout(self.graph_bt_ts)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(0, 0, 0, 0)
            # Add the canvas
            layout.addWidget(self.bt_ts_canvas)
            self.bt_ts_toolbar = NavigationToolbar(self.bt_ts_canvas, self)
            self.bt_ts_toolbar.hide()

        # Initialize the boat speed figure and assign to the canvas
        self.bt_ts_fig = Graphics(canvas=self.bt_ts_canvas)

        if self.meas.verticals[self.vertical_idx].data.boat_vel is not None:
            # Create the figure with the specified data
            self.bt_ts_fig.bt_tab_graphs(
                transect=self.meas.verticals[self.vertical_idx].data,
                units=self.units,
                beam=self.rb_bt_beam.isChecked(),
                error=self.rb_bt_error.isChecked(),
                vert=self.rb_bt_vert.isChecked(),
                source=self.rb_bt_source.isChecked(),
                bt=True,
                gga=False,
                vtg=False,
                x_axis_type="E",
            )
        else:
            self.bt_ts_fig.fig.clear()

        # Update list of figs
        self.figs = [self.bt_shiptrack_fig, self.bt_ts_fig]
        self.canvases = [self.bt_shiptrack_canvas, self.bt_ts_canvas]
        self.fig_calls = [self.bt_shiptrack, self.bt_ts_plots]
        self.toolbars = [self.bt_shiptrack_toolbar, self.bt_ts_toolbar]
        self.ui_parents = [i.parent() for i in self.canvases]
        self.figs_menu_connection()

        # Reset data cursor to work with new figure
        if self.actionData_Cursor.isChecked():
            self.data_cursor()
        # Draw canvas
        self.bt_ts_canvas.draw()

    @QtCore.pyqtSlot()
    def bt_radiobutton_control(self):
        """Identifies a change in radio buttons and calls the plot routine
        to update the graph.
        """
        with self.wait_cursor():
            if self.sender().isChecked():
                self.bt_plots()

    def bt_plot_change(self):
        """Coordinates changes in what references should be displayed in the
        boat speed and shiptrack plots.
        """

        with self.wait_cursor():
            self.bt_plots()
            self.bt_subtab_data.setFocus()

    def bt_update(self, settings, apply_all):
        """Applies change bottom track filters and updates the measurement
        and bottom track tab (table and graphics) .

        Parameters
        ----------
        settings: list
            List of tuples of setting name and value
        apply_all: bool
            Indicator if change should be applied to selected vertical or
            all verticals
        """

        # Save discharge from previous settings
        old_discharge = self.meas.summary

        # Apply new settings
        if apply_all:
            self.meas.change_vertical_setting(settings=settings)
        else:
            self.meas.change_vertical_setting(settings=settings, idx=self.vertical_idx)

        # Update tab
        self.bt_update_tab(old_discharge=old_discharge)

        # Reset focus
        self.bt_subtab_data.setFocus()

    def bt_beam_change(self, apply_all):
        """Coordinates user initiated change to the beam settings.

        Parameters
        ----------
        apply_all: bool
            Indicator if change should be applied to selected vertical or
                all verticals
        """

        with self.wait_cursor():
            self.combo_bt_3beam.blockSignals(True)
            text = self.combo_bt_3beam.currentText()

            # Change setting
            if text == "Auto":
                settings = [("BTbeamFilter", -1)]
            elif text == "Allow":
                settings = [("BTbeamFilter", 3)]
            elif text == "4-Beam Only":
                settings = [("BTbeamFilter", 4)]

            # Apply change, update measurement and display
            self.bt_update(settings, apply_all)
            self.change = True
            self.combo_bt_3beam.blockSignals(False)
            self.pb_bt_3b_apply.setEnabled(False)
            self.pb_bt_3b_apply_all.setEnabled(False)

    def bt_error_vel_change(self, apply_all):
        """Coordinates user initiated change to the error velocity settings.

        Parameters
        ----------
        apply_all: bool
           Indicator if change should be applied to selected vertical or
           all verticals
        """

        with self.wait_cursor():
            # Block signals
            self.combo_bt_error_velocity.blockSignals(True)
            self.ed_bt_error_vel_threshold.blockSignals(True)

            # Get user setting
            text = self.combo_bt_error_velocity.currentText()

            # Change setting based on combo box selection
            settings = [("BTdFilter", text)]
            if text == "Manual":
                # If Manual, enable the line edit box for user input. Updates
                # are not applied until the user has entered
                # a value in the line edit box.
                threshold = self.check_numeric_input(self.ed_bt_error_vel_threshold)
                threshold = threshold / self.units["V"]
                settings.append(("BTdFilterThreshold", threshold))
                self.ed_bt_error_vel_threshold.blockSignals(False)
            else:
                # If manual is not selected the line edit box is cleared and
                # disabled and the updates applied.
                self.ed_bt_error_vel_threshold.setEnabled(False)
                self.ed_bt_error_vel_threshold.setText("")

            # Apply settings
            self.bt_update(settings, apply_all)
            self.change = True

            # Unblock signals
            self.combo_bt_error_velocity.blockSignals(False)
            self.ed_bt_error_vel_threshold.blockSignals(False)

            # Disable apply buttons
            self.pb_bt_error_apply.setEnabled(False)
            self.pb_bt_error_apply_all.setEnabled(False)

    def bt_vert_vel_change(self, apply_all):
        """Coordinates user initiated change to the vertical velocity settings.

        Parameters
        ----------
        apply_all: bool
            Indicator if change should be applied to selected vertical or
            all verticals
        """

        with self.wait_cursor():
            # Block signals
            self.combo_bt_vert_velocity.blockSignals(True)
            self.ed_bt_vert_vel_threshold.blockSignals(True)

            # Get user setting
            text = self.combo_bt_vert_velocity.currentText()

            # Change setting based on combo box selection
            settings = [("BTwFilter", text)]

            if text == "Manual":
                # If Manual enable the line edit box for user input. Updates
                # are not applied until the user has entered
                # a value in the line edit box.
                threshold = self.check_numeric_input(self.ed_bt_vert_vel_threshold)
                threshold = threshold / self.units["V"]
                settings.append(("BTwFilterThreshold", threshold))
                self.ed_bt_vert_vel_threshold.blockSignals(False)
            else:
                # If manual is not selected the line edit box is cleared and
                # disabled and the updates applied.
                self.ed_bt_vert_vel_threshold.setEnabled(False)
                self.ed_bt_vert_vel_threshold.setText("")

            # Apply settings
            self.bt_update(settings, apply_all)
            self.change = True

            # Unblock signals
            self.combo_bt_vert_velocity.blockSignals(False)
            self.ed_bt_vert_vel_threshold.blockSignals(False)

            # Disable apply buttons
            self.pb_bt_vert_apply.setEnabled(False)
            self.pb_bt_vert_apply_all.setEnabled(False)

    def bt_comments_messages(self):
        """Displays comments and messages associated with bottom track
        filters in Messages tab.
        """

        # Clear comments and messages
        self.display_bt_comments.clear()

        if self.meas is not None:
            # Display each comment on a new line
            self.display_bt_comments.moveCursor(QtGui.QTextCursor.Start)
            for comment in self.meas.comments:
                self.display_bt_comments.textCursor().insertText(comment)
                self.display_bt_comments.moveCursor(QtGui.QTextCursor.End)
                self.display_bt_comments.textCursor().insertBlock()

            # Display each message on a new line
            self.messages_table(self.table_bt_messages, ["bt_vel"])

            # Update tab icons
            self.update_tab_icons()

    def bt_create_tooltip(self, row, column):
        """Create a tooltip for the row and column in the table based on the
        QA results.

        Parameters
        ----------
        row: int
            Row in table
        column: int
            Column in table

        Returns
        -------
        tt: str
            Text of tooltip
        """

        # Initialize variables
        cat = None
        tt = ""

        # From column determine category of QA results to use
        if column == 3:
            cat = "All: "
        elif column == 5:
            cat = "Original: "
        elif column == 6:
            cat = "3Beams: "
        elif column == 7:
            cat = "ErrorVel: "
        elif column == 8:
            cat = "VertVel: "

        # Create tooltip
        if cat is not None:
            tt = "".join(
                self.tooltip_qa_message(
                    qa_data=self.meas.qa.bt_vel,
                    cat=cat,
                    vertical_id=int(self.meas.summary["Station Number"][row]),
                    total_threshold_warning=self.meas.qa.percent_valid_warning_threshold,
                    total_threshold_caution=self.meas.qa.percent_valid_caution_threshold,
                )
            )
        return tt

    # Depth tab
    # =========
    def depth_tab_initial(self, old_discharge=None):
        """Initialize and configure depth tab."""

        # Setup data table
        tbl = self.table_depth
        table_header = [
            self.tr("Station \n Number"),
            self.tr("Location \n" + self.units["label_L"]),
            self.tr("Condition"),
            self.tr("Depth \n Source"),
            self.tr("Depth \n" + self.units["label_L"]),
            self.tr("Depth \n Std. Dev. \n" + self.units["label_L"]),
            self.tr("Number \n Ens."),
            self.tr("Number \n Valid \n Ens."),
            self.tr("Beam 1 \n Number \n Invalid"),
            self.tr("Beam 2 \n Number \n Invalid"),
            self.tr("Beam 3 \n Number \n Invalid"),
            self.tr("Beam 4 \n Number \n Invalid"),
            self.tr("Vertical \n Beam \n Number \n Invalid"),
            self.tr("ADCP \n Depth \n" + self.units["label_L"]),
            self.tr("Ice \n Thick. \n" + self.units["label_L"]),
            self.tr("Depth \n Ice \n Bottom \n" + self.units["label_L"]),
            self.tr("Depth \n Slush \n Bottom \n" + self.units["label_L"]),
            self.tr("Discharge \n Previous \n" + self.units["label_Q"]),
            self.tr("Discharge \n Now \n" + self.units["label_Q"]),
            self.tr("Discharge \n % Change"),
        ]
        ncols = len(table_header)
        nrows = self.meas.summary.shape[0] - 2
        tbl.setRowCount(nrows)
        tbl.setColumnCount(ncols)
        tbl.setHorizontalHeaderLabels(table_header)
        tbl.horizontalHeader().setFont(self.font_bold)
        tbl.verticalHeader().hide()
        tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)

        # Automatically resize rows and columns
        tbl.resizeColumnsToContents()
        tbl.resizeRowsToContents()

        # Initialize connections
        if not self.depth_initialized:
            # Table Clicked
            tbl.cellClicked.connect(self.depth_table_clicked)

            # Initialize checkbox settings top plot
            self.cb_depth_beam1.setCheckState(QtCore.Qt.Checked)
            self.cb_depth_beam2.setCheckState(QtCore.Qt.Checked)
            self.cb_depth_beam3.setCheckState(QtCore.Qt.Checked)
            self.cb_depth_beam4.setCheckState(QtCore.Qt.Checked)

            # Initialize checkbox settings bottom plot
            self.cb_depth_4beam_cs.setCheckState(QtCore.Qt.Unchecked)
            self.cb_depth_final_cs.setCheckState(QtCore.Qt.Checked)
            self.cb_depth_vert_cs.setCheckState(QtCore.Qt.Unchecked)
            self.cb_depth_cs.setCheckState(QtCore.Qt.Unchecked)

            # Connect top plot variable checkboxes
            self.cb_depth_beam1.stateChanged.connect(self.depth_plots)
            self.cb_depth_beam2.stateChanged.connect(self.depth_plots)
            self.cb_depth_beam3.stateChanged.connect(self.depth_plots)
            self.cb_depth_beam4.stateChanged.connect(self.depth_plots)
            self.cb_depth_vert.stateChanged.connect(self.depth_plots)

            # Connect bottom plot variable checkboxes
            self.cb_depth_4beam_cs.stateChanged.connect(self.depth_plot_control)
            self.cb_depth_final_cs.stateChanged.connect(self.depth_plot_control)
            self.cb_depth_vert_cs.stateChanged.connect(self.depth_plot_control)
            self.cb_depth_cs.stateChanged.connect(self.depth_plot_control_cs)

            # Depth source
            self.depth_source_grp = ComboEdGrp(
                combo=self.combo_depth_source,
                ed=self.ed_depth_manual,
                pb_apply=self.pb_depth_source_apply,
                pb_apply_all=self.pb_depth_source_apply_all,
                change=self.depth_source_change,
                min_value=0,
                previous=self.depth_settings_update,
            )
            self.combo_depth_source.activated[str].connect(
                self.depth_source_grp.enable_apply
            )
            self.ed_depth_manual.textChanged.connect(self.depth_source_grp.enable_apply)
            self.pb_depth_source_apply.clicked.connect(self.depth_source_grp.apply)
            self.pb_depth_source_apply_all.clicked.connect(
                self.depth_source_grp.apply_all
            )

            # BT Averaging
            self.depth_avg_grp = ComboGrp(
                combo=self.combo_depth_avg,
                pb_apply=self.pb_depth_avg_apply,
                pb_apply_all=self.pb_depth_avg_apply_all,
                change=self.depth_avg_change,
            )
            self.combo_depth_avg.activated[str].connect(self.depth_avg_grp.enable_apply)
            self.pb_depth_avg_apply.clicked.connect(self.depth_avg_grp.apply)
            self.pb_depth_avg_apply_all.clicked.connect(self.depth_avg_grp.apply_all)

            # Depth Filter
            self.depth_filter_grp = ComboGrp(
                combo=self.combo_depth_filter,
                pb_apply=self.pb_depth_filter_apply,
                pb_apply_all=self.pb_depth_filter_apply_all,
                change=self.depth_filter_change,
            )
            self.combo_depth_filter.activated[str].connect(
                self.depth_filter_grp.enable_apply
            )
            self.pb_depth_filter_apply.clicked.connect(self.depth_filter_grp.apply)
            self.pb_depth_filter_apply_all.clicked.connect(
                self.depth_filter_grp.apply_all
            )

            # ADCP Draft
            self.depth_draft_grp = EdGrp(
                ed=self.ed_depth_draft,
                pb_apply=self.pb_depth_draft_apply,
                pb_apply_all=self.pb_depth_draft_apply_all,
                change=self.depth_draft_change,
                min_value=0,
            )
            self.ed_depth_draft.textChanged.connect(self.depth_draft_grp.enable_apply)
            self.pb_depth_draft_apply.clicked.connect(self.depth_draft_grp.apply)
            self.pb_depth_draft_apply_all.clicked.connect(
                self.depth_draft_grp.apply_all
            )

            # ADCP Draft Ice
            self.depth_draft_ice_grp = EdGrp(
                ed=self.ed_depth_draft_ice,
                pb_apply=self.pb_depth_draft_ice_apply,
                pb_apply_all=self.pb_depth_draft_ice_apply_all,
                change=self.depth_draft_ice_change,
                min_value=0,
            )
            self.ed_depth_draft_ice.textChanged.connect(
                self.depth_draft_ice_grp.enable_apply
            )
            self.pb_depth_draft_ice_apply.clicked.connect(
                self.depth_draft_ice_grp.apply
            )
            self.pb_depth_draft_ice_apply_all.clicked.connect(
                self.depth_draft_ice_grp.apply_all
            )

            # Ice Thickness
            self.depth_ice_thickness_grp = EdGrp(
                ed=self.ed_depth_ice_thickness,
                pb_apply=self.pb_depth_ice_thickness_apply,
                pb_apply_all=self.pb_depth_ice_thickness_apply_all,
                change=self.depth_ice_thickness_change,
                min_value=0,
            )
            self.ed_depth_ice_thickness.textChanged.connect(
                self.depth_ice_thickness_grp.enable_apply
            )
            self.pb_depth_ice_thickness_apply.clicked.connect(
                self.depth_ice_thickness_grp.apply
            )
            self.pb_depth_ice_thickness_apply_all.clicked.connect(
                self.depth_ice_thickness_grp.apply_all
            )

            # WS to Ice Bottom
            self.depth_ice_bottom_grp = EdGrp(
                ed=self.ed_depth_ice_bottom,
                pb_apply=self.pb_depth_ice_bottom_apply,
                pb_apply_all=self.pb_depth_ice_bottom_apply_all,
                change=self.depth_ice_bottom_change,
                min_value=0,
            )
            self.ed_depth_ice_bottom.textChanged.connect(
                self.depth_ice_bottom_grp.enable_apply
            )
            self.pb_depth_ice_bottom_apply.clicked.connect(
                self.depth_ice_bottom_grp.apply
            )
            self.pb_depth_ice_bottom_apply_all.clicked.connect(
                self.depth_ice_bottom_grp.apply_all
            )

            # WS to Slush Bottom
            self.depth_slush_bottom_grp = EdGrp(
                ed=self.ed_depth_slush_bottom,
                pb_apply=self.pb_depth_slush_bottom_apply,
                pb_apply_all=self.pb_depth_slush_bottom_apply_all,
                change=self.depth_slush_bottom_change,
                min_value=0,
            )
            self.ed_depth_slush_bottom.textChanged.connect(
                self.depth_slush_bottom_grp.enable_apply
            )
            self.pb_depth_slush_bottom_apply.clicked.connect(
                self.depth_slush_bottom_grp.apply
            )
            self.pb_depth_slush_bottom_apply_all.clicked.connect(
                self.depth_slush_bottom_grp.apply_all
            )

            self.depth_initialized = True

        # Configure depth source options
        self.combo_depth_source.blockSignals(True)

        depth_source_options = [self.tr("Manual")]
        self.cb_depth_beam1.setEnabled(False)
        self.cb_depth_beam2.setEnabled(False)
        self.cb_depth_beam3.setEnabled(False)
        self.cb_depth_beam4.setEnabled(False)
        self.cb_depth_4beam_cs.setEnabled(False)

        if self.meas.verticals[self.vertical_idx].data.depths is not None:
            depth_source_options.append(self.tr("4-Beam Avg"))
            self.cb_depth_beam1.setEnabled(True)
            self.cb_depth_beam2.setEnabled(True)
            self.cb_depth_beam3.setEnabled(True)
            self.cb_depth_beam4.setEnabled(True)
            self.cb_depth_4beam_cs.setEnabled(True)

            # Setup for vertical beam
            if self.meas.verticals[self.vertical_idx].data.depths.vb_depths is not None:
                self.cb_depth_vert.blockSignals(True)
                self.cb_depth_vert.setCheckState(QtCore.Qt.Checked)
                self.cb_depth_vert.blockSignals(False)
                depth_source_options.append(self.tr("Comp 4-Beam Preferred"))
                depth_source_options.append(self.tr("Vertical"))
                depth_source_options.append(self.tr("Comp Vertical Preferred"))
                self.cb_depth_vert.setEnabled(True)
                self.cb_depth_vert_cs.setEnabled(True)
            else:
                self.cb_depth_vert.setEnabled(False)
                self.cb_depth_vert_cs.setEnabled(False)

        self.combo_depth_source.clear()
        self.combo_depth_source.addItems(depth_source_options)

        # Set units
        self.txt_depth_manual_units.setText(self.units["label_L"])
        self.gb_draft.setTitle(
            self.tr("ADCP Depth Below W.S. " + self.units["label_L"])
        )
        self.gb_draft_ice.setTitle(
            self.tr("ADCP Depth Below Ice " + self.units["label_L"])
        )
        self.gb_ice_thickness.setTitle(
            self.tr("Ice Thickness " + self.units["label_L"])
        )
        self.gb_ice_bottom.setTitle(
            self.tr("W.S. to Bottom of Ice " + self.units["label_L"])
        )
        self.gb_slush_bottom.setTitle(
            self.tr("W.S. to Bottom of Slush " + self.units["label_L"])
        )

        self.depth_tab_update(old_discharge=old_discharge)

    def depth_tab_update(self, old_discharge=None):
        """Updates the display of data in the tab.

        Parameters
        ----------
        old_discharge: dataframe
            Pandas dataframe of summary data
        """

        # Old discharge
        if old_discharge is None:
            old_discharge = self.meas.summary

        # Disable apply buttons
        self.pb_depth_source_apply.setEnabled(False)
        self.pb_depth_source_apply_all.setEnabled(False)
        self.pb_depth_avg_apply.setEnabled(False)
        self.pb_depth_avg_apply_all.setEnabled(False)
        self.pb_depth_filter_apply.setEnabled(False)
        self.pb_depth_filter_apply_all.setEnabled(False)
        self.pb_depth_draft_apply.setEnabled(False)
        self.pb_depth_draft_apply_all.setEnabled(False)
        self.pb_depth_draft_ice_apply.setEnabled(False)
        self.pb_depth_draft_ice_apply_all.setEnabled(False)
        self.pb_depth_ice_thickness_apply.setEnabled(False)
        self.pb_depth_ice_thickness_apply_all.setEnabled(False)
        self.pb_depth_ice_bottom_apply.setEnabled(False)
        self.pb_depth_ice_bottom_apply_all.setEnabled(False)
        self.pb_depth_slush_bottom_apply.setEnabled(False)
        self.pb_depth_slush_bottom_apply_all.setEnabled(False)

        self.depth_table_update(old_discharge=old_discharge)
        self.depth_settings_update()
        self.depth_plots()
        self.depth_comments_messages()

        # Setup list for use by graphics controls
        self.canvases = [self.depth_canvas]
        self.figs = [self.depth_fig]
        self.toolbars = [self.depth_toolbar]
        self.fig_calls = [self.depth_plots]
        self.ui_parents = [i.parent() for i in self.canvases]
        self.figs_menu_connection()

    def depth_table_update(self, old_discharge=None):
        """Updates the data displayed in the depth table.

        Parameters
        ----------
        old_discharge: dataframe
            Pandas dataframe of summary data
        """

        with self.wait_cursor():
            # Set tbl variable
            tbl = self.table_depth

            # Populate each row
            for row in range(tbl.rowCount()):
                # Identify vertical
                if self.meas.summary["Station Number"][row] == np.floor(
                    self.meas.summary["Station Number"][row]
                ):
                    # Get vertical
                    vertical_id = self.meas.verticals_used_idx[
                        int(self.meas.summary["Station Number"][row] - 1)
                    ]
                    vertical = self.meas.verticals[vertical_id]

                    # Identify a measured vertical
                    if self.meas.summary["Duration"][row] > 0:
                        depth_selected = getattr(
                            vertical.data.depths, vertical.data.depths.selected
                        )

                        # Beam data
                        valid_beams = vertical.data.depths.bt_depths.valid_beams
                        num_ensembles = len(valid_beams[0, :])
                        num_int = np.where(depth_selected.depth_source_ens == "IN")[
                            0
                        ].shape[0]
                        num_na = np.where(depth_selected.depth_source_ens == "NA")[
                            0
                        ].shape[0]
                        num_valid = num_ensembles - num_int - num_na
                        beam_1_invalid = np.nansum(np.logical_not(valid_beams[0, :]))
                        beam_2_invalid = np.nansum(np.logical_not(valid_beams[1, :]))
                        beam_3_invalid = np.nansum(np.logical_not(valid_beams[2, :]))
                        beam_4_invalid = np.nansum(np.logical_not(valid_beams[3, :]))

                        if vertical.data.depths.vb_depths is not None:
                            vb_invalid = np.nansum(
                                np.logical_not(
                                    vertical.data.depths.vb_depths.valid_beams[0]
                                )
                            )
                        else:
                            vb_invalid = np.nan

                    else:
                        num_ensembles = 0
                        num_valid = np.nan
                        beam_1_invalid = np.nan
                        beam_2_invalid = np.nan
                        beam_3_invalid = np.nan
                        beam_4_invalid = np.nan
                        vb_invalid = np.nan

                else:
                    num_ensembles = np.nan
                    num_valid = np.nan
                    beam_1_invalid = np.nan
                    beam_2_invalid = np.nan
                    beam_3_invalid = np.nan
                    beam_4_invalid = np.nan
                    vb_invalid = np.nan

                # Station Number
                col = 0
                if np.isnan(self.meas.summary["Station Number"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                elif np.modf(self.meas.summary["Station Number"][row])[0] == 0:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:3.0f}".format(self.meas.summary["Station Number"][row])
                        ),
                    )
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:3.1f}".format(self.meas.summary["Station Number"][row])
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Location
                col += 1
                if np.isnan(self.meas.summary["Location"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:6.2f}".format(
                                self.meas.summary["Location"][row] * self.units["L"]
                            )
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # WS Condition
                col += 1
                item = self.meas.summary["Condition"][row]
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
                if vertical.velocity_method == "Manual":
                    tbl.item(row, col).setForeground(QtGui.QColor(230, 138, 0))
                    tbl.item(row, col).setFont(self.font_bold)

                # Depth source
                col += 1
                item = self.meas.summary["Depth Source"][row]
                if len(item) > 6:
                    item = item[0:7]
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
                if (
                    vertical.velocity_method == "Manual"
                    or vertical.depth_source == "Manual"
                ):
                    tbl.item(row, col).setForeground(QtGui.QColor(230, 138, 0))
                    tbl.item(row, col).setFont(self.font_bold)

                if np.modf(self.meas.summary["Station Number"][row])[0] == 0:
                    if (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.depths["all_invalid"]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                    else:
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))

                    tbl.item(row, col).setToolTip(
                        self.tr(self.depth_create_tooltip(row, col))
                    )

                # Depth
                col += 1
                if np.isnan(self.meas.summary["Depth"][row]):
                    # if np.isnan(vertical.depth_m):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
                else:
                    # tbl.setItem(
                    #     row,
                    #     col,
                    #     QtWidgets.QTableWidgetItem(
                    #         "{:5.3f}".format(vertical.depth_m * self.units["L"])
                    #     ),
                    # )
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:5.3f}".format(
                                self.meas.summary["Depth"][row] * self.units["L"]
                            )
                        ),
                    )
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                    if (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.depths["total_warning"]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                        tbl.item(row, col).setToolTip(
                            self.depth_create_tooltip(row, col)
                        )
                    elif (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.depths["total_caution"]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                        tbl.item(row, col).setToolTip(
                            self.depth_create_tooltip(row, col)
                        )
                    elif (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.depths["consistency"]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                        tbl.item(row, col).setToolTip(
                            "Depth appears inconsistent with neighboring depths"
                        )
                    else:
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))

                    if (
                        vertical.velocity_method == "Manual"
                        or vertical.depth_source == "Manual"
                    ):
                        tbl.item(row, col).setForeground(QtGui.QColor(230, 138, 0))
                        tbl.item(row, col).setFont(self.font_bold)

                # Depth std. dev.
                col += 1
                if np.isnan(self.meas.summary["Depth StdDev"][row]):
                    item = ""
                else:
                    item = "{:4.3f}".format(
                        self.meas.summary["Depth StdDev"][row] * self.units["L"]
                    )
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Total number of ensembles
                col += 1
                if np.isnan(num_ensembles):
                    item = ""
                else:
                    item = "{:5.0f}".format(num_ensembles)
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Total number of valid ensembles
                col += 1
                if np.isnan(num_valid):
                    item = ""
                else:
                    item = "{:5.0f}".format(num_valid)
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Beam 1
                col += 1
                if np.isnan(beam_1_invalid):
                    item = ""
                else:
                    item = "{:5.0f}".format(beam_1_invalid)
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
                # if vertical_id in self.meas.qa.depths['total_warning'][
                # 'Beam 1: ']:
                #     tbl.item(row, col).setBackground(QtGui.QColor(255, 77,
                #     77))
                # elif vertical_id in self.meas.qa.depths['total_caution'][
                # 'Beam 1: ']:
                #     tbl.item(row, col).setBackground(QtGui.QColor(255,
                #     204, 0))
                # else:
                #     tbl.item(row, col).setBackground(QtGui.QColor(255,
                #     255, 255))

                if num_valid == 0:
                    tbl.item(row, col).setToolTip(self.tr("All data are invalid."))
                else:
                    tbl.item(row, col).setToolTip(
                        self.tr(self.depth_create_tooltip(row, col))
                    )

                # Beam 2
                col += 1
                if np.isnan(beam_2_invalid):
                    item = ""
                else:
                    item = "{:5.0f}".format(beam_2_invalid)
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
                # if vertical_id in self.meas.qa.depths['total_warning'][
                # 'Beam 2: ']:
                #     tbl.item(row, col).setBackground(QtGui.QColor(255, 77,
                #     77))
                # elif vertical_id in self.meas.qa.depths['total_caution'][
                # 'Beam 2: ']:
                #     tbl.item(row, col).setBackground(QtGui.QColor(255,
                #     204, 0))
                # else:
                #     tbl.item(row, col).setBackground(QtGui.QColor(255,
                #     255, 255))

                if num_valid == 0:
                    tbl.item(row, col).setToolTip(self.tr("All data are invalid."))
                else:
                    tbl.item(row, col).setToolTip(
                        self.tr(self.depth_create_tooltip(row, col))
                    )

                # Beam 3
                col += 1
                if np.isnan(beam_3_invalid):
                    item = ""
                else:
                    item = "{:5.0f}".format(beam_3_invalid)
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
                # if vertical_id in self.meas.qa.depths['total_warning'][
                # 'Beam 3: ']:
                #     tbl.item(row, col).setBackground(QtGui.QColor(255, 77,
                #     77))
                # elif vertical_id in self.meas.qa.depths['total_caution'][
                # 'Beam 3: ']:
                #     tbl.item(row, col).setBackground(QtGui.QColor(255,
                #     204, 0))
                # else:
                #     tbl.item(row, col).setBackground(QtGui.QColor(255,
                #     255, 255))

                if num_valid == 0:
                    tbl.item(row, col).setToolTip(self.tr("All data are invalid."))
                else:
                    tbl.item(row, col).setToolTip(
                        self.tr(self.depth_create_tooltip(row, col))
                    )

                # Beam 4
                col += 1
                if np.isnan(beam_4_invalid):
                    item = ""
                else:
                    item = "{:5.0f}".format(beam_4_invalid)
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
                # if vertical_id in self.meas.qa.depths['total_warning'][
                # 'Beam 4: ']:
                #     tbl.item(row, col).setBackground(QtGui.QColor(255, 77,
                #     77))
                # elif vertical_id in self.meas.qa.depths['total_caution'][
                # 'Beam 4: ']:
                #     tbl.item(row, col).setBackground(QtGui.QColor(255,
                #     204, 0))
                # else:
                #     tbl.item(row, col).setBackground(QtGui.QColor(255,
                #     255, 255))

                if num_valid == 0:
                    tbl.item(row, col).setToolTip(self.tr("All data are invalid."))
                else:
                    tbl.item(row, col).setToolTip(
                        self.tr(self.depth_create_tooltip(row, col))
                    )

                # Vertical Beam
                col += 1
                if np.isnan(vb_invalid):
                    item = ""
                else:
                    item = "{:5.0f}".format(vb_invalid)
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
                # if vertical_id in self.meas.qa.depths['total_warning'][
                # 'Vert. Beam: ']:
                #     tbl.item(row, col).setBackground(QtGui.QColor(255, 77,
                #     77))
                # elif vertical_id in self.meas.qa.depths['total_caution'][
                # 'Vert. Beam: ']:
                #     tbl.item(row, col).setBackground(QtGui.QColor(255,
                #     204, 0))
                # else:
                #     tbl.item(row, col).setBackground(QtGui.QColor(255,
                #     255, 255))

                if num_valid == 0:
                    tbl.item(row, col).setToolTip(self.tr("All data are invalid."))
                else:
                    tbl.item(row, col).setToolTip(
                        self.tr(self.depth_create_tooltip(row, col))
                    )

                # ADCP draft
                col += 1
                if np.isnan(self.meas.summary["ADCP Depth"][row]):
                    item = ""
                else:
                    item = "{:3.3f}".format(
                        self.meas.summary["ADCP Depth"][row] * self.units["L"]
                    )
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                if item != "":
                    if (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.depths["draft_zero"]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                        tbl.item(row, col).setToolTip(self.tr("Draft is too shallow."))

                    else:
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))
                        tbl.item(row, col).setToolTip(
                            self.tr(self.depth_create_tooltip(row, col))
                        )

                # Ice thickness
                col += 1
                if np.isnan(self.meas.summary["Ice Thickness"][row]):
                    item = ""
                else:
                    item = "{:3.3f}".format(
                        self.meas.summary["Ice Thickness"][row] * self.units["L"]
                    )
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Ice Bottom
                col += 1
                if np.isnan(self.meas.summary["Depth to Ice Bottom"][row]):
                    item = ""
                else:
                    item = "{:3.3f}".format(
                        self.meas.summary["Depth to Ice Bottom"][row] * self.units["L"]
                    )
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Slush Bottom
                col += 1
                if np.isnan(self.meas.summary["Depth to Slush Bottom"][row]):
                    item = ""
                else:
                    item = "{:3.3f}".format(
                        self.meas.summary["Depth to Slush Bottom"][row]
                        * self.units["L"]
                    )
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Discharge before changes
                col += 1
                if np.isnan(old_discharge["Q"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:8}".format(
                                self.q_digits(old_discharge["Q"][row] * self.units["Q"])
                            )
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Discharge after changes
                col += 1
                if np.isnan(self.meas.summary["Q"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:8}".format(
                                self.q_digits(
                                    self.meas.summary["Q"][row] * self.units["Q"]
                                )
                            )
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Percent difference in old and new discharges
                col += 1
                if np.isnan(self.meas.summary["Q"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    if np.abs(old_discharge["Q"][row]) > 0:
                        per_change = (
                            (self.meas.summary["Q"][row] - old_discharge["Q"][row])
                            / old_discharge["Q"][row]
                        ) * 100
                        tbl.setItem(
                            row,
                            col,
                            QtWidgets.QTableWidgetItem("{:3.1f}".format(per_change)),
                        )
                    else:
                        tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            item = tbl.horizontalHeaderItem(13)
            if self.meas.qa.depths["draft"] == 1:
                item.setForeground(QtGui.QColor(230, 138, 0))
                item.setToolTip("ADCP depth is inconsistent.")
            else:
                item.setForeground(QtGui.QColor(0, 0, 0))
                item.setToolTip("")

            tbl.show()
            # Set highlight selected vertical
            self.highlight_row(tbl, self.main_row_idx - 1)

            # Hide unused columns
            if np.any(self.meas.summary["Condition"].str.contains("Ice")):
                tbl.setColumnHidden(14, False)
                tbl.setColumnHidden(15, False)
                tbl.setColumnHidden(16, False)
            else:
                tbl.setColumnHidden(14, True)
                tbl.setColumnHidden(15, True)
                tbl.setColumnHidden(16, True)

            tbl.resizeColumnsToContents()
            tbl.resizeRowsToContents()

    def depth_create_tooltip(self, row, column):
        """Create tooltip for potential cautions and warnings."""

        # Initialize variables
        tt = ""

        # Depths
        if column == 4:
            tt = "".join(
                self.tooltip_qa_message(
                    qa_data=self.meas.qa.depths,
                    cat=None,
                    vertical_id=int(self.meas.summary["Station Number"][row]),
                    total_threshold_warning=self.meas.qa.percent_valid_warning_threshold,
                    total_threshold_caution=self.meas.qa.percent_valid_caution_threshold,
                )
            )
            tt = "".join(tt)

        # Draft
        elif column == 1:
            if self.meas.qa.depths["draft"] == 1:
                tt = self.tr("Transducer depth is not consistent among transects.")
            elif self.meas.qa.depths["draft"] == 2:
                tt = self.tr("Transducer depth is too shallow, likely 0.")
        return tt

    def depth_settings_update(self):
        """Update the user settings for the selected vertical."""

        # Turn signals off
        self.combo_depth_avg.blockSignals(True)
        self.combo_depth_filter.blockSignals(True)
        self.combo_depth_source.blockSignals(True)
        self.ed_depth_draft.blockSignals(True)
        self.ed_depth_draft_ice.blockSignals(True)
        self.ed_depth_ice_thickness.blockSignals(True)
        self.ed_depth_ice_bottom.blockSignals(True)
        self.ed_depth_slush_bottom.blockSignals(True)

        # Vertical selected for display
        vertical = self.meas.verticals[self.vertical_idx]

        # Configure depth source options
        self.combo_depth_source.blockSignals(True)

        depth_source_options = [self.tr("Manual")]
        self.cb_depth_beam1.setEnabled(False)
        self.cb_depth_beam2.setEnabled(False)
        self.cb_depth_beam3.setEnabled(False)
        self.cb_depth_beam4.setEnabled(False)
        self.cb_depth_4beam_cs.setEnabled(False)

        if vertical.data.depths is not None:
            depth_source_options.append(self.tr("4-Beam Avg"))
            self.cb_depth_beam1.setEnabled(True)
            self.cb_depth_beam2.setEnabled(True)
            self.cb_depth_beam3.setEnabled(True)
            self.cb_depth_beam4.setEnabled(True)
            self.cb_depth_4beam_cs.setEnabled(True)

            # Setup for vertical beam
            if vertical.data.depths.vb_depths is not None:
                self.cb_depth_vert.blockSignals(True)
                self.cb_depth_vert.setEnabled(True)
                self.cb_depth_vert.blockSignals(False)
                depth_source_options.append(self.tr("Comp 4-Beam Preferred"))
                depth_source_options.append(self.tr("Vertical"))
                depth_source_options.append(self.tr("Comp Vertical Preferred"))
                self.cb_depth_vert.setEnabled(True)
                self.cb_depth_vert_cs.setEnabled(True)
            else:
                self.cb_depth_vert.setEnabled(False)
                self.cb_depth_vert_cs.setEnabled(False)

        self.combo_depth_source.clear()
        self.combo_depth_source.addItems(depth_source_options)

        if vertical.data.depths is not None:
            self.combo_depth_source.setEnabled(True)
            self.combo_depth_source.show()
            self.ed_depth_manual.show()
            self.txt_depth_manual_units.show()
            self.pb_depth_source_apply.show()
            self.pb_depth_source_apply_all.show()
            self.gb_depth_averaging.show()
            self.gb_depth_filter.show()
            self.gb_draft.show()

            depth_source = vertical.data.depths.selected
            depth_composite = vertical.data.depths.composite

            # Set depth reference
            self.combo_depth_avg.setEnabled(True)
            self.combo_depth_filter.setEnabled(True)
            self.ed_depth_manual.setText("")
            if depth_source == "bt_depths":
                self.ed_depth_manual.setEnabled(False)
                if depth_composite == "On":
                    self.combo_depth_source.setCurrentIndex(2)
                else:
                    self.combo_depth_source.setCurrentIndex(1)

            elif depth_source == "vb_depths":
                self.ed_depth_manual.setEnabled(False)
                if depth_composite == "On":
                    self.combo_depth_source.setCurrentIndex(4)
                else:
                    self.combo_depth_source.setCurrentIndex(3)

            elif depth_source == "man_depths":
                self.ed_depth_manual.setEnabled(True)
                self.combo_depth_source.setCurrentIndex(0)
                self.ed_depth_manual.setText(
                    "{:3.2f}".format(vertical.depth_m * self.units["L"])
                )
                self.txt_depth_manual_units.setText(self.units["label_L"])
                self.gb_depth_averaging.hide()
                self.gb_depth_filter.hide()
                self.gb_draft.hide()
                self.gb_draft_ice.hide()

            # Set depth average method
            depths_selected = getattr(vertical.data.depths, depth_source)
            self.combo_depth_avg.blockSignals(True)
            if vertical.data.depths.bt_depths.avg_method == "IDW":
                self.combo_depth_avg.setCurrentIndex(0)
            else:
                self.combo_depth_avg.setCurrentIndex(1)

            # Set depth filter method
            self.combo_depth_filter.blockSignals(True)
            if depths_selected.filter_type == "Median":
                self.combo_depth_filter.setCurrentIndex(0)
            elif depths_selected.filter_type == "TRDI":
                self.combo_depth_filter.setCurrentIndex(1)
            elif depths_selected.filter_type == "K-Means":
                self.combo_depth_filter.setCurrentIndex(2)
            else:
                self.combo_depth_filter.setCurrentIndex(3)

            # ADCP Depth Below W.S.
            self.ed_depth_draft.setText(
                "{:3.2f}".format(vertical.adcp_depth_below_ws_m * self.units["L"])
            )
            self.gb_draft.setWindowTitle(
                "ADCP Depth Below W.S " + self.units["label_L"]
            )
            if vertical.ws_condition == "Ice":
                # ADCP Depth Below Ice
                self.ed_depth_draft_ice.setText(
                    "{:3.2f}".format(vertical.adcp_depth_below_ice_m * self.units["L"])
                )
                self.gb_draft.setWindowTitle(
                    "ADCP Depth Below Ice " + self.units["label_L"]
                )
                self.gb_draft_ice.show()

        else:
            self.combo_depth_source.setCurrentIndex(0)
            self.combo_depth_source.setEnabled(True)
            self.ed_depth_manual.setText(
                "{:3.2f}".format(vertical.depth_m * self.units["L"])
            )
            self.gb_depth_averaging.hide()
            self.gb_depth_filter.hide()
            self.gb_draft.hide()
            self.gb_draft_ice.hide()

        # Show ice settings only if the water surface condition is ice
        if vertical.ws_condition == "Ice":
            # Ice Thickness
            self.ed_depth_ice_thickness.setText(
                "{:3.2f}".format(vertical.ice_thickness_m * self.units["L"])
            )
            self.gb_ice_thickness.setWindowTitle(
                "Ice Thickness " + self.units["label_L"]
            )

            # WS to Bottom of Ice
            self.ed_depth_ice_bottom.setText(
                "{:3.2f}".format(vertical.ws_to_ice_bottom_m * self.units["L"])
            )
            self.gb_ice_bottom.setWindowTitle(
                "W.S. to Bottom of Ice " + self.units["label_L"]
            )

            # WS to Bottom of Slush
            self.ed_depth_slush_bottom.setText(
                "{:3.2f}".format(vertical.ws_to_slush_bottom_m * self.units["L"])
            )
            self.gb_slush_bottom.setWindowTitle(
                "W.S. to Bottom of Slush " + self.units["label_L"]
            )

            self.gb_ice_thickness.show()
            self.gb_ice_bottom.show()
            self.gb_slush_bottom.show()

        else:
            self.gb_draft_ice.hide()
            self.gb_ice_thickness.hide()
            self.gb_ice_bottom.hide()
            self.gb_slush_bottom.hide()

        # Turn signals on
        self.combo_depth_source.blockSignals(False)
        self.combo_depth_avg.blockSignals(False)
        self.combo_depth_filter.blockSignals(False)
        self.ed_depth_draft.blockSignals(False)
        self.ed_depth_draft_ice.blockSignals(False)
        self.ed_depth_ice_thickness.blockSignals(False)
        self.ed_depth_ice_bottom.blockSignals(False)
        self.ed_depth_slush_bottom.blockSignals(False)

    def depth_table_clicked(self, row):
        """Changes plotted data and user settings to the vertical selected.

        Parameters
        ----------
        row: int
            Row clicked by user
        """

        tbl = self.table_depth

        # Check to see that the row selected is a valid vertical. This
        # avoids the discharge lines for mean section
        if self.valid_row(row):
            self.highlight_row(tbl, row)
            self.depth_settings_update()
            self.depth_plots()
            self.depth_subtab_data.setFocus()
            self.change = True

    def depth_plot_control(self):
        """Controls what data are displayed in the bottom plot."""

        self.cb_depth_cs.blockSignals(True)
        self.cb_depth_final_cs.blockSignals(True)
        self.cb_depth_4beam_cs.blockSignals(True)
        self.cb_depth_vert_cs.blockSignals(True)

        self.cb_depth_cs.setCheckState(QtCore.Qt.Unchecked)

        self.depth_plots()

        self.cb_depth_cs.blockSignals(False)
        self.cb_depth_final_cs.blockSignals(False)
        self.cb_depth_4beam_cs.blockSignals(False)
        self.cb_depth_vert_cs.blockSignals(False)

    def depth_plot_control_cs(self):
        """Controls what data are displayed in the bottom plot."""

        self.cb_depth_cs.blockSignals(True)
        self.cb_depth_final_cs.blockSignals(True)
        self.cb_depth_4beam_cs.blockSignals(True)
        self.cb_depth_vert_cs.blockSignals(True)

        if self.cb_depth_cs.isChecked():
            self.cb_depth_final_cs.setCheckState(QtCore.Qt.Unchecked)
            self.cb_depth_vert_cs.setCheckState(QtCore.Qt.Unchecked)
            self.cb_depth_4beam_cs.setCheckState(QtCore.Qt.Unchecked)

        self.depth_plots()

        self.cb_depth_cs.blockSignals(False)
        self.cb_depth_final_cs.blockSignals(False)
        self.cb_depth_4beam_cs.blockSignals(False)
        self.cb_depth_vert_cs.blockSignals(False)

    def depth_plots(self):
        """Creates graphics for depth tab."""

        with self.wait_cursor():
            # If the canvas has not been previously created, create the
            # canvas and add the widget.
            if self.depth_canvas is None:
                # Create the canvas
                self.depth_canvas = MplCanvas(
                    parent=self.graph_depth, width=8, height=2, dpi=80
                )
                # Assign layout to widget to allow auto scaling
                layout = QtWidgets.QVBoxLayout(self.graph_depth)
                # Adjust margins of layout to maximize graphic area
                layout.setContentsMargins(0, 0, 0, 0)
                # Add the canvas
                layout.addWidget(self.depth_canvas)
                # Initialize hidden toolbar for use by graphics controls
                self.depth_toolbar = NavigationToolbar(self.depth_canvas, self)
                self.depth_toolbar.hide()

            # Initialize the top figure and assign to the canvas
            self.depth_fig = Graphics(canvas=self.depth_canvas)

            if self.meas.verticals[self.vertical_idx].data.depths is not None:
                # Create the figure with the specified data
                self.depth_fig.create_depth_tab_graphs(
                    meas=self.meas,
                    vertical_idx=self.vertical_idx,
                    units=self.units,
                    b1=self.cb_depth_beam1.isChecked(),
                    b2=self.cb_depth_beam2.isChecked(),
                    b3=self.cb_depth_beam3.isChecked(),
                    b4=self.cb_depth_beam4.isChecked(),
                    vb=self.cb_depth_vert.isChecked(),
                    ds=False,
                    avg4_final=self.cb_depth_4beam_cs.isChecked(),
                    vb_final=self.cb_depth_vert_cs.isChecked(),
                    ds_final=False,
                    final=self.cb_depth_final_cs.isChecked(),
                    cs=self.cb_depth_cs.isChecked(),
                    x_axis_type="E",
                )
            else:
                self.depth_fig.fig.clear()

            # Draw canvas
            self.depth_canvas.draw()

            # Update list of figs
            self.canvases = [self.depth_canvas]
            self.figs = [self.depth_fig]
            self.toolbars = [self.depth_toolbar]
            self.fig_calls = [self.depth_plots]
            self.ui_parents = [i.parent() for i in self.canvases]
            self.figs_menu_connection()

            # Reset data cursor to work with new figure
            if self.actionData_Cursor.isChecked():
                self.data_cursor()
            self.depth_subtab_data.setFocus()

    def depth_comments_messages(self):
        """Displays comments and messages associated with depth filters in
        Messages tab.
        """

        # Clear comments and messages
        self.display_depth_comments.clear()

        if self.meas is not None:
            # Display each comment on a new line
            self.display_depth_comments.moveCursor(QtGui.QTextCursor.Start)
            for comment in self.meas.comments:
                self.display_depth_comments.textCursor().insertText(comment)
                self.display_depth_comments.moveCursor(QtGui.QTextCursor.End)
                self.display_depth_comments.textCursor().insertBlock()

            # Display each message on a new line
            self.messages_table(self.table_depth_messages, ["depths"])

            self.update_tab_icons()

    def depth_source_change(self, apply_all=False):
        """Changes the depth source.

        Parameters
        ----------
        apply_all: bool
            Indicates if the change should be applied to all data or only
            selected data
        """

        with self.wait_cursor():
            # Save previous discharge data
            old_discharge = self.meas.summary

            # Block signals
            self.combo_depth_source.blockSignals(True)

            # Get selected source
            text = self.combo_depth_source.currentText()

            # Change settings based on combo box selection
            settings = []
            if text == "4-Beam Avg":
                settings.append(("depthReference", "bt_depths"))
                settings.append(("depthComposite", "Off"))
                settings.append(("depthInterpolation", "Linear"))
            elif text == "Comp 4-Beam Preferred":
                settings.append(("depthReference", "bt_depths"))
                settings.append(("depthComposite", "On"))
                settings.append(("depthInterpolation", "Linear"))
            elif text == "Vertical":
                settings.append(("depthReference", "vb_depths"))
                settings.append(("depthComposite", "Off"))
                settings.append(("depthInterpolation", "Linear"))
            elif text == "Comp Vertical Preferred":
                settings.append(("depthReference", "vb_depths"))
                settings.append(("depthComposite", "On"))
                settings.append(("depthInterpolation", "Linear"))
            elif text == "Manual":
                manual_depth = float(self.ed_depth_manual.text()) / self.units["L"]

                # Check that manual depth is greater that ice depth
                if self.meas.verticals[self.vertical_idx].ws_to_slush_bottom_m > 0.0:
                    if apply_all:
                        for idx in self.meas.verticals_used_idx:
                            if manual_depth < self.meas.verticals[idx].ws_to_slush_bottom_m:
                                self.popup_message("Entered depth less than depth to bottom of slush ice. Change not applied.")
                                manual_depth = None
                                break
                    else:
                        if manual_depth < self.meas.verticals[self.vertical_idx].ws_to_slush_bottom_m:
                            manual_depth = self.meas.verticals[self.vertical_idx].ws_to_slush_bottom_m
                            self.popup_message("Entered depth less than depth to bottom of slush ice. Manual depth set to depth to bottom of slush ice.")

                if manual_depth is not None:
                    settings.append(("depthReference", "man_depths"))
                    settings.append(("depthManual", manual_depth,))
                    settings.append(("depthComposite", "Off"))

            # Apply change
            if apply_all:
                idx = None
            else:
                idx = self.vertical_idx
            self.meas.change_vertical_setting(settings, idx=idx)
            self.change = True

            # Update GUI
            self.depth_tab_update(old_discharge=old_discharge)
            self.depth_subtab_data.setFocus()

    def depth_avg_change(self, apply_all=False):
        """Changes the bt depth averaging method.

        Parameters
        ----------
        apply_all: bool
            Indicates if the change should be applied to all data or only
            selected data
        """
        with self.wait_cursor():
            old_discharge = self.meas.summary

            text = self.combo_depth_avg.currentText()
            settings = [("depthAvgMethod", text)]

            if apply_all:
                idx = None
            else:
                idx = self.vertical_idx
            self.meas.change_vertical_setting(settings, idx=idx)
            self.change = True

            # Update GUI
            self.depth_tab_update(old_discharge=old_discharge)
            self.depth_subtab_data.setFocus()

    def depth_filter_change(self, apply_all=False):
        """Change depth filter.

        Parameters
        ----------
        apply_all: bool
            Indicates if the change should be applied to all data or only
            selected data
        """

        with self.wait_cursor():
            # Get discharge prior to change
            old_discharge = self.meas.summary

            # New filter setting
            text = self.combo_depth_filter.currentText()
            settings = [("depthFilterType", text)]

            # Make change
            if apply_all:
                idx = None
            else:
                idx = self.vertical_idx
            self.meas.change_vertical_setting(settings, idx=idx)
            self.change = True

            # Update GUI
            self.depth_tab_update(old_discharge=old_discharge)
            self.depth_subtab_data.setFocus()

    def depth_draft_change(self, apply_all=False):
        """Change draft.

        Parameters
        ----------
        apply_all: bool
            Indicates if the change should be applied to all data or only
            selected data
        """
        # Discharge prior to changes
        old_discharge = self.meas.summary
        
        if self.meas.verticals[self.vertical_idx].ws_to_slush_bottom_m > 0:
            self.popup_message("The ADCP depth below water surface cannot be changed directly when ice is present. Change the the ADCP depth below ice, W.S. to Bottom of Ice, or the W.S. to bottom of slush.")
        else:


            # Change draft
            draft = self.check_numeric_input(self.ed_depth_draft, block=True, min_value=0)
            if draft is not None:
                draft = draft / self.units["L"]
                if apply_all:
                    idx = None
                else:
                    idx = self.vertical_idx
                self.meas.change_draft(new_draft=draft, idx=idx)
                self.change = True

        # Update GUI
        self.depth_tab_update(old_discharge=old_discharge)
        self.depth_subtab_data.setFocus()

    def depth_draft_ice_change(self, apply_all=False):
        """Change draft below ice.

        Parameters
        ----------
        apply_all: bool
            Indicates if the change should be applied to all data or only
        selected data
        """

        # Discharge prior to changes
        old_discharge = self.meas.summary

        # Change draft
        draft_ice = self.check_numeric_input(
            self.ed_depth_draft_ice, block=True, min_value=0
        )
        if draft_ice is not None:
            draft_ice = draft_ice / self.units["L"]
            if apply_all:
                idx = None
            else:
                idx = self.vertical_idx
            self.meas.change_draft_ice(new_draft=draft_ice, idx=idx)
            self.change = True

        # Update GUI
        self.depth_tab_update(old_discharge=old_discharge)
        self.depth_subtab_data.setFocus()

    def depth_ice_thickness_change(self, apply_all=False):
        """Change ice thickness. Note: ice thickness is for reference only
        and does not change computations.

        Parameters
        ----------
        apply_all: bool
            Indicates if the change should be applied to all data or only
            selected data
        """
        # Discharge prior to changes
        old_discharge = self.meas.summary

        # Change draft
        value = self.check_numeric_input(
            self.ed_depth_ice_thickness, block=True, min_value=0
        )
        if value is not None:
            value = value / self.units["L"]
            if apply_all:
                idx = None
            else:
                idx = self.vertical_idx
            self.meas.change_ice_thickness(value=value, idx=idx)
            self.change = True

        # Update GUI
        self.depth_tab_update(old_discharge=old_discharge)
        self.depth_subtab_data.setFocus()

    def depth_ice_bottom_change(self, apply_all=False):
        """Change value of water surface to bottom of ice.

        Parameters
        ----------
        apply_all: bool
            Indicates if the change should be applied to all data or only
        selected data
        """

        # Discharge prior to changes
        old_discharge = self.meas.summary

        # Change depth to ice bottom
        value = self.check_numeric_input(
            self.ed_depth_ice_bottom, block=True, min_value=0
        )
        if value is not None:
            value = value / self.units["L"]
            if apply_all:
                idx = None
            else:
                idx = self.vertical_idx
            self.meas.change_ice_bottom(value=value, idx=idx)
            self.change = True

        # Update GUI
        self.depth_tab_update(old_discharge=old_discharge)
        self.depth_subtab_data.setFocus()

    def depth_slush_bottom_change(self, apply_all=False):
        """Change value of water surface to bottom of ice.

                Parameters
                ----------
                apply_all: bool
                    Indicates if the change should be applied to all data or only
        selected data
        """

        # Discharge prior to changes
        old_discharge = self.meas.summary

        # Change depth to slush bottom
        value = self.check_numeric_input(
            self.ed_depth_slush_bottom, block=True, min_value=0
        )
        if value is not None:
            value = value / self.units["L"]
            if apply_all:
                idx = None
            else:
                idx = self.vertical_idx
            self.meas.change_slush_bottom(value=value, idx=idx)
            self.change = True

        # Update GUI
        self.depth_tab_update(old_discharge=old_discharge)
        self.depth_subtab_data.setFocus()

    # WT Tab
    # ======
    def wt_tab_initial(self, old_discharge=None):
        """Initialize and configure wt tab."""

        # Setup data table
        tbl = self.table_wt
        table_header = [
            self.tr("Station \n Number"),
            self.tr("Location"),
            self.tr("Number \n Valid \n Ens."),
            self.tr("Number \n Valid \n Cells"),
            self.tr("Orig Data \n Invalid \n %"),
            self.tr("<4 Beam \n Invalid \n %"),
            self.tr("Error Vel \n Invalid \n %"),
            self.tr("Vert Vel \n Invalid \n %"),
            self.tr("SNR \n Invalid \n %"),
            self.tr("Velocity \n Mag. \n" + self.units["label_V"]),
            self.tr("Velocity \n Mag. \n CV (%)"),
            self.tr("Velocity \n Dir. \n (deg)"),
            self.tr("Velocity \n Dir. \n SD (deg)"),
            self.tr("Velocity \n Angle \n (deg)"),
            self.tr("Flow \n Angle \n Coef."),
            self.tr("Normal \n Velocity \n" + self.units["label_V"]),
            self.tr("Discharge \n Previous \n" + self.units["label_Q"]),
            self.tr("Discharge \n Now \n" + self.units["label_Q"]),
            self.tr("Discharge \n Change \n %"),
        ]

        ncols = len(table_header)
        nrows = self.meas.summary.shape[0] - 2
        tbl.setRowCount(nrows)
        tbl.setColumnCount(ncols)
        tbl.setHorizontalHeaderLabels(table_header)
        tbl.horizontalHeader().setFont(self.font_bold)
        tbl.verticalHeader().hide()
        tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)

        # Automatically resize rows and columns
        tbl.resizeColumnsToContents()
        tbl.resizeRowsToContents()

        # Initialize connections
        if not self.wt_initialized:
            tbl.cellClicked.connect(self.wt_table_clicked)

            self.rb_wt_contour.setChecked(True)

            # Connect radio buttons
            self.rb_wt_contour.toggled.connect(self.wt_radiobutton_control)
            self.rb_wt_beam.toggled.connect(self.wt_radiobutton_control)
            self.rb_wt_error.toggled.connect(self.wt_radiobutton_control)
            self.rb_wt_vert.toggled.connect(self.wt_radiobutton_control)
            self.rb_wt_snr.toggled.connect(self.wt_radiobutton_control)
            self.rb_wt_speed.toggled.connect(self.wt_radiobutton_control)
            self.rb_wt_magdir.toggled.connect(self.wt_profile_control)
            self.rb_wt_rssi.toggled.connect(self.wt_profile_control)

            # Vel source
            self.wt_vel_source_grp = ComboEdGrp(
                combo=self.combo_wt_vel_source,
                ed=self.ed_wt_vel_source,
                pb_apply=self.pb_wt_vel_source_apply,
                pb_apply_all=self.pb_wt_vel_source_apply_all,
                change=self.wt_vel_source_change,
                previous=self.wt_settings_update,
            )
            self.combo_wt_vel_source.activated.connect(
                self.wt_vel_source_grp.enable_apply
            )
            self.ed_wt_vel_source.textChanged.connect(
                self.wt_vel_source_grp.enable_apply
            )
            self.pb_wt_vel_source_apply.clicked.connect(self.wt_vel_source_grp.apply)
            self.pb_wt_vel_source_apply_all.clicked.connect(
                self.wt_vel_source_grp.apply_all
            )

            # Min ensembles
            self.wt_min_ens_grp = EdGrp(
                ed=self.ed_wt_min_ens,
                pb_apply=self.pb_wt_min_ens_apply,
                pb_apply_all=self.pb_wt_min_ens_apply_all,
                change=self.wt_min_ens_change,
                min_value=1,
            )
            self.ed_wt_min_ens.textChanged.connect(self.wt_min_ens_grp.enable_apply)
            self.pb_wt_min_ens_apply.clicked.connect(self.wt_min_ens_grp.apply)
            self.pb_wt_min_ens_apply_all.clicked.connect(self.wt_min_ens_grp.apply_all)

            # Excluded below
            self.wt_excluded_below_grp = ComboEdGrp(
                combo=self.combo_wt_excluded_below,
                ed=self.ed_wt_excluded_below,
                pb_apply=self.pb_wt_excluded_below_apply,
                pb_apply_all=self.pb_wt_excluded_below_apply_all,
                change=self.wt_excluded_below_change,
                min_value=0,
            )
            self.combo_wt_excluded_below.activated.connect(
                self.wt_excluded_below_grp.enable_apply
            )
            self.ed_wt_excluded_below.textChanged.connect(
                self.wt_excluded_below_grp.enable_apply
            )
            self.pb_wt_excluded_below_apply.clicked.connect(
                self.wt_excluded_below_grp.apply
            )
            self.pb_wt_excluded_below_apply_all.clicked.connect(
                self.wt_excluded_below_grp.apply_all
            )

            # Excluded above
            self.wt_excluded_above_grp = ComboEdGrp(
                combo=self.combo_wt_excluded_above,
                ed=self.ed_wt_excluded_above,
                pb_apply=self.pb_wt_excluded_above_apply,
                pb_apply_all=self.pb_wt_excluded_above_apply_all,
                change=self.wt_excluded_above_change,
                min_value=0,
            )
            self.combo_wt_excluded_above.activated.connect(
                self.wt_excluded_above_grp.enable_apply
            )
            self.ed_wt_excluded_above.textChanged.connect(
                self.wt_excluded_above_grp.enable_apply
            )
            self.pb_wt_excluded_above_apply.clicked.connect(
                self.wt_excluded_above_grp.apply
            )
            self.pb_wt_excluded_above_apply_all.clicked.connect(
                self.wt_excluded_above_grp.apply_all
            )

            # 3 beam
            self.wt_3beam_grp = ComboGrp(
                combo=self.combo_wt_3beam,
                pb_apply=self.pb_wt_3beam_apply,
                pb_apply_all=self.pb_wt_3beam_apply_all,
                change=self.wt_3beam_change,
            )
            self.combo_wt_3beam.activated.connect(self.wt_3beam_grp.enable_apply)
            self.pb_wt_3beam_apply.clicked.connect(self.wt_3beam_grp.apply)
            self.pb_wt_3beam_apply_all.clicked.connect(self.wt_3beam_grp.apply_all)

            # Error vel
            self.wt_error_vel_grp = ComboEdGrp(
                combo=self.combo_wt_error_velocity,
                ed=self.ed_wt_error_vel_threshold,
                pb_apply=self.pb_wt_error_vel_apply,
                pb_apply_all=self.pb_wt_error_vel_apply_all,
                change=self.wt_error_vel_change,
            )
            self.combo_wt_error_velocity.activated.connect(
                self.wt_error_vel_grp.enable_apply
            )
            self.ed_wt_error_vel_threshold.textChanged.connect(
                self.wt_error_vel_grp.enable_apply
            )
            self.pb_wt_error_vel_apply.clicked.connect(self.wt_error_vel_grp.apply)
            self.pb_wt_error_vel_apply_all.clicked.connect(
                self.wt_error_vel_grp.apply_all
            )

            # Vertical vel
            self.wt_vert_vel_grp = ComboEdGrp(
                combo=self.combo_wt_vert_velocity,
                ed=self.ed_wt_vert_vel_threshold,
                pb_apply=self.pb_wt_vert_vel_apply,
                pb_apply_all=self.pb_wt_vert_vel_apply_all,
                change=self.wt_vert_vel_change,
            )
            self.combo_wt_vert_velocity.activated.connect(
                self.wt_vert_vel_grp.enable_apply
            )
            self.ed_wt_vert_vel_threshold.textChanged.connect(
                self.wt_vert_vel_grp.enable_apply
            )
            self.pb_wt_vert_vel_apply.clicked.connect(self.wt_vert_vel_grp.apply)
            self.pb_wt_vert_vel_apply_all.clicked.connect(
                self.wt_vert_vel_grp.apply_all
            )

            # SNR
            self.wt_snr_grp = ComboGrp(
                combo=self.combo_wt_snr,
                pb_apply=self.pb_wt_snr_apply,
                pb_apply_all=self.pb_wt_snr_apply_all,
                change=self.wt_snr_change,
            )
            self.combo_wt_snr.activated.connect(self.wt_snr_grp.enable_apply)
            self.pb_wt_snr_apply.clicked.connect(self.wt_snr_grp.apply)
            self.pb_wt_snr_apply_all.clicked.connect(self.wt_snr_grp.apply_all)

            # Flow Angle
            self.wt_flow_angle_grp = ComboEdGrp(
                combo=self.combo_wt_flow_angle,
                ed=self.ed_wt_flow_angle,
                pb_apply=self.pb_wt_flow_angle_apply,
                pb_apply_all=self.pb_wt_flow_angle_apply_all,
                change=self.wt_flow_angle_change,
            )
            self.combo_wt_flow_angle.activated.connect(
                self.wt_flow_angle_grp.enable_apply
            )
            self.ed_wt_flow_angle.textChanged.connect(
                self.wt_flow_angle_grp.enable_apply
            )
            self.pb_wt_flow_angle_apply.clicked.connect(self.wt_flow_angle_grp.apply)
            self.pb_wt_flow_angle_apply_all.clicked.connect(
                self.wt_flow_angle_grp.apply_all
            )

            self.wt_initialized = True

        # Set units
        self.txt_wt_vel_source.setText(self.units["label_V"])
        self.gb_wt_excluded_below.setTitle(
            self.tr("Excluded Below Transducer " + self.units["label_L"])
        )
        self.gb_wt_excluded_above.setTitle(
            self.tr("Excluded Above Sidelobe " + self.units["label_L"])
        )
        self.gb_wt_error_velocity.setTitle(
            self.tr("Error Velocity" + self.units["label_V"])
        )
        self.gb_wt_vertical_velocity.setTitle(
            self.tr("Vertical Velocity" + self.units["label_V"])
        )

        if self.meas.verticals[self.first_idx].data.inst.manufacturer != "SonTek":
            self.rb_wt_snr.hide()
        else:
            self.rb_wt_snr.show()

        self.wt_tab_update(old_discharge=old_discharge)

    def wt_tab_update(self, old_discharge=None):
        """Updates the display of data in the tab.

        Parameters
        ----------
        old_discharge: dataframe
            Pandas dataframe of summary data
        """

        # Old discharge
        if old_discharge is None:
            old_discharge = self.meas.summary

        # Disable apply buttons
        self.pb_wt_vel_source_apply.setEnabled(False)
        self.pb_wt_vel_source_apply_all.setEnabled(False)
        self.pb_wt_min_ens_apply.setEnabled(False)
        self.pb_wt_min_ens_apply_all.setEnabled(False)
        self.pb_wt_excluded_below_apply.setEnabled(False)
        self.pb_wt_excluded_below_apply_all.setEnabled(False)
        self.pb_wt_excluded_above_apply.setEnabled(False)
        self.pb_wt_excluded_above_apply_all.setEnabled(False)
        self.pb_wt_3beam_apply.setEnabled(False)
        self.pb_wt_3beam_apply_all.setEnabled(False)
        self.pb_wt_error_vel_apply.setEnabled(False)
        self.pb_wt_error_vel_apply_all.setEnabled(False)
        self.pb_wt_vert_vel_apply.setEnabled(False)
        self.pb_wt_vert_vel_apply_all.setEnabled(False)
        self.pb_wt_snr_apply.setEnabled(False)
        self.pb_wt_snr_apply_all.setEnabled(False)
        self.pb_wt_flow_angle_apply.setEnabled(False)
        self.pb_wt_flow_angle_apply_all.setEnabled(False)

        # Update
        self.wt_table_update(old_discharge=old_discharge)
        self.wt_settings_update()
        self.wt_plots()
        self.wt_comments_messages()
        self.wt_subtab_data.setFocus()

        # Setup list for use by graphic controls
        self.canvases = [self.wt_filter_canvas, self.wt_profile_canvas]
        self.figs = [self.wt_filter_fig, self.wt_profile_fig]
        self.toolbars = [self.wt_filter_toolbar, self.wt_profile_toolbar]
        self.fig_calls = [self.wt_filter_plots, self.wt_profile_control]
        self.ui_parents = [i.parent() for i in self.canvases]
        self.figs_menu_connection()

    def wt_table_update(self, old_discharge):
        """Updates the data displayed in the wt table.

        Parameters
        ----------
        old_discharge: dataframe
            Pandas dataframe of summary data
        """

        with self.wait_cursor():
            # Set tbl variable
            tbl = self.table_wt

            # Populate each row
            for row in range(tbl.rowCount()):
                # Initialize variables
                num_useable_cells = np.nan
                num_snr_invalid = np.nan
                num_vert_invalid = np.nan
                num_error_invalid = np.nan
                num_orig_invalid = np.nan
                num_beam_invalid = np.nan

                # Identify vertical
                if self.meas.summary["Station Number"][row] == np.floor(
                    self.meas.summary["Station Number"][row]
                ):
                    # Get vertical
                    vertical_id = self.meas.verticals_used_idx[
                        int(self.meas.summary["Station Number"][row] - 1)
                    ]
                    vertical = self.meas.verticals[vertical_id]

                    # Identify a measured vertical
                    if self.meas.summary["Duration"][row] > 0:
                        # Prep data for table
                        valid_data = vertical.data.w_vel.valid_data

                        # Data excluded by user is not considered a useable cell
                        useable_cells = vertical.data.w_vel.valid_data[6, :, :]
                        num_useable_cells = np.nansum(
                            np.nansum(useable_cells.astype(int))
                        )
                        num_orig_invalid = np.nansum(
                            np.nansum(
                                np.logical_not(valid_data[1, :, :][useable_cells])
                            )
                        )
                        num_beam_invalid = np.nansum(
                            np.nansum(
                                np.logical_not(valid_data[5, :, :][useable_cells])
                            )
                        )
                        num_error_invalid = np.nansum(
                            np.nansum(
                                np.logical_not(valid_data[2, :, :][useable_cells])
                            )
                        )
                        num_vert_invalid = np.nansum(
                            np.nansum(
                                np.logical_not(valid_data[3, :, :][useable_cells])
                            )
                        )
                        num_snr_invalid = np.nansum(
                            np.nansum(
                                np.logical_not(valid_data[7, :, :][useable_cells])
                            )
                        )

                # Station Number
                col = 0
                if np.isnan(self.meas.summary["Station Number"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                elif np.modf(self.meas.summary["Station Number"][row])[0] == 0:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:3.0f}".format(self.meas.summary["Station Number"][row])
                        ),
                    )
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:3.1f}".format(self.meas.summary["Station Number"][row])
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Location
                col += 1
                if np.isnan(self.meas.summary["Location"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:6.2f}".format(
                                self.meas.summary["Location"][row] * self.units["L"]
                            )
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Number Valid Profiles
                col += 1
                if np.isnan(self.meas.summary["Number Valid Profiles"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:4.0f}".format(
                                self.meas.summary["Number Valid Profiles"][row]
                            )
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                if np.modf(self.meas.summary["Station Number"][row])[0] == 0:
                    if (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.w_vel["all_invalid"]["All: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                    elif (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.w_vel["total_warning"]["All: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                    elif (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.w_vel["total_caution"]["All: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                    else:
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))

                    tbl.item(row, col).setToolTip(
                        self.tr(self.wt_create_tooltip(row, col))
                    )

                # Number Valid Cells
                col += 1
                if np.isnan(self.meas.summary["Number Cells"][row]):
                    item = ""
                else:
                    item = "{:4.0f}".format(self.meas.summary["Number Cells"][row])
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                if (
                    self.meas.summary["Number Cells"][row] == 0
                    and tbl.rowCount() - 1 > row > 0
                ):
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                    tbl.item(row, col).setToolTip(
                        "There are no valid cells in the mean profile."
                    )

                # Original
                col += 1
                if np.isnan(num_orig_invalid) or num_useable_cells == 0:
                    item = ""
                else:
                    item = "{:4.2f}".format(
                        (num_orig_invalid / num_useable_cells) * 100
                    )
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                if item != "":
                    if (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.w_vel["total_warning"]["Original: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                    elif (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.w_vel["total_caution"]["Original: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                    else:
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))
                    tbl.item(row, col).setToolTip(
                        self.tr(self.wt_create_tooltip(row, col))
                    )

                # <4 Beam
                col += 1
                if np.isnan(num_beam_invalid) or num_useable_cells == 0:
                    item = ""
                else:
                    item = "{:4.2f}".format(
                        (num_beam_invalid / num_useable_cells) * 100
                    )
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                if item != "":
                    if (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.w_vel["total_warning"]["3Beams: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                    elif (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.w_vel["total_caution"]["3Beams: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                    else:
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))
                    tbl.item(row, col).setToolTip(
                        self.tr(self.wt_create_tooltip(row, col))
                    )

                # Error Vel
                col += 1
                if np.isnan(num_error_invalid) or num_useable_cells == 0:
                    item = ""
                else:
                    item = "{:4.2f}".format(
                        (num_error_invalid / num_useable_cells) * 100
                    )
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                if item != "":
                    if (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.w_vel["total_warning"]["ErrorVel: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                    elif (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.w_vel["total_caution"]["ErrorVel: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                    else:
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))
                    tbl.item(row, col).setToolTip(
                        self.tr(self.wt_create_tooltip(row, col))
                    )

                # Vertical Vel
                col += 1
                if np.isnan(num_vert_invalid) or num_useable_cells == 0:
                    item = ""
                else:
                    item = "{:4.2f}".format(
                        (num_vert_invalid / num_useable_cells) * 100
                    )
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                if item != "":
                    if (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.w_vel["total_warning"]["VertVel: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                    elif (
                        self.meas.summary["Station Number"][row]
                        in self.meas.qa.w_vel["total_caution"]["VertVel: "]
                    ):
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                    else:
                        tbl.item(row, col).setBackground(QtGui.QColor(255, 255, 255))
                    tbl.item(row, col).setToolTip(
                        self.tr(self.wt_create_tooltip(row, col))
                    )

                # SNR
                col += 1
                if np.isnan(num_snr_invalid) or num_useable_cells == 0:
                    item = ""
                else:
                    item = "{:4.2f}".format((num_snr_invalid / num_useable_cells) * 100)
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                if item != "":
                    try:
                        if (
                            self.meas.summary["Station Number"][row]
                            in self.meas.qa.w_vel["total_warning"]["SNR: "]
                        ):
                            tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                        elif (
                            self.meas.summary["Station Number"][row]
                            in self.meas.qa.w_vel["total_caution"]["SNR: "]
                        ):
                            tbl.item(row, col).setBackground(QtGui.QColor(255, 204, 0))
                        else:
                            tbl.item(row, col).setBackground(
                                QtGui.QColor(255, 255, 255)
                            )
                        tbl.item(row, col).setToolTip(
                            self.tr(self.wt_create_tooltip(row, col))
                        )
                    except KeyError:
                        pass

                # Vel Magnitude
                col += 1
                if np.isnan(self.meas.summary["Velocity Magnitude"][row]):
                    item = ""
                else:
                    item = "{:4.2f}".format(
                        self.meas.summary["Velocity Magnitude"][row] * self.units["V"]
                    )
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
                if item != "":
                    if vertical.velocity_method == "Manual":
                        tbl.item(row, col).setForeground(QtGui.QColor(230, 138, 0))
                        tbl.item(row, col).setFont(self.font_bold)

                # Vel Magnitude CV
                col += 1
                if np.isnan(self.meas.summary["Velocity Magnitude CV"][row]):
                    item = ""
                else:
                    item = "{:4.2f}".format(
                        self.meas.summary["Velocity Magnitude CV"][row]
                    )
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Vel Direction
                col += 1
                if np.isnan(self.meas.summary["Velocity Direction"][row]):
                    item = ""
                else:
                    item = "{:4.1f}".format(
                        self.meas.summary["Velocity Direction"][row]
                    )
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Vel Direction StdDev
                col += 1
                if np.isnan(self.meas.summary["Velocity Direction StdDev"][row]):
                    item = ""
                else:
                    item = "{:4.2f}".format(
                        self.meas.summary["Velocity Direction StdDev"][row]
                    )
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Velocity Angle
                col += 1
                if np.isnan(self.meas.summary["Velocity Angle"][row]):
                    item = ""
                else:
                    item = "{:4.1f}".format(self.meas.summary["Velocity Angle"][row])
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                if self.meas.discharge_method == "Magnitude":
                    tbl.setColumnHidden(col, False)
                else:
                    tbl.setColumnHidden(col, True)

                # Flow Angle Coefficient
                col += 1
                if np.isnan(self.meas.summary["Flow Angle Coefficient"][row]):
                    item = ""
                else:
                    item = "{:4.3f}".format(
                        self.meas.summary["Flow Angle Coefficient"][row]
                    )
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Mean Velocity
                col += 1
                if np.isnan(self.meas.summary["Normal Velocity"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:4.3f}".format(
                                self.meas.summary["Normal Velocity"][row]
                                * self.units["V"]
                            )
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Discharge before changes
                col += 1
                if np.isnan(old_discharge["Q"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:8}".format(
                                self.q_digits(old_discharge["Q"][row] * self.units["Q"])
                            )
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Discharge after changes
                col += 1
                if np.isnan(self.meas.summary["Q"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:8}".format(
                                self.q_digits(
                                    self.meas.summary["Q"][row] * self.units["Q"]
                                )
                            )
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Percent difference in old and new discharges
                col += 1
                if np.isnan(self.meas.summary["Q"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    if np.abs(old_discharge["Q"][row]) > 0:
                        per_change = (
                            (self.meas.summary["Q"][row] - old_discharge["Q"][row])
                            / old_discharge["Q"][row]
                        ) * 100
                        tbl.setItem(
                            row,
                            col,
                            QtWidgets.QTableWidgetItem("{:3.1f}".format(per_change)),
                        )
                    else:
                        tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Set highlight selected vertical
            self.highlight_row(tbl, self.main_row_idx - 1)

            tbl.resizeColumnsToContents()
            tbl.resizeRowsToContents()
            tbl.setFocus()

    def wt_create_tooltip(self, row, column):
        """Create a tooltip for the row and column in the table based on the
        QA results.

                Parameters
                ----------
                row: int
                    Row in table
                column: int
                    Column in table

                Returns
                -------
                tt: str
                    Text of tooltip
        """

        # Initialize variables
        tt = ""

        # From column determine category of QA results to use
        if column == 4:
            cat = "Original: "
        elif column == 5:
            cat = "3Beams: "
        elif column == 6:
            cat = "ErrorVel: "
        elif column == 7:
            cat = "VertVel: "
        elif column == 8:
            cat = "SNR"
        else:
            cat = "All: "

        # Create tooltip
        if cat is not None:
            tt = "".join(
                self.tooltip_qa_message(
                    qa_data=self.meas.qa.w_vel,
                    cat=cat,
                    vertical_id=int(self.meas.summary["Station Number"][row]),
                    total_threshold_warning=self.meas.qa.percent_valid_warning_threshold,
                    total_threshold_caution=self.meas.qa.percent_valid_caution_threshold,
                )
            )
        return tt

    def wt_table_clicked(self, row):
        """Changes plotted data and user settings to the vertical selected.

        Parameters
        ----------
        row: int
            Row clicked by user
        """

        tbl = self.table_wt

        # Check to see that the row selected is a valid vertical. This
        # avoids the discharge lines for mean section
        if self.valid_row(row):
            self.highlight_row(tbl, row)
            self.wt_settings_update()
            self.wt_plots()
            self.wt_subtab_data.setFocus()
            self.change = True

    def wt_settings_update(self):
        """Update the user settings for the selected vertical."""
        # Vertical selected for display
        vertical = self.meas.verticals[self.vertical_idx]

        # Turn signals off
        self.combo_wt_vel_source.blockSignals(True)
        self.ed_wt_vel_source.blockSignals(True)
        self.ed_wt_min_ens.blockSignals(True)
        self.combo_wt_excluded_below.blockSignals(True)
        self.ed_wt_excluded_below.blockSignals(True)
        self.combo_wt_excluded_above.blockSignals(True)
        self.ed_wt_excluded_above.blockSignals(True)
        self.combo_wt_3beam.blockSignals(True)
        self.combo_wt_error_velocity.blockSignals(True)
        self.ed_wt_error_vel_threshold.blockSignals(True)
        self.combo_wt_vert_velocity.blockSignals(True)
        self.ed_wt_vert_vel_threshold.blockSignals(True)
        self.combo_wt_snr.blockSignals(True)
        self.combo_wt_flow_angle.blockSignals(True)
        self.ed_wt_flow_angle.blockSignals(True)

        # Velocity Source Options
        wt_source_options = [self.tr("Manual")]

        # Min No Ens
        self.ed_wt_min_ens.setText(
            "{:3.0f}".format(vertical.cell_minimum_valid_ensembles)
        )

        # Velocity source
        wt_source_options.append(self.tr("ADCP"))
        self.combo_wt_vel_source.clear()
        self.combo_wt_vel_source.addItems(wt_source_options)
        if vertical.mean_speed_source == "ADCP":
            self.combo_wt_vel_source.setCurrentIndex(1)
            self.ed_wt_vel_source.setText("")
        elif vertical.mean_speed_source == "Manual":
            self.combo_wt_vel_source.setCurrentIndex(0)
            self.ed_wt_vel_source.setText(
                "{:3.2f}".format(vertical.mean_speed_mps * self.units["V"])
            )

        if vertical.data.w_vel is not None and vertical.mean_speed_source == "ADCP":
            self.gb_wt_vel_source.show()
            self.gb_wt_min_ens.show()
            self.gb_wt_excluded_above.show()
            self.gb_wt_excluded_below.show()
            self.gb_wt_3beam.show()
            self.gb_wt_error_velocity.show()
            self.gb_wt_vertical_velocity.show()
            self.gb_wt_snr.show()

            # Excluded Top
            if vertical.data.w_vel.excluded_top_type == "Distance":
                self.combo_wt_excluded_below.setCurrentIndex(0)
                if np.isnan(vertical.data.w_vel.excluded_top):
                    self.ed_wt_excluded_below.setText("")
                else:
                    self.ed_wt_excluded_below.setText(
                        "{:3.2f}".format(
                            vertical.data.w_vel.excluded_top * self.units["L"]
                        )
                    )
            elif vertical.data.w_vel.excluded_top_type == "Cells":
                self.combo_wt_excluded_below.setCurrentIndex(1)
                if np.isnan(vertical.data.w_vel.excluded_top):
                    self.ed_wt_excluded_below.setText("")
                else:
                    self.ed_wt_excluded_below.setText(
                        "{:3.0f}".format(vertical.data.w_vel.excluded_top)
                    )

            # Excluded Bottom
            if vertical.data.w_vel.excluded_bottom_type == "Distance":
                self.combo_wt_excluded_above.setCurrentIndex(0)
                if np.isnan(vertical.data.w_vel.excluded_bottom):
                    self.ed_wt_excluded_above.setText("")
                else:
                    self.ed_wt_excluded_above.setText(
                        "{:3.2f}".format(
                            vertical.data.w_vel.excluded_bottom * self.units["L"]
                        )
                    )
            elif vertical.data.w_vel.excluded_bottom_type == "Cells":
                self.combo_wt_excluded_above.setCurrentIndex(1)
                if np.isnan(vertical.data.w_vel.excluded_bottom):
                    self.ed_wt_excluded_above.setText("")
                else:
                    self.ed_wt_excluded_above.setText(
                        "{:3.0f}".format(vertical.data.w_vel.excluded_bottom)
                    )

            # Beam filter
            if vertical.data.w_vel.beam_filter < 0:
                self.combo_wt_3beam.setCurrentIndex(0)
            elif vertical.data.w_vel.beam_filter == 3:
                self.combo_wt_3beam.setCurrentIndex(1)
            elif vertical.data.w_vel.beam_filter == 4:
                self.combo_wt_3beam.setCurrentIndex(2)
            else:
                self.combo_wt_3beam.setCurrentIndex(0)

            # Error vel filter
            index = self.combo_wt_error_velocity.findText(
                vertical.data.w_vel.d_filter, QtCore.Qt.MatchFixedString
            )
            self.combo_wt_error_velocity.setCurrentIndex(index)

            if vertical.data.w_vel.d_filter == "Manual":
                threshold = "{:3.4f}".format(
                    vertical.data.w_vel.d_filter_thresholds * self.units["V"]
                )
                self.ed_wt_error_vel_threshold.setText(threshold)
                self.ed_wt_error_vel_threshold.setEnabled(True)
            else:
                self.ed_wt_error_vel_threshold.setText("")

            # Vertical vel filter
            index = self.combo_wt_vert_velocity.findText(
                vertical.data.w_vel.w_filter, QtCore.Qt.MatchFixedString
            )
            self.combo_wt_vert_velocity.setCurrentIndex(index)

            if vertical.data.w_vel.w_filter == "Manual":
                threshold = "{:3.4f}".format(
                    vertical.data.w_vel.w_filter_thresholds * self.units["V"]
                )
                self.ed_wt_vert_vel_threshold.setText(threshold)
                self.ed_wt_vert_vel_threshold.setEnabled(True)
            else:
                self.ed_wt_vert_vel_threshold.setText("")

            # SNR Filter
            if vertical.data.inst.manufacturer == "SonTek":
                if vertical.data.w_vel.snr_filter == "Auto":
                    self.combo_wt_snr.setCurrentIndex(0)
                else:
                    self.combo_wt_snr.setCurrentIndex(1)
            else:
                self.gb_wt_snr.hide()

            if vertical.velocity_method == "Magnitude":
                # Flow angle
                self.gb_wt_flow_angle.show()
                self.ed_wt_flow_angle.show()
                self.pb_wt_flow_angle_apply.show()
                self.pb_wt_flow_angle_apply_all.show()
                self.combo_wt_flow_angle.setCurrentIndex(0)
                if np.isnan(vertical.flow_angle_coefficient):
                    self.ed_wt_flow_angle.setText("")
                else:
                    self.ed_wt_flow_angle.setText(
                        "{:3.3f}".format(vertical.flow_angle_coefficient)
                    )
            else:
                self.gb_wt_flow_angle.hide()
        else:
            self.combo_wt_vel_source.clear()
            self.combo_wt_vel_source.addItems(wt_source_options)
            self.combo_wt_vel_source.setCurrentIndex(0)
            self.gb_wt_min_ens.hide()
            self.gb_wt_excluded_above.hide()
            self.gb_wt_excluded_below.hide()
            self.gb_wt_3beam.hide()
            self.gb_wt_error_velocity.hide()
            self.gb_wt_vertical_velocity.hide()
            self.gb_wt_snr.hide()

        # Turn signals on
        self.combo_wt_vel_source.blockSignals(False)
        self.ed_wt_vel_source.blockSignals(False)
        self.ed_wt_min_ens.blockSignals(False)
        self.combo_wt_excluded_below.blockSignals(False)
        self.ed_wt_excluded_below.blockSignals(False)
        self.combo_wt_excluded_above.blockSignals(False)
        self.ed_wt_excluded_above.blockSignals(False)
        self.combo_wt_3beam.blockSignals(False)
        self.combo_wt_error_velocity.blockSignals(False)
        self.ed_wt_error_vel_threshold.blockSignals(False)
        self.combo_wt_vert_velocity.blockSignals(False)
        self.ed_wt_vert_vel_threshold.blockSignals(False)
        self.combo_wt_snr.blockSignals(False)
        self.combo_wt_flow_angle.blockSignals(False)
        self.ed_wt_flow_angle.blockSignals(False)

    def wt_plots(self):
        """Controls what plots are shown based on selected vertical."""

        self.wt_filter_plots()
        self.wt_profile_control()

        # Setup list for use by graphic controls
        self.canvases = [self.wt_filter_canvas, self.wt_profile_canvas]
        self.figs = [self.wt_filter_fig, self.wt_profile_fig]
        self.toolbars = [self.wt_filter_toolbar, self.wt_profile_toolbar]
        self.fig_calls = [self.wt_filter_plots, self.wt_profile_control]
        self.ui_parents = [i.parent() for i in self.canvases]
        self.figs_menu_connection()

        # Reset data cursor to work with new figure
        if self.actionData_Cursor.isChecked():
            self.data_cursor()

    def wt_radiobutton_control(self):
        """Identifies a change in radio buttons and calls the plot routine
        to update the graph.
        """
        with self.wait_cursor():
            if self.sender().isChecked():
                self.wt_filter_plots()

    def wt_profile_control(self):
        """Identifies a change in the data to be shown in the profile plot."""
        if self.rb_wt_magdir.isChecked():
            self.wt_magdir_plot()
        else:
            self.wt_rssi_plot()

    def wt_filter_plots(self):
        """Creates plots of filter characteristics."""

        # If the canvas has not been previously created, create the canvas
        # and add the widget.
        if self.wt_filter_canvas is None:
            # Create the canvas
            self.wt_filter_canvas = MplCanvas(
                parent=self.graph_wt, width=10, height=2, dpi=80
            )
            # Assign layout to widget to allow auto scaling
            layout = QtWidgets.QVBoxLayout(self.graph_wt)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(0, 0, 0, 0)
            # Add the canvas
            layout.addWidget(self.wt_filter_canvas)
            # Initialize hidden toolbar for use by graphics controls
            self.wt_filter_toolbar = NavigationToolbar(self.wt_filter_canvas, self)
            self.wt_filter_toolbar.hide()

        # Initialize the water filters figure and assign to the canvas
        self.wt_filter_fig = Graphics(canvas=self.wt_filter_canvas)

        if self.meas.verticals[self.vertical_idx].data.w_vel is not None:
            self.wt_filter_fig.create_wt_tab_graphs(
                meas=self.meas,
                vertical_idx=self.vertical_idx,
                units=self.units,
                contour=self.rb_wt_contour.isChecked(),
                beam=self.rb_wt_beam.isChecked(),
                error=self.rb_wt_error.isChecked(),
                vert=self.rb_wt_vert.isChecked(),
                snr=self.rb_wt_snr.isChecked(),
                speed=self.rb_wt_speed.isChecked(),
                x_axis_type="E",
                color_map=self.color_map,
            )
        else:
            self.wt_filter_fig.fig.clear()

        # Draw canvas
        self.wt_filter_canvas.draw()

        # Reset data cursor to work with new figure
        if self.actionData_Cursor.isChecked():
            self.data_cursor()
        self.wt_subtab_data.setFocus()

    def wt_magdir_plot(self):
        """Creates magnitude and direction profile plot."""

        # If the canvas has not been previously created, create the canvas
        # and add the widget.
        if self.wt_profile_canvas is None:
            # Create the canvas
            self.wt_profile_canvas = MplCanvas(
                parent=self.graph_wt_profile, width=10, height=2, dpi=80
            )
            # Assign layout to widget to allow auto scaling
            layout = QtWidgets.QVBoxLayout(self.graph_wt_profile)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(0, 0, 0, 0)
            # Add the canvas
            layout.addWidget(self.wt_profile_canvas)
            # Initialize hidden toolbar for use by graphics controls
            self.wt_profile_toolbar = NavigationToolbar(self.wt_profile_canvas, self)
            self.wt_profile_toolbar.hide()

        # Initialize the water filters figure and assign to the canvas
        self.wt_profile_fig = Graphics(canvas=self.wt_profile_canvas)

        if self.meas.verticals[self.vertical_idx].mean_profile["speed"].shape[0] > 0:
            self.wt_profile_fig.magdir_graph(
                vertical=self.meas.verticals[self.vertical_idx], units=self.units
            )
        else:
            self.wt_profile_fig.fig.clear()

        # Draw canvas
        self.wt_profile_canvas.draw()

        # Reset data cursor to work with new figure
        if self.actionData_Cursor.isChecked():
            self.data_cursor()
        self.wt_subtab_data.setFocus()

    def wt_rssi_plot(self):
        """Creates the signal strength graph."""

        # If the canvas has not been previously created, create the canvas
        # and add the widget.
        if self.wt_profile_canvas is None:
            # Create the canvas
            self.wt_profile_canvas = MplCanvas(
                parent=self.graph_wt_profile, width=10, height=2, dpi=80
            )
            # Assign layout to widget to allow auto scaling
            layout = QtWidgets.QVBoxLayout(self.graph_wt_profile)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(0, 0, 0, 0)
            # Add the canvas
            layout.addWidget(self.wt_profile_canvas)
            # Initialize hidden toolbar for use by graphics controls
            self.wt_profile_toolbar = NavigationToolbar(self.wt_profile_canvas, self)
            self.wt_profile_toolbar.hide()

        # Initialize the shiptrack figure and assign to the canvas
        self.wt_profile_fig = Graphics(canvas=self.wt_profile_canvas)
        # Create the figure with the specified data
        if self.meas.verticals[self.vertical_idx].sampling_duration_sec > 0:
            self.wt_profile_fig.rssi_graph(
                meas=self.meas, units=self.units, idx=self.vertical_idx
            )
        else:
            self.wt_profile_fig.fig.clear()

        # Draw canvas
        self.wt_profile_canvas.draw()

    def wt_vel_source_change(self, apply_all=False):
        """Change velocity source.

        Parameters
        ----------
        apply_all: bool
            Indicates if the change should be applied to all verticals or
            only the selected vertical
        """

        # Discharge prior to change
        old_discharge = self.meas.summary

        # Velocity source
        source = self.combo_wt_vel_source.currentText()
        if source == "Manual":
            speed = self.check_numeric_input(self.ed_wt_vel_source, block=False)
            speed = speed / self.units["V"]
        else:
            speed = None

        # Apply change
        if apply_all:
            idx = None
        else:
            idx = self.vertical_idx
        self.meas.change_vel_source(source, speed=speed, idx=idx)
        self.change = True

        # Update GUI
        self.wt_tab_update(old_discharge=old_discharge)
        self.wt_subtab_data.setFocus()

    def wt_min_ens_change(self, apply_all=False):
        """Change minimum number of ensembles required for a valid velocity
        in a depth cell.

        Parameters
        ----------
        apply_all: bool
            Indicates if the change should be applied to all verticals or
        only the selected vertical
        """

        # Discharge prior to change
        old_discharge = self.meas.summary

        # Minimum number of ensembles
        min_ens = self.check_numeric_input(self.ed_wt_min_ens, block=False)

        # Apply change
        if apply_all:
            idx = None
        else:
            idx = self.vertical_idx
        self.meas.change_min_ens(threshold=min_ens, idx=idx)
        self.change = True

        # Update GUI
        self.wt_tab_update(old_discharge=old_discharge)
        self.wt_subtab_data.setFocus()

    def wt_excluded_below_change(self, apply_all=False):
        """Change excluded below transducer.

        Parameters
        ----------
        apply_all: bool
            Indicates if the change should be applied to all verticals or
            only the selected vertical
        """

        # Discharge prior to change
        old_discharge = self.meas.summary

        # Get user setting
        top_type = self.combo_wt_excluded_below.currentText()
        value = self.check_numeric_input(self.ed_wt_excluded_below, block=False)
        if top_type == "Dist.":
            value = value / self.units["L"]

        # Apply change
        if apply_all:
            idx = None
        else:
            idx = self.vertical_idx
        self.meas.change_excluded_top(top_type=top_type, value=value, idx=idx)
        self.change = True

        # Update GUI
        self.wt_tab_update(old_discharge=old_discharge)
        self.wt_subtab_data.setFocus()

    def wt_excluded_above_change(self, apply_all=False):
        """Change excluded above sidelobe.

        Parameters
        ----------
        apply_all: bool
            Indicates if the change should be applied to all verticals or
        only the selected vertical
        """

        # Discharge prior to change
        old_discharge = self.meas.summary

        # Get user setting
        bottom_type = self.combo_wt_excluded_above.currentText()
        value = self.check_numeric_input(self.ed_wt_excluded_above, block=False)
        if bottom_type == "Dist.":
            value = value / self.units["L"]

        # Apply change
        if apply_all:
            idx = None
        else:
            idx = self.vertical_idx
        self.meas.change_excluded_bottom(bottom_type=bottom_type, value=value, idx=idx)
        self.change = True

        # Update GUI
        self.wt_tab_update(old_discharge=old_discharge)
        self.wt_subtab_data.setFocus()

    def wt_3beam_change(self, apply_all=False):
        """Coordinates user initiated change to the beam settings.

        Parameters
        ----------
        apply_all: bool
            Indicator if change should be applied to selected vertical or
                all verticals
        """

        with self.wait_cursor():
            self.combo_wt_3beam.blockSignals(True)
            text = self.combo_wt_3beam.currentText()

            # Change setting
            if text == "Auto":
                settings = [("WTBeamFilter", -1)]
            elif text == "Allow":
                settings = [("WTBeamFilter", 3)]
            elif text == "4-Beam Only":
                settings = [("WTBeamFilter", 4)]

            # Apply change, update measurement and display
            self.wt_update(settings, apply_all)
            self.change = True

    def wt_error_vel_change(self, apply_all=False):
        """Coordinates user initiated change to the error velocity settings.

        Parameters
        ----------
        apply_all: bool
            Indicator if change should be applied to selected vertical or all
            verticals
        """

        with self.wait_cursor():
            # Block signals
            self.combo_wt_error_velocity.blockSignals(True)
            self.ed_wt_error_vel_threshold.blockSignals(True)

            # Get user setting
            text = self.combo_wt_error_velocity.currentText()

            # Change setting based on combo box selection
            settings = [("WTdFilter", text)]
            if text == "Manual":
                # If Manual, enable the line edit box for user input. Updates
                # are not applied until the user has entered
                # a value in the line edit box.
                threshold = self.check_numeric_input(self.ed_wt_error_vel_threshold)
                threshold = threshold / self.units["V"]
                settings.append(("WTdFilterThreshold", threshold))
                self.ed_wt_error_vel_threshold.blockSignals(False)
            else:
                # If manual is not selected the line edit box is cleared and
                # disabled and the updates applied.
                self.ed_wt_error_vel_threshold.setEnabled(False)
                self.ed_wt_error_vel_threshold.setText("")

            # Apply settings
            self.wt_update(settings, apply_all)
            self.change = True

    def wt_vert_vel_change(self, apply_all=False):
        """Coordinates user initiated change to the vertical velocity settings.

        Parameters
        ----------
        apply_all: bool
            Indicator if change should be applied to selected vertical or
            all verticals
        """

        with self.wait_cursor():
            # Block signals
            self.combo_wt_vert_velocity.blockSignals(True)
            self.ed_wt_vert_vel_threshold.blockSignals(True)

            # Get user setting
            text = self.combo_wt_vert_velocity.currentText()

            # Change setting based on combo box selection
            settings = [("WTwFilter", text)]

            if text == "Manual":
                # If Manual enable the line edit box for user input. Updates
                # are not applied until the user has entered
                # a value in the line edit box.
                threshold = self.check_numeric_input(self.ed_wt_vert_vel_threshold)
                threshold = threshold / self.units["V"]
                settings.append(("WTwFilterThreshold", threshold))
                self.ed_wt_vert_vel_threshold.blockSignals(False)
            else:
                # If manual is not selected, the line edit box is cleared and
                # disabled and the updates applied.
                self.ed_wt_vert_vel_threshold.setEnabled(False)
                self.ed_wt_vert_vel_threshold.setText("")

            # Apply settings
            self.wt_update(settings, apply_all)
            self.change = True

    def wt_snr_change(self, apply_all=False):
        """Coordinates user initiated change to the SNR filter.

        Parameters
        ----------
        apply_all: bool
            Indicator if change should be applied to selected vertical or
            all verticals
        """

        with self.wait_cursor():
            # Block signals
            self.combo_wt_snr.blockSignals(True)

            # Get user setting
            text = self.combo_wt_snr.currentText()

            # Change setting based on combo box selection
            settings = [("WTsnrFilter", text)]

            # Apply settings
            self.wt_update(settings, apply_all)
            self.change = True

    def wt_flow_angle_change(self, apply_all=False):
        """Coordinates user initiated change to the flow angle when using
            magnitude method.

        Parameters
        ----------
        apply_all: bool
            Indicator if change should be applied to selected vertical or
            all verticals
        """

        with self.wait_cursor():
            # Discharge prior to change
            old_discharge = self.meas.summary

            # Block signals
            self.combo_wt_flow_angle.blockSignals(True)
            self.ed_wt_flow_angle.blockSignals(True)

            # Get user setting
            text = self.combo_wt_flow_angle.currentText()
            value = self.check_numeric_input(self.ed_wt_flow_angle)

            # Convert to coefficient if necessary
            if text == "Degree":
                # Compute coefficient from user provided angle
                coefficient = cosd(value)
            else:
                coefficient = value

            # Apply change
            if apply_all:
                idx = None
            else:
                idx = self.vertical_idx
            self.meas.change_flow_angle(coefficient=coefficient, idx=idx)
            self.change = True

            # Update GUI
            self.wt_tab_update(old_discharge=old_discharge)
            self.wt_subtab_data.setFocus()

    def wt_update(self, settings, apply_all):
        """Applies change water track filters and updates the measurement
        and bottom track tab (table and graphics) .

        Parameters
        ----------
        settings: list
            List of tuples of setting name and value
        apply_all: bool
            Indicator if change should be applied to selected vertical or
                                              all verticals
        """

        # Save discharge from previous settings
        old_discharge = self.meas.summary

        # Apply new settings
        if apply_all:
            self.meas.change_vertical_setting(settings=settings)
        else:
            self.meas.change_vertical_setting(settings=settings, idx=self.vertical_idx)
        self.change = True

        # Update tab
        self.wt_tab_update(old_discharge=old_discharge)

        # Reset focus
        self.wt_subtab_data.setFocus()

    def wt_comments_messages(self):
        """Displays comments and messages associated with bottom track
        filters in Messages tab.
        """

        # Clear comments and messages
        self.display_wt_comments.clear()

        if self.meas is not None:
            # Display each comment on a new line
            self.display_wt_comments.moveCursor(QtGui.QTextCursor.Start)
            for comment in self.meas.comments:
                self.display_wt_comments.textCursor().insertText(comment)
                self.display_wt_comments.moveCursor(QtGui.QTextCursor.End)
                self.display_wt_comments.textCursor().insertBlock()

            # Display each message on a new line
            self.messages_table(self.table_wt_messages, ["w_vel"])

            self.update_tab_icons()

    # Extrap Tab
    # ==========

    def extrap_tab_initial(self, old_discharge=None):
        """Initialize extrap tab."""

        # Save copy of measurement to allow for reset to previous values
        self.save_4_reset = copy.deepcopy(self.meas)
        if old_discharge is None:
            old_discharge = self.save_4_reset.summary


        self.vertical_changed = [False] * len(self.meas.verticals_used_idx)

        # Setup transect / measurement list table
        tbl = self.table_extrap_verticals
        table_header = [
            self.tr("Station"),
            self.tr("Fit \n Method"),
            self.tr("Top \n Fit"),
            self.tr("Ice \n Exponent"),
            self.tr("Bottom \n Fit"),
            self.tr("Exponent"),
            self.tr("Discharge \n Previous \n" + self.units["label_Q"]),
            self.tr("Discharge \n Now \n" + self.units["label_Q"]),
            self.tr("Discharge \n % Change"),
        ]
        ncols = len(table_header)
        nrows = len(self.meas.summary.index) - 2
        tbl.setRowCount(nrows)
        tbl.setColumnCount(ncols)
        tbl.setHorizontalHeaderLabels(table_header)
        tbl.horizontalHeader().setFont(self.font_bold)
        tbl.verticalHeader().hide()
        tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)

        # Initialize connections
        if not self.extrap_initialized:
            # Table clicks
            self.table_extrap_qsen.cellClicked.connect(
                self.extrap_sensitivity_table_clicked
            )
            self.table_extrap_verticals.cellClicked.connect(
                self.extrap_verticals_table_clicked
            )

            # Connect fit options
            self.combo_extrap_fit.currentIndexChanged.connect(
                self.extrap_enable_fit_settings
            )
            self.combo_extrap_top.currentIndexChanged.connect(self.extrap_enable_apply)
            self.combo_extrap_bottom.currentIndexChanged.connect(
                self.extrap_enable_apply
            )

            self.extrap_ice_exponent_grp = EdGrp(
                ed=self.ed_extrap_ice_exponent,
                pb_apply=self.pb_extrap_fit_apply,
                pb_apply_all=self.pb_extrap_fit_apply_all,
                change=self.extrap_change_fit_settings,
                min_value=0,
            )
            self.ed_extrap_ice_exponent.textChanged.connect(
                self.extrap_ice_exponent_grp.enable_apply
            )

            self.extrap_exponent_grp = EdGrp(
                ed=self.ed_extrap_exponent,
                pb_apply=self.pb_extrap_fit_apply,
                pb_apply_all=self.pb_extrap_fit_apply_all,
                change=self.extrap_change_fit_settings,
                min_value=0,
            )
            self.ed_extrap_exponent.textChanged.connect(
                self.extrap_exponent_grp.enable_apply
            )

            self.pb_extrap_fit_apply.clicked.connect(self.extrap_change_fit_settings)
            self.pb_extrap_fit_apply_all.clicked.connect(
                self.extrap_change_fit_settings
            )

            # Connect display settings
            self.cb_extrap_data_points.stateChanged.connect(self.extrap_plot)
            self.cb_extrap_data_profiles.stateChanged.connect(self.extrap_plot)
            self.cb_extrap_surface.stateChanged.connect(self.extrap_plot)
            self.cb_extrap_mean_profile.stateChanged.connect(self.extrap_plot)
            self.cb_extrap_fit.stateChanged.connect(self.extrap_plot)
            self.cb_extrap_fit_auto.stateChanged.connect(self.extrap_plot)

            # Connect data settings
            self.extrap_min_ens_grp = EdGrp(
                ed=self.ed_extrap_min_ens,
                pb_apply=self.pb_extrap_min_ens_apply,
                pb_apply_all=self.pb_extrap_min_ens_apply_all,
                change=self.extrap_change_min_ens,
            )
            self.ed_extrap_min_ens.textChanged.connect(
                self.extrap_min_ens_grp.enable_apply
            )
            self.pb_extrap_min_ens_apply.clicked.connect(self.extrap_min_ens_grp.apply)
            self.pb_extrap_min_ens_apply_all.clicked.connect(
                self.extrap_min_ens_grp.apply_all
            )

            # Reset buttons
            self.pb_extrap_fit_reset.clicked.connect(self.extrap_reset)
            self.pb_extrap_fit_reset_all.clicked.connect(self.extrap_reset_all)

            self.extrap_initialized = True

        # Disable Apply and Reset buttons
        self.pb_extrap_fit_reset.setEnabled(False)
        self.pb_extrap_fit_reset_all.setEnabled(False)
        self.pb_extrap_fit_apply.setEnabled(False)
        self.pb_extrap_fit_apply_all.setEnabled(False)
        self.pb_extrap_min_ens_apply.setEnabled(False)
        self.pb_extrap_min_ens_apply_all.setEnabled(False)

        # Update tables, settings, and graph
        self.extrap_tab_update(old_discharge)

    def extrap_tab_update(self, old_discharge=None):
        """Update tables, settings, and graph."""

        self.extrap_fit_settings_update()
        self.extrap_tables_update(old_discharge=old_discharge)
        self.extrap_plot()
        self.extrap_comments_messages()

        # Reset button control
        if self.vertical_changed[self.meas.verticals_used_idx.index(self.vertical_idx)]:
            self.pb_extrap_fit_reset.setEnabled(True)
            self.pb_extrap_fit_reset_all.setEnabled(True)
        else:
            self.pb_extrap_fit_reset.setEnabled(False)
            self.pb_extrap_fit_reset_all.setEnabled(False)

        # Setup list for use by graphics controls
        self.canvases = [self.extrap_canvas]
        self.figs = [self.extrap_fig]
        self.toolbars = [self.extrap_toolbar]
        self.fig_calls = [self.extrap_plot]
        self.ui_parents = [i.parent() for i in self.canvases]
        self.figs_menu_connection()

    def extrap_fit_settings_update(self):
        """Update fit settings."""

        # Selected vertical
        vertical = self.meas.verticals[self.vertical_idx]

        if vertical.mean_speed_source == "Manual":
            self.combo_extrap_fit.hide()
            self.combo_extrap_top.hide()
            self.combo_extrap_bottom.hide()
            self.ed_extrap_ice_exponent.hide()
            self.ed_extrap_exponent.hide()
            self.pb_extrap_fit_reset.hide()
            self.pb_extrap_fit_apply.hide()
            self.pb_extrap_min_ens_apply.hide()
            self.pb_extrap_fit_reset_all.hide()
            self.pb_extrap_fit_apply_all.hide()
            self.pb_extrap_min_ens_apply_all.hide()
            self.ed_extrap_min_ens.hide()
            self.cb_extrap_fit_auto.hide()
            self.cb_extrap_data_profiles.hide()
            self.cb_extrap_fit.hide()
            self.cb_extrap_surface.hide()
            self.cb_extrap_mean_profile.hide()
            self.cb_extrap_data_points.hide()
        else:
            self.combo_extrap_fit.show()
            self.combo_extrap_top.show()
            self.combo_extrap_bottom.show()
            self.ed_extrap_ice_exponent.show()
            self.ed_extrap_exponent.show()
            self.pb_extrap_fit_reset.show()
            self.pb_extrap_fit_apply.show()
            self.pb_extrap_min_ens_apply.show()
            self.pb_extrap_fit_reset_all.show()
            self.pb_extrap_fit_apply_all.show()
            self.pb_extrap_min_ens_apply_all.show()
            self.ed_extrap_min_ens.show()
            self.cb_extrap_fit_auto.show()
            self.cb_extrap_data_profiles.show()
            self.cb_extrap_fit.show()
            self.cb_extrap_surface.show()
            self.cb_extrap_mean_profile.show()
            self.cb_extrap_data_points.show()

        # Setup fit method
        self.combo_extrap_fit.blockSignals(True)
        if len(vertical.extrapolation.bot_method) == 0:
            self.combo_extrap_fit.setEnabled(False)
        else:
            self.combo_extrap_fit.setEnabled(True)
            if vertical.extrapolation.extrap_fit.fit_method == "Automatic":
                self.combo_extrap_fit.setCurrentIndex(0)
                self.combo_extrap_top.setEnabled(False)
                self.ed_extrap_ice_exponent.setEnabled(False)
                self.combo_extrap_bottom.setEnabled(False)
                self.ed_extrap_exponent.setEnabled(False)
            else:
                self.combo_extrap_fit.setCurrentIndex(1)
                self.combo_extrap_top.setEnabled(True)
                if vertical.extrapolation.top_method == "Ice":
                    self.ed_extrap_ice_exponent.setEnabled(True)
                else:
                    self.ed_extrap_ice_exponent.setEnabled(False)
                self.combo_extrap_bottom.setEnabled(True)
                self.ed_extrap_exponent.setEnabled(True)
            self.combo_extrap_fit.blockSignals(False)

            # Set top method
            self.combo_extrap_top.blockSignals(True)
            if self.meas.verticals[self.vertical_idx].ws_condition == "Ice":
                self.combo_extrap_top.model().item(0).setEnabled(False)
                self.combo_extrap_top.model().item(1).setEnabled(False)
                self.combo_extrap_top.model().item(2).setEnabled(False)
                self.combo_extrap_top.model().item(3).setEnabled(True)
                self.combo_extrap_top.setCurrentIndex(3)
            else:
                self.combo_extrap_top.model().item(0).setEnabled(True)
                self.combo_extrap_top.model().item(1).setEnabled(True)
                self.combo_extrap_top.model().item(2).setEnabled(True)
                self.combo_extrap_top.model().item(3).setEnabled(False)
                if vertical.extrapolation.top_method == "Power":
                    self.combo_extrap_top.setCurrentIndex(0)
                elif vertical.extrapolation.top_method == "Constant":
                    self.combo_extrap_top.setCurrentIndex(1)
                elif vertical.extrapolation.top_method == "3-Point":
                    self.combo_extrap_top.setCurrentIndex(2)

            self.combo_extrap_top.blockSignals(False)

            # Set bottom method
            self.combo_extrap_bottom.blockSignals(True)
            if vertical.extrapolation.bot_method == "Power":
                self.combo_extrap_bottom.setCurrentIndex(0)
            else:
                self.combo_extrap_bottom.setCurrentIndex(1)
            self.combo_extrap_bottom.blockSignals(False)

            # Set ice exponent
            self.ed_extrap_ice_exponent.blockSignals(True)
            if vertical.extrapolation.top_method == "Ice":
                self.ed_extrap_ice_exponent.show()
                self.txt_ice_exponent.show()
                self.ed_extrap_ice_exponent.setText(
                    "{:6.4f}".format(vertical.extrapolation.ice_exponent)
                )
            else:
                self.ed_extrap_ice_exponent.hide()
                self.txt_ice_exponent.hide()
            self.ed_extrap_ice_exponent.blockSignals(False)

            # Set exponent
            self.ed_extrap_exponent.blockSignals(True)
            self.ed_extrap_exponent.setText(
                "{:6.4f}".format(vertical.extrapolation.exponent)
            )
            self.ed_extrap_exponent.blockSignals(False)

            # Min Ens
            self.ed_extrap_min_ens.blockSignals(True)
            self.ed_extrap_min_ens.setText(
                "{:4.0f}".format(vertical.cell_minimum_valid_ensembles)
            )
            self.ed_extrap_min_ens.blockSignals(False)

    def extrap_tables_update(self, old_discharge):
        """Update extrap tables."""

        if len(self.meas.verticals[self.vertical_idx].extrapolation.bot_method) > 0:
            self.extrap_n_points_table_update()
            self.extrap_q_sensitivity_table_update()
        else:
            self.table_extrap_n_points.clearContents()
            self.table_extrap_qsen.clearContents()
        self.extrap_verticals_table_update(old_discharge=old_discharge)

    def extrap_n_points_table_update(self):
        """Update values in points table."""

        # Setup number of points data table
        tbl = self.table_extrap_n_points
        table_header = [
            self.tr("Depth \n" + self.units["label_L"]),
            self.tr("No. Pts."),
        ]
        ncols = 2
        nrows = len(self.meas.verticals[self.vertical_idx].mean_profile["cell_depth"])
        if nrows == 0:
            ncols = 1
            nrows = 1

        tbl.setRowCount(nrows)
        tbl.setColumnCount(ncols)
        tbl.setHorizontalHeaderLabels(table_header)
        tbl.horizontalHeader().setFont(self.font_bold)
        tbl.verticalHeader().hide()
        tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)

        if ncols == 1:
            item = "No valid cells"
            tbl.setItem(0, 0, QtWidgets.QTableWidgetItem(item))
            tbl.item(0, 0).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.item(0, 0).setBackground(QtGui.QColor(255, 77, 77))

        # Populate table
        col = 0
        for row, depth in enumerate(
            self.meas.verticals[self.vertical_idx].mean_profile["cell_depth"]
        ):
            if np.isnan(depth):
                item = ""
            else:
                item = "{:4.3f}".format(depth * self.units["L"])
            tbl.setItem(row, col, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            n_points = self.meas.verticals[self.vertical_idx].mean_profile[
                "number_ensembles"
            ][row]
            if n_points > 0:
                item = "{:4.0f}".format(n_points)
            elif n_points < 0:
                item = "Extrap"
            elif n_points == 0:
                item = ""
            tbl.setItem(row, col + 1, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, col + 1).setFlags(QtCore.Qt.ItemIsEnabled)

        tbl.resizeColumnsToContents()
        tbl.resizeRowsToContents()

    def extrap_verticals_table_update(self, old_discharge=None):
        """update data in verticals table"""

        with self.wait_cursor():
            # Set tbl variable
            tbl = self.table_extrap_verticals

            # Populate each row
            for row in range(tbl.rowCount()):
                # Station Number
                col = 0
                if np.isnan(self.meas.summary["Station Number"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                elif np.modf(self.meas.summary["Station Number"][row])[0] == 0:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:3.0f}".format(self.meas.summary["Station Number"][row])
                        ),
                    )
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:3.1f}".format(self.meas.summary["Station Number"][row])
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Fit Method
                col += 1
                if len(self.meas.summary["Top Method"][row]) < 1:
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    fit_method = self.meas.summary["Fit Method"][row]
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(fit_method))
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Top Method
                col += 1
                if len(self.meas.summary["Top Method"][row]) < 1:
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            self.meas.summary["Top Method"][row]
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Ice Exponent
                col += 1
                if np.isnan(self.meas.summary["Ice Exponent"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:1.4f}".format(self.meas.summary["Ice Exponent"][row])
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Bottom Method
                col += 1
                if len(self.meas.summary["Bottom Method"][row]) < 1:
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            self.meas.summary["Bottom Method"][row]
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Bottom Exponent
                col += 1
                if np.isnan(self.meas.summary["Bottom Exponent"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:1.4f}".format(self.meas.summary["Bottom Exponent"][row])
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Previous Discharge
                col += 1
                if old_discharge is None:
                    old_discharge = self.save_4_reset.summary
                if np.isnan(old_discharge["Q"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:8}".format(
                                self.q_digits(
                                    old_discharge["Q"][row] * self.units["Q"]
                                )
                            )
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Current Discharge
                col += 1
                if np.isnan(self.meas.summary["Q"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                    tbl.item(row, col).setBackground(QtGui.QColor(255, 77, 77))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:6.3f}".format(
                                self.meas.summary["Q"][row] * self.units["Q"]
                            )
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

                # Percent difference in old and new discharges
                col += 1
                if np.isnan(self.meas.summary["Q"][row]):
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    old_q = old_discharge["Q"][row]
                    if np.abs(old_q) > 0:
                        per_change = (
                            (self.meas.summary["Q"][row] - old_q)
                            / old_q
                        ) * 100
                        tbl.setItem(
                            row,
                            col,
                            QtWidgets.QTableWidgetItem("{:3.1f}".format(per_change)),
                        )
                    else:
                        tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                    tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            if np.any(self.meas.summary["Condition"].str.contains("Ice")):
                tbl.setColumnHidden(3, False)
            else:
                tbl.setColumnHidden(3, True)

            # Set highlight selected vertical
            self.highlight_row(tbl, self.main_row_idx - 1)

            tbl.resizeColumnsToContents()
            tbl.resizeRowsToContents()

    def extrap_q_sensitivity_table_update(self):
        """Populates the discharge sensitivity table."""

        # Get sensitivity data
        sensitivity = self.meas.verticals[
            self.vertical_idx
        ].extrapolation.extrap_fit.sensitivity

        if self.meas.verticals[self.vertical_idx].ws_condition == "Open":
            # Setup sensitivity table
            tbl = self.table_extrap_qsen
            table_header = [
                self.tr("Top"),
                self.tr("Bottom"),
                self.tr("Exponent"),
                self.tr("% Diff"),
            ]
            ncols = len(table_header)
            nrows = 6
            tbl.setRowCount(nrows)
            tbl.setColumnCount(ncols)
            tbl.setHorizontalHeaderLabels(table_header)
            tbl.horizontalHeader().setFont(self.font_bold)
            tbl.verticalHeader().hide()
            tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)

            # Power / Power / 1/6
            row = 0
            tbl.setItem(row, 0, QtWidgets.QTableWidgetItem("Power"))
            tbl.item(row, 0).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.setItem(row, 1, QtWidgets.QTableWidgetItem("Power"))
            tbl.item(row, 1).setFlags(QtCore.Qt.ItemIsEnabled)
            item = "{:6.4f}".format(0.1667)
            tbl.setItem(row, 2, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, 2).setFlags(QtCore.Qt.ItemIsEnabled)
            item = "{:6.2f}".format(sensitivity.v_pp_per_diff)
            tbl.setItem(row, 3, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, 3).setFlags(QtCore.Qt.ItemIsEnabled)

            # Power / Power / Optimize
            row = 1
            tbl.setItem(row, 0, QtWidgets.QTableWidgetItem("Power"))
            tbl.item(row, 0).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.setItem(row, 1, QtWidgets.QTableWidgetItem("Power"))
            tbl.item(row, 1).setFlags(QtCore.Qt.ItemIsEnabled)
            item = "{:6.4f}".format(sensitivity.pp_exp)
            tbl.setItem(row, 2, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, 2).setFlags(QtCore.Qt.ItemIsEnabled)
            item = "{:6.2f}".format(sensitivity.v_pp_opt_per_diff)
            tbl.setItem(row, 3, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, 3).setFlags(QtCore.Qt.ItemIsEnabled)

            # Constant / No Slip / 1/6
            row = 2
            tbl.setItem(row, 0, QtWidgets.QTableWidgetItem("Constant"))
            tbl.item(row, 0).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.setItem(row, 1, QtWidgets.QTableWidgetItem("No Slip"))
            tbl.item(row, 1).setFlags(QtCore.Qt.ItemIsEnabled)
            item = "{:6.4f}".format(0.1667)
            tbl.setItem(row, 2, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, 2).setFlags(QtCore.Qt.ItemIsEnabled)
            item = "{:6.2f}".format(sensitivity.v_cns_per_diff)
            tbl.setItem(row, 3, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, 3).setFlags(QtCore.Qt.ItemIsEnabled)

            # Constant / No Slip / Optimize
            row = 3
            tbl.setItem(row, 0, QtWidgets.QTableWidgetItem("Constant"))
            tbl.item(row, 0).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.setItem(row, 1, QtWidgets.QTableWidgetItem("No Slip"))
            tbl.item(row, 1).setFlags(QtCore.Qt.ItemIsEnabled)
            item = "{:6.4f}".format(sensitivity.ns_exp)
            tbl.setItem(row, 2, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, 2).setFlags(QtCore.Qt.ItemIsEnabled)
            item = "{:6.2f}".format(sensitivity.v_cns_opt_per_diff)
            tbl.setItem(row, 3, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, 3).setFlags(QtCore.Qt.ItemIsEnabled)

            # 3-Point / No Slip / 1/6
            row = 4
            tbl.setItem(row, 0, QtWidgets.QTableWidgetItem("3-Point"))
            tbl.item(row, 0).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.setItem(row, 1, QtWidgets.QTableWidgetItem("No Slip"))
            tbl.item(row, 1).setFlags(QtCore.Qt.ItemIsEnabled)
            item = "{:6.4f}".format(0.1667)
            tbl.setItem(row, 2, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, 2).setFlags(QtCore.Qt.ItemIsEnabled)
            item = "{:6.2f}".format(sensitivity.v_3p_ns_per_diff)
            tbl.setItem(row, 3, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, 3).setFlags(QtCore.Qt.ItemIsEnabled)

            # 3-Point / No Slip / Optimize
            row = 5
            tbl.setItem(row, 0, QtWidgets.QTableWidgetItem("3-Point"))
            tbl.item(row, 0).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.setItem(row, 1, QtWidgets.QTableWidgetItem("No Slip"))
            tbl.item(row, 1).setFlags(QtCore.Qt.ItemIsEnabled)
            item = "{:6.4f}".format(sensitivity.ns_exp)
            tbl.setItem(row, 2, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, 2).setFlags(QtCore.Qt.ItemIsEnabled)
            item = "{:6.2f}".format(sensitivity.v_3p_ns_opt_per_diff)
            tbl.setItem(row, 3, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, 3).setFlags(QtCore.Qt.ItemIsEnabled)

            # Manually set fit method
            if not np.isnan(sensitivity.v_man):
                row = tbl.rowCount()
                if row == 6:
                    tbl.insertRow(row)
                else:
                    row = row - 1
                tbl.setItem(row, 0, QtWidgets.QTableWidgetItem(sensitivity.man_top))
                tbl.item(row, 0).setFlags(QtCore.Qt.ItemIsEnabled)
                tbl.setItem(row, 1, QtWidgets.QTableWidgetItem(sensitivity.man_bot))
                tbl.item(row, 1).setFlags(QtCore.Qt.ItemIsEnabled)
                item = "{:6.4f}".format(sensitivity.man_exp)
                tbl.setItem(row, 2, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, 2).setFlags(QtCore.Qt.ItemIsEnabled)
                item = "{:6.2f}".format(sensitivity.v_man_per_diff)
                tbl.setItem(row, 3, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, 3).setFlags(QtCore.Qt.ItemIsEnabled)
            elif tbl.rowCount() == 7:
                tbl.removeRow(6)

            # Set reference
            if not np.isnan(sensitivity.v_man):
                self.set_extrap_reference(6)
            elif np.abs(sensitivity.v_pp_per_diff) < 0.00000001:
                self.set_extrap_reference(0)
            elif np.abs(sensitivity.v_pp_opt_per_diff) < 0.00000001:
                self.set_extrap_reference(1)
            elif np.abs(sensitivity.v_cns_per_diff) < 0.00000001:
                self.set_extrap_reference(2)
            elif np.abs(sensitivity.v_cns_opt_per_diff) < 0.00000001:
                self.set_extrap_reference(3)
            elif np.abs(sensitivity.v_3p_ns_per_diff) < 0.00000001:
                self.set_extrap_reference(4)
            elif np.abs(sensitivity.v_3p_ns_opt_per_diff) < 0.00000001:
                self.set_extrap_reference(5)

        # Ice condition
        else:
            # Setup sensitivity table
            tbl = self.table_extrap_qsen
            table_header = [
                self.tr("Top"),
                self.tr("Ice \n Exponent"),
                self.tr("Bottom"),
                self.tr("Exponent"),
                self.tr("% Diff"),
            ]
            ncols = len(table_header)
            nrows = 2
            tbl.setRowCount(nrows)
            tbl.setColumnCount(ncols)
            tbl.setHorizontalHeaderLabels(table_header)
            tbl.horizontalHeader().setFont(self.font_bold)
            tbl.verticalHeader().hide()
            tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)

            # Ice 1/6 / No Slip 1/6
            row = 0
            tbl.setItem(row, 0, QtWidgets.QTableWidgetItem("Ice"))
            tbl.item(row, 0).setFlags(QtCore.Qt.ItemIsEnabled)
            item = "{:6.4f}".format(0.1667)
            tbl.setItem(row, 1, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, 1).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.setItem(row, 2, QtWidgets.QTableWidgetItem("No Slip"))
            tbl.item(row, 2).setFlags(QtCore.Qt.ItemIsEnabled)
            item = "{:6.4f}".format(0.1667)
            tbl.setItem(row, 3, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, 3).setFlags(QtCore.Qt.ItemIsEnabled)
            item = "{:6.2f}".format(sensitivity.v_ice_ns_per_diff)
            tbl.setItem(row, 4, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, 4).setFlags(QtCore.Qt.ItemIsEnabled)

            # Ice 1/6 / No Slip Opt
            row += 1
            tbl.setItem(row, 0, QtWidgets.QTableWidgetItem("Ice"))
            tbl.item(row, 0).setFlags(QtCore.Qt.ItemIsEnabled)
            item = "{:6.4f}".format(0.1667)
            tbl.setItem(row, 1, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, 1).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.setItem(row, 2, QtWidgets.QTableWidgetItem("No Slip"))
            tbl.item(row, 2).setFlags(QtCore.Qt.ItemIsEnabled)
            item = "{:6.4f}".format(sensitivity.ns_exp)
            tbl.setItem(row, 3, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, 3).setFlags(QtCore.Qt.ItemIsEnabled)
            item = "{:6.2f}".format(sensitivity.v_ice_ns_opt_per_diff)
            tbl.setItem(row, 4, QtWidgets.QTableWidgetItem(item))
            tbl.item(row, 4).setFlags(QtCore.Qt.ItemIsEnabled)

            # Manually set fit method
            if not np.isnan(sensitivity.v_man):
                row = tbl.rowCount()
                if row == 2:
                    tbl.insertRow(row)
                else:
                    row = row - 1
                tbl.setItem(row, 0, QtWidgets.QTableWidgetItem("Ice"))
                tbl.item(row, 0).setFlags(QtCore.Qt.ItemIsEnabled)
                item = "{:6.4f}".format(sensitivity.man_ice_exp)
                tbl.setItem(row, 1, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, 1).setFlags(QtCore.Qt.ItemIsEnabled)
                tbl.setItem(row, 2, QtWidgets.QTableWidgetItem("No Slip"))
                tbl.item(row, 2).setFlags(QtCore.Qt.ItemIsEnabled)
                item = "{:6.4f}".format(sensitivity.man_exp)
                tbl.setItem(row, 3, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, 3).setFlags(QtCore.Qt.ItemIsEnabled)
                item = "{:6.2f}".format(sensitivity.v_man_per_diff)
                tbl.setItem(row, 4, QtWidgets.QTableWidgetItem(item))
                tbl.item(row, 4).setFlags(QtCore.Qt.ItemIsEnabled)

            elif tbl.rowCount() == 3:
                tbl.removeRow(2)

            # Set reference
            if not np.isnan(sensitivity.v_man):
                self.set_extrap_reference(2)
            elif np.abs(sensitivity.v_ice_ns_per_diff) < 0.00000001:
                self.set_extrap_reference(0)
            elif np.abs(sensitivity.v_ice_ns_opt_per_diff) < 0.00000001:
                self.set_extrap_reference(1)

        tbl.resizeColumnsToContents()
        tbl.resizeRowsToContents()

    def set_extrap_reference(self, reference_row):
        """Sets the discharge sensitivity table to show the selected method
        as the reference.

        Parameters
        ----------
        reference_row: int
            Integer of the row in sensitivity table for the selected fit
            parameters
        """

        # Get table reference
        tbl = self.table_extrap_qsen

        # Set all data to normal font
        for row in range(tbl.rowCount()):
            for col in range(tbl.columnCount()):
                tbl.item(row, col).setFont(self.font_normal)

        # Set selected file to bold font
        for col in range(tbl.columnCount()):
            tbl.item(reference_row, col).setFont(self.font_bold)

    def extrap_plot(self):
        """Creates extrapolation plot."""

        # If the canvas has not been previously created, create the canvas
        # and add the widget.
        if self.extrap_canvas is None:
            # Create the canvas
            self.extrap_canvas = MplCanvas(
                parent=self.graph_extrap, width=4, height=4, dpi=80
            )
            # Assign layout to widget to allow auto scaling
            layout = QtWidgets.QVBoxLayout(self.graph_extrap)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(1, 1, 1, 1)
            # Add the canvas
            layout.addWidget(self.extrap_canvas)
            # Initialize hidden toolbar for use by graphics controls
            self.extrap_toolbar = NavigationToolbar(self.extrap_canvas, self)
            self.extrap_toolbar.hide()

        # Initialize the figure and assign to the canvas
        self.extrap_fig = Graphics(canvas=self.extrap_canvas)

        if self.meas.verticals[self.vertical_idx].mean_speed_source != "Manual":
            # Create the figure with the specified data
            self.extrap_fig.create_extrap_tab_graph(
                meas=self.meas,
                units=self.units,
                idx=self.vertical_idx,
                data_points=self.cb_extrap_data_points.isChecked(),
                data_profiles=self.cb_extrap_data_profiles.isChecked(),
                data_surface=self.cb_extrap_surface.isChecked(),
                mean_profile=self.cb_extrap_mean_profile.isChecked(),
                fit=self.cb_extrap_fit.isChecked(),
                fit_auto=self.cb_extrap_fit_auto.isChecked(),
            )
        else:
            self.extrap_fig.fig.clear()

        # Update list of figs
        self.figs = [self.extrap_fig]
        self.canvases = [self.extrap_canvas]
        self.toolbars = [self.extrap_toolbar]
        self.fig_calls = [self.extrap_plot]
        self.ui_parents = [i.parent() for i in self.canvases]
        self.figs_menu_connection()

        # Reset data cursor to work with new figure
        if self.actionData_Cursor.isChecked():
            self.data_cursor()

        self.extrap_canvas.draw()

    def extrap_verticals_table_clicked(self, row):
        """Changes plotted data and user settings to the vertical selected.

        Parameters
        ----------
        row: int
            Row clicked by user
        """

        tbl = self.table_extrap_verticals

        # Check to see that the row selected is a valid vertical. This
        # avoids the discharge lines for mean section
        if self.valid_row(row):
            self.highlight_row(tbl, row)
            self.extrap_tab_update()
            self.tab_extrap_subtab_data.setFocus()

    def extrap_sensitivity_table_clicked(self, row):
        """Sets extrapolation settings to those in clicked row.

        Parameters
        ----------
        row: int
            Row clicked by user
        """

        # Get settings
        tbl = self.table_extrap_qsen

        # Get settings from table
        if self.meas.verticals[self.vertical_idx].ws_condition == "Open":
            top = tbl.item(row, 0).text()
            ice_exp = np.nan
            bot = tbl.item(row, 1).text()
            exp = float(tbl.item(row, 2).text())
        else:
            top = tbl.item(row, 0).text()
            ice_exp = float(tbl.item(row, 1).text())
            bot = tbl.item(row, 2).text()
            exp = float(tbl.item(row, 3).text())

        # Apply settings
        self.meas.change_extrapolation(
            fit_method="Manual",
            idx=self.vertical_idx,
            top=top,
            bot=bot,
            exponent=exp,
            ice_exponent=ice_exp,
        )
        self.change = True
        self.vertical_changed[self.meas.verticals_used_idx[self.vertical_idx]] = True

        # Update GUI
        self.extrap_tab_update()
        self.tab_extrap_subtab_data.setFocus()

    def extrap_enable_fit_settings(self):
        """Enables or disables the fit settings based on the fit method (
        Automatic, Manual).
        """

        # Enable settings if Manual
        if self.combo_extrap_fit.currentText() == "Manual":
            self.combo_extrap_top.setEnabled(True)
            if self.meas.verticals[self.vertical_idx].ws_condition == "Ice":
                self.ed_extrap_ice_exponent.setEnabled(True)
            else:
                self.ed_extrap_ice_exponent.setEnabled(False)
            self.combo_extrap_bottom.setEnabled(True)
            self.ed_extrap_exponent.setEnabled(True)

        # Disable settings if Automatic
        else:
            self.combo_extrap_top.setEnabled(False)
            self.ed_extrap_ice_exponent.setEnabled(False)
            self.combo_extrap_bottom.setEnabled(False)
            self.ed_extrap_exponent.setEnabled(False)

        self.extrap_enable_apply()

    def extrap_enable_apply(self):
        """Enable apply buttons."""
        self.pb_extrap_fit_apply.setEnabled(True)
        self.pb_extrap_fit_apply_all.setEnabled(True)

    def extrap_change_fit_settings(self, apply_all=False):
        """Changes fit method and associated settings.

        Parameters
        ----------
        apply_all: bool
            Indicates if the change should be applied to all verticals or
            only the selected vertical
        """

        # Determine sender
        sender = self.sender().objectName()
        if sender == "pb_extrap_fit_apply":
            apply_all = False
        elif sender == "pb_extrap_fit_apply_all":
            apply_all = True

        # Get fit method
        fit_method = self.combo_extrap_fit.currentText()

        # Determine where to apply
        if apply_all:
            idx = None
            for n in range(len(self.vertical_changed)):
                self.vertical_changed[n] = True

        else:
            idx = np.where(self.meas.verticals_used_idx == self.vertical_idx)[0][0]
            self.vertical_changed[idx] = True

        # Apply automatic fit method
        if fit_method == "Automatic":
            self.meas.change_extrapolation(fit_method=fit_method, idx=idx)

        # Apply manual fit method
        else:
            top = self.combo_extrap_top.currentText()
            if top == "Ice":
                ice_exp = self.check_numeric_input(self.ed_extrap_ice_exponent)
            else:
                ice_exp = np.nan
            bot = self.combo_extrap_bottom.currentText()
            exp = self.check_numeric_input(self.ed_extrap_exponent)
            self.meas.change_extrapolation(
                fit_method=fit_method,
                idx=idx,
                top=top,
                bot=bot,
                exponent=exp,
                ice_exponent=ice_exp,
            )
        self.change = True

        # Update GUI
        self.pb_extrap_fit_apply.setEnabled(False)
        self.pb_extrap_fit_apply_all.setEnabled(False)
        self.extrap_tab_update()
        self.tab_extrap_subtab_data.setFocus()

    def extrap_change_min_ens(self, apply_all=False):
        """Changes the minimum number of ensembles required for a valid
            depth cell.

        Parameters
        ----------
        apply_all: bool
            Indicates if the change should be applied to all verticals or
            only the selected vertical
        """

        # Minimum number of ensembles
        min_ens = self.check_numeric_input(self.ed_extrap_min_ens, block=False)

        # Apply change
        if apply_all:
            idx = None
        else:
            idx = self.vertical_idx
        self.meas.change_min_ens(threshold=min_ens, idx=idx)
        self.change = True

        # Update GUI
        self.extrap_tab_update()
        self.tab_extrap_subtab_data.setFocus()

    def extrap_reset(self):
        """Reset vertical back to data and settings as they existed when the
        extrap tab was opened.
        """

        # Reset selected vertical
        self.meas.verticals[self.vertical_idx] = copy.deepcopy(
            self.save_4_reset.verticals[self.vertical_idx]
        )

        # Recompute discharge
        self.meas.compute_discharge()

        # Update change list
        self.vertical_changed[self.vertical_idx] = False

        # Update GUI
        self.extrap_tab_update()
        self.tab_extrap_subtab_data.setFocus()

    def extrap_reset_all(self):
        """Reset all verticals back to data and settings as they existed
        when the extrap tab was opened."""

        # Reset entire measurement
        self.meas = copy.deepcopy(self.save_4_reset)

        # Update change list
        for n in range(len(self.vertical_changed)):
            self.vertical_changed[n] = False

        # Update GUI
        self.extrap_tab_update()
        self.tab_extrap_subtab_data.setFocus()

    def extrap_comments_messages(self):
        """Displays comments and messages associated with bottom track
        filters in Messages tab.
        """

        # Clear comments and messages
        self.display_extrap_comments.clear()

        if self.meas is not None:
            # Display each comment on a new line
            self.display_extrap_comments.moveCursor(QtGui.QTextCursor.Start)
            for comment in self.meas.comments:
                self.display_extrap_comments.textCursor().insertText(comment)
                self.display_extrap_comments.moveCursor(QtGui.QTextCursor.End)
                self.display_extrap_comments.textCursor().insertBlock()

            # Display each message on a new line
            self.messages_table(self.table_extrap_messages, ["extrapolation"])

            self.update_tab_icons()

    # Edges Tab
    # =========
    def edges_tab_initial(self, old_discharge=None):
        """Initialize the edges tab."""

        # Configure Table
        tbl = self.table_edges
        if self.meas.discharge_method == "Mean":
            correction_label = self.tr("Coefficient")
        else:
            correction_label = self.tr("Correction \n Factor")

        table_header = [
            self.tr("Location \n" + self.units["label_L"]),
            self.tr("Depth \n" + self.units["label_L"]),
            self.tr("Normal \n Velocity \n" + self.units["label_V"]),
            correction_label,
            self.tr("Stage \n" + self.units["label_L"]),
            self.tr("Stage \n Time"),
            self.tr("Discharge \n Previous \n" + self.units["label_Q"]),
            self.tr("Discharge \n Now \n" + self.units["label_Q"]),
            self.tr("Discharge \n % Change"),
        ]
        ncols = len(table_header)
        if self.meas.discharge_method == "Mean":
            nrows = 7
        else:
            nrows = 5
        tbl.setRowCount(nrows)
        tbl.setColumnCount(ncols)
        tbl.setHorizontalHeaderLabels(table_header)
        tbl.horizontalHeader().setFont(self.font_bold)
        tbl.verticalHeader().hide()
        tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)

        # Configure Labels
        self.txt_edges_start_location.setText(
            self.tr("Location ") + self.units["label_L"] + ":"
        )
        self.txt_edges_start_depth.setText(
            self.tr("Depth ") + self.units["label_L"] + ":"
        )
        self.txt_edges_start_stage.setText(
            self.tr("Stage ") + self.units["label_L"] + ":"
        )
        self.txt_edges_start_correction.setText(correction_label)
        self.txt_edges_end_location.setText(
            self.tr("Location ") + self.units["label_L"] + ":"
        )
        self.txt_edges_end_depth.setText(
            self.tr("Depth ") + self.units["label_L"] + ":"
        )
        self.txt_edges_end_stage.setText(
            self.tr("Stage ") + self.units["label_L"] + ":"
        )
        self.txt_edges_end_correction.setText(correction_label)

        # Initialize connections
        if not self.edges_initialized:
            self.rb_edges_start_left.toggled.connect(self.edges_start_enable_apply)
            self.rb_edges_start_right.toggled.connect(self.edges_start_enable_apply)
            self.rb_edges_end_left.toggled.connect(self.edges_end_enable_apply)
            self.rb_edges_end_right.toggled.connect(self.edges_end_enable_apply)
            self.ed_edges_start_location.textChanged.connect(
                self.edges_start_enable_apply
            )
            self.ed_edges_start_depth.textChanged.connect(self.edges_start_enable_apply)
            self.ed_edges_start_correction.textChanged.connect(
                self.edges_start_enable_apply
            )
            self.ed_edges_start_stage.textChanged.connect(self.edges_start_enable_apply)
            self.ed_edges_start_stage_time.textChanged.connect(
                self.edges_start_enable_apply
            )
            self.pb_edges_start_apply.clicked.connect(self.edges_change_edge)
            self.ed_edges_end_location.textChanged.connect(self.edges_end_enable_apply)
            self.ed_edges_end_depth.textChanged.connect(self.edges_end_enable_apply)
            self.ed_edges_end_correction.textChanged.connect(
                self.edges_end_enable_apply
            )
            self.ed_edges_end_stage.textChanged.connect(self.edges_end_enable_apply)
            self.ed_edges_end_stage_time.textChanged.connect(
                self.edges_end_enable_apply
            )
            self.pb_edges_end_apply.clicked.connect(self.edges_change_edge)
            self.edges_initialized = True

        # Update tab
        self.edges_tab_update(old_discharge=old_discharge)

        # Initialize graphics
        self.canvases = [self.edges_contour_canvas, self.edges_bar_canvas]
        self.figs = [self.edges_contour_fig, self.edges_bar_fig]
        self.toolbars = [self.edges_contour_toolbar, self.edges_bar_toolbar]
        self.fig_calls = [self.edges_contour_graph, self.edges_bar_graph]
        self.ui_parents = [i.parent() for i in self.canvases]
        self.figs_menu_connection()

    def edges_tab_update(self, old_discharge=None):
        """Updates the tab with current data.

        Parameters
        ----------
        old_discharge: dataframe
            Pandas dataframe of summary data
        """

        # Old discharge used for comparison to change
        if old_discharge is None:
            old_discharge = self.meas.summary

        # Enable apply buttons
        self.pb_edges_start_apply.setEnabled(False)
        self.pb_edges_end_apply.setEnabled(False)

        # Update tab
        self.edges_table_update(old_discharge=old_discharge)
        self.edges_settings_update()
        self.edges_graph()
        self.edges_comments_messages()

    def edges_table_update(self, old_discharge):
        """Update data displayed in table.

        Parameters
        ----------
        old_discharge: dataframe
            Pandas dataframe of summary data
        """

        tbl = self.table_edges
        df_rows = len(self.meas.summary.index)
        if self.meas.discharge_method == "Mean":
            row_idx = [0, 1, 2, df_rows - 5, df_rows - 4, df_rows - 3, "Total"]
        else:
            row_idx = [0, 1, df_rows - 4, df_rows - 3, "Total"]

        for row in range(len(row_idx)):
            idx = row_idx[row]

            # Location
            col = 0
            if idx == "Total":
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(idx))
            elif np.isnan(self.meas.summary["Location"][idx]):
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            else:
                tbl.setItem(
                    row,
                    col,
                    QtWidgets.QTableWidgetItem(
                        "{:6.2f}".format(
                            self.meas.summary["Location"][idx] * self.units["L"]
                        )
                    ),
                )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Depth
            col += 1
            if np.isnan(self.meas.summary["Location"][idx]):
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            else:
                tbl.setItem(
                    row,
                    col,
                    QtWidgets.QTableWidgetItem(
                        "{:6.2f}".format(
                            self.meas.summary["Depth"][idx] * self.units["L"]
                        )
                    ),
                )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Speed
            col += 1
            if np.isnan(self.meas.summary["Normal Velocity"][idx]):
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            else:
                tbl.setItem(
                    row,
                    col,
                    QtWidgets.QTableWidgetItem(
                        "{:6.2f}".format(
                            self.meas.summary["Normal Velocity"][idx] * self.units["V"]
                        )
                    ),
                )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Correction
            col += 1
            if self.meas.discharge_method == "Mean":
                correction = self.meas.summary["Edge Coefficient"][idx]
            else:
                correction = self.meas.summary["Correction Factor"][idx]
            if np.isnan(correction):
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            else:
                tbl.setItem(
                    row, col, QtWidgets.QTableWidgetItem("{:1.4f}".format(correction))
                )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Stage
            col += 1
            if np.isnan(self.meas.summary["Stage"][idx]):
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            else:
                tbl.setItem(
                    row,
                    col,
                    QtWidgets.QTableWidgetItem(
                        "{:3.3f}".format(
                            self.meas.summary["Stage"][idx] * self.units["L"]
                        )
                    ),
                )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Stage Time
            col += 1
            try:
                if len(self.meas.summary["Stage Time"][idx]) == 0:
                    tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                else:
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{}".format(self.meas.summary["Stage Time"][idx])
                        ),
                    )
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
            except TypeError:
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))

            # Discharge before changes
            col += 1
            tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            if idx in old_discharge.index:
                if not np.isnan(self.meas.summary["Q"][idx]):
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:8}".format(
                                self.q_digits(old_discharge["Q"][idx] * self.units["Q"])
                            )
                        ),
                    )

            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Discharge after changes
            col += 1
            tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            if idx in self.meas.summary.index:
                if not np.isnan(self.meas.summary["Q"][idx]):
                    tbl.setItem(
                        row,
                        col,
                        QtWidgets.QTableWidgetItem(
                            "{:8}".format(
                                self.q_digits(
                                    self.meas.summary["Q"][idx] * self.units["Q"]
                                )
                            )
                        ),
                    )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            # Percent difference in old and new discharges
            col += 1
            tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
            if idx in old_discharge.index and idx in self.meas.summary.index:
                if not np.isnan(self.meas.summary["Q"][idx]):
                    if np.abs(old_discharge["Q"][idx]) > 0:
                        per_change = (
                            (self.meas.summary["Q"][idx] - old_discharge["Q"][idx])
                            / old_discharge["Q"][idx]
                        ) * 100
                        tbl.setItem(
                            row,
                            col,
                            QtWidgets.QTableWidgetItem("{:3.1f}".format(per_change)),
                        )

                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

        tbl.item(tbl.rowCount() - 1, 5).setFont(self.font_bold)
        tbl.item(tbl.rowCount() - 1, 6).setFont(self.font_bold)
        tbl.item(tbl.rowCount() - 1, 7).setFont(self.font_bold)

        item = tbl.horizontalHeaderItem(7)
        if self.meas.qa.edges["status"] == "caution":
            item.setForeground(QtGui.QColor(230, 138, 0))
            item.setToolTip("Check sign and percent of total discharge")
        else:
            item.setForeground(QtGui.QColor(0, 0, 0))
            item.setToolTip("")

        if self.meas.discharge_method == "Mid":
            for col in range(tbl.columnCount()):
                tbl.item(0, col).setBackground(QtGui.QColor(230, 230, 230))
                tbl.item(1, col).setBackground(QtGui.QColor(230, 230, 230))
        else:
            for col in range(tbl.columnCount()):
                tbl.item(0, col).setBackground(QtGui.QColor(230, 230, 230))
                tbl.item(1, col).setBackground(QtGui.QColor(230, 230, 230))
                tbl.item(2, col).setBackground(QtGui.QColor(230, 230, 230))

        tbl.show()
        # Set highlight selected vertical
        # self.highlight_row(tbl, self.main_row_idx - 1)

        tbl.resizeColumnsToContents()
        tbl.resizeRowsToContents()

    def edges_settings_update(self):
        """Update display of edge settings."""

        self.rb_edges_start_left.blockSignals(True)
        self.rb_edges_start_right.blockSignals(True)
        self.rb_edges_end_left.blockSignals(True)
        self.rb_edges_end_right.blockSignals(True)
        self.ed_edges_start_location.blockSignals(True)
        self.ed_edges_start_depth.blockSignals(True)
        self.ed_edges_start_correction.blockSignals(True)
        self.ed_edges_start_stage.blockSignals(True)
        self.ed_edges_start_stage_time.blockSignals(True)
        self.ed_edges_end_location.blockSignals(True)
        self.ed_edges_end_depth.blockSignals(True)
        self.ed_edges_end_correction.blockSignals(True)
        self.ed_edges_end_stage.blockSignals(True)
        self.ed_edges_end_stage_time.blockSignals(True)

        # Start Edge
        vertical = self.meas.verticals[self.meas.verticals_used_idx[0]]
        if vertical.is_edge:
            if vertical.edge_loc == "Left":
                self.rb_edges_start_left.setChecked(True)
                self.rb_edges_end_right.setChecked(True)
            else:
                self.rb_edges_start_right.setChecked(True)
                self.rb_edges_end_left.setChecked(True)
            self.ed_edges_start_location.setText(
                "{:6.2f}".format(vertical.location_m * self.units["L"])
            )
            self.ed_edges_start_depth.setText(
                "{:6.2f}".format(vertical.depth_m * self.units["L"])
            )
            if self.meas.discharge_method == "Mean":
                self.ed_edges_start_correction.setText(
                    "{:1.4f}".format(vertical.edge_coef)
                )
            else:
                self.ed_edges_start_correction.setText(
                    "{:1.4f}".format(vertical.velocity_correction)
                )
            self.ed_edges_start_stage.setText(
                "{:4.2f}".format(vertical.gh_m * self.units["L"])
            )
            self.ed_edges_start_stage_time.setText(vertical.gh_obs_time)
        else:
            self.ed_edges_start_location.setText("")
            self.ed_edges_start_depth.setText("")
            self.ed_edges_start_correction.setText("")
            self.ed_edges_start_stage.setText("")
            self.ed_edges_start_stage_time.setText("")

        # End Edge
        vertical = self.meas.verticals[self.meas.verticals_used_idx[-1]]
        if vertical.is_edge:
            if vertical.edge_loc == "Left":
                self.rb_edges_end_left.setChecked(True)
                self.rb_edges_start_right.setChecked(True)
            else:
                self.rb_edges_end_right.setChecked(True)
                self.rb_edges_start_left.setChecked(True)
            self.ed_edges_end_location.setText(
                "{:6.2f}".format(vertical.location_m * self.units["L"])
            )
            self.ed_edges_end_depth.setText(
                "{:6.2f}".format(vertical.depth_m * self.units["L"])
            )
            if self.meas.discharge_method == "Mean":
                self.ed_edges_end_correction.setText(
                    "{:1.4f}".format(vertical.edge_coef)
                )
            else:
                self.ed_edges_end_correction.setText(
                    "{:1.4f}".format(vertical.velocity_correction)
                )
            self.ed_edges_end_stage.setText(
                "{:4.2f}".format(vertical.gh_m * self.units["L"])
            )
            self.ed_edges_end_stage_time.setText(vertical.gh_obs_time)
        else:
            self.ed_edges_end_location.setText("")
            self.ed_edges_end_depth.setText("")
            self.ed_edges_end_correction.setText("")
            self.ed_edges_end_stage.setText("")
            self.ed_edges_end_stage_time.setText("")

        self.rb_edges_start_left.blockSignals(False)
        self.rb_edges_start_right.blockSignals(False)
        self.rb_edges_end_left.blockSignals(False)
        self.rb_edges_end_right.blockSignals(False)
        self.ed_edges_start_location.blockSignals(False)
        self.ed_edges_start_depth.blockSignals(False)
        self.ed_edges_start_correction.blockSignals(False)
        self.ed_edges_start_stage.blockSignals(False)
        self.ed_edges_start_stage_time.blockSignals(False)
        self.ed_edges_end_location.blockSignals(False)
        self.ed_edges_end_depth.blockSignals(False)
        self.ed_edges_end_correction.blockSignals(False)
        self.ed_edges_end_stage.blockSignals(False)
        self.ed_edges_end_stage_time.blockSignals(False)

    def edges_graph(self):
        """Calls methods to create graphs on edges tab."""
        self.edges_contour_graph()
        self.edges_bar_graph()

        # Update list of figs
        self.canvases = [self.edges_contour_canvas, self.edges_bar_canvas]
        self.figs = [self.edges_contour_fig, self.edges_bar_fig]
        self.toolbars = [self.edges_contour_toolbar, self.edges_bar_toolbar]
        self.fig_calls = [self.edges_contour_graph, self.edges_bar_graph]
        self.ui_parents = [i.parent() for i in self.canvases]
        self.figs_menu_connection()

    def edges_contour_graph(self):
        """Creates plots for reference on edges tab."""

        # If the canvas has not been previously created, create the canvas and add the widget.
        if self.edges_contour_canvas is None:
            # Create the canvas
            self.edges_contour_canvas = MplCanvas(
                parent=self.graph_edges_contour, width=4, height=4, dpi=80
            )
            # Assign layout to widget to allow auto scaling
            layout = QtWidgets.QVBoxLayout(self.graph_edges_contour)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(1, 1, 1, 1)
            # Add the canvas
            layout.addWidget(self.edges_contour_canvas)
            # Initialize hidden toolbar for use by graphics controls
            self.edges_contour_toolbar = NavigationToolbar(
                self.edges_contour_canvas, self
            )
            self.edges_contour_toolbar.hide()

        # Initialize the figure and assign to the canvas
        self.edges_contour_fig = Graphics(canvas=self.edges_contour_canvas)
        # Create the figure with the specified data
        self.edges_contour_fig.create_edges_tab_contour(
            meas=self.meas, units=self.units, x_axis_type="L", color_map=self.color_map
        )

        self.edges_contour_canvas.draw()

    def edges_bar_graph(self):
        """Creates plots for reference on edges tab."""

        # If the canvas has not been previously created, create the canvas and add the widget.
        if self.edges_bar_canvas is None:
            # Create the canvas
            self.edges_bar_canvas = MplCanvas(
                parent=self.graph_edges_bar, width=4, height=4, dpi=80
            )
            # Assign layout to widget to allow auto scaling
            layout = QtWidgets.QVBoxLayout(self.graph_edges_bar)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(1, 1, 1, 1)
            # Add the canvas
            layout.addWidget(self.edges_bar_canvas)
            # Initialize hidden toolbar for use by graphics controls
            self.edges_bar_toolbar = NavigationToolbar(self.edges_bar_canvas, self)
            self.edges_bar_toolbar.hide()

        # Initialize the figure and assign to the canvas
        self.edges_bar_fig = Graphics(canvas=self.edges_bar_canvas)
        # Create the figure with the specified data
        self.edges_bar_fig.create_edges_tab_bar(
            meas=self.meas,
            units=self.units,
            color_dict=self.plt_color_dict,
            agency_options=self.agency_options,
        )

        # Reset data cursor to work with new figure
        if self.actionData_Cursor.isChecked():
            self.data_cursor()

        self.edges_bar_canvas.draw()

    def edges_start_enable_apply(self):
        """Enables the apply button for start edge."""
        self.pb_edges_start_apply.setEnabled(True)

    def edges_end_enable_apply(self):
        """Enables the apply button for end edge."""
        self.pb_edges_end_apply.setEnabled(True)

    def edges_change_edge(self):
        """Applies the changes for the selected edge."""

        old_discharge = self.meas.summary

        # Identify edge to change
        if self.sender().objectName() == "pb_edges_start_apply":
            # Start edge
            location = (
                self.check_numeric_input(self.ed_edges_start_location) / self.units["L"]
            )
            location_check = self.edges_check_location(start_edge=True, location=location)
            if location_check:
                # Depth
                depth = (
                    self.check_numeric_input(self.ed_edges_start_depth, min_value=0))
                if depth is not None:
                    depth = depth / self.units["L"]
                    # Correction
                    correction = self.check_numeric_input(
                        self.ed_edges_start_correction, min_value=0
                    )
                    if correction is None:
                        if self.meas.discharge_method == "Mid":
                            correction = 0.0
                        else:
                            correction = 0.707
                    # Stage
                    stage = self.check_numeric_input(self.ed_edges_start_stage)
                    if stage is None:
                        stage = 0.0
                    else:
                        stage = stage / self.units["L"]

                    stage_time = self.ed_edges_start_stage_time.text()

                    if self.rb_edges_start_left.isChecked():
                        edge_bank = "Left"
                    else:
                        edge_bank = "Right"

                    self.meas.change_edge(
                        edge_idx=0,
                        edge_bank=edge_bank,
                        location=location,
                        depth=depth,
                        correction=correction,
                        stage=stage,
                        stage_time=stage_time,
                    )
                    self.pb_edges_start_apply.setEnabled(False)
        else:
            # End edge
            location = (
                self.check_numeric_input(self.ed_edges_end_location) / self.units["L"]
            )
            location_check = self.edges_check_location(start_edge=False, location=location)
            if location_check:
                # Depth
                depth = (
                    self.check_numeric_input(self.ed_edges_end_depth, min_value=0))
                if depth is not None:
                    depth = depth / self.units["L"]
                    # Correction
                    correction = self.check_numeric_input(
                        self.ed_edges_end_correction, min_value=0
                    )
                    if correction is None:
                        if self.meas.discharge_method == "Mid":
                            correction = 0.0
                        else:
                            correction = 0.707
                    # Stage
                    stage = self.check_numeric_input(self.ed_edges_end_stage)
                    if stage is None:
                        stage = 0.0
                    else:
                        stage = stage / self.units["L"]

                    stage_time = self.ed_edges_end_stage_time.text()

                    if self.rb_edges_end_left.isChecked():
                        edge_bank = "Left"
                    else:
                        edge_bank = "Right"
                    self.meas.change_edge(
                        edge_idx=-1,
                        edge_bank=edge_bank,
                        location=location,
                        depth=depth,
                        correction=correction,
                        stage=stage,
                        stage_time=stage_time,
                    )
                    self.pb_edges_end_apply.setEnabled(False)

        self.change = True

        # Update GUI
        self.edges_tab_update(old_discharge=old_discharge)
        self.tab_edges_subtab_data.setFocus()

    def edges_check_location(self, start_edge, location):
        """Verifies that a new edge location is outside the other station locations.

        Parameters
        ----------
        start_edge: bool
            Indicates if this is a start edge
        location: float
            New location in m

        Returns
        -------
        check: bool
            True if location is valid
        """

        check = True
        start_loc = self.meas.verticals[self.meas.verticals_used_idx[0]].location_m
        end_loc = self.meas.verticals[self.meas.verticals_used_idx[-1]].location_m

        if start_edge:
            if start_loc < end_loc:
                if location >= self.meas.verticals[self.meas.verticals_used_idx[1]].location_m:
                    self.popup_message("Start location must be less than next station.")
                    check = False
            else:
                if location <= self.meas.verticals[self.meas.verticals_used_idx[1]].location_m:
                    self.popup_message("Start location must be greater than next station.")
                    check = False
        else:
            if start_loc < end_loc:
                if location <= self.meas.verticals[self.meas.verticals_used_idx[-2]].location_m:
                    self.popup_message("End location must be greater than previous station.")
                    check = False
            else:
                if location >= self.meas.verticals[self.meas.verticals_used_idx[-2]].location_m:
                    self.popup_message("End location must be less than previous station.")
                    check = False
        return check

    def edges_comments_messages(self):
        """Displays comments and messages associated with edge filters
        in Messages tab.
        """

        # Clear comments and messages
        self.display_edges_comments.clear()

        if self.meas is not None:
            # Display each comment on a new line
            self.display_edges_comments.moveCursor(QtGui.QTextCursor.Start)
            for comment in self.meas.comments:
                self.display_edges_comments.textCursor().insertText(comment)
                self.display_edges_comments.moveCursor(QtGui.QTextCursor.End)
                self.display_edges_comments.textCursor().insertBlock()

            # Display each message on a new line
            self.messages_table(self.table_edges_messages, ["edges"])

            self.update_tab_icons()

    # Uncertainty tab
    # ===============
    def uncertainty_tab_initial(self):
        self.uncertainty_tab_uncertainty_table()
        self.uncertainty_tab_settings_table()
        self.uncertainty_tab_graph()

        self.uncertainty_comment_textedit.clear()
        if self.meas is not None:
            self.uncertainty_comment_textedit.moveCursor(QtGui.QTextCursor.Start)
            for comment in self.meas.comments:
                # Display each comment on a new line
                self.uncertainty_comment_textedit.textCursor().insertText(comment)
                self.uncertainty_comment_textedit.moveCursor(QtGui.QTextCursor.End)
                self.uncertainty_comment_textedit.textCursor().insertBlock()
        tab = "uncertainty_tab"
        if len(self.meas.uncertainty.method) > 3:
            self.top_tab.tabBar().setTabTextColor(
                self.top_tab.indexOf(self.top_tab.findChild(QtWidgets.QWidget, tab)),
                QtGui.QColor(0, 0, 255),
            )
        else:
            self.top_tab.tabBar().setTabTextColor(
                self.top_tab.indexOf(self.top_tab.findChild(QtWidgets.QWidget, tab)),
                QtGui.QColor(0, 0, 0),
            )

    def uncertainty_tab_uncertainty_table(self):
        # Configure uncertainty table
        tbl = self.table_uncertainty
        column_labels = [
            self.tr("Component"),
            self.tr("ISO"),
            self.tr("IVE"),
            self.tr("User"),
        ]

        row_labels = [
            self.tr("Systematic"),
            self.tr("# Stations"),
            self.tr("# Cells"),
            self.tr("Width"),
            self.tr("Depth"),
            self.tr("Velocity"),
            self.tr("Instrument Repeatability"),
            self.tr("Total Uncertainty"),
            self.tr("Expanded Uncertainty"),
        ]
        row_idx = ["u_s", "u_m", "u_p", "u_b", "u_d", "u_v", "u_c", "u_q", "u95_q"]
        ncols = len(column_labels)
        nrows = len(row_labels)

        tbl.setColumnCount(ncols)
        tbl.setRowCount(nrows)
        tbl.setHorizontalHeaderLabels(column_labels)
        tbl.verticalHeader().hide()
        tbl.horizontalHeader().setFont(self.font_bold)
        # tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)
        tbl.itemChanged.connect(self.user_uncertainty_change)
        tbl.itemChanged.disconnect()

        for row in range(nrows):
            # Component
            col = 0
            tbl.setItem(row, col, QtWidgets.QTableWidgetItem(row_labels[row]))
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.item(row, col).setFont(self.font_bold)

            # ISO
            col = 1
            tbl.setItem(
                row,
                col,
                QtWidgets.QTableWidgetItem(
                    f"{self.meas.uncertainty.u_iso[row_idx[row]] * 100:.2f}"
                ),
            )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
            if self.meas.uncertainty.method == "ISO":
                tbl.item(row, col).setFont(self.font_bold)
            # IVE
            col = 2
            if row_idx[row] in self.meas.uncertainty.u_ive:
                tbl.setItem(
                    row,
                    col,
                    QtWidgets.QTableWidgetItem(
                        f"{self.meas.uncertainty.u_ive[row_idx[row]] * 100:.2f}"
                    ),
                )
            else:
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                tbl.item(row, col).setBackground(QtGui.QColor(100, 100, 100))
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
            if self.meas.uncertainty.method == "IVE":
                tbl.item(row, col).setFont(self.font_bold)

            # User
            col = 3
            if not np.isnan(self.meas.uncertainty.u_user[row_idx[row]]):
                tbl.setItem(
                    row,
                    col,
                    QtWidgets.QTableWidgetItem(
                        f"{self.meas.uncertainty.u_user[row_idx[row]] * 100:.2f}"
                    ),
                )
                tbl.item(row, col).setFont(self.font_bold)
                tbl.item(row, 1).setFont(self.font_normal)
                tbl.item(row, 2).setFont(self.font_normal)
            else:
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))
                if self.meas.uncertainty.method[0:3] == "IVE":
                    if not row_idx[row] in self.meas.uncertainty.u_ive:
                        tbl.item(row, col).setBackground(QtGui.QColor(100, 100, 100))
                    else:
                        tbl.item(row, 1).setFont(self.font_normal)
                        tbl.item(row, 2).setFont(self.font_bold)
                else:
                    tbl.item(row, 2).setFont(self.font_normal)
                    tbl.item(row, 1).setFont(self.font_bold)
            if row > 6:
                tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

        tbl.resizeColumnsToContents()
        tbl.resizeRowsToContents()
        self.uncertainty_results_idx = row_idx
        tbl.itemChanged.connect(self.user_uncertainty_change)

    def uncertainty_tab_settings_table(self):
        # Configure settings table
        tbl = self.table_uncertainty_settings
        column_labels = [
            self.tr("Component"),
            self.tr("Base \n Default"),
            self.tr("User"),
        ]
        row_labels = [
            self.tr("Other Systematic"),
            self.tr("Instrument Systematic"),
            self.tr("# Stations (ISO Only)"),
            self.tr("Width:"),
            self.tr("Instrument Repeatability"),
        ]
        row_idx = ["u_so", "u_sm", "u_m", "u_b", "u_c"]
        ncols = len(column_labels)
        nrows = len(row_labels)

        tbl.setColumnCount(ncols)
        tbl.setRowCount(nrows)
        tbl.setHorizontalHeaderLabels(column_labels)
        tbl.verticalHeader().hide()
        tbl.horizontalHeader().setFont(self.font_bold)
        # tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)
        tbl.itemChanged.connect(self.user_uncertainty_setting_change)
        tbl.itemChanged.disconnect()

        for row in range(nrows):
            col = 0
            tbl.setItem(row, col, QtWidgets.QTableWidgetItem(row_labels[row]))
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)
            tbl.item(row, col).setFont(self.font_bold)

            col = 1
            tbl.setItem(
                row,
                col,
                QtWidgets.QTableWidgetItem(
                    f"{self.meas.uncertainty.default_values[row_idx[row]] * 100:.2f}"
                ),
            )
            tbl.item(row, col).setFlags(QtCore.Qt.ItemIsEnabled)

            col = 2
            if not np.isnan(self.meas.uncertainty.user_values[row_idx[row]]):
                tbl.setItem(
                    row,
                    col,
                    QtWidgets.QTableWidgetItem(
                        f"{self.meas.uncertainty.user_values[row_idx[row]] * 100:.2f}"
                    ),
                )
            else:
                tbl.setItem(row, col, QtWidgets.QTableWidgetItem(""))

        tbl.resizeColumnsToContents()
        tbl.resizeRowsToContents()
        self.uncertainty_settings_idx = row_idx
        tbl.itemChanged.connect(self.user_uncertainty_setting_change)

    def uncertainty_tab_graph(self):
        """Creates the uncertainty graph."""

        if self.uncertainty_canvas is None:
            # Create the canvas
            self.uncertainty_canvas = MplCanvas(
                parent=self.graph_uncertainty_tab, width=4, height=4, dpi=80
            )
            # Assign layout to widget to allow auto scaling
            layout = QtWidgets.QVBoxLayout(self.graph_uncertainty_tab)
            # Adjust margins of layout to maximize graphic area
            layout.setContentsMargins(1, 1, 1, 1)
            # Add the canvas
            layout.addWidget(self.uncertainty_canvas)
            # Initialize hidden toolbar for use by graphics controls
            self.uncertainty_toolbar = NavigationToolbar(self.uncertainty_canvas, self)
            self.uncertainty_toolbar.hide()

        # Initialize the  figure and assign to the canvas
        self.uncertainty_fig = Graphics(canvas=self.uncertainty_canvas)
        # Create the figure with the specified data
        if self.meas.uncertainty is not None:
            self.uncertainty_fig.uncertainty_plot(self.meas.uncertainty)
            self.uncertainty_fig.fig.ax.xaxis.label.set_fontsize(14)
            self.uncertainty_fig.fig.ax.tick_params(
                axis="both", which="major", labelsize=14
            )
        else:
            self.main_uncertainty_fig.fig.clear()

        # Draw canvas
        self.uncertainty_canvas.draw()
        self.figs = [self.uncertainty_fig]
        self.canvases = [self.uncertainty_canvas]
        self.toolbars = [self.uncertainty_toolbar]
        self.fig_calls = [self.uncertainty_tab_graph]
        self.ui_parents = [i.parent() for i in self.canvases]
        self.figs_menu_connection()

    def user_uncertainty_change(self):
        # Request justification from user
        self.add_comment()

        with self.wait_cursor():
            new_value = self.check_numeric_input(
                obj=self.table_uncertainty.selectedItems()[0], block=False
            )
            if new_value is None:
                new_value = np.nan
            else:
                new_value = new_value / 100

            col_index = self.table_uncertainty.selectedItems()[0].column()
            if col_index == 3:
                row_index = self.table_uncertainty.selectedItems()[0].row()
                if row_index < 7:
                    if self.meas.uncertainty.method == "IVE":
                        if row_index in [0, 3, 4, 5]:
                            self.meas.uncertainty.compute_user_uncertainty(
                                u_x=self.uncertainty_results_idx[row_index],
                                value=new_value,
                            )
                    else:
                        self.meas.uncertainty.compute_user_uncertainty(
                            u_x=self.uncertainty_results_idx[row_index], value=new_value
                        )
                self.uncertainty_tab_initial()

    def user_uncertainty_setting_change(self):
        # Request justification from user
        self.add_comment()

        with self.wait_cursor():
            new_value = self.check_numeric_input(
                obj=self.table_uncertainty_settings.selectedItems()[0], block=False
            )
            if new_value is None:
                new_value = np.nan
            else:
                new_value = new_value / 100

            col_index = self.table_uncertainty_settings.selectedItems()[0].column()
            if col_index == 2:
                row_index = self.table_uncertainty_settings.selectedItems()[0].row()
                self.meas.uncertainty.compute_uncertainty(
                    meas=self.meas,
                    u_x=self.uncertainty_settings_idx[row_index],
                    value=new_value,
                )
            self.uncertainty_tab_initial()

    # Graphics controls
    # =================
    def graphically_select_vertical(self, x, x_type="L"):
        """Selects the vertical based on a click in one of the graphics.

        Parameters
        ----------
        x: float
            X-coordinate of click
        x_type: str
            Type of coordinate L-location, S-station
        """
        if (
            not self.actionData_Cursor.isChecked()
            and not self.actionPan.isChecked()
            and not self.actionZoom.isChecked()
        ):
            if x_type == "L":
                x = x / self.units["L"]
                idx = self.meas.summary.iloc[
                    (self.meas.summary["Location"] - x).abs().argsort()[
                    :1]].index.tolist()[0]
            else:
                idx = self.meas.summary.index[
                    self.meas.summary["Station Number"] == np.round(x)].tolist()[0]
            if self.top_tab.currentIndex() == 0:
                self.main_select_vertical(idx + 1)
            else:
                row = np.where(self.meas.verticals_sorted_idx == self.meas.verticals_used_idx[idx])[0][0]
                self.stations_table_clicked(int(row), 1)

    def clear_zphd(self):
        """Clears the graphics user controls."""

        try:
            self.actionData_Cursor.setChecked(False)
            for fig in self.figs:
                fig.set_hover_connection(False)
            if self.actionPan.isChecked():
                self.actionPan.trigger()
            if self.actionZoom.isChecked():
                self.actionZoom.trigger()
        except (AttributeError, SystemError):
            pass

    def figs_menu_connection(self):
        """Connect ui to event filter for current figures."""
        for fig in self.figs:
            fig.canvas.parent().installEventFilter(self)

    def eventFilter(self, source, event):
        """Load events."""

        if event.type() == QtCore.QEvent.ContextMenu:
            if source in self.ui_parents:
                self.current_fig = self.figs[self.ui_parents.index(source)]

                # Determine axes of graph in which the click occured and save
                # for use by change axes limits
                extents = []
                for ax in self.current_fig.fig.axes:
                    extents.append(ax.bbox.extents)
                extents = np.array(extents)
                y_max = np.nanmax(extents[:, -1])
                y_adjusted = y_max - event.y()
                ax = np.where(
                    np.logical_and(
                        np.greater(event.x(), extents[:, 0]),
                        np.less(y_adjusted, extents[:, 3]),
                    )
                )[0]
                try:
                    self.current_axis = self.current_fig.fig.axes[ax[-1]]
                except IndexError:
                    return False

                # Context menu
                self.figsMenu.exec_(event.globalPos())
                return True
        return super().eventFilter(source, event)

    def save_fig(self):
        """Save selected figure."""
        if self.current_fig is not None:
            # Get the current folder setting.
            save_fig = SaveDialog(parent=self, save_type="fig")
            # Save figure in user format
            if len(save_fig.full_Name) > 0:
                self.current_fig.fig.savefig(
                    save_fig.full_Name, dpi=300, bbox_inches="tight"
                )

    def set_axes(self):
        """Scales the selected graph to either user specifications of automatics
        scaling.
        """

        scale = AxesScale()

        # Get current axes limits
        x_limits = self.current_axis.get_xlim()
        y_limits = self.current_axis.get_ylim()

        # Show limits in dialog
        scale.ed_x_left.setText("%.2f" % x_limits[0])
        scale.ed_x_right.setText("%.2f" % x_limits[1])
        scale.ed_y_bottom.setText("%.2f" % y_limits[0])
        scale.ed_y_top.setText("%.2f" % y_limits[1])

        rsp = scale.exec_()

        with self.wait_cursor():
            # Apply settings from options window
            if rsp == QtWidgets.QDialog.Accepted:
                if scale.cb_axes_auto.isChecked():
                    # If automatic is selected the original method that created the graph
                    # is identified and called. However, if that method cannot be
                    # identified or it requires extra arguments then the plot is
                    # rescaled using the data available from the plot axes. The
                    # reason the original plot method has priority is that for some
                    # graphs the tick scaling is customized.
                    try:
                        self.fig_calls[self.figs.index(self.current_fig)]()
                    except:
                        # Rescale the plot using data from the plot
                        ydata = np.array([])
                        xdata = np.array([])
                        for line in self.current_axis.lines:
                            ydata = np.hstack((ydata, line.get_ydata()))
                            xdata = np.hstack((xdata, line.get_xdata()))
                        ydata_max = np.nanmax(ydata) * 1.02
                        ydata_min = 0 - np.nanmax(ydata) * 0.02
                        xdata_max = np.nanmax(xdata) * 1.02
                        xdata_min = 0 - np.nanmax(xdata) * 0.02

                        if np.isnan(ydata_max):
                            ydata_max = 1
                            ydata_min = 0
                        if np.isnan(xdata_max):
                            xdata_max = 1
                            xdata_min = 0

                        if x_limits[0] < x_limits[1]:
                            new_x_limits = [xdata_min, xdata_max]
                        else:
                            new_x_limits = [xdata_max, xdata_min]

                        if y_limits[0] < y_limits[1]:
                            new_y_limits = [ydata_min, ydata_max]
                        else:
                            new_y_limits = [ydata_max, ydata_min]
                else:
                    x_left = self.check_numeric_input(scale.ed_x_left, block=False)
                    x_right = self.check_numeric_input(scale.ed_x_right, block=False)
                    y_bottom = self.check_numeric_input(scale.ed_y_bottom, block=False)
                    y_top = self.check_numeric_input(scale.ed_y_top, block=False)
                    new_x_limits = [x_left, x_right]
                    new_y_limits = [y_bottom, y_top]

                # Set new limits
                if not any(new_x_limits) is None and not any(new_y_limits) is None:
                    self.current_axis.set_xlim(left=x_left, right=x_right)
                    self.current_axis.set_ylim(bottom=y_bottom, top=y_top)
                    self.current_axis.yaxis.set_major_locator(AutoLocator())
                    self.current_fig.canvas.draw()

    # Support methods
    # ===============
    def combine_selected_qa_messages(self, qa_check_keys):
        """Combines multiple qa keys into a single list including the associated guidance.

        Parameters
        ----------
        qa_check_keys: list
            List of qa attributes

        Returns
        -------
        messages: list
            List of messages, codes, and guidance
        """
        # Initialize local variables
        qa = self.meas.qa


        # For each qa check retrieve messages and set tab icon based on the
        # status
        messages = []
        for key in qa_check_keys:
            if hasattr(qa, key):
                qa_type = copy.deepcopy(getattr(qa, key))
                if qa_type["messages"]:
                    for idx, message in enumerate(qa_type["messages"]):
                        if type(message) == np.ndarray:
                            message = message.tolist()
                        if type(message) is str:
                            if message[:3].isupper():
                                messages.append([message, 1])
                            else:
                                messages.append([message, 2])
                        else:
                            message[1] = int(message[1])
                            messages.append(message)
                        if len(qa_type["guidance"]) > 0:
                            messages[-1].append(qa_type["guidance"][idx])
                        else:
                            messages[-1].append("")
                self.set_icon(key, qa_type["status"])

        # Sort messages with warning at top
        messages.sort(key=lambda x: x[1])

        return messages

    def messages_table(self, tbl, qa_check_keys, messages=None):
        """Creates a messages table with tooltips containing guidance.

        Parameter
        ---------
        tbl: QTableWidget
            Object of QTableWidget to be populated
        qa_check_keys: list
            List of qa attributes to be included in the table
        """
        if messages is None:
            messages = self.combine_selected_qa_messages(qa_check_keys)
        # Setup table
        tbl.clear()
        tbl_header = [self.tr("Status"), self.tr("Message")]
        ncols = len(tbl_header)
        nrows = len(messages)
        tbl.setRowCount(nrows + 1)
        tbl.setColumnCount(ncols)
        tbl.setHorizontalHeaderLabels(tbl_header)
        hh_font = tbl.horizontalHeader().font()
        hh_font.setPointSize(12)
        hh_font.setBold(True)
        tbl.horizontalHeader().setFont(hh_font)
        tbl.verticalHeader().hide()
        tbl.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)

        # Populate table
        for row, message in enumerate(messages):
            # Handle messages from old QRev that did not have integer codes
            if type(message) is str:
                warn = message[:3].isupper()
                tbl.setItem(row, 1, QtWidgets.QTableWidgetItem(message))
            # Handle newer style messages
            else:
                try:
                    warn = int(message[1]) == 1
                except ValueError:
                    if "ERROR" in message[0]:
                        warn = True
                    else:
                        warn = False
                tbl.setItem(row, 1, QtWidgets.QTableWidgetItem(message[0]))
            if warn:
                tbl.item(row, 1).setFont(self.font_bold)
                item_warning = QtWidgets.QTableWidgetItem(self.icon_warning, "")
                tbl.setItem(row, 0, item_warning)
            else:
                tbl.item(row, 1).setFont(self.font_normal)
                item_caution = QtWidgets.QTableWidgetItem(self.icon_caution, "")
                tbl.setItem(row, 0, item_caution)
            if len(message[-1]) > 0:
                tbl.item(row, 1).setToolTip(message[-1])
        tbl.resizeColumnsToContents()
        tbl.resizeRowsToContents()
    @staticmethod
    def check_numeric_input(obj, block=True, min_value=None):
        """Check if input is a numeric object.

        Parameters
        ----------
        obj: QtWidget
            QtWidget user edit box.
        block: bool
            Block signals
        min_value: float
            Minimum allowed value

        Returns
        -------
        out: float
            obj converted to float if possible
        """
        if block:
            obj.blockSignals(True)
        out = None
        if len(obj.text()) > 0:
            # Try to convert obj to a float
            try:
                text = obj.text()
                out = float(text)
                if min_value is not None:
                    if out >= min_value:
                        return out
                    else:
                        obj.setText("")
                        msg = QtWidgets.QMessageBox()
                        msg.setIcon(QtWidgets.QMessageBox.Critical)
                        msg.setText("Error")
                        msg.setInformativeText(
                            "Value must be >= {:0.2f}".format(min_value)
                        )
                        msg.setWindowTitle("Error")
                        msg.exec_()
                        return None
            # If conversion is not successful warn user that the input is invalid.
            except ValueError:
                obj.setText("")
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Critical)
                msg.setText("Error")
                msg.setInformativeText(
                    "You have entered non-numeric text. Enter a numeric value."
                )
                msg.setWindowTitle("Error")
                msg.exec_()
                return None

        if block:
            obj.blockSignals(False)
        return out

    def find_first_measured_vertical(self):
        """Finds the index of the first measured vertical."""

        self.main_row_idx = (
            self.meas.summary.loc[self.meas.summary["Duration"] > 0].index[0] + 1
        )
        if self.meas.discharge_method == "Mean":
            self.vertical_idx = self.meas.verticals_used_idx[
                int(self.meas.summary["Station Number"][self.main_row_idx] - 1)
            ]
        else:
            self.vertical_idx = self.meas.verticals_used_idx[
                int(self.meas.summary["Station Number"][self.main_row_idx] - 2)
            ]

        self.stations_row = np.where(
            self.meas.verticals_sorted_idx == self.vertical_idx
        )[0][0]

    @contextmanager
    def wait_cursor(self):
        """Provide a busy cursor to the user while the code is processing."""
        try:
            QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
            yield
        finally:
            QtWidgets.QApplication.restoreOverrideCursor()

    def tooltip_qa_message(
        self,
        qa_data,
        cat,
        vertical_id,
        total_threshold_warning,
        total_threshold_caution,
    ):
        """Creates tooltip based on QA results.

        Parameters
        ----------
        qa_data: dict
            Dictionary of QA results for the vertical
        cat: str or None
            Category or type of QA check
        vertical_id: int
            Index of vertical
        total_threshold_warning: float
            Threshold for issuing a warning
        total_threshold_caution: float
            Threshold for issuing a caution

        Returns
        -------
        text: str
            Text for tooltip
        """

        text = []

        if cat is not None:
            if vertical_id in qa_data["all_invalid"][cat]:
                text.append("No valid data. ")

            elif vertical_id in qa_data["total_warning"][cat]:
                text.append(
                    self.tr("The percent of invalid data in this vertical exceeds")
                    + "%3.0f" % (100 - total_threshold_warning)
                    + "%; "
                )

            elif vertical_id in qa_data["total_caution"][cat]:
                text.append(
                    self.tr("The percent of invalid data in this vertical exceeds ")
                    + "%3.0f" % (100 - total_threshold_caution)
                    + "%; "
                )

        else:
            if vertical_id in qa_data["all_invalid"]:
                text.append("No valid data. \n")

            elif vertical_id in qa_data["total_warning"]:
                text.append(
                    self.tr("The percent of invalid ensembles in this vertical exceeds")
                    + "%3.0f" % (100 - total_threshold_warning)
                    + "%;\n"
                )

            elif vertical_id in qa_data["total_caution"]:
                text.append(
                    self.tr(
                        "The percent of invalid ensembles in this vertical exceeds "
                    )
                    + "%3.0f" % (100 - total_threshold_caution)
                    + "%;\n"
                )

        return text

    @staticmethod
    def popup_message(text):
        """Display a message box with messages specified in text.

        Parameters
        ----------
        text: str
            Message to be displayed.
        """

        QtWidgets.QApplication.restoreOverrideCursor()
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        msg.setText("Error")
        msg.setInformativeText(text)
        msg.setWindowTitle("Error")
        msg.exec_()
        return

    def closeEvent(self, event):
        """Warns user when closing QRev.

        Parameters
        ----------
        event: QCloseEvent
            Object of QCloseEvent
        """
        if event:
            if self.meas is not None:
                close = QtWidgets.QMessageBox()
                close.setIcon(QtWidgets.QMessageBox.Warning)
                close.setWindowTitle("Close")
                close.setText(
                    self.tr(
                        "If you haven't saved your data, "
                        "changes will be lost. \n Are you sure you want to Close? "
                    )
                )
                close.setStandardButtons(
                    QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.Cancel
                )
                close = close.exec()

                if close == QtWidgets.QMessageBox.Yes:
                    event.accept()
                else:
                    event.ignore()
            else:
                event.accept()
        else:
            event.accept()

    def keyPressEvent(self, e):
        """Implements shortcut keys.

        Parameters
        ----------
        e: event
            Event generated by key pressed by user
        """

        key_recognized = False

        # Help
        if e.key() == QtCore.Qt.Key_F1:
            self.help()

        else:
            # Change displayed row
            # Down arrow
            if e.key() == QtCore.Qt.Key_Down:
                # Increment row for stations tab, this tab has all verticals
                # whether used or not. It does not contain discharge
                if self.current_tab.strip() == "Stations":
                    row_add = 1
                    new_row_idx = self.stations_row + row_add
                    # Wrap to top if rows exceeded
                    if new_row_idx > self.stations_table.rowCount() - 2:
                        self.stations_row = row_add
                    else:
                        self.stations_row = new_row_idx
                    self.stations_table_clicked(row=self.stations_row, col=1)

                # Increment row for all other tables
                else:
                    # Increment by 2 if method is mean to show only measured verticals
                    if self.meas.discharge_method == "Mean":
                        row_add = 2
                    else:
                        row_add = 1
                    new_row_idx = self.main_row_idx + row_add
                    # Wrap to top if rows exceeded
                    if new_row_idx > self.main_summary_table.rowCount() - 2:
                        self.main_row_idx = 1 + row_add
                    else:
                        self.main_row_idx = new_row_idx
                key_recognized = True

            # Up arrow
            elif e.key() == QtCore.Qt.Key_Up:
                # Increment row for stations tab, this tab has all verticals
                # whether used or not. It does not contain discharge
                if self.current_tab.strip() == "Stations":
                    row_add = -1
                    new_row_idx = self.stations_row + row_add
                    # Wrap to bottom if top of table reached
                    if new_row_idx < 2:
                        self.stations_row = self.stations_table.rowCount() - 1 + row_add
                    else:
                        self.stations_row = new_row_idx
                    self.stations_table_clicked(row=self.stations_row, col=1)

                # Increment row for all other tables
                else:
                    # Increment by 2 if method is mean to show only measured verticals
                    if self.meas.discharge_method == "Mean":
                        row_add = -2
                    else:
                        row_add = -1
                    new_row_idx = self.main_row_idx + row_add
                    # Wrap to bottom if top of table reached
                    if new_row_idx < 2:
                        self.main_row_idx = (
                            self.main_summary_table.rowCount() - 1 + row_add
                        )
                    else:
                        self.main_row_idx = new_row_idx
                key_recognized = True

            if key_recognized:
                if self.current_tab.strip() == "Main":
                    self.main_select_vertical(self.main_row_idx)
                elif self.current_tab.strip() == "Compass/P/R":
                    self.compass_table_clicked(row=self.main_row_idx - 1)
                elif self.current_tab.strip() == "Temp/Sal":
                    self.tempsal_table_clicked(row=self.main_row_idx - 1)
                elif self.current_tab.strip() == "BT":
                    self.bt_table_clicked(row=self.main_row_idx - 1)
                elif self.current_tab.strip() == "Depth":
                    self.depth_table_clicked(row=self.main_row_idx - 1)
                elif self.current_tab.strip() == "WT":
                    self.wt_table_clicked(row=self.main_row_idx - 1)
                elif self.current_tab.strip() == "Extrap":
                    self.extrap_verticals_table_clicked(row=self.main_row_idx - 1)

    def q_digits(self, q):
        if self.q_digits_method == "sigfig":
            return sfrnd(q, self.q_digits_digits)
        else:
            return np.round(q, self.q_digits_digits)
        
    def check_magnitude_method(self):
        
        magnitude_prompt = False
        for idx in self.meas.verticals_used_idx:
            if self.meas.verticals[idx].sampling_duration_sec > 0:
                if self.meas.verticals[idx].velocity_method == "Magnitude":
                    heading = np.unique(self.meas.verticals[idx].data.sensors.heading_deg.internal.data)
                    if len(heading) > 1:
                        magnitude_prompt = True
                        break
        
        if magnitude_prompt:
            text = self.tr("The magnitude method has been selected for one or more stations. Heading data are available from the compass. These heading data can be used in the computation of the velocity magnitude. If the heading data are reliable it is recommended to use the heading data. If the heading data may be affected by magnetic interference then it may be better not to use the heading data. Note: This can by changed on the Compass/P/R tab by setting the heading source. \n \n Continue to use the heading data from the compass in the computation of the magnitude velocity.")
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Question)
            msg.setText(self.tr("Magnitude Method"))
            msg.setInformativeText(text)
            msg.setWindowTitle(self.tr("Question"))
            msg.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
            msg.setDefaultButton(QtWidgets.QMessageBox.Yes)
            retval = msg.exec_()

            if retval == QtWidgets.QMessageBox.No:
                self.meas.change_heading_source("user", idx=None)
                        


def excepthook(exc_type, exc_value, exc_traceback):
    # possible parameters: exc_type, exc_value, exc_tb
    # tb = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
    msg = QtWidgets.QMessageBox()
    msg.setIcon(QtWidgets.QMessageBox.Critical)
    msg.setText("Error")
    text = "An error has occured. Please email dave@genesishydrotech.com describing what you were doing when the error occured. IMPORTANT: attach the measurement causing the error."
    msg.setInformativeText(text)
    msg.setWindowTitle("Error")
    msg.exec_()
    QtWidgets.QApplication.quit()


# sys.excepthook = excepthook

# Main
# ====
if __name__ == "__main__":
    # os.environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
    log = ErrorLog("QRevMS", "QRevMS")
    app = QtWidgets.QApplication(sys.argv)
    window = QRevMS()
    if window.agreement:
        window.show()
        sys.excepthook = log.custom_excepthook
        app.exec_()
    else:
        app.closeAllWindows()
