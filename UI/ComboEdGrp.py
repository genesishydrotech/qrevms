from PyQt5 import QtWidgets


class ComboEdGrp(object):
    """Provides the logic to control the behavior of a combobox that allows an option for
    manual inputthat can be applied to one or all data.

    Attributes
    ----------
    combo: QtWidget
        Combobox widget
    ed: QtWidget
        Line edit widget for manual input
    pb_apply: QtWidget
        Pushbutton widget to apply to selected data only.
    pb_apply_all: QtWidget
        Pushbutton widget to apply to all data.
    change: method
        Method in the parent used to make the specified change
    """

    def __init__(
        self, combo, ed, pb_apply, pb_apply_all, change, min_value=None, previous=None
    ):
        """Initializes the combobox-edit group variables.

        Parameters
        ----------
        combo: QtWidget
            Combobox widget
        ed: QtWidget
            Line edit widget for manual input
        pb_apply: QtWidget
            Pushbutton widget to apply to selected data only.
        pb_apply_all: QtWidget
            Pushbutton widget to apply to all data.
        change: method
            Method in the parent used to make the specified change
        min_value: float or int
            Minimum acceptable value
        reset: method
            Method to update values
        """
        self.combo = combo
        self.ed = ed
        self.pb_apply = pb_apply
        self.pb_apply_all = pb_apply_all
        self.change = change
        self.min_value = min_value
        self.previous = previous

    def enable_apply(self):
        """Enables the apply buttons when a choice is selected from the combobox.
        If the choice is Manual the apply buttons are not enabled until a valid manual
        value is entered.
        """
        self.pb_apply.setEnabled(True)
        self.pb_apply_all.setEnabled(True)
        self.ed.setEnabled(True)

    def apply(self):
        """Applies the change to the selected data only."""
        if self.check_valid():
            self.change(apply_all=False)
        elif self.previous is not None:
            self.previous()
        self.pb_apply.setEnabled(False)
        self.pb_apply_all.setEnabled(False)

    def apply_all(self):
        """Applies the change to all the data."""
        if self.check_valid():
            self.change(apply_all=True)
        elif self.previous is not None:
            self.previous()
        self.pb_apply.setEnabled(False)
        self.pb_apply_all.setEnabled(False)

    def check_valid(self):
        """Coordinates checking of numerical values when required for user input."""

        combo_list = ["Manual", "User", "Cells", "Dist.", "Coefficient", "Degree"]

        if self.combo.currentText() in combo_list:
            value = self.check_numeric_input(
                self.ed, block=False, min_value=self.min_value
            )
            if value is not None:
                return True
            else:
                return False
        else:
            self.ed.setEnabled(False)
            self.ed.setText("")
            return True

    @staticmethod
    def check_numeric_input(obj, block=True, min_value=None):
        """Check if input is a numeric object.

        Parameters
        ----------
        obj: QtWidget
            QtWidget user edit box.
        block: bool
            Block signals
        min_value: float
            Minimum allowable value

        Returns
        -------
        out: float
            obj converted to float if possible
        """
        if block:
            obj.blockSignals(True)
        out = None
        if len(obj.text()) > 0:
            # Try to convert obj to a float
            try:
                text = obj.text()
                out = float(text)
                if min_value is not None:
                    if out >= min_value:
                        return out
                    else:
                        obj.setText("")
                        msg = QtWidgets.QMessageBox()
                        msg.setIcon(QtWidgets.QMessageBox.Critical)
                        msg.setText("Error")
                        msg.setInformativeText(
                            "Value must be >= {}".format(str(min_value))
                        )
                        msg.setWindowTitle("Error")
                        msg.exec_()
                        return None
            # If conversion is not successful warn user that the input is invalid.
            except ValueError:
                obj.setText("")
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Critical)
                msg.setText("Error")
                msg.setInformativeText(
                    "You have entered non-numeric text. Enter a numeric value."
                )
                msg.setWindowTitle("Error")
                msg.exec_()
                return None

        if block:
            obj.blockSignals(False)
        return out
