# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'wInsertVertical.ui'
#
# Created by: PyQt5 UI code generator 5.15.7
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_ManualVertical(object):
    def setupUi(self, ManualVertical):
        ManualVertical.setObjectName("ManualVertical")
        ManualVertical.resize(327, 559)
        font = QtGui.QFont()
        font.setPointSize(10)
        ManualVertical.setFont(font)
        self.verticalLayout = QtWidgets.QVBoxLayout(ManualVertical)
        self.verticalLayout.setObjectName("verticalLayout")
        self.layout_top = QtWidgets.QVBoxLayout()
        self.layout_top.setObjectName("layout_top")
        self.layout_man_location = QtWidgets.QHBoxLayout()
        self.layout_man_location.setObjectName("layout_man_location")
        spacerItem = QtWidgets.QSpacerItem(
            75, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum
        )
        self.layout_man_location.addItem(spacerItem)
        self.txt_man_location = QtWidgets.QLabel(ManualVertical)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.txt_man_location.setFont(font)
        self.txt_man_location.setObjectName("txt_man_location")
        self.layout_man_location.addWidget(self.txt_man_location)
        self.ed_man_location = QtWidgets.QLineEdit(ManualVertical)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.ed_man_location.sizePolicy().hasHeightForWidth()
        )
        self.ed_man_location.setSizePolicy(sizePolicy)
        self.ed_man_location.setMinimumSize(QtCore.QSize(80, 0))
        self.ed_man_location.setMaximumSize(QtCore.QSize(80, 16777215))
        self.ed_man_location.setObjectName("ed_man_location")
        self.layout_man_location.addWidget(self.ed_man_location)
        spacerItem1 = QtWidgets.QSpacerItem(
            20, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum
        )
        self.layout_man_location.addItem(spacerItem1)
        self.layout_top.addLayout(self.layout_man_location)
        self.layout_man_depth = QtWidgets.QHBoxLayout()
        self.layout_man_depth.setObjectName("layout_man_depth")
        spacerItem2 = QtWidgets.QSpacerItem(
            40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum
        )
        self.layout_man_depth.addItem(spacerItem2)
        self.txt_man_depth = QtWidgets.QLabel(ManualVertical)
        self.txt_man_depth.setObjectName("txt_man_depth")
        self.layout_man_depth.addWidget(self.txt_man_depth)
        self.ed_man_depth = QtWidgets.QLineEdit(ManualVertical)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.ed_man_depth.sizePolicy().hasHeightForWidth())
        self.ed_man_depth.setSizePolicy(sizePolicy)
        self.ed_man_depth.setMinimumSize(QtCore.QSize(80, 0))
        self.ed_man_depth.setMaximumSize(QtCore.QSize(80, 16777215))
        self.ed_man_depth.setObjectName("ed_man_depth")
        self.layout_man_depth.addWidget(self.ed_man_depth)
        spacerItem3 = QtWidgets.QSpacerItem(
            20, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum
        )
        self.layout_man_depth.addItem(spacerItem3)
        self.layout_top.addLayout(self.layout_man_depth)
        self.layout_man_vel = QtWidgets.QHBoxLayout()
        self.layout_man_vel.setObjectName("layout_man_vel")
        spacerItem4 = QtWidgets.QSpacerItem(
            55, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum
        )
        self.layout_man_vel.addItem(spacerItem4)
        self.txt_man_velocity = QtWidgets.QLabel(ManualVertical)
        self.txt_man_velocity.setObjectName("txt_man_velocity")
        self.layout_man_vel.addWidget(self.txt_man_velocity)
        self.ed_man_velocity = QtWidgets.QLineEdit(ManualVertical)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.ed_man_velocity.sizePolicy().hasHeightForWidth()
        )
        self.ed_man_velocity.setSizePolicy(sizePolicy)
        self.ed_man_velocity.setMinimumSize(QtCore.QSize(80, 0))
        self.ed_man_velocity.setMaximumSize(QtCore.QSize(80, 16777215))
        self.ed_man_velocity.setObjectName("ed_man_velocity")
        self.layout_man_vel.addWidget(self.ed_man_velocity)
        spacerItem5 = QtWidgets.QSpacerItem(
            20, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum
        )
        self.layout_man_vel.addItem(spacerItem5)
        self.layout_top.addLayout(self.layout_man_vel)
        self.layout_correction = QtWidgets.QHBoxLayout()
        self.layout_correction.setObjectName("layout_correction")
        spacerItem6 = QtWidgets.QSpacerItem(
            55, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum
        )
        self.layout_correction.addItem(spacerItem6)
        self.txt_correction = QtWidgets.QLabel(ManualVertical)
        self.txt_correction.setObjectName("txt_correction")
        self.layout_correction.addWidget(self.txt_correction)
        self.ed_man_correction = QtWidgets.QLineEdit(ManualVertical)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.ed_man_correction.sizePolicy().hasHeightForWidth()
        )
        self.ed_man_correction.setSizePolicy(sizePolicy)
        self.ed_man_correction.setMinimumSize(QtCore.QSize(80, 0))
        self.ed_man_correction.setMaximumSize(QtCore.QSize(80, 16777215))
        self.ed_man_correction.setObjectName("ed_man_correction")
        self.layout_correction.addWidget(self.ed_man_correction)
        spacerItem7 = QtWidgets.QSpacerItem(
            20, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum
        )
        self.layout_correction.addItem(spacerItem7)
        self.layout_top.addLayout(self.layout_correction)
        self.layout_man_ws_condition = QtWidgets.QHBoxLayout()
        self.layout_man_ws_condition.setObjectName("layout_man_ws_condition")
        spacerItem8 = QtWidgets.QSpacerItem(
            40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum
        )
        self.layout_man_ws_condition.addItem(spacerItem8)
        self.txt_man_ws_condition = QtWidgets.QLabel(ManualVertical)
        self.txt_man_ws_condition.setObjectName("txt_man_ws_condition")
        self.layout_man_ws_condition.addWidget(self.txt_man_ws_condition)
        self.combo_man_ws_condition = QtWidgets.QComboBox(ManualVertical)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.combo_man_ws_condition.sizePolicy().hasHeightForWidth()
        )
        self.combo_man_ws_condition.setSizePolicy(sizePolicy)
        self.combo_man_ws_condition.setMinimumSize(QtCore.QSize(80, 0))
        self.combo_man_ws_condition.setObjectName("combo_man_ws_condition")
        self.combo_man_ws_condition.addItem("")
        self.combo_man_ws_condition.addItem("")
        self.combo_man_ws_condition.addItem("")
        self.layout_man_ws_condition.addWidget(self.combo_man_ws_condition)
        spacerItem9 = QtWidgets.QSpacerItem(
            20, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum
        )
        self.layout_man_ws_condition.addItem(spacerItem9)
        self.layout_top.addLayout(self.layout_man_ws_condition)
        self.layout_man_ice_thickness = QtWidgets.QHBoxLayout()
        self.layout_man_ice_thickness.setObjectName("layout_man_ice_thickness")
        spacerItem10 = QtWidgets.QSpacerItem(
            35, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum
        )
        self.layout_man_ice_thickness.addItem(spacerItem10)
        self.txt_man_ice_thickness = QtWidgets.QLabel(ManualVertical)
        self.txt_man_ice_thickness.setObjectName("txt_man_ice_thickness")
        self.layout_man_ice_thickness.addWidget(self.txt_man_ice_thickness)
        self.ed_man_ice_thickness = QtWidgets.QLineEdit(ManualVertical)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.ed_man_ice_thickness.sizePolicy().hasHeightForWidth()
        )
        self.ed_man_ice_thickness.setSizePolicy(sizePolicy)
        self.ed_man_ice_thickness.setMinimumSize(QtCore.QSize(80, 0))
        self.ed_man_ice_thickness.setMaximumSize(QtCore.QSize(80, 16777215))
        self.ed_man_ice_thickness.setObjectName("ed_man_ice_thickness")
        self.layout_man_ice_thickness.addWidget(self.ed_man_ice_thickness)
        spacerItem11 = QtWidgets.QSpacerItem(
            20, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum
        )
        self.layout_man_ice_thickness.addItem(spacerItem11)
        self.layout_top.addLayout(self.layout_man_ice_thickness)
        self.layout_man_ice_bottom = QtWidgets.QHBoxLayout()
        self.layout_man_ice_bottom.setObjectName("layout_man_ice_bottom")
        spacerItem12 = QtWidgets.QSpacerItem(
            32, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum
        )
        self.layout_man_ice_bottom.addItem(spacerItem12)
        self.txt_man_ice_bottom = QtWidgets.QLabel(ManualVertical)
        self.txt_man_ice_bottom.setObjectName("txt_man_ice_bottom")
        self.layout_man_ice_bottom.addWidget(self.txt_man_ice_bottom)
        self.ed_man_ice_bottom = QtWidgets.QLineEdit(ManualVertical)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.ed_man_ice_bottom.sizePolicy().hasHeightForWidth()
        )
        self.ed_man_ice_bottom.setSizePolicy(sizePolicy)
        self.ed_man_ice_bottom.setMinimumSize(QtCore.QSize(80, 0))
        self.ed_man_ice_bottom.setMaximumSize(QtCore.QSize(80, 16777215))
        self.ed_man_ice_bottom.setObjectName("ed_man_ice_bottom")
        self.layout_man_ice_bottom.addWidget(self.ed_man_ice_bottom)
        spacerItem13 = QtWidgets.QSpacerItem(
            20, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum
        )
        self.layout_man_ice_bottom.addItem(spacerItem13)
        self.layout_top.addLayout(self.layout_man_ice_bottom)
        self.layout_man_slush_bottom = QtWidgets.QHBoxLayout()
        self.layout_man_slush_bottom.setObjectName("layout_man_slush_bottom")
        spacerItem14 = QtWidgets.QSpacerItem(
            18, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum
        )
        self.layout_man_slush_bottom.addItem(spacerItem14)
        self.txt_man_slush_bottom = QtWidgets.QLabel(ManualVertical)
        self.txt_man_slush_bottom.setObjectName("txt_man_slush_bottom")
        self.layout_man_slush_bottom.addWidget(self.txt_man_slush_bottom)
        self.ed_man_slush_bottom = QtWidgets.QLineEdit(ManualVertical)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.ed_man_slush_bottom.sizePolicy().hasHeightForWidth()
        )
        self.ed_man_slush_bottom.setSizePolicy(sizePolicy)
        self.ed_man_slush_bottom.setMinimumSize(QtCore.QSize(80, 0))
        self.ed_man_slush_bottom.setMaximumSize(QtCore.QSize(80, 16777215))
        self.ed_man_slush_bottom.setObjectName("ed_man_slush_bottom")
        self.layout_man_slush_bottom.addWidget(self.ed_man_slush_bottom)
        spacerItem15 = QtWidgets.QSpacerItem(
            20, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum
        )
        self.layout_man_slush_bottom.addItem(spacerItem15)
        self.layout_top.addLayout(self.layout_man_slush_bottom)
        self.verticalLayout.addLayout(self.layout_top)
        self.gb_sta_comment = QtWidgets.QGroupBox(ManualVertical)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.gb_sta_comment.setFont(font)
        self.gb_sta_comment.setObjectName("gb_sta_comment")
        self.gridLayout = QtWidgets.QGridLayout(self.gb_sta_comment)
        self.gridLayout.setObjectName("gridLayout")
        self.ed_sta_comment = QtWidgets.QPlainTextEdit(self.gb_sta_comment)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.ed_sta_comment.setFont(font)
        self.ed_sta_comment.setObjectName("ed_sta_comment")
        self.gridLayout.addWidget(self.ed_sta_comment, 0, 0, 1, 1)
        self.verticalLayout.addWidget(self.gb_sta_comment)
        self.buttonBox = QtWidgets.QDialogButtonBox(ManualVertical)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(
            QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok
        )
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(ManualVertical)
        self.buttonBox.accepted.connect(ManualVertical.accept)  # type: ignore
        self.buttonBox.rejected.connect(ManualVertical.reject)  # type: ignore
        QtCore.QMetaObject.connectSlotsByName(ManualVertical)

    def retranslateUi(self, ManualVertical):
        _translate = QtCore.QCoreApplication.translate
        ManualVertical.setWindowTitle(_translate("ManualVertical", "Manual Vertical"))
        self.txt_man_location.setText(_translate("ManualVertical", "Location (m):"))
        self.txt_man_depth.setText(_translate("ManualVertical", "Effective Depth (m):"))
        self.txt_man_velocity.setText(_translate("ManualVertical", "Mean Velocity:"))
        self.txt_correction.setText(
            _translate("ManualVertical", "Velocity Correction:")
        )
        self.txt_man_ws_condition.setText(
            _translate("ManualVertical", " Water surface condition:")
        )
        self.combo_man_ws_condition.setItemText(0, _translate("ManualVertical", "Open"))
        self.combo_man_ws_condition.setItemText(1, _translate("ManualVertical", "Ice"))
        self.combo_man_ws_condition.setItemText(
            2, _translate("ManualVertical", "Island")
        )
        self.txt_man_ice_thickness.setText(
            _translate("ManualVertical", "Ice thickness (m):")
        )
        self.txt_man_ice_bottom.setText(
            _translate("ManualVertical", "WS to Ice Bottom:")
        )
        self.txt_man_slush_bottom.setText(
            _translate("ManualVertical", "WS to Slush Bottom:")
        )
        self.gb_sta_comment.setTitle(_translate("ManualVertical", "Comment (Required)"))


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    ManualVertical = QtWidgets.QDialog()
    ui = Ui_ManualVertical()
    ui.setupUi(ManualVertical)
    ManualVertical.show()
    sys.exit(app.exec_())
