from PyQt5 import QtWidgets
from UI import wInsertVertical
import numpy as np


class InsertVertical(QtWidgets.QDialog, wInsertVertical.Ui_ManualVertical):
    """Dialog to allow users to insert a manual vertical.

    Parameters
    ----------
    wInsertVertical.Ui_InsertVertical : QDialog
        Dialog window with options for users
    """

    def __init__(
        self,
        units,
        loc_min_max,
        all_locations,
        ws_condition,
        vertical=None,
        parent=None,
    ):
        """Initialize dialog.

        Parameters
        ----------
        units: dict
            Dictionary of units conversions and labels
        loc_min_max: list
            Minimum and maximum allowed locations based on start and end
        all_locations: list
            List of all locations used to ensure inserted location is unique and
            not a duplicate
        ws_condition: str
            Water surface condition (Open or Ice)
        """
        super(InsertVertical, self).__init__(parent)
        self.setupUi(self)

        self.vertical = vertical
        self.loc_min_max = loc_min_max
        self.all_locations = all_locations
        self.units = units

        # Disable OK until required data have been entered
        self.buttonBox.button(QtWidgets.QDialogButtonBox.Ok).setEnabled(False)

        # Change labels based on units
        self.txt_man_location.setText(self.tr("Location ") + units["label_L"])
        self.txt_man_depth.setText(self.tr("Effective Depth ") + units["label_L"])
        self.txt_man_velocity.setText(self.tr("Mean Velocity ") + units["label_V"])
        self.txt_man_ice_thickness.setText(self.tr("Ice Thickness ") + units["label_L"])
        self.txt_man_ice_bottom.setText(self.tr("WS to Ice Bottom ") + units["label_L"])
        self.txt_man_slush_bottom.setText(
            self.tr("WS to Slush Bottom ") + units["label_L"]
        )

        # Setup connections
        self.ed_man_location.editingFinished.connect(self.location_data)
        self.ed_man_depth.editingFinished.connect(self.depth_data)
        self.ed_man_velocity.editingFinished.connect(self.vel_data)
        self.ed_man_correction.editingFinished.connect(self.correction_data)
        self.ed_man_ice_thickness.editingFinished.connect(self.ice_thickness_check)
        self.ed_man_ice_bottom.editingFinished.connect(self.ice_bottom_check)
        self.ed_man_slush_bottom.editingFinished.connect(self.slush_bottom_check)
        self.combo_man_ws_condition.activated.connect(self.ws_condition_changed)
        self.ed_sta_comment.textChanged.connect(self.comment)

        # Initial water surface condition
        if ws_condition == "Ice":
            self.combo_man_ws_condition.setCurrentIndex(1)
            self.txt_man_ice_thickness.show()
            self.txt_man_ice_bottom.show()
            self.txt_man_slush_bottom.show()
            self.ed_man_ice_thickness.show()
            self.ed_man_ice_bottom.show()
            self.ed_man_slush_bottom.show()
        else:
            self.combo_man_ws_condition.setCurrentIndex(0)
            self.txt_man_ice_thickness.hide()
            self.txt_man_ice_bottom.hide()
            self.txt_man_slush_bottom.hide()
            self.ed_man_ice_thickness.hide()
            self.ed_man_ice_bottom.hide()
            self.ed_man_slush_bottom.hide()

        # Initialize required data boolean list
        self.required_data = [False, False, False, False]

        if vertical is not None:
            self.ed_man_location.setText(
                "{:.2f}".format(vertical.location_m * units["L"])
            )
            self.ed_man_depth.setText("{:.3f}".format(vertical.depth_m * units["L"]))
            if np.isnan(vertical.mean_speed_mps):
                speed = ""
            else:
                speed = "{:.3f}".format(vertical.mean_speed_mps * units["V"])
            self.ed_man_velocity.setText(speed)
            self.ed_man_correction.setText(
                "{:.2f}".format(vertical.velocity_correction)
            )
            self.ed_man_ice_thickness.setText(
                "{:.3f}".format(vertical.ice_thickness_m * units["L"])
            )
            self.ed_man_ice_bottom.setText(
                "{:.3f}".format(vertical.ws_to_ice_bottom_m * units["L"])
            )
            self.ed_man_slush_bottom.setText(
                "{:.3f}".format(vertical.ws_to_slush_bottom_m * units["L"])
            )
            self.combo_man_ws_condition.setCurrentText(vertical.ws_condition)
            if np.round(vertical.velocity_correction, 2) != 0:
                self.ed_man_velocity.setEnabled(False)
                self.ed_man_correction.setEnabled(True)
            elif len(speed) > 0:
                self.ed_man_velocity.setEnabled(True)
                self.ed_man_correction.setEnabled(False)
            else:
                self.ed_man_velocity.setEnabled(True)
                self.ed_man_correction.setEnabled(True)

            self.required_data = [True, True, True, False]
            if vertical is not None:
                self.setFocus()

    def location_data(self):
        """Get user input for location"""

        self.ed_man_location.blockSignals(True)
        # Get data
        data = self.check_numeric_input(self.ed_man_location)

        # Initial check for validity
        if data is not None:
            self.required_data[0] = True
            if self.vertical is None or (
                np.round(data, 2) != np.round(self.vertical.location_m, 2)
            ):
                self.check_location()
        else:
            self.required_data[0] = False
            self.ed_man_location.setText("")

        # Check to see if all required data for the vertical has been entered
        self.check_required_data()


        self.ed_man_location.blockSignals(False)

    def check_location(self):
        """Secondary check of location data to ensure that it is between start and
        end points and is unique.
        """

        # Get data
        data = self.check_numeric_input(self.ed_man_location)

        # Ensure data is between start and end points or present message
        if self.loc_min_max[0] < data < self.loc_min_max[1]:
            self.required_data[0] = True
        else:
            self.popup_message(
                "New location must be between start ({:6.2f}) and end ({:6.2f}) ".format(
                    self.loc_min_max[0], self.loc_min_max[1]
                )
            )
            self.required_data[0] = False
            self.ed_man_location.blockSignals(True)

            try:
                if self.vertical.location_m is not None:
                    self.ed_man_location.setText(
                        "{:.2f}".format(self.vertical.location_m * self.units["L"])
                    )
            except AttributeError:
                self.ed_man_location.setText("")

            self.ed_man_location.blockSignals(False)
        if data in self.all_locations:
            self.popup_message(
                "New location duplicates an existing location. Enter a different location."
            )

            self.required_data[0] = False
            self.ed_man_location.blockSignals(True)
            self.ed_man_location.setText("")
            self.ed_man_location.blockSignals(False)

    def depth_data(self):
        """Get user input for depth"""

        # Get data
        data = self.check_numeric_input(self.ed_man_depth, min_value=0)

        # Check that data meets required status
        if data is not None:
            self.required_data[1] = True
        else:
            self.required_data[1] = False
            self.ed_man_depth.setText("")

        # Check to see if all required data for the vertical has been entered
        self.check_required_data()

    def vel_data(self):
        """Get user input for velocity"""

        # Get data
        data = self.check_numeric_input(self.ed_man_velocity)

        # Check that data meets required status
        if data is not None:
            self.required_data[2] = True
            self.ed_man_correction.setEnabled(False)
        else:
            self.required_data[2] = False
            self.ed_man_velocity.setText("")
            self.ed_man_correction.setEnabled(True)

        # Check to see if all required data for the vertical has been entered
        self.check_required_data()

    def correction_data(self):
        """Get user input for velocity correction"""

        # Get data
        data = self.check_numeric_input(self.ed_man_correction)
        self.ed_man_velocity.setText("")

        # Check that data meets required status
        if data is not None:
            self.required_data[2] = True
            self.ed_man_velocity.setEnabled(False)
        else:
            self.required_data[2] = False
            self.ed_man_correction.setText("")
            self.ed_man_velocity.setEnabled(True)

        # Check to see if all required data for the vertical has been entered
        self.check_required_data()

    def ice_thickness_check(self):
        """Check user input for ice thickness"""

        # Get data
        _ = self.check_numeric_input(self.ed_man_ice_thickness)

    def ice_bottom_check(self):
        """Check user input for ws to ice bottom"""

        # Get data
        _ = self.check_numeric_input(self.ed_man_ice_bottom)

    def slush_bottom_check(self):
        """Check user input for ws to slush bottom"""

        # Get data
        _ = self.check_numeric_input(self.ed_man_slush_bottom)

    def ws_condition_changed(self):
        """Respond to user change in ws condition"""

        if self.combo_man_ws_condition.currentText() == "Ice":
            self.txt_man_ice_thickness.show()
            self.txt_man_ice_bottom.show()
            self.txt_man_slush_bottom.show()
            self.ed_man_ice_thickness.show()
            self.ed_man_ice_bottom.show()
            self.ed_man_slush_bottom.show()
        else:
            self.txt_man_ice_thickness.hide()
            self.txt_man_ice_bottom.hide()
            self.txt_man_slush_bottom.hide()
            self.ed_man_ice_thickness.hide()
            self.ed_man_ice_bottom.hide()
            self.ed_man_slush_bottom.hide()

    def comment(self):
        """Check that a comment has been entered and toggle required data to True.0"""

        if len(self.ed_sta_comment.toPlainText()) > 0:
            self.required_data[3] = True
        else:
            self.required_data[3] = False

        # Check to see if all required data for the vertical has been entered
        self.check_required_data()

    def check_required_data(self):
        """Checks that valid location, depth, and velocity have been entered
        before enabling the OK button.
        """

        self.buttonBox.button(QtWidgets.QDialogButtonBox.Ok).setEnabled(False)
        if sum(self.required_data) == 4:
            self.buttonBox.button(QtWidgets.QDialogButtonBox.Ok).setEnabled(True)

    @staticmethod
    def check_numeric_input(obj, min_value=None):
        """Check if input is a numeric object.

        Parameters
        ----------
        obj: QtWidget
            QtWidget user edit box.

        Returns
        -------
        out: float
            obj converted to float if possible
        """
        out = None
        if len(obj.text()) > 0:
            # Try to convert obj to a float
            try:
                text = obj.text()
                out = float(text)
                if min_value is not None:
                    if out >= min_value:
                        return out
                    else:
                        obj.setText("")
                        msg = QtWidgets.QMessageBox()
                        msg.setIcon(QtWidgets.QMessageBox.Critical)
                        msg.setText("Error")
                        msg.setInformativeText(
                            "Value must be >= {:0.2f}".format(min_value)
                        )
                        msg.setWindowTitle("Error")
                        msg.exec_()
                        return None
            # If conversion is not successful warn user that the input is invalid.
            except ValueError:
                obj.setText("")
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Critical)
                msg.setText("Error")
                msg.setInformativeText(
                    "You have entered non-numeric text. Enter a numeric value."
                )
                msg.setWindowTitle("Error")
                msg.exec_()
        return out

    @staticmethod
    def popup_message(text):
        """Display a message box with messages specified in text.

        Parameters
        ----------
        text: str
            Message to be displayed.
        """

        QtWidgets.QApplication.restoreOverrideCursor()
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        msg.setText("Error")
        msg.setInformativeText(text)
        msg.setWindowTitle("Error")
        msg.exec_()
        return
