from PyQt5 import QtWidgets
from UI import wQMethod


class QMethod(QtWidgets.QDialog, wQMethod.Ui_Dialog):
    """Dialog to allow users to set discharge method."""

    def __init__(self, parent=None):
        super(QMethod, self).__init__(parent)
        self.setupUi(self)
        self.pb_ok.clicked.connect(self.accept)
        self.pb_cancel.clicked.connect(self.reject)
