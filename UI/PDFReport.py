from datetime import datetime
import numpy as np
import getpass
import io
from matplotlib.figure import Figure

from UI.MplCanvas import MplCanvas
from UI.Graphics import Graphics
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch
from reportlab.pdfgen import canvas
from reportlab.platypus import (
    SimpleDocTemplate,
    Paragraph,
    Spacer,
    Image,
    PageBreak,
    Table,
    TableStyle,
)
from Modules.local_time_utilities import tz_formatted_string


class Report:
    """Generates a PDF summary report."""

    def __init__(self, pdf_fullname, parent):
        """Constructs report doc.

        Parameters
        ----------
        pdf_fullname: str
            Filename of pdf file including path.
        parent: QRevIntMS
            Main object,
        """

        self.parent = parent
        self.doc = SimpleDocTemplate(
            pdf_fullname,
            pagesize=letter,
            rightMargin=36,
            leftMargin=36,
            topMargin=90,
            bottomMargin=36,
        )
        self.elements = []
        self.styles = getSampleStyleSheet()
        self.width, self.height = letter

    def header(self, page, doc):
        """Create header for each page of report.

        Parameters
        ----------
        page: Canvas
            Object of Canvas
        doc: SimpleDocTemplate
            Object of SimpleDocTemplate, required
        """

        # Logo
        logo = Image("QRevMS.ico", width=30, height=30)
        logo.wrapOn(page, self.width, self.height)
        logo.drawOn(page, 0.6 * inch, self.height - 0.65 * inch)

        # Title
        title = "<font size=14><b>QRevMS Discharge Measurement Report</b></font>"
        p = Paragraph(title, self.styles["Normal"])
        p.wrapOn(page, self.width, self.height)
        p.drawOn(page, 1.1 * inch, self.height - 0.5 * inch)

        # Line 2
        y = 0.85

        # Station Name
        ptext = "<font size = 10> Station Name: <b>{}</b></font>".format(
            self.parent.meas.station_name
        )
        p = Paragraph(ptext, self.styles["Normal"])
        p.wrapOn(page, self.width, self.height)
        p.drawOn(page, 0.6 * inch, self.height - y * inch)

        # Line 3
        y = 1.05

        # Station Number
        ptext = "<font size = 10> Station Number:  {}</font>".format(
            self.parent.meas.station_number
        )
        p = Paragraph(ptext, self.styles["Normal"])
        p.wrapOn(page, self.width, self.height)
        p.drawOn(page, 0.6 * inch, self.height - y * inch)

        # Measurement Date
        start_time_list = []
        for idx in self.parent.meas.verticals_used_idx:
            if self.parent.meas.verticals[idx].data.date_time is not None:
                start_time_list.append(
                    self.parent.meas.verticals[idx].data.date_time.start_serial_time
                )
        date = datetime.utcfromtimestamp(np.nanmin(start_time_list)).strftime(
            self.parent.date_format
        )

        ptext = "<font size = 10> Measurement Date: {}</font>".format(date)
        p = Paragraph(ptext, self.styles["Normal"])
        p.wrapOn(page, self.width, self.height)
        p.drawOn(page, 2.8 * inch, self.height - y * inch)

        # Measurement Number
        ptext = "<font size = 10> Measurement Number: {}</font>".format(
            self.parent.meas.meas_number
        )
        p = Paragraph(ptext, self.styles["Normal"])
        p.wrapOn(page, self.width, self.height)
        p.drawOn(page, 5.2 * inch, self.height - y * inch)

        # Line 4
        y = 1.25

        # Discharge
        ptext = "<font size = 10> Discharge: {:8} </font>".format(
            self.parent.q_digits(
                self.parent.meas.summary["Q"]["Total"] * self.parent.units["Q"]
            )
        )
        p = Paragraph(ptext, self.styles["Normal"])
        p.wrapOn(page, self.width, self.height)
        p.drawOn(page, 0.6 * inch, self.height - y * inch)

        # Stage
        if np.isnan(self.parent.meas.stage_meas_m):
            stage_mean = ""
        else:
            stage_mean = "{:.2f}".format(
                self.parent.meas.stage_meas_m * self.parent.units["L"]
            )
        ptext = "<font size = 10> Stage: {} </font>".format(stage_mean)
        p = Paragraph(ptext, self.styles["Normal"])
        p.wrapOn(page, self.width, self.height)
        p.drawOn(page, 2.8 * inch, self.height - y * inch)

        # User Rating
        ptext = "<font size = 10>User Rating: {} </font>".format(
            self.parent.meas.user_rating
        )
        p = Paragraph(ptext, self.styles["Normal"])
        p.wrapOn(page, self.width, self.height)
        p.drawOn(page, 3.7 * inch, self.height - y * inch)

        # Processed
        ptext = "<font size = 10> Processed: {}</font>".format(
            datetime.now().strftime(self.parent.date_format + " %H:%M:%S")
        )
        p = Paragraph(ptext, self.styles["Normal"])
        p.wrapOn(page, self.width, self.height)
        p.drawOn(page, 5.2 * inch, self.height - y * inch)

    def data_version(self):
        """Creates a paragraph with the data file name.

        Returns
        -------
        p: Paragraph
            Object of Paragraph
        """

        file = "Data File: {}".format(self.parent.meas.filename)

        user = "Processed by: {}".format(getpass.getuser())

        version = self.parent.QRevMS_version.split(" ")
        version_txt = "QRevMS Version: {}".format(version[1])

        data = [[file, user, version_txt]]

        tbl = Table(data, colWidths=None, rowHeights=None)
        tbl.setStyle([("LEFTPADDING", (0, 0), (0, 0), 0)])
        tbl.hAlign = "LEFT"

        return tbl

    def top_table(self):
        """Create the top table consisting of 3 columns

        Returns
        -------
        table: Table
            Object of Table
        """

        # Build table data list
        data = [
            [
                " Discharge Summary",
                " Measurement Information",
                " Uncertainty and Rating",
            ],
            [
                self.discharge_summary(),
                self.measurement_information(),
                self.uncertainty(),
            ],
        ]

        # Create and style table
        table = Table(data, colWidths=None, rowHeights=None)
        table.setStyle(
            [
                ("ALIGN", (0, 0), (-1, -1), "LEFT"),
                ("FONT", (0, 0), (-1, 0), "Helvetica-Bold", 10),
                ("VAlIGN", (0, 1), (-1, 1), "TOP"),
                ("GRID", (0, 0), (-1, -1), 1, colors.black),
                ("LEFTPADDING", (0, 0), (-1, -1), 1),
                ("RIGHTPADDING", (0, 0), (-1, -1), 1),
            ]
        )
        return table

    def discharge_summary(self):
        """Create discharge summary table.

        Returns
        -------
        q_summary_table: Table
            Object of Table
        """

        summary_df = self.parent.meas.summary

        # Data prep
        discharge = "{}".format(
            self.parent.q_digits(
                self.parent.meas.summary["Q"]["Total"] * self.parent.units["Q"]
            )
        )

        if np.isnan(self.parent.meas.stage_start_m):
            stage_start = ""
        else:
            stage_start = "{:.2f}".format(
                self.parent.meas.stage_start_m * self.parent.units["L"]
            )

        if np.isnan(self.parent.meas.stage_end_m):
            stage_end = ""
        else:
            stage_end = "{:.2f}".format(
                self.parent.meas.stage_end_m * self.parent.units["L"]
            )

        if np.isnan(self.parent.meas.stage_meas_m):
            stage_mean = ""
        else:
            stage_mean = "{:.2f}".format(
                self.parent.meas.stage_meas_m * self.parent.units["L"]
            )

        mean_velocity = "{:.3f}".format(
            summary_df["Normal Velocity"]["Mean"] * self.parent.units["V"]
        )
        max_velocity = "{:.3f}".format(
            summary_df["Normal Velocity"].max() * self.parent.units["V"]
        )
        mean_depth = "{:.3f}".format(
            summary_df["Depth"]["Mean"] * self.parent.units["L"]
        )
        max_depth = "{:.3f}".format(summary_df["Depth"].max() * self.parent.units["L"])
        area = "{:.3f}".format(summary_df["Area"]["Total"] * self.parent.units["A"])
        width = "{:.3f}".format(summary_df["Width"]["Total"] * self.parent.units["L"])
        start_time_list = []
        end_time_list = []
        for idx in self.parent.meas.verticals_used_idx:
            if self.parent.meas.verticals[idx].data.date_time is not None:
                start_time_list.append(
                    self.parent.meas.verticals[idx].data.date_time.start_serial_time
                )
                end_time_list.append(
                    self.parent.meas.verticals[idx].data.date_time.end_serial_time
                )
                utc_time_offset = self.parent.meas.verticals[idx].data.date_time.utc_time_offset

        start_time = tz_formatted_string(
            serial_time=np.nanmin(start_time_list),
            utc_time_offset=utc_time_offset,
            format="%H:%M:%S"
        )
        end_time = tz_formatted_string(np.nanmax(end_time_list),
                                       utc_time_offset=utc_time_offset,
            format="%H:%M:%S"
        )
        duration = "{:.1f}".format(summary_df["Duration"]["Total"])
        n_stations = summary_df.shape[0] - 2

        # Build table data
        data = [
            ["Discharge {}:".format(self.parent.units["label_Q"]), discharge],
            ["Stage Start:", stage_start],
            ["Stage End:", stage_end],
            ["Mean Stage:", stage_mean],
            ["Mean Velocity {}:".format(self.parent.units["label_V"]), mean_velocity],
            ["Max. Velocity {}:".format(self.parent.units["label_V"]), max_velocity],
            ["Mean Depth {}:".format(self.parent.units["label_L"]), mean_depth],
            ["Max. Depth {}:".format(self.parent.units["label_L"]), max_depth],
            ["Area {}:".format(self.parent.units["label_A"]), area],
            ["Width {}:".format(self.parent.units["label_L"]), width],
            ["Time Zone: ", self.parent.meas.time_zone],
            ["Start Time:", start_time],
            ["End Time:", end_time],
            ["Total Duration (s):", duration],
            ["Number Stations:", n_stations],
            
        ]

        # Create and style table
        q_summary_table = Table(data, colWidths=None, rowHeights=None)
        q_summary_table.setStyle(
            [
                ("ALIGN", (0, 0), (-1, -1), "LEFT"),
                ("LEFTPADDING", (0, 0), (-1, -1), 2),
                ("RIGHTPADDING", (0, 0), (-1, -1), 2),
            ]
        )
        return q_summary_table

    def measurement_information(self):
        """Creates the measurement information table.

        Returns
        -------
        meas_info_table: Table
            Object of Table
        """

        meas = self.parent.meas

        # Data prep
        user_temp = meas.temp_chk["user"]
        if np.isnan(user_temp):
            user_temp = ""
        else:
            user_temp = "{:.1f}".format(user_temp)

        adcp_temp = meas.temp_chk["adcp"]
        if np.isnan(adcp_temp):
            adcp_temp = ""

        if len(meas.system_tst) == 0:
            system_test = "None"
        elif meas.system_tst[-1].result["sysTest"]["n_failed"]:
            system_test = "Failed"
        else:
            system_test = "Passed"

        # Compass cal / eval
        if meas.verticals[self.parent.first_idx].data.inst.manufacturer == "SonTek":
            evals = meas.compass_cal
        else:
            evals = meas.compass_eval

        if len(meas.compass_cal) == 0 and len(meas.compass_eval) == 0:
            compass = "None"
        elif len(evals) > 0:
            if type(evals[-1].result["compass"]["error"]) == str:
                compass = evals[-1].result["compass"]["error"]
            else:
                compass = "{:2.2f}".format(evals[-1].result["compass"]["error"])
        else:
            compass = "Cal"

        # Combine data for nav_ref and vel_method
        nav_ref = []
        vel_method = []
        adcp_temperature = np.array([])
        for idx in meas.verticals_used_idx:
            nav_ref.append(meas.verticals[idx].velocity_reference)
            vel_method.append(meas.verticals[idx].velocity_method)
            if meas.verticals[idx].data.sensors is not None:
                adcp_temperature = np.append(adcp_temperature, meas.verticals[idx].data.sensors.temperature_deg_c.internal.data)

        # Average ADCP temperature from instrument if not suppied by user
        if type(adcp_temp) is not float:
            adcp_temp = np.nanmean(adcp_temperature)

        # Velocity or navigation reference
        ref = np.unique(nav_ref[1:-1])
        if len(ref) > 1:
            ref = self.parent.tr("Mixed")
        elif ref[0] == "None":
            ref = self.parent.tr("None")
        elif ref[0] == "BT":
            ref = self.parent.tr("BT")
        elif ref[0] == "bt_vel":
            ref = self.parent.tr("BT")

        # Velocity Method
        method = np.unique(vel_method)
        if len(method) > 1:
            method = "Mixed"
        elif method == "Fixed":
            method = self.parent.tr("Fixed")
        elif method == "Azimuth":
            method = self.parent.tr("Azimuth")
        else:
            method = self.parent.tr("Magnitude")

        # Build table data
        data = [
            ["Field Crew:", meas.persons],
            ["ADCP Model:", meas.verticals[self.parent.first_idx].data.inst.model],
            [
                "Serial Number:",
                meas.verticals[self.parent.first_idx].data.inst.serial_num,
            ],
            ["Firmware:", meas.verticals[self.parent.first_idx].data.inst.firmware],
            [
                "Frequency (kHz):",
                meas.verticals[self.parent.first_idx].data.inst.frequency_khz,
            ],
            ["W. Temp. (C):", user_temp],
            ["W. Temp. ADCP (C):", "{:.1f}".format(adcp_temp)],
            ["System Test:", system_test],
            ["Compass Cal/Eval:", compass],
            [
                "Magnetic Variaton:",
                meas.verticals[
                    self.parent.first_idx
                ].data.sensors.heading_deg.internal.mag_var_deg,
            ],
            ["Velocity Method:", method],
            ["Azimuth: (deg)", "{:.1f}".format(meas.tagline_azimuth_deg)],
            ["Velocity Reference", ref],
            ["Discharge Method", self.parent.meas.discharge_method],
            ["", ""],
        ]

        # Create and style table
        meas_info_table = Table(data, colWidths=None, rowHeights=None)
        meas_info_table.setStyle(
            [
                ("ALIGN", (0, 0), (-1, -1), "LEFT"),
                ("LEFTPADDING", (0, 0), (-1, -1), 2),
                ("RIGHTPADDING", (0, 0), (-1, -1), 2),
            ]
        )

        return meas_info_table

    def uncertainty(self):
        """Creates uncertainty table.

        Returns
        -------
        uncertainty_table: Table
            Object of Table
        """

        u = self.parent.meas.uncertainty

        # Build table data
        data = [
            [
                self.parent.tr("Component"),
                self.parent.tr("ISO"),
                self.parent.tr("IVE"),
                self.parent.tr("User"),
            ],
            [
                self.parent.tr("Systematic"),
                self.is_nan(u.u_iso["u_s"]),
                self.is_nan(u.u_ive["u_s"]),
                self.is_nan(u.u_user["u_s"]),
            ],
            [
                self.parent.tr("# Stations"),
                self.is_nan(u.u_iso["u_m"]),
                "",
                self.is_nan(u.u_user["u_m"]),
            ],
            [
                self.parent.tr("# Cells"),
                self.is_nan(u.u_iso["u_p"]),
                "",
                self.is_nan(u.u_user["u_p"]),
            ],
            [
                self.parent.tr("Width"),
                self.is_nan(u.u_iso["u_b"]),
                self.is_nan(u.u_ive["u_b"]),
                self.is_nan(u.u_user["u_b"]),
            ],
            [
                self.parent.tr("Depth"),
                self.is_nan(u.u_iso["u_d"]),
                self.is_nan(u.u_ive["u_d"]),
                self.is_nan(u.u_user["u_d"]),
            ],
            [
                self.parent.tr("Velocity"),
                self.is_nan(u.u_iso["u_v"]),
                self.is_nan(u.u_ive["u_v"]),
                self.is_nan(u.u_user["u_v"]),
            ],
            [
                self.parent.tr("Instr. Repeat."),
                self.is_nan(u.u_iso["u_c"]),
                "",
                self.is_nan(u.u_user["u_c"]),
            ],
            [
                self.parent.tr("Total Uncertainty"),
                self.is_nan(u.u_iso["u_q"]),
                self.is_nan(u.u_ive["u_q"]),
                self.is_nan(u.u_user["u_q"]),
            ],
            [
                self.parent.tr("95% Uncertainty"),
                self.is_nan(u.u_iso["u95_q"]),
                self.is_nan(u.u_ive["u95_q"]),
                self.is_nan(u.u_user["u95_q"]),
            ],
            ["", "", "", ""],
            ["User Rating: {}".format(self.parent.meas.user_rating), "", "", ""],
            ["", "", "", ""],
            ["", "", "", ""],
            ["", "", "", ""],
        ]

        # Create style list
        style_list = [
            ("ALIGN", (0, 0), (-1, -1), "LEFT"),
            ("FONT", (0, 0), (-1, 0), "Helvetica-Bold", 10),
            ("BACKGROUND", (2, 2), (2, 3), colors.darkgray),
            ("BACKGROUND", (2, 7), (2, 7), colors.darkgray),
            ("SPAN", (0, 14), (-1, 14)),
            ("LEFTPADDING", (0, 0), (-1, -1), 4),
            ("RIGHTPADDING", (0, 0), (-1, -1), 4),
            ("GRID", (0, 0), (-1, 9), 0.5, colors.black),
        ]

        # Conditional styling
        if u.method == "ISO":
            style_list.append(("FONT", (1, 1), (1, 9), "Helvetica-Bold", 10))
        elif u.method == "IVE":
            style_list.append(("FONT", (2, 1), (2, 9), "Helvetica-Bold", 10))
            style_list.append(("BACKGROUND", (3, 2), (3, 3), colors.darkgray))
            style_list.append(("BACKGROUND", (3, 7), (3, 7), colors.darkgray))
        else:
            style_list.append(("FONT", (3, 1), (3, 9), "Helvetica-Bold", 10))

        # Create and style table
        uncertainty_table = Table(data, colWidths=None, rowHeights=None)
        uncertainty_table.setStyle(style_list)

        return uncertainty_table

    @staticmethod
    def is_nan(data_in):
        """Format data while checking for nan

        Returns
        -------
        : str
        """

        if np.isnan(data_in):
            return ""

        return f"{data_in * 100:.2f}"

    def qa_messages(self):
        """Create table for QA messages.

        Returns
        -------
        messages_table: Table
            Object of Table
        """
        data = [["Automated QA Messages:"]]

        qa_check_keys = ["bt_vel", "compass", "depths", "edges", "extrapolation",
                         "stations", "verticals", "system_tst", "temperature", "user",
                         "w_vel", "main", "rating"]
        messages = self.parent.combine_selected_qa_messages(qa_check_keys)

        # Create each message as a list appended to data
        if len(messages) > 0:
            warning = []
            caution = []
            for line in messages:
                if line[1] == 1:
                    warning.append(line[0])
                else:
                    caution.append(line[0])

            data.append([Paragraph("<u> WARNINGS</u>")])
            style = ParagraphStyle(
                name="Normal", bulletIndent=20, leftIndent=35, bulletFontSize=14
            )
            for line in warning:
                data.append([Paragraph("<bullet>&bull;</bullet>" + line, style=style)])

            data.append([Paragraph("")])
            data.append([Paragraph("<u> Cautions</u>")])
            for line in caution:
                data.append([Paragraph("<bullet>&bull;</bullet>" + line, style=style)])

        # Create and style table
        messages_table = Table(
            data, colWidths=7.3 * inch, rowHeights=None, repeatRows=1
        )
        messages_table.setStyle([("FONT", (0, 0), (0, 0), "Helvetica-Bold", 10)])

        return messages_table

    def comments(self):
        """Create comments table.

        Returns
        -------
        comments_table: Table
            Object of Table
        """

        data = [["Comments:"]]

        # Create each comment as a list appended to data
        if len(self.parent.meas.comments) > 0:
            for line in self.parent.meas.comments:
                data.append([Paragraph("<bullet>&bull;</bullet>" + line)])
        else:
            data.append("")

        # Create and style table
        comments_table = Table(
            data, colWidths=7.3 * inch, rowHeights=None, repeatRows=1
        )
        comments_table.setStyle([("FONT", (0, 0), (0, 0), "Helvetica-Bold", 10)])

        return comments_table

    def summary_table_1(self):
        """Create 1st summary table containing discharge by station.

        Returns
        -------
        table_1: Table
            Object of Table
        """

        # Column labels
        data = [
            [
                self.label("Sta. Num.", bold=True),
                self.label("Loc. {}".format(self.parent.units["label_L"]), bold=True),
                self.label("Dur.<br/>(s)", bold=True),
                self.label("Valid Ens.", bold=True),
                self.label("Valid Cells", bold=True),
                self.label("Depth {}".format(self.parent.units["label_L"]), bold=True),
                self.label("Width {}".format(self.parent.units["label_L"]), bold=True),
                self.label("Area {}".format(self.parent.units["label_A"]), bold=True),
                self.label(
                    "Velocity {}".format(self.parent.units["label_V"]), bold=True
                ),
                self.label("Q {}".format(self.parent.units["label_Q"]), bold=True),
                self.label("Q<br/>(%)", bold=True),
            ]
        ]

        # Data for table extracted from summary dataframe
        summary_df = self.parent.meas.summary
        nrows = summary_df.shape[0] - 2
        caution = []
        warning = []
        if nrows > 0:
            idx_list = summary_df.index.values.tolist()
            for row in idx_list:
                if type(row) is int:
                    # Station Number
                    if np.isnan(summary_df["Station Number"][row]):
                        station_number = ""
                    elif np.modf(summary_df["Station Number"][row])[0] == 0:
                        station_number = "{:3.0f}".format(
                            summary_df["Station Number"][row]
                        )
                    else:
                        station_number = "{:3.1f}".format(
                            summary_df["Station Number"][row]
                        )
                else:
                    station_number = row

                # Location
                if np.isnan(summary_df["Location"][row]):
                    location = ""
                else:
                    location = "{:6.3f}".format(
                        summary_df["Location"][row] * self.parent.units["L"]
                    )

                # Duration
                if np.isnan(summary_df["Duration"][row]):
                    duration = ""
                else:
                    duration = "{:4.1f}".format(summary_df["Duration"][row])

                # Number Valid Profiles
                if np.isnan(summary_df["Number Valid Profiles"][row]):
                    valid_ens = ""
                else:
                    valid_ens = "{:4.0f}".format(
                        summary_df["Number Valid Profiles"][row]
                    )
                if (
                    summary_df["Station Number"][row]
                    in self.parent.meas.qa.verticals["valid_warning"]
                ):
                    warning.append((3, row + 1))
                elif (
                    summary_df["Station Number"][row]
                    in self.parent.meas.qa.verticals["valid_caution"]
                ):
                    caution.append((3, row + 1))

                # Number Valid Cells
                if np.isnan(summary_df["Number Cells"][row]):
                    valid_cells = ""
                else:
                    valid_cells = "{:4.0f}".format(summary_df["Number Cells"][row])

                # Depth
                if np.isnan(summary_df["Depth"][row]):
                    depth = ""
                else:
                    depth = "{:5.3f}".format(
                        summary_df["Depth"][row] * self.parent.units["L"]
                    )

                # Width
                if np.isnan(summary_df["Width"][row]):
                    width = ""
                else:
                    width = "{:7.3f}".format(
                        summary_df["Width"][row] * self.parent.units["L"]
                    )

                # Area
                if np.isnan(summary_df["Area"][row]):
                    area = ""
                else:
                    area = "{:8.3f}".format(
                        summary_df["Area"][row] * self.parent.units["A"]
                    )

                # Normal Velocity
                if np.isnan(summary_df["Normal Velocity"][row]):
                    vel = ""
                else:
                    vel = "{:4.3f}".format(
                        summary_df["Normal Velocity"][row] * self.parent.units["V"]
                    )

                if (
                    summary_df["Station Number"][row]
                    in self.parent.meas.qa.w_vel["all_invalid"]["All: "]
                ):
                    warning.append((8, row + 1))
                elif (
                    summary_df["Station Number"][row]
                    in self.parent.meas.qa.w_vel["total_warning"]["All: "]
                ):
                    warning.append((8, row + 1))
                elif (
                    summary_df["Station Number"][row]
                    in self.parent.meas.qa.w_vel["total_caution"]["All: "]
                ):
                    caution.append((8, row + 1))

                # Discharge
                if np.isnan(summary_df["Q"][row]):
                    q = ""
                else:
                    q = "{:.3f}".format(
                        self.parent.meas.summary["Q"][row] * self.parent.units["Q"]
                    )

                # Percent Discharge with highlighting based on value
                if np.isnan(summary_df["Q%"][row]):
                    q_per = ""
                else:
                    q_per = "{:3.2f}".format(summary_df["Q%"][row])
                    if (
                        summary_df["Q%"][row]
                        > self.parent.agency_options["StationQ"]["warning"]
                    ):
                        warning.append((10, row + 1))
                    elif (
                        summary_df["Q%"][row]
                        > self.parent.agency_options["StationQ"]["caution"]
                    ):
                        caution.append((10, row + 1))

                # Assign data to table row
                row_data = [
                    station_number,
                    location,
                    duration,
                    valid_ens,
                    valid_cells,
                    depth,
                    width,
                    area,
                    vel,
                    q,
                    q_per,
                ]
                data.append(row_data)

        # Create style list
        style_list = [
            ("ALIGN", (0, 0), (-1, 0), "CENTER"),
            ("ALIGN", (0, 1), (-1, -1), "RIGHT"),
            ("LINEABOVE", (0, 0), (-1, 0), 1, colors.black),
            ("LINEBELOW", (0, 0), (-1, 0), 1, colors.black),
            ("LINEBELOW", (0, nrows), (-1, nrows), 1, colors.black),
            ("LINEBELOW", (0, nrows + 2), (-1, nrows + 2), 1, colors.black),
        ]

        # Conditional styling
        for item in warning:
            style_list.append(("BACKGROUND", item, item, colors.red))

        for item in caution:
            style_list.append(("BACKGROUND", item, item, colors.gold))

        # Create and style table
        table_1 = Table(data, colWidths=None, rowHeights=None, repeatRows=1)
        table_style = TableStyle(style_list)
        table_1.setStyle(table_style)

        return table_1

    @staticmethod
    def color_key():
        """Creates a color key for caution and warning highlighting in table.

        Returns
        -------
        tbl: Table
            Object of Table
        """

        data = [["", "Caution", "", "Warning", "> See Automated QA Messages"]]

        tbl = Table(
            data,
            colWidths=[0.5 * inch, 0.75 * inch, 0.5 * inch, 0.6 * inch, 3 * inch],
            rowHeights=None,
        )
        tbl.setStyle(
            [
                ("BACKGROUND", (0, 0), (0, 0), colors.gold),
                ("BACKGROUND", (2, 0), (2, 0), colors.red),
            ]
        )
        tbl.hAlign = "LEFT"
        return tbl

    def summary_table_2(self):
        """Create 2nd summary table containing other station characteristics

        Returns
        -------
        table_2: Table
            Object of Table
        """

        # Column labels
        data = [
            [
                self.label("Sta. Num.", bold=True),
                self.label("Water Surface", bold=True),
                self.label("Velocity Method", bold=True),
                self.label("Velocity Ref.", bold=True),
                self.label("Depth Source", bold=True),
                self.label(
                    "Ice<br/>Depth {}".format(self.parent.units["label_L"]), bold=True
                ),
                self.label(
                    "Slush Depth {}".format(self.parent.units["label_L"]), bold=True
                ),
                self.label("Top Extrap.", bold=True),
                self.label("Ice Exp.", bold=True),
                self.label("Bottom Extrap", bold=True),
                self.label("Bottom Exp.", bold=True),
            ]
        ]

        # Data for table extracted from summary dataframe
        summary_df = self.parent.meas.summary
        nrows = summary_df.shape[0] - 2

        if nrows > 0:
            idx_list = summary_df.index.values.tolist()
            for row in idx_list:
                if (
                    type(row) is int
                    and np.modf(summary_df["Station Number"][row])[0] == 0
                ):
                    # Station Number
                    if np.isnan(summary_df["Station Number"][row]):
                        station_number = ""
                    else:
                        station_number = "{:3.0f}".format(
                            summary_df["Station Number"][row]
                        )

                    # Condition
                    condition = summary_df["Condition"][row]

                    # Velocity Method
                    vel_method = summary_df["Velocity Method"][row]

                    # Velocity Reference
                    vel_ref = summary_df["Velocity Reference"][row]

                    # Depth source
                    depth_source = summary_df["Depth Source"][row]

                    # Ice depth
                    ice_depth = "{:.3f}".format(summary_df["Depth to Ice Bottom"][row])

                    # Slush depth
                    slush_depth = "{:.3f}".format(
                        summary_df["Depth to Slush Bottom"][row]
                    )

                    # Top method
                    top_method = summary_df["Top Method"][row]

                    # Ice exponent
                    if np.isnan(summary_df["Ice Exponent"][row]) or condition != "Ice":
                        ice_exp = ""
                    else:
                        ice_exp = "{:.4f}".format(summary_df["Ice Exponent"][row])

                    # Bottom method
                    bottom_method = summary_df["Bottom Method"][row]

                    # Bottom exponent
                    if np.isnan(summary_df["Bottom Exponent"][row]):
                        bottom_exp = ""
                    else:
                        bottom_exp = "{:.4f}".format(summary_df["Bottom Exponent"][row])

                    row_data = [
                        station_number,
                        condition,
                        vel_method,
                        vel_ref,
                        depth_source,
                        ice_depth,
                        slush_depth,
                        top_method,
                        ice_exp,
                        bottom_method,
                        bottom_exp,
                    ]
                    data.append(row_data)

        # Create and style table
        table_2 = Table(
            data, colWidths=None, rowHeights=None, repeatRows=1, spaceBefore=12
        )
        table_2.setStyle(
            [
                ("ALIGN", (0, 0), (-1, -1), "CENTER"),
                ("LINEABOVE", (0, 0), (-1, 0), 1, colors.black),
                ("LINEBELOW", (0, 0), (-1, 0), 1, colors.black),
                ("LINEBELOW", (0, -1), (-1, -1), 1, colors.black),
            ]
        )
        return table_2

    def velocity_fig(self):
        # Initialize figure

        vel_canvas = MplCanvas(parent=None, width=7, height=4, dpi=300)

        # Initialize the figure and assign to the canvas
        vel_fig = Graphics(canvas=vel_canvas)
        # Create the figure with the specified data
        vel_fig.main_vel_graphs(
            meas=self.parent.meas,
            color_map=self.parent.color_map,
            selected=self.parent.vertical_idx,
            units=self.parent.units,
        )

        return self.fig2image(vel_fig.fig, 7)

    def label(self, txt, size=10, bold=False):
        """Use HTML to format labels

        Returns
        -------
        : Paragraph
            Object of Paragraph
        """
        if bold:
            return Paragraph(
                "<para align=center><font size={size}><b>{txt}</b></font></para>".format(
                    size=size, txt=txt
                ),
                self.styles["Normal"],
            )

        return Paragraph(
            "<para align=center><font size={size}>{txt}</font></para>".format(
                size=size, txt=txt
            ),
            self.styles["Normal"],
        )

    @staticmethod
    def fig2image(fig, width):
        """Convert figure to a ReportLab Image

        Parameters
        ----------
        fig: Figure
            Object of Figure
        width: float
            Width of figure in inches

        Returns
        -------
        : Image
            Object of Image
        """

        # Save fig to internal memory
        buf = io.BytesIO()
        fig.savefig(buf, format="png", dpi=300)
        buf.seek(0)

        # Scale image to specified width
        x, y = fig.get_size_inches()
        ratio = width / x

        return Image(buf, x * ratio * inch, y * ratio * inch)

    def save(self):
        """Build and save pdf file"""

        self.doc.build(
            self.elements,
            onFirstPage=self.header,
            onLaterPages=self.header,
            canvasmaker=PageNumCanvas,
        )

    def create(self):
        """Create pdf documents using flowtables."""

        # First page
        self.elements.append(self.data_version())
        self.elements.append(Spacer(1, 4))
        self.elements.append(self.top_table())
        self.elements.append(Spacer(1, 14))
        self.elements.append(self.qa_messages())
        self.elements.append(Spacer(1, 14))
        self.elements.append(self.comments())
        self.elements.append(PageBreak())

        # New page
        self.elements.append(Spacer(1, 6))
        self.elements.append(self.color_key())
        self.elements.append(Spacer(1, 4))
        self.elements.append(self.summary_table_1())
        self.elements.append(PageBreak())

        # New page
        self.elements.append(self.summary_table_2())
        self.elements.append(PageBreak())

        # New page
        self.elements.append(Spacer(1, 36))
        self.elements.append(self.velocity_fig())
        self.elements.append(Spacer(1, 36))
        self.elements.append(
            self.fig2image(self.parent.main_percent_discharge_fig.fig, 4.5)
        )

        self.save()


class PageNumCanvas(canvas.Canvas):
    """Creates page x of pages text for header."""

    def __init__(self, *args, **kwargs):
        """Constructor with variable parameters."""

        canvas.Canvas.__init__(self, *args, **kwargs)
        self.pages = []

    def showPage(self):
        """On a page break add information to list."""

        self.pages.append(dict(self.__dict__))
        self._startPage()

    def save(self):
        """Add the page number to each page (page x of y)."""

        page_count = len(self.pages)

        for page in self.pages:
            self.__dict__.update(page)
            self.draw_page_number(page_count)
            canvas.Canvas.showPage(self)

        canvas.Canvas.save(self)

    def draw_page_number(self, page_count):
        """Add the page number.

        Parameters
        ----------
        page_count: int
            Total number of pages
        """

        page = "%s of %s" % (self._pageNumber, page_count)
        self.setFont("Helvetica", 10)
        width, height = self._pagesize
        self.drawRightString(width - 0.5 * inch, height - 0.5 * inch, page)
