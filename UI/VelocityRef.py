from PyQt5 import QtWidgets
from UI import wVelocityRef


class VelocityRef(QtWidgets.QDialog, wVelocityRef.Ui_Dialog):
    """Dialog to allow users to set a velocity reference."""

    def __init__(self, parent=None):
        super(VelocityRef, self).__init__(parent)
        self.setupUi(self)
        self.pb_apply_all.clicked.connect(self.accept)
        self.pb_cancel.clicked.connect(self.reject)
