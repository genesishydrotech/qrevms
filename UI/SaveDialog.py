import os
import datetime as datetime
from PyQt5 import QtWidgets
from UI.StickySettings import StickySettings as SSet


class SaveDialog(QtWidgets.QDialog):
    """Dialog to allow users to select measurement files for processing.

    Parameters
    ----------
    wSelectFile.Ui_selectFile : QDialog
        Dialog window with options for users

    Attributes
    ----------
    full_Name: str
        Filename with path to save file.
    """

    def __init__(self, save_type="QRev", parent=None):
        """Initializes settings and connections.

        Parameters
        ----------
        parent
            Identifies parent GUI.
        save_type: str
            Indicates type of save
        """
        super(SaveDialog, self).__init__(parent)
        # self.setupUi(self)
        self.full_Name = None
        self.file_extension = None
        # Create settings object which contains the default folder
        settings = SSet(parent.settingsFile)
        title = ""
        file_name = ""
        filetype = ""

        # Get the current folder setting.
        folder = self.default_folder(settings)
        version = str(int(round(float(parent.QRevMS_version[-4:]) * 100)))
        if float(parent.QRevMS_version[-4:]) < 1:
            version = "0" + version

        # Create default file name
        if save_type == "QRev":
            file_name = os.path.join(
                folder,
                datetime.datetime.today().strftime(
                    "%Y%m%d_%H%M%S_" + version + "_QRevMS.qrevms"
                ),
            )
            title = self.tr("Save File")
            filetype = self.tr("QRevMS File (*_QRevMS.qrevms)")
            # Get the full names (path + file) of the selected file
            # self.full_Name = QtWidgets.QFileDialog.getSaveFileName(
            #     self,
            #     self.tr("Save File"),
            #     file_name,
            #     self.tr("QRevIntMS File (*_QRevIntMS.zip)"),
            # )[0]
            # if len(self.full_Name) > 0:
            #     if self.full_Name[-4:] != ".zip":
            #         self.full_Name = self.full_Name + ".zip"

        elif save_type == "fig":
            file_name = folder + "/" + folder.split("/")[-1]
            title = self.tr("Save figure")
            filetype = f"PNG (*.png);;JPEG (*.jpeg);;PDF (*.pdf);;SVG (*.svg);;{self.tr('All Files')} (*)"

        # Get the full names (path + file) of the selected file
        file_save = QtWidgets.QFileDialog.getSaveFileName(
            self, title, file_name, filetype
        )

        if file_save is not None:
            self.full_Name, file_type = file_save
            file_extension = file_type.split("*")[-1][:-1]
            if len(self.full_Name) > 0:
                self.file_extension = file_extension
                if self.full_Name[-len(file_extension) :] != file_extension:
                    self.full_Name = self.full_Name + file_extension

    @staticmethod
    def default_folder(settings):
        """Returns default folder.

        Returns the folder stored in settings or if no folder is stored, then the current
        working folder is returned.
        """
        try:
            folder = settings.get("Folder")
            if not folder:
                folder = os.getcwd()
        except KeyError:
            settings.new("Folder", os.getcwd())
            folder = settings.get("Folder")
        return folder
