from PyQt5 import QtWidgets
from UI import wAzimuth


class Azimuth(QtWidgets.QDialog, wAzimuth.Ui_Dialog):
    """Dialog to allow users to set azimuth."""

    def __init__(self, parent=None):
        super(Azimuth, self).__init__(parent)
        self.setupUi(self)
        self.pb_ok.clicked.connect(self.accept)
        self.pb_cancel.clicked.connect(self.reject)
