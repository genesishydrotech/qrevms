from PyQt5 import QtWidgets


class EdGrp(object):
    """Provides the logic to control the behavior of an edit box that can be applied to one or all data.

    Attributes
    ----------
    ed: QtWidget
        Line edit widget for manual input
    pb_apply: QtWidget
        Pushbutton widget to apply to selected data only.
    pb_apply_all: QtWidget
        Pushbutton widget to apply to all data.
    change: method
        Method in the parent used to make the specified change
    """

    def __init__(self, ed, pb_apply, pb_apply_all, change, min_value=None):
        """Initializes the edit group variables.

        Parameters
        ----------
        ed: QtWidget
            Line edit widget for manual input
        pb_apply: QtWidget
            Pushbutton widget to apply to selected data only.
        pb_apply_all: QtWidget
            Pushbutton widget to apply to all data.
        change: method
            Method in the parent used to make the specified change
        min_value: float or int
             Minimum acceptable value
        """

        self.ed = ed
        self.pb_apply = pb_apply
        self.pb_apply_all = pb_apply_all
        self.change = change
        self.min_value = min_value

    def enable_apply(self):
        """Enables the apply buttons when a valid value is entered."""
        self.pb_apply.setEnabled(True)
        self.pb_apply_all.setEnabled(True)

    def apply(self):
        """Applies the change to the selected data only."""
        if self.check_valid():
            self.change(apply_all=False)
        self.pb_apply.setEnabled(False)
        self.pb_apply_all.setEnabled(False)

    def apply_all(self):
        """Applies the change to all the data."""
        if self.check_valid():
            self.change(apply_all=True)
        self.pb_apply.setEnabled(False)
        self.pb_apply_all.setEnabled(False)

    def check_valid(self):
        """Checks if the input is valid and returns a bool."""
        value = self.check_numeric_input(self.ed, block=False, min_value=self.min_value)
        if value is not None:
            return True
        else:
            return False

    @staticmethod
    def check_numeric_input(obj, block=True, min_value=None):
        """Check if input is a numeric object.

        Parameters
        ----------
        obj: QtWidget
            QtWidget user edit box.
        block: bool
            Block signals
        min_value: float or int
             Minimum acceptable value

        Returns
        -------
        out: float
            obj converted to float if possible
        """
        if block:
            obj.blockSignals(True)
        out = None
        if len(obj.text()) > 0:
            # Try to convert obj to a float
            try:
                text = obj.text()
                out = float(text)
                if min_value is not None:
                    if out >= min_value:
                        return out
                    else:
                        obj.setText("")
                        msg = QtWidgets.QMessageBox()
                        msg.setIcon(QtWidgets.QMessageBox.Critical)
                        msg.setText("Error")
                        msg.setInformativeText("Value must be > 0")
                        msg.setWindowTitle("Error")
                        msg.exec_()
                        return None
            # If conversion is not successful warn user that the input is invalid.
            except ValueError:
                obj.setText("")
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Critical)
                msg.setText("Error")
                msg.setInformativeText(
                    "You have entered non-numeric text. Enter a numeric value."
                )
                msg.setWindowTitle("Error")
                msg.exec_()
                return None

        if block:
            obj.blockSignals(False)
        return out
