import numpy as np
import copy
import pandas as pd
from matplotlib import gridspec
import matplotlib.cm as cm
from matplotlib.dates import DateFormatter, num2date
import matplotlib.dates as mdates
from matplotlib import collections as collections
from matplotlib.patches import Polygon
from matplotlib.ticker import MaxNLocator
from PyQt5 import QtWidgets, QtCore
from contextlib import contextmanager
from datetime import datetime, timedelta
from Modules.common_functions import tand, convert_temperature


class Graphics(object):
    """Class contains method used to create graphics in QRevIntMS user interface.

    Attributes
    ----------
    canvas: MplCanvas
        Object of MplCanvas
    fig: canvas.fig
        Figure object
    units = dict
        Dictionary of unit conversions and labels
    hover_connection: int
        Index into data for data cursor
    parent = QRevIntMS
        Main GUI
    select_connection: int
        Index used to identify vertical selected from graph
    annot: list
        List with annotations for data cursor
    x_axis_type: str
        Identifies type of x-axis (E, L)
    fig_no: int
        Figure number
    color_map: str
        Name of color map to use for color contours (viridis, jet)
    x = np.array()
        Data for x-axis
    x_timestamp: np.array()
        Data for x-axis is time stamps are used
    ax: list
        List of axes
    n_subplots: int
        Number of subplots
    measurement: Measurement
        Object of class Measurement
    data_plotted: list
        List contain type and data plotted for use with data cursor
    plot_df: dataframe
        Dataframe for uncertainty data
    gs: gridspec
        Grid specification
    ping_name: dict
            Dictionary of ping names
    show_below_sl: bool
        Indicates if data below sidelobe should be shown
    transect: VerticalData
        Object of VerticalData
    p_type_color: dict
        Dictionary of colors used for displaying the ping types
    p_type_marker: dict
        Dictionary of markers used for displaying the ping types
    wt_legend_dict: dict
        Dictionary providing legend text for the water track ping types
    bt_legend_dict: dict
        Dictionary providing legend text for bottom track frequencies
    freq_color: dict
        Dictionary of colors to display frequency
    freq_marker: dict
        Dictionary of markers to display frequency
    """

    def __init__(self, canvas):
        """Initialize object using the specified canvas.

        Parameters
        ----------
        canvas: MplCanvas
            Object of MplCanvas
        """

        # Initialize attributes
        self.canvas = canvas
        self.fig = canvas.fig
        self.units = None
        self.hover_connection = None
        self.parent = None
        self.select_connection = None
        self.annot = None
        self.x_axis_type = "E"
        self.fig_no = 0
        self.color_map = "jet"
        self.x = None
        self.x_timestamp = None
        self.n_subplots = 0
        self.measurement = None
        self.ax = []
        self.annot = []
        self.data_plotted = []
        self.plot_df = None
        self.gs = None
        self.ping_name = None
        self.show_below_sl = False
        self.transect = None
        self.p_type_color = {
            "I": "b",
            "C": "#009933",
            "S": "#ffbf00",
            "1I": "b",
            "1C": "#009933",
            "3I": "#ffbf00",
            "3IC": "#ff33cc",
            "3C": "#ff33cc",
            "3BB": "b",
            "3PC": "#ff33cc",
            "3PCBB": "#ffbf00",
            "U": "b",
        }
        self.p_type_marker = {
            "I": ".",
            "C": "+",
            "S": "x",
            "1I": ".",
            "1C": "*",
            "3I": "+",
            "3IC": "+",
            "3C": "x",
            "3BB": ".",
            "3PC": "+",
            "3PCBB": "x",
            "U": ".",
        }
        self.wt_legend_dict = {
            "I": "Incoherent",
            "C": "Coherent",
            "S": "Surface Cell",
            "1I": "1MHz Inc",
            "1C": "1 MHz HD",
            "3I": "3 MHz Inc",
            "3IC": "3 MHz Inc",
            "3C": "3 MHz HD",
            "3BB": "3 MHz BB",
            "3PC": "3 MHz PC",
            "3PCBB": "3 MHz PC/BB",
            "U": "N/A",
        }
        self.bt_legend_dict = {
            "600": "600 kHz",
            "1200": "1200 kHz",
            "1000": "1 MHz",
            "2000": "2 MHz",
            "2400": "2.4 MHz",
            "3000": "3 MHz",
            "0": "N/U",
        }
        self.freq_color = {
            "0": "b",
            "600": "b",
            "1200": "b",
            "1000": "b",
            "2000": "b",
            "2400": "b",
            "3000": "#009933",
        }
        self.freq_marker = {
            "0": ".",
            "600": ".",
            "1200": ".",
            "1000": ".",
            "2000": ".",
            "2400": ".",
            "3000": "+",
        }

    def main_vel_graphs(self, meas, color_map, selected, units):
        """Create velocity quiver and color contour graphs for main summary tab.

        Parameters
        ----------
        meas: Measurement
            Object of Measurement class
        color_map: str
            Color map to use (viridis, jet)
        selected:
        units: dict
            Dictionary of units conversions
        """

        with self.wait_cursor():
            # Set units
            self.units = units

            # Clear the plot
            self.fig.clear()

            # Determine number of subplots
            self.n_subplots = 2

            # Initialize variable for subplots
            self.ax = []
            self.annot = []
            self.data_plotted = []

            # Create grid specification
            # Note: the second column of the grid is for the color bar.
            # It is blank but present even for time series
            # plots to allow the sharing of the x-axis between all plots
            self.gs = gridspec.GridSpec(
                self.n_subplots, 2, width_ratios=[50, 1], height_ratios=[1, 3]
            )

            # Create first subplot
            self.ax.append(self.fig.add_subplot(self.gs[self.fig_no]))
            self.velocity_quiver(meas)

            # Create second subplot
            self.fig_no += 2

            # Create water speed subplots as specified, sharing x axis for all plots
            self.ax.append(
                self.fig.add_subplot(self.gs[self.fig_no], sharex=self.ax[0])
            )
            self.mean_speed_contour(meas=meas, color_map=color_map, selected=selected)

            # Adjust the spacing of the subplots
            self.fig.subplots_adjust(
                left=0.1, bottom=0.1, right=0.9, top=0.95, wspace=0.02, hspace=0.20
            )

            # Apply the x-axis label to the bottom x-axis
            self.ax[-2].xaxis.label.set_fontsize(12)
            self.set_x_axis(meas, -2)

        self.canvas.draw()

    def main_extrap_graph(self, meas, units, idx):
        """Create extrapolation graph for main summary tab.

        Parameters
        ----------
        meas: Measurement
            Object of Measurement class
        units: dict
            Dictionary of units conversions
        idx: int
            Index of vertical to be graphed
        """

        with self.wait_cursor():
            # Set units
            self.units = units

            # Clear the plot
            self.fig.clear()

            # Determine number of subplots
            self.n_subplots = 1

            # Initialize variable for subplots
            self.ax = []
            self.annot = []
            self.data_plotted = []

            # Create grid specification
            self.gs = gridspec.GridSpec(self.n_subplots, 1)

            # Create first subplot
            self.ax.append(self.fig.add_subplot(self.gs[self.fig_no]))
            if meas.verticals[idx].edge_loc == "":
                self.extrap_graph(
                    meas=meas,
                    units=units,
                    idx=idx,
                    data_points=True,
                    data_profiles=False,
                    data_surface=False,
                    mean_profile=True,
                    fit=True,
                    fit_auto=True,
                )

            self.fig.subplots_adjust(
                left=0.13, bottom=0.1, right=0.98, top=0.98, wspace=0.1, hspace=0
            )

            self.annot[-1].set_visible(False)

            self.canvas.draw()

    def rssi_graph(self, meas, units, idx):
        """Create rssi graph.

        Parameters
        ----------
        meas: Measurement
            Object of Measurement class
        units: dict
            Dictionary of units conversions
        idx: int
            Index of vertical to be graphed
        """

        with self.wait_cursor():
            # Set units
            self.units = units

            # Clear the plot
            self.fig.clear()

            # Determine number of subplots
            self.n_subplots = 1

            # Initialize variable for subplots
            self.ax = []
            self.annot = []
            self.data_plotted = []

            # Create grid specification
            self.gs = gridspec.GridSpec(self.n_subplots, 1)

            # Create first subplot
            self.ax.append(self.fig.add_subplot(self.gs[self.fig_no]))
            self.fig.subplots_adjust(
                left=0.15, bottom=0.1, right=0.98, top=0.98, wspace=0.05, hspace=0
            )

            # Compute data to be plotted
            vertical = meas.verticals[idx]

            # Create graph is selected vertical is not an edge
            if vertical.edge_loc == "" and vertical.mean_profile["rssi"].shape[0] > 0:
                # Get data from vertical object
                rssi_units = vertical.data.w_vel.rssi_units
                rssi_plt = vertical.mean_profile["rssi"]
                cell_depth_plt = (
                    vertical.mean_profile["cell_depth"][1:-1] * self.units["L"]
                )

                # Plot data
                ax = self.ax[-1]
                ax.plot(rssi_plt[0, :], cell_depth_plt, linestyle="-", color="r")
                ax.plot(rssi_plt[1, :], cell_depth_plt, linestyle="-", color="b")
                ax.plot(rssi_plt[2, :], cell_depth_plt, linestyle="-", color="g")
                ax.plot(
                    rssi_plt[3, :], cell_depth_plt, linestyle="-", color="darkorange"
                )

                # Set plot parameters
                ax.set_xlabel(self.canvas.tr("Intensity ") + "(" + rssi_units + ")")
                ax.set_ylabel(self.canvas.tr("Depth ") + units["label_L"])
                ax.xaxis.label.set_fontsize(10)
                ax.yaxis.label.set_fontsize(10)
                ax.tick_params(
                    axis="both",
                    direction="in",
                    bottom=True,
                    top=True,
                    left=True,
                    right=True,
                )
                ax.grid()
                for label in ax.get_xticklabels() + ax.get_yticklabels():
                    label.set_fontsize(10)
                if np.isnan(vertical.depth_m):
                    bottom = 0
                else:
                    bottom = (vertical.depth_m * self.units["L"]) * 1.05
                ax.set_ylim(top=0, bottom=bottom)
                ax.legend(
                    [
                        self.canvas.tr("Beam 1"),
                        self.canvas.tr("Beam 2"),
                        self.canvas.tr("Beam 3"),
                        self.canvas.tr("Beam 4"),
                    ]
                )
                # Save data plotted for use with data cursor
                self.data_plotted = [
                    {"type": "profile", "x": rssi_plt, "y": cell_depth_plt}
                ]

                if vertical.ws_condition == "Ice":
                    ice = vertical.ws_to_ice_bottom_m
                    slush_bottom = vertical.ws_to_slush_bottom_m
                    xmin, xmax = self.ax[-1].get_xlim()

                    coords = [
                        (xmin, 0),
                        (xmin, ice),
                        (xmax, ice),
                        (xmax, 0),
                        (xmin - 0.5, 0),
                    ]
                    self.ax[-1].add_patch(
                        Polygon(coords, hatch="xxx", edgecolor="k", facecolor="#b3ffff")
                    )
                    coords = [
                        (xmin, ice),
                        (xmin, slush_bottom),
                        (xmax, slush_bottom),
                        (xmax, ice),
                        (xmin, ice),
                    ]
                    self.ax[-1].add_patch(
                        Polygon(
                            coords, hatch="....", edgecolor="k", facecolor="#e6ffff"
                        )
                    )

                # Initialize annotation for data cursor
                self.annot.append(
                    ax.annotate(
                        "",
                        xy=(0, 0),
                        xytext=(-20, 20),
                        textcoords="offset points",
                        bbox=dict(boxstyle="round", fc="w"),
                        arrowprops=dict(arrowstyle="->"),
                    )
                )
                self.annot[-1].set_visible(False)
                self.canvas.draw()

    def compass_pr_graphs(self, meas, units, heading_types, pr_types, idx_list):
        """Create compass pitch and roll graphs for compass_pr tab.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        units: dict
            Dictionary of units conversions and labels
        heading_types: str or list
            Identifies source of heading data
        pr_types: str or list
            Identifies source of pitch and roll data
        idx_list: list
            List of verticals to plot
        """

        with self.wait_cursor():
            # Set units
            self.units = units

            # Clear the plot
            self.fig.clear()

            # Determine number of subplots
            self.n_subplots = 2

            # Initialize variable for subplots
            self.ax = []
            self.annot = []
            self.data_plotted = []

            # Create grid specification
            # Note: the second column of the grid is for the color bar.
            # It is blank but present even for time series
            # plots to allow the sharing of the x-axis between all plots
            self.gs = gridspec.GridSpec(self.n_subplots, 1, height_ratios=[1, 1])

            # Create first subplot
            self.ax.append(self.fig.add_subplot(self.gs[self.fig_no]))

            # Plot heading data
            for heading_type in heading_types:
                # Internal heading
                if heading_type == "Internal":
                    for idx in idx_list:
                        data = meas.verticals[
                            idx
                        ].data.sensors.heading_deg.internal.data
                        self.x = list(range(1, data.shape[0] + 1))
                        fmt = [{"color": "r", "linestyle": "-"}]
                        data_units = (1, "Heading (deg)")
                        self.plt_timeseries(
                            data=data,
                            data_units=data_units,
                            ax=self.ax[-1],
                            fmt=fmt,
                            set_annot=True,
                        )
                # External heading
                if heading_type == "External":
                    for idx in idx_list:
                        data = meas.verticals[
                            idx
                        ].data.sensors.heading_deg.external.data
                        self.x = list(range(1, data.shape[0] + 1))
                        fmt = [{"color": "b", "linestyle": "-"}]
                        data_units = (1, "Heading (deg)")
                        self.plt_timeseries(
                            data=data,
                            data_units=data_units,
                            ax=self.ax[-1],
                            fmt=fmt,
                            set_annot=False,
                        )
                # Magnetic change
                if heading_type == "MagChange":
                    self.ax.append(self.ax[-1].twinx())
                    for idx in idx_list:
                        data = meas.verticals[
                            idx
                        ].data.sensors.heading_deg.internal.mag_error
                        self.x = list(range(1, data.shape[0] + 1))
                        fmt = [{"color": "k", "linestyle": "-"}]
                        data_units = (1, "Magnetic Change (%)")
                        self.plt_timeseries(
                            data=data,
                            data_units=data_units,
                            ax=self.ax[-1],
                            fmt=fmt,
                            set_annot=False,
                            set_ylim=False
                        )
            self.ax[0].set_yticks([0, 90, 180, 270, 360])
            self.ax[0].set_ylim([-10, 370])
            self.ax[0].grid(False)
            if len(self.ax) > 1:
                ymax = np.nanmax(self.ax[1].get_ylim())
                self.ax[1].set_ylim([0, ymax * 1.1])
                self.ax[1].grid(False)
            
            # Pitch and roll plots
            self.fig_no += 1
            self.ax.append(
                self.fig.add_subplot(self.gs[self.fig_no], sharex=self.ax[0])
            )

            min_max = np.array([0.0, 0.0, 0.0, 0.0])

            for pr_type in pr_types:
                # Roll
                if pr_type == "Roll":
                    for idx in idx_list:
                        data = meas.verticals[idx].data.sensors.roll_deg.internal.data
                        self.x = list(range(1, data.shape[0] + 1))
                        fmt = [{"color": "b", "linestyle": "-"}]
                        data_units = (1, "Pitch or Roll (deg)")
                        self.plt_timeseries(
                            data=data,
                            data_units=data_units,
                            ax=self.ax[-1],
                            fmt=fmt,
                            set_annot=False,
                        )
                        if np.nanmax(data) > min_max[0]:
                            min_max[0] = np.nanmax(data)
                        if np.nanmin(data) < min_max[1]:
                            min_max[1] = np.nanmin(data)

                # Pitch
                if pr_type == "Pitch":
                    for idx in idx_list:
                        data = meas.verticals[idx].data.sensors.pitch_deg.internal.data
                        self.x = list(range(1, data.shape[0] + 1))
                        fmt = [{"color": "r", "linestyle": "-"}]
                        data_units = (1, "Pitch or Roll (deg)")
                        self.plt_timeseries(
                            data=data,
                            data_units=data_units,
                            ax=self.ax[-1],
                            fmt=fmt,
                            set_annot=False,
                        )
                        if np.nanmax(data) > min_max[2]:
                            min_max[2] = np.nanmax(data)
                        if np.nanmin(data) < min_max[3]:
                            min_max[3] = np.nanmin(data)

                self.ax[-1].set_ylim(
                    [np.nanmin(min_max) * 1.1, np.nanmax(min_max) * 1.1]
                )

        self.ax[-1].set_xlabel(self.canvas.tr("Ensembles"))

        # Adjust the spacing of the subplots
        self.fig.subplots_adjust(
            left=0.05, bottom=0.08, right=0.96, top=0.95, wspace=0.02, hspace=0.08
        )

        # Initialize annotation for data cursor
        self.annot.append(
            self.ax[-1].annotate(
                "",
                xy=(0, 0),
                xytext=(-20, 20),
                textcoords="offset points",
                bbox=dict(boxstyle="round", fc="w"),
                arrowprops=dict(arrowstyle="->"),
            )
        )
        self.annot[-1].set_visible(False)

        self.canvas.draw()

    def temperature_ts_graph(self, meas, temp_units, idx=None):
        """Creates temperature time series plot.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        temp_units: str
            Identifies temperature units (F, C)
        idx: int
            Specifies vertical to highlight on time series graph
        """

        with self.wait_cursor():
            # Clear the plot
            self.fig.clear()

            # Determine number of subplots
            self.n_subplots = 1

            # Initialize variable for subplots
            self.ax = []
            self.annot = []
            self.data_plotted = []

            # Create grid specification
            self.gs = gridspec.GridSpec(self.n_subplots, 1)

            # Create first subplot
            self.ax.append(self.fig.add_subplot(self.gs[self.fig_no]))

            # Set margins and padding for figure
            self.fig.subplots_adjust(
                left=0.1, bottom=0.15, right=0.98, top=0.98, wspace=0.1, hspace=0
            )

            # Compute time series
            temp, serial_time = meas.compute_time_series(meas, "Temperature")

            # Create list from time stamps
            time_stamp = []
            for t in serial_time:
                time_stamp.append(datetime.utcfromtimestamp(t))

            # Set label to display correct units
            y_label = self.canvas.tr("Degrees C")
            if temp_units == "F":
                temp = convert_temperature(temp_in=temp, units_in="C", units_out="F")
                y_label = self.canvas.tr("Degrees F")

            # Plot data
            ax = self.ax[-1]
            ax.plot(time_stamp, temp, "b.")
            # Customize axis
            time_fmt = mdates.DateFormatter("%H:%M:%S")
            ax.xaxis.set_major_formatter(time_fmt)
            ax.set_xlabel(self.canvas.tr("Time "))
            ax.set_ylabel(y_label)
            ax.xaxis.label.set_fontsize(12)
            ax.yaxis.label.set_fontsize(12)
            ax.tick_params(
                axis="both",
                direction="in",
                bottom=True,
                top=True,
                left=True,
                right=True,
            )
            ax.set_ylim([np.nanmin(temp) - 2, np.nanmax(temp) + 2])
            ax.grid()

            self.data_plotted = [{"type": "ts", "x": time_stamp, "y": temp}]

            # Highlight selected vertical
            if idx is not None:
                # Compute time series
                temp_idx, serial_time_idx = meas.compute_time_series(
                    meas, "Temperature", vertical_idx=idx
                )

                # Create list from time stamps
                time_stamp_idx = []
                for t in serial_time_idx:
                    time_stamp_idx.append(datetime.utcfromtimestamp(t))
                if temp_units == "F":
                    temp_idx = convert_temperature(
                        temp_in=temp_idx, units_in="C", units_out="F"
                    )
                ax.plot(time_stamp_idx, temp_idx, "k.")

            # Initialize annotation for data cursor
            self.annot.append(
                self.ax[-1].annotate(
                    "",
                    xy=(0, 0),
                    xytext=(-20, 20),
                    textcoords="offset points",
                    bbox=dict(boxstyle="round", fc="w"),
                    arrowprops=dict(arrowstyle="->"),
                )
            )

            self.annot[-1].set_visible(False)

            self.canvas.draw()

    def bt_tab_graphs(
        self,
        transect,
        units,
        beam=True,
        error=False,
        vert=False,
        source=False,
        bt=True,
        gga=False,
        vtg=False,
        x_axis_type="E",
    ):
        """Creates the plots for the bottom track tab.
        This approach allows zoom and pan to work together for both plots.

        Parameters
        ----------
        transect: VerticalData
            Object of class VerticalData
        units: dict
            Units selected
        beam: bool
            Indicates if number of beams is plotted
        error: bool
            Indicates if error velocity is plotted
        vert: bool
            Indicates if vertical velocity is plotted
        source: bool
            Indicates if the boat speed source is plotted
        bt: bool
            Indicates if the bottom track is plotted in shiptrack
        gga: bool
            Indicates if the gga track is plotted in the shiptrack
        vtg: bool
            Indicates if the vtg track is plotted in the shiptrack
        x_axis_type: str
            Specifies what variable (ensemble, length or time) to be used for the x-axis
        """
        with self.wait_cursor():
            # Initialize data sources
            self.transect = transect

            # Set x axis type and units
            self.x_axis_type = x_axis_type
            self.units = units

            # Clear the plot
            self.fig.clear()

            # Determine number of subplots
            self.n_subplots = 2

            # Compute x-axis variable
            self.compute_x_axis()

            # Initialize variable for subplots
            self.ax = []
            self.annot = []
            self.data_plotted = []

            # Create grid specification
            # Note: the second column of the grid is for the color bar. It is blank but
            # present even for time series plots to allow the sharing of the x-axis
            # between all plots
            self.gs = gridspec.GridSpec(self.n_subplots, 2, width_ratios=[50, 1])

            # Create first subplot
            self.ax.append(self.fig.add_subplot(self.gs[self.fig_no]))
            if beam:
                self.bt_3beam_ts()
            elif error:
                self.bt_error_ts()
            elif vert:
                self.bt_vertical_ts()
            elif source:
                if transect.boat_vel.selected == "None":
                    ref = None
                else:
                    ref = getattr(transect.boat_vel, transect.boat_vel.selected)
                self.source_ts(ref, "Boat Source")

            self.fig_no += 2
            # Create boat speed subplots as specified, sharing x axis for all plots
            self.ax.append(
                self.fig.add_subplot(self.gs[self.fig_no], sharex=self.ax[0])
            )
            if bt:
                self.bt_speed_ts(lbl="Boat speed")
            if gga:
                self.gga_speed_ts(lbl="Boat speed")
            if vtg:
                self.vtg_speed_ts(lbl="Boat speed")

            # Adjust the spacing of the subplots
            self.fig.subplots_adjust(
                left=0.05, bottom=0.1, right=0.99, top=0.95, wspace=0.08, hspace=0.08
            )

            # Apply the x-axis label to the bottom x-axis
            idx = -1
            self.ax[idx].xaxis.label.set_fontsize(12)
            self.set_x_axis(None, idx)

        self.canvas.draw()

    def create_depth_tab_graphs(
        self,
        meas,
        vertical_idx,
        units,
        b1=True,
        b2=True,
        b3=True,
        b4=True,
        vb=False,
        ds=False,
        avg4_final=False,
        vb_final=False,
        ds_final=False,
        final=True,
        cs=False,
        x_axis_type="E",
    ):
        """Creates the plots for the depth tab. This approach allows zoom and pan to
         work together for both plots.

        Parameters
        ----------
        meas: Measurement
            Measurement data
        vertical_idx: int
            Index of vertical to plot
        units: dict
            Units selected
        b1: bool
            Indicates if beam 1 is plotted
        b2: bool
            Indicates if beam 2 is plotted
        b3: bool
            Indicates if beam 3 is plotted
        b4: bool
            Indicates if beam 4 is plotted
        vb: bool
            Indicates if the vertical beam is plotted
        ds: bool
            Indicates if the depth sounder is plotted
        avg4_final: bool
            Indicates if the 4-beam average cross section is plotted
        vb_final: bool
            Indicates if the vertical beam cross section is plotted
        ds_final: bool
            Indicates if the depth sounder cross section is plotted
        final: bool
            Indicates if the selected method for determining the cross section is plotted
        cs: bool
            Indicates if all verticals comprising the cross section is plotted
        x_axis_type: str
            Specifies what variable (ensemble, length or time) to be used for the x-axis
        """
        with self.wait_cursor():
            # Initialize data sources
            self.transect = meas.verticals[vertical_idx].data

            # Set default axis type and units
            self.x_axis_type = x_axis_type
            self.units = units

            # Clear the plot
            self.fig.clear()

            # Determine number of subplots
            self.n_subplots = 2

            # Compute x-axis variable
            self.compute_x_axis()

            # Initialize variable for subplots
            self.ax = []
            self.annot = []
            self.data_plotted = []

            # Create grid specification
            # Note: the second column of the grid is for the color bar.
            # It is blank but present even for time series
            # plots to allow the sharing of the x-axis between all plots
            self.gs = gridspec.GridSpec(self.n_subplots, 2, width_ratios=[50, 1])

            # Create first subplot
            self.ax.append(self.fig.add_subplot(self.gs[self.fig_no]))
            self.depths_beam_ts(b1, b2, b3, b4, vb, ds, leg=False)

            self.fig_no += 2
            # Create additional subplots as specified
            if not cs:
                # If the bottom plot is not a full cross section share axes
                self.ax.append(
                    self.fig.add_subplot(
                        self.gs[self.fig_no], sharex=self.ax[0], sharey=self.ax[0]
                    )
                )
                self.depths_final_ts(
                    avg4_final=avg4_final,
                    vb_final=vb_final,
                    ds_final=ds_final,
                    final=final,
                )

                # X label
                idx = -1
                self.ax[idx].xaxis.label.set_fontsize(12)
                self.set_x_limits(meas, self.ax[idx])
                self.set_x_axis(None, idx)

                # Adjust the spacing of the subplots
                self.fig.subplots_adjust(
                    left=0.05,
                    bottom=0.07,
                    right=0.99,
                    top=0.95,
                    wspace=0.02,
                    hspace=0.08,
                )

                ydata = np.array([])
                for ax in self.ax:
                    for line in ax.lines:
                        ydata = np.hstack((ydata, line.get_ydata()))

                ydata_max = np.nanmax(ydata) * 1.1
                ydata_min = 0 - np.nanmax(ydata) * 0.1

                if np.isnan(ydata_max):
                    ydata_max = 1
                    ydata_min = 0

                self.ax[0].set_ylim(top=ydata_min, bottom=ydata_max)

            else:
                # If the bottom plot is a full cross section do not share axes
                self.ax.append(self.fig.add_subplot(self.gs[self.fig_no]))
                self.depths_cross_section(meas, vertical_idx)

                # X label
                idx = -1
                self.ax[idx].xaxis.label.set_fontsize(12)
                self.x_axis_type = "L"
                self.set_x_axis(meas, idx)
                self.set_x_limits(meas, self.ax[idx])

                # Adjust the spacing of the subplots
                self.fig.subplots_adjust(
                    left=0.05,
                    bottom=0.07,
                    right=0.99,
                    top=0.95,
                    wspace=0.02,
                    hspace=0.2,
                )

                ydata = np.array([])
                for line in self.ax[0].lines:
                    ydata = np.hstack((ydata, line.get_ydata()))

                ydata_max = np.nanmax(ydata) * 1.1
                ydata_min = 0 - np.nanmax(ydata) * 0.1

                if np.isnan(ydata_max):
                    ydata_max = 1
                    ydata_min = 0

                self.ax[0].set_ylim(top=ydata_min, bottom=ydata_max)

        self.canvas.draw()

    def create_wt_tab_graphs(
        self,
        meas,
        vertical_idx,
        units,
        contour=False,
        beam=False,
        error=False,
        vert=False,
        snr=False,
        speed=False,
        x_axis_type="E",
        color_map="viridis",
    ):
        """Creates the plots for the water track tab.
        This approach allows zoom and pan to work together for both plots.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        vertical_idx: int
            Index to selected vertical
        units: dict
            Units selected
        contour: bool
            Indicates if filtered contour is plotted
        beam: bool
            Indicates if number of beams is plotted
        error: bool
            Indicates if error velocity is plotted
        vert: bool
            Indicates if vertical velocity is plotted
        snr: bool
            Indicates if SNR is plotted
        speed: bool
            Indicates if the average water speed source is plotted
        x_axis_type: str
            Specifies what variable (ensemble, length or time) to be used for the x-axis
        color_map: str
            Name of color map to be used for color contour plots
        """
        with self.wait_cursor():
            # Initialize data sources
            self.transect = meas.verticals[vertical_idx].data

            # Set axis type and units
            self.x_axis_type = x_axis_type
            self.units = units
            self.color_map = color_map

            # Clear the plot
            self.fig.clear()

            # Determine number of subplots
            self.n_subplots = 2

            # Compute x-axis variable
            self.compute_x_axis()

            # Initialize variable for subplots
            self.ax = []
            self.annot = []
            self.data_plotted = []

            # Create grid specification
            # Note: the second column of the grid is for the color bar. It is blank but
            # present even for time series
            # plots to allow the sharing of the x-axis between all plots
            self.gs = gridspec.GridSpec(self.n_subplots, 2, width_ratios=[50, 1])

            # Create first subplot
            self.ax.append(self.fig.add_subplot(self.gs[self.fig_no]))
            if contour:
                self.speed_filtered_contour(vertical=meas.verticals[vertical_idx])
            elif beam:
                self.wt_3beam_ts()
            elif error:
                self.wt_error_ts()
            elif vert:
                self.wt_vertical_ts()
            elif snr:
                self.wt_snr_ts()
            elif speed:
                self.wt_avg_speed_ts()

            self.fig_no += 2
            # Create additional subplots as specified, sharing x axis for all plots
            # and also y axis for contour plots
            if contour:
                self.ax.append(
                    self.fig.add_subplot(
                        self.gs[self.fig_no], sharex=self.ax[0], sharey=self.ax[0]
                    )
                )
            else:
                self.ax.append(
                    self.fig.add_subplot(self.gs[self.fig_no], sharex=self.ax[0])
                )
            self.speed_final_contour(vertical=meas.verticals[vertical_idx])

            # Adjust the spacing of the subplots
            self.fig.subplots_adjust(
                left=0.08, bottom=0.07, right=0.92, top=0.95, wspace=0.02, hspace=0.08
            )

            # Apply the x-axis label to the bottom x-axis
            idx = -2
            self.ax[idx].xaxis.label.set_fontsize(12)
            self.set_x_axis(None, idx)

        self.canvas.draw()

    def create_extrap_tab_graph(
        self,
        meas,
        units,
        idx,
        data_points=True,
        data_profiles=False,
        data_surface=False,
        mean_profile=True,
        fit=True,
        fit_auto=False,
    ):
        with self.wait_cursor():
            # Set units
            self.units = units

            # Clear the plot
            self.fig.clear()

            # Determine number of subplots
            self.n_subplots = 1

            # Initialize variable for subplots
            self.ax = []
            self.annot = []
            self.data_plotted = []

            # Create grid specification
            self.gs = gridspec.GridSpec(self.n_subplots, 1)

            # Create first subplot
            self.ax.append(self.fig.add_subplot(self.gs[self.fig_no]))
            if meas.verticals[idx].edge_loc == "":
                self.extrap_graph(
                    meas=meas,
                    units=units,
                    idx=idx,
                    data_points=data_points,
                    data_profiles=data_profiles,
                    data_surface=data_surface,
                    mean_profile=mean_profile,
                    fit=fit,
                    fit_auto=fit_auto,
                )

            self.fig.subplots_adjust(
                left=0.13, bottom=0.1, right=0.98, top=0.98, wspace=0.1, hspace=0
            )

            self.annot[-1].set_visible(False)

            self.canvas.draw()

    def create_edges_tab_contour(
        self, meas, units, x_axis_type="E", color_map="viridis"
    ):
        """Creates the plots for the edges track tab.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        units: dict
            Units selected
        x_axis_type: str
            Specifies what variable (ensemble, length or time) to be used for the x-axis
        color_map: str
            Name of color map to be used for color contour plots
        """
        with self.wait_cursor():
            # Set axis type and units
            self.x_axis_type = x_axis_type
            self.units = units
            self.color_map = color_map

            # Clear the plot
            self.fig.clear()

            # Determine number of subplots
            self.n_subplots = 2

            # Initialize variable for subplots
            self.ax = []
            self.annot = []
            self.data_plotted = []

            # Create grid specification
            # Note: the second column of the grid is for the color bar. It is blank but
            # present even for time series
            # plots to allow the sharing of the x-axis between all plots
            self.gs = gridspec.GridSpec(1, self.n_subplots, width_ratios=[60, 1])

            # Create first subplot
            self.ax.append(self.fig.add_subplot(self.gs[self.fig_no]))
            self.mean_speed_contour(meas=meas, color_map=color_map, selected=None)

            # Apply the x-axis label to the bottom x-axis
            idx = 0
            self.ax[idx].xaxis.label.set_fontsize(12)
            self.set_x_axis(meas, idx)

            # Adjust the spacing of the subplots
            self.fig.subplots_adjust(
                left=0.05, bottom=0.07, right=0.95, top=0.95, wspace=0.02, hspace=0.05
            )

        self.canvas.draw()

    def create_edges_tab_bar(self, meas, units, color_dict, agency_options):
        """Creates bar graph of discharges for edges tab.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        units: dict
            Units selected
        color_dict: dict
            Dictionary of colors to use for Good, Caution, and Warning
        """

        self.percent_q_bar(meas, units, color_dict, agency_options)

        # Adjust the spacing of the subplots
        self.fig.subplots_adjust(
            left=0.1, bottom=0.07, right=0.98, top=0.95, wspace=0.02, hspace=0.05
        )

    def magdir_graph(self, vertical, units):
        """Creates a mean magnitude and direction profile of valid cells used to
        compute the mean profile.

        Parameters
        ----------
        vertical: Vertical
           Object of class Vertical with data to be displayed
        units: dict
           Dictionary of unit conversions and labels
        """

        # Set units
        self.units = units

        # Clear the plot
        self.fig.clear()

        # Determine number of subplots
        self.n_subplots = 1

        # Initialize variable for subplots
        self.ax = []
        self.annot = []
        self.data_plotted = []

        # Create grid specification
        self.gs = gridspec.GridSpec(self.n_subplots, 1)

        # Create first subplot
        self.ax.append(self.fig.add_subplot(self.gs[self.fig_no]))

        # Compute x data
        mag = vertical.mean_profile["mag"]
        direction = vertical.mean_profile["dir"]

        cell_depth = vertical.mean_profile["cell_depth"] * self.units["L"]
        mag = mag * self.units["V"]
        idx = np.logical_not(np.isnan(mag))
        mag = mag[idx]
        direction = direction[idx]
        cell_depth = cell_depth[idx]

        # Plot magnitude on bottom x axis
        ax = self.ax[-1]
        ax.plot(mag, cell_depth, "bo-")
        ax.set_ylabel(self.canvas.tr("Cell Depth ") + self.units["label_L"])
        ax.grid(True)
        ax.yaxis.label.set_fontsize(12)
        ax.xaxis.label.set_fontsize(12)
        ax.tick_params(
            axis="both", direction="in", bottom=True, top=True, left=True, right=True
        )
        ax.set_xlabel(self.canvas.tr("Velocity Magnitude ") + self.units["label_V"])
        ax.xaxis.label.set_color("blue")
        if np.isnan(vertical.depth_m):
            bottom = 0
        else:
            bottom = (vertical.depth_m * self.units["L"]) * 1.05
        ax.set_ylim(top=0, bottom=bottom)

        # Plot direction on top x axis
        self.ax.append(self.ax[-1].twiny())
        ax = self.ax[-1]
        ax.plot(direction, cell_depth, "ro-")
        ax.set_xlabel(self.canvas.tr("Velocity Direction (deg)"))
        ax.xaxis.label.set_color("red")
        ax.yaxis.label.set_fontsize(12)
        ax.xaxis.label.set_fontsize(12)

        self.fig.subplots_adjust(
            left=0.15, bottom=0.1, right=0.98, top=0.9, wspace=0.1, hspace=0
        )

        self.data_plotted = [
            {"type": "profile", "x": np.vstack((mag, direction)), "y": cell_depth}
        ]

        if vertical.ws_condition == "Ice":
            ice = vertical.ws_to_ice_bottom_m
            slush_bottom = vertical.ws_to_slush_bottom_m
            xmin, xmax = self.ax[-1].get_xlim()

            coords = [(xmin, 0), (xmin, ice), (xmax, ice), (xmax, 0), (xmin - 0.5, 0)]
            self.ax[-1].add_patch(
                Polygon(coords, hatch="xxx", edgecolor="k", facecolor="#b3ffff")
            )
            coords = [
                (xmin, ice),
                (xmin, slush_bottom),
                (xmax, slush_bottom),
                (xmax, ice),
                (xmin, ice),
            ]
            self.ax[-1].add_patch(
                Polygon(coords, hatch="....", edgecolor="k", facecolor="#e6ffff")
            )

        # Initialize annotation for data cursor
        self.annot.append(
            ax.annotate(
                "",
                xy=(0, 0),
                xytext=(-20, 20),
                textcoords="offset points",
                bbox=dict(boxstyle="round", fc="w"),
                arrowprops=dict(arrowstyle="->"),
            )
        )

        self.annot[-1].set_visible(False)

        self.canvas.draw()

    def extrap_graph(
        self,
        meas,
        units,
        idx,
        data_points=True,
        data_profiles=False,
        data_surface=False,
        mean_profile=True,
        fit=True,
        fit_auto=False,
    ):
        """Create extrapolation graph.t

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        units: dict
            Dictionary of units conversions
        idx: int
            Index for vertical to be plotted
        data_points: bool
            Indicates if data points should be plotted
        data_profiles: bool
            Indicates if the data points should be plotted as continuous line for e
            ach ensemble
        data_surface: bool
            Indicates if the cell nearest the surface should be highlighted
        mean_profile: bool
            Indicates if the mean profile should be plotted
        fit: bool
            Indicates if the currently selected fit methods should be plotted
        fit_auto: bool
            Indicates if the automatically selected fit methods should be plotted
        """

        # Plot data points
        if data_points:
            self.extrap_plot_data_points(meas.verticals[idx].ensemble_profiles)

        # Plot data profiles
        if data_profiles:
            self.extrap_plot_data_profiles(meas.verticals[idx].ensemble_profiles)

        # Highlight top cells
        if data_surface:
            self.extrap_plot_surface(meas.verticals[idx].ensemble_profiles)

        # Plot profile
        if mean_profile:
            self.extrap_plot_profile(meas.verticals[idx].mean_profile)

        # Plot fit
        if fit:
            self.extrap_plot_fit(
                meas.verticals[idx].extrapolation.extrap_fit.sel_fit,
                meas.verticals[idx].depth_m,
            )

        # Plot automatic fit
        if fit_auto:
            self.extrap_plot_fit_auto(
                meas.verticals[idx].extrapolation.extrap_fit.sel_fit,
                meas.verticals[idx].depth_m,
            )

        # Configure axis
        ax = self.ax[-1]
        ax.set_xlabel(self.canvas.tr("Normal Velocity ") + units["label_V"])
        ax.set_ylabel(self.canvas.tr("Depth ") + units["label_L"])
        ax.xaxis.label.set_fontsize(10)
        ax.yaxis.label.set_fontsize(10)
        ax.tick_params(
            axis="both", direction="in", bottom=True, top=True, left=True, right=True
        )
        ax.grid()
        for label in ax.get_xticklabels() + ax.get_yticklabels():
            label.set_fontsize(10)
        if np.isnan(meas.verticals[idx].depth_m):
            bottom = 0
        else:
            bottom = (meas.verticals[idx].depth_m * self.units["L"]) * 1.05
        ax.set_ylim(top=0, bottom=bottom)

        self.data_plotted = [{"type": "ts"}]
        self.x_axis_type = "L"
        ax.xaxis.set_major_locator(MaxNLocator(8))

        # Initialize annotation for data cursor
        self.annot.append(
            ax.annotate(
                "",
                xy=(0, 0),
                xytext=(-20, 20),
                textcoords="offset points",
                bbox=dict(boxstyle="round", fc="w"),
                arrowprops=dict(arrowstyle="->"),
            )
        )

        if meas.verticals[idx].ws_condition == "Ice":
            ice = meas.verticals[idx].ws_to_ice_bottom_m * self.units["L"]
            slush_bottom = meas.verticals[idx].ws_to_slush_bottom_m * self.units["L"]
            xmin, xmax = self.ax[-1].get_xlim()

            coords = [(xmin, 0), (xmin, ice), (xmax, ice), (xmax, 0), (xmin - 0.5, 0)]
            self.ax[-1].add_patch(
                Polygon(coords, hatch="xxx", edgecolor="k", facecolor="#b3ffff")
            )
            coords = [
                (xmin, ice),
                (xmin, slush_bottom),
                (xmax, slush_bottom),
                (xmax, ice),
                (xmin, ice),
            ]
            self.ax[-1].add_patch(
                Polygon(coords, hatch="....", edgecolor="k", facecolor="#e6ffff")
            )

    def extrap_plot_data_points(self, data):
        """Plot data for each depth cell as points.

        Parameters
        ----------
        data: dict
            Dictionary of ensemble_profiles
        """

        ax = self.ax[-1]

        ax.plot(
            data["speed"] * self.units["V"],
            data["cell_depth"] * self.units["L"],
            marker="o",
            color="#cecece",
            markerfacecolor="#cecece",
            linestyle="None",
            markersize=2,
        )

    def extrap_plot_data_profiles(self, data):
        """Plot data for each depth cell as a continuous line for each ensemble

        Parameters
        ----------
        data: dict
            Dictionary of ensemble_profiles
        """

        ax = self.ax[-1]

        for col in range(data["speed"].shape[1]):
            ax.plot(
                data["speed"][:, col] * self.units["V"],
                data["cell_depth"][:, col] * self.units["L"],
                color="#cecece",
                linestyle="-",
            )

    def extrap_plot_profile(self, data):
        """Plot data for each depth cell. These data will be either for a single transect
        or for the composite for the whole measurement.

        Parameters
        ----------
        data: dict
            Dictionary of mean_profile
        """

        ax = self.ax[-1]

        # Plot measured portion of profile
        ax.plot(
            data["speed"] * self.units["V"],
            data["cell_depth"] * self.units["L"],
            marker="s",
            color="k",
            markerfacecolor="k",
            linestyle="None",
        )

        # Plot top extrapolated portion of profile
        valid = np.where(np.logical_not(np.isnan(data["speed"])))[0]
        if len(valid) > 0:
            ax.plot(
                data["speed"][valid[0]] * self.units["V"],
                data["cell_depth"][valid[0]] * self.units["L"],
                marker="o",
                color="darkorange",
                markerfacecolor="darkorange",
                linestyle="None",
                markeredgecolor="k",
                markersize=9,
            )

            # Plot bottom extrapolated portion of profile
            ax.plot(
                data["speed"][valid[-1]] * self.units["V"],
                data["cell_depth"][valid[-1]] * self.units["L"],
                marker="o",
                color="darkorange",
                markerfacecolor="darkorange",
                linestyle="None",
                markeredgecolor="k",
                markersize=9,
            )

    def extrap_plot_fit(self, sel_fit, depth_m):
        """Plot fit line(s)

        Parameters
        ----------
        sel_fit: SelectFit
            Object of class SelectFit
        depth_m: float
            Mean depth of vertical
        """

        ax = self.ax[-1]

        y = depth_m - sel_fit.z
        ax.plot(
            sel_fit.u * self.units["V"], y * self.units["L"], color="k", linewidth=2
        )

    def extrap_plot_fit_auto(self, sel_fit, depth_m):
        """Plot fit line(s)

        Parameters
        ----------
        sel_fit: SelectFit
            Object of class SelectFit
        depth_m: float
            Mean depth of vertical
        """

        ax = self.ax[-1]

        y = depth_m - sel_fit.z_auto
        ax.plot(
            sel_fit.u_auto * self.units["V"], y * self.units["L"], "-g", linewidth=3
        )

    def extrap_plot_surface(self, data):
        """Highlights the depth cell data representing the topmost depth cell.

        Parameters
        ----------
        data: dict
            Dictionary of ensemble_profiles
        """

        ax = self.ax[-1]

        # Indentify indices for topmost depth cell
        surface_idx = np.argmax(np.logical_not(np.isnan(data["speed"])), 0)
        idx = (surface_idx, np.arange(len(surface_idx)))

        # Plot data
        ax.scatter(
            data["speed"][idx] * self.units["V"],
            data["cell_depth"][idx] * self.units["L"],
            marker="o",
            s=10,
            c="#00ffff",
        )

    def percent_q_bar(
        self, meas, units, color_dict, agency_options, selected=None, ax=None
    ):
        """Create color bar graph showing percent discharge in each vertical
         or subsection

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        units: dict
            Dictionary of units conversions
        color_dict: dict
            Dictionary of colors to use for Good, Caution, and Warning
        agency_options: dict
            Contains thresholds for coloring bars
        selected: int or None
            Vertical index
        ax: list
            List of subplots
        """

        with self.wait_cursor():
            self.units = units
            self.x_axis_type = "S"

            if ax is None:
                self.fig.clear()
                self.gs = gridspec.GridSpec(1, 1)

                # Create first subplot
                self.ax.append(self.fig.add_subplot(self.gs[self.fig_no]))
                ax = self.ax[-1]

                # Adjust the spacing of the subplots
                self.fig.subplots_adjust(
                    left=0.12,
                    bottom=0.25,
                    right=0.95,
                    top=0.95,
                    wspace=0.02,
                    hspace=0.08,
                )

            # Get data from summary data frame
            percent_q = meas.summary["Q%"].to_numpy()
            x_data = meas.summary["Station Number"].to_numpy()

            # Plot data with warning
            mask = percent_q > 8
            x = x_data[mask]
            y = percent_q[mask]
            if len(x) > 0:
                ax.bar(x, y, color=color_dict["Warning"])
            if selected is not None:
                try:
                    selected = meas.verticals_used_idx.index(selected)
                    if meas.discharge_method == "Mean":
                        selected = selected * 2 - 1
                    if mask[selected]:
                        ax.bar(
                            x_data[selected],
                            percent_q[selected],
                            color=color_dict["Warning"],
                            edgecolor="k",
                            linewidth=2,
                        )
                except ValueError:
                    pass

            # Plot data with caution
            mask = np.logical_and(
                agency_options["StationQ"]["warning"] >= percent_q,
                percent_q > agency_options["StationQ"]["caution"],
            )
            x = x_data[mask]
            y = percent_q[mask]
            if len(x) > 0:
                ax.bar(x, y, color=color_dict["Caution"])
            if selected is not None:
                if mask[selected]:
                    ax.bar(
                        x_data[selected],
                        percent_q[selected],
                        color=color_dict["Caution"],
                        edgecolor="k",
                        linewidth=2,
                    )

            # Plot good data
            mask = percent_q <= agency_options["StationQ"]["caution"]
            x = x_data[mask]
            y = percent_q[mask]
            if len(x) > 0:
                ax.bar(x, y, color=color_dict["Good"])
            if selected is not None:
                if mask[selected]:
                    ax.bar(
                        x_data[selected],
                        percent_q[selected],
                        color=color_dict["Good"],
                        edgecolor="k",
                        linewidth=2,
                    )

            # Label axes
            ax.yaxis.label.set_fontsize(12)
            ax.xaxis.label.set_fontsize(12)
            ax.set_xlabel(self.canvas.tr("Station Number"))
            ax.set_ylabel(self.canvas.tr("% Discharge"))
            ax.tick_params(
                axis="both",
                direction="in",
                bottom=True,
                top=True,
                left=True,
                right=True,
            )

            if meas.verticals[meas.verticals_used_idx[0]].edge_loc == "Right":
                ax.invert_xaxis()

            self.data_plotted.append({"type": "bar", "x": x_data, "y": percent_q})

            # Initialize annotation for data cursor.
            self.annot.append(
                ax.annotate(
                    "",
                    xy=(0, 0),
                    xytext=(-20, 20),
                    textcoords="offset points",
                    bbox=dict(boxstyle="round", fc="w"),
                    arrowprops=dict(arrowstyle="->"),
                )
            )

            self.annot[-1].set_visible(False)

            self.canvas.draw()

    def mean_speed_contour(self, meas, color_map="viridis", selected=None):
        """Creates water speed contour plot.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        color_map: str
            Specifies color map (viridis, jet)
        selected: int or None
            Index of selected vertical
        """

        with self.wait_cursor():
            # Adjust the spacing of the subplots
            self.fig.subplots_adjust(
                left=0.05, bottom=0.02, right=0.95, top=0.95, wspace=0.02, hspace=0.08
            )

            # Create arrays for data from each used vertical
            number_cells = []
            for idx in meas.verticals_used_idx:
                number_cells.append(meas.verticals[idx].mean_profile["speed"].shape[0])
            max_number_cells = max(number_cells)
            if max_number_cells > 0:
                number_verticals = len(meas.verticals_used_idx)
                speed = np.tile(np.nan, (max_number_cells, number_verticals))
                cell_depth = np.tile(np.nan, (max_number_cells, number_verticals))
                cell_size = np.tile(np.nan, (max_number_cells, number_verticals))
                depth = np.tile(np.nan, number_verticals)
                station = np.tile(np.nan, number_verticals)

                # Poplulate arrays
                col = -1
                for idx in meas.verticals_used_idx:
                    col += 1
                    speed[0 : number_cells[col], col] = meas.verticals[
                        idx
                    ].mean_profile["speed"]
                    cell_depth[0 : number_cells[col], col] = meas.verticals[
                        idx
                    ].mean_profile["cell_depth"]
                    cell_size[0 : number_cells[col], col] = meas.verticals[
                        idx
                    ].mean_profile["cell_size"]
                    depth[col] = meas.verticals[idx].depth_m
                    station[col] = meas.verticals[idx].location_m
                    # Fill in nan values, for proper plotting of last cell
                    mask = np.where(np.logical_not(np.isnan(cell_size[:, col])))[0]
                    if len(mask) > 0:
                        cell_size[np.isnan(cell_size[:, col]), col] = cell_size[
                            mask[-1], col
                        ]
                        cell_depth[np.isnan(cell_depth[:, col]), col] = (
                            depth[col] + 0.5 * cell_size[mask[-1], col]
                        )

                # Compute data for contour plot
                x_plt, cell_plt, data_plt, depth = self.contour_data_prep(
                    data=speed,
                    cell_depth=cell_depth,
                    cell_size=cell_size,
                    depth=depth,
                    x_1d=station,
                )
                if selected is None:
                    selected_vertical = None
                else:
                    try:
                        selected_vertical = meas.verticals_used_idx.index(selected)
                    except ValueError:
                        selected_vertical = meas.verticals_used_idx[1]

                # Plot the data
                self.plt_contour(
                    x_plt_in=x_plt * self.units["L"],
                    cell_plt_in=cell_plt,
                    data_plt_in=data_plt,
                    x=station * self.units["L"],
                    depth=depth,
                    selected=selected_vertical,
                    cmap_in=color_map,
                    data_units=(
                        self.units["V"],
                        "Normal Velocity " + self.units["label_V"],
                    ),
                )

                self.x = station
                self.x_axis_type = "L"

                n = -1
                x = station * self.units["L"]
                for idx in meas.verticals_used_idx:
                    n += 1
                    if meas.verticals[idx].ws_condition == "Ice":
                        ice = meas.verticals[idx].ws_to_ice_bottom_m * self.units["L"]
                        slush_bottom = (
                            meas.verticals[idx].ws_to_slush_bottom_m * self.units["L"]
                        )
                        if idx == meas.verticals_used_idx[0]:
                            try:
                                half_back = np.abs(0.5 * (x[n + 1] - x[n]))
                                half_forward = half_back
                            except IndexError:
                                half_back = x[0] - 0.5
                                half_forward = x[0] + 0.5
                        elif idx == meas.verticals_used_idx[-1]:
                            half_forward = np.abs(0.5 * (x[n] - x[n - 1]))
                            half_back = half_forward
                        else:
                            half_back = np.abs(0.5 * (x[n] + x[n - 1]))
                            half_forward = np.abs(0.5 * (x[n + 1] + x[n]))
                        coords = [
                            (half_back, 0),
                            (half_back, ice),
                            (half_forward, ice),
                            (half_forward, 0),
                            (half_back, 0),
                        ]
                        self.ax[-2].add_patch(
                            Polygon(
                                coords, hatch="xxx", edgecolor="k", facecolor="#b3ffff"
                            )
                        )
                        coords = [
                            (half_back, ice),
                            (half_back, slush_bottom),
                            (half_forward, slush_bottom),
                            (half_forward, ice),
                            (half_back, ice),
                        ]
                        self.ax[-2].add_patch(
                            Polygon(
                                coords, hatch="....", edgecolor="k", facecolor="#e6ffff"
                            )
                        )

    def speed_final_contour(self, vertical):
        """Creates water speed contour plot.

        Parameters
        ----------
        vertical: Vertical
            Object of class Vertical to be graphed
        """

        with self.wait_cursor():
            # Adjust the spacing of the subplots
            self.fig.subplots_adjust(
                left=0.05, bottom=0.02, right=0.95, top=0.95, wspace=0.02, hspace=0.08
            )

            # Create arrays for data from the vertical
            speed = vertical.ensemble_profiles["speed"]
            cell_depth = vertical.data.depths.bt_depths.depth_cell_depth_m
            cell_size = vertical.data.depths.bt_depths.depth_cell_size_m
            depths = getattr(vertical.data.depths, vertical.data.depths.selected)
            depth = depths.depth_processed_m
            x = np.array(list(range(depth.shape[0]))) + 1

            # Compute data for contour plot
            x_plt, cell_plt, data_plt, depth = self.contour_data_prep(
                data=speed,
                cell_depth=cell_depth,
                cell_size=cell_size,
                depth=depth,
                x_1d=x,
            )

            # Plot the data
            self.plt_contour(
                x_plt_in=x_plt,
                cell_plt_in=cell_plt,
                data_plt_in=data_plt,
                x=x,
                depth=depth,
                data_units=(
                    self.units["V"],
                    "Normal Velocity " + self.units["label_V"],
                ),
                topbot=False,
                sidelobe=True,
                depth_obj=depths,
            )

            self.x = x
            self.x_axis_type = "E"

            if vertical.ws_condition == "Ice":
                ice = vertical.ws_to_ice_bottom_m * self.units["L"]
                slush_bottom = vertical.ws_to_slush_bottom_m * self.units["L"]
                coords = [
                    (x[0] - 0.5, 0),
                    (x[0] - 0.5, ice),
                    (x[-1] + 0.5, ice),
                    (x[-1] + 0.5, 0),
                    (x[0] - 0.5, 0),
                ]
                self.ax[-2].add_patch(
                    Polygon(coords, hatch="xxx", edgecolor="k", facecolor="#b3ffff")
                )
                coords = [
                    (x[0] - 0.5, ice),
                    (x[0] - 0.5, slush_bottom),
                    (x[-1] + 0.5, slush_bottom),
                    (x[-1] + 0.5, ice),
                    (x[0] - 0.5, ice),
                ]
                self.ax[-2].add_patch(
                    Polygon(coords, hatch="....", edgecolor="k", facecolor="#e6ffff")
                )

    def speed_filtered_contour(self, vertical):
        """Create contour of water speed with no interpolation for invalid water data.

        Parameters
        ----------
        vertical: Vertical
            Object of class Vertical
        """

        # Compute water speed for each cell
        water_u = self.transect.w_vel.u_processed_mps[:, self.transect.in_transect_idx]
        water_v = self.transect.w_vel.v_processed_mps[:, self.transect.in_transect_idx]
        water_speed = np.sqrt(water_u**2 + water_v**2)
        water_speed[np.logical_not(self.transect.w_vel.valid_data[0, :, :])] = np.nan

        cell_depth = self.transect.depths.bt_depths.depth_cell_depth_m
        cell_size = self.transect.depths.bt_depths.depth_cell_size_m
        depths = getattr(self.transect.depths, self.transect.depths.selected)
        depth = depths.depth_processed_m
        x = np.array(list(range(depth.shape[0]))) + 1

        # Compute data for contour plot
        x_plt, cell_plt, data_plt, depth = self.contour_data_prep(
            data=water_speed,
            cell_depth=cell_depth,
            cell_size=cell_size,
            depth=depth,
            x_1d=x,
        )

        # Plot data
        self.plt_contour(
            x_plt_in=x_plt,
            cell_plt_in=cell_plt,
            data_plt_in=data_plt,
            x=self.x,
            depth=depth,
            data_units=(self.units["V"], "Filtered \n Speed " + self.units["label_V"]),
            topbot=False,
            sidelobe=True,
            depth_obj=depths,
        )

        if vertical.ws_condition == "Ice":
            ice = vertical.ws_to_ice_bottom_m * self.units["L"]
            slush_bottom = vertical.ws_to_slush_bottom_m * self.units["L"]
            coords = [
                (x[0] - 0.5, 0),
                (x[0] - 0.5, ice),
                (x[-1] + 0.5, ice),
                (x[-1] + 0.5, 0),
                (x[0] - 0.5, 0),
            ]
            self.ax[-2].add_patch(
                Polygon(coords, hatch="xxx", edgecolor="k", facecolor="#b3ffff")
            )
            coords = [
                (x[0] - 0.5, ice),
                (x[0] - 0.5, slush_bottom),
                (x[-1] + 0.5, slush_bottom),
                (x[-1] + 0.5, ice),
                (x[0] - 0.5, ice),
            ]
            self.ax[-2].add_patch(
                Polygon(coords, hatch="....", edgecolor="k", facecolor="#e6ffff")
            )

    def velocity_quiver(self, meas, ax=None):
        """Creates quiver plot of speed and velocity data.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        ax: list
            List of subplots
        """
        with self.wait_cursor():
            # Initialize subplots
            if ax is None:
                ax = self.ax[-1]

            # Create arrays for data from each used vertical
            number_cells = []
            for idx in meas.verticals_used_idx:
                number_cells.append(meas.verticals[idx].mean_profile["speed"].shape[0])
            number_verticals = len(meas.verticals_used_idx)
            speed = np.tile(np.nan, number_verticals)
            u = np.tile(np.nan, number_verticals)
            direction = np.tile(np.nan, number_verticals)
            mag = np.tile(np.nan, number_verticals)
            station = np.tile(np.nan, number_verticals)
            zero = np.tile(0, number_verticals)

            # Populate arrays
            col = -1
            for idx in meas.verticals_used_idx:
                col += 1
                speed[col] = meas.verticals[idx].mean_speed_mps
                mag[col] = meas.verticals[idx].mean_velocity_magnitude_mps
                direction[col] = meas.verticals[idx].mean_velocity_angle_deg
                station[col] = meas.verticals[idx].location_m
                u[col] = speed[col] * tand(meas.verticals[idx].mean_velocity_angle_deg)

            y_max = np.nanmax(speed * self.units["V"]) * 1.4
            y_min = np.nanmin(speed * self.units["V"])
            # scale = 0.20
            if y_min > 0:
                y_min = 0
            else:
                y_min = y_min * 1.4

            p1 = ax.quiver(
                station * self.units["L"],
                zero,
                zero,
                speed * self.units["V"],
                color="b",
                units="dots",
                width=2,
                scale_units="y",
                scale=1.0,
            )

            p1._init()
            assert isinstance(p1.scale, float)

            ax.quiver(
                station * self.units["L"],
                zero,
                u * self.units["V"],
                speed * self.units["V"],
                color="g",
                units="dots",
                width=2,
                scale_units="y",
                scale=p1.scale,
            )

            ax.legend([self.canvas.tr("Normal"), self.canvas.tr("Actual")])
            ax.grid(True)
            ax.yaxis.label.set_fontsize(12)
            ax.tick_params(
                axis="both",
                direction="in",
                bottom=True,
                top=True,
                left=True,
                right=True,
            )
            top = y_max
            if np.isnan(top):
                top = 0
            bottom = y_min
            if np.isnan(bottom):
                bottom = 0
            ax.set_ylim(top=top, bottom=bottom)

            ax.set_ylabel(self.canvas.tr("Velocity " + self.units["label_V"]))

            self.x = station
            self.x_axis_type = "L"

            self.data_plotted.append(
                {
                    "type": "vel",
                    "Station": station * self.units["L"],
                    "Normal": speed * self.units["V"],
                    "Actual": mag * self.units["V"],
                    "Angle": direction,
                }
            )

            # Initialize annotation for data cursor
            self.annot.append(
                ax.annotate(
                    "",
                    xy=(0, 0),
                    xytext=(-20, 20),
                    textcoords="offset points",
                    bbox=dict(boxstyle="round", fc="w"),
                    arrowprops=dict(arrowstyle="->"),
                )
            )

            self.annot[-1].set_visible(False)

            self.canvas.draw()

    def bt_speed_ts(self, lbl="BT Speed"):
        """Create time series plot of BT speed.

        Parameters
        ----------
        lbl: str
            Label for source of speed
        """

        # Prepare data
        data = np.sqrt(
            self.transect.boat_vel.bt_vel.u_processed_mps**2
            + self.transect.boat_vel.bt_vel.v_processed_mps**2
        )
        invalid = np.logical_not(self.transect.boat_vel.bt_vel.valid_data)
        data_invalid = np.sqrt(
            self.transect.boat_vel.bt_vel.u_mps**2
            + self.transect.boat_vel.bt_vel.v_mps**2
        )
        data_invalid[np.isnan(data_invalid)] = 0

        # Specify format
        fmt = [
            {"color": "r", "linestyle": "-"},
            {"color": "k", "linestyle": "", "marker": "$O$"},
            {"color": "k", "linestyle": "", "marker": "$E$"},
            {"color": "k", "linestyle": "", "marker": "$V$"},
            {"color": "k", "linestyle": "", "marker": "$S$"},
            {"color": "k", "linestyle": "", "marker": "$B$"},
        ]

        data_units = (self.units["V"], lbl + " " + self.units["label_V"])
        self.plt_timeseries(
            data=data,
            data_units=data_units,
            ax=self.ax[-1],
            data_2=data_invalid,
            data_mask=invalid,
            fmt=fmt,
        )

    def bt_3beam_ts(self):
        """Create time series plot of BT number of beams."""

        # Determine number of beams for each ensemble
        bt_temp = copy.deepcopy(self.transect.boat_vel.bt_vel)
        bt_temp.filter_beam(4)
        valid_4beam = bt_temp.valid_data[5, :].astype(int)
        data = np.copy(valid_4beam).astype(int)
        data[valid_4beam == 1] = 4
        data[valid_4beam == 0] = 3
        data[np.logical_not(self.transect.boat_vel.bt_vel.valid_data[1, :])] = 0

        # Configure plot settings
        invalid = np.logical_not(
            self.transect.boat_vel.bt_vel.valid_data[5, :]
        ).tolist()
        fmt = [
            {"color": "b", "linestyle": "", "marker": "."},
            {"color": "r", "linestyle": "", "marker": "o", "markerfacecolor": "none"},
        ]
        data_units = (1, "Number of Beams ")
        data_mask = [[], invalid]

        # Plot data
        self.plt_timeseries(
            data=data,
            data_units=data_units,
            ax=self.ax[-1],
            data_2=data,
            data_mask=data_mask,
            fmt=fmt,
        )
        self.ax[-1].set_ylim(top=4.5, bottom=-0.5)

    def bt_error_ts(self):
        """Create time series plot of BT error velocity."""

        # Plot error velocity
        y_data = self.transect.boat_vel.bt_vel.d_mps
        invalid = np.logical_not(
            self.transect.boat_vel.bt_vel.valid_data[2, :]
        ).tolist()
        data_units = (self.units["V"], "BT Error Vel " + self.units["label_V"])

        # Specify format
        freq_used = (
            np.unique(self.transect.boat_vel.bt_vel.frequency_khz)
            .astype(int)
            .astype(str)
        )
        freq_ensembles = self.transect.boat_vel.bt_vel.frequency_khz.astype(int).astype(
            str
        )

        # Create data mask
        data_mask = []
        fmt = []
        for freq in freq_used:
            data_mask.append(freq_ensembles == freq)
            fmt.append(
                {
                    "marker": self.freq_marker[freq],
                    "linestyle": "",
                    "mfc": self.freq_color[freq],
                    "mec": self.freq_color[freq],
                }
            )
        data_mask.append(invalid)
        fmt.append(
            {"marker": "o", "color": "r", "ms": 8, "linestyle": "", "mfc": "none"}
        )

        self.plt_timeseries(
            data=None,
            data_units=data_units,
            ax=self.ax[-1],
            data_2=y_data,
            data_mask=data_mask,
            fmt=fmt,
        )

        # Create legend
        legend_txt = []
        for freq in freq_used:
            legend_txt.append(self.bt_legend_dict[freq])
        self.ax[-1].legend(legend_txt)

    def bt_vertical_ts(self):
        """Create time series plot of bottom track vertical velocities."""

        y_data = self.transect.boat_vel.bt_vel.w_mps
        invalid = np.logical_not(
            self.transect.boat_vel.bt_vel.valid_data[3, :]
        ).tolist()
        data_units = (self.units["V"], "BT Vertical Vel " + self.units["label_V"])

        freq_used = (
            np.unique(self.transect.boat_vel.bt_vel.frequency_khz)
            .astype(int)
            .astype(str)
        )
        freq_ensembles = self.transect.boat_vel.bt_vel.frequency_khz.astype(int).astype(
            str
        )

        data_mask = []
        fmt = []
        for freq in freq_used:
            data_mask.append(freq_ensembles == freq)
            fmt.append(
                {
                    "marker": self.freq_marker[freq],
                    "linestyle": "",
                    "mfc": self.freq_color[freq],
                    "mec": self.freq_color[freq],
                }
            )
        data_mask.append(invalid)
        fmt.append(
            {"marker": "o", "color": "r", "ms": 8, "linestyle": "", "mfc": "none"}
        )

        self.plt_timeseries(
            data=None,
            data_units=data_units,
            ax=self.ax[-1],
            data_2=y_data,
            data_mask=data_mask,
            fmt=fmt,
        )

        # Create legend
        legend_txt = []
        for freq in freq_used:
            legend_txt.append(self.bt_legend_dict[freq])
        self.ax[-1].legend(legend_txt)

    def bt_source_ts(self):
        """Create time series plot of boat velocity source."""

        self.source_ts(self.transect.boat_vel.bt_vel, "BT Source")

    def bt_corr_ts(self):
        """Creates time series plot of bottom track correlation."""

        data = self.transect.boat_vel.bt_vel.corr
        data_units = (1, "Corr. (cnts)")

        # Compute max and min
        max_data = np.nanmax(np.nanmax(data))
        if np.isnan(max_data):
            max_data = 255
        min_data = np.nanmin(np.nanmin(data))
        if np.isnan(min_data):
            min_data = 0

        # Plot beam 1 using mask to identify invalid data
        fmt = [
            {
                "color": "k",
                "linestyle": "-",
                "marker": "o",
                "markersize": 4,
                "label": "B1",
            }
        ]
        self.plt_timeseries(
            data=data[0, :],
            data_units=data_units,
            ax=self.ax[-1],
            fmt=fmt,
            set_annot=True,
        )

        # Plot beam 2 using mask to identify invalid data
        fmt = [
            {
                "color": "#005500",
                "linestyle": "-",
                "marker": "o",
                "markersize": 4,
                "label": "B2",
            }
        ]
        self.plt_timeseries(
            data=data[1, :],
            data_units=data_units,
            ax=self.ax[-1],
            fmt=fmt,
            set_annot=False,
        )

        # Plot beam 3 using mask to identify invalid data
        fmt = [
            {
                "color": "b",
                "linestyle": "-",
                "marker": "o",
                "markersize": 4,
                "label": "B3",
            }
        ]
        self.plt_timeseries(
            data=data[2, :],
            data_units=data_units,
            ax=self.ax[-1],
            fmt=fmt,
            set_annot=False,
        )

        # Plot beam 4 using mask to identify invalid data
        fmt = [
            {
                "color": "#aa5500",
                "linestyle": "-",
                "marker": "o",
                "markersize": 4,
                "label": "B4",
            }
        ]
        self.plt_timeseries(
            data=data[3, :],
            data_units=data_units,
            ax=self.ax[-1],
            fmt=fmt,
            set_annot=False,
        )

        # Show legend
        self.ax[-1].legend()

        # Configure y axis
        self.ax[-1].set_ylim(
            top=np.ceil(max_data * 1.1), bottom=np.floor(min_data * 1.1)
        )

    def bt_rssi_ts(self):
        """Creates time series plot of bottom track intensity."""

        data = self.transect.boat_vel.bt_vel.rssi
        data_units = (1, "RSSI (cnts)")

        # Compute max and min
        max_data = np.nanmax(np.nanmax(data))
        if np.isnan(max_data):
            max_data = 255
        min_data = np.nanmin(np.nanmin(data))
        if np.isnan(min_data):
            min_data = 0

        # Plot beam 1 using mask to identify invalid data
        fmt = [
            {
                "color": "k",
                "linestyle": "-",
                "marker": "o",
                "markersize": 4,
                "label": "B1",
            }
        ]
        self.plt_timeseries(
            data=data[0, :],
            data_units=data_units,
            ax=self.ax[-1],
            fmt=fmt,
            set_annot=True,
        )

        # Plot beam 2 using mask to identify invalid data
        fmt = [
            {
                "color": "#005500",
                "linestyle": "-",
                "marker": "o",
                "markersize": 4,
                "label": "B2",
            }
        ]
        self.plt_timeseries(
            data=data[1, :],
            data_units=data_units,
            ax=self.ax[-1],
            fmt=fmt,
            set_annot=False,
        )

        # Plot beam 3 using mask to identify invalid data
        fmt = [
            {
                "color": "b",
                "linestyle": "-",
                "marker": "o",
                "markersize": 4,
                "label": "B3",
            }
        ]
        self.plt_timeseries(
            data=data[2, :],
            data_units=data_units,
            ax=self.ax[-1],
            fmt=fmt,
            set_annot=False,
        )

        # Plot beam 4 using mask to identify invalid data
        fmt = [
            {
                "color": "#aa5500",
                "linestyle": "-",
                "marker": "o",
                "markersize": 4,
                "label": "B4",
            }
        ]
        self.plt_timeseries(
            data=data[3, :],
            data_units=data_units,
            ax=self.ax[-1],
            fmt=fmt,
            set_annot=False,
        )

        # Show legend
        self.ax[-1].legend()

        # Configure y axis
        self.ax[-1].set_ylim(
            top=np.ceil(max_data * 1.1), bottom=np.floor(min_data * 1.1)
        )

    def wt_3beam_ts(self):
        """Create time series plot of WT beams used."""

        # Determine number of beams for each ensemble
        wt_temp = copy.deepcopy(self.transect.w_vel)
        wt_temp.filter_beam(4)
        valid_4beam = wt_temp.valid_data[5, :, :].astype(int)
        beam_data = np.copy(valid_4beam).astype(int)
        beam_data[valid_4beam == 1] = 4
        beam_data[wt_temp.valid_data[6, :, :]] = 4
        beam_data[valid_4beam == 0] = 3
        beam_data[np.logical_not(self.transect.w_vel.valid_data[1, :, :])] = -999

        # Configure plot settings
        hold_x = np.copy(self.x)
        self.x = np.tile(self.x, (self.transect.w_vel.valid_data[0, :, :].shape[0], 1))
        invalid = np.logical_and(
            np.logical_not(self.transect.w_vel.valid_data[5, :, :]),
            self.transect.w_vel.cells_above_sl,
        )
        fmt = [
            {"color": "b", "linestyle": "", "marker": "."},
            {"color": "r", "linestyle": "", "marker": "o", "markerfacecolor": "none"},
        ]
        data_units = (1, "WT Number of Beams ")
        data_mask = [[], invalid]

        # Plot data
        self.plt_timeseries(
            data=beam_data,
            data_units=data_units,
            ax=self.ax[-1],
            data_2=beam_data,
            data_mask=data_mask,
            fmt=fmt,
        )
        self.x = hold_x

        # Format axis
        self.ax[-1].set_ylim(top=4.5, bottom=-0.5)

    def wt_error_ts(self):
        """Create time series plot of WT error velocity."""

        # Plot error velocity
        invalid = np.logical_and(
            np.logical_not(self.transect.w_vel.valid_data[2, :, :]),
            self.transect.w_vel.cells_above_sl,
        )

        # Data to plot
        hold_x = np.copy(self.x)
        self.x = np.tile(self.x, (self.transect.w_vel.valid_data[0, :, :].shape[0], 1))
        self.x = self.x[self.transect.w_vel.cells_above_sl]
        y_data = self.transect.w_vel.d_mps[self.transect.w_vel.cells_above_sl]

        data_units = (self.units["V"], "WT Error Vel " + self.units["label_V"])

        # Setup ping type
        if self.transect.w_vel.ping_type.size > 1:
            ping_type = self.transect.w_vel.ping_type[
                self.transect.w_vel.cells_above_sl
            ]
            ping_type_used = np.unique(ping_type)

            p_types = np.unique(ping_type)

            data_mask = []
            fmt = []
            for pt in ping_type_used:
                data_mask.append(ping_type == pt)
                fmt.append(
                    {
                        "marker": self.p_type_marker[pt],
                        "linestyle": "",
                        "mfc": self.p_type_color[pt],
                        "mec": self.p_type_color[pt],
                    }
                )

            data_mask.append(invalid[self.transect.w_vel.cells_above_sl])
            fmt.append(
                {"marker": "o", "color": "r", "ms": 8, "linestyle": "", "mfc": "none"}
            )

            # Plot
            self.plt_timeseries(
                data=None,
                data_units=data_units,
                ax=self.ax[-1],
                data_2=y_data,
                data_mask=data_mask,
                fmt=fmt,
            )

        else:
            fmt = [
                {
                    "marker": ".",
                    "color": "b",
                    "ms": 8,
                    "linestyle": "",
                    "mfc": "b",
                    "mec": "b",
                },
                {"marker": "o", "color": "r", "ms": 8, "linestyle": "", "mfc": "none"},
            ]

            p_types = ["U"]
            data_mask = [[], invalid]

            # Plot first ping type
            self.plt_timeseries(
                data=y_data,
                data_units=data_units,
                ax=self.ax[-1],
                data_2=y_data,
                data_mask=data_mask,
                fmt=fmt,
            )
        self.x = hold_x

        # Create legend
        legend_txt = []
        for p_type in p_types:
            legend_txt.append(self.wt_legend_dict[p_type])
        self.ax[-1].legend(legend_txt)

    def wt_vertical_ts(self):
        """Create time series plot of WT vertical velocity."""

        # Plot vertical velocity
        invalid = np.logical_and(
            np.logical_not(self.transect.w_vel.valid_data[3, :, :]),
            self.transect.w_vel.cells_above_sl,
        )

        # Data to plot
        hold_x = np.copy(self.x)
        self.x = np.tile(self.x, (self.transect.w_vel.valid_data[0, :, :].shape[0], 1))
        self.x = self.x[self.transect.w_vel.cells_above_sl]
        y_data = self.transect.w_vel.w_mps[self.transect.w_vel.cells_above_sl]

        data_units = (self.units["V"], "WT Vert. Vel " + self.units["label_V"])

        # Setup ping type
        if self.transect.w_vel.ping_type.size > 1:
            ping_type = self.transect.w_vel.ping_type[
                self.transect.w_vel.cells_above_sl
            ]
            ping_type_used = np.unique(ping_type)

            p_types = np.unique(ping_type)

            data_mask = []
            fmt = []
            for pt in ping_type_used:
                data_mask.append(ping_type == pt)
                fmt.append(
                    {
                        "marker": self.p_type_marker[pt],
                        "linestyle": "",
                        "mfc": self.p_type_color[pt],
                        "mec": self.p_type_color[pt],
                    }
                )

            data_mask.append(invalid[self.transect.w_vel.cells_above_sl])
            fmt.append(
                {"marker": "o", "color": "r", "ms": 8, "linestyle": "", "mfc": "none"}
            )

            # Plot
            self.plt_timeseries(
                data=None,
                data_units=data_units,
                ax=self.ax[-1],
                data_2=y_data,
                data_mask=data_mask,
                fmt=fmt,
            )

        else:
            fmt = [
                {
                    "marker": ".",
                    "color": "b",
                    "ms": 8,
                    "linestyle": "",
                    "mfc": "b",
                    "mec": "b",
                },
                {"marker": "o", "color": "r", "ms": 8, "linestyle": "", "mfc": "none"},
            ]
            p_types = ["U"]
            data_mask = [[], invalid]

            # Plot first ping type
            self.plt_timeseries(
                data=y_data,
                data_units=data_units,
                ax=self.ax[-1],
                data_2=y_data,
                data_mask=data_mask,
                fmt=fmt,
            )
        self.x = hold_x

        # Create legend
        legend_txt = []
        for p_type in p_types:
            legend_txt.append(self.wt_legend_dict[p_type])
        self.ax[-1].legend(legend_txt)

    def wt_snr_ts(self):
        """Create time series plot of WT SNR range."""

        # Plot vertical velocity
        invalid = np.logical_and(
            np.logical_not(self.transect.w_vel.valid_data[7, :, :]),
            self.transect.w_vel.cells_above_sl,
        )[0, :]

        # Data to plot
        y_data = self.transect.w_vel.snr_rng
        data_units = (1, "WT SNR Range (dB)")

        # Setup ping type
        if self.transect.w_vel.ping_type.size > 1:
            ping_type = self.transect.w_vel.ping_type[0, :]
            ping_type_used = np.unique(ping_type)

            p_types = np.unique(ping_type)

            data_mask = []
            fmt = []
            for pt in ping_type_used:
                data_mask.append(ping_type == pt)
                fmt.append(
                    {
                        "marker": self.p_type_marker[pt],
                        "linestyle": "",
                        "mfc": self.p_type_color[pt],
                        "mec": self.p_type_color[pt],
                    }
                )

            data_mask.append(invalid)
            fmt.append(
                {"marker": "o", "color": "r", "ms": 8, "linestyle": "", "mfc": "none"}
            )

            # Plot
            self.plt_timeseries(
                data=None,
                data_units=data_units,
                ax=self.ax[-1],
                data_2=y_data,
                data_mask=data_mask,
                fmt=fmt,
            )

        else:
            fmt = [
                {
                    "marker": ".",
                    "color": "b",
                    "ms": 8,
                    "linestyle": "",
                    "mfc": "b",
                    "mec": "b",
                },
                {"marker": "o", "color": "r", "ms": 8, "linestyle": "", "mfc": "none"},
            ]
            p_types = ["U"]
            data_mask = [[], invalid]

            # Plot first ping type
            self.plt_timeseries(
                data=y_data,
                data_units=data_units,
                ax=self.ax[-1],
                data_2=y_data,
                data_mask=data_mask,
                fmt=fmt,
            )

        # Create legend
        legend_txt = []
        for p_type in p_types:
            legend_txt.append(self.wt_legend_dict[p_type])
        self.ax[-1].legend(legend_txt)

    def wt_avg_speed_ts(self):
        """Create average water speed time series plot."""

        # Compute mean water speed for each ensemble using a weighted average based on
        # depth cell size
        water_u = self.transect.w_vel.u_processed_mps[:, self.transect.in_transect_idx]
        water_v = self.transect.w_vel.v_processed_mps[:, self.transect.in_transect_idx]
        depth_selected = getattr(self.transect.depths, self.transect.depths.selected)

        weight = depth_selected.depth_cell_size_m[:, self.transect.in_transect_idx]
        weight[np.isnan(water_u)] = np.nan

        mean_u = np.nansum(water_u * weight, axis=0) / np.nansum(weight, axis=0)
        mean_v = np.nansum(water_v * weight, axis=0) / np.nansum(weight, axis=0)
        avg_speed = np.sqrt(mean_u**2 + mean_v**2)

        # Plot data
        data_units = (self.units["V"], "Water speed " + self.units["label_V"])
        self.plt_timeseries(data=avg_speed, data_units=data_units, ax=self.ax[-1])

    def source_ts(self, selected, axis_label):
        """Plot source of data.

        Parameters
        ----------
        selected: BoatData
            Boat velocity reference data
        axis_label: str
            Label for y axis
        """

        # Handle situation where transect does not contain the selected source
        if selected is None:
            source = np.tile("None", len(self.x))
            source = source.astype(object)
        else:
            source = selected.processed_source

        # Plot dummy data to establish consistent order of y axis
        temp_hold = np.copy(self.x)
        if isinstance(temp_hold[0], datetime):
            dummy_time = temp_hold[0] - timedelta(days=1)
            self.x = [dummy_time, dummy_time, dummy_time, dummy_time, dummy_time]
        else:
            self.x = [-10, -10, -10, -10]
        data = ["None", "INV", "INT", "BT"]
        fmt = [{"color": "w", "linestyle": "-"}]
        data_units = (1, "")
        self.plt_timeseries(data=data, data_units=data_units, ax=self.ax[-1], fmt=fmt)

        # Plot data
        self.x = np.copy(temp_hold)
        data_units = (1, axis_label)
        fmt = [{"color": "b", "linestyle": "", "marker": "."}]
        self.plt_timeseries(data=source, data_units=data_units, ax=self.ax[-1], fmt=fmt)

        # Format y axis
        self.ax[-1].set_yticks(["None", "INV", "INT", "BT"])

    def gga_speed_ts(self, lbl="GGA Speed"):
        """Plot boat speed using GGA reference."""
        if (
            self.transect.boat_vel.gga_vel is not None
            and self.transect.boat_vel.gga_vel.u_mps is not None
        ):
            # Compute speed from processed GGA data
            data = np.sqrt(
                self.transect.boat_vel.gga_vel.u_processed_mps**2
                + self.transect.boat_vel.gga_vel.v_processed_mps**2
            )

            # Create data mask for invalid GGA data
            invalid = np.logical_not(self.transect.boat_vel.gga_vel.valid_data)
            data_invalid = np.sqrt(
                self.transect.boat_vel.gga_vel.u_mps**2
                + self.transect.boat_vel.gga_vel.v_mps**2
            )
            data_invalid[np.isnan(data_invalid)] = 0

            # Format for data and invalid identification
            fmt = [
                {"color": "b", "linestyle": "-"},
                {"color": "k", "linestyle": "", "marker": "$O$"},
                {"color": "k", "linestyle": "", "marker": "$Q$"},
                {"color": "k", "linestyle": "", "marker": "$A$"},
                {"color": "k", "linestyle": "", "marker": "$S$"},
                {"color": "k", "linestyle": "", "marker": "$H$"},
            ]

            data_units = (self.units["V"], lbl + " " + self.units["label_V"])

            # Plot data
            self.plt_timeseries(
                data=data,
                data_units=data_units,
                ax=self.ax[-1],
                data_2=data_invalid,
                data_mask=invalid,
                fmt=fmt,
            )

    def vtg_speed_ts(self, lbl="VTG Speed"):
        """Plot boat speed using VTG reference."""
        if (
            self.transect.boat_vel.vtg_vel is not None
            and self.transect.boat_vel.vtg_vel.u_mps is not None
        ):
            # Compute speed from processed VTG data
            data = np.sqrt(
                self.transect.boat_vel.vtg_vel.u_processed_mps**2
                + self.transect.boat_vel.vtg_vel.v_processed_mps**2
            )

            # Create data mask for invalid VTG data
            invalid = np.logical_not(self.transect.boat_vel.vtg_vel.valid_data)
            data_mask = [[], invalid[1], invalid[4], invalid[5]]

            # Use original unprocessed speed to plot invalid symbols
            data_invalid = np.sqrt(
                self.transect.boat_vel.vtg_vel.u_mps**2
                + self.transect.boat_vel.vtg_vel.v_mps**2
            )

            # Format for data and invalid identification
            fmt = [
                {"color": "g", "linestyle": "-"},
                {"color": "k", "linestyle": "", "marker": "$O$"},
                {"color": "k", "linestyle": "", "marker": "$S$"},
                {"color": "k", "linestyle": "", "marker": "$H$"},
            ]
            data_units = (self.units["V"], lbl + " " + self.units["label_V"])

            # Plot data
            self.plt_timeseries(
                data=data,
                data_units=data_units,
                ax=self.ax[-1],
                data_2=data_invalid,
                data_mask=data_mask,
                fmt=fmt,
            )

    def other_ts(self, data, data_color="r-"):
        """Create plot of other smooth filter.

        Parameters
        ----------
        data: BoatData
            Object of boat data to be plotted
        data_color: str
            Color of line containing original data
        """

        if data is not None and data.u_mps is not None:
            # Plot smooth
            speed = np.sqrt(data.u_mps**2 + data.v_mps**2)
            invalid_other_vel = np.logical_not(data.valid_data[4, :])
            if data.smooth_filter == "On":
                self.ax[-1].plot(
                    self.x, data.smooth_lower_limit * self.units["V"], color="#d5dce6"
                )
                self.ax[-1].plot(
                    self.x, data.smooth_upper_limit * self.units["V"], color="#d5dce6"
                )
                self.ax[-1].fill_between(
                    self.x,
                    data.smooth_lower_limit * self.units["V"],
                    data.smooth_upper_limit * self.units["V"],
                    facecolor="#d5dce6",
                )

                self.ax[-1].plot(self.x, speed * self.units["V"], data_color)
                self.ax[-1].plot(self.x, data.smooth_speed * self.units["V"])
                self.ax[-1].plot(
                    self.x[invalid_other_vel],
                    speed[invalid_other_vel] * self.units["V"],
                    "ko",
                    linestyle="",
                )
            else:
                self.ax[-1].plot(self.x, speed * self.units["V"], data_color)
            self.ax[-1].set_ylabel(self.canvas.tr("Speed " + self.units["label_V"]))

    def depths_beam_ts(
        self, b1=True, b2=True, b3=True, b4=True, vb=True, ds=True, leg=True
    ):
        """Plot available beam depths including depth sounder on single plot.

        Parameters
        ----------
        b1: bool
            Indicates if beam 1 depths are plotted
        b2: bool
            Indicates if beam 2 depths are plotted
        b3: bool
            Indicates if beam 3 depths are plotted
        b4: bool
            Indicates if beam 4 depths are plotted
        vb: bool
            Indicates if vertical beam depths are plotted
        ds: bool
            Indicates if depth sounder depths are plotted
        leg: bool
            Indicates is a legend should be shown
        """

        # Slant beams
        invalid_beams = np.logical_not(
            self.transect.depths.bt_depths.valid_beams
        ).tolist()
        beam_depths = self.transect.depths.bt_depths.depth_beams_m

        # Compute max depth from slant beams
        max_depth = [np.nanmax(np.nanmax(beam_depths))]

        # Plot beam 1 using mask to identify invalid data
        if b1:
            data_mask = [[], invalid_beams[0]]
            data_units = (self.units["L"], "Depth " + self.units["label_L"])
            fmt = [
                {
                    "color": "k",
                    "linestyle": "-",
                    "marker": "o",
                    "markersize": 4,
                    "label": "B1",
                },
                {
                    "color": "r",
                    "linestyle": "",
                    "marker": "o",
                    "markersize": 8,
                    "markerfacecolor": "none",
                    "label": None,
                },
            ]
            self.plt_timeseries(
                data=beam_depths[0, :],
                data_units=data_units,
                data_mask=data_mask,
                ax=self.ax[-1],
                fmt=fmt,
                set_annot=True,
            )

        # Plot beam 2 using mask to identify invalid data
        if b2:
            data_mask = [[], invalid_beams[1]]
            data_units = (self.units["L"], "Depth " + self.units["label_L"])
            fmt = [
                {
                    "color": "#005500",
                    "linestyle": "-",
                    "marker": "o",
                    "markersize": 4,
                    "label": "B2",
                },
                {
                    "color": "r",
                    "linestyle": "",
                    "marker": "o",
                    "markersize": 8,
                    "markerfacecolor": "none",
                    "label": None,
                },
            ]
            self.plt_timeseries(
                data=beam_depths[1, :],
                data_units=data_units,
                data_mask=data_mask,
                ax=self.ax[-1],
                fmt=fmt,
                set_annot=False,
            )

        # Plot beam 3 using mask to identify invalid data
        if b3:
            data_mask = [[], invalid_beams[2]]
            data_units = (self.units["L"], "Depth " + self.units["label_L"])
            fmt = [
                {
                    "color": "b",
                    "linestyle": "-",
                    "marker": "o",
                    "markersize": 4,
                    "label": "B3",
                },
                {
                    "color": "r",
                    "linestyle": "",
                    "marker": "o",
                    "markersize": 8,
                    "markerfacecolor": "none",
                    "label": None,
                },
            ]
            self.plt_timeseries(
                data=beam_depths[2, :],
                data_units=data_units,
                data_mask=data_mask,
                ax=self.ax[-1],
                fmt=fmt,
                set_annot=False,
            )

        # Plot beam 4 using mask to identify invalid data
        if b4:
            data_mask = [[], invalid_beams[3]]
            data_units = (self.units["L"], "Depth " + self.units["label_L"])
            fmt = [
                {
                    "color": "#aa5500",
                    "linestyle": "-",
                    "marker": "o",
                    "markersize": 4,
                    "label": "B4",
                },
                {
                    "color": "r",
                    "linestyle": "",
                    "marker": "o",
                    "markersize": 8,
                    "markerfacecolor": "none",
                    "label": None,
                },
            ]
            self.plt_timeseries(
                data=beam_depths[3, :],
                data_units=data_units,
                data_mask=data_mask,
                ax=self.ax[-1],
                fmt=fmt,
                set_annot=False,
            )

        # Plot vertical beam, if available
        if vb:
            if self.transect.depths.vb_depths is not None:
                invalid_beams = np.logical_not(
                    self.transect.depths.vb_depths.valid_beams[0, :]
                ).tolist()
                beam_depths = self.transect.depths.vb_depths.depth_beams_m[0, :]
                data_mask = [[], invalid_beams]
                data_units = (self.units["L"], "Depth " + self.units["label_L"])
                fmt = [
                    {
                        "color": "#aa00ff",
                        "linestyle": "-",
                        "marker": "o",
                        "markersize": 4,
                        "label": "VB",
                    },
                    {
                        "color": "r",
                        "linestyle": "",
                        "marker": "o",
                        "markersize": 8,
                        "markerfacecolor": "none",
                        "label": None,
                    },
                ]
                self.plt_timeseries(
                    data=beam_depths,
                    data_units=data_units,
                    data_mask=data_mask,
                    ax=self.ax[-1],
                    fmt=fmt,
                    set_annot=False,
                )
                # Add max depth from vertical beam to list
                max_depth.append(np.nanmax(beam_depths))

        # Plot depth sounder data, if available
        if ds:
            if self.transect.depths.ds_depths is not None:
                invalid_beams = np.logical_not(
                    self.transect.depths.ds_depths.valid_beams[0, :]
                )
                beam_depths = self.transect.depths.ds_depths.depth_beams_m[0, :]
                data_mask = [[], invalid_beams]
                data_units = (self.units["L"], "Depth " + self.units["label_L"])
                fmt = [
                    {
                        "color": "#00aaff",
                        "linestyle": "-",
                        "marker": "o",
                        "markersize": 4,
                        "label": "DS",
                    },
                    {
                        "color": "r",
                        "linestyle": "",
                        "marker": "o",
                        "markersize": 8,
                        "markerfacecolor": "none",
                        "label": None,
                    },
                ]
                self.plt_timeseries(
                    data=beam_depths,
                    data_units=data_units,
                    data_mask=data_mask,
                    ax=self.ax[-1],
                    fmt=fmt,
                    set_annot=False,
                )
                # Add max depth from depth sounder to list
                max_depth.append(np.nanmax(beam_depths))

        if leg:
            # Show legend
            self.ax[-1].legend()

    def depths_final_ts(
        self, avg4_final=False, vb_final=False, ds_final=False, final=True
    ):
        """Plot final cross section used to compute discharge.

        Parameters
        ----------
        avg4_final: bool
            Indicates if 4 beam avg cross section should be plotted
        vb_final: bool
            Indicates if vertical beam cross section should be plotted
        ds_final: bool
            Indicates if depth sounder cross section should be plotted
        final: bool
            Indicates if the selected cross section should be plotted
        """

        data_units = (self.units["L"], "Depth " + self.units["label_L"])

        # Selected cross section
        if final:
            # Get selected depth
            depth_selected = getattr(
                self.transect.depths, self.transect.depths.selected
            )
            beam_depths = depth_selected.depth_processed_m

            # Plot processed depth
            fmt = [{"color": "k", "linestyle": "-", "marker": "o", "markersize": 4}]
            self.plt_timeseries(
                data=beam_depths,
                data_units=data_units,
                ax=self.ax[-1],
                fmt=fmt,
                set_ylim=False,
            )

        # 4 beam avg cross section
        if avg4_final:
            beam_depths = self.transect.depths.bt_depths.depth_processed_m
            fmt = [{"color": "r", "linestyle": "-", "marker": "o", "markersize": 4}]
            self.plt_timeseries(
                data=beam_depths,
                data_units=data_units,
                ax=self.ax[-1],
                fmt=fmt,
                set_ylim=False,
            )

        # Vertical beam cross section
        if vb_final:
            beam_depths = self.transect.depths.vb_depths.depth_processed_m
            fmt = [
                {"color": "#aa00ff", "linestyle": "-", "marker": "o", "markersize": 4}
            ]
            self.plt_timeseries(
                data=beam_depths,
                data_units=data_units,
                ax=self.ax[-1],
                fmt=fmt,
                set_ylim=False,
            )

        # Depth sounder cross section
        if ds_final:
            beam_depths = self.transect.depths.ds_depths.depth_processed_m
            fmt = [
                {"color": "#00aaff", "linestyle": "-", "marker": "o", "markersize": 4}
            ]
            self.plt_timeseries(
                data=beam_depths,
                data_units=data_units,
                ax=self.ax[-1],
                fmt=fmt,
                set_ylim=False,
            )

    def depths_cross_section(self, meas, vertical_idx):
        """Plot cross section using depth from all used verticals.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        vertical_idx: int
            Index of vertical selected
        """

        # Set data units
        data_units = (self.units["L"], "Depth " + self.units["label_L"])

        # Get data to plot
        x = meas.summary["Location"].to_numpy()
        y = meas.summary["Depth"].to_numpy()
        self.x = x[:-2] * self.units["L"]
        y = y[:-2]

        # Plot data
        fmt = [{"color": "k", "linestyle": "-", "marker": "o", "markersize": 4}]
        self.plt_timeseries(data=y, data_units=data_units, ax=self.ax[-1], fmt=fmt)
        self.ax[-1].invert_yaxis()
        self.ax[-1].plot(
            meas.verticals[vertical_idx].location_m * self.units["L"],
            meas.verticals[vertical_idx].depth_m * self.units["L"],
            marker="o",
            color="k",
            ms=8,
            linestyle="",
            mfc="none",
        )

        self.ax[-1].set_xlabel(self.canvas.tr("Location ") + self.units["label_L"])

    def depths_source_ts(self):
        """Plot source of depth for final cross section."""

        # Use selected depth source
        depth_selected = getattr(self.transect.depths, self.transect.depths.selected)
        source = depth_selected.depth_source_ens

        # Plot dummy data to establish consistent order of y axis
        # self.x is passed through reference to self so it must be temporarily changed for
        # the dummy data
        temp_hold = np.copy(self.x)
        if isinstance(temp_hold[0], datetime):
            dummy_time = temp_hold[0] - timedelta(days=1)
            self.x = [dummy_time, dummy_time, dummy_time, dummy_time, dummy_time]
        else:
            self.x = [-10, -10, -10, -10, -10]
        data = ["INV", "INT", "BT", "VB", "DS"]
        fmt = [{"color": "w", "linestyle": "-"}]
        data_units = (1, "")
        self.plt_timeseries(data=data, data_units=data_units, ax=self.ax[-1], fmt=fmt)

        # Plot source data
        # Restore self.x to original values
        self.x = np.copy(temp_hold)
        data_units = (1, "Depth Source")
        fmt = [{"color": "b", "linestyle": "", "marker": "."}]
        self.plt_timeseries(data=source, data_units=data_units, ax=self.ax[-1], fmt=fmt)
        self.ax[-1].set_yticks(["INV", "INT", "BT", "VB", "DS"])

    def uncertainty_plot(self, uncertainty):
        """Generates the lollipop plot.

        Parameters
        ----------
        uncertainty: Uncertainty
            Object of class Uncertainty
        """
        self.fig.clear()

        # Configure axis
        self.fig.ax = self.fig.add_subplot(1, 1, 1)

        # Set margins and padding for figure
        self.fig.subplots_adjust(
            left=0.25, bottom=0.15, right=0.98, top=0.92, wspace=0.05, hspace=0
        )

        # Get uncertainty value
        if uncertainty.method == "IVE" or uncertainty.method == "IVE_User":
            if uncertainty.method == "IVE":
                total95 = uncertainty.u_ive["u95_q"] * 100
                self.plot_df = pd.DataFrame.from_dict(
                    uncertainty.u_ive_contribution, orient="index"
                )
            else:
                total95 = uncertainty.u_user["u95_q"] * 100
                self.plot_df = pd.DataFrame.from_dict(
                    uncertainty.u_user_contribution, orient="index"
                )
            self.plot_df = self.plot_df.mul(100)
            self.plot_df.columns = ["Percent"]
            self.plot_df = self.plot_df.rename(
                index={
                    "u_d": "Depth",
                    "u_v": "Velocity",
                    "u_b": "Width",
                    "u_s": "Systematic",
                }
            )
        elif uncertainty.method == "ISO" or uncertainty.method == "ISO_User":
            if uncertainty.method == "ISO":
                total95 = uncertainty.u_iso["u95_q"] * 100
                self.plot_df = pd.DataFrame.from_dict(
                    uncertainty.u_iso_contribution, orient="index"
                )
            else:
                total95 = uncertainty.u_user["u95_q"] * 100
                self.plot_df = pd.DataFrame.from_dict(
                    uncertainty.u_user_contribution, orient="index"
                )
            self.plot_df = self.plot_df.mul(100)
            self.plot_df.columns = ["Percent"]
            self.plot_df = self.plot_df.rename(
                index={
                    "u_s": "Systematic",
                    "u_c": "Instrument",
                    "u_d": "Depth",
                    "u_v": "Velocity",
                    "u_b": "Width",
                    "u_m": "# Stations",
                    "u_p": "# Cells",
                }
            )
        else:
            total95 = np.nan

        # Configure plot dataframe
        self.plot_df = self.plot_df.sort_values(by="Percent")

        # Generate plot
        self.fig.ax.hlines(y=self.plot_df.index, xmin=0, xmax=self.plot_df["Percent"])
        self.fig.ax.plot(
            self.plot_df["Percent"], self.plot_df.index, "o", markersize=11
        )
        self.fig.ax.set_xlabel(self.canvas.tr("Percent of Total"))
        self.fig.ax.xaxis.label.set_fontsize(12)
        self.fig.ax.tick_params(
            axis="both", direction="in", which="major", labelsize=12
        )
        if np.isnan(total95):
            self.fig.ax.set_title(
                self.canvas.tr("95% Total Uncertainty: N/A"), fontsize="16"
            )
        else:
            self.fig.ax.set_title(
                self.canvas.tr(uncertainty.method + " 95% Total Uncertainty: ")
                + "%5.1f" % total95,
                fontsize="14",
            )

        # Setup annotation features
        self.annot.append(
            self.fig.ax.annotate(
                "",
                xy=(0, 0),
                xytext=(-20, 20),
                textcoords="offset points",
                bbox=dict(boxstyle="round", fc="w"),
                arrowprops=dict(arrowstyle="->"),
            )
        )

        self.annot[-1].set_visible(False)

        self.canvas.draw()

    def compute_x_axis(self):
        """Compute x axis data."""

        # Initialize x
        x = None

        # x axis is length
        if self.x_axis_type == "L":
            boat_track = self.transect.boat_vel.compute_boat_track(
                transect=self.transect
            )
            if not np.alltrue(np.isnan(boat_track["track_x_m"])):
                x = boat_track["distance_m"] * self.units["L"]
            self.x = x[self.transect.in_transect_idx]

        # x axis is ensembles
        elif self.x_axis_type == "E":
            x = np.arange(1, len(self.transect.depths.bt_depths.depth_processed_m) + 1)
            self.x = x[self.transect.in_transect_idx]

        # x axis is time
        elif self.x_axis_type == "T":
            timestamp = (
                np.nancumsum(self.transect.date_time.ens_duration_sec)
                + self.transect.date_time.start_serial_time
            )
            x = np.copy(timestamp)
            # Timestamp is needed to create contour plots and  setting axis limits
            self.x_timestamp = x[self.transect.in_transect_idx]
            x = []
            # datetime is needed to plot timeseries and x-axis labels
            for stamp in timestamp:
                x.append(datetime.utcfromtimestamp(stamp))
            x = np.array(x)
            self.x = x[self.transect.in_transect_idx]

    @staticmethod
    def contour_data_prep(data, cell_depth, cell_size, depth, x_1d=None):
        """Modifies the selected data from transect into arrays matching the
        meshgrid format for creating contour or color plots.

        Parameters
        ----------
        data: np.array(float)
            Contour data
        cell_depth: np.array(float)
            Depth to center of each depth cell
        cell_size: np.array(float)
            Size of each depth cells
        depth: np.array(float)
            Depth for each vertical
        x_1d: np.array
            Array of x-coordinates for each ensemble

        Returns
        -------
        x_plt: np.array
            Data in meshgrid format used for the contour x variable
        cell_plt: np.array
            Data in meshgrid format used for the contour y variable
        data_plt: np.array
            Data in meshgrid format used to determine colors in plot
        depth: np.array
            Depth data used to plot the cross section bottom
        """

        # Prep water speed to use -999 instead of nans
        data_2_plot = np.copy(data)
        data_2_plot[np.isnan(data_2_plot)] = -999

        # Create x for contour plot
        x = np.tile(x_1d, (cell_size.shape[0], 1))
        n_ensembles = x.shape[1]

        # Prep data in x direction
        j = -1
        x_xpand = np.tile(np.nan, (cell_size.shape[0], 2 * cell_size.shape[1]))
        cell_depth_xpand = np.tile(np.nan, (cell_size.shape[0], 2 * cell_size.shape[1]))
        cell_size_xpand = np.tile(np.nan, (cell_size.shape[0], 2 * cell_size.shape[1]))
        data_xpand = np.tile(np.nan, (cell_size.shape[0], 2 * cell_size.shape[1]))
        depth_xpand = np.array([np.nan] * (2 * cell_size.shape[1]))

        # Center ensembles in grid
        for n in range(n_ensembles):
            if n == 0:
                try:
                    half_back = np.abs(0.5 * (x[:, n + 1] - x[:, n]))
                    half_forward = half_back
                except IndexError:
                    half_back = x[:, 0] - 0.5
                    half_forward = x[:, 0] + 0.5
            elif n == n_ensembles - 1:
                half_forward = np.abs(0.5 * (x[:, n] - x[:, n - 1]))
                half_back = half_forward
            else:
                half_back = np.abs(0.5 * (x[:, n] - x[:, n - 1]))
                half_forward = np.abs(0.5 * (x[:, n + 1] - x[:, n]))
            j += 1
            x_xpand[:, j] = x[:, n] - half_back
            cell_depth_xpand[:, j] = cell_depth[:, n]
            data_xpand[:, j] = data_2_plot[:, n]
            cell_size_xpand[:, j] = cell_size[:, n]
            depth_xpand[j] = depth[n]
            j += 1
            x_xpand[:, j] = x[:, n] + half_forward
            cell_depth_xpand[:, j] = cell_depth[:, n]
            data_xpand[:, j] = data_2_plot[:, n]
            cell_size_xpand[:, j] = cell_size[:, n]
            depth_xpand[j] = depth[n]

        # Create plotting mesh grid
        n_cells = x.shape[0]
        j = -1
        x_plt = np.tile(np.nan, (2 * cell_size.shape[0], 2 * cell_size.shape[1]))
        data_plt = np.tile(np.nan, (2 * cell_size.shape[0], 2 * cell_size.shape[1]))
        cell_plt = np.tile(np.nan, (2 * cell_size.shape[0], 2 * cell_size.shape[1]))
        for n in range(n_cells):
            j += 1
            x_plt[j, :] = x_xpand[n, :]
            cell_plt[j, :] = cell_depth_xpand[n, :] - 0.5 * cell_size_xpand[n, :]
            data_plt[j, :] = data_xpand[n, :]
            j += 1
            x_plt[j, :] = x_xpand[n, :]
            cell_plt[j, :] = cell_depth_xpand[n, :] + 0.5 * cell_size_xpand[n, :]
            data_plt[j, :] = data_xpand[n, :]

        cell_plt[np.isnan(cell_plt)] = 0
        data_plt[np.isnan(data_plt)] = -999
        x_plt[np.isnan(x_plt)] = 0
        data_plt = data_plt[:-1, :-1]

        return x_plt, cell_plt, data_plt, depth

    def plt_contour(
        self,
        x_plt_in,
        cell_plt_in,
        data_plt_in,
        x,
        depth,
        data_units,
        topbot=True,
        selected=None,
        data_limits=None,
        cmap_in=None,
        ping_name=None,
        n_names=None,
        sidelobe=None,
        depth_obj=None,
    ):
        """Create contour plot.

        Parameters
        ----------
        x_plt_in: np.ndarray()
            x data used for contour plot
        cell_plt_in: np.ndarray()
            Cell depth data
        data_plt_in: np.ndarray()
            Primary data to plot
        x: np.ndarray()
            x data used for depth plot
        depth: np.ndarray()
            Depth data
        selected: int
            Index of selected vertical
        data_units: tuple
            Tuple of data multiplier and label
        topbot: bool
            Specifies if extrapolated top and bot values are shown
        data_limits: list
            Optional list of min max data limits
        cmap_in: str
            Name of color map to use
        ping_name: dict
            Dictionary of ping names
        n_names: int
            Number of names
        sidelobe: bool
            Indicates if sidelobe line should plotted
        depth_obj: DepthData
            Object of DepthData needed to plot sidelobe
        """

        # Use last subplot
        ax = self.ax[-1]

        # Create plot variables for input
        if self.x_axis_type == "T":
            # If x axis is time, create x_plt
            x_plt = np.zeros(x_plt_in.shape, dtype="object")
            for r in range(x_plt_in.shape[0]):
                for c in range(x_plt_in.shape[1]):
                    x_plt[r, c] = datetime.utcfromtimestamp(x_plt_in[r, c])
        else:
            x_plt = x_plt_in

        cell_plt = cell_plt_in * self.units["L"]
        data_plt = data_plt_in * self.units["V"]

        # Determine limits for color map
        if data_limits is not None:
            max_limit = data_limits[1]
            min_limit = data_limits[0]
        elif np.sum(np.abs(data_plt_in[data_plt_in > -900])) > 0:
            max_limit = np.percentile(
                data_plt_in[data_plt_in > -900] * data_units[0], 99
            )
            min_limit = np.min(data_plt_in[data_plt_in > -900] * data_units[0])
            if 0 < min_limit < 0.1:
                min_limit = 0
        else:
            max_limit = 1
            min_limit = 0

        # Create color map
        if cmap_in is None:
            cmap = cm.get_cmap(self.color_map).copy()
        else:
            cmap = cm.get_cmap(cmap_in).copy()

        cmap.set_under("white")

        # Generate color contour
        c = ax.pcolormesh(
            x_plt, cell_plt, data_plt, cmap=cmap, vmin=min_limit, vmax=max_limit
        )

        # Create data plotted for annotation use
        self.data_plotted.append(
            {"type": "contour", "x": x_plt, "y": cell_plt, "z": data_plt}
        )

        # Initialize annotation for data cursor
        self.annot.append(
            ax.annotate(
                "",
                xy=(0, 0),
                xytext=(-20, 20),
                textcoords="offset points",
                bbox=dict(boxstyle="round", fc="w"),
                arrowprops=dict(arrowstyle="->"),
            )
        )

        self.annot[-1].set_visible(False)

        # Add color bar and axis labels in separate subplot
        self.ax.append(self.fig.add_subplot(self.gs[self.fig_no + 1]))
        self.data_plotted.append({"type": "colorbar"})
        self.annot.append("")
        cb = self.fig.colorbar(c, self.ax[-1])
        cb.ax.set_ylabel(self.canvas.tr(data_units[1]))
        cb.ax.yaxis.label.set_fontsize(12)
        cb.ax.tick_params(labelsize=10)
        cb.ax.set_ylim([min_limit, max_limit])
        ax.invert_yaxis()
        if ping_name is not None:
            tick_list = list(range(n_names))
            label_list = []
            for tick in tick_list:
                label_list.append(ping_name[tick])
            cb.set_ticks(tick_list)
            cb.ax.set_yticklabels(label_list, rotation=90, verticalalignment="center")

        if topbot:
            top_depth = cell_plt[1, :]
            bot_bool = data_plt > -998
            bot_idx = np.nansum(bot_bool, axis=0) - 2
            bot_idx[bot_idx < 0] = 0
            bot_depth = []
            for n in range(bot_idx.shape[0]):
                bot_depth.append(cell_plt[bot_idx[n], n])
            bot_depth = np.array(bot_depth)

            ax.plot(
                x_plt[0, 2:-2],
                top_depth[2:-2],
                linewidth=2,
                color="w",
                linestyle="dotted",
            )
            ax.plot(
                x_plt[0, 2:-2],
                bot_depth[2:-1],
                linewidth=2,
                color="w",
                linestyle="dotted",
            )

        if sidelobe:
            # Plot side lobe cutoff if available
            if self.transect.w_vel.sl_cutoff_m is not None:
                last_valid_cell = (
                    np.nansum(self.transect.w_vel.cells_above_sl, axis=0) - 1
                )
                last_depth_cell_size = depth_obj.depth_cell_size_m[
                    last_valid_cell, np.arange(depth_obj.depth_cell_size_m.shape[1])
                ]
                y_plt_sl = (
                    self.transect.w_vel.sl_cutoff_m + (last_depth_cell_size * 0.5)
                ) * self.units["L"]
                ax.plot(x, y_plt_sl, color="r", linewidth=0.5)

        # Plot depth
        ax.plot(x, depth * self.units["L"], color="k")

        # Plot a box around the selected vertical
        if selected is not None:
            ax.plot(
                x_plt[0, selected * 2 : selected * 2 + 2],
                np.array([0, 0]) * self.units["L"],
                linewidth=2,
                color="k",
            )
            ax.plot(
                x_plt[0, selected * 2 : selected * 2 + 2],
                np.array([depth[selected], depth[selected]]) * self.units["L"],
                linewidth=2,
                color="k",
            )
            ax.plot(
                x_plt[0:2, selected * 2],
                np.array([0, depth[selected]]) * self.units["L"],
                linewidth=2,
                color="k",
            )
            ax.plot(
                x_plt[0:2, selected * 2 + 2],
                np.array([0, depth[selected]]) * self.units["L"],
                linewidth=2,
                color="k",
            )
            # Save data for future use when updating selected vertical,
            # this avoids creating the contour plot
            ax.x_plt = x_plt

        # Label and limits for y axis
        ax.set_ylabel(self.canvas.tr("Depth ") + self.units["label_L"])
        ax.yaxis.label.set_fontsize(12)
        ax.tick_params(
            axis="both", direction="in", bottom=True, top=True, left=True, right=True
        )
        bottom = np.nanmax(depth * self.units["L"]) * 1.05
        if np.isnan(bottom):
            bottom = 0
        ax.set_ylim(top=0, bottom=bottom)

    def update_contour_selection(self, meas, selected):
        """Updates the box indicating the selected vertical.

        Parameters
        ----------
        meas: Measurement
            Object of class measurement
        selected: int
            Index of selected vertical
        """

        idx = meas.verticals_used_idx.index(selected)

        # Get axis to work with
        ax = self.ax[1]

        if len(ax.lines) > 0:
            # Remove lines drawing box
            ax.lines[6].remove()
            ax.lines[5].remove()
            ax.lines[4].remove()
            ax.lines[3].remove()

            # Get data
            x_plt = ax.x_plt
            depth = meas.verticals[selected].depth_m
            
            # Draw new box
            ax.plot(
                x_plt[0, idx * 2 : idx * 2 + 2],
                np.array([0, 0]) * self.units["L"],
                linewidth=2,
                color="k",
            )
            ax.plot(
                x_plt[0, idx * 2 : idx * 2 + 2],
                np.array([depth, depth]) * self.units["L"],
                linewidth=2,
                color="k",
            )
            ax.plot(
                x_plt[0:2, idx * 2],
                np.array([0, depth]) * self.units["L"],
                linewidth=2,
                color="k",
            )
            ax.plot(
                x_plt[0:2, idx * 2 + 2],
                np.array([0, depth]) * self.units["L"],
                linewidth=2,
                color="k",
            )

    def plt_timeseries(
        self,
        data,
        data_units,
        ax=None,
        data_2=None,
        data_mask=None,
        fmt=None,
        set_annot=True,
        set_ylim=True,
    ):
        """Create timeseries plot.

        Parameters
        ----------
        data: np.ndarray()
            1-D array of data to be plotted
        data_units: tuple
            Tuple of data multiplier and label
        ax: subplot
            Optional subplot
        data_2: np.ndarray()
            Optional data that can be masked
        data_mask: list
            List of bool indicating what data should be plotted
        fmt: list
            List of dictionary providing plot format properties
        set_annot: bool
            Indicates if annotation should be associated.
        set_ylim: bool
            Indicates if y axis limit should be set
        """

        # Use last subplot if not defined
        if ax is None:
            ax = self.ax[-1]

        # Setup plot
        ax.set_ylabel(self.canvas.tr(data_units[1]))
        ax.grid(True)
        ax.yaxis.label.set_fontsize(12)
        ax.tick_params(
            axis="both", direction="in", bottom=True, top=True, left=True, right=True
        )

        # Get format for first call to plot
        if fmt is not None:
            kwargs = fmt[0]
        else:
            kwargs = {"linestyle": "-", "color": "b"}

        # First call to plot uses masked data if there is no primary data
        if data is not None:
            ax.plot(self.x, data * data_units[0], **kwargs)
        else:
            ax.plot(
                self.x[data_mask[0]], data_2[data_mask[0]] * data_units[0], **kwargs
            )

        # Compile all data from primary and masked data sets
        all_data = data
        if data_mask is not None:
            if data_2 is None:
                data_2 = data
                all_data = data
            elif data is None:
                all_data = data_2
            else:
                all_data = np.concatenate([data, data_2])

            # Plot calls for other masked data
            for n in range(1, len(fmt)):
                if fmt is None:
                    kwargs = {
                        "color": "r",
                        "marker": "o",
                        "ms": 8,
                        "markerfacecolor": "none",
                    }
                else:
                    kwargs = fmt[n]

                ax.plot(
                    self.x[data_mask[n]], data_2[data_mask[n]] * data_units[0], **kwargs
                )

        # Create dictionary of data for use by annotation
        self.data_plotted.append({"type": "ts", "x": self.x, "y": all_data})

        # Set axis limits
        if set_ylim:
            try:
                max_y = (
                    np.nanmax(all_data) + np.abs(np.nanmax(all_data) * 0.1)
                ) * data_units[0]
                min_y = (
                    np.nanmin(all_data) - np.abs(np.nanmin(all_data)) * 0.1
                ) * data_units[0]
                if min_y > max_y * -0.05:
                    min_y = max_y * -0.05
                if not np.any(np.isnan([max_y, min_y])):
                    ax.set_ylim(top=max_y, bottom=min_y)

            except (TypeError, ValueError):
                pass

        # Initialize annotation for data cursor. Annotation should only be associated with
        # one call to plt_timeseries if figure makes multiple calls to create
        # multiple lines on the same graph.
        if set_annot:
            self.annot.append(
                ax.annotate(
                    "",
                    xy=(0, 0),
                    xytext=(-20, 20),
                    textcoords="offset points",
                    bbox=dict(boxstyle="round", fc="w"),
                    arrowprops=dict(arrowstyle="->"),
                )
            )

            self.annot[-1].set_visible(False)

        self.canvas.draw()

    def set_x_limits(self, meas, ax):
        """Set the x limits for the specified axis assuming the location is used
        for the x axis.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        ax: AxesSubplot
            Axis on which limits are set

        """

        if meas.verticals[meas.verticals_used_idx[0]].location_m < 0:
            x_min = (
                meas.verticals[meas.verticals_used_idx[0]].location_m
                * self.units["L"]
                * 1.02
            )
        else:
            x_min = (
                meas.verticals[meas.verticals_used_idx[0]].location_m
                * self.units["L"]
                * 0.98
            )

        if meas.verticals[meas.verticals_used_idx[-1]].location_m < 0:
            x_max = (
                meas.verticals[meas.verticals_used_idx[-1]].location_m
                * self.units["L"]
                * 0.98
            )
        else:
            x_max = (
                meas.verticals[meas.verticals_used_idx[-1]].location_m
                * self.units["L"]
                * 1.02
            )
        ax.set_xlim(x_min, x_max)
        if meas.start_edge == "Right":
            ax.invert_xaxis()

    def set_x_axis(self, meas, idx):
        """Configures the x-axis.

        Parameters
        ----------
        meas: Measurement
            Object of Measurement class
        idx: int
            Axis number to apply x-axis configuration to.
        """

        # x-axis is length
        if self.x_axis_type == "L":
            if self.x[0] < 0:
                x_min = self.x[0] * self.units["L"] * 1.02
            else:
                x_min = self.x[0] * self.units["L"] * 0.98

            if self.x[-1] < 0:
                x_max = self.x[-1] * self.units["L"] * 0.98
            else:
                x_max = self.x[-1] * self.units["L"] * 1.02
            self.ax[idx].set_xlim(x_min, x_max)

            if meas.verticals[meas.verticals_used_idx[0]].edge_loc == "Right":
                self.ax[idx].invert_xaxis()

            self.ax[idx].set_xlabel(self.canvas.tr("Location " + self.units["label_L"]))

        # x-axis is ensembles
        elif self.x_axis_type == "E":
            self.ax[idx].set_xlim(left=0, right=self.x[-1] + 1)
            self.ax[idx].set_xlabel(self.canvas.tr("Ensembles "))

        # x-axis is time
        elif self.x_axis_type == "T":
            axis_buffer = (self.x_timestamp[-1] - self.x_timestamp[0]) * 0.02
            if meas.verticals[meas.verticals_used_idx[0]].edge_loc == "Right":
                self.ax[idx].invert_xaxis()
                self.ax[idx].set_xlim(
                    right=datetime.utcfromtimestamp(self.x_timestamp[0] - axis_buffer),
                    left=datetime.utcfromtimestamp(self.x_timestamp[-1] + axis_buffer),
                )
            else:
                self.ax[idx].set_xlim(
                    left=datetime.utcfromtimestamp(self.x_timestamp[0] - axis_buffer),
                    right=datetime.utcfromtimestamp(self.x_timestamp[-1] + axis_buffer),
                )
            date_form = DateFormatter("%H:%M:%S")
            self.ax[idx].xaxis.set_major_formatter(date_form)
            self.ax[idx].set_xlabel(self.canvas.tr("Time "))

    def hover(self, event):
        """Determines if the user has selected a location with data and makes
        annotation visible and calls method to update the text of the annotation. If the
        location is not valid the existing annotation is hidden.

        Parameters
        ----------
        event: MouseEvent
            Triggered when mouse button is pressed.
        """

        # Determine if mouse location references a data point in the plot and update
        # the annotation.
        for n, item in enumerate(self.ax):
            if event.inaxes == item:
                # Verify that location is associated with plotted data
                cont_fig = False
                line_num = None
                if item is not None:
                    for ax in self.ax:
                        for line_num, line in enumerate(ax.lines):
                            cont_fig, ind_fig = line.contains(event)
                            if cont_fig:
                                break
                        if cont_fig:
                            break
                    if not cont_fig:
                        line_num = None
                        for ax in self.ax:
                            cont_ax, ind_ax = ax.contains(event)
                            if cont_ax:
                                child_list = ax.get_children()
                                for child in child_list:
                                    if type(child) is collections.QuadMesh:
                                        cont_fig, ind_fig = child.contains(event)
                                    if cont_fig:
                                        break
                            if cont_fig:
                                break

                value = None
                if (
                    self.data_plotted[n]["type"] == "bar"
                    or self.data_plotted[n]["type"] == "vel"
                    or self.data_plotted[n]["type"] == "contour"
                ):
                    cont_fig = True
                if cont_fig and self.fig.get_visible():
                    # Annotation for contour plot
                    if self.data_plotted[n]["type"] == "contour":
                        # Get plotted data
                        x_plt = self.data_plotted[n]["x"]
                        y_plt = self.data_plotted[n]["y"]
                        z_plt = self.data_plotted[n]["z"]

                        # Determine data column index
                        if self.x_axis_type == "T":
                            col_idx = np.where(
                                x_plt[0, :] < num2date(event.xdata).replace(tzinfo=None)
                            )[0][-1]
                        elif self.x_axis_type == "L":
                            col_idx = np.where(x_plt[0, :] < event.xdata)[0][-1]
                        else:
                            col_idx = int(round(abs(event.xdata * 2 - x_plt[0, 0])) - 1)

                        # Determine plotted value
                        for row_idx, cell in enumerate(y_plt[:, col_idx]):
                            if event.ydata < cell:
                                value = z_plt[row_idx, col_idx]
                                break

                        # Create annotation
                        self.update_annot(
                            ax_idx=n,
                            x=event.xdata,
                            y=event.ydata,
                            v=value,
                            v_dict=self.ping_name,
                        )
                        self.annot[n].set_visible(True)
                        self.canvas.draw_idle()
                        break
                    elif self.data_plotted[n]["type"] == "vel":
                        x_idx = np.abs(
                            self.data_plotted[n]["Station"] - event.xdata
                        ).argmin()

                        self.update_annot(
                            ax_idx=n,
                            x=self.data_plotted[n]["Station"][x_idx],
                            y=0,
                            v=x_idx,
                        )
                        self.annot[n].set_visible(True)
                        self.canvas.draw_idle()
                        break

                    elif (
                        self.data_plotted[n]["type"] == "profile"
                        and line_num is not None
                    ):
                        y_idx = np.nanargmin(
                            np.abs(self.data_plotted[n]["y"] - event.ydata)
                        )
                        x = self.data_plotted[n]["x"][line_num, y_idx]
                        y = self.data_plotted[n]["y"][y_idx]
                        self.update_annot(ax_idx=n, x=x, y=y, v=line_num + 1)
                        self.annot[n].set_visible(True)
                        self.canvas.draw_idle()
                        break

                    elif self.data_plotted[n]["type"] == "bar":
                        x_idx = np.nanargmin(
                            np.abs(self.data_plotted[n]["x"] - event.xdata)
                        )
                        x = self.data_plotted[n]["x"][x_idx]
                        y = self.data_plotted[n]["y"][x_idx]
                        self.update_annot(ax_idx=n, x=x, y=y, v=None)
                        self.annot[n].set_visible(True)
                        self.canvas.draw_idle()
                        break

                    # Annotation for time series data
                    elif self.data_plotted[n]["type"] == "ts":
                        self.update_annot(
                            ax_idx=n, x=event.xdata, y=event.ydata, v=value
                        )

                        self.annot[n].set_visible(True)
                        self.canvas.draw_idle()
                        break

                    else:
                        self.annot_hide(n)

            else:
                self.annot_hide(n)

    def annot_hide(self, n):
        """Hides annotation if cursor is outside data limits.

        n: int
            Index for annotation
        """
        # If the cursor location is not associated with the plotted data hide the
        # annotation.
        if self.fig.get_visible():
            if type(self.annot[n]) != str:
                self.annot[n].set_visible(False)
            self.canvas.draw_idle()

    def set_hover_connection(self, setting):
        """Turns the connection to the mouse event on or off.

        Parameters
        ----------
        setting: bool
            Boolean to specify whether the connection for the mouse event is active or not
        """

        if setting and self.hover_connection is None:
            self.hover_connection = self.canvas.mpl_connect(
                "button_press_event", self.hover
            )
        elif not setting:
            self.canvas.mpl_disconnect(self.hover_connection)
            self.hover_connection = None
            for item in self.annot:
                if type(item) != str:
                    item.set_visible(False)
            self.canvas.draw_idle()

    def update_annot(self, ax_idx, x, y, v=None, v_dict=None):
        """Updates the location and text and makes visible the previously initialized and hidden annotation.

        Parameters
        ----------
        ax_idx: int
            Index of axis
        x: float
            x coordinate for annotation, ensemble
        y: float
            y coordinate for annotation, depth
        v: float or None
            Speed for annotation
        v_dict: dict
            Ping type name for value
        """

        # Set local variables
        pos = [x, y]
        plt_ref = self.ax[ax_idx]
        annot_ref = self.annot[ax_idx]

        # Shift annotation box left or right depending on which half of the axis the pos x is located and the
        # direction of x increasing.
        if plt_ref.viewLim.intervalx[0] < plt_ref.viewLim.intervalx[1]:
            if (
                pos[0]
                < (plt_ref.viewLim.intervalx[0] + plt_ref.viewLim.intervalx[1]) / 2
            ):
                annot_ref._x = -20
            else:
                annot_ref._x = -80
        else:
            if (
                pos[0]
                < (plt_ref.viewLim.intervalx[0] + plt_ref.viewLim.intervalx[1]) / 2
            ):
                annot_ref._x = -80
            else:
                annot_ref._x = -20

        # Shift annotation box up or down depending on which half of the axis the pos y is located and the
        # direction of y increasing.
        if plt_ref.viewLim.intervaly[0] < plt_ref.viewLim.intervaly[1]:
            if (
                pos[1]
                > (plt_ref.viewLim.intervaly[0] + plt_ref.viewLim.intervaly[1]) / 2
            ):
                annot_ref._y = -40
            else:
                annot_ref._y = 20
        else:
            if (
                pos[1]
                > (plt_ref.viewLim.intervaly[0] + plt_ref.viewLim.intervaly[1]) / 2
            ):
                annot_ref._y = 20
            else:
                annot_ref._y = -40
        annot_ref.xy = pos
        text = ""

        # Annotation of contour plot
        if (
            v is not None
            and v > -999
            and self.data_plotted[ax_idx]["type"] == "contour"
        ):
            # Format for time axis
            if self.x_axis_type == "T":
                x_label = num2date(pos[0]).strftime("%H:%M:%S.%f")[:-4]
                if v_dict is None:
                    text = "x: {}, y: {:.2f}, \n v: {:.2f}".format(x_label, y, v)
                else:
                    text = "x: {}, y: {:.2f}, \n {}".format(x_label, y, v_dict[v])
            # Format for ensemble axis
            elif self.x_axis_type == "E":
                if v_dict is None:
                    text = "x: {:.2f}, y: {:.2f}, \n v: {:.2f}".format(
                        int(round(x)), y, v
                    )
                else:
                    text = "x: {}, y: {:.2f}, \n {}".format(int(round(x)), y, v_dict[v])
            # Format for length axis
            elif self.x_axis_type == "L":
                if v_dict is None:
                    text = "x: {:.2f}, y: {:.2f}, \n v: {:.2f}".format(x, y, v)
                else:
                    text = "x: {}, y: {:.2f}, \n {}".format(x, y, v_dict[v])

        elif self.data_plotted[ax_idx]["type"] == "vel":
            text = " Station: {:.2f} \n Normal: {:.2f} \n Actual: {:.2f} \n Angle: {:.1f}".format(
                self.data_plotted[ax_idx]["Station"][v],
                self.data_plotted[ax_idx]["Normal"][v],
                self.data_plotted[ax_idx]["Actual"][v],
                self.data_plotted[ax_idx]["Angle"][v],
            )
            if annot_ref._x == -20:
                annot_ref._x = 20 * 1.4
            else:
                annot_ref._x = annot_ref._x * 1.4

        elif self.data_plotted[ax_idx]["type"] == "profile":
            text = " Beam: {:.0f} \n RSSI: {:.2f}".format(v, x)

        elif self.data_plotted[ax_idx]["type"] == "bar":
            text = "Station Number: {:.0f} \n Percent Q: {:.2f}".format(x, y)

        # Annotation for time series
        else:
            # Format for time axis
            if self.x_axis_type == "T":
                x_label = num2date(pos[0]).strftime("%H:%M:%S.%f")[:-4]
                text = "x: {}, y: {:.2f}".format(x_label, y)
            # Format for ensemble axis
            elif self.x_axis_type == "E":
                text = "x: {:.2f}, y: {:.2f}".format(int(round(x)), y)
            # Format for length axis
            elif self.x_axis_type == "L":
                text = "x: {:.2f}, y: {:.2f}".format(x, y)

        annot_ref.set_text(text)

    # Methods to select and change vertical based on a click on the graph
    # -------------------------------------------------------------------

    def select_vertical(self, parent):
        """Implements the click event for the plots on Main that allow vertical selection.

        Parameters
        ----------
        parent: QRevIntMS
            Object of QRevIntMS
        """

        # Initialize the connection and save the parent.
        if self.hover_connection is None:
            self.select_connection = self.canvas.mpl_connect(
                "button_press_event", self.select
            )
            self.parent = parent

    def select(self, event):
        """Calls the parent method to update the Main display, indicating the selected
        vertical.

        Parameters
        ----------
        event:
            Triggered when mouse button is pressed.
        """
        if event.button == QtCore.Qt.LeftButton:
            self.parent.graphically_select_vertical(event.xdata, self.x_axis_type)

    @contextmanager
    def wait_cursor(self):
        """Provide a busy cursor to the user while the code is processing."""
        try:
            QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
            yield
        finally:
            QtWidgets.QApplication.restoreOverrideCursor()
