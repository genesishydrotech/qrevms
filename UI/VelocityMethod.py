from PyQt5 import QtWidgets
from UI import wVelocityMethod


class VelocityMethod(QtWidgets.QDialog, wVelocityMethod.Ui_Dialog):
    """Dialog to allow users to set velocity method."""

    def __init__(self, parent=None):
        super(VelocityMethod, self).__init__(parent)
        self.setupUi(self)
        self.pb_apply_all.clicked.connect(self.accept)
        self.pb_cancel.clicked.connect(self.reject)
