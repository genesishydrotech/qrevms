class ComboGrp(object):
    """Provides the logic to control the behavior of a combobox that can be applied to one or all data.

    Attributes
    ----------
    combo: QtWidget
        Combobox widget
    pb_apply: QtWidget
        Pushbutton widget to apply to selected data only.
    pb_apply_all: QtWidget
        Pushbutton widget to apply to all data.
    change: method
        Method in the parent used to make the specified change
    """

    def __init__(self, combo, pb_apply, pb_apply_all, change):
        """Initializes the combobox group variables.

        Parameters
        ----------
        combo: QtWidget
            Combobox widget
        pb_apply: QtWidget
            Pushbutton widget to apply to selected data only.
        pb_apply_all: QtWidget
            Pushbutton widget to apply to all data.
        change: method
            Method in the parent used to make the specified change
        """
        self.combo = combo
        self.pb_apply = pb_apply
        self.pb_apply_all = pb_apply_all
        self.change = change

    def enable_apply(self):
        """Enables the apply buttons when a choice is selected from the combobox."""

        self.pb_apply.setEnabled(True)
        self.pb_apply_all.setEnabled(True)

    def apply(self):
        """Applies the change to the selected data only."""
        self.change(apply_all=False)

    def apply_all(self):
        """Applies the change to all the data."""
        self.change(apply_all=True)
