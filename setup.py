# -*- coding: utf-8 -*-

# Learn more: https://github.com/kennethreitz/setup.py

from setuptools import setup, find_packages


with open("README.md") as f:
    readme = f.read()

with open("LICENSE.md") as f:
    license = f.read()

setup(
    name="QRevMS",
    version="1.00.0",
    description="Provides QRev like processing for ADCP measurements made with the mid- and mean-section methods",
    long_description=readme,
    author="David S. Mueller",
    author_email="dave@genesishydrotech.com",
    url="https://www.genesishydrotech.com",
    license=license,
    REQUIRES_PYTHON="3.9.7",
    packages=["QRevMS", "QRevMS.Classes", "QRevMS.Modules", "QRevMS.UI"],
    install_requires=[
        "json_tricks==3.15.5",
        "matplotlib==3.4.3",
        "numpy==1.21.2",
        "pandas==1.3.4",
        "PyQt5==5.15.7",
        "scipy==1.7.1",
        "sigfig==1.3.2",
        "utm==0.7.0",
        "xmltodict==0.12.0",
        "reportlab==4.0.5",
        "setuptools==58.0.4",
        "pyinstaller==5.3",
        "zipp==3.8.1",
        "Pillow==10.0.1"
    ],
)
