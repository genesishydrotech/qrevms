# QRevMS

**QRevMS** is an extension of QRev and QRevInt to support the mid- and mean-section methods when using an ADCP. fork, created by Genesis HydroTech LLC, of QRev version 4 developed by the USGS. Data collected with the Teledyne RD Instrument (TRDI) SxS Pro or SonTek RiverSurveyor Stationary Live or RSQ software are supported. The software improves the consistency and efficiency of processing streamflow measurements by providing:

* Automated data quality checks with feedback to the user
* Automated data filtering
* Automated application of extrap\
* Consistent processing algorithms independent of the ADCP used to collect the data
* Improved handing of variable and invalid data

## Download Windows Executables

Windows Executables can be downloaded from (https://www.genesishydrotech.com)

# Development
Genesis HydroTech LLC is coordinating the development of QRevMS with the guidance and contributions from the following agencies:  

* The Norwegian Water Resources and Energy Directorate
* Environment and Climate Change Canada
* New Zealand Hydrological Society
* Swedish Meteorological and Hydrological Institute
* Groupe Doppler Hydrométrie
* U.S. Geological Survey
* UK Centre for Ecology & Hydrology
* Queensland Government Department of Resources
* Australian Hydrographers Association
* Icelandic Meteorological Office
* Finish Environment Institute

## Goal
The goal of this project is to: 

* improve and expand the features currently available in QRev and QRevInt to mid- and mean-section measurement methods, 
* assist in the development of a standard processing software that is used internationally, and 
* ensure long-term support of the open-source software.

## Bugs
Please report all bugs with appropriate instructions and files to reproduce the issue and add this to the issues tracking feature in this repository or email (dave@genesishydrotech.com).

## Feature Requests
Feature requests are welcome. But take a moment to find out whether your idea fits with the scope and aims of the project. It's up to you to make a strong case to convince the project's developers of the merits of this feature. Please provide as much detail and context as possible. Your proposed feature will be shared with the guidance group to ensure the contributions are consistent with this project and save you from work that might not be accepted into QRevInt. Be aware there is already a long list of proposed enhancement, so even if you request is accepted, there is no guarantee when if might be implemented, without associated resources.

## Contributions
If you would like to contribute your expertise to this project you are encourage to email your proposed contributions to dave@genesishydrotech.com so that the guidance group can ensure the contributions are consistent with this project and save you from work that might not be accepted into QRevInt. Contributions could include research into improving or developing better methods, writing code, reviewing code, reviewing and updating manuals, etc. If your contributions involve writing code or updating manuals please refer to the [CONTRIBUTORS_GUIDELINES.md](https://bitbucket.org/genesishydrotech/qrevms/src/master/CONTRIBUTORS_GUIDELINES.md) in this repository. Adherence to these guidelines will facilitate efficient merging of your contributions into the project and keep the project style and documentation consistent.

If you would like to contribute to this project finacially please contact dave@genesishydrotech.com.

## Requirements and Dependencies
QRevMS is currently being developed using Python 3.9.7 and has the following requirements:

json_tricks==3.15.5
matplotlib==3.4.3
numpy==1.21.2
pandas==1.3.4
PyQt5==5.15.7
reportlab==4.0.5
scipy==1.7.1
sigfig==1.3.2
utm==0.7.0
xmltodict==0.12.0
reportlab~=4.0.5
setuptools~=58.0.4
pyinstaller~=5.3
zipp==3.8.1
Pillow==10.0.1

# Disclaimer
This software (QRevMS) is a fork of QRevInt developed by Genesis HydroTech LLC through funding from various international agencies for processing mid- and mean-section measurements. While Genesis HydroTech LLC makes every effort to deliver high quality products, Genesis HydroTech LLC does not guarantee that the product is free from defects. QRevMS is provided “as is," and you use the software at your own risk. Genesis HydroTech LLC and contributing agencies make no warranties as to performance, merchantability, fitness for a particular purpose, or any other warranties whether expressed or implied. No oral or written communication from or information provided by Genesis HydroTech LLC or contributing agencies shall create a warranty. Under no circumstances shall Genesis HydroTech LLC or the contributing agencies be liable for direct, indirect, special, incidental, or consequential damages resulting from the use, misuse, or inability to use this software, even if Genesis HydroTech LLC or the contributing agencies have been advised of the possibility of such damages.

# License
Unless otherwise noted, this project is in the public domain in the United States because it contains materials that originally came from the United States Geological Survey, an agency of the United States Department of Interior. For more information, see the official USGS copyright policy at https://www.usgs.gov/information-policies-and-instructions/copyrights-and-credits. Additionally, Genesis HydroTech LLC waives copyright and related rights in the work worldwide through the CC0 1.0 Universal public domain dedication.

QRevMS includes several open-source software packages. These open-source packages are governed by the terms and conditions of the applicable open-source license, and you are bound by the terms and conditions of the applicable open-source license in connection with your use and distribution of the open-source software in this product. 

Copyright / License - CC0 1.0: The person who associated a work with this deed has dedicated the work to the public domain by waiving all of his or her rights to the work worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law. You can copy, modify, distribute and perform the work, even for commercial purposes, all without asking permission. 

In no way are the patent or trademark rights of any person affected by CC0, nor are the rights that other persons may have in the work or in how the work is used, such as publicity or privacy rights.

Unless expressly stated otherwise, the person who associated a work with this deed makes no warranties about the work, and disclaims liability for all uses of the work, to the fullest extent permitted by applicable law.

When using or citing the work, you should not imply endorsement by the author or the affirmer.

Publicity or privacy: The use of a work free of known copyright restrictions may be otherwise regulated or limited. The work or its use may be subject to personal data protection laws, publicity, image, or privacy rights that allow a person to control how their voice, image or likeness is used, or other restrictions or limitations under applicable law.

Endorsement: In some jurisdictions, wrongfully implying that an author, publisher or anyone else endorses your use of a work may be unlawful.

# Suggested citations
Mueller, D.S., 2023, QRevMS, Version 1.00, Genesis HydroTech LLC, https://bitbucket.org/genesishydrotech/qrevms/src/master/.

# Author
David S Mueller  
Genesis HydroTech LLC 
Louisville, KY  
<dave@genesishydrotech.com>