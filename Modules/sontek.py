import shutil
import os
import json
import scipy.io as sio
import numpy as np


def read_sontek(filename):
    ext = filename[filename.rfind(".") :]

    # RSQ Stationary Raw
    if ext == ".rsqst":
        sontek_data = read_rsqst(filename)
        data_type = ext[1:]

    # Sontek Matlab output
    elif ext == ".mat":
        sontek_data = sio.loadmat(filename, struct_as_record=False, squeeze_me=True)

        # RSSL
        if "MidSection" in sontek_data:
            data_type = "matst"
            # Convert data to SI units if in English units
            if sontek_data["RawWaterTrack"].Units.Velocity == "ft/s":
                if hasattr(sontek_data["Processing"], "RawVelocityFix"):
                    sontek_data = convert2metric(sontek_data)
                else:
                    sontek_data = (
                        "Due to issues with RSSL versions 4.2 and earlier "
                        "creating Matlab files in English units, the Matlab "
                        "file must be created using RSSL newer than 4.2 or use "
                        "SI units to create the Matlab file when using RSSL "
                        "versions 4.2 and earlier."
                    )

        # RSL
        else:
            data_type = "matmb"
            # Convert data to SI units if in English units
            if sontek_data["BottomTrack"].Units.BT_Depth == "ft":
                sontek_data = convert2metric(sontek_data)

            if hasattr(sontek_data["RawGPSData"], "VtgMode"):
                sontek_data["RawGPSData"].VtgMode[
                    np.isnan(sontek_data["RawGPSData"].VtgMode)
                ] = 0
                sontek_data["RawGPSData"].VtgMode = np.array(
                    [chr(x) for x in range(127)]
                )[sontek_data["RawGPSData"].VtgMode.astype(int)]
    else:
        sontek_data = {}
        data_type = ""
    return sontek_data, data_type


def read_rsqst(filename):
    temp_path = os.path.join(os.getenv("APPDATA"), "QRev_Data")
    shutil.unpack_archive(filename, temp_path, "zip")

    sontek_data = {
        "RawAdcpSessionInfo": None,
        "StationarySetup": dict(),
        "AdcpData": [],
        "utc_time_offset": ""
    }

    with open(os.path.join(temp_path, "RawAdcpSessionInfo.json")) as json_file:
        sontek_data["RawAdcpSessionInfo"] = json.load(json_file)

    with open(os.path.join(temp_path, "StationarySetup.json")) as json_file:
        sontek_data["StationarySetup"] = json.load(json_file)

    for station in sontek_data["StationarySetup"]["RecordedStations"]:
        if len(station["AdcpDataId"]) > 0:

            data_filename = os.path.join(
                temp_path, "AdcpData", station["AdcpDataId"], "RawData.jsonlog"
            )
            with open(data_filename) as data_file:
                data = data_file.readlines()
            json_idx = data[2].find("{")
            section = json.loads(data[2][json_idx:])
            section["Samples"] = []
            for n in range(4, len(data), 2):
                json_idx = data[n].find("{")
                section["Samples"].append(json.loads(data[n][json_idx:]))
            sontek_data["AdcpData"].append(section)
        else:
            sontek_data["AdcpData"].append(station)

    with open(os.path.join(temp_path, "DataSessionProperties.json")) as json_file:
        temp = json.load(json_file)
        sontek_data["utc_time_offset"] = temp["DataCollectionLocalTimeUtcOffset"]

    # Remove temporary files
    shutil.rmtree(temp_path)
    return sontek_data


def convert2metric(sontek_data):
    """Converts all data in English units to metric units.

    Parameters
    ----------
    sontek_data: dict
        Dictionary of data from Matlab file
    """

    data2correct = list(sontek_data.keys())

    for item in data2correct:
        data = sontek_data[item]
        if hasattr(data, "Units"):
            units = data.Units
            names = units._fieldnames
            for name in names:
                if getattr(units, name) == "ft":
                    setattr(data, name, getattr(data, name) * 0.3048)
                    setattr(units, name, "m")
                elif getattr(units, name) == "ft/s":
                    setattr(data, name, getattr(data, name) * 0.3048)
                    setattr(units, name, "m/s")
                elif getattr(units, name) == "degF":
                    setattr(data, name, (getattr(data, name) - 32) * (5.0 / 9.0))
                    setattr(units, name, "degC")
                elif getattr(units, name) == "cfs":
                    setattr(data, name, getattr(data, name) * (0.3048**3))
                    setattr(units, name, "m3/s")
                elif getattr(units, name) == "ft2":
                    setattr(data, name, getattr(data, name) * (0.3048**2))
                    setattr(units, name, "m2")
    return sontek_data
