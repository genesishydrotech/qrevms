import numpy as np
from scipy.optimize.minpack import curve_fit
from scipy.stats import t


class FitData(object):
    """Class to compute top and bottom extrapolation methods and associated statistics.

     Data required for the constructor method include data of class
     NormData, threshold for the minimum number of points for a valid
     median, top extrapolation method, bottom extrapolation method, type
     of fit, and if a manual fit, the exponent.

    Attributes
    ----------

    bot_method: str
        Bottom extrapolation method
    coef: float
        Power fit coefficient
    exp_method: str
        Method to determine exponent (default, optimize, or manual)
    exponent: float
        Power fit exponent
    exponent_95_ci: np.array(float)
        95% confidence intervals for optimized exponent
    file_name: float
        Vertical location, in m
    ice_exponent: float
        Exponent for ice extrapolation
    r_squared: float
        R squared of model
    residuals: np.array(float)
        Residuals from fit
    top_method: str
        Top extrapolation method
    u: np.array(float)
        Fit values of the variable
    u_auto: np.array(float)
        Fit values from automatic fit
    z: np.array(float)
        Distance from the streambed for fit variable
    z_auto: np.array(float)
        z values for automtic fit
    """

    def __init__(self):
        """Initialize object and instance variables."""

        self.file_name = np.nan
        self.top_method = "Power"
        self.bot_method = "Power"
        self.coef = 0
        self.exponent = 0.1667
        self.ice_exponent = 0.1667
        self.u = np.nan
        self.u_auto = np.nan
        self.z_auto = np.nan
        self.z = np.nan
        self.exp_method = "Power"
        self.exponent_95_ci = 0
        self.residuals = np.array([])
        self.r_squared = 0

    def populate_data(
        self, vertical, valid_data, top, bot, method, exponent=None, ice_exponent=None
    ):
        """Computes fit and stores associated data.

        Parameters
        ----------
        vertical: Vertical
            Object of class Vertical
        valid_data: np.array(bool)
            Array identifying valid data in the mean profile
        top: str
            Top extrapolation method
        bot: str
            Bottom extrapolation method
        method: str
            Method used to define the exponent (default, optimize, or manual), default is 1/6.
        exponent: float
            Exponent for power or no slip fit methods.
        ice_exponent: float
            Exponent for ice fit
        """

        # Setup variables
        avg_z = vertical.depth_m - vertical.mean_profile["cell_depth"][valid_data]
        cell_size = vertical.mean_profile["cell_size"][valid_data]
        speed = vertical.mean_profile["speed"][valid_data]

        # Remove data with an avg_z less than zero. This could occur where the
        # vertical beam is used and measures less than the slant beams.
        cell_size = cell_size[avg_z >= 0]
        speed = speed[avg_z >= 0]
        avg_z = avg_z[avg_z >= 0]

        z_top = np.nan
        idxz = np.arange(speed.size)

        # Set bounds for coefficient, exponent
        lower_bound = [-np.inf, 0.01]
        upper_bound = [np.inf, 1]
        bounds = None

        # Initial guess for fit
        p0 = None

        # Variable for top speed
        u_top = np.nan

        # Process data if available
        if idxz.size > 0:
            idx_power = idxz

            # Create arrays for data fitting
            # ==============================

            # If bottom is No Slip, Power at top is not allowed
            if bot == "No Slip":
                if top == "Power":
                    top = "Constant"

            # Create fit combination
            fit_combo = "".join([top, bot])

            # Create arrays for computing the results of power / power fit
            if fit_combo == "PowerPower":
                self.z = np.arange(0, vertical.depth_m, 0.01)
                self.z = np.hstack((self.z, vertical.depth_m))
                z_top = np.nan
                u_top = np.nan

            # Create arrays for computing the results of constant / power fit
            elif fit_combo == "ConstantPower":
                self.z = np.arange(0, np.max(avg_z[idxz]), 0.01)
                self.z = np.hstack([self.z, np.nan])
                z_top = np.arange(np.max(avg_z[idxz]) + 0.01, vertical.depth_m, 0.01)
                u_top = np.tile(speed[idxz[0]], z_top.shape)

            # Create arrays for computing the results of 3-point / power fit
            elif fit_combo == "3-PointPower":
                self.z = np.arange(0, np.max(avg_z[idxz]), 0.01)
                self.z = np.hstack([self.z, np.nan])
                # If less than 6 bins use constant at the top
                if len(idxz) < 6:
                    z_top = np.arange(
                        np.max(avg_z[idxz]) + 0.01, vertical.depth_m, 0.01
                    )
                    u_top = np.tile(speed[idxz[0]], z_top.shape)
                else:
                    p = np.polyfit(avg_z[idxz[0:3]], speed[idxz[0:3]], 1)
                    z_top = np.arange(
                        np.max(avg_z[idxz]) + 0.01, vertical.depth_m, 0.01
                    )
                    u_top = z_top * p[0] + p[1]

            # Create arrays for computing the results of constant / no slip fit
            elif fit_combo == "ConstantNo Slip":
                idx, method = self.no_slip_idx(vertical, method, avg_z, idxz)

                # Create arrays
                idxns = np.array([idx]).T
                # idxns = np.array([idxz[-2:]]).T
                # Check to make sure a value of z => 0.1 is included in idx
                while idxns[0] >= 0 and avg_z[idxns[0]] < 0.1 * (
                    vertical.depth_m - vertical.ws_to_slush_bottom_m
                ):
                    idxns = np.vstack((idxns[0] - 1, idxns))
                self.z = np.arange(0, avg_z[idxns[0]], 0.01)
                self.z = np.hstack([self.z, [np.nan]])
                idx_power = idx
                z_top = np.arange(np.max(avg_z[idxz]) + 0.01, vertical.depth_m, 0.01)
                u_top = np.tile(speed[idxz[0]], z_top.shape)

            # Create arrays for computing the results of 3-point / no slip fit
            elif fit_combo == "3-PointNo Slip":
                idx, method = self.no_slip_idx(vertical, method, avg_z, idxz)

                # Indices of cells to use for no slip fit
                idx_power = idx

                # Create arrays
                idxns = np.array([idx]).T
                # Check to make sure a value of z => 0.1 is included in idx
                while idxns[0] >= 0 and avg_z[idxns[0]] < 0.1 * vertical.depth_m:
                    idxns = np.vstack((idxns[0] - 1, idxns))
                self.z = np.arange(0, avg_z[idxns[0]], 0.01)
                self.z = np.hstack([self.z, [np.nan]])

                # If less than 6 bins use constant at the top, otherwise fit line
                if len(idxz) < 6:
                    z_top = np.arange(
                        np.max(avg_z[idxz]) + 0.01, vertical.depth_m, 0.01
                    )
                    u_top = np.tile(speed[idxz[0]], z_top.shape)
                else:
                    p = np.polyfit(avg_z[idxz[0:3]], speed[idxz[0:3]], 1)
                    z_top = np.arange(
                        np.max(avg_z[idxz]) + 0.01, vertical.depth_m, 0.01
                    )
                    u_top = z_top * p[0] + p[1]

            # Create arrays for computing the results of ice / no slip fit
            # There is currently no optimization for the ice fit, so the fit is
            # computed directly
            elif fit_combo == "IceNo Slip":
                self.ice_exponent = ice_exponent

                idx, method = self.no_slip_idx(vertical, method, avg_z, idxz)

                # Indices of cells to use for no slip fit
                idx_power = idx

                # Create arrays for no slip portion of the fit
                idxns = idx.T
                # Check to make sure a value of z => 0.1 is included in idx
                while (
                    idxns[0] >= 0
                    and avg_z[idxns[0]]
                    < 0.1 * vertical.depth_m - vertical.ws_to_slush_bottom_m
                ):
                    idxns = np.hstack((idxns[0] - 1, idxns))
                self.z = np.arange(0, avg_z[idxns[0]], 0.01)
                self.z = np.hstack([self.z, [np.nan]])

                # Adjust range to bottom for presence of ice, z coordinate does not need
                # adjustment because both cell depth and depth would be adjusted the same
                range_2_bottom = vertical.depth_m - vertical.ws_to_slush_bottom_m

                # Create array to compute and display ice fit
                z_top = np.arange(avg_z[idxz[0]], np.round(range_2_bottom, 2), 0.01)
                idx_ice = np.where(
                    np.round(avg_z[idxz], 2) >= np.round(0.8 * range_2_bottom, 2)
                )[0]
                if len(idx_ice) < 1:
                    idx_ice = idxz[0]
                elif len(idx_ice) > z_top.shape[0]:
                    idx_ice = idxz[idx_ice[0 : z_top.shape[0]]]
                else:
                    idx_ice = idxz[idx_ice]

                # To apply power fit to top (ice) convert z_top so that top is zero
                z_ice = z_top[-1] - z_top

                # Fit power curve
                ice_coef = (
                    (ice_exponent + 1) * np.nansum(cell_size[idx_ice] * speed[idx_ice])
                ) / np.nansum(
                    (
                        (z_ice[idx_ice] + (0.5 * cell_size[idx_ice]))
                        ** (ice_exponent + 1)
                        - (
                            (z_ice[idx_ice] - (0.5 * cell_size[idx_ice]))
                            ** (ice_exponent + 1)
                        )
                    )
                )

                # Compute the fit
                u_top = ice_coef * z_ice**self.ice_exponent

            # Compute power or no slip fit using data determined from above
            # =============================================================
            # TODO for optimization of ice the following code would likely be moved to
            #  a method that could be called
            # TODO for power, no slip, or ice with appropriate variables passed.
            # Compute exponent
            zfit = avg_z[idx_power]
            yfit = speed[idx_power]

            # Check data validity
            ok_ = np.logical_and(np.isfinite(zfit), np.isfinite(yfit))

            # Initialize variables
            self.exponent = np.nan
            self.exponent_95_ci = np.nan
            self.r_squared = np.nan
            fit_func = "linear"

            # Force method to lower case to avoid capitalization errors
            lower_method = method.lower()

            # Set fit function, exponent, and bounds base on method
            if lower_method == "manual":
                fit_func = "linear"
                self.exponent = exponent
                bounds = None
                p0 = None

            elif lower_method == "default":
                fit_func = "linear"
                self.exponent = 0.1667
                bounds = None
                p0 = None

            elif lower_method == "optimize":
                fit_func = "power"
                bounds = [lower_bound, upper_bound]
                strt = yfit[ok_]
                p0 = [strt[-1], 0.1667]

            # Define fit functions
            fit_funcs = {
                "linear": lambda x, a: a * x**self.exponent,
                "power": lambda x, a, b: a * x**b,
            }

            # Compute fit parameters
            if ok_.size > 1:
                try:
                    if bounds is not None:
                        popt, pcov = curve_fit(
                            fit_funcs[fit_func], zfit, yfit, p0=p0, bounds=bounds
                        )
                    else:
                        popt, pcov = curve_fit(fit_funcs[fit_func], zfit, yfit, p0=p0)
                except RuntimeError:
                    popt = [np.nan, 0.1667]
                    pcov = [np.nan]

                # Extract exponent
                if lower_method == "optimize":
                    self.exponent = popt[1]
                    if self.exponent is None or self.exponent < 0.05:
                        self.exponent = 0.05

                # Compute confidence intervals
                if len(zfit[ok_]) > 2:
                    n = len(zfit)  # number of data points

                    t_val = t.ppf(0.975, n - 2)

                    # Get 95% confidence intervals
                    lower = popt[-1] - t_val * np.sqrt(np.diag(pcov)[-1])
                    upper = popt[-1] + t_val * np.sqrt(np.diag(pcov)[-1])
                    self.exponent_95_ci = np.hstack([lower, upper])

                    # Get the rsquared for the model
                    ss_tot = np.sum((speed[idx_power] - np.mean(yfit)) ** 2)
                    ss_res = np.sum(
                        (speed[idx_power] - fit_funcs[fit_func](zfit, *popt)) ** 2
                    )
                    self.r_squared = 1 - (ss_res / ss_tot)
                else:
                    self.exponent_95_ci = [np.nan, np.nan]
                    self.r_squared = np.nan

            # Fit power curve to appropriate data
            if idx_power.shape[0] < idxz.shape[0]:
                # If no slip compute coefficient using cells in lower 20% of depth
                idx_power, method = self.no_slip_idx(vertical, "manual", avg_z, idxz)

            self.coef = (
                (self.exponent + 1) * np.nansum(speed[idx_power] * cell_size[idx_power])
            ) / np.nansum(
                (
                    (avg_z[idx_power] + (0.5 * cell_size[idx_power]))
                    ** (self.exponent + 1)
                    - (
                        (avg_z[idx_power] - (0.5 * cell_size[idx_power]))
                        ** (self.exponent + 1)
                    )
                )
            )

            # Compute residuals
            self.residuals = (
                speed[idx_power] - self.coef * avg_z[idx_power] ** self.exponent
            )
            if self.residuals is None:
                self.residuals = np.array([])

            # Compute values (velocity or discharge) based on exponent and compute
            # coefficient
            self.u = self.coef * self.z**self.exponent
            if type(z_top) == np.ndarray:
                self.u = np.append(self.u, u_top)
                self.z = np.append(self.z, z_top)

            # Assign variables to object properties
            self.top_method = top
            self.bot_method = bot
            self.exp_method = method

        else:
            # If not data are valid simply apply methods
            self.exponent = np.nan
            self.exponent_95_ci = [np.nan, np.nan]
            self.r_squared = np.nan
            self.file_name = vertical.location_m
            self.top_method = top
            self.bot_method = bot
            self.exp_method = method

    @staticmethod
    def no_slip_idx(vertical, method, avg_z, idxz):
        """Computes the indices used to compute an optimized or final no slip fit.

        Parameters
        ----------
        vertical: Vertical
            Object of class vertical
        method: str
            Specifies if fit should be optimized
        avg_z: np.array(float)
            z values for profile
        idxz: np.array(int)
            indices of profile data
        """
        # Optimize no slip using 30% of the cells if sufficient cells are available
        if method.lower() == "optimize":
            idx = idxz[int(len(idxz) - np.floor(len(avg_z[idxz]) / 3)) : :]
            if len(idx) < 4:
                method = "default"

        # Otherwise compute No Slip using WinRiver II and
        # RiverSurveyor Live default cells
        else:
            idx = np.where(
                np.round(avg_z[idxz], 2)
                <= np.round(0.2 * (vertical.depth_m - vertical.ws_to_slush_bottom_m), 2)
            )[0]
            if len(idx) < 1:
                idx = np.array([idxz[-1]])
            elif len(idx) == 1:
                idx = idxz[idx]
            else:
                idx = idxz[idx]

        return idx, method
