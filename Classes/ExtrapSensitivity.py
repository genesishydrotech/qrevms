import numpy as np
import copy


class ExtrapSensitivity(object):
    """Class computes the sensitivity on the mean velocity of the various fit options

    Attributes
    ----------
    ice_exp: float
        Exponent for ice fit
    man_bot: str
        Manually specified bottom method
    man_exp: float
        Manually specified exponent
    man_ice_exp: float
        Manually specified exponent for ice
    man_top: str
        Manually specified top method
    ns_exp: float
        Optimized no slip Exponent
    pp_exp: float
        Optimized power power exponent
    v_3p_ns: float
        Mean speed 3-pt no slip
    v_3p_ns_opt: float
        Mean speed 3-pt optimized no slip
    v_3p_ns_opt_per_diff: float
        3-point optimized no slip percent difference from reference
    v_3p_ns_per_diff: float
        3-point no skip percent difference from reference
    v_cns: float
        Mean speed constant no RoutingSlipDelivery
    v_cns_opt: float
        Mean speed constant optimized no slip
    v_cns_opt_per_diff: float
        Constant optimized no slip percent difference from reference
    v_cns_per_diff: float
        Constant no slip percent difference from reference
    v_ice_ns: float
        Mean speed assuming no slip / no slip 1/6
    v_ice_ns_opt: float
        Mean speed assuming no slip / no slip optimized exponents equal
    v_ice_ns_opt_per_diff: float
        Ice optimized equal exponents percent difference from reference
    v_ice_ns_per_diff: float
        Ice default percent difference from reference
    v_man: float
        Mean speed for manually specified extrapolations
    v_man_per_diff: float
        Manually specified extrapolations percent difference from reference
    v_pp: float
        Mean speed power power 1/6
    v_pp_opt: float
        Mean speed power power optimized
    v_pp_opt_per_diff: float
        Power power optimized percent difference from reference
    v_pp_per_diff: float
        Power power 1/6 difference from reference
    """

    def __init__(self):
        self.v_pp = np.nan
        self.v_pp_opt = np.nan
        self.v_cns = np.nan
        self.v_cns_opt = np.nan
        self.v_3p_ns = np.nan
        self.v_3p_ns_opt = np.nan
        self.v_ice_ns = np.nan
        self.v_ice_ns_per_diff = np.nan
        self.v_ice_ns_opt = np.nan
        self.v_ice_ns_opt_per_diff = np.nan
        self.v_pp_per_diff = np.nan
        self.v_pp_opt_per_diff = np.nan
        self.v_cns_per_diff = np.nan
        self.v_cns_opt_per_diff = np.nan
        self.v_3p_ns_per_diff = np.nan
        self.v_3p_ns_opt_per_diff = np.nan
        self.pp_exp = np.nan
        self.ns_exp = np.nan
        self.ice_exp = np.nan
        self.man_top = ""
        self.man_bot = ""
        self.man_exp = np.nan
        self.man_ice_exp = np.nan
        self.v_man = np.nan
        self.v_man_per_diff = np.nan

    def populate_data(self, vertical):
        """Populate the data structure.

        Parameters
        ----------
        vertical: Vertical
            Object of class Vertical
        """

        # Copy vertical to avoid changing original
        vertical_copy = copy.deepcopy(vertical)

        self.v_man = np.nan

        if vertical.ws_condition == "Open":
            # Power/Power/Default
            self.v_pp = self.compute_mean_speed(
                vertical_copy, top_method="Power", bot_method="Power", exp=0.1667
            )
            self.v_pp_per_diff = (
                (self.v_pp - vertical.mean_speed_mps) / vertical.mean_speed_mps
            ) * 100

            # Power/Power/Optimized
            self.v_pp_opt = self.compute_mean_speed(
                vertical_copy,
                top_method="Power",
                bot_method="Power",
                exp=vertical.extrapolation.extrap_fit.sel_fit.pp_exponent,
            )
            self.v_pp_opt_per_diff = (
                (self.v_pp_opt - vertical.mean_speed_mps) / vertical.mean_speed_mps
            ) * 100

            # Constant/No Slip/Default
            self.v_cns = self.compute_mean_speed(
                vertical_copy, top_method="Constant", bot_method="No Slip", exp=0.1667
            )
            self.v_cns_per_diff = (
                (self.v_cns - vertical.mean_speed_mps) / vertical.mean_speed_mps
            ) * 100

            # Constant/No Slip/Optimized
            self.v_cns_opt = self.compute_mean_speed(
                vertical_copy,
                top_method="Constant",
                bot_method="No Slip",
                exp=vertical.extrapolation.extrap_fit.sel_fit.ns_exponent,
            )
            self.v_cns_opt_per_diff = (
                (self.v_cns_opt - vertical.mean_speed_mps) / vertical.mean_speed_mps
            ) * 100

            # 3-Point/No Slip/Default
            self.v_3p_ns = self.compute_mean_speed(
                vertical_copy, top_method="3-Point", bot_method="No Slip", exp=0.1667
            )
            self.v_3p_ns_per_diff = (
                (self.v_3p_ns - vertical.mean_speed_mps) / vertical.mean_speed_mps
            ) * 100

            # 3-Point/No Slip/Optimized
            self.v_3p_ns_opt = self.compute_mean_speed(
                vertical_copy,
                top_method="3-Point",
                bot_method="No Slip",
                exp=vertical.extrapolation.extrap_fit.sel_fit.ns_exponent,
            )
            self.v_3p_ns_opt_per_diff = (
                (self.v_3p_ns_opt - vertical.mean_speed_mps) / vertical.mean_speed_mps
            ) * 100

        # Ice is present
        else:
            # Ice Default / No Slip Default
            self.v_ice_ns = self.compute_mean_speed(
                vertical_copy,
                top_method="Ice",
                bot_method="No Slip",
                exp=0.1667,
                ice_exp=0.1667,
            )
            self.v_ice_ns_per_diff = (
                (self.v_ice_ns - vertical.mean_speed_mps) / vertical.mean_speed_mps
            ) * 100

            # Ice Default / No Slip Optimized
            self.v_ice_ns_opt = self.compute_mean_speed(
                vertical_copy,
                top_method="Ice",
                bot_method="No Slip",
                exp=vertical.extrapolation.extrap_fit.sel_fit.ns_exponent,
                ice_exp=0.1667,
            )
            self.v_ice_ns_opt_per_diff = (
                (self.v_ice_ns_opt - vertical.mean_speed_mps) / vertical.mean_speed_mps
            ) * 100

        if vertical.extrapolation.extrap_fit.fit_method == "Manual":
            self.v_man = self.compute_mean_speed(
                vertical_copy,
                top_method=vertical.extrapolation.top_method,
                bot_method=vertical.extrapolation.bot_method,
                exp=vertical.extrapolation.exponent,
                ice_exp=vertical.extrapolation.ice_exponent,
            )
            self.v_man_per_diff = (
                (self.v_man - vertical.mean_speed_mps) / vertical.mean_speed_mps
            ) * 100
            self.man_top = vertical.extrapolation.top_method
            self.man_bot = vertical.extrapolation.bot_method
            self.man_exp = vertical.extrapolation.exponent
            self.man_ice_exp = vertical.extrapolation.ice_exponent

        self.pp_exp = vertical.extrapolation.extrap_fit.sel_fit.pp_exponent
        self.ns_exp = vertical.extrapolation.extrap_fit.sel_fit.ns_exponent
        self.ice_exp = vertical.extrapolation.extrap_fit.sel_fit.ice_exponent

    @staticmethod
    def compute_mean_speed(vertical_temp, top_method, bot_method, exp, ice_exp=None):
        """Computes the mean speed for a vertical profile to be used in
        sensitivity assessment

        Parameters
        ----------
        vertical_temp: Vertical
            Temporary copy of Vertical object
        top_method: str
            Specifies top extrapolation method
        bot_method: str
            Specifies bottom extrapolation method
        exp: float
            Exponent for power and no slip methods
        ice_exp: float
            Exponent for ice method
        """
        valid_data = vertical_temp.mean_profile["number_ensembles"] > 0
        vertical_temp.mean_profile["speed"] = vertical_temp.mean_profile["speed"][
            valid_data
        ]
        vertical_temp.mean_profile["cell_depth"] = vertical_temp.mean_profile[
            "cell_depth"
        ][valid_data]
        vertical_temp.mean_profile["cell_size"] = vertical_temp.mean_profile[
            "cell_size"
        ][valid_data]
        vertical_temp.mean_profile["number_ensembles"] = vertical_temp.mean_profile[
            "number_ensembles"
        ][valid_data]
        vertical_temp.compute_top_bot(
            top_method=top_method, bot_method=bot_method, exp=exp, ice_exp=ice_exp
        )
        vertical_temp.compute_mean_speed()
        return vertical_temp.mean_speed_mps
