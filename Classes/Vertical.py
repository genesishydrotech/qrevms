import numpy as np
from datetime import datetime, timezone
from Modules.common_functions import (
    cart2pol,
    rad2azdeg,
    cosd,
    azdeg2rad,
    pol2cart,
    nan_less,
    nan_greater_equal,
    nan_less_equal,
)
from Modules.local_time_utilities import tz_formatted_string, local_time_from_iso
from Classes.VerticalData import VerticalData
from Classes.Extrapolation import Extrapolation


class Vertical(object):
    """Stores data and settings for a vertical profile.

    Attributes
    ----------
    adcp_depth_below_ice_m: float
        Depth of ADCP below ice in m
    adcp_depth_below_ice_orig_m: float
        Original depth of ADCP below ice in m
    adcp_depth_below_ws_m: float
        Depth of ADCP below the water surface, in m
    adcp_depth_below_ws_orig_m: float
        Original depth of ADCP below the water surface, in m
    battery_voltage: float
        Average battery voltage for vertical
    beam_3_misalignment_deg: float
        Adjustment to beam 3 direction for ADCP meter method
    beam_3_rotation_deg: float
        Additional rotation of beam 3 for this vertical,
        will be added to beam_3_misalignment, fixed method only
    cell_minimum_valid_ensembles: int
        Minimum number of valid ensembles for a cell to be include in the profile
    cell_minimum_valid_ensembles_orig: int
        Original value of valid ensembles for a cell to be include in the profile
    data: VerticalData
        Object of VerticalData
    depth_m: float
        Depth of vertical, in m
    depth_orig_m: float
        Original depth of the vertical, in m
    depth_source: str
        Source of depth (Manual, BT, BT Composite, VB, VB Composite)
    edge_coef: float
        Coefficient used to compute edge discharge for mean section
    edge_coef_orig: float
        Original edge coefficient from manufacturer data file
    edge_loc: str
        Edge location (Left, Right, Island)
    edge_loc_orig: str
        Original edge location from manufacturer data file
    ensemble_profiles: dict
        Defines the profile for each ensemble with cell_depth, cell_size, and speed
    extrapolation: Extrapolation
        Extrapolation settings for mean profile
    flow_angle_coefficient: float
            Coefficient to correct for flow angle
    gh_m: float
        Observed stage or gauge height during this vertical, in m
    gh_orig_m: float
        Original gauge height from manufacturer data file
    gh_obs_time: str
        Time of observed stage or gauge height
    gh_obs_time_orig: str
        Original time of observed stage or gauge height from manufacturer data file
    ice_thickness_m: float
        Thickness of ice, in m
    ice_thickness_orig_m: float
        Original thickness of ice from manufacturer data file, in m
    is_edge: bool
        Indicates if the vertical is an edge
    kmeans: bool
        Indicates if kmeans filter should be used on this vertical
    lat_deg: float
        Latitude, in decimal degrees
    location_m: float
        Station distance from reference, in m
    location_source: str
        Source used to determine location (Manual or GPS)
    location_orig_m: float
        Original station distance from reference, in m
    lon_deg: float
        Longitude, in decimal degrees
    mean_profile: dict
        Defines the mean profile with cell_depth, cell_size, number_ensembles, and speed
    mean_measured_speed_cv: float
        Coefficient of variation for the mean speed using the mean ensemble speeds
    mean_measured_speed_mps: float
        Mean speed of middle or measured portion of the vertical,
        includes all corrections, m/s
    mean_speed_mps: float
        Mean speed for the vertical including top, middle,
        and bottom and all corrections, m/s
    mean_speed_source: str
        Source of mean velocity (ADCP, Point, Manual, Est)
    mean_speed_source_orig: str
        Original source from manufacturer data file
    mean_velocity_angle_deg: float
        Angle of flow relative to Fixed: instrument direction,
        Azimuth: perpendicular to tagline, Magnitude: manual
    mean_velocity_direction_deg: float
        Direction of flow relative to north or beam 3 in absence of compass
    mean_velocity_direction_stdev_deg: float
        Standard deviation of the flow direction
    mean_velocity_magnitude_mps: float
        Mean velocity magnitude of the measured portion of the vertical
    mean_velocity_magnitude_cv: float
        Coefficient of variation of the velocity magnitude
    number_of_ensembles: int
        Number of ensembles in the vertical
    number_of_valid_ensembles: int
        Number of ensembles with valid velocity data
    number_valid_cells: int
        Number of valid cells used to compute the profile
    profile_method: str
        Method used to compute the mean profile (Components, DS_Mag )
    sampling_duration_sec: int
        Duration of data collected at this vertical, in seconds
    settings_orig: dict
        Original settings from manufacturer software used for comparisons only
    tagline_azimuth_deg: float
        Azimuth of tagline relative to magnetic North, in degrees
    time_end: str
        End time based on last measured vertical used
    time_start: str
        Start time based on 1st measured vertical used
    use: bool
        Indicates if vertical is used to compute discharge
    use_measurement_thresholds: bool
        Indicates if the entire measurement should be used to set filter thresholds
    use_orig: bool
        Original setting for use
    use_ping_type: bool
        Indicates if ping types should be used in BT and WT filters
    velocity_correction: float
        Correction for velocity at a boundary or in a manual vertical with depth other than zero
    velocity_correction_orig: float
        Original velocity correction from manufacturer data file, SonTek=1, set to 0
    velocity_method: str
        Method used to compute mean speed from velocity data
        (magnitude, azimuth, fixed, manual)
    velocity_method_orig: str
        Original velocity method from manufacturer data file
    velocity_reference: str
        Reference used to compute velocity (Inst or BT)
    velocity_reference_orig: str
        Original velocity reference from manufacture data file
    ws_condition: str
        Condition of water surface (Open, Ice)
    ws_condition_orig: str
        Original water surface condition from manufacturer data file
    ws_to_ice_bottom_m: float
        Depth from water surface to bottom of ice, in m
    ws_to_ice_bottom_orig_m: float
        Original depth from water surface to bottom of ice, in m
    ws_to_slush_bottom_m: float
        Depth from water surface to bottom of slush, in m
    ws_to_slush_bottom_orig_m: float
        Original depth from water surface to bottom of slush, in m
    """

    def __init__(self, cell_minimum_valid_ensembles=8, kmeans=False):
        """Initialize class attributes."""

        self.beam_3_rotation_deg = 0
        self.beam_3_misalignment_deg = 0
        self.ws_condition = ""
        self.tagline_azimuth_deg = np.nan
        self.use = True
        self.use_orig = True
        self.use_ping_type = True
        self.use_measurement_thresholds = False
        self.is_edge = False
        self.edge_loc = ""
        self.velocity_correction = 0
        self.mean_velocity_magnitude_mps = np.nan
        self.mean_velocity_magnitude_cv = np.nan
        self.mean_speed_source = ""
        self.mean_speed_source_orig = self.mean_speed_source
        self.mean_velocity_direction_deg = np.nan
        self.mean_velocity_direction_stdev_deg = np.nan
        self.mean_velocity_angle_deg = np.nan
        self.velocity_reference = ""
        self.velocity_method = ""
        self.mean_measured_speed_mps = np.nan
        self.mean_speed_mps = np.nan
        self.mean_measured_speed_cv = np.nan
        self.flow_angle_coefficient = 1.0
        self.depth_m = np.nan
        self.depth_orig_m = np.nan
        self.depth_source = ""
        self.location_m = np.nan
        self.adcp_depth_below_ws_m = 0.0
        self.ice_thickness_m = 0.0
        self.ws_to_ice_bottom_m = 0.0
        self.ws_to_slush_bottom_m = 0.0
        self.adcp_depth_below_ice_m = 0.0
        self.sampling_duration_sec = 0
        self.gh_m = np.nan
        self.gh_obs_time = ""
        self.lat_deg = np.nan
        self.lon_deg = np.nan
        self.number_valid_cells = 0
        self.number_of_ensembles = 0
        self.number_of_valid_ensembles = 0
        self.battery_voltage = np.nan
        self.cell_minimum_valid_ensembles = cell_minimum_valid_ensembles
        self.cell_minimum_valid_ensembles_orig = self.cell_minimum_valid_ensembles
        self.profile_method = "Components"
        self.time_end = ""
        self.time_start = ""
        self.velocity_method_orig = self.velocity_method
        self.velocity_reference_orig = self.velocity_reference
        self.gh_orig_m = self.gh_m
        self.gh_obs_time_orig = self.gh_obs_time
        self.location_orig_m = self.location_m
        self.ws_condition_orig = self.ws_condition
        self.ensemble_profiles = {
            "cell_depth": np.array([]),
            "cell_size": np.array([]),
            "speed": np.array([]),
            "u": np.array([]),
            "v": np.array([]),
        }
        self.mean_profile = {
            "cell_depth": np.array([]),
            "cell_size": np.array([]),
            "speed": np.array([]),
            "number_ensembles": np.array([]),
            "u": np.array([]),
            "v": np.array([]),
            "rssi": np.array([]),
        }
        self.extrapolation = Extrapolation()
        self.data = VerticalData()
        self.adcp_depth_below_ice_orig_m = self.adcp_depth_below_ice_m
        self.adcp_depth_below_ws_orig_m = self.adcp_depth_below_ws_m
        self.ice_thickness_orig_m = self.ice_thickness_m
        self.ws_to_ice_bottom_orig_m = self.ws_to_ice_bottom_m
        self.ws_to_slush_bottom_orig_m = self.ws_to_slush_bottom_m
        self.edge_coef = np.nan
        self.edge_loc_orig = ""
        self.edge_loc_orig = np.nan
        self.velocity_correction_orig = 0
        self.edge_coef_orig = np.nan
        self.settings_orig = dict()
        self.location_source = "Manual"
        self.kmeans = kmeans

    def edge(
        self,
        is_edge,
        edge_loc,
        depth_m,
        depth_source,
        station_m,
        velocity_correction,
        edge_coef,
        use=True,
        gh_m=np.nan,
        gh_obs_time="",
    ):
        """Assigns data to an edge vertical.

        Parameters
        ----------
        is_edge: bool
            Indicates if the vertical is an edge
        edge_loc: str
            Edge location (Left, Right, Island)
        depth_m: float
            Depth of vertical
        depth_source: str
            Source of depth (Computed or Manual)
        station_m: float
            Station from reference for vertical
        velocity_correction: float
            Correction for velocity at a boundary with depth other than zero
        edge_coef: float
            Coefficient used to compute edge discharge for mean section method
        use: bool
            Indicates if vertical is used to compute discharge
        gh_m: float
            Observed stage or gauge height during this vertical
        gh_obs_time: str
            Time of observed state or gauge height
        """

        self.is_edge = is_edge
        self.edge_loc = edge_loc
        self.edge_loc_orig = edge_loc
        self.use = use
        self.use_orig = use
        self.depth_m = depth_m
        self.depth_orig_m = depth_m
        self.depth_source = depth_source
        self.location_m = station_m
        self.location_orig_m = station_m
        self.velocity_correction = velocity_correction
        self.velocity_correction_orig = velocity_correction
        self.gh_m = gh_m
        self.gh_orig_m = gh_m
        self.gh_obs_time = gh_obs_time
        self.gh_obs_time_orig = gh_obs_time
        self.edge_coef = edge_coef
        self.edge_coef_orig = edge_coef

    def from_trdi(
        self,
        mmt,
        cfg,
        section,
        idx,
        pd0_sections,
        beam_3_misalignment,
        tagline_azimuth,
        excluded,
        date_format,
    ):
        """Assigns TRDI data to class attributes.

        Parameters
        ----------
        mmt: dict
            Dictionary of full mmt file
        cfg: dict
            Dictionary of latest configuration node from mmt file
        section: dict
            Dictionary of section data from mmt file
        idx: int
            Index identifying the measured vertical in the pd0 file
        pd0_sections: list
            List of pd0 data by section
        beam_3_misalignment: float
            Adjustment to beam 3 direction for ADCP meter method
        tagline_azimuth: float
            Azimuth of tagline relative to magnetic North, in degrees
        excluded: dict
            Dictionary of excluded top distances for selected ADCPs
        date_format: str
            String defining date format
        """

        # Is section to be used to computer discharge
        if section["@Checked"] == "1":
            self.use = True
            self.use_orig = True
        else:
            self.use = False
            self.use_orig = False

        # Basic section data
        self.mean_speed_source = "ADCP"
        self.mean_speed_source_orig = self.mean_speed_source
        self.sampling_duration_sec = float(section["SampleTime"])
        self.location_m = float(section["DistanceFromIP"])
        self.beam_3_misalignment_deg = beam_3_misalignment
        self.tagline_azimuth_deg = tagline_azimuth

        # Velocity reference
        self.velocity_reference = "None"
        if mmt["Site_Information"]["Reference"] == "BT":
            self.velocity_reference = "BT"

        # Velocity method
        method = {"0": "Magnitude", "1": "Fixed", "2": "Azimuth"}
        self.velocity_method = method[cfg["Processing"]["VelocityMethod"]]

        # Approximate section location
        if "GPSLatitude" in section.keys():
            self.lat_deg = float(section["GPSLatitude"])
            self.lon_deg = float(section["GPSLongitude"])

        # Angle for correction, entered by user
        if float(section["SectionFlowAngle"]) != 0:
            if self.velocity_method == "Fixed":
                self.beam_3_rotation_deg = float(section["SectionFlowAngle"])
            elif self.velocity_method == "Magnitude":
                self.flow_angle_coefficient = cosd(float(section["SectionFlowAngle"]))

        # Minimum number of valid ensembles required to use a cell in the profile
        self.cell_minimum_valid_ensembles_orig = int(
            cfg["Processing"]["Good_Ens_In_Cell_Threshold"]
        )
        if self.cell_minimum_valid_ensembles_orig > self.cell_minimum_valid_ensembles:
            self.cell_minimum_valid_ensembles = self.cell_minimum_valid_ensembles_orig

        # Raw and processed time series data for each section
        self.data = VerticalData()

        # Measured section
        # ----------------
        if section["SectionMadeUp"] == "NO":
            pd0_section = pd0_sections[idx]
            self.data.from_trdi(mmt, cfg, section, pd0_section, excluded, date_format)
            self.number_of_ensembles = self.data.w_vel.u_processed_mps.shape[1]

            # Time
            self.time_start = datetime.utcfromtimestamp(
                self.data.date_time.start_serial_time
            ).strftime("%H:%M:%S.%f")
            self.time_end = datetime.utcfromtimestamp(
                self.data.date_time.end_serial_time
            ).strftime("%H:%M:%S.%f")
            self.sampling_duration_sec = np.nansum(self.data.date_time.ens_duration_sec)

            # Voltage
            if self.data.sensors.battery_voltage.internal.data is not None:
                self.battery_voltage = np.nanmean(
                    self.data.sensors.battery_voltage.internal.data
                )

            # TRDI only has one entry for gage height. It is repeated here for each
            # vertical. Time is not provided.
            self.gh_m = float(mmt["Site_Information"]["Outside_Gage_Height"])

            # Depth
            depth_data = getattr(self.data.depths, self.data.depths.selected)

            # Use only data with valid velocities unless Manual depth is provided
            valid_velocities = np.any(
                np.logical_not(np.isnan(self.data.w_vel.u_processed_mps)), axis=0
            )
            self.number_of_valid_ensembles = np.nansum(valid_velocities)
            self.depth_m = np.nanmean(depth_data.depth_processed_m[valid_velocities])
            self.depth_orig_m = self.depth_m
            if self.data.depths.selected == "man_depths":
                self.depth_source = "Manual"
                self.depth_m = np.nanmean(depth_data.depth_processed_m)
            elif self.data.depths.selected == "bt_depths":
                self.depth_source = "BT"
                if self.data.depths.composite == "On":
                    self.depth_source = self.depth_source + " Composite"
            elif self.data.depths.selected == "vb_depths":
                self.depth_source = "VB"
                if self.data.depths.composite == "On":
                    self.depth_source = self.depth_source + " Composite"

            # Default water surface condition
            self.ws_condition = "Open"

            # Open water transducer depth
            depths_selected = getattr(self.data.depths, self.data.depths.selected)
            self.adcp_depth_below_ws_m = depths_selected.draft_use_m

            # Extrapolation settings
            # ----------------------
            exponent = 0.1667
            if "Power_Exponent" in section:
                exponent = float(section["Power_Exponent"])
            elif "Power_Curve_Coef" in cfg["Discharge"]:
                exponent = float(cfg["Discharge"]["Power_Curve_Coef"])

            # Normal: Power / Power
            if section["DischargeMethod"] == "0":
                self.extrapolation.top_method = "Power"
                self.extrapolation.top_method_orig = "Power"
                self.extrapolation.bot_method = "Power"
                self.extrapolation.bot_method_orig = "Power"
                self.extrapolation.exponent = exponent
                self.extrapolation.exponent_orig = exponent

            # Wind: Constant / No Slip
            elif section["DischargeMethod"] == "1":
                self.extrapolation.top_method = "Constant"
                self.extrapolation.top_method_orig = "Constant"
                self.extrapolation.bot_method = "No Slip"
                self.extrapolation.bot_method_orig = "No Slip"
                self.extrapolation.exponent = exponent
                self.extrapolation.exponent_orig = exponent

            # Ice: No Slip / No Slip
            elif section["DischargeMethod"] == "2":
                self.extrapolation.top_method = "Ice"
                self.extrapolation.top_method_orig = "Ice"
                self.extrapolation.ice_exponent = exponent
                self.extrapolation.ice_exponent_orig = exponent
                self.extrapolation.bot_method = "No Slip"
                self.extrapolation.bot_method_orig = "No Slip"
                self.extrapolation.exponent = exponent
                self.extrapolation.exponent_orig = exponent

                # Ice information
                # Use of ice information in SxS Pro is triggered by extrapolation setting.
                self.ice_thickness_m = float(section["SolidIceThickness"])
                self.ws_to_ice_bottom_m = float(section["WS_ToBottomOfSolidIce"])
                self.ws_to_slush_bottom_m = float(section["WS_ToBottomOfSlush"])
                self.ws_condition = "Ice"
                self.adcp_depth_below_ice_m = float(section["XDucerDepth"])
                self.adcp_depth_below_ws_m = (
                    self.adcp_depth_below_ice_m + self.ws_to_slush_bottom_m
                )

            # Bidirectional: 3pt / No Slip
            elif section["DischargeMethod"] == "3":
                self.extrapolation.top_method = "3-Point"
                self.extrapolation.top_method_orig = "3-Point"
                self.extrapolation.bot_method = "No Slip"
                self.extrapolation.bot_method_orig = "No Slip"
                self.extrapolation.exponent = exponent
                self.extrapolation.exponent_orig = exponent

            # Store original settings
            self.velocity_method_orig = self.velocity_method
            self.velocity_reference_orig = self.velocity_reference
            self.gh_orig_m = self.gh_m
            self.gh_obs_time_orig = self.gh_obs_time
            self.location_orig_m = self.location_m
            self.ws_condition_orig = self.ws_condition
            self.adcp_depth_below_ice_orig_m = self.adcp_depth_below_ice_m
            self.adcp_depth_below_ws_orig_m = self.adcp_depth_below_ws_m
            self.ice_thickness_orig_m = self.ice_thickness_m
            self.ws_to_ice_bottom_orig_m = self.ws_to_ice_bottom_m
            self.ws_to_slush_bottom_orig_m = self.ws_to_slush_bottom_m

            # Store original settings for batch comparison use
            self.trdi_store_original_settings(cfg, section)

            # Apply default settings
            settings = self.qrev_default_settings()
            self.compute_profile(settings)

        # Manual section
        # --------------
        else:
            if section["DischargeMethod"] == "2":
                ws_condition = "Ice"
            else:
                ws_condition = "Open"
            self.from_user_input(
                location=self.location_m,
                depth=float(section["ManualDepth"]),
                velocity=float(section["ManualSpeed"]),
                ws_condition=ws_condition,
                ice_thickness_m=float(section["SolidIceThickness"]),
                ws_to_ice_bottom_m=float(section["WS_ToBottomOfSolidIce"]),
                ws_to_slush_bottom_m=float(section["WS_ToBottomOfSlush"]),
                use=self.use,
            )

    def from_matst(
        self,
        sontek_data,
        n,
        stations_item,
        data_indices,
        cell_start,
        excluded,
        snr_3beam_comp,
        date_format,
    ):
        """Assigns RSSL Matlab data to class attributes.

        Parameters
        ----------
        sontek_data: dict
            Dictionary of Matlab structures
        n: int
            Vertical number
        stations_item: mat_struct
            Station information
        data_indices: np.array(int)
            Array of indices to raw data for this vertical
        cell_start: float
            Top of cell including draft
        excluded: dict
            Dictionary of excluded top distances for selected ADCPs
        snr_3beam_comp: bool
            Indicates if invalid data due to SNR filter should be computed using 3-beam
            solution
        date_format: str
            String defining date format
        """

        if sontek_data["StationStation"].Island_Edge[n]:
            self.matst_island_edge(sontek_data, stations_item, n)
        else:
            # Is section to be used to computer discharge
            if stations_item.Active:
                self.use = True
                self.use_orig = True
            else:
                self.use = False
                self.use = False

            # Basic section data
            self.mean_speed_source = "ADCP"
            self.mean_speed_source_orig = self.mean_speed_source
            self.sampling_duration_sec = data_indices.shape[0]
            self.location_m = sontek_data["StationStation"].Location[n]
            self.location_orig_m = self.location_m
            self.tagline_azimuth_deg = sontek_data["Setup"].transectAzimuth

            # Velocity reference
            self.velocity_reference = "None"
            if int(sontek_data["Setup"].velocityReference) == 1:
                self.velocity_reference = "BT"
            self.velocity_reference_orig = self.velocity_reference

            # Velocity method
            # If the coordinate system = 0 this is XYZ and the velocity method is Fixed
            # If the coordinate system = 1 this is ENU and the velocity method is Azimuth
            if stations_item.CoordinateSystem == 0:
                self.velocity_method = "Fixed"
            elif stations_item.CoordinateSystem == 1:
                self.velocity_method = "Azimuth"
            self.velocity_method_orig = self.velocity_method

            # Approximate section location, this will be updated when GPS data are processed
            if "StationGPS" in sontek_data:
                self.lat_deg = sontek_data["StationGPS"].Latitude[n]
                self.lon_deg = sontek_data["StationGPS"].Longitude[n]

            if hasattr(sontek_data["StationStation"], "Use_Gps"):
                if sontek_data["StationStation"].Use_Gps[n]:
                    self.location_source = "GPS"
            # Depth data
            self.depth_source = "Composite"

            # ADCP depth
            self.adcp_depth_below_ws_m = stations_item.SensorDepth / 10000.0
            self.adcp_depth_below_ws_orig_m = self.adcp_depth_below_ws_m

            # Ice information
            self.ice_thickness_m = stations_item.IceThicknessInMM / 10000.0
            self.ice_thickness_orig_m = self.ice_thickness_m
            self.ws_to_ice_bottom_m = stations_item.IceDepthInTNTHMM / 10000.0
            self.ws_to_ice_bottom_orig_m = self.ws_to_ice_bottom_m

            if stations_item.SurfaceType > 1:
                self.ws_to_slush_bottom_m = stations_item.SlushDepthInTNTHMM / 10000.0
            else:
                self.ws_to_slush_bottom_m = 0

            if self.ws_to_ice_bottom_m > self.ws_to_slush_bottom_m:
                self.ws_to_slush_bottom_m = self.ws_to_ice_bottom_m
            self.ws_to_slush_bottom_orig_m = self.ws_to_slush_bottom_m
            self.adcp_depth_below_ice_m = (
                self.adcp_depth_below_ws_m - self.ws_to_slush_bottom_m
            )
            self.adcp_depth_below_ws_orig_m = self.adcp_depth_below_ws_m
            self.adcp_depth_below_ice_orig_m = self.adcp_depth_below_ice_m

            # Raw and processed time series data for each section
            self.data = VerticalData()
            if data_indices.shape[0] > 0:
                self.data.from_matst(
                    sontek_data,
                    stations_item,
                    data_indices,
                    cell_start,
                    excluded,
                    snr_3beam_comp,
                    date_format,
                )

                # Number of ensembles
                self.number_of_ensembles = self.data.w_vel.u_processed_mps.shape[1]

                # Time
                self.time_start = datetime.utcfromtimestamp(
                    self.data.date_time.start_serial_time
                ).strftime("%H:%M:%S.%f")
                self.time_end = datetime.utcfromtimestamp(
                    self.data.date_time.end_serial_time
                ).strftime("%H:%M:%S.%f")
                self.sampling_duration_sec = np.nansum(
                    self.data.date_time.ens_duration_sec
                )

                # Voltage
                if self.data.sensors.battery_voltage.internal.data is not None:
                    self.battery_voltage = np.nanmean(
                        self.data.sensors.battery_voltage.internal.data
                    )

                # Gauge height
                self.gh_m = stations_item.GaugeHeightInTNTHMM / 10000.0
                if stations_item.GaugeHeightObservationTime > 0:
                    self.gh_obs_time = datetime.utcfromtimestamp(
                            stations_item.GaugeHeightObservationTime
                            + ((30 * 365) + 7) * 24 * 60 * 60
                        ).strftime("%H:%M:%S")
                else:
                    self.gh_obs_time = ""
                self.gh_orig_m = self.gh_m
                self.gh_obs_time_orig = self.gh_obs_time

                # Depth
                depth_data = getattr(self.data.depths, self.data.depths.selected)

                # Initially use only data with valid velocities unless Manual depth is provided
                valid_velocities = np.any(
                    np.logical_not(np.isnan(self.data.w_vel.u_processed_mps)), axis=0
                )
                self.number_of_valid_ensembles = np.nansum(valid_velocities)
                self.depth_m = np.nanmean(
                    depth_data.depth_processed_m[valid_velocities]
                )
                self.depth_orig_m = self.depth_m
                if self.data.depths.selected == "man_depths":
                    self.depth_source = "Manual"
                    self.depth_m = np.nanmean(depth_data.depth_processed_m)
                elif self.data.depths.selected == "bt_depths":
                    self.depth_source = "BT"
                    if self.data.depths.composite == "On":
                        self.depth_source = self.depth_source + " Composite"
                elif self.data.depths.selected == "vb_depths":
                    self.depth_source = "VB"
                    if self.data.depths.composite == "On":
                        self.depth_source = self.depth_source + " Composite"

                # Water surface condition
                if stations_item.SurfaceType == 0:
                    self.ws_condition = "Open"
                else:
                    self.ws_condition = "Ice"
                self.ws_condition_orig = self.ws_condition

                self.extrapolation.top_method = self.data.extrap.top_method
                self.extrapolation.top_method_orig = self.data.extrap.top_method
                self.extrapolation.ice_exponent = self.data.extrap.ice_exponent
                self.extrapolation.ice_exponent_orig = self.data.extrap.ice_exponent
                self.extrapolation.bot_method = self.data.extrap.bot_method
                self.extrapolation.bot_method_orig = self.data.extrap.bot_method
                self.extrapolation.exponent = self.data.extrap.exponent
                self.extrapolation.exponent_orig = self.data.extrap.exponent

                # Open water transducer depth
                depths_selected = getattr(self.data.depths, self.data.depths.selected)
                self.adcp_depth_below_ws_m = depths_selected.draft_use_m

                # Apply default settings
                settings = self.qrev_default_settings()
                self.compute_profile(settings)

    def matst_island_edge(self, sontek_data, stations_item, n):
        """Creates a manual station for an island edge from an RSSL mat file.

        Parameters
        ----------
        sontek_data: dict
            Dictionary of Matlab structures
        stations_item: mat_struct
            Station information
        n: int
            Vertical number
        """

        # RSSL defaults the velocity correction to 1, but for a manual edge the
        # default should be zero.
        vcf = sontek_data["StationStation"].Velocity_Correction_Factor[n]
        if vcf == 1:
            vcf = 0

        # Determine if station should be used. This parameter in StationStation
        # does not appear to be correct, so using stations_item
        if stations_item.Active:
            use = True
        else:
            use = False

        # Create manual station
        self.from_user_input(
            location=sontek_data["StationStation"].Location[n],
            depth=sontek_data["StationStation"].Station_Depth[n],
            velocity=np.nan,
            ws_condition="Island",
            ice_thickness_m=sontek_data["StationStation"].Ice_Thickness[n],
            ws_to_ice_bottom_m=sontek_data[
                "StationStation"
            ].Water_Surface_To_Bottom_Of_Ice[n],
            ws_to_slush_bottom_m=sontek_data[
                "StationStation"
            ].Water_Surface_To_Bottom_Of_Slus[n],
            use=use,
            velocity_correction=vcf,
        )

    def from_rsqst(
        self, station_data, adcp_data, system_configuration, snr_3beam_comp, utc_time_offset, date_format
    ):
        """Assigns data from raw RSQ data to object classes

        Parameters
        ----------
        station_data: dict
            Station information
        adcp_data: dict
            Raw ADCP data for vertical
        system_configuration: dict
            General system configuration
        snr_3beam_comp: bool
            Indicates if 3-beam solutions should be used for ensembles with invalid SNR
        utc_time_offset: str
            String containing time offset to get to local time.
        date_format: str
            String defining date format
        """

        # Compute vertical based on type
        if station_data["StationType"] == "IslandEdge":
            self.rsqst_island_edge(station_data)
        else:
            # Is section to be used to compute discharge
            self.use = station_data["UseStation"]
            self.use_orig = station_data["UseStation"]

            # Basic section data
            self.mean_speed_source = "ADCP"
            self.mean_speed_source_orig = self.mean_speed_source
            self.sampling_duration_sec = station_data["StationAveragingInterval"]
            self.location_m = station_data["Location (m)"]
            self.location_orig_m = self.location_m
            self.tagline_azimuth_deg = system_configuration["TaglineAzimuth (deg)"]

            # Velocity reference
            self.velocity_reference = "None"
            if system_configuration["TrackReference"] == "BottomTrack":
                self.velocity_reference = "BT"
            self.velocity_reference_orig = self.velocity_reference

            # Velocity method
            if station_data["EnableCompass"]:
                self.velocity_method = "Azimuth"
            else:
                self.velocity_method = "Fixed"
            self.velocity_method_orig = self.velocity_method

            # Approximate section location, average of start and stop locations
            try:
                self.lat_deg = (
                    adcp_data["Samples"][0]["Gps"]["StartLatitude"]
                    + adcp_data["Samples"][-1]["Gps"]["EndLatitude"]
                ) / 2.0
                self.lon_deg = (
                    adcp_data["Samples"][0]["Gps"]["StartLongitude"]
                    + adcp_data["Samples"][-1]["Gps"]["EndLongitude"]
                ) / 2.0
            except (TypeError, KeyError):
                self.lat_deg = np.nan
                self.lon_deg = np.nan

            # Depth data
            self.depth_source = "Composite"

            # ADCP depth
            self.adcp_depth_below_ws_m = station_data["TransducerDepth (m)"]
            self.adcp_depth_below_ws_orig_m = self.adcp_depth_below_ws_m

            # Ice information
            self.ice_thickness_m = station_data["IceThickness (m)"]
            self.ice_thickness_orig_m = self.ice_thickness_m
            self.ws_to_ice_bottom_m = station_data["WaterSurfaceToBottomOfIce (m)"]
            self.ws_to_ice_bottom_orig_m = self.ws_to_ice_bottom_m
            self.ws_to_slush_bottom_m = station_data["WaterSurfaceToBottomOfSlush (m)"]
            if self.ws_to_ice_bottom_m > self.ws_to_slush_bottom_m:
                self.ws_to_slush_bottom_m = self.ws_to_ice_bottom_m
            self.ws_to_slush_bottom_orig_m = self.ws_to_slush_bottom_m
            if self.adcp_depth_below_ws_m < self.ws_to_slush_bottom_m:
                self.adcp_depth_below_ws_m = self.adcp_depth_below_ws_m + self.ws_to_slush_bottom_m
            self.adcp_depth_below_ice_m = (
                self.adcp_depth_below_ws_m - self.ws_to_slush_bottom_m
            )
            self.adcp_depth_below_ws_orig_m = self.adcp_depth_below_ws_m
            self.adcp_depth_below_ice_orig_m = self.adcp_depth_below_ice_m

            # Raw and processed time series data for each section
            self.data = VerticalData()
            self.data.from_rsqst(
                adcp_data,
                station_data,
                system_configuration,
                snr_3beam_comp,
                utc_time_offset,
                date_format,
                self.adcp_depth_below_ws_m
            )

            # Number of ensembles
            self.number_of_ensembles = len(adcp_data["Samples"])

            # Time
            self.time_start = tz_formatted_string(
                serial_time=self.data.date_time.start_serial_time,
                utc_time_offset=self.data.date_time.utc_time_offset, format="%H:%M:%S.%f")
            self.time_end = tz_formatted_string(
                serial_time=self.data.date_time.end_serial_time,
                utc_time_offset=self.data.date_time.utc_time_offset, format="%H:%M:%S.%f")
            self.sampling_duration_sec = np.nansum(self.data.date_time.ens_duration_sec)

            # Voltage
            if self.data.sensors.battery_voltage.internal.data is not None:
                self.battery_voltage = np.nanmean(
                    self.data.sensors.battery_voltage.internal.data
                )

            # Gauge height
            self.gh_m = station_data["GaugeHeight (m)"]
            if len(station_data["GaugeHeightObservationTime"]) > 0:
                time_local = local_time_from_iso(station_data["GaugeHeightObservationTime"][0:-5], None)
                self.gh_obs_time=tz_formatted_string(
                    serial_time=time_local.timestamp(),
                    utc_time_offset=self.data.date_time.utc_time_offset,
                    format="%H:%M:%S")
            else:
                self.gh_obs_time = ""
            self.gh_orig_m = self.gh_m
            self.gh_obs_time_orig = self.gh_obs_time

            # Depth
            depth_data = getattr(self.data.depths, self.data.depths.selected)

            # Initially use only data with valid velocities unless Manual depth is provided
            # Velocities are only valid if a depth is provided
            valid_velocities = np.any(
                np.logical_not(np.isnan(self.data.w_vel.u_processed_mps)), axis=0
            )
            self.number_of_valid_ensembles = np.nansum(valid_velocities)
            self.depth_m = np.nanmean(depth_data.depth_processed_m[valid_velocities])
            self.depth_orig_m = self.depth_m
            if self.data.depths.selected == "man_depths":
                self.depth_source = "Manual"
                self.depth_m = np.nanmean(depth_data.depth_processed_m)
            elif self.data.depths.selected == "bt_depths":
                self.depth_source = "BT"
                if self.data.depths.composite == "On":
                    self.depth_source = self.depth_source + " Composite"
            elif self.data.depths.selected == "vb_depths":
                self.depth_source = "VB"
                if self.data.depths.composite == "On":
                    self.depth_source = self.depth_source + " Composite"

            # Water surface condition
            if station_data["StationType"] == "OpenWater":
                self.ws_condition = "Open"
            else:
                self.ws_condition = "Ice"
            self.ws_condition_orig = self.ws_condition

            self.extrapolation.top_method = self.data.extrap.top_method
            self.extrapolation.top_method_orig = self.data.extrap.top_method
            self.extrapolation.ice_exponent = self.data.extrap.ice_exponent
            self.extrapolation.ice_exponent_orig = self.data.extrap.ice_exponent
            self.extrapolation.bot_method = self.data.extrap.bot_method
            self.extrapolation.bot_method_orig = self.data.extrap.bot_method
            self.extrapolation.exponent = self.data.extrap.exponent
            self.extrapolation.exponent_orig = self.data.extrap.exponent

            # Open water transducer depth
            self.adcp_depth_below_ws_m = depth_data.draft_use_m

            # Apply default settings
            settings = self.qrev_default_settings()
            self.compute_profile(settings)

    def rsqst_island_edge(self, station_data):
        """Creates a manual station for an island edge for a rsqst file.

        Parameters
        ----------
        station_data: dict
            Dictionary of data for a station

        """

        # Create a manual station
        self.from_user_input(
            location=station_data["Location (m)"],
            depth=station_data["UserWaterDepth (m)"],
            velocity=np.nan,
            ws_condition="Island",
            ice_thickness_m=station_data["IceThickness (m)"],
            ws_to_ice_bottom_m=station_data["WaterSurfaceToBottomOfIce (m)"],
            ws_to_slush_bottom_m=station_data["WaterSurfaceToBottomOfSlush (m)"],
            use=station_data["UseStation"],
            velocity_correction=station_data["VelocityCorrectionFactor"],
        )

    def from_user_input(
        self,
        location,
        depth,
        velocity,
        ws_condition,
        ice_thickness_m,
        ws_to_ice_bottom_m,
        ws_to_slush_bottom_m,
        use,
        velocity_correction=0.0,
    ):
        """Creates vertical from user input.

        Parameters
        ----------
        location: float
            Distance from IP of vertical, m
        depth: float
            Depth for vertical, m
        velocity: float
            Mean velocity for vertical, m/s
        ws_condition: str
            Water surface condition (Open, Ice)
        ice_thickness_m: float
            Thickness of ice, in m
        ws_to_ice_bottom_m: float
            Depth from water surface to bottom of ice, in m
        ws_to_slush_bottom_m: float
            Depth from water surface to bottom of slush, in m
        use: bool
            Indicates if vertical should be used
        velocity_correction: float
            Corrects velocity for angle
        """

        self.ws_condition = ws_condition
        self.ws_condition_orig = ws_condition
        self.use = use
        self.mean_speed_source = "Manual"
        self.mean_speed_source_orig = self.mean_speed_source
        self.mean_speed_mps = velocity
        self.mean_velocity_magnitude_mps = velocity
        self.velocity_reference = "None"
        self.velocity_reference_orig = self.velocity_reference
        self.velocity_method = "Manual"
        self.velocity_method_orig = "Manual"
        self.depth_m = depth + ws_to_slush_bottom_m
        self.depth_source = "Manual"
        self.location_m = location
        self.location_orig_m = location
        self.ice_thickness_m = ice_thickness_m
        self.ice_thickness_orig_m = ice_thickness_m
        self.ws_to_ice_bottom_m = ws_to_ice_bottom_m
        self.ws_to_ice_bottom_orig_m = ws_to_ice_bottom_m
        self.ws_to_slush_bottom_m = ws_to_slush_bottom_m
        self.ws_to_slush_bottom_orig_m = ws_to_slush_bottom_m
        self.velocity_correction = velocity_correction
        self.velocity_correction_orig = velocity_correction

        self.mean_profile = {
            "cell_depth": np.array([(depth / 2) + ws_to_slush_bottom_m]),
            "cell_size": np.array([depth]),
            "speed": np.array([velocity]),
            "number_ensembles": np.array([0]),
            "u": np.array([np.nan]),
            "v": np.array([np.nan]),
            "rssi": np.array([np.nan]),
            "mag": np.array([np.nan]),
            "dir": np.array([np.nan]),
        }

    def trdi_store_original_settings(self, cfg, section):
        """Stores settings from mmt file.

        Parameters
        ----------
        cfg: dict
            Dictionary of most recent configuration in mmt
        section: dict
            Dictionary of section data in mmt file
        """

        # Store original settings
        self.velocity_method_orig = self.velocity_method
        self.velocity_reference_orig = self.velocity_reference
        self.gh_orig_m = self.gh_m
        self.gh_obs_time_orig = self.gh_obs_time
        self.location_orig_m = self.location_m
        self.ws_condition_orig = self.ws_condition
        self.adcp_depth_below_ice_orig_m = self.adcp_depth_below_ice_m
        self.adcp_depth_below_ws_orig_m = self.adcp_depth_below_ws_m
        self.ice_thickness_orig_m = self.ice_thickness_m
        self.ws_to_ice_bottom_orig_m = self.ws_to_ice_bottom_m
        self.ws_to_slush_bottom_orig_m = self.ws_to_slush_bottom_m

        # Navigation reference
        self.settings_orig["NavRef"] = self.velocity_reference

        # Composite tracks, no support for GPS
        self.settings_orig["CompTracks"] = "Off"

        # Water track filter settings
        if cfg["Processing"]["Use_3_Beam_Solution_For_WT"] == "YES":
            self.settings_orig["WTBeamFilter"] = 3
        else:
            self.settings_orig["WTBeamFilter"] = 4
        self.settings_orig["WTdFilter"] = "Manual"
        if "WT_Error_Velocity_Threshold" in section:
            self.settings_orig["WTdFilterThreshold"] = float(
                section["WT_Error_Velocity_Threshold"]
            )
        else:
            self.settings_orig["WTdFilterThreshold"] = float(
                cfg["Processing"]["WT_Error_Velocity_Threshold"]
            )
        self.settings_orig["WTwFilter"] = "Manual"
        self.settings_orig["WTwFilterThreshold"] = float(
            cfg["Processing"]["WT_Up_Velocity_Threshold"]
        )

        # SNR Filter for Sontek only
        self.settings_orig["WTsnrFilter"] = None

        # Requires a valid depth for valid water data
        self.settings_orig["WTwtDepthFilter"] = "On"

        # Data exclusions
        self.settings_orig["WTExcludedTopType"] = self.data.w_vel.excluded_top_type
        self.settings_orig["WTExcludedTop"] = self.data.w_vel.excluded_top
        self.settings_orig[
            "WTExcludedBottomType"
        ] = self.data.w_vel.excluded_bottom_type
        self.settings_orig["WTExcludedBottom"] = self.data.w_vel.excluded_bottom

        # Bottom track filter settings
        if cfg["Processing"]["Use_3_Beam_Solution_For_BT"] == "YES":
            self.settings_orig["BTBeamFilter"] = 3
        else:
            self.settings_orig["BTBeamFilter"] = 4
        self.settings_orig["BTdFilter"] = "Manual"
        self.settings_orig["BTdFilterThreshold"] = float(
            cfg["Processing"]["BT_Error_Velocity_Threshold"]
        )
        self.settings_orig["BTwFilter"] = "Manual"
        self.settings_orig["BTwFilterThreshold"] = float(
            cfg["Processing"]["BT_Up_Velocity_Threshold"]
        )

        # Depth Averaging
        if cfg["Processing"]["Use_Weighted_Mean_Depth"] == "NO":
            self.settings_orig["depthAvgMethod"] = "Simple"
        else:
            self.settings_orig["depthAvgMethod"] = "IDW"

        self.settings_orig["depthValidMethod"] = "TRDI"

        # Default to 4 beam depth average
        self.settings_orig["depthReference"] = self.data.depths.selected

        # Depth settings
        self.settings_orig["depthFilterType"] = "Off"
        self.settings_orig["depthComposite"] = self.data.depths.composite
        if float(section["ManualDepth"]) > 0:
            self.settings_orig["depthManual"] = float(section["ManualDepth"])
        else:
            self.settings_orig["depthManual"] = np.nan

        # Extrapolation settings
        self.settings_orig["extrapTop"] = self.extrapolation.top_method_orig
        self.settings_orig["extrapBot"] = self.extrapolation.bot_method_orig
        self.settings_orig["extrapBotExp"] = self.extrapolation.exponent_orig
        self.settings_orig["extrapTopExp"] = np.nan

        # Interpolation settings
        self.settings_orig["BTInterpolation"] = "None"
        self.settings_orig["WTEnsInterpolation"] = "None"
        self.settings_orig["WTCellInterpolation"] = "None"
        self.settings_orig["GPSInterpolation"] = "None"
        self.settings_orig["depthInterpolation"] = "None"

        self.settings_orig["UseMeasurementThresholds"] = False
        self.settings_orig["UsePingType"] = False

    def current_settings(self):
        """Returns the current settings for a vertical.
    
        Returns
        -------
        settings: dict
            Dictionary of settings for the vertical
        """

        settings = {
            "NavRef": self.velocity_reference,
            "CompTracks": self.data.boat_vel.composite,
            "WTBeamFilter": self.data.w_vel.beam_filter,
            "WTdFilter": self.data.w_vel.d_filter,
            "WTdFilterThreshold": self.data.w_vel.d_filter_thresholds,
            "WTwFilter": self.data.w_vel.w_filter,
            "WTwFilterThreshold": self.data.w_vel.w_filter_thresholds,
            "WTsmoothFilter": self.data.w_vel.smooth_filter,
            "WTsnrFilter": self.data.w_vel.snr_filter,
            "WTwtDepthFilter": self.data.w_vel.wt_depth_filter,
            "WTEnsInterpolation": self.data.w_vel.interpolate_ens,
            "WTCellInterpolation": self.data.w_vel.interpolate_cells,
            "WTExcludedTopType": self.data.w_vel.excluded_top_type,
            "WTExcludedTop": self.data.w_vel.excluded_top,
            "WTExcludedBottomType": self.data.w_vel.excluded_bottom_type,
            "WTExcludedBottom": self.data.w_vel.excluded_bottom,
            "BTbeamFilter": self.data.boat_vel.bt_vel.beam_filter,
            "BTdFilter": self.data.boat_vel.bt_vel.d_filter,
            "BTdFilterThreshold": self.data.boat_vel.bt_vel.d_filter_thresholds,
            "BTwFilter": self.data.boat_vel.bt_vel.w_filter,
            "BTwFilterThreshold": self.data.boat_vel.bt_vel.w_filter_thresholds,
            "BTsmoothFilter": self.data.boat_vel.bt_vel.smooth_filter,
            "BTInterpolation": self.data.boat_vel.bt_vel.interpolate,
            "UseMeasurementThresholds": self.use_measurement_thresholds,
            "UsePingType": self.use_ping_type,
            "depthReference": self.data.depths.selected,
            "depthComposite": self.data.depths.composite,
        }

        # Depth Settings
        select = getattr(self.data.depths, self.data.depths.selected)
        settings["depthInterpolation"] = select.interp_type
        settings["depthFilterType"] = select.filter_type
        settings["depthAvgMethod"] = self.data.depths.bt_depths.avg_method
        settings["depthValidMethod"] = self.data.depths.bt_depths.valid_data_method
        if self.data.depths.man_depths is not None:
            try:
                settings["depthManual"] = self.data.depths.man_depths.depth_processed_m[
                    0
                ]
            except TypeError:
                settings["depthManual"] = self.data.depths.man_depths.depth_processed_m
        else:
            settings["depthManual"] = np.nan

        # Extrap Settings
        if self.data.extrap is None:
            settings["extrapTop"] = "Power"
            settings["extrapBot"] = "Power"
            settings["extrapBotExp"] = 0.1667
            settings["extrapTopExp"] = np.nan
        else:
            settings["extrapTop"] = self.data.extrap.top_method
            settings["extrapBot"] = self.data.extrap.bot_method
            settings["extrapBotExp"] = self.data.extrap.exponent
            settings["extrapTopExp"] = self.data.extrap.exponent

        return settings

    def apply_settings(self, settings):
        """Applies reference, filter, and interpolation settings.

        Parameters
        ----------
        settings: dict
            Dictionary of reference, filter, and interpolation settings
        """

        # Use of ping type for filters
        self.use_ping_type = settings["UsePingType"]

        if not settings["UsePingType"]:
            self.data.w_vel.ping_type = np.tile("U", self.data.w_vel.ping_type.shape)
            self.data.boat_vel.bt_vel.frequency_khz = np.tile(
                0, self.data.boat_vel.bt_vel.frequency_khz.shape
            )

        # Set difference velocity BT filter
        bt_kwargs = {}
        if settings["BTdFilter"] == "Manual":
            bt_kwargs["difference"] = settings["BTdFilter"]
            bt_kwargs["difference_threshold"] = settings["BTdFilterThreshold"]
        else:
            bt_kwargs["difference"] = settings["BTdFilter"]

        # Set vertical velocity BT filter
        if settings["BTwFilter"] == "Manual":
            bt_kwargs["vertical"] = settings["BTwFilter"]
            bt_kwargs["vertical_threshold"] = settings["BTwFilterThreshold"]
        else:
            bt_kwargs["vertical"] = settings["BTwFilter"]

            # Apply beam filter
            bt_kwargs["beam"] = settings["BTbeamFilter"]

        self.data.boat_vel.bt_vel.use_measurement_thresholds = settings[
            "UseMeasurementThresholds"
        ]

        # Apply BT settings
        self.data.boat_vel.bt_vel.apply_filter(self.data, **bt_kwargs)

        # Navigation reference
        if self.data.boat_vel.selected != settings["NavRef"]:
            self.data.boat_vel.change_nav_reference(
                reference=settings["NavRef"], transect=self.data
            )
            self.velocity_reference = settings["NavRef"]

        # Set depth reference
        self.data.depths.selected = settings["depthReference"]

        if self.data.depths.selected == "man_depths":
            self.depth_source = "Manual"
            # Update manual depth is new value provided
            if not np.isnan(settings["depthManual"]):
                self.data.depths.add_depth_object(
                    depth_in=np.array(
                        [
                            [settings["depthManual"]]
                            * self.data.depths.bt_depths.depth_cell_depth_m.shape[1]
                        ]
                    ),
                    source_in="MAN",
                    freq_in=np.nan,
                    draft_in=self.data.depths.bt_depths.draft_use_m,
                    cell_depth_in=self.data.depths.bt_depths.depth_cell_depth_m,
                    cell_size_in=self.data.depths.bt_depths.depth_cell_size_m,
                )

        elif self.data.depths.selected == "bt_depths":
            self.depth_source = "BT"
            if settings["depthComposite"] == "On":
                self.depth_source = self.depth_source + " Composite"

        elif self.data.depths.selected == "vb_depths":
            self.depth_source = "VB"
            if settings["depthComposite"] == "On":
                self.depth_source = self.depth_source + " Composite"

        self.data.depths.bt_depths.valid_data_method = settings["depthValidMethod"]
        self.data.depths.bt_depths.avg_method = settings["depthAvgMethod"]

        self.data.depths.depth_filter(filter_method=settings["depthFilterType"])
        if self.data.depths.bt_depths is not None:
            self.data.depths.bt_depths.interp_type = settings["depthInterpolation"]
        if self.data.depths.vb_depths is not None:
            self.data.depths.vb_depths.interp_type = settings["depthInterpolation"]
        if settings["depthComposite"] == "On":
            self.data.depths.composite_depths(
                transect=self.data, setting=settings["depthComposite"]
            )
        else:
            self.data.depths.composite = settings["depthComposite"]
            depth_data = getattr(self.data.depths, self.data.depths.selected)
            if np.any(np.logical_not(depth_data.valid_data)):
                self.data.depths.depth_interpolation(
                    transect=self.data, method=settings["depthInterpolation"]
                )

        # Update water for any changes in depth and navigation reference
        if self.data.w_vel.sl_cutoff_type == "Percent":
            value = self.data.w_vel.sl_cutoff_percent
        else:
            value = self.data.w_vel.sl_cutoff_number
        (
            self.data.w_vel.cells_above_sl,
            self.data.w_vel.sl_cutoff_m,
        ) = self.data.side_lobe_cutoff(
            depths=self.data.depths,
            sl_lag_effect=self.data.w_vel.sl_lag_effect_m,
            slc_type="Percent",
            value=1 - (value / 100.0),
        )
        self.data.w_vel.adjust_side_lobe(transect=self.data)
        self.data.w_vel.set_nav_reference(self.data.boat_vel)

        # Set WT difference velocity filter
        wt_kwargs = {}
        if settings["WTdFilter"] == "Manual":
            wt_kwargs["difference"] = settings["WTdFilter"]
            wt_kwargs["difference_threshold"] = settings["WTdFilterThreshold"]
        else:
            wt_kwargs["difference"] = settings["WTdFilter"]

        # Set WT vertical velocity filter
        if settings["WTwFilter"] == "Manual":
            wt_kwargs["vertical"] = settings["WTwFilter"]
            wt_kwargs["vertical_threshold"] = settings["WTwFilterThreshold"]
        else:
            wt_kwargs["vertical"] = settings["WTwFilter"]

        # Set other wt settings
        wt_kwargs["snr"] = settings["WTsnrFilter"]
        wt_kwargs["beam"] = settings["WTBeamFilter"]
        wt_kwargs["wt_depth"] = settings["WTwtDepthFilter"]
        wt_kwargs["excluded_top_type"] = settings["WTExcludedTopType"]
        wt_kwargs["excluded_top"] = settings["WTExcludedTop"]
        wt_kwargs["excluded_bottom_type"] = settings["WTExcludedBottomType"]
        wt_kwargs["excluded_bottom"] = settings["WTExcludedBottom"]

        # WT interpolation settings
        self.data.w_vel.interpolate_cells = settings["WTCellInterpolation"]
        self.data.w_vel.interpolate_ens = settings["WTEnsInterpolation"]

        # Measurement thresholds
        self.data.w_vel.use_measurement_thresholds = settings[
            "UseMeasurementThresholds"
        ]
        if (
            self.data.w_vel.ping_type.size == 0
            and self.data.inst.manufacturer == "SonTek"
        ):
            # Correlation and frequency can be used to determine ping type
            self.data.w_vel.ping_type = Vertical.sontek_ping_type(
                corr=self.data.w_vel.corr, freq=self.data.w_vel.frequency
            )

        # Apply filter
        self.data.w_vel.apply_filter(transect=self.data, **wt_kwargs)

        # Apply interpolation
        self.data.w_vel.apply_interpolation(
            transect=self.data,
            ens_interp=settings["WTEnsInterpolation"],
            cells_interp=settings["WTCellInterpolation"],
        )

        # Depth data
        depth_data = getattr(self.data.depths, self.data.depths.selected)
        # valid_velocities = np.any(
        #     np.logical_not(np.isnan(self.data.w_vel.u_processed_mps)), axis=0
        # )
        # self.number_of_valid_ensembles = np.nansum(valid_velocities)

        # Determine depth for vertical
        if self.data.depths.selected == "man_depths":
            self.depth_m = np.nanmean(depth_data.depth_processed_m)
        else:
            # Compute mean depths using only ensembles with valid velocities
            # self.depth_m = np.nanmean(depth_data.depth_processed_m[valid_velocities])
            self.depth_m = np.nanmean(depth_data.depth_processed_m)
        
        if self.depth_m < self.ws_to_slush_bottom_m:
            self.depth_m = self.ws_to_slush_bottom_m

    def qrev_default_settings(self):
        """QRev default and filter settings for a measurement."""

        settings = dict()

        # Navigation reference
        settings["NavRef"] = self.velocity_reference

        # Composite tracks
        settings["CompTracks"] = "Off"

        # Water track filter settings
        settings["WTBeamFilter"] = -1
        settings["WTdFilter"] = "Auto"
        settings["WTdFilterThreshold"] = np.nan
        settings["WTwFilter"] = "Auto"
        settings["WTwFilterThreshold"] = np.nan

        # SNR Filter for Sontek only
        if self.data.inst.manufacturer == "TRDI":
            settings["WTsnrFilter"] = None
        else:
            settings["WTsnrFilter"] = "Auto"

        # Requires a valid depth for valid water data
        settings["WTwtDepthFilter"] = "Auto"

        # Data exclusions
        settings["WTExcludedTopType"] = self.data.w_vel.excluded_top_type
        settings["WTExcludedTop"] = self.data.w_vel.excluded_top
        settings["WTExcludedBottomType"] = self.data.w_vel.excluded_bottom_type
        settings["WTExcludedBottom"] = self.data.w_vel.excluded_bottom

        # Bottom track filter settings
        settings["BTbeamFilter"] = -1
        settings["BTdFilter"] = "Auto"
        settings["BTdFilterThreshold"] = np.nan
        settings["BTwFilter"] = "Auto"
        settings["BTwFilterThreshold"] = np.nan

        # Depth Averaging
        settings["depthAvgMethod"] = "IDW"
        settings["depthValidMethod"] = "QRev"

        # Default to 4 beam depth average
        settings["depthReference"] = self.data.depths.selected

        # Depth settings
        settings["depthFilterType"] = "Median"
        if hasattr(self, "kmeans"):
            if self.ws_condition == "Ice" and self.kmeans:
                settings["depthFilterType"] = "K-Means"

        settings["depthComposite"] = "Off"
        settings["depthComposite"] = self.data.depths.composite
        settings["depthManual"] = np.nan

        # Extrapolation Settings
        settings["extrapTop"] = "Power"
        settings["extrapBot"] = "Power"
        settings["extrapBotExp"] = 0.1667
        settings["extrapTopExp"] = np.nan

        # Interpolation settings
        settings["BTInterpolation"] = "None"
        settings["WTEnsInterpolation"] = "None"
        settings["WTCellInterpolation"] = "None"
        settings["GPSInterpolation"] = "None"
        settings["depthInterpolation"] = "Linear"

        settings["UseMeasurementThresholds"] = False
        settings["UsePingType"] = True

        return settings

    @staticmethod
    def sontek_ping_type(corr, freq, expected_std=None):
        """Determines ping type based on the fact that HD has correlation but incoherent
        does not.

        Parameters
        ----------
        corr: np.array(int)
            Water track correlation
        freq:
            Frequency of ping in Hz
        expected_std: float
            Expected standard deviation of water track velocity provided by SonTek

        Returns
        -------
        ping_type: np.array(int)
            Ping_type for each ensemble, 3 - 1 MHz Incoherent, 4 - 1 MHz HD,
            5 - 3 MHz Incoherent, 6 - 3 MHz HD
        """
        # Determine ping type

        if expected_std is None:
            # M9 or S5
            if corr.size > 0:
                corr_exists = np.nansum(np.nansum(corr, axis=1), axis=0)
                coherent = corr_exists > 0
            else:
                coherent = np.tile([False], freq.size)
            ping_type = []
            for n in range(len(coherent)):
                if coherent[n]:
                    if freq[n] == 3000:
                        ping_type.append("3C")
                    else:
                        ping_type.append("1C")
                else:
                    if freq[n] == 3000:
                        ping_type.append("3I")
                    else:
                        ping_type.append("1I")
            ping_type = np.array(ping_type)
        else:
            # RS5
            ves = []
            for n in range(4):
                ves.append(np.nanmean(expected_std[:, n, :], axis=0))

            ves = np.array(ves)

            ves_avg = np.nanmean(ves, axis=0)

            ping_type = np.tile(["PC/BB"], ves_avg.size)
            ping_type[ves_avg < 0.01] = "PC"
            ping_type[ves_avg > 0.025] = "BB"

        return ping_type

    def compute_profile(self, settings=None):
        """Call appropriate methods to compute the ensemble and mean profiles.

        Parameters
        ----------
        settings: dict
            Dictionary of settings for processing data
        """

        # QRevIntMS: Fixed, TRDI: Y-Velocity, SonTek: XYZ coordinates
        if self.velocity_method == "Fixed":
            if self.mean_speed_source == "ADCP":
                self.compute_fixed_profile(settings)

        # QRevIntMS: Azimuth, TRDI: 2-D Velocity, SonTek: Earth coordinates
        elif self.velocity_method == "Azimuth":
            if self.mean_speed_source == "ADCP":
                self.compute_azimuth_profile(settings)

        # QRevIntMS: Magnitude, TRDI: Magnitude, SonTek: not available
        elif self.velocity_method == "Magnitude":
            if self.mean_speed_source == "ADCP":
                # self.compute_magnitude_profile(settings)
                self.mean_components_by_bin(settings)

        elif self.velocity_method == "StreamPro":
            self.streampro_method(settings=settings)

        elif self.velocity_method == "Manual":
            # self.apply_settings(settings=settings)
            self.mean_profile = {
                "cell_depth": np.array([(self.depth_m / 2) + self.ws_to_slush_bottom_m]),
                "cell_size": np.array([self.depth_m]),
                "speed": np.array([self.mean_speed_mps]),
                "number_ensembles": np.array([0]),
                "u": np.array([np.nan]),
                "v": np.array([np.nan]),
                "rssi": np.array([np.nan]),
                "mag": np.array([np.nan]),
                "dir": np.array([np.nan]), }

        if self.sampling_duration_sec > 0:
            valid_velocities = np.any(
                np.logical_not(np.isnan(self.data.w_vel.u_processed_mps)), axis=0
            )
            self.number_of_valid_ensembles = np.nansum(valid_velocities)

    def compute_fixed_profile(self, settings=None):
        """Uses the ADCP or xyz coordinate system to determine the downstream velocity.
        TRDI does provide two means of adding ADCP rotation:
        1) Beam 3 misalignment and 2) Flow Angle in each vertical.
        The Flow Angle in each vertical is the beam_3_rotation in QRevIntMS.

        settings: dict
            Dictionary of processing settings
        """

        # Set data to instrument coordinates so pitch and roll and heading are not applied.
        # This approach prevents the use of erroneous pitch and roll from a StreamPro
        # where the pitch and roll sensors are not in the transducer head.
        # Note: TRDI may use earth coordinates, which I believe could introduce error
        # from compass and pitch and roll.
        if self.data.inst.model == "StreamPro":
            self.data.boat_vel.change_coord_sys("Inst", self.data.sensors, self.data.inst)
            self.data.w_vel.change_coord_sys("Inst", self.data.sensors, self.data.inst)

        elif self.data.w_vel.orig_coord_sys == "Earth":
            # Remove heading from Earth coordinate system to achieve Ship coordinate system
            heading_selected = getattr(self.data.sensors.heading_deg, self.data.sensors.heading_deg.selected)
            heading_change = heading_selected.data * -1
            self.data.boat_vel.bt_vel.change_heading(heading_change)
            self.data.boat_vel.bt_vel.coord_sys = "Ship"
            if self.data.boat_vel.selected == "None":
                boat_data = "None"
            else:
                boat_data = getattr(self.data.boat_vel, self.data.boat_vel.selected)
            self.data.w_vel.change_heading(self.data.boat_vel, heading_change)
            self.data.w_vel.coord_sys = "Ship"
        else:
            self.data.boat_vel.change_coord_sys(
                "Ship", self.data.sensors, self.data.inst
            )
            self.data.w_vel.change_coord_sys("Ship", self.data.sensors, self.data.inst)


        # Apply settings
        if settings is None:
            s = self.current_settings()
        else:
            s = settings
        if self.velocity_reference == "BT":
            s["NavRef"] = "bt_vel"
        self.apply_settings(settings=s)

        # TRDI allows a global beam_3_misalignment for the entire measurement and an additional rotation for
        # each vertical. In SxS Pro the beam_3_rotation is entered in the flow angle when using y-velocity method
        # Although SonTek does not provide this capability, it can be used with
        # SonTek data in QRevIntMS.
        adjustment_angle = self.beam_3_misalignment_deg + self.beam_3_rotation_deg

        # # SonTek ship coordinate data uses the u component as the streamwise
        # # component.
        # if self.data.inst.manufacturer == "SonTek" and self.data.w_vel.orig_coord_sys == "Ship":
        #     # Project the measured velocity to obtain speed if adjustment is required.
        #     if adjustment_angle != 0:
        #         unit_v = np.sin(np.radians(-1 * adjustment_angle))
        #         unit_u = np.cos(np.radians(-1 * adjustment_angle))
        #         speed = (
        #                 self.data.w_vel.u_processed_mps * unit_u + self.data.w_vel.v_processed_mps * unit_v)
        #     else:
        #         speed = self.data.w_vel.u_processed_mps
        #
        # else:

        # Project the measured velocity to obtain speed if adjustment is required.
        if adjustment_angle != 0:
            unit_u = np.sin(np.radians(-1 * adjustment_angle))
            unit_v = np.cos(np.radians(-1 * adjustment_angle))
            speed = (
                self.data.w_vel.u_processed_mps * unit_u
                + self.data.w_vel.v_processed_mps * unit_v
            )
        else:
            speed = self.data.w_vel.v_processed_mps

        u = self.data.w_vel.u_processed_mps
        v = self.data.w_vel.v_processed_mps

        # Store speed profiles for all ensembles.
        # if speed.shape[0] > 0:
        self.ensemble_profiles["u"] = u
        self.ensemble_profiles["v"] = v
        self.ensemble_profiles["speed"] = speed
        depths = getattr(self.data.depths, self.data.depths.selected)
        self.ensemble_profiles["cell_depth"] = np.copy(depths.depth_cell_depth_m)
        self.ensemble_profiles["cell_size"] = np.copy(depths.depth_cell_size_m)
        self.ensemble_profiles["cell_depth"][
            np.isnan(self.ensemble_profiles["speed"])
        ] = np.nan
        self.ensemble_profiles["cell_size"][
            np.isnan(self.ensemble_profiles["speed"])
        ] = np.nan

        # Compute the mean profile
        self.compute_mean_profile()

        # Determine the velocity magnitude and direction relative to the ADCP orientation, not North
        velocity = self.compute_mean_magnitude_direction()
        self.mean_velocity_magnitude_mps = velocity["mean_magnitude_mps"]
        self.mean_velocity_direction_deg = velocity["mean_dir_deg"]
        if velocity["mean_dir_deg"] > 180:
            velocity["mean_dir_deg"] = velocity["mean_dir_deg"] - 360
        self.mean_velocity_angle_deg = (
            self.beam_3_misalignment_deg
            + self.beam_3_rotation_deg
            + velocity["mean_dir_deg"]
        )

        # Compute the velocity magnitude and direction statistics
        self.compute_velocity_variation()

    def compute_azimuth_profile(self, settings=None):
        """Computes the speed perpendicular to the specified azimuth for each depth cell
        and the mean profile using all ensembles.

        Parameters
        ----------
        settings: dict
            Dictionary of settings for processing data
        """

        # Set data to earth coordinates
        self.data.change_heading_source("internal")
        if self.data.w_vel is not None and self.data.w_vel.coord_sys != "Earth":
            self.data.w_vel.change_coord_sys("Earth", self.data.sensors, self.data.inst)
        if (
            self.data.boat_vel is not None
            and self.data.boat_vel.bt_vel.coord_sys != "Earth"
        ):
            self.data.boat_vel.change_coord_sys(
                "Earth", self.data.sensors, self.data.inst
            )

        # Apply settings
        if settings is None:
            s = self.current_settings()
        else:
            s = settings
        if self.velocity_reference == "BT":
            s["NavRef"] = "bt_vel"
        self.apply_settings(settings=s)

        # Compute unit vector perpendicular to tagline in downstream direction
        ds_radians = azdeg2rad(self.tagline_azimuth_deg - 90)
        unit_ds_x, unit_ds_y = pol2cart(ds_radians, 1)

        # Compute the speed perpendicular to the tagline for each depth cell using the
        # dot product
        self.ensemble_profiles["speed"] = (
            unit_ds_x * self.data.w_vel.u_processed_mps
            + unit_ds_y * self.data.w_vel.v_processed_mps
        )

        # Store the velocity components
        self.ensemble_profiles["u"] = self.data.w_vel.u_processed_mps
        self.ensemble_profiles["v"] = self.data.w_vel.v_processed_mps

        # Store cell size and depth
        depths = getattr(self.data.depths, self.data.depths.selected)
        self.ensemble_profiles["cell_depth"] = np.copy(depths.depth_cell_depth_m)
        self.ensemble_profiles["cell_size"] = np.copy(depths.depth_cell_size_m)

        # Use values for cell depth and size only where speed is valid
        self.ensemble_profiles["cell_depth"][
            np.isnan(self.ensemble_profiles["speed"])
        ] = np.nan
        self.ensemble_profiles["cell_size"][
            np.isnan(self.ensemble_profiles["speed"])
        ] = np.nan

        # Compute the mean profile
        self.compute_mean_profile()

        # Compute the magnitude and direction of the velocity vector (not projected)
        velocity = self.compute_mean_magnitude_direction()
        self.mean_velocity_magnitude_mps = velocity["mean_magnitude_mps"]
        self.mean_velocity_direction_deg = velocity["mean_dir_deg"]
        self.mean_velocity_angle_deg = velocity["mean_dir_deg"] - (
            self.tagline_azimuth_deg - 90
        )
        if self.mean_velocity_angle_deg > 180:
            self.mean_velocity_angle_deg = self.mean_velocity_angle_deg - 360

        # Compute the velocity variation statistics
        self.compute_velocity_variation()

    def streampro_method(self, settings=None):
        """Coordinate the computation of the mean profile using the magnitude method
        as used by TRDI for a Streampro with no compass.

        Parameters
        ----------
        settings: dict
            Dictionary of settings for processing data
        """

        # Use values for cell depth and size only where velocity is valid
        self.ensemble_profiles["cell_depth"][
            np.isnan(self.ensemble_profiles["u"])] = np.nan
        self.ensemble_profiles["cell_size"][
            np.isnan(self.ensemble_profiles["u"])] = np.nan

        self.profile_method = "Components"
        self.mean_components_by_bin(settings)

    def mean_components_by_bin(self, settings):
        """The mean components by bin method computes the speed in each depth cell
        by first computing mean profile components, u and v, across all ensembles
        for each depth cell. The mean u and v components of the profile are then computed
        as the mean u and v components in the profile. Finally the overall mean u and v
        components are converted to unit vectors and the velocity in each depth cell
        is projected onto the unit vector. The mean speed profile is projected in the
        mean flow direction represented by the unit vectors. The velocity magnitude,
        direction, and coefficient of variation are also computed.

        Parameters
        ----------
        settings: dict
            Dictionary of settings for processing data
        """

        # Set data to Earth coordinates
        if self.data.w_vel is not None:
            self.data.w_vel.change_coord_sys("Earth", self.data.sensors, self.data.inst)
        if self.data.boat_vel is not None:
            self.data.boat_vel.change_coord_sys(
                "Earth", self.data.sensors, self.data.inst
            )

        # Apply settings
        if settings is None:
            s = self.current_settings()
        else:
            s = settings
        if self.velocity_reference == "BT":
            s["NavRef"] = "bt_vel"
        self.apply_settings(settings=s)

        # Assign data to ensemble profiles
        depths = getattr(self.data.depths, self.data.depths.selected)
        self.ensemble_profiles["cell_depth"] = np.copy(depths.depth_cell_depth_m)
        self.ensemble_profiles["cell_size"] = np.copy(depths.depth_cell_size_m)
        self.ensemble_profiles["u"] = self.data.w_vel.u_processed_mps
        self.ensemble_profiles["v"] = self.data.w_vel.v_processed_mps

        # Use values for cell depth and size only where velocity is valid
        self.ensemble_profiles["cell_depth"][
            np.isnan(self.ensemble_profiles["u"])
        ] = np.nan
        self.ensemble_profiles["cell_size"][
            np.isnan(self.ensemble_profiles["u"])
        ] = np.nan

        # To complete the first step the speed is estimated as the magnitude of the
        # velocity in each depth cell to make use of the algorithms available in
        # compute_mean_profile to handle ensembles with varying cell size. The mean
        # direction is the computed and the velocities projected and the final
        # mean profile computed.

        # Compute the profile using an estimate of speed in each cell
        self.ensemble_profiles["speed"] = np.sqrt(
            self.ensemble_profiles["u"] ** 2 + self.ensemble_profiles["v"] ** 2
        )

        # Compute the mean profile using the speed estimates
        self.compute_mean_profile()

        # Use profile data to compute mean u and v
        u_mean = self.weighted_mean(
            self.mean_profile["u"], self.mean_profile["cell_size"]
        )
        v_mean = self.weighted_mean(
            self.mean_profile["v"], self.mean_profile["cell_size"]
        )

        # Compute the velocity magnitude used to compute unit vector coordinates
        _, mean_magnitude_mps = cart2pol(u_mean, v_mean)

        # Compute unit vectors in mean direction
        unit_u = u_mean / mean_magnitude_mps
        unit_v = v_mean / mean_magnitude_mps

        # Compute the speed in each depth cell in the mean direction
        self.ensemble_profiles["speed"] = (
            self.data.w_vel.u_processed_mps * unit_u
            + self.data.w_vel.v_processed_mps * unit_v
        )

        # Compute mean profile using computed speed
        self.compute_mean_profile()

        # Compute velocity components and means
        velocity = self.compute_mean_magnitude_direction()

        self.mean_velocity_magnitude_mps = velocity["mean_magnitude_mps"]
        self.mean_velocity_direction_deg = velocity["mean_dir_deg"]
        self.mean_velocity_angle_deg = np.rad2deg(
            np.arccos(self.flow_angle_coefficient)
        )

        # Compute the velocity statistics
        self.compute_velocity_variation()

    def ds_magnitudes(self, settings=None):
        """Computes the speed of each depth cell by first computing the mean direction
        for the ensemble and then computing the projected speed for each depth cell
        in the direction of the ensemble mean.

        Parameters
        ----------
        settings: dict
            Dictionary of settings for processing data
        """

        # Set data to Ship coordinates
        # if self.data.w_vel.coord_sys == "Earth":
        #     if self.data.sensors.heading_deg.selected == "internal":
        #         self.change_heading_source("user")

        # Apply settings
        if settings is None:
            s = self.current_settings()
        else:
            s = settings
        if self.velocity_reference == "BT":
            s["NavRef"] = "bt_vel"
        self.apply_settings(settings=s)

        # Assign data to ensemble profiles
        depths = getattr(self.data.depths, self.data.depths.selected)
        self.ensemble_profiles["cell_depth"] = np.copy(depths.depth_cell_depth_m)
        self.ensemble_profiles["cell_size"] = np.copy(depths.depth_cell_size_m)
        self.ensemble_profiles["u"] = self.data.w_vel.u_processed_mps
        self.ensemble_profiles["v"] = self.data.w_vel.v_processed_mps

        # Compute the mean u and v components for each ensemble
        u_ens_mean = self.weighted_mean(
            self.data.w_vel.u_processed_mps, self.ensemble_profiles["cell_size"], axis=0
        )
        v_ens_mean = self.weighted_mean(
            self.data.w_vel.v_processed_mps, self.ensemble_profiles["cell_size"], axis=0
        )

        # Compute the mean velocity magnitude and direction for each ensemble
        dir_ens, mag_ens = cart2pol(u_ens_mean, v_ens_mean)

        # Project velocity in each depth cell in mean direction of ensemble
        u_ens_unit = u_ens_mean / mag_ens
        v_ens_unit = v_ens_mean / mag_ens

        # Compute ensembles profile
        self.ensemble_profiles["speed"] = np.multiply(
            self.data.w_vel.u_processed_mps, u_ens_unit
        ) + np.multiply(self.data.w_vel.v_processed_mps, v_ens_unit)

        self.ensemble_profiles["u"] = self.data.w_vel.u_processed_mps
        self.ensemble_profiles["v"] = self.data.w_vel.v_processed_mps

        self.ensemble_profiles["cell_depth"][
            np.isnan(self.ensemble_profiles["speed"])
        ] = np.nan

        self.ensemble_profiles["cell_size"][
            np.isnan(self.ensemble_profiles["speed"])
        ] = np.nan

        # Compute mean profile
        self.compute_mean_profile()

        # Compute mean velocity magnitude and direction
        self.mean_velocity_magnitude_mps = (
            self.mean_measured_speed_mps / self.flow_angle_coefficient
        )

        # Compute the mean flow direction, using velocity magnitude weighting
        mean_direction = np.nansum(dir_ens * mag_ens) / np.nansum(mag_ens)
        self.mean_velocity_direction_deg = rad2azdeg(mean_direction)
        self.mean_velocity_angle_deg = np.rad2deg(
            np.arccos(self.flow_angle_coefficient)
        )

        # Compute velocity statistics
        self.compute_velocity_variation(direction=dir_ens)

    def compute_mean_profile(self):
        """Computes the mean profile from the ensemble_profiles."""

        # Determine number of ensembles in each cell, assuming uniform cell size
        self.mean_profile["number_ensembles"] = np.nansum(
            np.logical_not(np.isnan(self.ensemble_profiles["speed"])), axis=1
        )

        # Identify cells having sufficient ensembles to be considered valid
        valid_idx = (
            np.array(self.mean_profile["number_ensembles"])
            >= self.cell_minimum_valid_ensembles
        )

        # Proceed if data available
        if np.nansum(valid_idx) > 0:
            # Compute sampling duration
            self.sampling_duration_sec = np.nansum(self.data.date_time.ens_duration_sec)

            # Localize ensemble profiles data
            cell_size = np.round(self.ensemble_profiles["cell_size"], 2)
            cell_depth = np.round(self.ensemble_profiles["cell_depth"], 3)
            speed = np.copy(self.ensemble_profiles["speed"])
            u = np.copy(self.ensemble_profiles["u"])
            v = np.copy(self.ensemble_profiles["v"])

            # Determine what cell sizes are present in vertical
            valid_data = np.logical_not(np.isnan(speed))
            unique_cell_sizes, cell_size_count = np.unique(
                cell_size[valid_data], return_counts=True
            )

            # Check number of cell sizes
            if unique_cell_sizes.shape[0] == 1:
                # Compute profile for consistent cell size
                self.mean_profile["speed"] = np.nanmean(speed, axis=1)[valid_idx]
                self.mean_profile["cell_depth"] = np.nanmean(cell_depth, axis=1)[
                    valid_idx
                ]
                self.mean_profile["cell_size"] = np.nanmean(cell_size, axis=1)[
                    valid_idx
                ]
                self.mean_profile["u"] = np.nanmean(u, axis=1)[valid_idx]
                self.mean_profile["v"] = np.nanmean(v, axis=1)[valid_idx]
                self.mean_profile["number_ensembles"] = self.mean_profile[
                    "number_ensembles"
                ][valid_idx]
                self.mean_profile["rssi"] = np.nanmean(
                    self.data.w_vel.rssi[:, valid_idx, :], axis=2
                )

            else:
                # Determine standard cell size (most frequently used) for profile
                reg_cell_idx = np.logical_and(
                    np.logical_not(self.data.w_vel.ping_type == "S"),
                    np.logical_not(np.isnan(speed)),
                )
                unique_reg_cell_sizes, reg_cell_size_count = np.unique(
                    cell_size[reg_cell_idx], return_counts=True
                )
                std_size = unique_reg_cell_sizes[
                    np.where(reg_cell_size_count == np.nanmax(reg_cell_size_count))
                ][0]

                # If there are no regular cells the std size is set to the surface cell
                # size
                if np.isnan(std_size):
                    std_size = unique_cell_sizes[
                        np.where(cell_size_count == np.nanmax(cell_size_count))
                    ][0]

                # Compute depths of top and bottom of cells
                cell_bots = self.ensemble_profiles["cell_depth"] + (
                    self.ensemble_profiles["cell_size"] * 0.5
                )
                cell_tops = self.ensemble_profiles["cell_depth"] - (
                    self.ensemble_profiles["cell_size"] * 0.5
                )
                std_cell_top = np.nanmin(cell_tops[cell_size == std_size])

                # Compute target cells
                target_cell_depths = np.arange(
                    std_cell_top + 0.5 * std_size,
                    self.depth_m - 0.5 * std_size,
                    std_size,
                )
                target_cells = []
                for depth in target_cell_depths:
                    target_cells.append((depth, std_size))

                # Check for cells, which will be above the top of the std_size
                # cells. Due to varying cell sizes and varying number of cell depths,
                # these cells could be a mix of small regular cells and surface cells.
                surface_cell_depths = np.unique(
                    np.round(cell_depth[cell_depth < std_cell_top], 3)
                )
                surface_cell_size = np.unique(
                    np.round(cell_size[cell_depth < std_cell_top], 2)
                )

                # Only one size surface cell
                if surface_cell_size.shape[0] == 1:
                    surface_target = []
                    for depth in surface_cell_depths:
                        surface_target.append((depth, surface_cell_size[0]))
                    target_cells = surface_target + target_cells

                # Multiple surface cell sizes
                elif surface_cell_size.shape[0] > 1:
                    # Determine surface cell size (most frequently used) for profile
                    surf_cell_idx = np.logical_and(
                        self.data.w_vel.ping_type == "S",
                        np.logical_not(np.isnan(speed)), )
                    unique_surf_cell_sizes, surf_cell_size_count = np.unique(
                        cell_size[surf_cell_idx], return_counts=True)
                    if len(surf_cell_size_count) > 0:
                        surf_size = unique_surf_cell_sizes[
                            np.where(surf_cell_size_count == np.nanmax(surf_cell_size_count))][
                            0]
                    
                        # Compute minimum depth of the top of each surface cell size
                        surface_min_tops = []
                        surface_depths_by_size = []
                        for n, size in enumerate(surface_cell_size):
                            temp_depths = np.round(
                                np.unique(
                                    self.ensemble_profiles["cell_depth"][
                                        np.round(self.ensemble_profiles["cell_size"], 2)
                                        == size
                                    ]
                                ),
                                3,
                            )

                            surface_depths_by_size.append(
                                temp_depths[temp_depths < std_cell_top]
                            )
                            surface_tops_by_size = surface_depths_by_size[n] - 0.5 * size
                            surface_min_tops.append(np.nanmin(surface_tops_by_size))

                        surf_cell_top = np.nanmin(surface_min_tops)

                        # Compute the target cell depths
                        surface_target = []
                        surf_bottom = surf_cell_top + surf_size
                        while surf_bottom < std_cell_top:
                            surface_target.append((np.round(surf_bottom - 0.5 * surf_size, 3), surf_size))
                            surf_bottom = surf_bottom + surf_size

                        # Combine surface targets with regular cell targets
                        target_cells = surface_target + target_cells

                # Initialize lists
                profile_speed = []
                profile_depth = []
                profile_u = []
                profile_v = []
                profile_cell_size = []
                profile_n_ensembles = []
                profile_rssi = []

                # Compute mean profile
                for target in target_cells:
                    # Compute target bounds
                    target_top = target[0] - 0.5 * target[1]
                    target_bot = target[0] + 0.5 * target[1]
                    target_center = target[0]
                    target_size = target[1]

                    # Find cells that lay within the target
                    idx1 = np.where(
                        np.logical_and(
                            np.logical_and(
                                self.ensemble_profiles["cell_depth"] > target_top,
                                self.ensemble_profiles["cell_depth"] < target_bot,
                            ),
                            np.round(self.ensemble_profiles["cell_size"], 2)
                            <= np.round(target_size, 2),
                        )
                    )

                    weights1 = cell_size[idx1]
                    speed1 = speed[idx1]
                    u1 = u[idx1]
                    v1 = v[idx1]

                    rssi1 = self.data.w_vel.rssi[:, idx1[0], idx1[1]]

                    # Interpolate data from cells in which the target lies
                    idx2 = np.where(
                        np.logical_and(
                            np.logical_and(
                                cell_tops < target_center, cell_bots > target_center
                            ),
                            cell_size > target_size,
                        )
                    )

                    weights2 = np.array([target_size] * idx2[0].shape[0])
                    speed2 = speed[idx2]
                    u2 = u[idx2]
                    v2 = v[idx2]
                    rssi2 = self.data.w_vel.rssi[:, idx2[0], idx2[1]]

                    # Use weights and target_size to compute number of ensembles
                    sum_weights = np.nansum(weights1) + np.nansum(weights2)

                    n_ensembles = np.round(sum_weights / target_size, 0)

                    # If sufficient ensembles compute mean for mean profile
                    if n_ensembles >= self.cell_minimum_valid_ensembles:
                        wspd1 = np.nansum(speed1 * weights1)
                        wu1 = np.nansum(u1 * weights1)
                        wv1 = np.nansum(v1 * weights1)
                        wrssi1 = np.nansum(rssi1 * weights1[None, :], axis=1)
                        wspd2 = np.nansum(speed2 * weights2)
                        wu2 = np.nansum(u2 * weights2)
                        wv2 = np.nansum(v2 * weights2)
                        wrssi2 = np.nansum(rssi2 * weights2[None, :], axis=1)
                        profile_speed.append((wspd1 + wspd2) / sum_weights)
                        profile_u.append((wu1 + wu2) / sum_weights)
                        profile_v.append((wv1 + wv2) / sum_weights)
                        profile_rssi.append((wrssi1 + wrssi2) / sum_weights)
                        profile_depth.append(target_center)
                        profile_cell_size.append(target_size)
                        profile_n_ensembles.append(n_ensembles)

                # Assign to mean profile
                self.mean_profile["speed"] = np.array(profile_speed)
                self.mean_profile["cell_depth"] = np.array(profile_depth)
                self.mean_profile["number_ensembles"] = np.array(profile_n_ensembles)
                self.mean_profile["u"] = np.array(profile_u)
                self.mean_profile["v"] = np.array(profile_v)
                self.mean_profile["cell_size"] = np.array(profile_cell_size)
                self.mean_profile["rssi"] = np.array(profile_rssi).T

            # Compute the mean_measured_speed_mps
            self.mean_measured_speed_mps = (
                self.weighted_mean(
                    self.mean_profile["speed"], self.mean_profile["cell_size"]
                )
                * self.flow_angle_coefficient
            )

            self.number_valid_cells = self.mean_profile["cell_depth"].shape[0]

            if np.nansum(self.mean_profile["number_ensembles"]) > 0:
                # Compute extrapolation fit
                self.extrapolation.extrap_fit.populate_data(
                    vertical=self,
                    fit_method=self.extrapolation.extrap_fit.fit_method,
                    compute_sensitivity=False,
                )

                # Compute mean speed for top and bottom estimates from extrapolation fit
                self.compute_top_bot()

                # Compute the mean_speed_mps of the profile including top and
                # bottom estimates and flow angle coefficient
                self.compute_mean_speed()

                # Compute mag and direction profile
                mean_dir_rad, mean_magnitude_mps = cart2pol(
                    self.mean_profile["u"], self.mean_profile["v"]
                )
                mean_dir_deg = rad2azdeg(mean_dir_rad)
                self.mean_profile["dir"] = mean_dir_deg
                self.mean_profile["mag"] = mean_magnitude_mps

                # Update extrapolation sensitivity
                self.extrapolation.extrap_fit.update_q_sensitivity(self)
        else:
            self.mean_profile["number_ensembles"] = np.array([])
            self.mean_profile["speed"] = np.array([])
            self.mean_profile["cell_depth"] = np.array([])
            self.mean_profile["u"] = np.array([])
            self.mean_profile["v"] = np.array([])
            self.mean_profile["cell_size"] = np.array([])
            self.mean_profile["rssi"] = np.array([])
            self.mean_profile["dir"] = np.nan
            self.mean_profile["mag"] = np.nan
            self.number_valid_cells = 0
            self.mean_measured_speed_mps = np.nan

            self.compute_mean_speed()
            self.compute_velocity_variation()
            self.extrapolation = Extrapolation()
            self.extrapolation.extrap_fit.populate_data(
                vertical=self,
                fit_method=self.extrapolation.extrap_fit.fit_method,
                compute_sensitivity=False,
            )

    def compute_top_bot(self, top_method=None, bot_method=None, exp=None, ice_exp=None):
        """Compute the top and bottom speed from the extrapolation fit and profile data.

        Parameters
        ----------
        top_method: str
            Top extrapolation method
        bot_method: str
            Bottom extrapolation method
        exp: float
            Exponent for power and no slip methods
        ice_exp: float
            Exponent for ice method
        """

        # Set fit variables
        if top_method is None:
            top_method = self.extrapolation.top_method
        if bot_method is None:
            bot_method = self.extrapolation.bot_method
        if exp is None:
            exp = self.extrapolation.exponent
        if ice_exp is None:
            ice_exp = self.extrapolation.ice_exponent

        # Store extrapolation fit
        self.extrapolation.set_extrap_data(
            top=top_method, bot=bot_method, exp=exp, ice_exp=ice_exp
        )

        # Compute top speed and range (cell size)
        top_speed, top_range = self.extrapolate_top()

        # Update profile with top extrapolation
        self.mean_profile["speed"] = np.hstack([top_speed, self.mean_profile["speed"]])
        self.mean_profile["cell_size"] = np.hstack(
            [top_range, self.mean_profile["cell_size"]]
        )
        self.mean_profile["u"] = np.hstack([np.nan, self.mean_profile["u"]])
        self.mean_profile["v"] = np.hstack([np.nan, self.mean_profile["v"]])
        if self.ws_condition == "Open":
            ice_depth = 0
        else:
            ice_depth = self.ws_to_slush_bottom_m
        self.mean_profile["cell_depth"] = np.hstack(
            [(top_range / 2) + ice_depth, self.mean_profile["cell_depth"]]
        )
        self.mean_profile["number_ensembles"] = np.hstack(
            [-1, self.mean_profile["number_ensembles"]]
        )

        # Compute bottom speed and range (cell size)
        bot_speed, bot_range = self.extrapolate_bot()

        # Update profile with bottom extrapolation
        bot_idx = np.where(np.logical_not(np.isnan(self.mean_profile["speed"])))[0]
        if len(bot_idx) > 0:
            bot_idx = bot_idx[-1] + 1
            if bot_idx < self.mean_profile["speed"].shape[0]:
                self.mean_profile["speed"][bot_idx] = bot_speed
                self.mean_profile["cell_size"][bot_idx] = bot_range
                self.mean_profile["cell_depth"][bot_idx] = self.depth_m - (
                    bot_range / 2
                )
                self.mean_profile["number_ensembles"][bot_idx] = -1
            else:
                self.mean_profile["speed"] = np.hstack(
                    [self.mean_profile["speed"], bot_speed]
                )
                self.mean_profile["cell_size"] = np.hstack(
                    [self.mean_profile["cell_size"], bot_range]
                )
                self.mean_profile["cell_depth"] = np.hstack(
                    [self.mean_profile["cell_depth"], self.depth_m - (bot_range / 2)]
                )
                self.mean_profile["number_ensembles"] = np.hstack(
                    [self.mean_profile["number_ensembles"], -1]
                )
                self.mean_profile["u"] = np.hstack(
                    [
                        self.mean_profile["u"],
                        np.nan,
                    ]
                )
                self.mean_profile["v"] = np.hstack([self.mean_profile["v"], np.nan])

    def compute_mean_speed(self):
        """Compute the mean_speed_mps of the profile including top and bottom estimates
        and flow angle coefficient"""

        self.mean_speed_mps = (
            self.weighted_mean(
                self.mean_profile["speed"], self.mean_profile["cell_size"]
            )
            * self.flow_angle_coefficient
        )

    def compute_mean_magnitude_direction(self):
        """Compute the mean velocity magnitude and direction using the mean components
        from the mean profile. The direction will be relative to the coordinate system
        used to compute the mean profile (North or Beam 3).
        """

        # Compute mean components from mean profile
        u_mean = self.weighted_mean(
            self.mean_profile["u"], self.mean_profile["cell_size"]
        )
        v_mean = self.weighted_mean(
            self.mean_profile["v"], self.mean_profile["cell_size"]
        )

        # Compute the flow angle and the associated unit vector from the mean components
        mean_dir_rad, mean_magnitude_mps = cart2pol(u_mean, v_mean)
        mean_dir_deg = rad2azdeg(mean_dir_rad)

        velocity = {
            "mean_magnitude_mps": mean_magnitude_mps,
            "mean_dir_deg": mean_dir_deg,
        }

        return velocity

    def compute_velocity_variation(self, direction=None):
        """Computes the coefficient of variation of the measured speed and the
        standard deviation of the mean flow direction.

        Parameters
        ----------
        direction: np.array(float)
            Flow direction of each ensembles in radians
        """

        if not np.isnan(self.mean_measured_speed_mps):
            # Cutoff ensemble speeds at same level as mean profile
            speed = np.copy(self.ensemble_profiles["speed"])
            cell_size = np.copy(self.ensemble_profiles["cell_size"])

            # Compute the coefficient of variation
            ensemble_speed = self.weighted_mean(speed, cell_size, axis=0)
            self.mean_measured_speed_cv = (
                np.nanstd(ensemble_speed, ddof=1) / np.nanmean(ensemble_speed)
            ) * 100

            if direction is None:
                # Compute the mean u and v components for each ensemble using valid data
                u_ens_mean = self.weighted_mean(
                    self.ensemble_profiles["u"],
                    self.ensemble_profiles["cell_size"],
                    axis=0,
                )
                v_ens_mean = self.weighted_mean(
                    self.ensemble_profiles["v"],
                    self.ensemble_profiles["cell_size"],
                    axis=0,
                )

                # Compute the flow angle from the mean components
                direction, mag = cart2pol(u_ens_mean, v_ens_mean)
                self.mean_velocity_magnitude_cv = (
                    np.nanstd(mag, ddof=1) / self.mean_velocity_magnitude_mps
                ) * 100
            else:
                self.mean_velocity_magnitude_cv = self.mean_measured_speed_cv

            # Compute standard deviation of flow direction using Yamartino method
            if np.any(np.logical_not(np.isnan(direction))) and not np.isnan(
                self.mean_speed_mps
            ):
                sa = np.nanmean(np.sin(direction))
                ca = np.nanmean(np.cos(direction))
                eta = np.sqrt(1 - (sa**2 + ca**2))
                dir_std = np.arcsin(eta) * (1 + ((2 / np.sqrt(3)) - 1) * eta**3)
                self.mean_velocity_direction_stdev_deg = np.degrees(dir_std)
            else:
                self.mean_velocity_direction_stdev_deg = np.nan
        else:
            self.mean_velocity_magnitude_cv = np.nan
            self.mean_velocity_direction_stdev_deg = np.nan

    def extrapolate_top(self, top_method=None, exponent=None):
        """Computes the extrapolated top discharge.

        Parameters
        ----------
        top_method: str
            Specifies method to use for top extrapolation
        exponent: float
            Exponent to use for power extrapolation

        Returns
        -------
        speed_top: float
            Speed in top extrapolated portion of vertical
        top_rng: float
            Range from water surface to top of first valid cell
        """

        if np.any(np.logical_not(np.isnan(self.mean_profile["speed"]))):
            if top_method is None:
                top_method = self.extrapolation.top_method
                exponent = self.extrapolation.exponent
            # Get data from vertical
            cell_depth = np.copy(self.mean_profile["cell_depth"])
            cell_size = np.copy(self.mean_profile["cell_size"])
            depth = self.depth_m
            speed = self.mean_profile["speed"]

            # If ice adjust cell depth and overall depth to reflect depth below ice
            if top_method == "Ice":
                exponent = self.extrapolation.ice_exponent
                cell_depth = cell_depth - self.ws_to_slush_bottom_m
                depth = depth - self.ws_to_slush_bottom_m

            # Compute top variables
            idx_valid = np.where(np.logical_not(np.isnan(speed)))[0]
            idx_top = idx_valid[0]
            if self.number_valid_cells > 2:
                idx_top3 = idx_valid[idx_top : idx_top + 3]
            else:
                idx_top3 = idx_valid[idx_top : self.number_valid_cells]

            top_rng = cell_depth[idx_top] - 0.5 * cell_size[idx_top]

            # Compute z
            z = np.subtract(depth, cell_depth)

            # Use only valid data
            valid_data = np.logical_not(np.isnan(speed))
            z[np.logical_not(valid_data)] = np.nan
            cell_size[np.logical_not(valid_data)] = np.nan
            cell_depth[np.logical_not(valid_data)] = np.nan

            # Compute top discharge
            speed_top = self.speed_top(
                top_method,
                exponent,
                idx_top,
                idx_top3,
                top_rng,
                speed,
                cell_size,
                cell_depth,
                depth,
                z,
            )
        else:
            speed_top = np.nan
            top_rng = np.nan

        return speed_top, top_rng

    @staticmethod
    def speed_top(
        top_method,
        exponent,
        idx_top,
        idx_top_3,
        top_rng,
        component,
        cell_size,
        cell_depth,
        depth,
        z,
    ):
        """Computes the top extrapolated value of the provided component.

        Parameters
        ----------
        top_method: str
            Top extrapolation method (Power, Constant, 3-Point)
        exponent: float
            Exponent for the power extrapolation method
        idx_top: int
            Index to the topmost valid depth cell in each ensemble
        idx_top_3: list
            Index to the top 3 valid depth cells in each ensemble
        top_rng: float
            Range from the water surface to the top of the topmost cell
        component: np.array(float)
            The variable to be extrapolated (speed)
        cell_size: np.array(float)
            Array of cell sizes (n cells )
        cell_depth: np.array(float)
            Depth of each cell (n cells)
        depth: float
            Bottom depth
        z: np.array(float)
            Relative depth from the bottom of each depth cell computed in discharge top method

        Returns
        -------
        top_value: total for the specified component integrated over the top range
        """

        # Initialize return
        top_value = 0

        # Top power extrapolation
        if top_method == "Power":
            numerator = (exponent + 1) * np.nansum(component * cell_size, 0)
            denominator = np.nansum(
                ((z + 0.5 * cell_size) ** (exponent + 1))
                - ((z - 0.5 * cell_size) ** (exponent + 1)),
                0,
            )
            coef = np.divide(numerator, denominator, where=denominator != 0)
            top_value = (
                (coef / (exponent + 1))
                * (depth ** (exponent + 1) - (depth - top_rng) ** (exponent + 1))
                / top_rng
            )

        # Top constant extrapolation
        elif top_method == "Constant":
            top_value = np.nan
            if idx_top >= 0:
                top_value = component[idx_top]

        # Top 3-point extrapolation
        elif top_method == "3-Point":
            # Determine number of bins available in each profile
            valid_data = np.logical_not(np.isnan(component))
            n_bins = np.nansum(valid_data)
            top_value = np.nan
            if (n_bins < 6) and (n_bins > 0) and (idx_top >= 0):
                top_value = component[idx_top]

            # If 6 or more bins use 3-pt at top
            if n_bins > 5:
                sumd = np.nansum(cell_depth[idx_top_3])
                sumd2 = np.nansum(cell_depth[idx_top_3] ** 2)
                sumq = np.nansum(component[idx_top_3])
                sumqd = np.nansum(component[idx_top_3] * cell_depth[idx_top_3])
                delta = 3 * sumd2 - sumd**2
                a = (3 * sumqd - sumq * sumd) / delta
                b = (sumq * sumd2 - sumqd * sumd) / delta
                top_value = (a * top_rng**2) / 2 + b

        # Ice extrapolation
        elif top_method == "Ice":
            # Ice uses the same No Slip algorithm as the bottom No Slip except the cell depth is used in
            # place of the z value, so that 0 occurs at the water surface rather than at the streambed
            # Valid data in the upper 20% of the water column or
            # the first valid depth cell are used to compute the no slip power fit
            cutoff_depth = np.round(0.2 * depth, 2)
            depth_ok = nan_less_equal(
                np.round(cell_depth, 2), np.tile(cutoff_depth, cell_depth.shape[0])
            )
            component_ok = np.logical_not(np.isnan(component))
            use_ns = depth_ok * component_ok

            # Always use the top cell even if not within the top 20%
            if idx_top >= 0:
                use_ns[idx_top] = 1

            # Create component and d arrays for the data to be used in
            # no slip computations
            component_ns = np.copy(component)
            component_ns[np.logical_not(use_ns)] = np.nan
            d_ns = np.copy(cell_depth)
            # Adjust for bottom of slush
            d_ns[np.logical_not(use_ns)] = np.nan
            numerator = (exponent + 1) * np.nansum(component_ns * cell_size, 0)
            denominator = np.nansum(
                ((d_ns + 0.5 * cell_size) ** (exponent + 1))
                - ((d_ns - 0.5 * cell_size) ** (exponent + 1)),
                0,
            )
            coef = np.divide(numerator, denominator, where=denominator != 0)
            if denominator == 0:
                coef = np.nan

            # Compute the top mean speed of each profile
            top_value = (coef / (exponent + 1)) * (top_rng ** (exponent + 1)) / top_rng

        return top_value

    def extrapolate_bot(self, bot_method=None, exponent=None):
        """Computes the extrapolated bottom discharge

        Parameters
        ----------
        bot_method: str
            Bottom extrapolation method
        exponent: float
            Bottom extrapolation exponent

        Returns
        -------
        speed_bot: float
            Speed in bottom extrapolated portion of profile
        bot_rng: float
            Range from bottom of last valid cell to streambed
        """

        if np.any(np.logical_not(np.isnan(self.mean_profile["speed"]))):
            # Determine extrapolation methods and exponent
            if bot_method is None:
                bot_method = self.extrapolation.bot_method
                exponent = self.extrapolation.exponent

            # Get data from vertical
            speed = self.mean_profile["speed"]
            cell_size = np.copy(self.mean_profile["cell_size"])
            cell_depth = np.copy(self.mean_profile["cell_depth"])
            depth = self.depth_m

            # Compute bottom variables
            idx_temp = np.where(np.logical_not(np.isnan(speed)))[0]
            idx_bot = idx_temp[-1]
            bot_rng = depth - cell_depth[idx_bot] - 0.5 * cell_size[idx_bot]

            # Compute z
            z = np.subtract(depth, cell_depth)

            # Use only valid data
            valid_data = np.logical_not(np.isnan(speed))
            z[np.logical_not(valid_data)] = np.nan
            z[nan_less(z, 0)] = np.nan
            cell_size[np.logical_not(valid_data)] = np.nan
            cell_depth[np.logical_not(valid_data)] = np.nan

            # Compute bottom speed
            speed_bot = self.speed_bot(
                bot_method,
                exponent,
                idx_bot,
                bot_rng,
                speed,
                cell_size,
                cell_depth,
                depth,
                z,
            )
        else:
            speed_bot = np.nan
            bot_rng = np.nan

        return speed_bot, bot_rng

    def change_rotation(self, misalignment):
        """Change vertical beam 3 rotation.

        Parameters
        ----------
        misalignment: float
            Total misalignment.
        """

        self.beam_3_rotation_deg = misalignment - self.beam_3_misalignment_deg

        # Apply setting and recompute the profiles
        settings = self.current_settings()
        self.compute_profile(settings)

    def change_mag_var(self, magvar):
        """Change magnetic variation.

        Parameters
        ----------
        magvar: float
            Magnetic variation in degrees.
        """

        if self.data.sensors is not None:
            # Change magvar
            self.data.change_magvar(magvar)

            # Apply setting and recompute the profiles
            settings = self.current_settings()
            self.compute_profile(settings)

    def change_heading_source(self, h_source):
        """Changes heading source (internal or external).

        Parameters
        ----------
        h_source: str
            Heading source (internal or external or user)
        """

        if self.data.sensors is not None and self.data.w_vel.coord_sys == "Earth":
            # Change heading source
            self.data.change_heading_source(h_source)

            # Apply setting and recompute the profiles
            settings = self.current_settings()

            self.compute_profile(settings)

    def change_sos(
        self, parameter=None, salinity=None, temperature=None, selected=None, speed=None
    ):
        """Coordinates changing the speed of sound.

        Parameters
        ----------
        parameter: str
            Speed of sound parameter to be changed ('temperatureSrc', 'temperature',
            'salinity', 'sosSrc')
        salinity: float
            Salinity in ppt
        temperature: float
            Temperature in deg C
        selected: str
            Selected speed of sound ('internal', 'computed', 'user') or
            temperature ('internal', 'user')
        speed: float
            Manually supplied speed of sound for 'user' source
        """

        if self.data.sensors is not None:
            self.data.change_sos(
                parameter=parameter,
                salinity=salinity,
                temperature=temperature,
                selected=selected,
                speed=speed,
            )

            # Apply setting and recompute the profiles
            settings = self.current_settings()
            self.compute_profile(settings)

    def change_draft(self, draft):
        """Change ADCP depth below water surface.

        Parameters
        ----------
        draft: float
            New draft value
        """

        # Update VerticalData
        self.data.depths.bt_depths.change_draft(draft)
        if self.data.depths.vb_depths is not None:
            self.data.depths.vb_depths.change_draft(draft)

        # Update vertical attributes
        self.adcp_depth_below_ws_m = draft
        if self.ws_condition == "Ice":
            self.adcp_depth_below_ice_m = (
                self.adcp_depth_below_ws_m - self.ws_to_slush_bottom_m
            )

        # Apply setting and recompute the profiles
        settings = self.current_settings()
        self.compute_profile(settings)

    def change_draft_ice(self, draft):
        """Change ADCP depth below ice (slush).

        Parameters
        __________
        draft: float
            New ADCP depth below ice
        """

        self.adcp_depth_below_ice_m = draft
        adcp_ws_draft = self.ws_to_slush_bottom_m + self.adcp_depth_below_ice_m

        self.change_draft(draft=adcp_ws_draft)

        # Apply setting and recompute the profiles
        settings = self.current_settings()
        self.compute_profile(settings)

    def change_slush_bottom(self, value):
        """Changes depth to bottom of slush and makes appropriate adjustments for
        consistency with draft.

        Parameters
        ----------
        value: float
            New value for depth to slush bottom
        """

        self.ws_to_slush_bottom_m = value
        adcp_ws_draft = self.ws_to_slush_bottom_m + self.adcp_depth_below_ice_m

        # Update adcp depth below ws, which applies settings and computes profile
        self.change_draft(draft=adcp_ws_draft)

        # Apply setting and recompute the profiles
        settings = self.current_settings()
        self.compute_profile(settings)

    def change_vel_source(self, source, speed=None):
        """Change source of velocity.

        Parameters
        ----------
        source: str
            Source of velocity data
        speed: float
            Manually entered speed
        """

        self.mean_speed_source = source
        if source == "Manual":
            self.mean_speed_mps = speed
            self.mean_velocity_direction_deg = np.nan
            self.mean_velocity_magnitude_mps = speed
            self.mean_velocity_direction_stdev_deg = np.nan
            self.mean_velocity_magnitude_cv = np.nan
            self.velocity_method = "Manual"
            self.mean_profile = {
                "cell_depth": np.array([(self.depth_m / 2) + self.ws_to_slush_bottom_m]),
                "cell_size": np.array([self.depth_m]),
                "speed": np.array([self.mean_speed_mps]),
                "number_ensembles": np.array([0]),
                "u": np.array([np.nan]),
                "v": np.array([np.nan]),
                "rssi": np.array([np.nan]),
                "mag": np.array([np.nan]),
                "dir": np.array([np.nan]), }
        else:
            if self.velocity_method == "Manual":
                self.velocity_method = self.velocity_method_orig
            self.compute_profile()

    def change_excluded_top(self, top_type, value):
        """Change cells or distance marked invalid below transducer.

        Parameters
        ----------
        top_type: str
            Type specified (Cells, Distance)
        value: float
            Number of cells or distance in m
        """

        # Determine distance
        if top_type == "Cells":
            self.data.w_vel.excluded_top_type = "Cells"
            self.data.w_vel.excluded_top = value
        else:
            self.data.w_vel.excluded_top_type = "Distance"
            self.data.w_vel.excluded_top = value

        # Apply change and recompute profile
        self.data.w_vel.apply_filter(transect=self.data)
        self.compute_profile()

    def change_excluded_bottom(self, bottom_type, value):
        """Change cells or distance marked invalid above sidelobe.

        Parameters
        ----------
        bottom_type: str
            Type specified (Cells, Distance)
        value: float
            Number of cells or distance in m
        """

        # Determine distance
        if bottom_type == "Cells":
            self.data.w_vel.excluded_bottom_type = "Cells"
            self.data.w_vel.excluded_bottom = value
        else:
            self.data.w_vel.excluded_bottom_type = "Distance"
            self.data.w_vel.excluded_bottom = value

        # Apply change and recompute profile
        self.data.w_vel.apply_filter(transect=self.data)
        self.compute_profile()

    def change_fit_method(
        self, fit_method, top=None, bot=None, exponent=None, ice_exponent=None
    ):
        """Changes the extrapolation.

        Parameters
        ----------
        fit_method:str
            Specifies fit method (Automatic, Manual)
        top: str
            Specifies top extrapolation method (Power, Constant, 3-Point, Ice)
        ice_exponent: float
            Exponent for top no slip if Ice method selected
        bot: str
            Specifies bottom extrapolation method (Power, No Slip)
        exponent: float
            Exponent for power or no slip fits
        """

        self.extrapolation.extrap_fit.change_fit_method(
            vertical=self,
            new_fit_method=fit_method,
            top=top,
            bot=bot,
            exponent=exponent,
            ice_exponent=ice_exponent,
        )

        self.extrapolation.set_extrap_data(
            top=self.extrapolation.extrap_fit.sel_fit.top_method,
            bot=self.extrapolation.extrap_fit.sel_fit.bot_method,
            exp=self.extrapolation.extrap_fit.sel_fit.exponent,
            ice_exp=self.extrapolation.extrap_fit.sel_fit.ice_exponent,
        )

        self.compute_profile()

    @staticmethod
    def speed_bot(
        bot_method,
        exponent,
        idx_bot,
        bot_rng,
        component,
        cell_size,
        cell_depth,
        depth,
        z,
    ):
        """Computes the bottom extrapolated value of the provided component.

        Parameters
        ----------
        bot_method: str
            Bottom extrapolation method (Power, No Slip)
        exponent: float
            Exponent for power and no slip
        idx_bot:
            Index to the bottom most valid depth cell in each ensemble
        bot_rng: float
            Range from the streambed to the bottom of the bottom most cell
        component: np.array(float)
            The variable to be extrapolated
        cell_size: np.array(float)
            Array of cell sizes (n cells x n ensembles)
        cell_depth: np.array(float)
            Depth of each cell (n cells x n ensembles)
        depth: float
            Bottom depth for each ensemble
        z: np.array(float)
            Relative depth from the bottom to each depth cell

        Returns
        -------
        bot_value: np.array(float)
            Total for the specified component integrated over the bottom range for
            each ensemble
        """

        # Initialize
        coef = 0

        # Bottom power extrapolation
        if bot_method == "Power":
            numerator = (exponent + 1) * np.nansum(component * cell_size, 0)
            denominator = np.nansum(
                ((z + 0.5 * cell_size) ** (exponent + 1))
                - (z - 0.5 * cell_size) ** (exponent + 1),
                0,
            )
            coef = np.divide(numerator, denominator, where=denominator != 0)

        # Bottom no slip extrapolation
        elif bot_method == "No Slip":
            # Valid data in the lower 20% of the water column or
            # the last valid depth cell are used to compute the no slip power fit
            cutoff_depth = np.round(0.8 * depth, 2)
            depth_ok = nan_greater_equal(
                np.round(cell_depth, 2), np.tile(cutoff_depth, cell_depth.shape[0])
            )
            component_ok = np.logical_not(np.isnan(component))
            use_ns = depth_ok * component_ok
            if idx_bot >= 0:
                use_ns[idx_bot] = 1

            # Create cross product and z arrays for the data to be used in
            # no slip computations
            component_ns = np.copy(component)
            component_ns[np.logical_not(use_ns)] = np.nan
            z_ns = np.copy(z)
            z_ns[np.logical_not(use_ns)] = np.nan
            numerator = (exponent + 1) * np.nansum(component_ns * cell_size, 0)
            denominator = np.nansum(
                ((z_ns + 0.5 * cell_size) ** (exponent + 1))
                - ((z_ns - 0.5 * cell_size) ** (exponent + 1)),
                0,
            )
            coef = np.divide(numerator, denominator, where=denominator != 0)
            if denominator == 0:
                coef = np.nan

        # Compute the bottom discharge of each profile
        bot_value = (coef / (exponent + 1)) * (bot_rng ** (exponent + 1)) / bot_rng

        return bot_value

    @staticmethod
    def weighted_mean(data, weights, axis=None):
        """Computes a weighted mean.

        Parameters
        ----------
        data: np.array()
            Array of data to be averaged
        weights: np.array()
            Array of weights
        axis: int
            Specifies to mean the rows (0) or columns (1)

        Returns
        -------
        data_mean: np.array()
            Mean of data
        """

        data_mean = np.nan
        if np.logical_not(np.all(np.isnan(data))):
            # Where there is no data set weights to nan
            weights = np.copy(weights)
            weights[np.isnan(data)] = np.nan

            # Compute weighted mean
            weighted_data = data * weights
            sum_weighted_data = np.nansum(weighted_data, axis=axis)
            if type(sum_weighted_data) is np.ndarray:
                sum_weighted_data[sum_weighted_data == 0] = np.nan
                sum_weights = np.nansum(weights, axis=axis)
                sum_weights[sum_weights == 0] = np.nan
                data_mean = sum_weighted_data / sum_weights
            else:
                sum_weights = np.nansum(weights, axis=axis)
                data_mean = sum_weighted_data / sum_weights

        return data_mean
