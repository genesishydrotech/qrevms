class DateTime(object):
    """Stores the date and time data in Python compatible format

    Attributes
    ----------
    date: str
        Measurement date as mm/dd/yyyy
    end_serial_time: float
        Python serial time for end of transect (seconds since 1/1/1970), timestamp
    ens_duration_sec: np.array(float)
        Duration of each ensemble, in seconds
    start_serial_time: float
        Python serial time for start of transect (seconds since 1/1/1970), timestamp
    transect_duration_sec: float
        Duration of transect or vertical, in seconds
    utc_time_offset: str
        String containing utc offset to local time

    """

    def __init__(self):
        """Initialize class and instance variables."""

        self.date = None
        self.start_serial_time = None
        self.end_serial_time = None
        self.transect_duration_sec = None
        self.ens_duration_sec = None
        self.utc_time_offset = "00:00:00"

    def populate_data(self, date_in, start_in, end_in, ens_dur_in, utc_time_offset=None):
        """Populate data in object.

        Parameters
        ----------
        date_in: str
            Measurement date as mm/dd/yyyy
        start_in: float
            Python serial time for start of transect.
        end_in: float
            Python serial time for end of transect.
        ens_dur_in: np.array(float)
            Duration of each ensemble, in seconds.
        utc_time_offset=str
            String containing utc time offset to acheive local time
        """

        self.date = date_in

        self.start_serial_time = start_in
        self.end_serial_time = end_in
        self.transect_duration_sec = float(end_in - start_in)
        self.ens_duration_sec = ens_dur_in.astype(float)
        self.utc_time_offset = utc_time_offset