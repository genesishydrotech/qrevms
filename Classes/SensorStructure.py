from Classes.SensorData import SensorData


class SensorStructure(object):
    """Stores sensor data from various sources.

    Attributes
    ----------
    external: SensorData
        Contains the data from an external sensor
    internal: SensorData
        Contains the data from the internal sensor
    selected: str
        The selected sensor reference name (internal, external, user)
    user: SensorData
        Contains user supplied value
    """

    def __init__(self):
        """Initialize class and set variable to None."""

        self.selected = None
        self.internal = None
        self.external = None
        self.user = None

    def set_selected(self, selected_name):
        """Set the selected source for the specified object

        Parameters
        ----------
        selected_name: str
            Type of data (internal, external, user).
        """
        self.selected = selected_name
