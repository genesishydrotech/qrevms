import os
import numpy as np
import pandas as pd
from datetime import datetime
import xml.etree.ElementTree as ETree
from xml.dom.minidom import parseString
from Modules.trdi import read_trdi
from Modules.sontek import read_sontek
from Classes.PreMeasurement import PreMeasurement
from Classes.Vertical import Vertical
from Classes.QAData import QAData
from Classes.Uncertainty import Uncertainty
from Modules.common_functions import dateformat
from Modules.local_time_utilities import local_time_from_iso, tz_formatted_string


class Measurement(object):
    """Class to hold all measurement details and coordinate reading and processing of data.

    Attributes
    ----------
    beam_3_misalignment_deg: float
        Adjustment to beam 3 direction for ADCP meter method
    beam_3_misalignment_orig_deg: float
        Beam 3 misalignment read from data file.
    cell_minimum_valid_ensembles_default: int
        Number of valid ensembles for data in the depth cell to be valid
    comments: list
        List of user comments
    compass_cal: list
        List of compass calibration objects of PreMeasurement
    compass_eval: list
        List of compass evaluation objects of PreMeasurement
    date_format: str
        String indicating date format to use
    discharge: dict
        Dictionary of measurement discharges
    discharge_method: str
        Method used to compute discharge (Mid, Mean)
    excluded: dict
        Agency settings for excluded distance for RioPro and M9
    filename: str
        Full name of input file including path
    kmeans: bool
        Indicates if kmeans filter should be applied for all ice verticals
    meas_number: str
        User assigned measurement number
    persons: str
        Persons collecting and/or processing the measurement
    qa: QAData
        Object of QAData
    snr_3beam_comp: bool
        Indicates if invalid data due to SNR filter should be computed using 3-beam
        solution
    stage_end_m: float
        Stage at end of measurement, in m
    stage_meas_m: float
        Stage assigned to measurement, in m
    stage_start_m: float
        Stage at start of measurement, in m
    start_edge: str
        Starting edge (Left or Right)
    station_name: str
        Station name
    station_number: str
        Station number
    station_q: dict
            User settings for station discharge caution and warning thresholds
    stations_inserts: str
            Message identifying with a manual station is inserted, later copied to QA messages
    summary: dataframe
        Summary table of for final values and settings
    system_tst: list
        List of system test objects of PreMeasurement
    tagline_azimuth_deg: float
        Azimuth of tagline, in degrees
    tagline_azimuth_orig_deg: float
        Original azimuth of tagline in degrees
    temp_chk: dict
        Dictionary of external temperature readings
    time_zone: str
        String containing UTC time zone
    time_zone_required: bool
        Indicates if a time zone entry is required by the agency
    tr: object
        Translation object
    uncertainty: Uncertainty
            Object of class Uncertainty
    use_ping_type: bool
        Indicates if ping type should be used in filters
    user_rating: str
        Optional user rating
    verticals: list
        List of Vertical objects
    verticals_sorted_idx: list
        List of vertical indices sorted by station_m in ascending order
    verticals_used_idx: list
        List of Boolean for each vertical
    """

    def __init__(self, agency_options, uncertainty="IVE", kmeans=False, qt_tr=None, time_zone_required=False):
        """Initialize class attributes.

        Parameters
        ----------
        agency_options: dict
            Dictionary containing agency optional parameters
        uncertainty: str
            Indicates uncertainty model to use
        kmeans: bool
            Indicates if kmeans filter should be used for ice measurements
        qt_tr: object
            Translation function
        time_zone_required: bool
            Indicates if the time zone is required to be specified
        """

        # Check for use of qt_tr for translation
        if qt_tr is None:
            self.tr = self.no_tr
        else:
            self.tr = qt_tr

        self.time_zone_required = time_zone_required
        self.time_zone = ""
        self.filename = ""
        self.station_name = ""
        self.station_number = ""
        self.meas_number = ""
        self.persons = ""
        self.beam_3_misalignment_deg = 0
        self.beam_3_misalignment_orig_deg = 0
        self.stage_start_m = np.nan
        self.stage_end_m = np.nan
        self.stage_meas_m = np.nan
        self.discharge_method = "Mid"
        self.tagline_azimuth_deg = np.nan
        self.tagline_azimuth_orig_deg = np.nan
        self.system_tst = []
        self.compass_cal = []
        self.compass_eval = []
        self.user_rating = "Not Rated"
        self.use_ping_type = True
        self.notes = []
        self.temp_chk = {
            "user": np.nan,
            "units": "C",
            "adcp": np.nan,
            "user_orig": np.nan,
            "adcp_orig": np.nan,
        }
        self.verticals = []
        self.verticals_sorted_idx = []
        self.verticals_used_idx = []
        self.start_edge = ""
        self.stations_inserts = []
        self.summary = pd.DataFrame(
            columns=[
                "Station Number",
                "Location",
                "Condition",
                "Duration",
                "Number Profiles",
                "Number Valid Profiles",
                "Number Cells",
                "Depth",
                "Depth StdDev",
                "Width",
                "Area",
                "Ice Area",
                "Slush Area",
                "Velocity Magnitude",
                "Velocity Magnitude CV",
                "Velocity Direction",
                "Velocity Direction StdDev",
                "Velocity Angle",
                "Flow Angle Coefficient",
                "Correction Factor",
                "Edge Coefficient",
                "Velocity Method",
                "Velocity Reference",
                "Measured Normal Velocity",
                "Measured Normal Velocity CV",
                "Normal Velocity",
                "Q Top",
                "Q Middle",
                "Q Bottom",
                "Q",
                "Q%",
                "Start Time",
                "End Time",
                "Stage",
                "Stage Time",
                "ADCP Depth",
                "Depth Source",
                "Ice Thickness",
                "Depth to Ice Bottom",
                "Depth to Slush Bottom",
                "ADCP Depth Below Ice",
                "Average Voltage",
                "BT % Invalid",
                "Depth % Invalid",
                "WT % Invalid",
                "Excluded Top",
                "Excluded Top Type",
                "Excluded Bottom",
                "Excluded Bottom Type",
                "Fit Method",
                "Top Method",
                "Ice Exponent",
                "Bottom Method",
                "Bottom Exponent",
                "MagVar",
                "Beam Misalignment",
                "Heading Source",
                "Mean Pitch",
                "Pitch StdDev",
                "Mean Roll",
                "Roll StdDev",
                "Temperature Source",
                "Mean Temperature",
                "Salinity",
                "SOS Source",
                "SOS",
                "Top Cell Speed",
                "Location Source",
            ]
        )
        self.discharge = {}
        self.comments = []
        self.qa = None
        self.uncertainty = Uncertainty(uncertainty)
        self.excluded = {
            "RioPro": agency_options["Excluded"]["RioPro"],
            "M9": agency_options["Excluded"]["M9"],
        }
        self.station_q = {
            "caution": agency_options["StationQ"]["caution"],
            "warning": agency_options["StationQ"]["warning"],
        }
        self.cell_minimum_valid_ensembles_default = 8
        self.snr_3beam_comp = agency_options["SNR"]["Use3Beam"]
        self.date_format = dateformat(agency_options["DateFormat"]["format"])
        self.kmeans = kmeans

    @staticmethod
    def no_tr(text):
        """This method replaces the pyqt tr method when this code is not run from a pyqt
        user interface. It simply returns the string provided.

        Parameters
        ----------
        text: str
            Input text string

        Returns
        -------
        text: str
            Same as input text
        """
        return text

    def from_sontek(self, filename):
        """Loads and assigns SonTek data to object classes.

        Parameters
        ----------
        filename: str
            Path and filename to SonTek data file
        """

        message = ""
        data_type = ""

        # Read Sontek data
        try:
            sontek_data, data_type = read_sontek(filename)
            if type(sontek_data) is str:
                message = sontek_data
        except Exception:
            sontek_data = None
            message = "Could not read Matlab file."

        if len(message) == 0:
            # Filename
            pathname, filename = os.path.split(filename)
            self.filename = filename

            # Assign data to object classes based on type of file selected
            if data_type == "rsqst":
                self.rsqst(sontek_data)
            elif data_type == "matst":
                if "MidSection" in sontek_data:
                    self.matst(sontek_data, pathname)
                else:
                    message = "The selected file is not a RiverSurveyor Stationary Live Matlab file"
            else:
                message = "Data type not recognized."

        return message

    def rsqst(self, sontek_data):
        """Assigns data from SonTek RSQ raw data files to QRev object classes

        Parameters
        ----------
        sontek_data: dict
            Dictionary of SonTek raw data
        """

        # Site information
        self.station_name = sontek_data["StationarySetup"]["SiteInformation"][
            "SiteName"
        ]
        self.station_number = sontek_data["StationarySetup"]["SiteInformation"][
            "StationNumber"
        ]
        self.meas_number = sontek_data["StationarySetup"]["SiteInformation"][
            "MeasurementNumber"
        ]
        self.persons = sontek_data["StationarySetup"]["SiteInformation"]["Operator"]

        # Comments entered in data collection software
        if sontek_data["StationarySetup"]["SiteInformation"]["Comments"] is not None:
            if len(sontek_data["StationarySetup"]["SiteInformation"]["Comments"]) > 0:
                self.comments.append(
                    "RSQ Comments: "
                    + sontek_data["StationarySetup"]["SiteInformation"]["Comments"]
                )

        # This Gauge_Height is stored as a string and doesn't get converted by changing units
        if (
            "GaugeHeightInformation"
            in sontek_data["StationarySetup"]["SiteInformation"]
        ):
            if (
                sontek_data["StationarySetup"]["SiteInformation"][
                    "GaugeHeightInformation"
                ]
                is not None
            ) and (
                len(
                    sontek_data["StationarySetup"]["SiteInformation"][
                        "GaugeHeightInformation"
                    ]
                )
                > 0
            ):
                self.comments.append(
                    "Gauge Height from Site Information: "
                    + sontek_data["StationarySetup"]["SiteInformation"][
                        "GaugeHeightInformation"
                    ]
                )
                try:
                    self.stage_meas_m = float(
                        sontek_data["StationarySetup"]["SiteInformation"][
                            "GaugeHeightInformation"
                        ]
                    )
                except ValueError:
                    self.stage_meas_m = np.nan

        # Compass Calibration
        # TODO Not sure about multiple calibrations
        cal = PreMeasurement()
        time_stamp = sontek_data["StationarySetup"]["CompassCalibration"][
            "CalibrationTime"
        ]
        cal.populate_data(time_stamp, sontek_data, "RSQCC")
        self.compass_cal.append(cal)

        # System Test
        # TODO Not sure about multiple system tests
        tst = PreMeasurement()
        time_stamp = sontek_data["StationarySetup"]["SystemTest"]["TestTime"]
        tst.populate_data(
            time_stamp, sontek_data["StationarySetup"]["SystemTest"], "RSQST"
        )
        self.system_tst.append(tst)

        # Discharge method
        if (
            sontek_data["StationarySetup"]["SystemConfiguration"]["DischargeMethod"]
            == "MidSection"
        ):
            self.discharge_method = "Mid"
        else:
            self.discharge_method = "Mean"

        self.tagline_azimuth_deg = sontek_data["StationarySetup"][
            "SystemConfiguration"
        ]["TaglineAzimuth (deg)"]
        self.tagline_azimuth_orig_deg = self.tagline_azimuth_deg

        hour = int(sontek_data["utc_time_offset"].split(":")[0])
        if hour > 0:
            self.time_zone = "UTC" + "+" + str(hour)
        elif hour < 0:
            self.time_zone = "UTC" + str(hour)
        else:
            self.time_zone = "UTC"

        # Verticals
        n_verticals = len(sontek_data["StationarySetup"]["RecordedStations"])
        system_configuration = sontek_data["StationarySetup"]["SystemConfiguration"]

        for n in range(n_verticals):
            station_data = sontek_data["StationarySetup"]["RecordedStations"][n]
            adcp_data = sontek_data["AdcpData"][n]
            self.verticals.append(
                Vertical(self.cell_minimum_valid_ensembles_default, self.kmeans)
            )
            if "Bank" in station_data["StationType"]:
                # Bank vertical
                start_edge = False
                if n == 0:
                    start_edge = True

                self.rsqst_edge(start_edge, station_data, sontek_data["utc_time_offset"])
            else:
                # Sampled vertical
                self.verticals[-1].from_rsqst(
                    station_data,
                    adcp_data,
                    system_configuration,
                    self.snr_3beam_comp,
                    sontek_data["utc_time_offset"],
                    self.date_format,
                )

        self.process_island_edges()

        # Order and finalize verticals
        self.sort_verticals()

        # Compute discharge using the specified method
        self.compute_discharge()

    def rsqst_edge(self, start_edge, station_data, utc_time_offset):
        """Creates an edge vertical

        Parameters
        ----------
        start_edge: bool
            Identifies if edge is at the start of the measurement
        station_data: dict
            Dictionary of station information
        """

        # Determine left or right edge
        if "Right" in station_data["StationType"]:
            edge_loc = "Right"
        elif "Left" in station_data["StationType"]:
            edge_loc = "Left"
        else:
            edge_loc = "Island"

        # Identify start edge
        if start_edge:
            self.start_edge = edge_loc

        # Populate edge vertical
        time_local = local_time_from_iso(station_data["GaugeHeightObservationTime"][0:-5],
                                         utc_time_offset)
        obs_time = tz_formatted_string(serial_time=time_local.timestamp(),
            utc_time_offset=utc_time_offset, format="%H:%M:%S")

        if station_data["UserWaterDepth (m)"] > 0:
            velocity_correction = station_data["VelocityCorrectionFactor"]
        else:
            velocity_correction = 0
        self.verticals[-1].edge(
            is_edge=True,
            edge_loc=edge_loc,
            depth_m=station_data["UserWaterDepth (m)"],
            depth_source="Manual",
            station_m=station_data["Location (m)"],
            velocity_correction=velocity_correction,
            edge_coef=0.707,
            use=station_data["UseStation"],
            gh_m=station_data["GaugeHeight (m)"],
            gh_obs_time=obs_time,
        )

    def matst(self, sontek_data, pathname):
        """Assigns data from SonTek Matlab output from RSSL to QRev object classes

        Parameters
        ----------
        sontek_data: dict
            Dictionary of SonTek data
        pathname:
            Path to location for SonTek data
        """

        # Site information pulled from last file
        if "SiteInfo" in sontek_data:
            if hasattr(sontek_data["SiteInfo"], "Site_Name"):
                if len(sontek_data["SiteInfo"].Site_Name) > 0:
                    self.station_name = sontek_data["SiteInfo"].Site_Name
                else:
                    self.station_name = ""
            if hasattr(sontek_data["SiteInfo"], "Station_Number"):
                if len(sontek_data["SiteInfo"].Station_Number) > 0:
                    self.station_number = sontek_data["SiteInfo"].Station_Number
                else:
                    self.station_number = ""
            if hasattr(sontek_data["SiteInfo"], "Meas_Number"):
                if len(sontek_data["SiteInfo"].Meas_Number) > 0:
                    self.meas_number = sontek_data["SiteInfo"].Meas_Number
            if hasattr(sontek_data["SiteInfo"], "Party"):
                if len(sontek_data["SiteInfo"].Party) > 0:
                    self.persons = sontek_data["SiteInfo"].Party

            if hasattr(sontek_data["SiteInfo"], "Comments"):
                if len(sontek_data["SiteInfo"].Comments) > 0:
                    self.comments.append(
                        "RS Comments: " + sontek_data["SiteInfo"].Comments
                    )

            # This Gauge_Height is stored as a string and doesn't get converted by changing units
            if hasattr(sontek_data["SiteInfo"], "Gauge_Height"):
                if len(sontek_data["SiteInfo"].Gauge_Height) > 0:
                    self.comments.append(
                        "Gauge Height from Site Information: "
                        + sontek_data["SiteInfo"].Gauge_Height
                    )
                    try:
                        self.stage_meas_m = float(sontek_data["SiteInfo"].Gauge_Height)
                    except ValueError:
                        self.stage_meas_m = np.nan

        # Compass Calibration
        compass_cal_folder = ""
        folder_test1 = os.path.join(pathname, "CompassCal")
        folder_test2, _ = os.path.split(pathname)
        folder_test2 = os.path.join(folder_test2, "CompassCal")
        if os.path.isdir(folder_test1):
            compass_cal_folder = folder_test1
        elif os.path.isdir(folder_test2):
            compass_cal_folder = folder_test2
        time_stamp = None
        if os.path.isdir(compass_cal_folder):
            for file in os.listdir(compass_cal_folder):
                valid_file = False
                # G3 compasses
                if file.endswith(".ccal"):
                    time_stamp = file.split("_")
                    time_stamp = time_stamp[0] + "_" + time_stamp[1]
                    valid_file = True

                # G2 compasses
                elif file.endswith(".txt"):
                    prefix, _ = os.path.splitext(file)
                    time_stamp = prefix.split("l")[1]
                    valid_file = True

                if valid_file:
                    with open(os.path.join(compass_cal_folder, file)) as f:
                        cal_data = f.read()
                        cal = PreMeasurement()
                        cal.populate_data(time_stamp, cal_data, "SCC")
                        self.compass_cal.append(cal)

        # System Test
        system_test_folder = ""
        folder_test1 = os.path.join(pathname, "SystemTest")
        folder_test2, _ = os.path.split(pathname)
        folder_test2 = os.path.join(folder_test2, "SystemTest")
        if os.path.isdir(folder_test1):
            system_test_folder = folder_test1
        elif os.path.isdir(folder_test2):
            system_test_folder = folder_test2
        if os.path.isdir(system_test_folder):
            for file in os.listdir(system_test_folder):
                # Find system test files.
                if file.startswith("SystemTest"):
                    with open(os.path.join(system_test_folder, file)) as f:
                        test_data = f.read()
                        test_data = test_data.replace("\x00", "")
                    time_stamp = file[10:24]
                    sys_test = PreMeasurement()
                    sys_test.populate_data(
                        time_stamp=time_stamp, data_in=test_data, data_type="SST"
                    )
                    self.system_tst.append(sys_test)

        # Discharge method
        if sontek_data["Setup"].dischargeMethod == 0:
            self.discharge_method = "Mid"
        else:
            self.discharge_method = "Mean"

        # Tagline azimuth
        self.tagline_azimuth_deg = sontek_data["Setup"].transectAzimuth
        self.tagline_azimuth_orig_deg = self.tagline_azimuth_deg

        # Determine the number of stations including edges
        stations = sontek_data["MidSection"].Stations._fieldnames
        n_verticals = len(stations)

        for n in range(n_verticals):
            self.verticals.append(
                Vertical(self.cell_minimum_valid_ensembles_default, self.kmeans)
            )
            stations_item = getattr(sontek_data["MidSection"].Stations, stations[n])
            cell_start = sontek_data["StationSystem"].Cell_Start[n]

            if stations_item.IsEdge:
                # Edge vertical
                self.matst_edge(sontek_data, n, n_verticals, stations_item)
            else:
                # The MidSection structure in Matlab is in location order not in order as
                # collected, which is the order the raw data are stored. The data range
                # must be associated with the MidSection structure.
                data_indices = np.where(
                    sontek_data["RawStation"].Station_ID
                    == sontek_data["StationStation"].Station_ID[n]
                )[0]

                self.verticals[-1].from_matst(
                    sontek_data,
                    n,
                    stations_item,
                    data_indices,
                    cell_start,
                    self.excluded,
                    self.snr_3beam_comp,
                    self.date_format,
                )
        self.process_island_edges()

        # Independent temperature
        # SonTek Water Temperature defaults to zero.
        # Zero may be acceptable if the water surface condition is ice
        if sontek_data["Setup"].WaterTemperature > 0:
            self.temp_chk["user"] = sontek_data["Setup"].WaterTemperature
            self.temp_chk["user_orig"] = self.temp_chk["user"]
        else:
            for vertical in self.verticals:
                if vertical.ws_condition == "Ice":
                    self.temp_chk["user"] = sontek_data["Setup"].WaterTemperature
                    self.temp_chk["user_orig"] = self.temp_chk["user"]
                    break

        # Order and finalize verticals
        self.sort_verticals()

        # Compute discharge using the specified method
        self.compute_discharge()

    def matst_edge(self, sontek_data, n, n_verticals, stations_item):
        """Creates edge vertical from RSSL output.
        Parameters
        ----------
        sontek_data: dict
            Dictionary of data in RSSL Matlab output
        n: int
            Vertical number
        n_verticals: int
            Total number of verticals
        stations_item: mat_struct
            Information associated with the station (vertical)

        """

        # Start edge
        edge_loc = ""
        if n == 0:
            if sontek_data["Setup"].startEdge > 0.1:
                self.start_edge = "Right"
                edge_loc = "Right"
            else:
                self.start_edge = "Left"
                edge_loc = "Left"

        # End edge
        elif n == n_verticals - 1:
            if self.start_edge == "Right":
                edge_loc = "Left"
            else:
                edge_loc = "Right"

        # Island edge
        elif sontek_data["StationStation"].Island_Edge[n]:
            edge_loc = "Island"

        # Populate edge vertical
        if (
            sontek_data["StationStation"].Station_Depth[n]
            == sontek_data["StationStation"].User_Input_Water_Depth[n]
        ):
            depth_source = "Manual"
        else:
            depth_source = "Computed"
        if sontek_data["StationStation"].GH_Observation_Time[n] > 0:
            gh_obs_time = datetime.strftime(
                datetime.utcfromtimestamp(
                    stations_item.GaugeHeightObservationTime
                    + ((30 * 365) + 7) * 24 * 60 * 60
                ),
                "%H:%M:%S",
            )
        else:
            gh_obs_time = ""

        if sontek_data["StationStation"].Station_Depth[n] == 0:
            velocity_correction = 0
        else:
            velocity_correction = sontek_data[
                "StationStation"
            ].Velocity_Correction_Factor[n]

        self.verticals[-1].edge(
            is_edge=stations_item.IsEdge,
            edge_loc=edge_loc,
            depth_m=sontek_data["StationStation"].Station_Depth[n],
            depth_source=depth_source,
            station_m=sontek_data["StationStation"].Location[n],
            velocity_correction=velocity_correction,
            edge_coef=0.707,
            use=stations_item.Active,
            gh_m=sontek_data["StationStation"].Gauge_Height[n],
            gh_obs_time=gh_obs_time,
        )

    def process_island_edges(self):
        """Identifies island edges with non-zero depths and adds adjacent vertical with
        zero depth to properly compute an island with a vertical edge such as a pier.
        """

        # Turn on watch for island edge
        island_watch = True
        n_verticals = len(self.verticals)

        for idx in range(n_verticals):
            if self.verticals[idx].ws_condition == "Island" and self.verticals[idx].use and self.verticals[idx].depth_m > 0:
                if self.verticals[idx + 1].ws_condition == "Island" and island_watch:
                    # Turn off watch for island edge because next island edge is the
                    # second in the pair
                    island_watch = False
                    # Determine direction of stationing and insert two manual island
                    # verticals just inside each island edge.
                    if self.verticals[idx].location_m > self.verticals[idx + 1].location_m:
                        self.insert_manual_vertical(
                            location=self.verticals[idx].location_m - 0.0001,
                            depth=0,
                            velocity=0,
                            ws_condition="Island",
                            ice_thickness=0,
                            ice_bottom=0,
                            slush_bottom=0,
                            use=True,
                            velocity_correction=0.0
                        )
                        self.insert_manual_vertical(
                            location=self.verticals[idx + 1].location_m + 0.0001,
                            depth=0,
                            velocity=0,
                            ws_condition="Island",
                            ice_thickness=0,
                            ice_bottom=0,
                            slush_bottom=0,
                            use=True,
                            velocity_correction=0.0
                        )
                    else:
                        self.insert_manual_vertical(
                            location=self.verticals[idx].location_m + 0.0001,
                            depth=0,
                            velocity=0,
                            ws_condition="Island",
                            ice_thickness=0,
                            ice_bottom=0,
                            slush_bottom=0,
                            use=True,
                            velocity_correction=0.0
                        )
                        self.insert_manual_vertical(
                            location=self.verticals[idx + 1].location_m - 0.0001,
                            depth=0,
                            velocity=0,
                            ws_condition="Island",
                            ice_thickness=0,
                            ice_bottom=0,
                            slush_bottom=0,
                            use=True,
                            velocity_correction=0.0
                        )
            else:
                island_watch = True

    def from_trdi(self, filename):
        """Loads and assigns TRDI data to object classes. Calls methods to compute discharge.

        Parameters
        ----------
        filename: str
            Path and filename to TRDI mmt file
        """

        message = ""

        # Read TRDI mmt file and check compatibility
        trdi_data = read_trdi(filename)
        if trdi_data["type"] == "WR2":
            message = (
                filename
                + " is not a SxS file. Use QRevInt to process moving-boat measurements."
            )
            return message
        else:
            mmt = trdi_data["mmt"]
            pd0_sections = trdi_data["pd0_list"]

            # Check for misformed SxS mmt file
            valid = self.check_mmt(mmt, pd0_sections)

            if not valid:
                message = (
                    filename
                    + " is a SxS file that is not correctly formatted. "
                    + "Use SxS Pro to fix the errors and then reload into QRevIntMS"
                )
                return message
            else:
                # Filename
                pathname, filename = os.path.split(filename)
                self.filename = filename

                # Site visit information
                if mmt["Site_Information"]["Name"] is not None:
                    self.station_name = mmt["Site_Information"]["Name"]
                if mmt["Site_Information"]["Number"] is not None:
                    self.station_number = mmt["Site_Information"]["Number"]
                if mmt["Site_Information"]["MeasurementNmb"] is not None:
                    self.meas_number = mmt["Site_Information"]["MeasurementNmb"]
                if mmt["Site_Information"]["Party"] is not None:
                    self.persons = mmt["Site_Information"]["Party"]
                if mmt["Site_Information"]["Outside_Gage_Height"] is not None:
                    self.stage_meas_m = float(
                        mmt["Site_Information"]["Outside_Gage_Height"]
                    )
                # Save comments from mmt file in comments
                if mmt["Site_Information"]["Remarks"] is not None:
                    self.comments.append(
                        "MMT Remarks: " + mmt["Site_Information"]["Remarks"]
                    )

                if "Water_Temperature" in mmt["Site_Information"]:
                    temp = mmt["Site_Information"]["Water_Temperature"]
                    if temp != "-32768":
                        self.temp_chk["user"] = float(temp)
                        self.temp_chk["user_orig"] = float(temp)

                # System Test and Compass Cal/Eval
                if "QA_QC" in mmt:
                    if "RG_Test" in mmt["QA_QC"].keys():
                        # Single system test
                        if not isinstance(mmt["QA_QC"]["RG_Test"]["TestResult"], list):
                            p_m = PreMeasurement()
                            p_m.populate_data(
                                mmt["QA_QC"]["RG_Test"]["TestResult"]["TimeStamp"],
                                mmt["QA_QC"]["RG_Test"]["TestResult"]["Text"],
                                "TST",
                            )
                            self.system_tst.append(p_m)

                        # Multiple system tests
                        else:
                            for test in mmt["QA_QC"]["RG_Test"]["TestResult"]:
                                p_m = PreMeasurement()
                                p_m.populate_data(
                                    test["TimeStamp"], test["Text"], "TST"
                                )
                                self.system_tst.append(p_m)

                    if "Compass_Calibration" in mmt["QA_QC"]:
                        p_m = PreMeasurement()

                        # Single compass calibration
                        if not isinstance(
                            mmt["QA_QC"]["Compass_Calibration"]["TestResult"], list
                        ):
                            p_m.populate_data(
                                mmt["QA_QC"]["Compass_Calibration"]["TestResult"][
                                    "TimeStamp"
                                ],
                                mmt["QA_QC"]["Compass_Calibration"]["TestResult"][
                                    "Text"
                                ],
                                "TCC",
                            )
                            self.compass_cal.append(p_m)

                        # Multiple compass calibrations
                        else:
                            for cal in mmt["QA_QC"]["Compass_Calibration"][
                                "TestResult"
                            ]:
                                p_m.populate_data(cal["TimeStamp"], cal["Text"], "TST")
                                self.compass_cal.append(p_m)

                    if "Compass_Evaluation" in mmt["QA_QC"]:
                        p_m = PreMeasurement()

                        # Single compass evaluation
                        if not isinstance(
                            mmt["QA_QC"]["Compass_Evaluation"]["TestResult"], list
                        ):
                            p_m.populate_data(
                                mmt["QA_QC"]["Compass_Evaluation"]["TestResult"][
                                    "TimeStamp"
                                ],
                                mmt["QA_QC"]["Compass_Evaluation"]["TestResult"][
                                    "Text"
                                ],
                                "TCC",
                            )
                            self.compass_eval.append(p_m)

                        # Multiple compass evaluations
                        else:
                            for evaluation in mmt["QA_QC"]["Compass_Evaluation"][
                                "TestResult"
                            ]:
                                p_m.populate_data(
                                    evaluation["TimeStamp"], evaluation["Text"], "TCC"
                                )
                                self.compass_eval.append(p_m)

                # Get latest configuration settings
                if type(mmt["Site_Discharge"]["Measurement"]["Configuration"]) is list:
                    cfg = mmt["Site_Discharge"]["Measurement"]["Configuration"][-1]
                else:
                    cfg = mmt["Site_Discharge"]["Measurement"]["Configuration"]

                # Beam 3 misalignment, applies only to fixed method
                if "Beam3_Misalignment" in cfg["Offsets"]:
                    self.beam_3_misalignment_deg = float(
                        cfg["Offsets"]["Beam3_Misalignment"]
                    )
                    self.beam_3_misalignment_orig_deg = self.beam_3_misalignment_deg

                # Tagline azimuth, applies only to azimuth method
                if "TagLine_Angle" in cfg["Offsets"]:
                    self.tagline_azimuth_deg = float(cfg["Offsets"]["TagLine_Angle"])
                    self.tagline_azimuth_orig_deg = self.tagline_azimuth_deg

                # TRDI only has one entry for gage height.
                # It is repeated here for each vertical. Time is not provided.
                gh_m = float(mmt["Site_Information"]["Outside_Gage_Height"])

                # Initialize Edges
                for edge in cfg["Edge"]:
                    if edge["LeftEdge"] == "YES":
                        edge_loc = "Left"
                    else:
                        edge_loc = "Right"
                    if edge["StartEdge"] == "YES":
                        self.start_edge = edge_loc

                    self.verticals.append(
                        Vertical(self.cell_minimum_valid_ensembles_default, self.kmeans)
                    )

                    self.verticals[-1].edge(
                        is_edge=True,
                        edge_loc=edge_loc,
                        depth_m=float(edge["EdgeDepth"]),
                        depth_source="Manual",
                        station_m=float(edge["EdgeDistanceFromIP"]),
                        velocity_correction=float(edge["EdgeVelCor"]),
                        gh_m=gh_m,
                        edge_coef=float(edge["SlopeCoeff"]),
                    )

                # Discharge method
                if cfg["Processing"]["SxSDischargeMethod"] == "0":
                    self.discharge_method = "Mid"
                else:
                    self.discharge_method = "Mean"

                # Verticals
                measured_idx = 0
                for section in cfg["Section"]:
                    self.verticals.insert(
                        -1,
                        Vertical(
                            self.cell_minimum_valid_ensembles_default, self.kmeans
                        ),
                    )
                    self.verticals[-2].from_trdi(
                        mmt,
                        cfg,
                        section,
                        measured_idx,
                        pd0_sections,
                        self.beam_3_misalignment_deg,
                        self.tagline_azimuth_deg,
                        self.excluded,
                        self.date_format,
                    )
                    if self.verticals[-2].data.w_vel is not None:
                        measured_idx += 1

                    # Apply user salinity or speed of sound, if specified
                    if cfg["Processing"]["Speed_of_Sound_Correction"] == 1:
                        self.verticals[-2].data.change_sos(
                            parameter="salinity", selected="user"
                        )
                    elif cfg["Processing"]["Speed_of_Sound_Correction"] == 2:
                        self.verticals[-2].data.change_sos(
                            parameter="sosSrc", selected="user"
                        )

                # Order and finalize verticals
                self.sort_verticals()

                # Compute discharge using the specified method
                self.compute_discharge()

                return message

    def from_qrevms(self, qt_tr=None):
        """Updates the loaded qrevms file for features that have been added
        since the measurement was previously save. These updates won't affect
        the initial display and are turned off by default to prevent there use
        when recalculating."""

        # Check for use of qt_tr for translation
        if qt_tr is None:
            self.tr = self.no_tr
        else:
            self.tr = qt_tr

        # KMeans addition
        if not hasattr(self, "kmeans"):
            self.kmeans = False
            for vertical in self.verticals:
                vertical.kmeans = False
                
        # SNR 3 Beam computation addition
        if not hasattr(self, "snr_3beam_comp"):
            self.snr_3beam_comp = False
            for vertical in self.verticals:
                if vertical.data.w_vel is not None:
                    vertical.data.w_vel.snr_3beam_comp = False
                    
        # Date format addition
        if not hasattr(self, "date_format"):
            self.date_format = "%Y.%m.%d"
            
        # Time zone required addition
        if not hasattr(self, "time_zone_required"):
            self.time_zone_required = False
        
        # Translation addition
        self.qa.tr = self.tr
            
        # Main qa type addition
        if not hasattr(self.qa, "main"):
            self.qa.main = {"messages": [], "guidance": [], "status": "good"}
        
        # Rating qa type addition
        if not hasattr(self.qa, "rating"):
            self.qa.rating = {"messages": [], "guidance": [], "status": "good"}
        
        # Fix previous bug showing custom for any message
        if self.qa.settings_dict["stations_tab"] == "Custom":
            self.qa.settings_dict["stations_tab"] = "Default"
            self.qa.stations["guidance"] = []
            self.qa.tr = self.tr
            self.qa.check_stations_settings(self)
            
        for vertical in self.verticals:
            if vertical.data.date_time is not None:
                if not hasattr(vertical.data.date_time, "utc_time_offset"):
                    vertical.data.date_time.utc_time_offset = "00:00:00"
                    self.time_zone = ""
                elif  vertical.data.date_time.utc_time_offset is None:
                    self.time_zone = ""
                else:
                    self.time_zone = "UTC" + str(int(vertical.data.date_time.utc_time_offset.split(":")[0]))

        qa_check_keys = ["bt_vel", "compass", "depths", "edges", "extrapolation",
                         "stations", "verticals", "system_tst", "temperature", "user",
                         "w_vel", "main", "rating"]

        # Account for older saved files that did not have guidance stored.
        for key in qa_check_keys:
            if hasattr(self.qa, key):
                qa_key = getattr(self.qa, key)
                if "guidance" not in qa_key:
                    qa_key["guidance"] = ""

    @staticmethod
    def check_mmt(mmt, pd0_sections):
        """Checks the mmt file to make sure it is configured properly. Identifies
        a problem when mmt files are not configured properly.

        Parameters
        ----------
        mmt: dict
            Dictionary of mmt file data
        pd0_sections: list
            List of sections from the pd0 file

        Returns
        -------
        playback_pass: bool
            Indicates if the playback portion of the mmt file passed the checks
        """

        # Determine number of valid pd0 segments
        num_valid_pd0_segments = 0
        check_list = []
        for section in pd0_sections:
            if len(section["n"]) > 0:
                num_valid_pd0_segments += 1
                check_list.append(section["Sxs"].dist_ip)

        # Determine number of valid measured playback segments
        num_valid_playback_segments = 0
        try:
            for segment in mmt["Site_Discharge"]["Measurement"]["Configuration"][-1][
                "Section"
            ]:
                if segment["SectionMadeUp"] == "NO":
                    num_valid_playback_segments += 1
        except KeyError:
            for segment in mmt["Site_Discharge"]["Measurement"]["Configuration"][
                "Section"
            ]:
                if segment["SectionMadeUp"] == "NO":
                    num_valid_playback_segments += 1

        # Perform pass/fail check for playback segments
        if num_valid_pd0_segments == num_valid_playback_segments:
            playback_pass = True
        else:
            playback_pass = False

        return playback_pass

    def sort_verticals(self):
        """Creates an index of vertical sorted by station and
        then an index of sorted verticals to be used for discharge computations.
        """

        # Create list of verticals to sort
        stations = []

        for vertical in self.verticals:
            if vertical.data.date_time is None:
                stations.append((vertical.location_m, 0))
            else:
                stations.append(
                    (vertical.location_m, vertical.data.date_time.start_serial_time)
                )
        stations = np.array(stations, dtype=[("loc", np.float), ("start", np.float)])
        # Sort verticals in station order
        self.verticals_sorted_idx = np.argsort(stations, order=("loc", "start"))

        # Create list of vertical indices identifying verticals to be used for
        # discharge computations.
        self.verticals_used_idx = []
        vertical_location = []
        for idx in self.verticals_sorted_idx:
            if self.verticals[idx].use:
                self.verticals_used_idx.append(idx)
                vertical_location.append(self.verticals[idx].location_m)

        # Check for duplicate locations
        locations, counts = np.unique(vertical_location, return_counts=True)
        if np.any(np.greater(counts, 1)):
            # Find duplicate locations
            dup_locations = locations[np.greater(counts, 1)]
            idx_to_delete = []
            # Only the last data collected for a duplicate location is used and the
            # other verticals with the same location are not used.
            for loc in dup_locations:
                loc_idx = np.where(np.isclose(vertical_location, loc))[0]
                times = []
                for idx in loc_idx:
                    times.append(
                        self.verticals[
                            self.verticals_used_idx[idx]
                        ].data.date_time.start_serial_time
                    )
                last_time = np.argmax(times)

                for idx in loc_idx:
                    if idx != loc_idx[last_time]:
                        self.verticals[self.verticals_used_idx[idx]].use = False
                        idx_to_delete.append(idx)
            for idx in sorted(idx_to_delete, reverse=True):
                del self.verticals_used_idx[idx]

    def change_coord_sys(self, new_coord_sys):
        """Changes the coordinate system for water and boat velocities for all verticals.

        Parameters
        ----------
        new_coord_sys: str
            Specifies new coordinate system (Beam, Inst, Ship, Earth)
        """

        for vertical in self.verticals:
            if vertical.data.w_vel is not None:
                vertical.data.w_vel.change_coord_sys(
                    new_coord_sys, vertical.data.sensors, vertical.data.inst
                )
            if vertical.data.boat_vel is not None:
                vertical.data.boat_vel.change_coord_sys(
                    new_coord_sys, vertical.data.sensors, vertical.data.inst
                )

    def change_discharge_method(self, new_method):
        """Change discharge method.

        Parameters
        ----------
        new_method: str
            Discharge method (Mean, Mid)
        """

        # Assign new method
        self.discharge_method = new_method

        self.compute_discharge()

    def change_timezone (self, text):
        """Sets the time zone identifier and checks to see if time zone is required.
        
        text: str
            Text specifying UTC time zone
        """
        self.time_zone = text
        self.qa = QAData(self, tr=self.tr)

    def process_mid_section(self):
        """Computes the discharge based on the mid-section method described in ISO 748."""

        self.discharge = {
            "location_m": np.array([]),
            "depth_m": np.array([]),
            "width_m": np.array([]),
            "area_sqm": np.array([]),
            "ice_area_sqm": np.array([]),
            "slush_area_sqm": np.array([]),
            "speed_mps": np.array([]),
            "q_top_cms": np.array([]),
            "q_middle_cms": np.array([]),
            "q_bottom_cms": np.array([]),
            "q_cms": np.array([]),
            "q_%": np.array([]),
        }

        # Compute discharge for measured verticals
        for n, idx in enumerate(self.verticals_used_idx[1:-1]):
            # Compute width
            width_m = (
                np.abs(
                    self.verticals[self.verticals_used_idx[n + 2]].location_m
                    - self.verticals[self.verticals_used_idx[n]].location_m
                )
                / 2
            )

            # Compute area
            area_sqm = width_m * (
                self.verticals[idx].depth_m - self.verticals[idx].ws_to_slush_bottom_m
            )
            ice_area_sqm = width_m * self.verticals[idx].ws_to_ice_bottom_m
            slush_area_sqm = (
                width_m * self.verticals[idx].ws_to_slush_bottom_m
            ) - ice_area_sqm

            # Check for measured discharge
            if (
                self.verticals[idx].sampling_duration_sec > 0
                and self.verticals[idx].mean_profile["speed"].shape[0] > 0
                and self.verticals[idx].velocity_method != "Manual"
            ):
                # Compute top discharge
                top_speed = self.verticals[idx].mean_profile["speed"][0] * self.verticals[idx].flow_angle_coefficient
                top_rng = self.verticals[idx].mean_profile["cell_size"][0]
                q_top_cms = top_rng * top_speed * width_m

                # Compute middle discharge
                mid_depth = (
                    self.verticals[idx].depth_m
                    - self.verticals[idx].ws_to_slush_bottom_m
                    - self.verticals[idx].mean_profile["cell_size"][0]
                    - self.verticals[idx].mean_profile["cell_size"][-1]
                )

                q_middle_cms = (
                    mid_depth * width_m * self.verticals[idx].mean_measured_speed_mps
                )

                # Compute bottom discharge
                mask = np.logical_not(
                    np.isnan(self.verticals[idx].mean_profile["speed"])
                )
                if np.any(mask):
                    bot_speed = self.verticals[idx].mean_profile["speed"][mask][-1] * self.verticals[idx].flow_angle_coefficient
                    bot_rng = self.verticals[idx].mean_profile["cell_size"][mask][-1]
                    q_bottom_cms = bot_rng * bot_speed * width_m
                else:
                    q_bottom_cms = 0

                # Compute total discharge for vertical
                q_cms = q_top_cms + q_middle_cms + q_bottom_cms

            else:
                #
                if (
                    self.verticals[idx].mean_speed_source == "Manual"
                    and np.round(self.verticals[idx].velocity_correction, 2)
                ) != 0:
                    self.compute_manual_velocity(n + 1)
                q_cms = (
                    width_m
                    * (
                        self.verticals[idx].depth_m
                        - self.verticals[idx].ws_to_slush_bottom_m
                    )
                    * self.verticals[idx].mean_speed_mps
                )
                q_top_cms = np.nan
                q_middle_cms = np.nan
                q_bottom_cms = np.nan

            # Store results
            self.discharge["location_m"] = np.hstack(
                [self.discharge["location_m"], self.verticals[idx].location_m]
            )
            self.discharge["depth_m"] = np.hstack(
                [self.discharge["depth_m"], self.verticals[idx].depth_m]
            )
            self.discharge["width_m"] = np.hstack([self.discharge["width_m"], width_m])
            self.discharge["area_sqm"] = np.hstack(
                [self.discharge["area_sqm"], area_sqm]
            )
            self.discharge["ice_area_sqm"] = np.hstack(
                [self.discharge["ice_area_sqm"], ice_area_sqm]
            )
            self.discharge["slush_area_sqm"] = np.hstack(
                [self.discharge["slush_area_sqm"], slush_area_sqm]
            )
            self.discharge["speed_mps"] = np.hstack(
                [self.discharge["speed_mps"], self.verticals[idx].mean_speed_mps]
            )
            self.discharge["q_top_cms"] = np.hstack(
                [self.discharge["q_top_cms"], q_top_cms]
            )
            self.discharge["q_middle_cms"] = np.hstack(
                [self.discharge["q_middle_cms"], q_middle_cms]
            )
            self.discharge["q_bottom_cms"] = np.hstack(
                [self.discharge["q_bottom_cms"], q_bottom_cms]
            )
            self.discharge["q_cms"] = np.hstack([self.discharge["q_cms"], q_cms])

        # Compute discharge for start bank
        if self.verticals[self.verticals_used_idx[0]].is_edge:
            width_m = (
                np.abs(
                    self.verticals[self.verticals_used_idx[1]].location_m
                    - self.verticals[self.verticals_used_idx[0]].location_m
                )
                / 2
            )
            
            self.verticals[self.verticals_used_idx[0]].mean_velocity_magnitude_mps = 0
            self.verticals[self.verticals_used_idx[0]].mean_velocity_angle_deg = 0
            self.verticals[self.verticals_used_idx[0]].mean_speed_cv = 0

            # Determine speed at start bank
            self.verticals[self.verticals_used_idx[0]].mean_speed_mps = (
                self.verticals[self.verticals_used_idx[1]].mean_speed_mps
                * self.verticals[self.verticals_used_idx[0]].velocity_correction
            )

            # Area of start bank section
            area_sqm = width_m * (
                self.verticals[self.verticals_used_idx[0]].depth_m - self.verticals[self.verticals_used_idx[0]].ws_to_slush_bottom_m
            )
            ice_area_sqm = width_m * self.verticals[self.verticals_used_idx[0]].ws_to_ice_bottom_m
            slush_area_sqm = width_m * self.verticals[self.verticals_used_idx[0]].ws_to_slush_bottom_m
            q_cms = area_sqm * self.verticals[self.verticals_used_idx[0]].mean_speed_mps
            if np.isnan(q_cms):
                q_cms = 0
        else:
            # First vertical is not an edge. Assume width is equal to the distance to
            # the second vertical and use the measured velocity and depth data for
            # the first vertical.
            width_m = np.abs(
                self.verticals[self.verticals_used_idx[1]].location_m
                - self.verticals[self.verticals_used_idx[0]].location_m
            )

            # Area of start bank section
            area_sqm = width_m * (
                self.verticals[self.verticals_used_idx[0]].depth_m - self.verticals[self.verticals_used_idx[0]].ws_to_slush_bottom_m
            )
            ice_area_sqm = width_m * self.verticals[self.verticals_used_idx[0]].ws_to_ice_bottom_m
            slush_area_sqm = width_m * self.verticals[self.verticals_used_idx[0]].ws_to_slush_bottom_m
            q_cms = area_sqm * self.verticals[self.verticals_used_idx[0]].mean_speed_mps
            if np.isnan(q_cms):
                q_cms = 0

        # Store results
        self.discharge["location_m"] = np.hstack(
            [
                self.verticals[self.verticals_used_idx[0]].location_m,
                self.discharge["location_m"],
            ]
        )
        self.discharge["depth_m"] = np.hstack(
            [
                self.verticals[self.verticals_used_idx[0]].depth_m,
                self.discharge["depth_m"],
            ]
        )
        self.discharge["width_m"] = np.hstack([width_m, self.discharge["width_m"]])
        self.discharge["area_sqm"] = np.hstack([area_sqm, self.discharge["area_sqm"]])
        self.discharge["ice_area_sqm"] = np.hstack(
            [ice_area_sqm, self.discharge["ice_area_sqm"]]
        )
        self.discharge["slush_area_sqm"] = np.hstack(
            [slush_area_sqm, self.discharge["slush_area_sqm"]]
        )
        self.discharge["speed_mps"] = np.hstack(
            [
                self.verticals[self.verticals_used_idx[0]].mean_speed_mps,
                self.discharge["speed_mps"],
            ]
        )
        self.discharge["q_top_cms"] = np.hstack([np.nan, self.discharge["q_top_cms"]])
        self.discharge["q_middle_cms"] = np.hstack(
            [np.nan, self.discharge["q_middle_cms"]]
        )
        self.discharge["q_bottom_cms"] = np.hstack(
            [np.nan, self.discharge["q_bottom_cms"]]
        )
        self.discharge["q_cms"] = np.hstack([q_cms, self.discharge["q_cms"]])

        # Compute discharge for end bank section
        width_m = (
            np.abs(
                self.verticals[self.verticals_used_idx[-1]].location_m
                - self.verticals[self.verticals_used_idx[-2]].location_m
            )
            / 2
        )
        self.verticals[self.verticals_used_idx[-1]].mean_velocity_magnitude_mps = 0
        self.verticals[self.verticals_used_idx[-1]].mean_velocity_angle_deg = 0
        self.verticals[self.verticals_used_idx[-1]].mean_speed_cv = 0

        # Determine speed at end bank
        if self.verticals[self.verticals_used_idx[-1]].is_edge:
            self.verticals[self.verticals_used_idx[-1]].mean_speed_mps = (
                self.verticals[self.verticals_used_idx[-2]].mean_speed_mps
                * self.verticals[self.verticals_used_idx[-1]].velocity_correction
            )

            # Area of end bank section
            area_sqm = width_m * (
                self.verticals[self.verticals_used_idx[-1]].depth_m - self.verticals[self.verticals_used_idx[-1]].ws_to_slush_bottom_m
            )
            ice_area_sqm = width_m * self.verticals[self.verticals_used_idx[-1]].ws_to_ice_bottom_m
            slush_area_sqm = width_m * self.verticals[self.verticals_used_idx[-1]].ws_to_slush_bottom_m
            q_cms = (
                area_sqm * self.verticals[self.verticals_used_idx[-1]].mean_speed_mps
            )
            if np.isnan(q_cms):
                q_cms = 0

        else:
            # Last vertical is not an edge. Assume width is equal to the distance to
            # the second to last vertical and use the measured velocity and depth data for
            # the last vertical.
            width_m = np.abs(
                self.verticals[self.verticals_used_idx[-1]].location_m
                - self.verticals[self.verticals_used_idx[-2]].location_m
            )

            # Area of end bank section
            area_sqm = width_m * (
                self.verticals[self.verticals_used_idx[-1]].depth_m - self.verticals[self.verticals_used_idx[-1]].ws_to_slush_bottom_m
            )
            ice_area_sqm = width_m * self.verticals[self.verticals_used_idx[-1]].ws_to_ice_bottom_m
            slush_area_sqm = width_m * self.verticals[self.verticals_used_idx[-1]].ws_to_slush_bottom_m
            q_cms = (
                area_sqm * self.verticals[self.verticals_used_idx[-1]].mean_speed_mps
            )
            if np.isnan(q_cms):
                q_cms = 0

        # Store results
        self.discharge["location_m"] = np.hstack(
            [
                self.discharge["location_m"],
                self.verticals[self.verticals_used_idx[-1]].location_m,
            ]
        )
        self.discharge["depth_m"] = np.hstack(
            [
                self.discharge["depth_m"],
                self.verticals[self.verticals_used_idx[-1]].depth_m,
            ]
        )
        self.discharge["width_m"] = np.hstack([self.discharge["width_m"], width_m])
        self.discharge["area_sqm"] = np.hstack([self.discharge["area_sqm"], area_sqm])
        self.discharge["ice_area_sqm"] = np.hstack(
            [self.discharge["ice_area_sqm"], ice_area_sqm]
        )
        self.discharge["slush_area_sqm"] = np.hstack(
            [self.discharge["slush_area_sqm"], slush_area_sqm]
        )
        self.discharge["speed_mps"] = np.hstack(
            [
                self.discharge["speed_mps"],
                self.verticals[self.verticals_used_idx[-1]].mean_speed_mps,
            ]
        )
        self.discharge["q_top_cms"] = np.hstack([self.discharge["q_top_cms"], np.nan])
        self.discharge["q_middle_cms"] = np.hstack(
            [self.discharge["q_middle_cms"], np.nan]
        )
        self.discharge["q_bottom_cms"] = np.hstack(
            [self.discharge["q_bottom_cms"], np.nan]
        )
        self.discharge["q_cms"] = np.hstack([self.discharge["q_cms"], q_cms])

        # Compute total discharge
        q_total = np.nansum(self.discharge["q_cms"])
        # Compute percentage in each vertical
        for idx in range(self.discharge["q_cms"].shape[0]):
            self.discharge["q_%"] = np.hstack(
                [self.discharge["q_%"], (self.discharge["q_cms"][idx] / q_total) * 100]
            )

    def mid_section_summary(self):
        """Populates a data frame with vertical data and variables and discharges computed
        from the mid-section method.
        """

        # Create summary
        self.summary = self.summary[0:0]
        number = 0
        for idxq, idxv in enumerate(self.verticals_used_idx):
            number = number + 1

            # Set user specified excluded parameters or set to defaults for edges.
            if self.verticals[idxv].data.w_vel is not None:
                excluded_top = self.verticals[idxv].data.w_vel.excluded_top
                excluded_top_type = self.verticals[idxv].data.w_vel.excluded_top_type
                excluded_bottom = self.verticals[idxv].data.w_vel.excluded_bottom
                excluded_bottom_type = self.verticals[
                    idxv
                ].data.w_vel.excluded_bottom_type
            else:
                excluded_top = np.nan
                excluded_top_type = ""
                excluded_bottom = np.nan
                excluded_bottom_type = ""

            # Compute depth std dev
            if self.verticals[idxv].data.depths is not None:
                depths = getattr(
                    self.verticals[idxv].data.depths,
                    self.verticals[idxv].data.depths.selected,
                )
                depth_stddev = np.nanstd(depths.depth_processed_m, ddof=1)
            else:
                depth_stddev = np.nan

            # Get values for sensor data
            if self.verticals[idxv].data.sensors is not None:
                pitch = getattr(
                    self.verticals[idxv].data.sensors.pitch_deg,
                    self.verticals[idxv].data.sensors.pitch_deg.selected,
                )
                pitch_mean = np.nanmean(pitch.data)
                pitch_stddev = np.nanstd(pitch.data)
                roll = getattr(
                    self.verticals[idxv].data.sensors.roll_deg,
                    self.verticals[idxv].data.sensors.roll_deg.selected,
                )
                roll_mean = np.nanmean(roll.data)
                roll_stddev = np.nanstd(roll.data)
                magvar = self.verticals[
                    idxv
                ].data.sensors.heading_deg.internal.mag_var_deg
                heading_source = self.verticals[idxv].data.sensors.heading_deg.selected
                if heading_source.lower() == "internal":
                    heading_source = "Internal"
                elif heading_source.lower() == "user":
                    heading_source = "None"
                temperature_source = self.verticals[
                    idxv
                ].data.sensors.temperature_deg_c.selected
                temp = getattr(
                    self.verticals[idxv].data.sensors.temperature_deg_c,
                    self.verticals[idxv].data.sensors.temperature_deg_c.selected,
                )
                temp_mean = np.nanmean(temp.data)
                sal = getattr(
                    self.verticals[idxv].data.sensors.salinity_ppt,
                    self.verticals[idxv].data.sensors.salinity_ppt.selected,
                )
                sal_mean = np.nanmean(sal.data)
                sos_source = self.verticals[
                    idxv
                ].data.sensors.speed_of_sound_mps.selected
                sos = getattr(
                    self.verticals[idxv].data.sensors.speed_of_sound_mps,
                    self.verticals[idxv].data.sensors.speed_of_sound_mps.selected,
                )
                if sos_source == "internal":
                    if sos.source == "Calculated":
                        sos_source = "Internal"
                    else:
                        sos_source = "Computed"
                elif sos_source == "user":
                    sos_source = "User"
                sos_mean = np.nanmean(sos.data)
            else:
                pitch_mean = np.nan
                pitch_stddev = np.nan
                roll_mean = np.nan
                roll_stddev = np.nan
                magvar = np.nan
                heading_source = "None"
                temperature_source = ""
                temp_mean = np.nan
                sal_mean = np.nan
                sos_source = ""
                sos_mean = np.nan

            # Compute % invalid
            n_ensembles = self.verticals[idxv].number_of_ensembles
            if n_ensembles > 0:
                # WT
                valid_data = self.verticals[idxv].data.w_vel.valid_data
                useable_cells = self.verticals[idxv].data.w_vel.valid_data[6, :, :]
                num_useable_cells = np.nansum(np.nansum(useable_cells.astype(int)))
                num_invalid = np.nansum(
                    np.nansum(np.logical_not(valid_data[0, :, :][useable_cells]))
                )
                wt_per_invalid = (num_invalid / num_useable_cells) * 100.0

                # BT
                bt_valid = np.nansum(
                    self.verticals[idxv].data.boat_vel.bt_vel.valid_data[0, :]
                )
                bt_per_invalid = ((n_ensembles - bt_valid) / n_ensembles) * 100.0

                # Depth
                depth_selected = getattr(
                    self.verticals[idxv].data.depths,
                    self.verticals[idxv].data.depths.selected,
                )
                num_int = np.where(depth_selected.depth_source_ens == "IN")[0].shape[0]
                num_na = np.where(depth_selected.depth_source_ens == "NA")[0].shape[0]
                depth_per_invalid = ((num_int + num_na) / n_ensembles) * 100.0
            else:
                bt_per_invalid = np.nan
                depth_per_invalid = np.nan
                wt_per_invalid = np.nan

            # Get speed in top cell
            try:
                top_cell_speed = self.verticals[idxv].mean_profile["speed"][1]
            except IndexError:
                top_cell_speed = ""

            # Ice
            if np.isnan(self.verticals[idxv].ice_thickness_m):
                ice_thickness = 0
            else:
                ice_thickness = self.verticals[idxv].ice_thickness_m

            if np.isnan(self.verticals[idxv].ws_to_ice_bottom_m):
                ws_to_ice_bottom = 0
            else:
                ws_to_ice_bottom = self.verticals[idxv].ws_to_ice_bottom_m

            if np.isnan(self.verticals[idxv].ws_to_slush_bottom_m):
                ws_to_slush_bottom = 0
            else:
                ws_to_slush_bottom = self.verticals[idxv].ws_to_slush_bottom_m

            # Populate data frame
            self.summary = self.summary.append(
                {
                    "Station Number": number,
                    "Location": self.verticals[idxv].location_m,
                    "Condition": self.verticals[idxv].ws_condition,
                    "Depth": self.discharge["depth_m"][idxq],
                    "Depth StdDev": depth_stddev,
                    "Width": self.discharge["width_m"][idxq],
                    "Area": self.discharge["area_sqm"][idxq],
                    "Number Cells": self.verticals[idxv].number_valid_cells,
                    "Duration": self.verticals[idxv].sampling_duration_sec,
                    "Velocity Magnitude": self.verticals[
                        idxv
                    ].mean_velocity_magnitude_mps,
                    "Velocity Magnitude CV": self.verticals[
                        idxv
                    ].mean_velocity_magnitude_cv,
                    "Velocity Direction": self.verticals[
                        idxv
                    ].mean_velocity_direction_deg,
                    "Velocity Direction StdDev": self.verticals[
                        idxv
                    ].mean_velocity_direction_stdev_deg,
                    "Velocity Angle": self.verticals[idxv].mean_velocity_angle_deg,
                    "Correction Factor": self.verticals[idxv].velocity_correction,
                    "Velocity Method": self.verticals[idxv].velocity_method,
                    "Measured Normal Velocity": self.verticals[
                        idxv
                    ].mean_measured_speed_mps,
                    "Measured Normal Velocity CV": self.verticals[
                        idxv
                    ].mean_measured_speed_cv,
                    "Normal Velocity": self.verticals[idxv].mean_speed_mps,
                    "Q Top": self.discharge["q_top_cms"][idxq],
                    "Q Middle": self.discharge["q_middle_cms"][idxq],
                    "Q Bottom": self.discharge["q_bottom_cms"][idxq],
                    "Q": self.discharge["q_cms"][idxq],
                    "Q%": self.discharge["q_%"][idxq],
                    "Start Time": self.verticals[idxv].time_start,
                    "End Time": self.verticals[idxv].time_end,
                    "Stage": self.verticals[idxv].gh_m,
                    "Stage Time": self.verticals[idxv].gh_obs_time,
                    "Number Profiles": self.verticals[idxv].number_of_ensembles,
                    "Number Valid Profiles": self.verticals[
                        idxv
                    ].number_of_valid_ensembles,
                    "Ice Thickness": ice_thickness,
                    "Depth to Ice Bottom": ws_to_ice_bottom,
                    "Ice Area": self.discharge["ice_area_sqm"][idxq],
                    "Depth to Slush Bottom": ws_to_slush_bottom,
                    "Slush Area": self.discharge["slush_area_sqm"][idxq],
                    "ADCP Depth Below Ice": self.verticals[idxv].adcp_depth_below_ice_m,
                    "Average Voltage": self.verticals[idxv].battery_voltage,
                    "BT % Invalid": bt_per_invalid,
                    "Depth % Invalid": depth_per_invalid,
                    "WT % Invalid": wt_per_invalid,
                    "Velocity Reference": self.verticals[idxv].velocity_reference,
                    "Flow Angle Coefficient": self.verticals[
                        idxv
                    ].flow_angle_coefficient,
                    "ADCP Depth": self.verticals[idxv].adcp_depth_below_ws_m,
                    "Depth Source": self.verticals[idxv].depth_source,
                    "Excluded Top": excluded_top,
                    "Excluded Top Type": excluded_top_type,
                    "Excluded Bottom": excluded_bottom,
                    "Excluded Bottom Type": excluded_bottom_type,
                    "Fit Method": self.verticals[
                        idxv
                    ].extrapolation.extrap_fit.fit_method,
                    "Top Method": self.verticals[idxv].extrapolation.top_method,
                    "Ice Exponent": self.verticals[idxv].extrapolation.ice_exponent,
                    "Bottom Method": self.verticals[idxv].extrapolation.bot_method,
                    "Bottom Exponent": self.verticals[idxv].extrapolation.exponent,
                    "MagVar": magvar,
                    "Beam Misalignment": self.verticals[idxv].beam_3_misalignment_deg
                    + self.verticals[idxv].beam_3_rotation_deg,
                    "Heading Source": heading_source,
                    "Mean Pitch": pitch_mean,
                    "Pitch StdDev": pitch_stddev,
                    "Mean Roll": roll_mean,
                    "Roll StdDev": roll_stddev,
                    "Temperature Source": temperature_source,
                    "Mean Temperature": temp_mean,
                    "Salinity": sal_mean,
                    "SOS Source": sos_source,
                    "SOS": sos_mean,
                    "Top Cell Speed": top_cell_speed,
                    "Location Source": self.verticals[idxv].location_source,
                },
                ignore_index=True,
            )

        # Compute totals and means
        self.summary.loc["Total"] = self.summary[
            [
                "Width",
                "Area",
                "Ice Area",
                "Slush Area",
                "Duration",
                "Q Top",
                "Q Middle",
                "Q Bottom",
                "Q",
            ]
        ].sum(axis=0)
        self.summary.loc["Mean"] = self.summary[
            [
                "Velocity Magnitude CV",
                "Measured Normal Velocity CV",
                "Depth StdDev",
                "BT % Invalid",
                "Depth % Invalid",
                "WT % Invalid",
                "Average Voltage",
            ]
        ].mean(axis=0)

        # Mean depth is to water surface. Area is flow area. Ice and slush areas are
        # added to get a total area below water surface
        mean_depth = (
            self.summary.loc["Total"]["Area"]
            + self.summary.loc["Total"]["Ice Area"]
            + self.summary.loc["Total"]["Slush Area"]
        ) / self.summary.loc["Total"]["Width"]
        mean_speed = self.summary.loc["Total"]["Q"] / self.summary.loc["Total"]["Area"]
        self.summary.at["Mean", "Depth"] = mean_depth
        self.summary.at["Mean", "Normal Velocity"] = mean_speed

    def process_mean_section(self):
        """Computes discharge using the mean section technique with edge discharge
        computations as documented in the SxS Pro User Guide, p. 119
        """

        self.discharge = {
            "location_m": np.array([]),
            "depth_m": np.array([]),
            "width_m": np.array([]),
            "area_sqm": np.array([]),
            "ice_area_sqm": np.array([]),
            "slush_area_sqm": np.array([]),
            "ws_to_slush_bottom": np.array([]),
            "speed_mps": np.array([]),
            "q_top_cms": np.array([]),
            "q_middle_cms": np.array([]),
            "q_bottom_cms": np.array([]),
            "q_cms": np.array([]),
            "q_%": np.array([]),
        }

        # Compute discharge for measured verticals
        # ----------------------------------------
        for n, idx in enumerate(self.verticals_used_idx[1:-2]):
            # Compute manual speed when using velocity correction
            if (
                self.verticals[idx].mean_speed_source == "Manual"
                and np.round(self.verticals[idx].velocity_correction, 2)
            ) != 0:
                self.compute_manual_velocity(idx)

            # Compute discharge variables
            width_m = np.abs(
                self.verticals[self.verticals_used_idx[n + 2]].location_m
                - self.verticals[self.verticals_used_idx[n + 1]].location_m
            )
            idx1 = self.verticals_used_idx[n + 2]
            depth_1 = (
                self.verticals[idx1].depth_m - self.verticals[idx1].ws_to_slush_bottom_m
            )
            idx2 = self.verticals_used_idx[n + 1]
            depth_2 = (
                self.verticals[idx2].depth_m - self.verticals[idx2].ws_to_slush_bottom_m
            )
            depth_m = (depth_1 + depth_2) / 2

            ice_1 = self.verticals[idx1].ws_to_ice_bottom_m
            ice_2 = self.verticals[idx2].ws_to_ice_bottom_m
            ice_m = (ice_1 + ice_2) / 2

            slush_1 = self.verticals[idx1].ws_to_slush_bottom_m
            slush_2 = self.verticals[idx2].ws_to_slush_bottom_m
            slush_m = (slush_1 + slush_2) / 2
            if np.isnan(slush_m):
                slush_m = 0

            speed_mps = (
                self.verticals[self.verticals_used_idx[n + 2]].mean_speed_mps
                + self.verticals[self.verticals_used_idx[n + 1]].mean_speed_mps
            ) / 2
            location_m = (
                self.verticals[self.verticals_used_idx[n + 2]].location_m
                + self.verticals[self.verticals_used_idx[n + 1]].location_m
            ) / 2
            area_sqm = width_m * depth_m
            ice_area_sqm = width_m * ice_m
            slush_area_sqm = (width_m * slush_m) - ice_area_sqm

            # Compute discharge
            q_cms = width_m * depth_m * speed_mps
            q_top, q_meas, q_bot = self.compute_q_tmb(width_m, idx1=n + 1, idx2=n + 2)

            # Store results
            self.discharge["location_m"] = np.hstack(
                [self.discharge["location_m"], location_m]
            )
            self.discharge["depth_m"] = np.hstack([self.discharge["depth_m"], depth_m])
            self.discharge["width_m"] = np.hstack([self.discharge["width_m"], width_m])
            self.discharge["area_sqm"] = np.hstack(
                [self.discharge["area_sqm"], area_sqm]
            )
            self.discharge["ice_area_sqm"] = np.hstack(
                [self.discharge["ice_area_sqm"], ice_area_sqm]
            )
            self.discharge["slush_area_sqm"] = np.hstack(
                [self.discharge["slush_area_sqm"], slush_area_sqm]
            )
            self.discharge["ws_to_slush_bottom"] = np.hstack(
                [self.discharge["ws_to_slush_bottom"], slush_m]
            )
            self.discharge["speed_mps"] = np.hstack(
                [self.discharge["speed_mps"], speed_mps]
            )
            self.discharge["q_cms"] = np.hstack([self.discharge["q_cms"], q_cms])
            self.discharge["q_top_cms"] = np.hstack(
                [self.discharge["q_top_cms"], q_top]
            )
            self.discharge["q_middle_cms"] = np.hstack(
                [self.discharge["q_middle_cms"], q_meas]
            )
            self.discharge["q_bottom_cms"] = np.hstack(
                [self.discharge["q_bottom_cms"], q_bot]
            )

        # Compute discharge for start bank section
        # ----------------------------------------
        width_m = np.abs(
            self.verticals[self.verticals_used_idx[1]].location_m
            - self.verticals[self.verticals_used_idx[0]].location_m
        )

        idx1 = self.verticals_used_idx[1]
        depth_1 = (
            self.verticals[idx1].depth_m - self.verticals[idx1].ws_to_slush_bottom_m
        )
        idx2 = self.verticals_used_idx[0]
        depth_2 = (
            self.verticals[idx2].depth_m - self.verticals[idx2].ws_to_slush_bottom_m
        )
        depth_m = (depth_1 + depth_2) / 2

        ice_1 = self.verticals[idx1].ws_to_ice_bottom_m
        ice_2 = self.verticals[idx2].ws_to_ice_bottom_m
        ice_m = (ice_1 + ice_2) / 2

        slush_1 = self.verticals[idx1].ws_to_slush_bottom_m
        slush_2 = self.verticals[idx2].ws_to_slush_bottom_m
        slush_m = (slush_1 + slush_2) / 2
        if np.isnan(slush_m):
            slush_m = 0

        location_m = (
            self.verticals[self.verticals_used_idx[1]].location_m
            + self.verticals[self.verticals_used_idx[0]].location_m
        ) / 2
        area_sqm = width_m * depth_m
        ice_area_sqm = width_m * ice_m
        slush_area_sqm = width_m * slush_m

        if self.verticals[self.verticals_used_idx[0]].is_edge:
            # Compute speed for start edge based on SxS Pro approach
            speed_mps = (
                self.verticals[self.verticals_used_idx[1]].mean_speed_mps
                * self.verticals[self.verticals_used_idx[0]].edge_coef
            )
            self.verticals[0].mean_speed_mps = 0
        else:
            # If the first vertical is not an edge use the measured speed
            speed_mps = self.verticals[0].mean_speed_mps

        # Compute discharge
        q_cms = width_m * depth_m * speed_mps

        q_top, q_meas, q_bot = self.compute_q_tmb(width_m, idx1=0, idx2=1)

        # Store results
        self.discharge["location_m"] = np.hstack(
            [location_m, self.discharge["location_m"]]
        )
        self.discharge["depth_m"] = np.hstack([depth_m, self.discharge["depth_m"]])
        self.discharge["width_m"] = np.hstack([width_m, self.discharge["width_m"]])
        self.discharge["area_sqm"] = np.hstack([area_sqm, self.discharge["area_sqm"]])
        self.discharge["ice_area_sqm"] = np.hstack(
            [ice_area_sqm, self.discharge["ice_area_sqm"]]
        )
        self.discharge["ws_to_slush_bottom"] = np.hstack(
            [slush_m, self.discharge["ws_to_slush_bottom"]]
        )
        self.discharge["slush_area_sqm"] = np.hstack(
            [slush_area_sqm, self.discharge["slush_area_sqm"]]
        )
        self.discharge["ws_to_slush_bottom"] = np.hstack(
            [slush_m, self.discharge["ws_to_slush_bottom"]]
        )
        self.discharge["speed_mps"] = np.hstack(
            [speed_mps, self.discharge["speed_mps"]]
        )
        self.discharge["q_cms"] = np.hstack([q_cms, self.discharge["q_cms"]])
        self.discharge["q_top_cms"] = np.hstack([q_top, self.discharge["q_top_cms"]])
        self.discharge["q_middle_cms"] = np.hstack(
            [q_meas, self.discharge["q_middle_cms"]]
        )
        self.discharge["q_bottom_cms"] = np.hstack(
            [q_bot, self.discharge["q_bottom_cms"]]
        )

        # Compute discharge for end bank section
        # --------------------------------------
        width_m = np.abs(
            self.verticals[self.verticals_used_idx[-1]].location_m
            - self.verticals[self.verticals_used_idx[-2]].location_m
        )
        idx1 = self.verticals_used_idx[-2]
        depth_1 = (
            self.verticals[idx1].depth_m - self.verticals[idx1].ws_to_slush_bottom_m
        )
        idx2 = self.verticals_used_idx[-1]
        depth_2 = (
            self.verticals[idx2].depth_m - self.verticals[idx2].ws_to_slush_bottom_m
        )
        depth_m = (depth_1 + depth_2) / 2

        ice_1 = self.verticals[idx1].ws_to_ice_bottom_m
        ice_2 = self.verticals[idx2].ws_to_ice_bottom_m
        ice_m = (ice_1 + ice_2) / 2

        slush_1 = self.verticals[idx1].ws_to_slush_bottom_m
        slush_2 = self.verticals[idx2].ws_to_slush_bottom_m
        slush_m = (slush_1 + slush_2) / 2
        if np.isnan(slush_m):
            slush_m = 0

        location_m = (
            self.verticals[self.verticals_used_idx[-2]].location_m
            + self.verticals[self.verticals_used_idx[-1]].location_m
        ) / 2
        area_sqm = width_m * depth_m
        ice_area_sqm = width_m * ice_m
        slush_area_sqm = width_m * slush_m

        if self.verticals[-1].is_edge:
            # Compute speed for start edge based on SxS Pro approach
            speed_mps = (
                self.verticals[self.verticals_used_idx[-2]].mean_speed_mps
                * self.verticals[self.verticals_used_idx[-1]].edge_coef
            )
            self.verticals[-1].mean_speed_mps = 0
        else:
            speed_mps = self.verticals[-1].mean_speed_mps

        # Compute discharge
        q_cms = width_m * depth_m * speed_mps

        q_top, q_meas, q_bot = self.compute_q_tmb(width_m, idx1=-1, idx2=-2)

        # Store results
        self.discharge["location_m"] = np.hstack(
            [self.discharge["location_m"], location_m]
        )
        self.discharge["depth_m"] = np.hstack([self.discharge["depth_m"], depth_m])
        self.discharge["width_m"] = np.hstack([self.discharge["width_m"], width_m])
        self.discharge["area_sqm"] = np.hstack([self.discharge["area_sqm"], area_sqm])
        self.discharge["ice_area_sqm"] = np.hstack(
            [self.discharge["ice_area_sqm"], ice_area_sqm]
        )
        self.discharge["slush_area_sqm"] = np.hstack(
            [self.discharge["slush_area_sqm"], slush_area_sqm]
        )
        self.discharge["ws_to_slush_bottom"] = np.hstack(
            [self.discharge["ws_to_slush_bottom"], slush_m]
        )
        self.discharge["speed_mps"] = np.hstack(
            [self.discharge["speed_mps"], speed_mps]
        )
        self.discharge["q_cms"] = np.hstack([self.discharge["q_cms"], q_cms])
        self.discharge["q_top_cms"] = np.hstack([self.discharge["q_top_cms"], q_top])
        self.discharge["q_middle_cms"] = np.hstack(
            [self.discharge["q_middle_cms"], q_meas]
        )
        self.discharge["q_bottom_cms"] = np.hstack(
            [self.discharge["q_bottom_cms"], q_bot]
        )

        # Compute total discharge
        q_total = np.nansum(self.discharge["q_cms"])

        # Compute percentage in each vertical
        for q in self.discharge["q_cms"]:
            self.discharge["q_%"] = np.hstack(
                [self.discharge["q_%"], (q / q_total) * 100]
            )

    def compute_q_tmb(self, width_m, idx1, idx2):
        """Computes the discharge in the top, middle, and bottom portions of the section
        for the mean section method. The portions of the discharge are computed by
        decomposing the total discharge equation for each section into top
        middle and bottom. Additional code is provided for the edge computations,
        using a similar approach but recognizing that there is only one speed available,
        i.e. there is no speed at the edge.

        Parameters
        ----------
        width_m: float
            Width of the section
        idx1: int
            Index of one of the boundary verticals
        idx2: int
            Index of the other boundary vertical

        Returns
        -------
        top_q: float
            Discharge in top extrapolated portion of section
        mid_q: float
            Discharge in middle measured portion of section
        bot_q: float
            Discharge in bottom extrapolated portion of the section
        """

        # Get data for each index.
        (
            top_speed_1,
            top_size_1,
            mid_speed_1,
            mid_size_1,
            bot_speed_1,
            bot_size_1,
            depth_1,
        ) = self.get_tmb_data(idx1)
        (
            top_speed_2,
            top_size_2,
            mid_speed_2,
            mid_size_2,
            bot_speed_2,
            bot_size_2,
            depth_2,
        ) = self.get_tmb_data(idx2)

        # Edges will not have a speed for one of the verticals, use the speed from adjacent vertical
        if np.isnan(top_speed_1) or depth_1 == 0:
            # This indicates that idx1 is the edge vertical

            # Compute mean depth
            depth = (depth_1 + depth_2) / 2

            # Edge coefficient used to compute discharge in edge
            coef = self.verticals[self.verticals_used_idx[idx1]].edge_coef

            # The size of the top, middle, and bottom sections for the edge are computed as a ratio of the edge depth
            # and the depth of the adjacent vertical with valid speed.
            top_q = top_speed_2 * top_size_2 * (depth / depth_2) * width_m * coef
            mid_q = mid_speed_2 * mid_size_2 * (depth / depth_2) * width_m * coef
            bot_q = bot_speed_2 * bot_size_2 * (depth / depth_2) * width_m * coef

        elif np.isnan(top_speed_2) or depth_2 == 0:
            # This indicates that idx2 is the edge vertical

            # Compute mean depth
            depth = (depth_1 + depth_2) / 2

            # Edge coefficient used to compute discharge in edge
            coef = self.verticals[self.verticals_used_idx[idx2]].edge_coef

            # The size of the top, middle, and bottom sections for the edge are computed as a ratio of the edge depth
            # and the depth of the adjacent vertical with valid speed.
            top_q = top_speed_1 * top_size_1 * (depth / depth_1) * width_m * coef
            mid_q = mid_speed_1 * mid_size_1 * (depth / depth_1) * width_m * coef
            bot_q = bot_speed_1 * bot_size_1 * (depth / depth_1) * width_m * coef

        else:
            # Compute the discharge for top, middle,and bottom
            top_q = (
                (
                    (top_speed_1 * top_size_1 * (1 + depth_2 / depth_1))
                    + (top_speed_2 * top_size_2 * (1 + depth_1 / depth_2))
                )
                / 4
            ) * width_m

            mid_q = (
                (
                    (mid_speed_1 * mid_size_1 * (1 + depth_2 / depth_1))
                    + (mid_speed_2 * mid_size_2 * (1 + depth_1 / depth_2))
                )
                / 4
            ) * width_m

            bot_q = (
                (
                    (bot_speed_1 * bot_size_1 * (1 + depth_2 / depth_1))
                    + (bot_speed_2 * bot_size_2 * (1 + depth_1 / depth_2))
                )
                / 4
            ) * width_m

        return top_q, mid_q, bot_q

    def get_tmb_data(self, idx):
        """Retrieves the data needed to compute the top, middle, and bottom discharges for
        the edge sections. If the speed data is not available, such as for the edge
        vertical, all variables but the depth are set to nan.

        Parameters
        ----------
        idx: int
            Index of vertical from which to retrieve data.

        Returns
        -------
        top_speed: float
            Speed in top extrapolated portion of the vertical
        top_size: float
            Size of top extrapolation portion of the vertical
        mid_speed: float
            Speed in the middle measured portion of the vertical
        mid_size: float
            Size of the middle measured portion of the vertical
        bot_speed: float
            Speed of the bottom extrapolated portion of the vertical
        bot_size: float
            Size of the bottom extrapolated portion of the vertical
        depth: float
            Depth of the vertical
        """

        # Get depth
        depth = (
            self.verticals[self.verticals_used_idx[idx]].depth_m
            - self.verticals[self.verticals_used_idx[idx]].ws_to_slush_bottom_m
        )

        # Attempt to get other variables. If they are not available set to nan
        try:
            mask = np.logical_not(
                np.isnan(
                    self.verticals[self.verticals_used_idx[idx]].mean_profile["speed"]
                )
            )
            bot_speed = self.verticals[self.verticals_used_idx[idx]].mean_profile[
                "speed"
            ][mask][-1]
            bot_size = self.verticals[self.verticals_used_idx[idx]].mean_profile[
                "cell_size"
            ][mask][-1]
            top_speed = self.verticals[self.verticals_used_idx[idx]].mean_profile[
                "speed"
            ][mask][0]
            top_size = self.verticals[self.verticals_used_idx[idx]].mean_profile[
                "cell_size"
            ][mask][0]
            mid_speed = self.verticals[
                self.verticals_used_idx[idx]
            ].mean_measured_speed_mps
            # For manual verticals
            if np.isnan(mid_speed):
                mid_speed = self.verticals[self.verticals_used_idx[idx]].mean_speed_mps
            mid_size = depth - top_size - bot_size

        except IndexError:
            top_speed = np.nan
            top_size = np.nan
            mid_speed = np.nan
            mid_size = np.nan
            bot_speed = np.nan
            bot_size = np.nan

        return top_speed, top_size, mid_speed, mid_size, bot_speed, bot_size, depth

    def mean_section_summary(self):
        """Populates a data frame with vertical data and variables and discharges computed
        from the mean-section method.
        """

        # Initialize variables
        self.summary = self.summary[0:0]
        number = 0
        n_discharge_sections = self.discharge["q_cms"].shape[0]

        # Populate summary
        for idx in self.verticals_used_idx:
            number = number + 1

            # Excluded data
            if self.verticals[idx].data.w_vel is not None:
                excluded_top = self.verticals[idx].data.w_vel.excluded_top
                excluded_top_type = self.verticals[idx].data.w_vel.excluded_top_type
                excluded_bottom = self.verticals[idx].data.w_vel.excluded_bottom
                excluded_bottom_type = self.verticals[
                    idx
                ].data.w_vel.excluded_bottom_type
            else:
                excluded_top = np.nan
                excluded_top_type = ""
                excluded_bottom = np.nan
                excluded_bottom_type = ""

            # Compute depth std dev
            if self.verticals[idx].data.depths is not None:
                depths = getattr(
                    self.verticals[idx].data.depths,
                    self.verticals[idx].data.depths.selected,
                )
                depth_stddev = np.nanstd(depths.depth_processed_m, ddof=1)
            else:
                depth_stddev = np.nan

            # Compute magvar and pitch and roll stats
            if self.verticals[idx].data.sensors is not None:
                pitch = getattr(
                    self.verticals[idx].data.sensors.pitch_deg,
                    self.verticals[idx].data.sensors.pitch_deg.selected,
                )
                pitch_mean = np.nanmean(pitch.data)
                pitch_stddev = np.nanstd(pitch.data)
                roll = getattr(
                    self.verticals[idx].data.sensors.roll_deg,
                    self.verticals[idx].data.sensors.roll_deg.selected,
                )
                roll_mean = np.nanmean(roll.data)
                roll_stddev = np.nanstd(roll.data)
                magvar = self.verticals[
                    idx
                ].data.sensors.heading_deg.internal.mag_var_deg
                heading_source = self.verticals[idx].data.sensors.heading_deg.selected
                if heading_source.lower() == "internal":
                    heading_source = "Internal"
                elif heading_source.lower() == "user":
                    heading_source = "None"
                temperature_source = self.verticals[
                    idx
                ].data.sensors.temperature_deg_c.selected
                temp = getattr(
                    self.verticals[idx].data.sensors.temperature_deg_c,
                    self.verticals[idx].data.sensors.temperature_deg_c.selected,
                )
                temp_mean = np.nanmean(temp.data)
                sal = getattr(
                    self.verticals[idx].data.sensors.salinity_ppt,
                    self.verticals[idx].data.sensors.salinity_ppt.selected,
                )
                sal_mean = np.nanmean(sal.data)
                sos_source = self.verticals[
                    idx
                ].data.sensors.speed_of_sound_mps.selected
                sos = getattr(
                    self.verticals[idx].data.sensors.speed_of_sound_mps,
                    self.verticals[idx].data.sensors.speed_of_sound_mps.selected,
                )
                sos_mean = np.nanmean(sos.data)
            else:
                pitch_mean = np.nan
                pitch_stddev = np.nan
                roll_mean = np.nan
                roll_stddev = np.nan
                magvar = np.nan
                heading_source = "None"
                temperature_source = ""
                temp_mean = np.nan
                sal_mean = np.nan
                sos_source = ""
                sos_mean = np.nan

            n_ensembles = self.verticals[idx].number_of_ensembles
            # Compute % invalid
            if n_ensembles > 0:
                wt_valid = self.verticals[idx].number_of_valid_ensembles
                wt_per_invalid = ((n_ensembles - wt_valid) / n_ensembles) * 100.0
                bt_valid = np.nansum(
                    self.verticals[idx].data.boat_vel.bt_vel.valid_data[0, :]
                )
                bt_per_invalid = ((n_ensembles - bt_valid) / n_ensembles) * 100.0
                depth_selected = getattr(
                    self.verticals[idx].data.depths,
                    self.verticals[idx].data.depths.selected,
                )
                num_int = np.where(depth_selected.depth_source_ens == "IN")[0].shape[0]
                num_na = np.where(depth_selected.depth_source_ens == "NA")[0].shape[0]
                depth_per_invalid = ((num_int + num_na) / n_ensembles) * 100.0
            else:
                bt_per_invalid = np.nan
                depth_per_invalid = np.nan
                wt_per_invalid = np.nan

            # Get top cell speed
            try:
                top_cell_speed = self.verticals[idx].mean_profile["speed"][1]
            except IndexError:
                top_cell_speed = ""

            # Ice
            if np.isnan(self.verticals[idx].ice_thickness_m):
                ice_thickness = 0
            else:
                ice_thickness = self.verticals[idx].ice_thickness_m

            if np.isnan(self.verticals[idx].ws_to_ice_bottom_m):
                ws_to_ice_bottom = 0
            else:
                ws_to_ice_bottom = self.verticals[idx].ws_to_ice_bottom_m

            if np.isnan(self.verticals[idx].ws_to_slush_bottom_m):
                ws_to_slush_bottom = 0
            else:
                ws_to_slush_bottom = self.verticals[idx].ws_to_slush_bottom_m

            self.summary = self.summary.append(
                {
                    "Station Number": number,
                    "Location": self.verticals[idx].location_m,
                    "Condition": self.verticals[idx].ws_condition,
                    "Depth": self.verticals[idx].depth_m,
                    "Depth StdDev": depth_stddev,
                    "Number Cells": self.verticals[idx].number_valid_cells,
                    "Duration": self.verticals[idx].sampling_duration_sec,
                    "Velocity Magnitude": self.verticals[
                        idx
                    ].mean_velocity_magnitude_mps,
                    "Velocity Direction": self.verticals[
                        idx
                    ].mean_velocity_direction_deg,
                    "Velocity Direction StdDev": self.verticals[
                        idx
                    ].mean_velocity_direction_stdev_deg,
                    "Velocity Magnitude CV": self.verticals[
                        idx
                    ].mean_velocity_magnitude_cv,
                    "Velocity Angle": self.verticals[idx].mean_velocity_angle_deg,
                    "Edge Coefficient": self.verticals[idx].edge_coef,
                    "Velocity Method": self.verticals[idx].velocity_method,
                    "Measured Normal Velocity": self.verticals[
                        idx
                    ].mean_measured_speed_mps,
                    "Measured Normal Velocity CV": self.verticals[
                        idx
                    ].mean_measured_speed_cv,
                    "Normal Velocity": self.verticals[idx].mean_speed_mps,
                    "Start Time": self.verticals[idx].time_start,
                    "End Time": self.verticals[idx].time_end,
                    "Stage": self.verticals[idx].gh_m,
                    "Stage Time": self.verticals[idx].gh_obs_time,
                    "Number Profiles": self.verticals[idx].number_of_ensembles,
                    "Number Valid Profiles": self.verticals[
                        idx
                    ].number_of_valid_ensembles,
                    "Ice Thickness": ice_thickness,
                    "Depth to Ice Bottom": ws_to_ice_bottom,
                    "Depth to Slush Bottom": ws_to_slush_bottom,
                    "ADCP Depth Below Ice": self.verticals[idx].adcp_depth_below_ice_m,
                    "Average Voltage": self.verticals[idx].battery_voltage,
                    "BT % Invalid": bt_per_invalid,
                    "Depth % Invalid": depth_per_invalid,
                    "WT % Invalid": wt_per_invalid,
                    "Velocity Reference": self.verticals[idx].velocity_reference,
                    "Flow Angle Coefficient": self.verticals[
                        idx
                    ].flow_angle_coefficient,
                    "ADCP Depth": self.verticals[idx].adcp_depth_below_ws_m,
                    "Depth Source": self.verticals[idx].depth_source,
                    "Excluded Top": excluded_top,
                    "Excluded Top Type": excluded_top_type,
                    "Excluded Bottom": excluded_bottom,
                    "Excluded Bottom Type": excluded_bottom_type,
                    "Fit Method": self.verticals[
                        idx
                    ].extrapolation.extrap_fit.fit_method,
                    "Top Method": self.verticals[idx].extrapolation.top_method,
                    "Ice Exponent": self.verticals[idx].extrapolation.ice_exponent,
                    "Bottom Method": self.verticals[idx].extrapolation.bot_method,
                    "Bottom Exponent": self.verticals[idx].extrapolation.exponent,
                    "MagVar": magvar,
                    "Beam Misalignment": self.verticals[idx].beam_3_misalignment_deg
                    + self.verticals[idx].beam_3_rotation_deg,
                    "Heading Source": heading_source,
                    "Mean Pitch": pitch_mean,
                    "Pitch StdDev": pitch_stddev,
                    "Mean Roll": roll_mean,
                    "Roll StdDev": roll_stddev,
                    "Temperature Source": temperature_source,
                    "Mean Temperature": temp_mean,
                    "Salinity": sal_mean,
                    "SOS Source": sos_source,
                    "SOS": sos_mean,
                    "Top Cell Speed": top_cell_speed,
                    "Location Source": self.verticals[idx].location_source,
                },
                ignore_index=True,
            )

            if number - 1 < n_discharge_sections:
                self.summary = self.summary.append(
                    {
                        "Station Number": number + 0.5,
                        "Location": self.discharge["location_m"][number - 1],
                        "Depth": self.discharge["depth_m"][number - 1],
                        "Width": self.discharge["width_m"][number - 1],
                        "Area": self.discharge["area_sqm"][number - 1],
                        "Ice Area": self.discharge["ice_area_sqm"][number - 1],
                        "Slush Area": self.discharge["slush_area_sqm"][number - 1],
                        "Depth to Slush Bottom": self.discharge["ws_to_slush_bottom"][
                            number - 1
                        ],
                        "Normal Velocity": self.discharge["speed_mps"][number - 1],
                        "Q Top": self.discharge["q_top_cms"][number - 1],
                        "Q Middle": self.discharge["q_middle_cms"][number - 1],
                        "Q Bottom": self.discharge["q_bottom_cms"][number - 1],
                        "Q": self.discharge["q_cms"][number - 1],
                        "Q%": self.discharge["q_%"][number - 1],
                        "Start Time": "",
                        "End Time": "",
                        "Condition": "",
                        "Depth Source": "",
                        "Top Method": "",
                        "Bottom Method": "",
                        "Temperature Source": "",
                        "Heading Source": "",
                        "SOS Source": "",
                        "Velocity Method": "",
                    },
                    ignore_index=True,
                )

        # Compute totals and means
        self.summary.loc["Total"] = self.summary[
            [
                "Width",
                "Area",
                "Ice Area",
                "Slush Area",
                "Duration",
                "Q Top",
                "Q Middle",
                "Q Bottom",
                "Q",
            ]
        ].sum(axis=0)
        self.summary.loc["Mean"] = self.summary[
            [
                "Velocity Magnitude CV",
                "Measured Normal Velocity CV",
                "Depth StdDev",
                "BT % Invalid",
                "Depth % Invalid",
                "WT % Invalid",
                "Average Voltage",
            ]
        ].mean(axis=0)

        # Mean depth is to water surface. Area is flow area. Ice and slush areas are
        # added to get a total area below water surface
        mean_depth = (
            self.summary.loc["Total"]["Area"]
            + self.summary.loc["Total"]["Ice Area"]
            + self.summary.loc["Total"]["Slush Area"]
        ) / self.summary.loc["Total"]["Width"]
        mean_speed = self.summary.loc["Total"]["Q"] / self.summary.loc["Total"]["Area"]
        self.summary.at["Mean", "Depth"] = mean_depth
        self.summary.at["Mean", "Normal Velocity"] = mean_speed

    def compute_manual_velocity(self, idx):
        """Computes the manaul velocity when an edge coefficient or a velocity
        correction factor is include instead of a manually entered velocity.

        Parameters
        ----------
        idx: int
            Index of current vertical
        """

        # Indices of before, after, and target
        idx_before = self.verticals_used_idx[idx - 1]
        idx_after = self.verticals_used_idx[idx + 1]
        idx = self.verticals_used_idx[idx]

        # Determine correction factor
        correction_factor = self.verticals[idx].velocity_correction

        if self.verticals[idx_before].mean_speed_source != "Manual":
            # Compute mean speed using previous measured vertical
            self.verticals[idx].mean_speed_mps = (
                self.verticals[idx_before].mean_speed_mps * correction_factor
            )

        elif self.verticals[idx_after].mean_speed_source != "Manual":
            # Compute mean speed using next measured vertical
            self.verticals[idx].mean_speed_mps = (
                self.verticals[idx_after].mean_speed_mps * correction_factor
            )

        else:
            # No measured adjacent vertical, set speed to 0
            self.verticals[idx].mean_speed_mps = 0

        self.verticals[idx].mean_profile["speed"] = np.array(
            [self.verticals[idx].mean_speed_mps]
        )

    def change_azimuth(self, az):
        """Coordinate changing the azimuth provided by the user for the azimuth method.

        Parameters
        ----------
        az: float
            New azimuth in degrees
        """

        # Store tagline azimuth in Measurement
        self.tagline_azimuth_deg = az
        for vertical in self.verticals:
            # Store tagline_azimuth in each vertical
            vertical.tagline_azimuth_deg = az
            # Recompute profile
            vertical.compute_profile()

        self.compute_discharge()

    def change_beam_3_misalignment(self, beam_misalignment):
        """Coordinate changing the beam 3 misalignment provided by the user for the fixed
        method.

        Parameters
        ----------
        beam_misalignment: float
            New beam 3 misalignment in degrees
        """

        # Store beam 3 misalignment in Measurement
        self.beam_3_misalignment_deg = beam_misalignment
        for vertical in self.verticals:
            # Store general beam 3 misalignment in each vertical
            vertical.beam_3_misalignment_deg = beam_misalignment
            # Recompute profile
            vertical.compute_profile()

        self.compute_discharge()

    def change_station_misalignment(self, beam_misalignment, idx=None):
        """Coordinate changing the beam 3 rotation resulting in a new total misalignment
        for a station or stations.

        Parameters
        ----------
        beam_misalignment: float
            New total beam 3 misalignment in degrees
        idx: int
            Index of selected vertical
        """

        if idx is None:
            for vertical in self.verticals:
                if vertical.sampling_duration_sec > 0:
                    vertical.change_rotation(beam_misalignment)
        else:
            if self.verticals[idx].sampling_duration_sec > 0:
                self.verticals[idx].change_rotation(beam_misalignment)

        self.compute_discharge()

    def change_magvar(self, magvar, idx=None):
        """Coordinates the magnetic variation and recomputes the profiles and discharge.

        Parameters
        ----------
        magvar: float
            New magnetic variation in degrees
        idx: int
            Index of vertical to change
        """

        if idx is None:
            for vertical in self.verticals:
                vertical.change_mag_var(magvar)
        else:
            self.verticals[idx].change_mag_var(magvar)

        self.compute_discharge()

    def change_heading_source(self, source, idx=None):
        """Coordinates the change in heading source and recomputes the profiles and
        discharge.

        Parameters
        ----------
        source: str
            Heading source
        idx: int
            Index of vertical to change
        """
        if idx is None:
            for vertical in self.verticals:
                vertical.change_heading_source(source)
        else:
            self.verticals[idx].change_heading_source(source)

        self.compute_discharge()

    def change_sos(
        self,
        vertical_idx=None,
        parameter=None,
        salinity=None,
        temperature=None,
        selected=None,
        speed=None,
    ):
        """Applies a change in speed of sound to one or all vertical
        and update the discharge and uncertainty computations

        Parameters
        ----------
        vertical_idx: int
            Index of transect to change
        parameter: str
            Speed of sound parameter to be changed ('temperatureSrc', 'temperature',
            'salinity', 'sosSrc')
        salinity: float
            Salinity in ppt
        temperature: float
            Temperature in deg C
        selected: str
            Selected speed of sound ('internal', 'computed', 'user') or temperature
            ('internal', 'user')
        speed: float
            Manually supplied speed of sound for 'user' source
        """

        if vertical_idx is None:
            # Apply to all transects
            for vertical in self.verticals:
                vertical.change_sos(
                    parameter=parameter,
                    salinity=salinity,
                    temperature=temperature,
                    selected=selected,
                    speed=speed,
                )
        else:
            # Apply to a single transect
            self.verticals[vertical_idx].change_sos(
                parameter=parameter,
                salinity=salinity,
                temperature=temperature,
                selected=selected,
                speed=speed,
            )
        # Reapply settings to newly adjusted data
        self.compute_discharge()

    def change_vertical_location(self, idx, new_location):
        """Changes the location of specified vertical and recomputes discharge based on
        the new vertical location.

        Parameters
        ----------
        idx: int
            Index of specified vertical
        new_location: float
            New location for vertical, in m
        """

        # Set new location
        self.verticals[idx].location_m = new_location
        self.verticals[idx].location_source = "Manual"

        # Resort verticals and compute discharge
        self.sort_verticals()
        self.compute_discharge()

    def toggle_vertical_use(self, idx):
        """Toggles whether or not the vertical should be used for computing discharge.

        Parameters
        ----------
        idx: int
            Index of vertical to change
        """

        # Change use setting for the vertical
        if self.verticals[idx].use:
            self.verticals[idx].use = False
        else:
            self.verticals[idx].use = True

        # Reset the verticals used and compute discharge
        self.sort_verticals()
        self.compute_discharge()

    def update_vertical_stage(self, idx, stage, stage_time):
        """Store new stage and observation time associated with vertical.

        Parameters
        ----------
        idx: int
            Index of vertical
        stage: float
            New stage
        stage_time: str
            Stage time ##:##:##
        """

        if stage is not None:
            self.verticals[idx].gh_m = stage

        self.verticals[idx].gh_obs_time = stage_time

    def change_ws_condition(self, condition, idx=None):
        """Sets the water surface condition for one or all verticals.

        Parameters
        ----------
        condition: str
            Water surface condition (Open or Ice)
        idx: int
            Index of selected vertical
        """

        if idx is not None:
            self.verticals[idx].ws_condition = condition
            self.verticals[idx].change_fit_method(fit_method="Automatic")
            self.verticals[idx].compute_profile()
        else:
            for vertical in self.verticals:
                if vertical.sampling_duration_sec > 0:
                    vertical.ws_condition = condition
                    vertical.change_fit_method(fit_method="Automatic")
                    vertical.compute_profile()

        # Recompute discharge
        self.compute_discharge()

    def change_vel_method(self, method, idx=None):
        """Sets the velocity method for one or all verticals.

        Parameters
        ----------
        method: str
            Velocity method (Fixed, Azimuth, Magnitude)
        idx: int
            Index of selected vertical
        """

        if idx is not None:
            if method == "Azimuth" and not self.azimuth_requirements_met(
                self.verticals[idx]
            ):
                return "Azimuth is not a valid method when there is no heading data for one or more verticals."
            
            self.verticals[idx].velocity_method = method
            self.verticals[idx].compute_profile()

        else:
            for vertical in self.verticals:
                if not vertical.is_edge and not vertical.velocity_method == "Manual":
                    if method == "Azimuth" and not self.azimuth_requirements_met(
                        vertical
                    ):
                        return "Azimuth is not a valid method when there is no compass or heading data."

                    vertical.velocity_method = method
                    vertical.compute_profile()

        # Recompute discharge
        self.compute_discharge()
        return ""

    @staticmethod
    def azimuth_requirements_met(vertical):
        """Check that heading data are available if azimuth method used.
        
        Parameters
        ----------
        vertical: VerticalData
            Object of VerticalData
            
        Returns
        -------
        : boolean
        
        """
        if np.all(np.equal(vertical.data.sensors.heading_deg.internal.data, 0)):
            return False
        return True

    def change_vel_reference(self, ref, idx=None):
        """Changes the velocity reference for one or all verticals.

        Parameters
        ----------
        ref: str
            Velocity reference (None, BT)
        idx: int
            Index of specified vertical
        """

        if idx is not None:
            settings = self.verticals[idx].current_settings()
            settings["NavRef"] = ref
            self.verticals[idx].velocity_reference = ref
            self.verticals[idx].compute_profile(settings=settings)

        else:
            for vertical in self.verticals:
                if not vertical.is_edge and vertical.data.date_time is not None:
                    settings = vertical.current_settings()
                    settings["NavRef"] = ref
                    vertical.velocity_reference = ref
                    vertical.compute_profile(settings=settings)

        # Recompute discharge
        self.compute_discharge()

    def change_vel_source(self, source, speed=None, idx=None):
        """Change velocity source.

        Parameters
        ----------
        source: str
            Velocity source (ADCP, Manual)
        speed: float
            Manually entered speed in m/s
        idx: int
            Index of selected vertical, if None apply to all verticals
        """

        if idx is not None:
            # Single vertical
            self.verticals[idx].change_vel_source(source, speed)
        else:
            # All verticals
            for idx in self.verticals_used_idx:
                if self.verticals[idx].data.w_vel is not None:
                    self.verticals[idx].change_vel_source(source, speed)

        self.compute_discharge()

    def change_min_ens(self, threshold, idx=None):
        """Change the minimum number of ensembles for a valid velocity in a depth cell.

        Parameters
        ----------
        threshold: int
            Minimum number of ensembles
        idx: int
            Index of selected vertical, if None apply to all verticals
        """

        if idx is not None:
            # Single vertical
            self.verticals[idx].cell_minimum_valid_ensembles = threshold
            self.verticals[idx].compute_profile()
        else:
            # All verticals being used
            for idx in self.verticals_used_idx:
                if self.verticals[idx].sampling_duration_sec > 0:
                    self.verticals[idx].cell_minimum_valid_ensembles = threshold
                    self.verticals[idx].compute_profile()

        # Recompute discharge
        self.compute_discharge()

    def change_flow_angle(self, coefficient, idx=None):
        """Change the flow angle coefficient, used only for magnitude method.

        Parameters
        ----------
        coefficient: float
            Flow coefficient (cos(angle))
        idx: int
            Index of selected vertical, if None apply to all verticals
        """

        if idx is not None:
            # Single vertical
            self.verticals[idx].flow_angle_coefficient = coefficient
            self.verticals[idx].compute_mean_profile()
        else:
            # All verticals being used
            for idx in self.verticals_used_idx:
                if self.verticals[idx].sampling_duration_sec > 0:
                    self.verticals[idx].flow_angle_coefficient = coefficient
                    self.verticals[idx].compute_mean_profile()

        # Recompute discharge
        self.compute_discharge()

    def change_excluded_top(self, top_type, value, idx=None):
        """Changes the distance below the transducer marked invalid.

        Parameters
        ----------
        top_type: str
            Type of value (Cells, Distance)
        value: float
            Number of cells or distance
        idx: int
            Index of selected vertical, if None apply to all verticals
        """

        if idx is not None:
            # Single vertical
            self.verticals[idx].change_excluded_top(top_type=top_type, value=value)
        else:
            # All verticals being used
            for idx in self.verticals_used_idx:
                if self.verticals[idx].sampling_duration_sec > 0:
                    self.verticals[idx].change_excluded_top(
                        top_type=top_type, value=value
                    )

        # Recompute discharge
        self.compute_discharge()

    def change_excluded_bottom(self, bottom_type, value, idx=None):
        """Changes the distance above sidelobe marked invalid.

        Parameters
        ----------
        bottom_type: str
            Type of value (Cells, Distance)
        value: float
            Number of cells or distance
        idx: int
            Index of selected vertical, if None apply to all verticals
        """

        if idx is not None:
            # Single vertical
            self.verticals[idx].change_excluded_bottom(
                bottom_type=bottom_type, value=value
            )
        else:
            # All verticals being used
            for idx in self.verticals_used_idx:
                if self.verticals[idx].sampling_duration_sec > 0:
                    self.verticals[idx].change_excluded_bottom(
                        bottom_type=bottom_type, value=value
                    )

        # Recompute discharge
        self.compute_discharge()

    def change_uncertainty(self, method):
        """Changes the uncertainty method.
        
        Parameters
        ----------
        method: str
            Specifies the method (ISO or IVE)
        """
        self.uncertainty.method = method
        self.uncertainty.compute_uncertainty(self)

    def change_kmeans(self, kmeans, idx=None):
        """Changes the use of the kmeans filter.
        
        Parameters
        ----------
        kmeans: boolean
            Indicates if kmeans filter should be applied
        idx: int
            If specified the kmeans filter will be changed only for the specified vertical
        """
        """Change kmeans setting"""
        if idx is not None:
            # Single vertical
            self.verticals[idx].kmeans = kmeans
            self.verticals[idx].compute_profile()
        else:
            # All verticals being used
            for idx in self.verticals_used_idx:
                if self.verticals[idx].sampling_duration_sec > 0:
                    settings = self.verticals[idx].current_settings()
                    if self.verticals[idx].ws_condition == "Ice" and kmeans:
                        settings["depthFilterType"] = "K-Means"
                    else:
                        settings["depthFilterType"] = "Median"

                    self.verticals[idx].apply_settings(settings)
                    self.verticals[idx].compute_profile()

        # Recompute discharge
        self.compute_discharge()

    def insert_manual_vertical(
        self,
        location,
        depth,
        velocity,
        ws_condition,
        ice_thickness,
        ice_bottom,
        slush_bottom,
        use,
        velocity_correction=0.0,
    ):
        """Inserts a manual vertical.

        Parameters
        ----------
        location: float
            Location of vertical in m
        depth: float
            Depth of vertical in m
        velocity: float
            Mean velocity of vertical in m/s
        ws_condition: str
            Water surfact condition to be assigned to vertical
        ice_thickness: float
            Thickness of ice in m. Not used in computations.
        ice_bottom: float
            Depth from water surface to bottom of solid ice.
        slush_bottom: float
            Depth from water surface to bottom of slush ice.
        use: bool
            Indicates if vertical should be used
        velocity_correction: float
            Corrects velocity for angle
        """

        # Create new vertical and populate with user supplied data

        if ice_bottom > slush_bottom:
            slush_bottom = ice_bottom

        new_vertical = Vertical()

        new_vertical.from_user_input(
            location,
            depth,
            velocity,
            ws_condition,
            ice_thickness,
            ice_bottom,
            slush_bottom,
            use=use,
            velocity_correction=velocity_correction,
        )
        self.verticals.append(new_vertical)
        self.stations_inserts.append(
            [
                self.tr("Stations: Manual vertical inserted at location") + " {:4.2f}".format(
                    location
                ),
                2,
                1,
            ]
        )

        # Sort verticals
        self.sort_verticals()

        # Recompute discharge
        self.compute_discharge()

    def change_vertical_setting(self, settings, idx=None):
        """Applies a change in setting for one or more verticals and recomputed the
        discharge.

        settings: list
            List of tuples with setting variable and value
        idx: int
            Index of vertical, if being applied to a single vertical
        """

        # If index is not specified apply to all verticals used for discharge computations
        if idx is None:
            for idx in self.verticals_used_idx:
                if self.verticals[idx].sampling_duration_sec > 0:
                    s = self.verticals[idx].current_settings()
                    for setting in settings:
                        s[setting[0]] = setting[1]
                    self.verticals[idx].compute_profile(settings=s)
                else:
                    self.change_manual_vertical(self.verticals[idx], settings)
        # Apply to only the selected vertical
        else:
            if self.verticals[idx].sampling_duration_sec > 0:
                s = self.verticals[idx].current_settings()
                for setting in settings:
                    s[setting[0]] = setting[1]
                self.verticals[idx].compute_profile(settings=s)
            else:
                self.change_manual_vertical(self.verticals[idx], settings)
        # Recompute discharge and QA
        self.compute_discharge()

    def change_manual_vertical(self, vertical, settings):
        """Coordinates changing the depth reference to or from manual.
        
        Parameters
        ----------
        vertical: VerticalData
            Object of VerticalData
        settings: dict
            Dictionary of the all settings for the vertical
        """
        for setting in settings:
            if setting[0] == "depthManual":
                vertical.depth_m = setting[1]
            elif setting[0] == "depthReference":
                vertical.depth_source == setting[1]

    def change_draft(self, new_draft, idx):
        """Change draft

        Parameters
        ----------
        new_draft: float
            Draft entered by user in m
        idx: int
            Index of vertical to apply the change to
        """

        if idx is None:
            # Apply to all verticals
            for idx in self.verticals_used_idx:
                if self.verticals[idx].sampling_duration_sec > 0:
                    self.verticals[idx].change_draft(draft=new_draft)
        else:
            # Apply to selected vertical
            self.verticals[idx].change_draft(draft=new_draft)

        # Recompute discharge and QA
        self.compute_discharge()

    def change_draft_ice(self, new_draft, idx):
        """Change adcp depth below ice

        Parameters
        ----------
        new_draft: float
            Draft entered by user in m
        idx: int
            Index of vertical to apply the change to
        """

        if idx is None:
            # Apply to all verticals
            for idx in self.verticals_used_idx:
                if self.verticals[idx].sampling_duration_sec > 0:
                    self.verticals[idx].change_draft_ice(draft=new_draft)
        else:
            # Apply to selected vertical
            self.verticals[idx].change_draft_ice(draft=new_draft)

        # Recompute discharge and QA
        self.compute_discharge()

    def change_ice_thickness(self, value, idx):
        """Change ice thickness. Note: This is for reference only and does not affect
        computations.

         Parameters
        ----------
        value: float
            Ice thickness in m
        idx: int
            Index of vertical to apply the change to
        """

        if idx is None:
            # Apply to all verticals
            for idx in self.verticals_used_idx:
                self.verticals[idx].ice_thickness_m = value
                self.summary.at[idx, "Ice Thickness"] = value
        else:
            # Apply to selected vertical
            self.verticals[idx].ice_thickness_m = value
            self.summary.at[idx, "Ice Thickness"] = value

    def change_ice_bottom(self, value, idx):
        """Changes the depth of water surface to bottom of ice.

        Parameters
        ----------
        value: float
            New depth to bottom of ice
        idx: int
            Index of selected vertical
        """

        if idx is None:
            # Apply to all verticals
            for idx in self.verticals_used_idx:
                self.verticals[idx].ws_to_ice_bottom_m = value
                # Depth to slush bottom is always equal to or greater than depth to
                # ice bottom
                if (
                    self.verticals[idx].ws_to_ice_bottom_m
                    > self.verticals[idx].ws_to_slush_bottom_m
                ):
                    # Only slush bottom is used in discharge computations
                    self.verticals[idx].change_slush_bottom(value)
        else:
            self.verticals[idx].ws_to_ice_bottom_m = value
            # Depth to slush bottom is always equal to or greater than depth to
            # ice bottom
            if (
                self.verticals[idx].ws_to_ice_bottom_m
                > self.verticals[idx].ws_to_slush_bottom_m
            ):
                # Only slush bottom is used in discharge computations
                self.verticals[idx].change_slush_bottom(value)

        # Recompute discharge and QA
        self.compute_discharge()

    def change_slush_bottom(self, value, idx):
        """Changes the depth of water surface to bottom of slush.

        Parameters
        ----------
        value: float
            New depth to bottom of slush
        idx: int
            Index of selected vertical
        """

        if idx is None:
            # Apply to all verticals
            for idx in self.verticals_used_idx:
                self.verticals[idx].ws_to_slush_bottom_m = value
                # Depth to slush bottom is always equal to or greater than depth to
                # ice bottom
                if (
                    self.verticals[idx].ws_to_ice_bottom_m
                    > self.verticals[idx].ws_to_slush_bottom_m
                ):
                    self.verticals[idx].ws_to_ice_bottom_m = value
                self.verticals[idx].change_slush_bottom(value)
        else:
            self.verticals[idx].ws_to_slush_bottom_m = value
            # Depth to slush bottom is always equal to or greater than depth to
            # ice bottom
            if (
                self.verticals[idx].ws_to_ice_bottom_m
                > self.verticals[idx].ws_to_slush_bottom_m
            ):
                self.verticals[idx].ws_to_ice_bottom_m = value
            self.verticals[idx].change_slush_bottom(value)

        # Recompute discharge and QA
        self.compute_discharge()

    def change_extrapolation(
        self, fit_method, idx, top=None, bot=None, exponent=None, ice_exponent=None
    ):
        """Change extrapolation for one or more verticals.

        Parameters
        ----------
        fit_method:str
            Specifies fit method (Automatic, Manual)
        idx: int
            Indicates vertical to apply to, None means all verticals
        top: str
            Specifies top extrapolation method (Power, Constant, 3-Point, Ice)
        ice_exponent: float
            Exponent for top no slip if Ice method selected
        bot: str
            Specifies bottom extrapolation method (Power, No Slip)
        exponent: float
            Exponent for power or no slip fits
        """

        # Apply change to all verticals
        if idx is None:
            for idx in self.verticals_used_idx:
                if self.verticals[idx].sampling_duration_sec > 0:
                    self.verticals[idx].change_fit_method(
                        fit_method=fit_method,
                        top=top,
                        bot=bot,
                        exponent=exponent,
                        ice_exponent=ice_exponent,
                    )
        # Apply change to selected vertical
        else:
            self.verticals[idx].change_fit_method(
                fit_method=fit_method,
                top=top,
                bot=bot,
                exponent=exponent,
                ice_exponent=ice_exponent,
            )

        # Recompute discharge and QA
        self.compute_discharge()

    def change_edge(
        self, edge_idx, edge_bank, location, depth, correction, stage, stage_time
    ):
        """Change settings for specified edge and recompute discharge.

        Parameters
        ----------
        edge_idx: int
            Index of specified edge start=0, end=-1
        edge_bank: str
            Bank identifier for edge (Left, Right)
        location: float
            Location of edge, m
        depth: float
            Depth of edge, m
        correction: float
            Velocity correction for mid-section or edge coefficient for mean section
        stage: float
            Observed stage, m
        stage_time: str
            Time of observed stage
        """

        if edge_idx == 0:
            edge_idx = self.verticals_used_idx[0]
            other_edge_idx = self.verticals_used_idx[-1]
        else:
            other_edge_idx = self.verticals_used_idx[0]
            edge_idx = self.verticals_used_idx[-1]
            
        # Set opposite locations for start and end edges
        self.verticals[edge_idx].edge_loc = edge_bank
        if edge_bank == "Left":
            self.verticals[other_edge_idx].edge_loc = "Right"
        else:
            self.verticals[other_edge_idx].edge_loc = "Left"

        # Identify if an edge data are missing and create the appropriate edge
        if (edge_idx == 0 and not self.qa.edges["start_edge"]) or (
            edge_idx == -1 and not self.qa.edges["end_edge"]
        ):
            # Add edge vertical
            self.verticals.append(Vertical())
            if self.discharge_method == "Mean":
                edge_coef = correction
                velocity_correction = 0.0
            else:
                velocity_correction = correction
                edge_coef = 0.707

            self.verticals[-1].edge(
                is_edge=True,
                edge_loc=edge_bank,
                depth_m=depth,
                depth_source="Manual",
                station_m=location,
                velocity_correction=velocity_correction,
                edge_coef=edge_coef,
                use=True,
                gh_m=stage,
                gh_obs_time=stage_time,
            )

        else:
            # Change existing edge vertical
            self.verticals[edge_idx].location_m = location
            self.verticals[edge_idx].depth_m = depth
            self.verticals[edge_idx].gh_m = stage
            self.verticals[edge_idx].gh_obs_time = stage_time
            if self.discharge_method == "Mean":
                self.verticals[edge_idx].edge_coef = correction
            else:
                self.verticals[edge_idx].velocity_correction = correction

        # Sort verticals
        self.sort_verticals()

        # Compute discharge
        self.compute_discharge()

    def compute_discharge(self):
        """Computes the discharge using the specified discharge method."""

        # Recompute discharge
        if self.discharge_method == "Mid":
            self.process_mid_section()
            self.mid_section_summary()
        elif self.discharge_method == "Mean":
            self.process_mean_section()
            self.mean_section_summary()

        self.uncertainty.compute_uncertainty(self)
        self.qa = QAData(self, tr=self.tr)

    @staticmethod
    def compute_time_series(meas, variable=None, vertical_idx=None):
        """Computes the time series using serial time for any variable.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        variable: np.ndarray()
            Data for which the time series is requested
        vertical_idx: int
            Index of selected vertical
        """

        # Initialize variables
        data = np.array([])
        serial_time = np.array([])

        # Process transects
        if vertical_idx is None:
            for idx in meas.verticals_used_idx:
                if meas.verticals[idx].data.sensors is not None:
                    if variable == "Temperature":
                        data = np.append(
                            data,
                            meas.verticals[
                                idx
                            ].data.sensors.temperature_deg_c.internal.data,
                        )
                    ens_cum_time = np.nancumsum(
                        meas.verticals[idx].data.date_time.ens_duration_sec
                    )
                    ens_time = (
                        meas.verticals[idx].data.date_time.start_serial_time
                        + ens_cum_time
                    )
                    serial_time = np.append(serial_time, ens_time)
        else:
            idx = vertical_idx
            if meas.verticals[idx].data.sensors is not None:
                if variable == "Temperature":
                    data = np.append(
                        data,
                        meas.verticals[
                            idx
                        ].data.sensors.temperature_deg_c.internal.data,
                    )
                ens_cum_time = np.nancumsum(
                    meas.verticals[idx].data.date_time.ens_duration_sec
                )
                ens_time = (
                    meas.verticals[idx].data.date_time.start_serial_time + ens_cum_time
                )
                serial_time = np.append(serial_time, ens_time)

        return data, serial_time

    def xml_output(self, version, file_name):
        """Creates and writes and xml output file

        Parameters
        ----------
        version: str
            QRevIntMS version
        file_name: str
            filename of xml file to write

        """

        for idx in self.verticals_used_idx:
            if self.verticals[idx].data.date_time is not None:
                idx_to_use = idx
                break

        channel = ETree.Element(
            "Channel",
            QRevFilename=os.path.basename(file_name[:-4]),
            QRevVersion=version,
        )

        # (2) SiteInformation Node
        # ========================
        if self.station_name or self.station_number:
            site_info = ETree.SubElement(channel, "SiteInformation")

            # (3) StationName Node
            if self.station_name:
                ETree.SubElement(
                    site_info, "StationName", type="char"
                ).text = self.station_name

            # (3) SiteID Node
            if type(self.station_number) is str:
                ETree.SubElement(
                    site_info, "SiteID", type="char"
                ).text = self.station_number
            else:
                ETree.SubElement(site_info, "SiteID", type="char").text = str(
                    self.station_number
                )

            # (3) Persons
            ETree.SubElement(site_info, "Persons", type="char").text = self.persons

            # (3) Measurement Number
            ETree.SubElement(
                site_info, "MeasurementNumber", type="char"
            ).text = self.meas_number

            # (3) Stage start
            temp = self.stage_start_m
            if not np.isnan(temp):
                ETree.SubElement(
                    site_info, "StageStart", type="double", unitsCode="m"
                ).text = "{:.5f}".format(temp)

            # (4) Stage end
            temp = self.stage_end_m
            if not np.isnan(temp):
                ETree.SubElement(
                    site_info, "StageEnd", type="double", unitsCode="m"
                ).text = "{:.5f}".format(temp)

            # (3) Stage meas
            temp = self.stage_meas_m
            if not np.isnan(temp):
                ETree.SubElement(
                    site_info, "StageMeasurement", type="double", unitsCode="m"
                ).text = "{:.5f}".format(temp)

        # (2) QA Node
        # ===========
        qa = ETree.SubElement(channel, "QA")

        # (3) DiagnosticTestResult Node
        if len(self.system_tst) > 0:
            last_test = self.system_tst[-1].data
            failed_idx = last_test.count("FAIL")
            if failed_idx == 0:
                test_result = "Pass"
            else:
                test_result = str(failed_idx) + " Failed"
        else:
            test_result = "None"
        ETree.SubElement(qa, "DiagnosticTestResult", type="char").text = test_result

        # (3) CompassCalibrationResult Node
        try:
            last_eval = self.compass_eval[-1]
            # StreamPro, RR
            idx = last_eval.data.find("Typical Heading Error: <")
            if idx == (-1):
                # Rio Grande
                idx = last_eval.data.find(">>> Total error:")
                if idx != (-1):
                    idx_start = idx + 17
                    idx_end = idx_start + 10
                    comp_error = last_eval.data[idx_start:idx_end]
                    comp_error = "".join(
                        [n for n in comp_error if n.isdigit() or n == "."]
                    )
                else:
                    comp_error = ""
            else:
                # StreamPro, RR
                idx_start = idx + 24
                idx_end = idx_start + 10
                comp_error = last_eval.data[idx_start:idx_end]
                comp_error = "".join([n for n in comp_error if n.isdigit() or n == "."])

            # Evaluation could not be determined
            if not comp_error:
                ETree.SubElement(
                    qa, "CompassCalibrationResult", type="char"
                ).text = "Yes"
            elif comp_error == "":
                ETree.SubElement(
                    qa, "CompassCalibrationResult", type="char"
                ).text = "No"
            else:
                ETree.SubElement(qa, "CompassCalibrationResult", type="char").text = (
                    "Max " + comp_error
                )

        except (IndexError, TypeError, AttributeError):
            try:
                if len(self.compass_cal) > 0:
                    ETree.SubElement(
                        qa, "CompassCalibrationResult", type="char"
                    ).text = "Yes"
                else:
                    ETree.SubElement(
                        qa, "CompassCalibrationResult", type="char"
                    ).text = "No"
            except (IndexError, TypeError):
                ETree.SubElement(
                    qa, "CompassCalibrationResult", type="char"
                ).text = "No"

        # (3) DiagnosticTest and Text Node
        if self.system_tst:
            test_text = ""
            for test in self.system_tst:
                test_text += test.data
            diag_test = ETree.SubElement(qa, "DiagnosticTest")
            ETree.SubElement(diag_test, "Text", type="char").text = test_text

        # (3) CompassCalibration and Text Node
        compass_text = ""
        try:
            for each in self.compass_cal:
                if (self.verticals[idx_to_use].inst.manufacturer == "SonTek"):
                    idx = each.data.find("CAL_TIME")
                    compass_text += each.data[idx:]
                else:
                    compass_text += each.data
        except (IndexError, TypeError, AttributeError):
            pass
        try:
            for each in self.compass_eval:
                if (self.verticals[idx_to_use].inst.manufacturer == "SonTek"):
                    idx = each.data.find("CAL_TIME")
                    compass_text += each.data[idx:]
                else:
                    compass_text += each.data
        except (IndexError, TypeError, AttributeError):
            pass

        if len(compass_text) > 0:
            comp_cal = ETree.SubElement(qa, "CompassCalibration")
            ETree.SubElement(comp_cal, "Text", type="char").text = compass_text

        # (3) TemperatureCheck Node
        temp_check = ETree.SubElement(qa, "TemperatureCheck")

        # (4) VerificationTemperature Node
        if not np.isnan(self.temp_chk["user"]):
            ETree.SubElement(
                temp_check, "VerificationTemperature", type="double", unitsCode="degC"
            ).text = "{:.2f}".format(self.temp_chk["user"])

        # (4) InstrumentTemperature Node
        if not np.isnan(self.temp_chk["adcp"]):
            ETree.SubElement(
                temp_check, "InstrumentTemperature", type="double", unitsCode="degC"
            ).text = "{:.2f}".format(self.temp_chk["adcp"])

        # (4) TemperatureChange Node:
        temp_all = np.array([np.nan])
        for n in self.verticals_used_idx[1:-1]:
            if self.verticals[n].sampling_duration_sec > 0:
                # Check for situation where user has entered a constant temperature
                temperature_selected = getattr(
                    self.verticals[n].data.sensors.temperature_deg_c,
                    self.verticals[n].data.sensors.temperature_deg_c.selected,
                )
                temperature = temperature_selected.data
                if self.verticals[n].data.sensors.temperature_deg_c.selected != "user":
                    # Temperatures for ADCP.
                    temp_all = np.concatenate((temp_all, temperature))
                else:
                    # User specified constant temperature.
                    # Concatenate a matrix of size of internal data with repeated user values.
                    user_arr = np.tile(
                        self.verticals[n].data.sensors.temperature_deg_c.user.data,
                        (
                            np.size(
                                self.verticals[
                                    n
                                ].data.sensors.temperature_deg_c.internal.data
                            )
                        ),
                    )
                    temp_all = np.concatenate((temp_all, user_arr))

        t_range = np.nanmax(temp_all) - np.nanmin(temp_all)
        ETree.SubElement(
            temp_check, "TemperatureChange", type="double", unitsCode="degC"
        ).text = "{:.2f}".format(t_range)

        # (3) QRev_Message Node
        qa_check_keys = [
            "bt_vel",
            "compass",
            "depths",
            "edges",
            "extrapolation",
            "system_tst",
            "temperature",
            "stations",
            "verticals",
            "user",
            "w_vel",
            "rating",
            "main"
        ]

        # For each qa check retrieve messages
        messages = []
        for key in qa_check_keys:
            qa_type = getattr(self.qa, key)
            if qa_type["messages"]:
                for message in qa_type["messages"]:
                    if type(message) is str:
                        if message[:3].isupper():
                            messages.append([message, 1])
                        else:
                            messages.append([message, 2])
                    else:
                        messages.append(message)

        # Sort messages with warning at top
        messages.sort(key=lambda x: x[1])

        if len(messages) > 0:
            temp = ""
            for message in messages:
                temp = temp + message[0]
            ETree.SubElement(qa, "QRev_Message", type="char").text = temp

        # (2) Instrument Node
        # ===================
        instrument = ETree.SubElement(channel, "Instrument")

        # (3) Manufacturer Node
        ETree.SubElement(instrument, "Manufacturer", type="char").text = self.verticals[
            idx_to_use].data.inst.manufacturer
        # (3) Model Node
        ETree.SubElement(instrument, "Model", type="char").text = self.verticals[
            idx_to_use].data.inst.model

        # (3) SerialNumber Node
        sn = self.verticals[idx_to_use].data.inst.serial_num
        ETree.SubElement(instrument, "SerialNumber", type="char").text = str(sn)

        # (3) FirmwareVersion Node
        ver = self.verticals[idx_to_use].data.inst.firmware
        ETree.SubElement(instrument, "FirmwareVersion", type="char").text = str(ver)

        # (3) Frequency Node
        freq = self.verticals[idx_to_use].data.inst.frequency_khz
        if type(freq) == np.ndarray:
            freq = "Multi"
        ETree.SubElement(
            instrument, "Frequency", type="char", unitsCode="kHz"
        ).text = str(freq)

        # (3) BeamAngle Node
        ang = self.verticals[idx_to_use].data.inst.beam_angle_deg
        ETree.SubElement(
            instrument, "BeamAngle", type="double", unitsCode="deg"
        ).text = "{:.1f}".format(ang)

        # (3) BlankingDistance Node
        blank = []
        for n in self.verticals_used_idx[1:-1]:
            each = self.verticals[n]
            if each.sampling_duration_sec > 0:
                blank.append(each.data.w_vel.blanking_distance_m)
        if isinstance(blank[0], float):
            temp = np.mean(blank)
        else:
            temp = 0
        ETree.SubElement(
            instrument, "BlankingDistance", type="double", unitsCode="m"
        ).text = "{:.4f}".format(temp)

        # (3) InstrumentConfiguration Node
        if (
            self.verticals[idx_to_use].data.inst.manufacturer
            == "TRDI"
        ):
            commands = ""
            for element in self.verticals[idx_to_use].data.inst.configuration_commands:
                if type(element) is str:
                    commands += element + ":  "
                else:
                    for key in element:
                        if key[0] == "C":
                            commands += element[key] + "  "
                ETree.SubElement(
                    instrument, "InstrumentConfiguration", type="char"
                ).text = commands

        # (2) Processing Node
        # ===================
        processing = ETree.SubElement(channel, "Processing")

        # (3) SoftwareVersion Node
        ETree.SubElement(processing, "SoftwareVersion", type="char").text = version

        # (3) Type Node
        ETree.SubElement(processing, "Type", type="char").text = "QRev"

        # (3) Discharge Method Node
        if self.discharge_method == "Mid":
            temp = "Mid-Section"
        else:
            temp = "Mean-Section"
        ETree.SubElement(processing, "DischargeMethod", type="char").text = temp

        velocity_reference = []
        velocity_method = []
        ws_condition = []
        for n in range(len(self.verticals_used_idx)):
            vertical = self.verticals[self.verticals_used_idx[n]]
            velocity_reference.append(vertical.velocity_reference)
            velocity_method.append(vertical.velocity_method)
            ws_condition.append(vertical.ws_condition)

        # (3) Velocity Reference
        temp = np.unique(velocity_reference[1:-1])
        if len(temp) > 1:
            temp = "Mixed"
        else:
            temp = temp[0]
        ETree.SubElement(processing, "VelocityReference", type="char").text = temp

        # (3) Velocity Method
        temp = np.unique(velocity_method[1:-1])
        if len(temp) > 1:
            temp_text = "Mixed"
        else:
            temp_text = temp[0]
        ETree.SubElement(processing, "VelocityMethod", type="char").text = temp_text

        # (3) Tagline Azimuth
        idx = np.where(temp == "Azimuth")[0]
        if idx.shape[0] > 0:
            temp = self.tagline_azimuth_deg
            ETree.SubElement(
                processing, "TaglineAzimuth", type="double", unitsCode="deg"
            ).text = "{:.1f}".format(temp)

        # (3) Water Surface Condition
        temp = np.unique(ws_condition[1:-1])
        if len(temp) > 1:
            temp = "Mixed"
        else:
            temp = temp[0]
        ETree.SubElement(processing, "WaterSurfaceCondition", type="char").text = temp

        # (3) Filename Node
        ETree.SubElement(processing, "Filename", type="char").text = self.filename

        # (2) ChannelSummary
        # ==================
        summary = ETree.SubElement(channel, "ChannelSummary")
        # (3) Top
        temp = np.nansum(self.discharge["q_top_cms"])
        ETree.SubElement(
            summary, "ChannelTopQ", type="double", unitsCode="cms"
        ).text = "{:.5f}".format(temp)

        # (3) Middle
        temp = np.nansum(self.discharge["q_middle_cms"])
        ETree.SubElement(
            summary, "ChannelMiddleQ", type="double", unitsCode="cms"
        ).text = "{:.5f}".format(temp)

        # (3) Bottom
        temp = np.nansum(self.discharge["q_bottom_cms"])
        ETree.SubElement(
            summary, "ChannelBottomQ", type="double", unitsCode="cms"
        ).text = "{:.5f}".format(temp)

        # (3) Total
        total_q = np.nansum(self.discharge["q_cms"])
        ETree.SubElement(
            summary, "ChannelTotalQ", type="double", unitsCode="cms"
        ).text = "{:.5f}".format(total_q)

        # (3) Width
        width = self.discharge["location_m"][-1] - self.discharge["location_m"][0]
        ETree.SubElement(
            summary, "ChannelWidth", type="double", unitsCode="m"
        ).text = "{:.2f}".format(width)

        # (3) Area
        area = np.nansum(self.discharge["area_sqm"])
        ETree.SubElement(
            summary, "ChannelArea", type="double", unitsCode="sqm"
        ).text = "{:.2f}".format(area)

        # (3) Q over A
        temp = total_q / area
        ETree.SubElement(
            summary, "MeanNormalVelocity", type="double", unitsCode="mps"
        ).text = "{:.3f}".format(temp)

        # (3) MeanDepth
        temp = area / width
        ETree.SubElement(
            summary, "ChannelMeanDepth", type="double", unitsCode="m"
        ).text = "{:.3f}".format(temp)

        # (3) MaximumDepth
        temp = np.nanmax(self.discharge["depth_m"])
        ETree.SubElement(
            summary, "ChannelMaximumDepth", type="double", unitsCode="m"
        ).text = "{:.3f}".format(temp)

        # (3) MaximumWaterSpeed
        temp = np.nanmax(self.discharge["speed_mps"])
        ETree.SubElement(
            summary, "MaximumNormalVelocity", type="double", unitsCode="mps"
        ).text = "{:.3f}".format(temp)

        # (3) UserRating
        ETree.SubElement(summary, "UserRating", type="char").text = self.user_rating

        # (3) Uncertainty
        s_u = ETree.SubElement(summary, "Uncertainty")
        # Get uncertainty value
        total95 = np.nan
        if self.uncertainty.method[0:3] == "IVE":
            if self.uncertainty.method == "IVE":
                total95 = self.uncertainty.u_ive["u95_q"] * 100
            else:
                total95 = self.uncertainty.u_user["u95_q"] * 100
        elif self.uncertainty.method[0:3] == "ISO":
            if self.uncertainty.method == "ISO":
                total95 = self.uncertainty.u_iso["u95_q"] * 100
            else:
                total95 = self.uncertainty.u_user["u95_q"] * 100
        ETree.SubElement(s_u, "Model", type="char").text = self.uncertainty.method
        if not np.isnan(total95):
            ETree.SubElement(
                s_u, "Total", type="double", unitsCode="%"
            ).text = "{:.1f}".format(total95)
        else:
            ETree.SubElement(s_u, "Total", type="char").text = "N/A"
        # (3) Uncertainty Settings
        s_u_settings = ETree.SubElement(summary, "UncertaintySettings")

        # (4) Other Systematic
        ETree.SubElement(
            s_u_settings, "DefaultOtherSystematic", type="double", unitsCode="%"
        ).text = "{:.1f}".format(self.uncertainty.default_values["u_so"] * 100)

        # (4) Instrument Systematic
        ETree.SubElement(
            s_u_settings, "DefaultInstrumentSystematic", type="double", unitsCode="%"
        ).text = "{:.1f}".format(self.uncertainty.default_values["u_sm"] * 100)

        # (4) Number of Stations
        ETree.SubElement(
            s_u_settings, "DefaultNumStations", type="double", unitsCode="%"
        ).text = "{:.1f}".format(self.uncertainty.default_values["u_m"] * 100)

        # (4) Width
        ETree.SubElement(
            s_u_settings, "DefaultWidth", type="double", unitsCode="%"
        ).text = "{:.1f}".format(self.uncertainty.default_values["u_b"] * 100)

        # (4) Instrument Repeatability
        ETree.SubElement(
            s_u_settings, "DefaultInstrumentRepeatability", type="double", unitsCode="%"
        ).text = "{:.1f}".format(self.uncertainty.default_values["u_c"] * 100)

        # (4) User Other Systematic
        if not np.isnan(self.uncertainty.user_values["u_so"]):
            ETree.SubElement(
                s_u_settings, "UserOtherSystematic", type="double", unitsCode="%"
            ).text = "{:.1f}".format(self.uncertainty.user_values["u_so"] * 100)

        # (4) User Instrument Systematic
        if not np.isnan(self.uncertainty.user_values["u_sm"]):
            ETree.SubElement(
                s_u_settings, "UserInstrumentSystematic", type="double", unitsCode="%"
            ).text = "{:.1f}".format(self.uncertainty.user_values["u_sm"] * 100)

        # (4) User Number of Stations
        if not np.isnan(self.uncertainty.user_values["u_sm"]):
            ETree.SubElement(
                s_u_settings, "UserNumStations", type="double", unitsCode="%"
            ).text = "{:.1f}".format(self.uncertainty.user_values["u_m"] * 100)

        # (4) User Width
        if not np.isnan(self.uncertainty.user_values["u_b"]):
            ETree.SubElement(
                s_u_settings, "UserWidth", type="double", unitsCode="%"
            ).text = "{:.1f}".format(self.uncertainty.user_values["u_b"] * 100)

        # (4) User Instrument Repeatability
        if not np.isnan(self.uncertainty.user_values["u_c"]):
            ETree.SubElement(
                s_u_settings,
                "UserInstrumentRepeatability",
                type="double",
                unitsCode="%",
            ).text = "{:.1f}".format(self.uncertainty.user_values["u_c"] * 100)

        # (3) ISO Uncertainty
        s_u_iso = ETree.SubElement(summary, "ISOUncertainty")

        # (4) Systematic
        ETree.SubElement(
            s_u_iso, "AutoSystematic", type="double", unitsCode="%"
        ).text = "{:.1f}".format(self.uncertainty.u_iso["u_s"] * 100)

        # (4) Depth
        ETree.SubElement(
            s_u_iso, "AutoDepth", type="double", unitsCode="%"
        ).text = "{:.1f}".format(self.uncertainty.u_iso["u_d"] * 100)

        # (4) Velocity
        ETree.SubElement(
            s_u_iso, "AutoVelocity", type="double", unitsCode="%"
        ).text = "{:.1f}".format(self.uncertainty.u_iso["u_v"] * 100)

        # (4) Width
        ETree.SubElement(
            s_u_iso, "AutoWidth", type="double", unitsCode="%"
        ).text = "{:.1f}".format(self.uncertainty.u_iso["u_b"] * 100)

        # (4) Number of stations
        ETree.SubElement(
            s_u_iso, "AutoNumStations", type="double", unitsCode="%"
        ).text = "{:.1f}".format(self.uncertainty.u_iso["u_m"] * 100)

        # (4) Number of points
        ETree.SubElement(
            s_u_iso, "AutoNumPoints", type="double", unitsCode="%"
        ).text = "{:.1f}".format(self.uncertainty.u_iso["u_m"] * 100)

        # (4) Discharge 95
        ETree.SubElement(
            s_u_iso, "AutoTotal", type="double", unitsCode="%"
        ).text = "{:.1f}".format(self.uncertainty.u_iso["u95_q"] * 100)

        if self.uncertainty.method == "ISO_User":
            # (4) User Systematic
            if not np.isnan(self.uncertainty.u_user["u_s"]):
                ETree.SubElement(
                    s_u_iso, "UserSystematic", type="double", unitsCode="%"
                ).text = "{:.1f}".format(self.uncertainty.u_user["u_s"] * 100)

            # (4) User Depth
            if not np.isnan(self.uncertainty.u_user["u_d"]):
                ETree.SubElement(
                    s_u_iso, "UserDepth", type="double", unitsCode="%"
                ).text = "{:.1f}".format(self.uncertainty.u_user["u_d"] * 100)

            # (4) User Velocity
            if not np.isnan(self.uncertainty.u_user["u_v"]):
                ETree.SubElement(
                    s_u_iso, "UserVelocity", type="double", unitsCode="%"
                ).text = "{:.1f}".format(self.uncertainty.u_user["u_v"] * 100)

            # (4) User Width
            if not np.isnan(self.uncertainty.u_user["u_b"]):
                ETree.SubElement(
                    s_u_iso, "UserWidth", type="double", unitsCode="%"
                ).text = "{:.1f}".format(self.uncertainty.u_user["u_b"] * 100)

            # (4) User Number of stations
            if not np.isnan(self.uncertainty.u_user["u_m"]):
                ETree.SubElement(
                    s_u_iso, "UserNumStations", type="double", unitsCode="%"
                ).text = "{:.1f}".format(self.uncertainty.u_user["u_m"] * 100)

            # (4) User Number of points
            if not np.isnan(self.uncertainty.u_user["u_p"]):
                ETree.SubElement(
                    s_u_iso, "UserNumPoints", type="double", unitsCode="%"
                ).text = "{:.1f}".format(self.uncertainty.u_user["u_p"] * 100)

            # (4) Discharge 95
            if not np.isnan(self.uncertainty.u_user["u95_q"]):
                ETree.SubElement(
                    s_u_iso, "UserTotal", type="double", unitsCode="%"
                ).text = "{:.1f}".format(self.uncertainty.u_user["u95_q"] * 100)

        # (3) IVE Uncertainty
        s_u_ive = ETree.SubElement(summary, "IVEUncertainty")

        # (4) Systematic
        ETree.SubElement(
            s_u_ive, "AutoSystematic", type="double", unitsCode="%"
        ).text = "{:.1f}".format(self.uncertainty.u_ive["u_s"] * 100)

        # (4) Depth
        ETree.SubElement(
            s_u_ive, "AutoDepth", type="double", unitsCode="%"
        ).text = "{:.1f}".format(self.uncertainty.u_ive["u_d"] * 100)

        # (4) Velocity
        ETree.SubElement(
            s_u_ive, "AutoVelocity", type="double", unitsCode="%"
        ).text = "{:.1f}".format(self.uncertainty.u_ive["u_v"] * 100)

        # (4) Width
        ETree.SubElement(
            s_u_ive, "AutoWidth", type="double", unitsCode="%"
        ).text = "{:.1f}".format(self.uncertainty.u_ive["u_b"] * 100)

        # (4) Discharge 95
        ETree.SubElement(
            s_u_ive, "AutoTotal", type="double", unitsCode="%"
        ).text = "{:.1f}".format(self.uncertainty.u_ive["u95_q"] * 100)

        if self.uncertainty.method == "IVE_User":
            # (4) User Systematic
            if not np.isnan(self.uncertainty.u_user["u_s"]):
                ETree.SubElement(
                    s_u_ive, "UserSystematic", type="double", unitsCode="%"
                ).text = "{:.1f}".format(self.uncertainty.u_user["u_s"] * 100)

            # (4) User Depth
            if not np.isnan(self.uncertainty.u_user["u_d"]):
                ETree.SubElement(
                    s_u_ive, "UserDepth", type="double", unitsCode="%"
                ).text = "{:.1f}".format(self.uncertainty.u_user["u_d"] * 100)

            # (4) User Velocity
            if not np.isnan(self.uncertainty.u_user["u_v"]):
                ETree.SubElement(
                    s_u_ive, "UserVelocity", type="double", unitsCode="%"
                ).text = "{:.1f}".format(self.uncertainty.u_user["u_v"] * 100)

            # (4) User Width
            if not np.isnan(self.uncertainty.u_user["u_b"]):
                ETree.SubElement(
                    s_u_ive, "UserWidth", type="double", unitsCode="%"
                ).text = "{:.1f}".format(self.uncertainty.u_user["u_b"] * 100)

            # (4) User Discharge 95
            if not np.isnan(self.uncertainty.u_user["u95_q"]):
                ETree.SubElement(
                    s_u_ive, "UserTotal", type="double", unitsCode="%"
                ).text = "{:.1f}".format(self.uncertainty.u_user["u95_q"] * 100)

        # (2) StationDischarge
        sta_q = ETree.SubElement(channel, "StationDischarge")

        for n in range(self.discharge["q_cms"].shape[0]):
            # (3) Station
            q_summary = ETree.SubElement(sta_q, "Station")

            # (4) Location
            ETree.SubElement(
                q_summary, "StationLocation", type="double", unitsCode="m"
            ).text = "{:.2f}".format(self.discharge["location_m"][n])

            # (4) Depth
            ETree.SubElement(
                q_summary, "StationDepth", type="double", unitsCode="m"
            ).text = "{:.3f}".format(self.discharge["depth_m"][n])

            # (4) Width
            ETree.SubElement(
                q_summary, "StationWidth", type="double", unitsCode="m"
            ).text = "{:.3f}".format(self.discharge["width_m"][n])

            # (4) Area
            ETree.SubElement(
                q_summary, "StationArea", type="double", unitsCode="sqm"
            ).text = "{:.2f}".format(self.discharge["area_sqm"][n])

            # (4) Normal Velocity
            ETree.SubElement(
                q_summary, "StationNormalVelocity", type="double", unitsCode="mps"
            ).text = "{:.3f}".format(self.discharge["speed_mps"][n])

            # (4) Top Q
            ETree.SubElement(
                q_summary, "StationTopQ", type="double", unitsCode="cms"
            ).text = "{:.5f}".format(self.discharge["q_top_cms"][n])

            # (4) Middle Q
            ETree.SubElement(
                q_summary, "StationMiddleQ", type="double", unitsCode="cms"
            ).text = "{:.5f}".format(self.discharge["q_middle_cms"][n])

            # (4) Bottom Q
            ETree.SubElement(
                q_summary, "StationBottomQ", type="double", unitsCode="cms"
            ).text = "{:.5f}".format(self.discharge["q_bottom_cms"][n])

            # (4) Station Q
            ETree.SubElement(
                q_summary, "StationTotalQ", type="double", unitsCode="cms"
            ).text = "{:.5f}".format(self.discharge["q_cms"][n])

            # (4) Percent
            ETree.SubElement(
                q_summary, "StationPercentQ", type="double", unitsCode="cms"
            ).text = "{:.2f}".format(self.discharge["q_%"][n])

        # (2) VerticalDetails
        # ==================
        vert_details = ETree.SubElement(channel, "VerticalDetails")

        for n in self.verticals_used_idx:
            vertical = self.verticals[n]

            # (3) Vertical
            station = ETree.SubElement(vert_details, "Vertical")

            # Edge Vertical
            if vertical.is_edge:
                # (4) Bank
                temp = vertical.edge_loc
                ETree.SubElement(station, "Bank", type="char").text = temp

                # (4) EdgeLocation
                temp = vertical.location_m
                ETree.SubElement(
                    station, "Location", type="double", unitscode="m"
                ).text = "{:.2f}".format(temp)

                # (4) EdgeDepth
                temp = vertical.depth_m
                ETree.SubElement(
                    station, "EdgeDepth", type="double", unitscode="m"
                ).text = "{:.3f}".format(temp)

                if self.discharge_method == "Mid":
                    # (4) VelocityCorrectionFactor
                    temp = vertical.velocity_correction
                    ETree.SubElement(
                        station, "VelocityCorrectionFactor", type="double"
                    ).text = "{:.2f}".format(temp)

                else:
                    # (4) EdgeCoefficient
                    temp = vertical.edge_coef
                    ETree.SubElement(
                        station, "EdgeCoefficient", type="double"
                    ).text = "{:.4f}".format(temp)

            else:
                # (4) Location
                ETree.SubElement(
                    station, "Location", type="double", unitscode="m"
                ).text = "{:.2f}".format(vertical.location_m)

                # (4) StartDateTime Node
                if vertical.sampling_duration_sec > 0:
                    temp = int(vertical.data.date_time.start_serial_time)
                    temp = datetime.utcfromtimestamp(temp).strftime(
                        self.date_format + " %H:%M:%S"
                    )
                else:
                    temp = ""
                ETree.SubElement(station, "StartDateTime", type="char").text = temp

                # (4) EndDateTime Node
                if vertical.sampling_duration_sec > 0:
                    temp = int(vertical.data.date_time.end_serial_time)
                    temp = datetime.utcfromtimestamp(temp).strftime(
                        self.date_format + " %H:%M:%S"
                    )
                else:
                    temp = ""
                ETree.SubElement(station, "EndDateTime", type="char").text = temp

                # (4) WaterSurfaceCondition
                ETree.SubElement(
                    station, "WaterSurfaceCondition", type="char"
                ).text = vertical.ws_condition

                # Ice
                if vertical.ws_condition == "Ice":
                    # (4) ADCPDepthBelowSlush
                    ETree.SubElement(
                        station, "ADCPDepthBelowSlush", type="double", unitscode="m"
                    ).text = "{:.2f}".format(vertical.adcp_depth_below_ice_m)
                    # (4) IceThickness
                    ETree.SubElement(
                        station, "IceThickness", type="double", unitscode="m"
                    ).text = "{:.2f}".format(vertical.ice_thickness_m)
                    # (4) IceDepth2Bottom
                    ETree.SubElement(
                        station, "Depth2IceBottom", type="double", unitscode="m"
                    ).text = "{:.2f}".format(vertical.ws_to_ice_bottom_m)
                    # (4) SlushDepth2Bottom
                    ETree.SubElement(
                        station, "Depth2SlushBottom", type="double", unitscode="m"
                    ).text = "{:.2f}".format(vertical.ws_to_slush_bottom_m)

                # (4) ADCPDepth
                ETree.SubElement(
                    station, "ADCPDepth", type="double", unitscode="m"
                ).text = "{:.2f}".format(vertical.adcp_depth_below_ws_m)

                # (4) Depth
                ETree.SubElement(
                    station, "Depth", type="double", unitscode="m"
                ).text = "{:.2f}".format(vertical.depth_m)

                # (4) FlowAngleCoefficient
                if vertical.velocity_method == "Magnitude":
                    ETree.SubElement(
                        station, "FlowAngleCoefficient", type="double", unitscode="m"
                    ).text = "{:.2f}".format(vertical.flow_angle_coefficient)

                # (4) GaugeHeight
                ETree.SubElement(
                    station, "GaugeHeight", type="double", unitscode="m"
                ).text = "{:.3f}".format(vertical.gh_m)

                # (4) GaugeHeightTime
                ETree.SubElement(
                    station, "GaugeHeightTime", type="char"
                ).text = vertical.gh_obs_time

                # (4) Velocity Method
                ETree.SubElement(
                    station, "VelocityMethod", type="char"
                ).text = vertical.velocity_method

                # (4) Velocity Reference
                ETree.SubElement(
                    station, "VelocityReference", type="char"
                ).text = vertical.velocity_reference

                # (4) Velocity Direction
                ETree.SubElement(
                    station, "VelocityDirection", type="double", unitscode="deg"
                ).text = "{:.2f}".format(vertical.mean_velocity_direction_deg)

                # (4) Direction Standard Deviation
                ETree.SubElement(
                    station, "VelocityDirectionStdDev", type="double", unitscode="deg"
                ).text = "{:.2f}".format(vertical.mean_velocity_direction_stdev_deg)

                # (4) Velocity Magnitude
                ETree.SubElement(
                    station, "VelocityMagnitude", type="double", unitscode="mps"
                ).text = "{:.3f}".format(vertical.mean_velocity_magnitude_mps)

                # (4) Velocity Magnitude Coefficient of Variation
                ETree.SubElement(
                    station, "VelocityMagnitudeCV", type="double"
                ).text = "{:.3f}".format(vertical.mean_velocity_magnitude_cv)

                # (4) Normal Velocity
                ETree.SubElement(
                    station, "NormalVelocity", type="double", unitscode="mps"
                ).text = "{:.3f}".format(vertical.mean_speed_mps)

                # (4) Number of Valid Ensembles
                ETree.SubElement(
                    station, "NumberValidEnsembles", type="int"
                ).text = "{:.0f}".format(vertical.number_of_valid_ensembles)

                # (4) Number of Cells
                ETree.SubElement(
                    station, "NumberValidCells", type="int"
                ).text = "{:.0f}".format(vertical.number_valid_cells)

                # (4) Duration
                ETree.SubElement(
                    station, "Duration", type="double", unitscode="sec"
                ).text = "{:.1f}".format(vertical.sampling_duration_sec)

                # (4) Processing
                vertical_processing = ETree.SubElement(station, "Processing")

                # (5) Navigation Node
                navigation = ETree.SubElement(vertical_processing, "Navigation")

                # (6) Reference Node
                if vertical.sampling_duration_sec > 0:
                    ETree.SubElement(
                        navigation, "Reference", type="char"
                    ).text = vertical.data.w_vel.nav_ref
                else:
                    ETree.SubElement(navigation, "Reference", type="char").text = ""

                # (6) CompositeTrack
                if vertical.sampling_duration_sec > 0:
                    ETree.SubElement(
                        navigation, "CompositeTrack", type="char"
                    ).text = vertical.data.boat_vel.composite
                else:
                    ETree.SubElement(
                        navigation, "CompositeTrack", type="char"
                    ).text = ""

                # (6) MagneticVariation Node
                if vertical.sampling_duration_sec > 0:
                    mag_var = vertical.data.sensors.heading_deg.internal.mag_var_deg
                    mag_var = "{:.2f}".format(mag_var)
                else:
                    mag_var = ""
                ETree.SubElement(
                    navigation, "MagneticVariation", type="double", unitsCode="deg"
                ).text = mag_var

                if vertical.sampling_duration_sec > 0:
                    nav_data = vertical.data.boat_vel.bt_vel

                # (6) BeamFilter
                if vertical.sampling_duration_sec > 0:
                    temp = nav_data.beam_filter
                    if temp < 0:
                        temp = "Auto"
                    else:
                        temp = str(temp)
                else:
                    temp = ""
                ETree.SubElement(navigation, "BeamFilter", type="char").text = temp

                # (6) ErrorVelocityFilter Node
                if vertical.sampling_duration_sec > 0:
                    evf = nav_data.d_filter
                    if evf == "Manual":
                        evf = "{:.4f}".format(nav_data.d_filter_thresholds)
                else:
                    evf = ""
                ETree.SubElement(
                    navigation, "ErrorVelocityFilter", type="char", unitsCode="mps"
                ).text = evf

                # (6) VerticalVelocityFilter Node
                if vertical.sampling_duration_sec > 0:
                    vvf = nav_data.w_filter
                    if vvf == "Manual":
                        vvf = "{:.4f}".format(nav_data.w_filter_thresholds)
                else:
                    vvf = ""
                ETree.SubElement(
                    navigation, "VerticalVelocityFilter", type="char", unitsCode="mps"
                ).text = vvf

                # (6) Use measurement thresholds
                if vertical.sampling_duration_sec > 0:
                    temp = nav_data.use_measurement_thresholds
                    if temp:
                        temp = "Yes"
                    else:
                        temp = "No"
                else:
                    temp = ""
                ETree.SubElement(
                    navigation, "UseMeasurementThresholds", type="char"
                ).text = temp

                # (6) OtherFilter Node
                if vertical.sampling_duration_sec > 0:
                    o_f = nav_data.smooth_filter
                else:
                    o_f = ""
                ETree.SubElement(navigation, "OtherFilter", type="char").text = o_f

                # (6) InterpolationType Node
                if vertical.sampling_duration_sec > 0:
                    temp = nav_data.interpolate
                else:
                    temp = ""
                ETree.SubElement(
                    navigation, "InterpolationType", type="char"
                ).text = temp

                # (5) Depth Node
                depth = ETree.SubElement(vertical_processing, "Depth")

                # (6) Reference Node
                if vertical.sampling_duration_sec > 0:
                    if vertical.data.depths.selected == "bt_depths":
                        temp = "BT"
                    elif vertical.data.depths.selected == "vb_depths":
                        temp = "VB"
                    elif vertical.data.depths.selected == "ds_depths":
                        temp = "DS"
                else:
                    temp = ""
                ETree.SubElement(depth, "Reference", type="char").text = temp

                # (6) CompositeDepth Node
                if vertical.sampling_duration_sec > 0:
                    ETree.SubElement(
                        depth, "CompositeDepth", type="char"
                    ).text = vertical.data.depths.composite
                else:
                    ETree.SubElement(depth, "CompositeDepth", type="char").text = ""

                # (6) ADCPDepth Node
                if vertical.sampling_duration_sec > 0:
                    depth_data = getattr(
                        vertical.data.depths, vertical.data.depths.selected
                    )
                    temp = "{:2.4f}".format(depth_data.draft_use_m)
                else:
                    temp = ""
                ETree.SubElement(
                    depth, "ADCPDepth", type="double", unitsCode="m"
                ).text = temp

                # (6) FilterType Node
                if vertical.sampling_duration_sec > 0:
                    temp = depth_data.filter_type
                else:
                    temp = ""
                ETree.SubElement(depth, "FilterType", type="char").text = temp

                # (6) InterpolationType Node
                if vertical.sampling_duration_sec > 0:
                    temp = depth_data.interp_type
                else:
                    temp = ""
                ETree.SubElement(depth, "InterpolationType", type="char").text = temp

                # (6) AveragingMethod Node
                if vertical.sampling_duration_sec > 0:
                    temp = depth_data.avg_method
                else:
                    temp = ""
                ETree.SubElement(depth, "AveragingMethod", type="char").text = temp

                # (6) ValidDataMethod Node
                if vertical.sampling_duration_sec > 0:
                    temp = depth_data.valid_data_method
                else:
                    temp = ""
                ETree.SubElement(depth, "ValidDataMethod", type="char").text = temp

                # (5) WaterTrack Node
                water_track = ETree.SubElement(vertical_processing, "WaterTrack")

                # (6) CellMinEnsembles
                ETree.SubElement(
                    water_track, "CellMinEnsembles", type="integer", unitsCode="m"
                ).text = "{:.0f}".format(vertical.cell_minimum_valid_ensembles)

                # (6) ExcludedTop Node
                if vertical.sampling_duration_sec > 0:
                    if vertical.data.w_vel.excluded_top_type == "Distance":
                        temp = vertical.data.w_vel.excluded_top
                        ETree.SubElement(
                            water_track, "ExcludedTop", type="double", unitsCode="m"
                        ).text = "{:.4f}".format(temp)
                        ETree.SubElement(
                            water_track, "ExcludedTopType", type="char"
                        ).text = "Distance"
                    else:
                        temp = vertical.data.w_vel.excluded_top
                        ETree.SubElement(
                            water_track, "ExcludedTop", type="double", unitsCode="m"
                        ).text = "{:.0f}".format(temp)
                        ETree.SubElement(
                            water_track, "ExcludedTopType", type="char"
                        ).text = "Cells"
                else:
                    ETree.SubElement(
                        water_track, "ExcludedTop", type="double", unitsCode="m"
                    ).text = ""
                    ETree.SubElement(
                        water_track, "ExcludedTopType", type="char"
                    ).text = ""

                # (6) ExcludedBottom Node
                if vertical.sampling_duration_sec > 0:
                    if vertical.data.w_vel.excluded_bottom_type == "Distance":
                        temp = vertical.data.w_vel.excluded_bottom
                        ETree.SubElement(
                            water_track, "ExcludedBottom", type="double", unitsCode="m"
                        ).text = "{:.4f}".format(temp)
                        ETree.SubElement(
                            water_track, "ExcludedBottomType", type="char"
                        ).text = "Distance"
                    else:
                        temp = vertical.data.w_vel.excluded_bottom
                        ETree.SubElement(
                            water_track, "ExcludedBottom", type="double", unitsCode="m"
                        ).text = "{:.0f}".format(temp)
                        ETree.SubElement(
                            water_track, "ExcludedBottomType", type="char"
                        ).text = "Cells"
                else:
                    ETree.SubElement(
                        water_track, "ExcludedBottom", type="double", unitsCode="m"
                    ).text = ""
                    ETree.SubElement(
                        water_track, "ExcludedBottomType", type="char"
                    ).text = ""

                # (6) BeamFilter Node
                if vertical.sampling_duration_sec > 0:
                    temp = vertical.data.w_vel.beam_filter
                    if temp < 0:
                        temp = "Auto"
                    else:
                        temp = str(temp)
                else:
                    temp = ""
                ETree.SubElement(water_track, "BeamFilter", type="char").text = temp

                # (6) ErrorVelocityFilter Node
                if vertical.sampling_duration_sec > 0:
                    temp = vertical.data.w_vel.d_filter
                    if temp == "Manual":
                        temp = "{:.4f}".format(vertical.data.w_vel.d_filter_thresholds)
                else:
                    temp = ""
                ETree.SubElement(
                    water_track, "ErrorVelocityFilter", type="char", unitsCode="mps"
                ).text = temp

                # (6) VerticalVelocityFilter Node
                if vertical.sampling_duration_sec > 0:
                    temp = vertical.data.w_vel.w_filter
                    if temp == "Manual":
                        temp = "{:.4f}".format(vertical.data.w_vel.w_filter_thresholds)
                else:
                    temp = ""
                ETree.SubElement(
                    water_track, "VerticalVelocityFilter", type="char", unitsCode="mps"
                ).text = temp

                # (6) Use measurement thresholds
                if vertical.sampling_duration_sec > 0:
                    temp = vertical.data.w_vel.use_measurement_thresholds
                    if temp:
                        temp = "Yes"
                    else:
                        temp = "No"
                else:
                    temp = ""
                ETree.SubElement(
                    water_track, "UseMeasurementThresholds", type="char"
                ).text = temp

                # (6) OtherFilter Node
                if vertical.sampling_duration_sec > 0:
                    temp = vertical.data.w_vel.smooth_filter
                else:
                    temp = ""
                ETree.SubElement(water_track, "OtherFilter", type="char").text = temp

                # (6) SNRFilter Node
                if vertical.sampling_duration_sec > 0:
                    temp = vertical.data.w_vel.snr_filter
                else:
                    temp = ""
                ETree.SubElement(water_track, "SNRFilter", type="char").text = temp

                # (6) CellInterpolation Node
                if vertical.sampling_duration_sec > 0:
                    temp = vertical.data.w_vel.interpolate_cells
                else:
                    temp = ""
                ETree.SubElement(
                    water_track, "CellInterpolation", type="char"
                ).text = temp

                # (6) EnsembleInterpolation Node
                if vertical.sampling_duration_sec > 0:
                    temp = vertical.data.w_vel.interpolate_ens
                else:
                    temp = ""
                ETree.SubElement(
                    water_track, "EnsembleInterpolation", type="char"
                ).text = temp

                # (5) Extrapolation Node
                extrap = ETree.SubElement(vertical_processing, "Extrapolation")

                # (6) TopMethod Node
                temp = vertical.extrapolation.top_method
                ETree.SubElement(extrap, "TopMethod", type="char").text = temp

                # (6) BottomMethod Node
                temp = vertical.extrapolation.bot_method
                ETree.SubElement(extrap, "BottomMethod", type="char").text = temp

                # (6) Exponent Node
                temp = vertical.extrapolation.exponent
                ETree.SubElement(
                    extrap, "Exponent", type="double"
                ).text = "{:.4f}".format(temp)

                # (6) Exponent Node
                temp = vertical.extrapolation.ice_exponent
                ETree.SubElement(
                    extrap, "IceExponent", type="double"
                ).text = "{:.4f}".format(temp)

                # (4) Sensor Node
                t_sensor = ETree.SubElement(station, "Sensor")

                # (5) TemperatureSource Node
                if vertical.sampling_duration_sec > 0:
                    temp = vertical.data.sensors.temperature_deg_c.selected
                else:
                    temp = ""
                ETree.SubElement(t_sensor, "TemperatureSource", type="char").text = temp

                # (5) MeanTemperature Node
                if vertical.sampling_duration_sec > 0:
                    dat = getattr(
                        vertical.data.sensors.temperature_deg_c,
                        vertical.data.sensors.temperature_deg_c.selected,
                    )
                    temp = np.nanmean(dat.data)
                    temp = "{:.2f}".format(temp)
                else:
                    temp = ""
                ETree.SubElement(
                    t_sensor, "MeanTemperature", type="double", unitsCode="degC"
                ).text = temp

                # (5) MeanSalinity
                if vertical.sampling_duration_sec > 0:
                    sal_data = getattr(
                        vertical.data.sensors.salinity_ppt,
                        vertical.data.sensors.salinity_ppt.selected,
                    )
                    temp = "{:.0f}".format(np.nanmean(sal_data.data))
                else:
                    temp = ""
                ETree.SubElement(
                    t_sensor, "MeanSalinity", type="double", unitsCode="ppt"
                ).text = temp

                # (5) SpeedofSoundSource Node
                if vertical.sampling_duration_sec > 0:
                    sos_selected = getattr(
                        vertical.data.sensors.speed_of_sound_mps,
                        vertical.data.sensors.speed_of_sound_mps.selected,
                    )
                    temp = sos_selected.source
                else:
                    temp = ""
                ETree.SubElement(
                    t_sensor, "SpeedofSoundSource", type="char"
                ).text = temp

                # (5) SpeedofSound
                if vertical.sampling_duration_sec > 0:
                    sos_data = getattr(
                        vertical.data.sensors.speed_of_sound_mps,
                        vertical.data.sensors.speed_of_sound_mps.selected,
                    )
                    temp = "{:.4f}".format(np.nanmean(sos_data.data))
                else:
                    temp = ""
                ETree.SubElement(
                    t_sensor, "SpeedofSound", type="double", unitsCode="mps"
                ).text = temp

                # (5) Voltage
                if vertical.sampling_duration_sec > 0:
                    temp = "{:.2f}".format(
                        np.nanmean(vertical.data.sensors.battery_voltage.internal.data)
                    )
                else:
                    temp = ""
                ETree.SubElement(
                    t_sensor, "BatteryVoltage", type="double", unitsCode="vdc"
                ).text = temp

                if vertical.sampling_duration_sec > 0:
                    pitch_source_selected = getattr(
                        vertical.data.sensors.pitch_deg,
                        vertical.data.sensors.pitch_deg.selected,
                    )
                    roll_source_selected = getattr(
                        vertical.data.sensors.roll_deg,
                        vertical.data.sensors.roll_deg.selected,
                    )
                    mean_pitch = "{:.2f}".format(np.nanmean(pitch_source_selected.data))
                    mean_roll = "{:.2f}".format(np.nanmean(roll_source_selected.data))
                    pitch_std = "{:.2f}".format(
                        np.nanstd(pitch_source_selected.data, ddof=1)
                    )
                    roll_std = "{:.2f}".format(
                        np.nanstd(roll_source_selected.data, ddof=1)
                    )
                else:
                    mean_pitch = ""
                    mean_roll = ""
                    pitch_std = ""
                    roll_std = ""

                # (5) MeanPitch
                ETree.SubElement(
                    t_sensor, "MeanPitch", type="double", unitsCode="deg"
                ).text = mean_pitch

                # (5) MeanRoll
                ETree.SubElement(
                    t_sensor, "MeanRoll", type="double", unitsCode="deg"
                ).text = mean_roll

                # (5) PitchStdDev
                ETree.SubElement(
                    t_sensor, "PitchStdDev", type="double", unitsCode="deg"
                ).text = pitch_std

                # (5) RollStdDev
                ETree.SubElement(
                    t_sensor, "RollStdDev", type="double", unitsCode="deg"
                ).text = roll_std

        # (2) UserComment
        if len(self.comments) > 1:
            temp = ""
            for comment in self.comments:
                temp = temp + comment.replace("\n", " |||") + " |||"
            ETree.SubElement(channel, "UserComment", type="char").text = temp

        # Create xml output file
        with open(file_name, "wb") as xml_file:
            # Create binary coded output file
            et = ETree.ElementTree(channel)
            root = et.getroot()
            xml_out = ETree.tostring(root)

            # Add tabs to make output more readable and apply utf-8 encoding
            xml_out = parseString(xml_out).toprettyxml(encoding="utf-8")
            # Write file
            xml_file.write(xml_out)
