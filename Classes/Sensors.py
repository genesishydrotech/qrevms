import numpy as np
from Classes.SensorStructure import SensorStructure


class Sensors(object):
    """Stores data from meter sensors.

    Attributes
    ----------
    battery_voltage: SensorStructure
        Battery voltage suppling power to ADCP
    heading_deg: HeadingData
        Heading data for each ensemble
    pitch_deg: SensorStructure
        Pitch data for each ensemble
    roll_deg: SensorStructure
        Roll data for each ensemble
    salinity_ppt: SensorStructure
        Salinity data for each ensemble
    speed_of_sound_mps: SensorStructure
        Speed of sound data for each ensemble
    temperature_deg_c: SensorStructure
        Temperature data for each ensemble
    """

    def __init__(self):
        """Initialize class and create variable objects"""

        self.heading_deg = SensorStructure()
        self.pitch_deg = SensorStructure()
        self.roll_deg = SensorStructure()
        self.temperature_deg_c = SensorStructure()
        self.salinity_ppt = SensorStructure()
        self.speed_of_sound_mps = SensorStructure()
        self.battery_voltage = SensorStructure()

    @staticmethod
    def speed_of_sound(temperature, salinity):
        """Computes speed of sound from temperature and salinity.

        Parameters
        ----------
        temperature: float or np.array(float)
            Water temperature at transducer face, in degrees C.
        salinity: float or np.array(float)
            Water salinity at transducer face, in ppt.
        """

        # Not provided in RS Matlab file, computed from equation used in TRDI BBSS
        sos = (
            1449.2
            + 4.6 * temperature
            - 0.055 * temperature**2
            + 0.00029 * temperature**3
            + (1.34 - 0.01 * temperature) * (salinity - 35.0)
        )
        return sos

    @staticmethod
    def avg_temperature(verticals):
        """Compute mean temperature from temperature data from all transects.

        Parameters
        ----------
        verticals: list
            List of TransectData objects
        """

        temps = np.array([])
        for vertical in verticals:
            if vertical.use and vertical.data.sensors is not None:
                temps = np.append(
                    temps, vertical.data.sensors.temperature_deg_c.internal.data
                )
        return np.nanmean(temps)

    def get_hpr(self):
        """Returns the heading, pitch and roll data for the transect.

        Returns
        -------
        h: np.array(float)
            Array of heading in degrees
        p: np.array(float)
            Array of pitch in degrees
        r: np.array(float)
            Array of roll in degrees
        """
        p = getattr(self.pitch_deg, self.pitch_deg.selected).data
        r = getattr(self.roll_deg, self.roll_deg.selected).data
        h = getattr(self.heading_deg, self.heading_deg.selected).data

        return h, p, r