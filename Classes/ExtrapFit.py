import numpy as np
from Classes.SelectFit import SelectFit
from Classes.ExtrapSensitivity import ExtrapSensitivity


class ExtrapFit(object):
    """Class to compute the optimized or manually specified extrapolation methods

    Attributes
    ----------
    bot: str
        Bottom extrapolation method
    exponent: float
        Exponent for power or no slip at bottom
    fit_method: str
        Method used to determine fit.  Automatic or manual
    ice_exponent: float
        Exponent for no slip at top ice interface
    messages: str
        Variable for messages to UserWarning
    sel_fit: SelectFit
        Object of class SelectFit
    sensitivity: ExtrapSensitivity
        Object of class ExtrapSensitivity
    top: str
        Top extrapolation method

    """

    def __init__(self):
        """Initialize instance variables."""

        self.fit_method = "Automatic"
        self.sel_fit = SelectFit()
        self.sensitivity = ExtrapSensitivity()
        self.messages = []

    def populate_data(self, vertical, fit_method="Automatic", compute_sensitivity=True):
        """Store data in instance variables.

        Parameters
        ----------
        vertical: Vertical
            Object of Vertical
        fit_method: str
            Specifies fit method (Automatic, Manual)
        compute_sensitivity: bool
            Determines if sensitivity should be computed.
        """

        # Set fit method
        self.fit_method = fit_method

        # Process the vertical profile
        self.process_profile(vertical=vertical)

        # Compute the sensitivity of the final discharge to changes in extrapolation methods
        if compute_sensitivity:
            self.sensitivity.populate_data(vertical=vertical)

    def process_profile(self, vertical):
        """Function that coordinates the fitting process.

        Parameters
        ----------
        vertical: Vertical
            Object of Vertical
        """

        # Compute the fit for the selected  method
        if self.fit_method == "Manual":
            self.sel_fit.populate_data(
                vertical=vertical,
                fit_method=self.fit_method,
                top=vertical.extrapolation.top_method,
                bot=vertical.extrapolation.bot_method,
                exponent=vertical.extrapolation.exponent,
            )
        else:
            self.sel_fit.populate_data(vertical=vertical, fit_method=self.fit_method)

        if self.sel_fit.top_fit_r2 is not None:
            # Evaluate if there is a potential that a 3-point top method
            # may be appropriate
            if (self.sel_fit.top_fit_r2 > 0.9 or self.sel_fit.top_r2 > 0.9) and np.abs(
                self.sel_fit.top_max_diff
            ) > 0.2:
                self.messages.append(
                    "The vertical may warrant a 3-point fit at the top"
                )

        # Update extrapolation settings
        vertical.extrapolation.top_method = self.sel_fit.top_method
        vertical.extrapolation.bot_method = self.sel_fit.bot_method
        vertical.extrapolation.exponent = self.sel_fit.exponent
        vertical.extrapolation.ice_exponent = self.sel_fit.ice_exponent

    def update_q_sensitivity(self, vertical):
        """Updates the discharge sensitivity values.

        Parameters
        ----------
        vertical: Vertical
            Object of Vertical
        """
        self.sensitivity = ExtrapSensitivity()
        self.sensitivity.populate_data(vertical)

    def change_fit_method(
        self,
        vertical,
        new_fit_method,
        top=None,
        ice_exponent=None,
        bot=None,
        exponent=None,
        compute_sens=True,
    ):
        """Function to change the extrapolation method.

        Parameters
        ----------
        vertical: Vertical
            Object of Vertical
        new_fit_method: str
            Identifies fit method automatic or manual
        top: str
            Specifies top fit
        ice_exponent: float
            Specifies exponent for ice top method
        bot: str
            Specifies bottom fit
        exponent: float
            Specifies exponent for power or no slip fits
        compute_sens: bool
            Specifies if the velocity sensitivities should be recomputed
        """
        self.fit_method = new_fit_method

        self.sel_fit.populate_data(
            vertical=vertical,
            fit_method=new_fit_method,
            top=top,
            bot=bot,
            exponent=exponent,
            ice_exponent=ice_exponent,
        )

        self.top = self.sel_fit.top_method
        self.bot = self.sel_fit.bot_method
        self.exponent = self.sel_fit.exponent
        self.ice_exponent = self.sel_fit.ice_exponent

        if compute_sens:
            self.sensitivity = ExtrapSensitivity()
            self.sensitivity.populate_data(vertical)
