import copy
import numpy as np
import scipy.stats as sp

# This line of imports added so pyinstaller could find them when running exe.
from sklearn.metrics._pairwise_distances_reduction import (
    _datasets_pair,
    _base,
    _middle_term_computer,
    _dispatcher,
    _argkmin,
    _argkmin_classmode,
    _radius_neighbors,
)
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score


class DepthData(object):
    """Process and store depth data.
    Supported sources include bottom track, vertical beam, and manual data.

    Attributes
    ----------
    avg_method: str
            Defines averaging method applicable to bottom track (Simple, IDW)
    depth_beams_m: np.array(float)
            Depth data for transect or vertical adjusted for draft changes, in m
    depth_cell_depth_m: np.array(float)
            Depth to centerline of depth cells adjusted for draft or speed of sound changes, in m
    depth_cell_depth_orig_m: np.array
            Depth to centerline of depth cells in raw data, in m
    depth_cell_size_m:
            Size of depth cells adjusted for draft or speed of sound changes, in m
    depth_cell_size_orig_m: np.array
            Size of depth cells from raw data, in m
    depth_freq_kHz: float or nd.array(float)
            Defines acoustic frequency used to measure depth
    depth_invalid_index:
            Index of depths marked invalid
    depth_orig_m: np.array(float)
            Original beam depth data from transect or vertical (includes draft_orig), in m
    depth_processed_m: np.array(float)
            Depth data filtered and interpolated, in m
    depth_source: str
            Source of depth data (BT, VB, MN)
    depth_source_ens: np.array(object)
            Source of each depth value (BT, VB, MN, IN)
    draft_orig_m: float
            Original draft from data files, in m
    draft_use_m: float
            Draft used in computation of depth_beams_m and depth_cell_depths_m, in m
    filter_type: str
            Type of filter (None, TRDI, Median)
    interp_type: str
            Type of interpolation (None, Linear)
    valid_beams: np.array(bool)
            Logical array, 1 row for each beam identifying valid data
    valid_data: np.array(bool)
            Logical array of valid mean depth for each ensemble
    valid_data_method: str
            Method used to determine if depth for each ensemble is valid (QRev, TRDI)
    """

    def __init__(self):
        """Initialize attributes."""

        self.depth_orig_m = None
        self.depth_beams_m = None
        self.depth_processed_m = None
        self.depth_freq_kHz = None
        self.depth_invalid_index = None
        self.depth_source = None
        self.depth_source_ens = None
        self.draft_orig_m = None
        self.draft_use_m = None
        self.depth_cell_depth_orig_m = None
        self.depth_cell_depth_m = None
        self.depth_cell_size_orig_m = None
        self.depth_cell_size_m = None
        self.avg_method = None
        self.filter_type = None
        self.interp_type = None
        self.valid_data_method = None
        self.valid_data = None
        self.valid_beams = None

    def populate_data(
        self, depth_in, source_in, freq_in, draft_in, cell_depth_in, cell_size_in
    ):
        """Stores data in DepthData.

        Parameters
        ----------
        depth_in: np.array
            Raw depth data, in meters.
        source_in: str
            Source of raw depth data.
        freq_in: float
            Acoustic frequency used to measure depths, in kHz.
        draft_in: float
            Draft of transducer used to measure depths, in meters.
        cell_depth_in: np.array
            Depth to centerline of each depth cell, in meters. If source does not have
            depth cells the depth cell depth from bottom track should be used.
        cell_size_in: np.array
            Size of each depth cell, in meters. If source does not have depth cells the
            depth cell size from bottom track should be used.
        """

        self.depth_orig_m = depth_in
        self.depth_beams_m = depth_in
        self.depth_source = source_in
        self.depth_source_ens = np.array([source_in] * depth_in.shape[-1], dtype=object)
        self.depth_freq_kHz = freq_in
        self.draft_orig_m = draft_in
        self.draft_use_m = draft_in
        self.filter_type = "None"
        self.interp_type = "None"
        self.valid_data_method = "QRev"

        # For BT data set method to average multiple beam depths
        if source_in == "BT":
            self.avg_method = "IDW"
        else:
            self.avg_method = "None"

        # Store cell data
        self.depth_cell_depth_orig_m = cell_depth_in
        self.depth_cell_size_orig_m = cell_size_in
        self.depth_cell_size_m = cell_size_in
        self.depth_cell_depth_m = cell_depth_in

        # Remove all filters to initialize data
        self.apply_filter(filter_type="Off")

    def change_draft(self, draft):
        """Changes the draft for object

        Parameters
        ----------
        draft: float
            new draft for object
        """
        # Compute draft change
        draft_change = draft - self.draft_use_m
        self.draft_use_m = draft

        # Apply draft to ensemble depths if BT or VB
        if self.depth_source != "DS":
            self.depth_beams_m = self.depth_beams_m + draft_change
            self.depth_processed_m = self.depth_processed_m + draft_change

        # Apply draft to depth cell locations
        if len(self.depth_cell_depth_m) > 0:
            self.depth_cell_depth_m = self.depth_cell_depth_m + draft_change

    def add_cell_data(self, bt_depths):
        """Adds cell data to depth objects with no cell data
        such as the vertical beam and depth sounder.  This allows
        a single object to contain all the required depth data

        Parameters
        ----------
        bt_depths: DepthData
            Object of DepthData with bottom track depths
        """

        self.depth_cell_depth_orig_m = bt_depths.depth_cell_depth_orig_m
        self.depth_cell_size_m = bt_depths.depth_cell_size_m
        self.depth_cell_depth_m = bt_depths.depth_cell_depth_m

    def compute_avg_bt_depth(self, method=None):
        """Computes average depth for BT_Depths

        Parameters
        ----------
        method: str
            Averaging method (Simple or IDW)
        """

        if method is not None:
            self.avg_method = method

        # Get valid depths
        depth = np.copy(self.depth_beams_m)
        depth[np.logical_not(self.valid_beams)] = np.nan

        # Compute average depths
        self.depth_processed_m = DepthData.average_depth(
            depth, self.draft_use_m, self.avg_method
        )

        # Set depths to nan if depth are not valid beam depths
        self.depth_processed_m[np.equal(self.valid_data, False)] = np.nan

    def apply_filter(self, filter_type=None, bt_depths=None):
        """Coordinate the application of depth filters.

        Parameters
        ----------
        filter_type: str
            Type of filter to apply (None, Median, TRDI).
        bt_depths: DepthData
            Object of DepthData for bottom track depths
        """

        # Compute selected filter
        if filter_type == "Off" or filter_type is None:
            # No filter
            self.filter_none()

        elif filter_type == "TRDI":
            if self.depth_source == "BT":
                # TRDI filter for multiple returns
                self.filter_trdi()
            else:
                self.filter_trdi(bt_depths)
            self.filter_type = "TRDI"

        elif filter_type == "Median":
            self.filter_median()

        elif filter_type == "K-Means":
            self.filter_kmeans()

        self.valid_mean_data()

        # Update processed depth with filtered results
        if self.depth_source == "BT":
            # Multiple beams require averaging to obtain 1-D array
            self.compute_avg_bt_depth()
        else:
            # Single beam (VB or DS) save to 1-D array
            self.depth_processed_m = np.array(self.depth_beams_m[0, :])
            self.depth_processed_m[np.squeeze(np.equal(self.valid_data, 0))] = np.nan

    def apply_interpolation(self, transect, method=None):
        """Coordinates application of interpolations

        Parameters
        ----------
        transect: TransectData
            Object of TransectData
        method: str
            Type of interpolation to apply (None, HoldLast, Linear)
        """

        # Determine interpolation to apply
        if method is None:
            method = self.interp_type

        # Apply selected interpolation
        self.interp_type = method
        # No filtering
        if method == "None":
            self.interpolate_none()

        # Hold last valid depth indefinitely
        elif method == "HoldLast":
            self.interpolate_hold_last()

            # Identify ensembles with interpolated depths
            idx = np.where(np.logical_not(self.valid_data[:]))
            if len(idx[0]) > 0:
                idx = idx[0]
                idx2 = np.where(np.logical_not(np.isnan(self.depth_processed_m[idx])))
                if len(idx2) > 0:
                    idx2 = idx2[0]
                    self.depth_source_ens[idx[idx2]] = "IN"

        # Linear interpolation
        else:
            self.interpolate_linear(transect=transect)
            # Identify ensembles with interpolated depths
            idx = np.where(np.logical_not(self.valid_data[:]))
            if len(idx[0]) > 0:
                idx = idx[0]
                idx2 = np.where(np.logical_not(np.isnan(self.depth_processed_m[idx])))
                if len(idx2) > 0:
                    idx2 = idx2[0]
                    idx3 = idx[idx2]
                    self.depth_source_ens[idx3] = "IN"
            # Identify ensembles with interpolated depths
            idx = np.where(np.isnan(self.depth_processed_m))
            self.extrapolate_avg()
            if len(idx[0]) > 0:
                idx = idx[0]
                self.depth_source_ens[idx] = "EX"

    def apply_composite(self, comp_depth, comp_source):
        """Applies the data from CompDepth computed in DepthStructure
        to DepthData object

        Parameters
        ----------
        comp_depth: np.array(float)
            Composite depth computed in DepthStructure
        comp_source: str
            Source of composite depth (BT, VB, DS)
        """

        # Assign composite depth to property
        self.depth_processed_m = comp_depth
        self.extrapolate_avg()

        # Assign appropriate composite source for each ensemble
        self.depth_source_ens[comp_source == 1] = "BT"
        self.depth_source_ens[comp_source == 2] = "VB"
        self.depth_source_ens[comp_source == 3] = "MN"
        self.depth_source_ens[comp_source == 4] = "IN"
        self.depth_source_ens[comp_source == 5] = "EX"
        self.depth_source_ens[comp_source == 0] = "NA"

    def sos_correction(self, ratio):
        """Correct depth for new speed of sound setting

        Parameters
        ----------
        ratio: float
            Ratio of new to old speed of sound value
        """

        # Correct unprocessed depths
        self.depth_beams_m = self.draft_use_m + (
            (self.depth_beams_m - self.draft_use_m) * ratio
        )

        # Correct processed depths
        self.depth_processed_m = self.draft_use_m + (
            (self.depth_processed_m - self.draft_use_m) * ratio
        )

        # Correct cell size and location
        self.depth_cell_size_m = self.depth_cell_size_m * ratio
        self.depth_cell_depth_m = self.draft_use_m + (
            (self.depth_cell_depth_m - self.draft_use_m) * ratio
        )

    def valid_mean_data(self):
        """Determines if raw data are sufficient to compute a valid depth without
        interpolation.
        """

        if self.depth_source == "BT":
            self.valid_data = np.tile(True, self.valid_beams.shape[1])
            nvalid = np.sum(self.valid_beams, axis=0)

            if self.valid_data_method == "TRDI":
                self.valid_data[nvalid < 3] = False
            else:
                self.valid_data[nvalid < 2] = False
        else:
            self.valid_data = self.valid_beams[0, :]

    def filter_none(self):
        """Applies no filter to depth data. Removes filter if one was applied."""

        # Set all ensembles to have valid data
        if len(self.depth_beams_m.shape) > 1:
            self.valid_beams = np.tile(True, self.depth_beams_m.shape)
        else:
            self.valid_beams = np.tile(True, (1, self.depth_beams_m.shape[0]))

        # Set ensembles with no depth data to invalid
        if self.depth_source != "MAN":
            self.valid_beams[self.depth_beams_m == 0] = False
            self.valid_beams[np.isnan(self.depth_beams_m)] = False

        self.filter_type = "None"

    def filter_median(self):
        """This filter uses the median depth and 20% of the median depth
        to identify invalid depths for each beam.
        """

        # Assign raw depth data to local variable
        depth_raw = np.copy(self.depth_orig_m)

        # Determine number of beams
        n_beams = depth_raw.shape[0]

        # Reset filters to none
        self.filter_none()

        # Set filter type to Median
        self.filter_type = "Median"

        # Apply filter
        for n in range(n_beams):
            beam_median, beam_iqr = DepthData.compute_stats(depth_raw[n, :])
            upper = beam_median + beam_median * 0.2
            lower = beam_median - beam_median * 0.2
            self.valid_beams[n, depth_raw[n, :] > upper] = False
            self.valid_beams[n, depth_raw[n, :] < lower] = False

    def filter_kmeans(self):
        """This filter uses the median depth and 20% of the median depth
        to identify invalid depths for each beam.
        """

        # Assign raw depth data to local variable
        depth_raw = np.copy(self.depth_orig_m)

        # Determine number of beams
        n_beams = depth_raw.shape[0]

        # Reset filters to none
        self.filter_none()

        # Set filter type to KMeans
        self.filter_type = "K-Means"

        # Apply filter
        for n in range(n_beams):
            if np.sum(np.logical_not(np.isnan(depth_raw[n, :]))) > 0:
                beam_median = self.median_kmeans(depth_raw[n, :])
                upper = beam_median + beam_median * 0.2
                lower = beam_median - beam_median * 0.2
                self.valid_beams[n, depth_raw[n, :] > upper] = False
                self.valid_beams[n, depth_raw[n, :] < lower] = False
            else:
                self.valid_beams[n, :] = False

    def median_kmeans(self, depths):


        # Reshape of single variable data
        data = np.copy(depths)
        data = data[np.logical_not(np.isnan(data))]
        n_pts = data.size
        data = data.reshape(-1, 1)
        median_value = np.max(data)
        if n_pts > 1:
            # A list holds the silhouette coefficients for each k
            silhouette_coefficients = []
            # Default kmean settings
            kmeans_kwargs = {
                "init": "random",
                "n_init": 10,
                "max_iter": 300,
                "random_state": None,
            }

            # Determine optimal number of clusters using silhouette method
            for k in range(2, 5):
                if k < data.size:
                    kmeans = KMeans(n_clusters=k, **kmeans_kwargs)
                    kmeans.fit(data)
                    n_labels = np.unique(kmeans.labels_).size
                    if n_labels > 1 and n_labels < data.size:
                        score = silhouette_score(data, kmeans.labels_)
                    else:
                        score = 1
                    silhouette_coefficients.append(score)
            if len(silhouette_coefficients) > 0:
                max_coeff = max(silhouette_coefficients)
                if max_coeff == 1:
                    n_clusters = 1
                else:
                    n_clusters = silhouette_coefficients.index(max_coeff) + 2
                # Compute kmeans with optimal number of clusters
                opt_kmeans = KMeans(n_clusters=n_clusters, **kmeans_kwargs)
                opt_kmeans.fit(data)
                # Determine cluster with valid depths
                cluster, count = np.unique(opt_kmeans.labels_, return_counts=True)
                valid_count_idx = count >= np.nanmax([np.floor(0.1 * n_pts), 3])
                if np.any(valid_count_idx):
                    max_cluster_idx = np.where(
                        opt_kmeans.cluster_centers_
                        == np.max(opt_kmeans.cluster_centers_[valid_count_idx])
                    )[0][0]
                    median_value = np.max(opt_kmeans.cluster_centers_[valid_count_idx])

        return median_value

    @staticmethod
    def compute_stats(data):
        """Compute the median and innerquartile range for a 1D array. nan values are
        excluded from the computation.

        Parameters
        ----------
        data: np.array(float)
            Data for which statistics are desired.

        Returns
        -------
        q50: float
            Median values
        sp_iqr: float
            Innerquartile range
        """
        # Remove nan elements
        idx = np.where(np.logical_not(np.isnan(data)))[0]
        data_1d = data[idx]

        # Compute statistics
        q25, q50, q75 = sp.mstats.mquantiles(data_1d, alphap=0.5, betap=0.5)
        sp_iqr = q75 - q25

        return q50, sp_iqr

    def filter_trdi(self, bt_depths=None):
        """Filter used by TRDI to filter out multiple reflections that get digitized as
        depth.

        Parameters
        ----------
        bt_depths: DepthData
            Object of DepthData for bottom track depths
        """

        # Assign raw depth data to local variable
        depth_raw = np.copy(self.depth_orig_m)

        if bt_depths is not None:
            bt_raw = np.copy(bt_depths.depth_orig_m)
            depth_raw = np.vstack((depth_raw, bt_raw))

        # Determine number of beams
        n_beams = depth_raw.shape[0]

        # Reset filters to none
        self.filter_none()

        # Set filter type to TRDI
        self.filter_type = "TRDI"

        for n in range(n_beams):
            depth_ratio = depth_raw[n, :] / depth_raw
            exceeded = depth_ratio > 1.75
            exceeded_ens = np.nansum(exceeded, 0)
            self.valid_beams[n, exceeded_ens > 0] = False
            # If vertical beam only first row is valid
            if bt_depths is not None:
                break

    def interpolate_none(self):
        """Applies no interpolation."""

        # Compute processed depth without interpolation
        if self.depth_source == "BT":
            # Bottom track methods
            self.compute_avg_bt_depth()
        else:
            # Vertical beam or depth sounder depths
            self.depth_processed_m = self.depth_beams_m[0, :]

        self.depth_processed_m[np.squeeze(np.equal(self.valid_data, False))] = np.nan

        # Set interpolation type
        self.interp_type = "None"

    def interpolate_hold_last(self):
        """This function holds the last valid value until the next valid data point."""

        # Get number of ensembles
        n_ensembles = len(self.depth_processed_m)

        # Process data by ensemble
        for n in range(1, n_ensembles):
            # If current ensemble's depth is invalid assign depth from previous example
            if not self.valid_data[n]:
                self.depth_processed_m[n] = self.depth_processed_m[n - 1]

    def interpolate_next(self):
        """This function back fills with the next valid value."""

        # Get number of ensembles
        n_ens = len(self.depth_processed_m)

        # Process data by ensemble
        for n in np.arange(0, n_ens - 1)[::-1]:
            # If current ensemble's depth is invalid assign depth from previous example
            if not self.valid_data[n]:
                self.depth_processed_m[n] = self.depth_processed_m[n + 1]

    def interpolate_linear(self, transect):
        """Apply linear interpolation

        Parameters
        ----------
        transect: VerticalData
            Object of VerticalData
        """

        # Set interpolation type
        self.interp_type = "Linear"

        # Create position array
        if transect.boat_vel.selected == "None":
            select = None
        else:
            select = getattr(transect.boat_vel, transect.boat_vel.selected)
        if select is not None:
            boat_vel_x = select.u_processed_mps
            boat_vel_y = select.v_processed_mps
            track_x = boat_vel_x * transect.date_time.ens_duration_sec
            track_y = boat_vel_y * transect.date_time.ens_duration_sec
        else:
            select = getattr(transect.boat_vel, "bt_vel")
            track_x = np.tile(np.nan, select.u_processed_mps.shape)
            track_y = np.tile(np.nan, select.v_processed_mps.shape)

        idx = np.where(np.isnan(track_x[1:]))

        # If the navigation reference has no gaps use it for interpolation, if not use time
        if len(idx[0]) < 1:
            x = np.nancumsum(np.sqrt(track_x**2 + track_y**2))
        else:
            # Compute accumulated time
            x = np.nancumsum(transect.date_time.ens_duration_sec)

        # Determine number of beams
        n_beams = self.depth_beams_m.shape[0]
        depth_mono = copy.deepcopy(self.depth_beams_m)
        depth_new = copy.deepcopy(self.depth_beams_m)

        #  Create strict monotonic arrays for depth and track by identifying duplicate
        #  track values.  The first track value is used and the remaining duplicates
        #  are set to nan.  The depth assigned to that first track value is the average
        #  of all duplicates.  The depths for the duplicates are then set to nan.  Only
        #  valid strictly monotonic track and depth data are used for the input in to
        #  linear interpolation.   Only the interpolated data for invalid depths are
        #  added to the valid depth data to create depth_new

        x_mono = x

        idx0 = np.where(np.diff(x) == 0)[0]
        if len(idx0) > 0:
            if len(idx0) > 1:
                # Split array into subarrays in proper sequence e.g [[2,3,4],[7,8,9]] etc.
                idx1 = np.add(np.where(np.diff(idx0) != 1)[0], 1)
                group = np.split(idx0, idx1)

            else:
                # Group of only 1 point
                group = np.array([idx0])

            # Replace repeated values with mean
            n_group = len(group)
            for k in range(n_group):
                indices = group[k]
                indices = np.append(indices, indices[-1] + 1)
                depth_avg = np.nanmean(depth_mono[:, indices], axis=1)
                depth_mono[:, indices[0]] = depth_avg
                depth_mono[:, indices[1:]] = np.nan
                x[indices[1:]] = np.nan

        # Interpolate each beam

        for n in range(n_beams):
            # Determine ensembles with valid depth data
            valid_depth_mono = np.logical_not(np.isnan(depth_mono[n]))
            valid_x_mono = np.logical_not(np.isnan(x_mono))
            valid_data = copy.deepcopy(self.valid_beams[n])
            valid = np.vstack([valid_depth_mono, valid_x_mono, valid_data])
            valid = np.all(valid, 0)

            if np.sum(valid) > 1:
                # Compute interpolation function from all valid data
                depth_int = np.interp(
                    x_mono,
                    x_mono[valid],
                    depth_mono[n, valid],
                    left=np.nan,
                    right=np.nan,
                )
                # Fill in invalid data with interpolated data
                depth_new[n, np.logical_not(self.valid_beams[n])] = depth_int[
                    np.logical_not(self.valid_beams[n])
                ]

        if self.depth_source == "BT":
            # Bottom track depths
            self.depth_processed_m = self.average_depth(
                depth_new, self.draft_use_m, self.avg_method
            )
        else:
            # Vertical beam or depth sounder depths
            self.depth_processed_m = np.copy(depth_new[0, :])

    def extrapolate_avg(self):
        avg = np.nanmean(self.depth_processed_m)
        self.depth_processed_m[np.isnan(self.depth_processed_m)] = avg

    @staticmethod
    def average_depth(depth, draft, method):
        """Compute average depth from bottom track beam depths.

        Parameters
        ----------
        depth: np.array(float)
            Individual beam depths for each beam in each ensemble including the draft
        draft: float
            Draft of ADCP
        method: str
            Averaging method (Simple, IDW)

        Returns
        -------
        avg_depth: np.array(float)
            Average depth for each ensemble

        """
        if method == "Simple":
            avg_depth = np.nanmean(depth, 0)
        else:
            # Compute inverse weighted mean depth
            rng = depth - draft
            w = 1 - np.divide(rng, np.nansum(rng, 0))
            w[w == 0] = 1
            avg_depth = np.tile(np.nan, rng.shape[1])
            valid = np.nansum(np.logical_not(np.isnan(rng)), axis=0) > 0
            avg_depth[valid] = draft + np.nansum(
                (rng[:, valid] * w[:, valid]) / np.nansum(w[:, valid], axis=0), axis=0
            )
            avg_depth[avg_depth == draft] = np.nan

        return avg_depth
