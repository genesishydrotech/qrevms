import numpy as np
from Classes.FitData import FitData


class SelectFit(object):
    """Class automates the extrapolation method selection

    Attributes
    ----------
    bot_diff: float
        Difference between power and no slop at z = 0.1
    bot_method: str
        Bottom extrapolation method (Power, No Slip)
    bot_method_auto: str
        Selected extrapolation for top
    bot_r2: float
        Bottom fit r^2
    coef: float
        Power fit coefficient
    exp_method: str
        Method to determine exponent (default, optimize, or manual)
    exponent: float
        Power / No Slip fit exponent
    exponent_95_ci: np.array(float)
        95% confidence intervals for optimized exponent
    exponent_auto: float
        Selected exponent
    fit_method: str
        User selected method Automatic or Manual
    fit_r2: float
        Selected fit of selected power/no slip fit
    ice_exponent: float
        Exponent for ice fit
    ns_exponent: float
        No slip optimized exponent
    pp_exponent: float
        Power Power optimized exponent
    residuals: np.array(float)
        Residuals from fit
    rsqr: float
        Adjusted r^2 for optimized exponent
    top_fit_r2: float
        Top fit custom r^2
    top_max_diff: float
        Maximum difference between power and 3-pt at top
    top_method: str
        Top extrapolation method (Power, Constant, 3-Point, Ice)
    top_method_auto: str
        Selected extrapolation for bottom
    top_r2: float
        r^2 for linear fit of top 4 cells
    u: np.array(float)
        Fit values of the variable
    u_auto: np.array(float)
        Fit values from automatic fit
    z: np.array(float)
        Distance from the streambed for fit variable
    z_auto: np.array(float)
        z values for automatic fit
    """

    def __init__(self):
        """Intialize object and instance variables."""

        self.fit_method = "Automatic"
        self.top_method = "Power"
        self.bot_method = "Power"
        self.exponent = 0.1667
        self.exp_method = None
        self.u = np.nan
        self.u_auto = np.nan
        self.z = np.nan
        self.z_auto = np.nan
        self.residuals = np.array([])
        self.coef = 0
        self.bot_method_auto = "Power"
        self.top_method_auto = "Power"
        self.exponent_auto = 0.1667
        self.top_fit_r2 = 0
        self.top_max_diff = 0
        self.bot_diff = 0
        self.bot_r2 = 0
        self.fit_r2 = 0
        self.ns_exponent = 0.1667
        self.pp_exponent = 0.1667
        self.top_r2 = 0
        self.rsqr = 0
        self.exponent_95_ci = 0
        self.ice_exponent = 0.1667

    def populate_data(
        self, vertical, fit_method, top=None, bot=None, exponent=None, ice_exponent=None
    ):
        """Determine selected fit.

        Parameters
        ----------
        vertical: Vertical
            Object of class Vertical
        fit_method: str
            Fit method (Automatic or Manual)
        top: str
            Top extrapolation method
        bot: str
            Bottom extrapolation method
        ice_exponent: float
            Exponent for ice extrapolation method
        exponent: float
            Exponent for open water extrapolation
        """

        # Valid data should not include existing estimates for the top and bottom velocity
        # extrapolation
        if vertical.mean_profile["speed"].shape[0] > 0:
            valid_data = np.where(
                np.logical_and(
                    np.logical_not(np.isnan(vertical.mean_profile["speed"])),
                    vertical.mean_profile["number_ensembles"] > 0,
                )
            )[0]

            speed = np.copy(vertical.mean_profile["speed"])
            cell_z = vertical.depth_m - vertical.mean_profile["cell_depth"]

            # Store data in properties to object
            self.fit_method = fit_method

            update_fd = FitData()

            if fit_method == "Automatic":
                # Compute power fit with optimized exponent as reference to determine
                # if constant no slip will be more appropriate
                ppobj = FitData()
                ppobj.populate_data(
                    vertical=vertical,
                    valid_data=valid_data,
                    top="Power",
                    bot="Power",
                    method="optimize",
                )

                # Store results in object
                self.pp_exponent = ppobj.exponent
                self.residuals = ppobj.residuals
                self.rsqr = ppobj.r_squared
                self.exponent_95_ci = ppobj.exponent_95_ci

                # Begin automatic fit

                # More than 6 cells are required to compute an optimized fit.  For fewer
                # than 7 cells the default power/power fit is selected due to lack of
                # sufficient data for a good analysis
                if len(self.residuals) > 6:
                    # DSM (6/4/2021) the top and bottom were mislabeled (even in Matlab).
                    # I corrected. The computations are unaffected as the top2 and bot2
                    #  are only used in the c_shape_condition equation:
                    # c_shape_condition = (np.sign(bot2) * np.sign(top2) == np.sign(mid2)
                    # and np.abs(bot2 + top2) > 0.1)
                    # Compute the difference between the bottom two cells of data
                    # and the optimized power fit
                    bot2 = np.nansum(
                        speed[valid_data[-2:]]
                        - ppobj.coef * cell_z[valid_data[-2:]] ** ppobj.exponent
                    )

                    # Compute the difference between the top two cells of data and
                    # the optimized power fit
                    top2 = np.nansum(
                        speed[valid_data[:2]]
                        - ppobj.coef * cell_z[valid_data[:2]] ** ppobj.exponent
                    )

                    # Compute the difference between the middle two cells of data and
                    # the optimized power fit
                    mid1 = (
                        int(np.floor(len(np.logical_not(np.isnan(valid_data))) / 2)) - 1
                    )

                    mid2 = np.nansum(
                        speed[valid_data[mid1 : mid1 + 2]]
                        - ppobj.coef
                        * cell_z[valid_data[mid1 : mid1 + 2]] ** ppobj.exponent
                    )

                    self.top_method_auto = "Power"
                    self.bot_method_auto = "Power"

                    # Evaluate difference in data and power fit at water surface using
                    # a linear fit through the top 4 median cells and save results
                    y = speed[valid_data[:4]]
                    x = cell_z[valid_data[:4]]

                    coeffs = np.polyfit(x, y, 1)
                    resid = y - (coeffs[0] * x + coeffs[1])
                    corr = np.corrcoef(x, y)[0, 1]
                    self.top_fit_r2 = 1 - (np.sum(resid**2) / np.mean(np.abs(resid)))
                    self.top_r2 = corr**2

                    # Evaluate overall fit
                    # If the optimized power fit does not have an r^2 better than 0.8 or
                    # if the optimize exponent if 0.1667 falls within the 95% confidence interval
                    # of the optimized fit, there is insufficient justification to change the
                    #  exponent from 0.1667
                    if (ppobj.r_squared < 0.8) or (
                        (0.1667 > self.exponent_95_ci[0])
                        and (0.1667 < self.exponent_95_ci[1])
                    ):
                        # If an optimized exponent cannot be justified the linear fit is used
                        # to determine if a constant fit at the top is a better alternative
                        # than a power fit.  If the power fit is the better alternative
                        # the exponent is set to the default 0.1667 and the data is refit
                        if np.abs(self.top_fit_r2 < 0.8 or self.top_r2 < 0.9):
                            ppobj = FitData()
                            ppobj.populate_data(
                                vertical=vertical,
                                valid_data=valid_data,
                                top="Power",
                                bot="Power",
                                method="Manual",
                                exponent=0.1667,
                            )

                    # Evaluate fit of top and bottom portions of the profile
                    # Set save selected exponent and associated fit statistics
                    self.exponent_auto = ppobj.exponent
                    self.fit_r2 = ppobj.r_squared

                    # Compute the difference at the water surface between a linear fit of the
                    # top 4 measured cells and the best selected power fit of the whole profile
                    # self.top_max_diff = ppobj.u[-1] - np.sum(coeffs)
                    self.top_max_diff = ppobj.u[-1] - (
                        coeffs[0] * vertical.depth_m + coeffs[1]
                    )

                    # Evaluate the difference at the bottom between power using the whole
                    # profile and power using only the bottom third
                    ns_fd = FitData()
                    ns_fd.populate_data(
                        vertical=vertical,
                        valid_data=valid_data,
                        top="Constant",
                        bot="No Slip",
                        method="Optimize",
                    )
                    self.ns_exponent = ns_fd.exponent
                    self.bot_r2 = ns_fd.r_squared
                    bottom_check = np.round(
                        0.1 * (vertical.depth_m - vertical.ws_to_slush_bottom_m), 2
                    )
                    self.bot_diff = (
                        ppobj.u[np.round(ppobj.z, 2) == bottom_check][0]
                        - ns_fd.u[np.round(ns_fd.z, 2) == bottom_check][0]
                    )

                    # Begin automatic selection logic
                    # -----------------------------------

                    # A constant no slip fit condition is selected if:
                    #
                    # 1)The top of the power fit doesn't fit the data well.
                    # This is determined to be the situation when
                    # (a) the difference at the water surface between the
                    # linear fit and the power fit is greater than 10% and
                    # (b) the difference is either positive or the difference
                    # of the top measured cell differs from the best
                    # selected power fit by more than 5%.
                    top_condition = np.abs(
                        self.top_max_diff
                    ) > 0.1 * vertical.mean_measured_speed_mps and (
                        (self.top_max_diff > 0)
                        or np.abs(speed[valid_data[0]] - ppobj.u[-1])
                        > 0.05 * vertical.mean_measured_speed_mps
                    )

                    # OR

                    # 2) The bottom of the power fit doesn't fit the data
                    # well. This is determined to be the situation when (a)
                    # the difference between an optimized no slip fit
                    # and the selected best power fit of the whole profile
                    # is greater than 10% and (b) the optimized on slip fit has
                    # an r^2 greater than 0.6.
                    bottom_condition = (
                        np.abs(self.bot_diff) > 0.1 * vertical.mean_measured_speed_mps
                    ) and self.bot_r2 > 0.6

                    # OR

                    # 3) Flow is bidirectional. The sign of the top of the
                    # profile is different from the sign of the bottom of
                    # the profile.
                    bidirectional_condition = np.sign(speed[valid_data[0]]) != np.sign(
                        speed[valid_data[-1]]
                    )
                    # OR

                    # 4) The profile is C-shaped. This is determined by
                    # (a) the sign of the top and bottom difference from
                    # the best selected power fit being different than the
                    # sign of the middle difference from the best selected
                    # power fit and (b) the combined difference of the top
                    # and bottom difference from the best selected power
                    # fit being greater than 10%.
                    c_shape_condition = (
                        np.sign(bot2) * np.sign(top2) == np.sign(mid2)
                        and np.abs(bot2 + top2) > 0.1 * vertical.mean_measured_speed_mps
                    )
                    # OR

                    # 5) The water surface condition is ice
                    power_condition = False
                    if vertical.ws_condition == "Ice":
                        ice = True
                    else:
                        ice = False

                        # OR

                        # 6) Power fit is less than 0.95
                        # if self.rsqr >= 0.90:
                        #     power_condition = True

                    if (
                        top_condition
                        or bottom_condition
                        or bidirectional_condition
                        or c_shape_condition
                        or ice
                    ):
                        # if (
                        #     (self.rsqr < 0.4)
                        #     or (bidirectional_condition or c_shape_condition or ice)
                        #     or (not power_condition and (top_condition or bottom_condition))
                        # ):
                        # Set the bottom to no slip
                        self.bot_method_auto = "No Slip"
                        # If the no slip fit with an optimized exponent does not have
                        # r^2 better than 0.8 use the default 0.1667 for the no slip exponent
                        if ns_fd.r_squared > 0.8:
                            self.exponent_auto = ns_fd.exponent
                            self.fit_r2 = ns_fd.r_squared
                        else:
                            self.exponent_auto = 0.1667
                            self.fit_r2 = np.nan

                        # Use the no slip 95% confidence intervals if they are available
                        if ns_fd.exponent_95_ci is not None and np.all(
                            np.isnan(np.logical_not(ns_fd.exponent_95_ci))
                        ):
                            self.exponent_95_ci[0] = ns_fd.exponent_95_ci[0]
                            self.exponent_95_ci[1] = ns_fd.exponent_95_ci[1]
                        else:
                            self.exponent_95_ci[0] = np.nan
                            self.exponent_95_ci[1] = np.nan

                        # Set the top method to constant
                        if vertical.ws_condition == "Ice":
                            self.top_method_auto = "Ice"
                        else:
                            self.top_method_auto = "Constant"

                    else:
                        # Leave fit power/power and set the best selected optimized exponent
                        # as the automatic fit exponent
                        self.exponent_auto = ppobj.exponent

                elif vertical.ws_condition == "Ice":
                    # If the data are insufficient for a valid analysis use the power/power fit
                    # with the default 0.1667 exponent
                    self.top_method_auto = "Ice"
                    self.bot_method_auto = "No Slip"
                    self.exponent_auto = 0.1667
                    self.ns_exponent = 0.1667
                    self.ice_exponent = 0.1667
                else:
                    # If the data are insufficient for a valid analysis use the power/power fit
                    # with the default 0.1667 exponent
                    self.top_method_auto = "Power"
                    self.bot_method_auto = "Power"
                    self.exponent_auto = 0.1667

                # Update the fit using the automatically selected methods
                update_fd.populate_data(
                    vertical=vertical,
                    valid_data=valid_data,
                    top=self.top_method_auto,
                    bot=self.bot_method_auto,
                    method="Manual",
                    exponent=self.exponent_auto,
                    ice_exponent=self.ice_exponent,
                )
                self.u = update_fd.u
                self.u_auto = update_fd.u
                self.z_auto = update_fd.z
                self.z = update_fd.z

            elif fit_method == "Manual":
                # Identify changes in fit settings
                if top is None:
                    top = self.top_method
                if bot is None:
                    bot = self.bot_method
                if exponent is None:
                    exponent = self.exponent
                if ice_exponent is None:
                    ice_exponent = self.ice_exponent

                # Update fit with manual settings
                update_fd.populate_data(
                    vertical=vertical,
                    valid_data=valid_data,
                    top=top,
                    bot=bot,
                    method=fit_method,
                    exponent=exponent,
                    ice_exponent=ice_exponent,
                )
                self.u = update_fd.u
                self.z = update_fd.z

            # Store fit data in object
            self.top_method = update_fd.top_method
            self.bot_method = update_fd.bot_method
            self.exponent = update_fd.exponent
            self.coef = update_fd.coef
            self.exp_method = update_fd.exp_method
            self.residuals = update_fd.residuals
            self.ice_exponent = update_fd.ice_exponent
