import numpy as np
from datetime import datetime
from datetime import timezone
from Classes.DateTime import DateTime
from Classes.InstrumentData import InstrumentData
from Classes.BoatStructure import BoatStructure
from Classes.GPSData import GPSData
from Classes.DepthStructure import DepthStructure
from Classes.WaterData import WaterData
from Classes.Extrapolation import Extrapolation
from Classes.Sensors import Sensors
from Classes.HeadingData import HeadingData
from Classes.SensorData import SensorData
from Modules.common_functions import nandiff, cosd, arctand, tand, nan_less
from Modules.common_functions import (
    cart2pol,
    pol2cart,
    rad2azdeg,
    azdeg2rad,
    rotate_coordinates,
)
from Modules.local_time_utilities import local_time_from_iso


class VerticalData(object):
    """Raw and processed data used to compute the depth and velocity for each vertical.

    Attributes
    ----------
    boat_vel: BoatStructure
        Stores and processes bottom track and gps data, if available
    date_time: DateTime
        Stores time for each ensemble
    depths: DepthStructure
        Stores and processes manual and time series depth data
    extrap: Extrapolation
        Stores information about the extrapolation selected
    gps: GPSData
        Stores time series GPS data
    in_transect_idx: np.array(int)
        Array of ensemble indices within the vertical
        (for compatibility with QRevInt methods)
    inst: InstrumentData
        Stores information about the instrument used to collect the data
    sensors: Sensors
        Stores and processes information and time series data from sensors
    w_vel: WaterData
        Stores and processes water track data
    """

    def __init__(self):
        """Initialize attributes."""

        self.inst = None
        self.boat_vel = None
        self.w_vel = None
        self.depths = None
        self.extrap = None
        self.sensors = None
        self.date_time = None
        self.gps = None
        self.in_transect_idx = None

    # trdi
    # ====
    def from_trdi(self, mmt, cfg, section, pd0_data, excluded, date_format):
        """Coordinates assignment of data to object classes.

        Parameters
        ----------
        mmt: dict
            Data from mmt file
        cfg: dict
            Playback configuration
        section: dict
            Section information
        pd0_data: dict
            Dictionary of raw data objects
        excluded: dict
            Dictionary of excluded top distances for selected ADCPs
        date_format: str
            String defining date format
        """

        # Check to see if data are present
        if pd0_data["Wt"] is not None:
            # Date and time
            self.trdi_date_time(pd0_data, date_format)

            # Meter data
            self.inst = InstrumentData()
            self.inst.trdi_sxs(pd0=pd0_data, mmt=mmt, cfg=cfg)

            # Create valid frequency time series
            freq_ts = self.valid_frequencies(pd0_data["Inst"].freq)

            # Boat data
            self.trdi_boat(cfg, pd0_data, freq_ts)

            self.boat_vel.set_nav_reference(mmt["Site_Information"]["Reference"])

            # Depth data
            self.trdi_depth(pd0_data, section, freq_ts)

            # Water data
            self.trdi_wt(pd0_data, section, freq_ts, excluded)

            # Sensor data
            self.trdi_sensors(pd0_data, cfg)

            self.in_transect_idx = np.arange(self.w_vel.cells_above_sl.shape[1])

            # Convert data to ship coordinates. Applies pitch and roll but not heading
            self.boat_vel.change_coord_sys("Ship", self.sensors, self.inst)
            self.w_vel.change_coord_sys("Ship", self.sensors, self.inst)

            # Extrapolation
            self.trdi_extrap(section, cfg)

    def trdi_date_time(self, pd0_data, date_format):
        """Create date_time object.

        Parameters
        ----------
        pd0_data: dict
            Dictionary of pd0 data for this vertical
        date_format: str
            String defining date format
        """

        # Ensemble times
        # Compute time for each ensemble in seconds
        ens_time_sec = (
            pd0_data["Sensor"].time[:, 0] * 3600
            + pd0_data["Sensor"].time[:, 1] * 60
            + pd0_data["Sensor"].time[:, 2]
            + pd0_data["Sensor"].time[:, 3] / 100
        )

        # Compute the duration of each ensemble in seconds adjusting for lost data
        ens_delta_time = np.tile([np.nan], ens_time_sec.shape)
        idx_time = np.where(np.logical_not(np.isnan(ens_time_sec)))[0]
        ens_delta_time[idx_time[1:]] = nandiff(ens_time_sec[idx_time])

        # Adjust for transects tha last past midnight
        idx_24hr = np.where(nan_less(ens_delta_time, 0))[0]
        ens_delta_time[idx_24hr] = 24 * 3600 + ens_delta_time[idx_24hr]
        ens_delta_time = ens_delta_time.T

        # Start date and time
        idx = np.where(np.logical_not(np.isnan(pd0_data["Sensor"].time[:, 0])))[0][0]
        start_year = int(pd0_data["Sensor"].date[idx, 0])

        # StreamPro doesn't include y2k dates
        if start_year < 100:
            start_year = 2000 + int(pd0_data["Sensor"].date_not_y2k[idx, 0])

        start_month = int(pd0_data["Sensor"].date[idx, 1])
        start_day = int(pd0_data["Sensor"].date[idx, 2])
        start_hour = int(pd0_data["Sensor"].time[idx, 0])
        start_min = int(pd0_data["Sensor"].time[idx, 1])
        start_sec = int(
            pd0_data["Sensor"].time[idx, 2] + pd0_data["Sensor"].time[idx, 3] / 100
        )
        start_micro = int(
            (
                (
                    pd0_data["Sensor"].time[idx, 2]
                    + pd0_data["Sensor"].time[idx, 3] / 100
                )
                - start_sec
            )
            * 10**6
        )

        start_dt = datetime(
            start_year,
            start_month,
            start_day,
            start_hour,
            start_min,
            start_sec,
            start_micro,
            tzinfo=timezone.utc,
        )
        start_serial_time = start_dt.timestamp()
        start_date = datetime.strftime(
            datetime.utcfromtimestamp(start_serial_time), date_format
        )

        # End data and time
        idx = np.where(np.logical_not(np.isnan(pd0_data["Sensor"].time[:, 0])))[0][-1]
        end_year = int(pd0_data["Sensor"].date[idx, 0])
        # StreamPro does not include Y@K dates
        if end_year < 100:
            end_year = 2000 + int(pd0_data["Sensor"].date_not_y2k[idx, 0])

        end_month = int(pd0_data["Sensor"].date[idx, 1])
        end_day = int(pd0_data["Sensor"].date[idx, 2])
        end_hour = int(pd0_data["Sensor"].time[idx, 0])
        end_min = int(pd0_data["Sensor"].time[idx, 1])
        end_sec = int(
            pd0_data["Sensor"].time[idx, 2] + pd0_data["Sensor"].time[idx, 3] / 100
        )
        end_micro = int(
            (
                (
                    pd0_data["Sensor"].time[idx, 2]
                    + pd0_data["Sensor"].time[idx, 3] / 100
                )
                - end_sec
            )
            * 10**6
        )

        end_dt = datetime(
            end_year,
            end_month,
            end_day,
            end_hour,
            end_min,
            end_sec,
            end_micro,
            tzinfo=timezone.utc,
        )
        end_serial_time = end_dt.timestamp()

        # Create date/time object
        self.date_time = DateTime()
        self.date_time.populate_data(
            date_in=start_date,
            start_in=start_serial_time,
            end_in=end_serial_time,
            ens_dur_in=ens_delta_time,
        )

    def trdi_boat(self, cfg, pd0_data, freq_ts):
        """Create boat object.

        Parameters
        ----------
        cfg: dict
            Dictionary of latest configuration node from mmt file
        pd0_data:
            pd0 data for this vertical
        freq_ts: nd.array()
            Array of frequencies used
        """
        # Initialize boat vel
        self.boat_vel = BoatStructure()

        # Apply 3-beam setting from mmt file
        if cfg["Processing"]["Use_3_Beam_Solution_For_BT"] == "NO":
            min_beams = 4
        else:
            min_beams = 3
        self.boat_vel.add_boat_object(
            source="TRDI",
            vel_in=pd0_data["Bt"].vel_mps,
            freq_in=freq_ts,
            coord_sys_in=pd0_data["Cfg"].coord_sys[0],
            nav_ref_in="BT",
            min_beams=min_beams,
            bottom_mode=pd0_data["Cfg"].bm[0],
            corr_in=pd0_data["Bt"].corr,
            rssi_in=pd0_data["Bt"].rssi,
        )

        if pd0_data["Gps2"].gps_data_available:
            # Compute velocities from GPS Data
            # ------------------------------------
            # Raw Data
            raw_gga_utc = pd0_data["Gps2"].utc
            raw_gga_lat = pd0_data["Gps2"].lat_deg
            raw_gga_lon = pd0_data["Gps2"].lon_deg

            # Determine correct sign for latitude
            for n, lat_ref in enumerate(pd0_data["Gps2"].lat_ref):
                idx = np.nonzero(np.array(lat_ref) == "S")
                raw_gga_lat[n, idx] = raw_gga_lat[n, idx] * -1

            # Determine correct sign for longitude
            for n, lon_ref in enumerate(pd0_data["Gps2"].lon_ref):
                idx = np.nonzero(np.array(lon_ref) == "W")
                raw_gga_lon[n, idx] = raw_gga_lon[n, idx] * -1

            # Assign data to local variables
            raw_gga_alt = pd0_data["Gps2"].alt
            raw_gga_diff = pd0_data["Gps2"].corr_qual
            raw_gga_hdop = pd0_data["Gps2"].hdop
            raw_gga_num_sats = pd0_data["Gps2"].num_sats
            raw_vtg_course = pd0_data["Gps2"].course_true
            raw_vtg_speed = pd0_data["Gps2"].speed_kph * 0.2777778
            raw_vtg_delta_time = pd0_data["Gps2"].vtg_delta_time
            raw_vtg_mode_indicator = pd0_data["Gps2"].mode_indicator
            raw_gga_delta_time = pd0_data["Gps2"].gga_delta_time

            # RSL provided ensemble values, not supported for TRDI data
            ext_gga_utc = []
            ext_gga_lat = []
            ext_gga_lon = []
            ext_gga_alt = []
            ext_gga_diff = []
            ext_gga_hdop = []
            ext_gga_num_sats = []
            ext_vtg_course = []
            ext_vtg_speed = []

            # QRev methods GPS processing methods
            gga_p_method = "Mindt"
            gga_v_method = "Mindt"
            vtg_method = "Mindt"

            # If valid gps data exist, process the data
            if (np.nansum(np.nansum(np.abs(raw_gga_lat))) > 0) or (
                np.nansum(np.nansum(np.abs(raw_vtg_speed))) > 0
            ):
                # Process raw GPS data
                self.gps = GPSData()
                self.gps.populate_data(
                    raw_gga_utc=raw_gga_utc,
                    raw_gga_lat=raw_gga_lat,
                    raw_gga_lon=raw_gga_lon,
                    raw_gga_alt=raw_gga_alt,
                    raw_gga_diff=raw_gga_diff,
                    raw_gga_hdop=raw_gga_hdop,
                    raw_gga_num_sats=raw_gga_num_sats,
                    raw_gga_delta_time=raw_gga_delta_time,
                    raw_vtg_course=raw_vtg_course,
                    raw_vtg_speed=raw_vtg_speed,
                    raw_vtg_delta_time=raw_vtg_delta_time,
                    raw_vtg_mode_indicator=raw_vtg_mode_indicator,
                    ext_gga_utc=ext_gga_utc,
                    ext_gga_lat=ext_gga_lat,
                    ext_gga_lon=ext_gga_lon,
                    ext_gga_alt=ext_gga_alt,
                    ext_gga_diff=ext_gga_diff,
                    ext_gga_hdop=ext_gga_hdop,
                    ext_gga_num_sats=ext_gga_num_sats,
                    ext_vtg_course=ext_vtg_course,
                    ext_vtg_speed=ext_vtg_speed,
                    gga_p_method=gga_p_method,
                    gga_v_method=gga_v_method,
                    vtg_method=vtg_method,
                )

                # If valid gga data exists create gga boat velocity object
                if np.nansum(np.nansum(np.abs(raw_gga_lat))) > 0:
                    self.boat_vel.add_boat_object(
                        source="TRDI",
                        vel_in=self.gps.gga_velocity_ens_mps,
                        coord_sys_in="Earth",
                        nav_ref_in="GGA",
                    )

                # If valid vtg data exist create vtg boat velocity object
                if np.nansum(np.nansum(np.abs(raw_vtg_speed))) > 0:
                    self.boat_vel.add_boat_object(
                        source="TRDI",
                        vel_in=self.gps.vtg_velocity_ens_mps,
                        coord_sys_in="Earth",
                        nav_ref_in="VTG",
                    )

    def trdi_depth(self, pd0_data, section, freq_ts):
        """Create depth object.

        Parameters
        ----------
        pd0_data:
            pd0 data for this vertical
        section: dict
            Dictionary of section node for this vertical from mmt file
        freq_ts: nd.array()
            Array of frequencies used
        """

        # Get and compute ensemble beam depths
        temp_depth_bt = np.array(pd0_data["Bt"].depth_m)

        # Screen out invalid depths
        temp_depth_bt[temp_depth_bt < 0.01] = np.nan

        # Add draft
        if section["DischargeMethod"] == "2":
            draft = float(section["XDucerDepth"]) + float(section["WS_ToBottomOfSlush"])
        else:
            draft = float(section["XDucerDepth"])
        temp_depth_bt += draft

        # Get instrument cell data
        (
            cell_size_all_m,
            cell_depth_m,
            sl_cutoff_per,
            sl_lag_effect_m,
        ) = self.trdi_compute_cell_data(pd0_data)

        # Adjust cell depth for draft

        cell_depth_m = np.add(draft, cell_depth_m)

        # Create depth data object for BT
        self.depths = DepthStructure()
        self.depths.add_depth_object(
            depth_in=temp_depth_bt,
            source_in="BT",
            freq_in=freq_ts,
            draft_in=draft,
            cell_depth_in=cell_depth_m,
            cell_size_in=cell_size_all_m,
        )

        # Check for the presence of vertical beam data
        if "L" in pd0_data["Sensor"].vert_beam_gain or "H" in pd0_data["Sensor"].vert_beam_gain:
            temp_depth_vb = np.tile(np.nan, (1, cell_depth_m.shape[1]))
            temp_depth_vb[0, :] = pd0_data["Sensor"].vert_beam_range_m

            # Screen out invalid depths
            temp_depth_vb[temp_depth_vb < 0.01] = np.nan

            # Add draft
            temp_depth_vb = temp_depth_vb + draft

            # Create depth data object for vertical beam
            self.depths.add_depth_object(
                depth_in=temp_depth_vb,
                source_in="VB",
                freq_in=freq_ts,
                draft_in=draft,
                cell_depth_in=cell_depth_m,
                cell_size_in=cell_size_all_m,
            )

        # Manual depth
        if float(section["ManualDepth"]) >= 0:
            # Create depth data object for manual depth
            self.depths.add_depth_object(
                depth_in=np.array(
                    [[float(section["ManualDepth"])] * cell_depth_m.shape[1]]
                ),
                source_in="MAN",
                freq_in=np.array([0] * cell_depth_m.shape[1]),
                draft_in=float(section["XDucerDepth"]),
                cell_depth_in=cell_depth_m,
                cell_size_in=cell_size_all_m,
            )

        # Set depth reference to value from mmt file
        self.depths.selected = "bt_depths"
        self.depths.composite_depths(transect=self, setting="Off")

        if "WaterDepthSource" in section:
            # Manual depth
            if section["WaterDepthSource"] == "1":
                if self.depths.man_depths is not None:
                    self.depths.selected = "man_depths"
                else:
                    self.depths.selected = "bt_depths"
                self.depths.composite_depths(transect=self, setting="Off")

            # Vertical beam
            elif section["WaterDepthSource"] == "2":
                if self.depths.vb_depths is not None:
                    self.depths.selected = "vb_depths"
                else:
                    self.depths.selected = "bt_depths"
                self.depths.composite_depths(transect=self, setting="Off")

            # BT beams
            elif section["WaterDepthSource"] == "3":
                if self.depths.vb_depths is not None:
                    self.depths.selected = "vb_depths"
                    if self.depths.vb_depths is not None:
                        self.depths.composite_depths(transect=self, setting="On")
                    else:
                        self.depths.composite_depths(transect=self, setting="Off")
                elif self.depths.bt_depths is not None:
                    self.depths.selected = "bt_depths"
                    self.depths.composite_depths(transect=self, setting="Off")
                else:
                    self.depths.selected = "man_depths"
                    self.depths.composite_depths(transect=self, setting="Off")

    def trdi_wt(self, pd0_data, section, freq_ts, excluded):
        """Create w_vel object.

        Parameters
        ----------
        pd0_data:
            pd0 data for this vertical
        section: dict
            Dictionary of section node for this vertical from mmt file
        freq_ts: nd.array()
            Array of frequencies used
        excluded: dict
            Dictionary of excluded top distances for selected ADCPs
        """

        # Determine ping_type
        ensemble_ping_type = self.trdi_ping_type(pd0_data)

        # Compute cells above side lobe
        (
            cell_size_all_m,
            cell_depth_m,
            sl_cutoff_per,
            sl_lag_effect_m,
        ) = self.trdi_compute_cell_data(pd0_data)

        cells_above_sl, sl_cutoff_m = self.side_lobe_cutoff(
            depths=self.depths,
            sl_lag_effect=sl_lag_effect_m,
            slc_type="Percent",
            value=1 - sl_cutoff_per / 100,
        )

        # Check for RiverRay and RiverPro data
        firmware = str(pd0_data["Inst"].firm_ver[0])

        # Determine excluded top and bottom
        excluded_bottom = 0
        excluded_bottom_type = "Distance"
        excluded_top = 0
        excluded_top_type = "Distance"
        if "CutTopBins" in section:
            if int(section["CutTopBins"]) > 0:
                excluded_top = int(section["CutTopBins"])
                excluded_top_type = "Cells"
        if "CutBinsAboveSidelobe" in section:
            if int(section["CutBinsAboveSidelobe"]) > 0:
                excluded_bottom = int(section["CutBinsAboveSidelobe"])
                excluded_bottom_type = "Cells"

        # The minimum distance for RioPro is 25 cm
        if (firmware[:2] == "56") and (
            np.nanmax(pd0_data["Sensor"].vert_beam_status) < 0.9
        ):
            if excluded_top_type == "Cells":
                dist = self.depths.bt_depths.depth_cell_depth_m[excluded_top - 1]
                dist[dist < excluded["RioPro"]] = excluded["RioPro"]
            elif excluded_top < excluded["RioPro"]:
                excluded_top = excluded["RioPro"]
                excluded_top_type = "Distance"

        if (firmware[:2] == "44") or (firmware[:2] == "56"):
            # Process water velocities for RiverRay and RiverPro
            self.w_vel = WaterData()
            self.w_vel.populate_data(
                vel_in=pd0_data["Wt"].vel_mps,
                freq_in=freq_ts,
                coord_sys_in=pd0_data["Cfg"].coord_sys,
                nav_ref_in="None",
                rssi_in=pd0_data["Wt"].rssi,
                rssi_units_in="Counts",
                excluded_bottom_in=excluded_bottom,
                excluded_bottom_type_in=excluded_bottom_type,
                excluded_top_in=excluded_top,
                excluded_top_type_in=excluded_top_type,
                cells_above_sl_in=cells_above_sl,
                sl_cutoff_per_in=sl_cutoff_per,
                sl_cutoff_num_in=0,
                sl_cutoff_type_in="Percent",
                sl_lag_effect_in=sl_lag_effect_m,
                sl_cutoff_m=sl_cutoff_m,
                wm_in=pd0_data["Cfg"].wm[0],
                blank_in=pd0_data["Cfg"].wf_cm[0] / 100,
                corr_in=pd0_data["Wt"].corr,
                surface_vel_in=pd0_data["Surface"].vel_mps,
                surface_rssi_in=pd0_data["Surface"].rssi,
                surface_corr_in=pd0_data["Surface"].corr,
                surface_num_cells_in=pd0_data["Surface"].no_cells,
                ping_type=ensemble_ping_type,
            )

        else:
            # Process water velocities for Rio Grande and StreamPro ADCPs
            self.w_vel = WaterData()
            self.w_vel.populate_data(
                vel_in=pd0_data["Wt"].vel_mps,
                freq_in=freq_ts,
                coord_sys_in=pd0_data["Cfg"].coord_sys[0],
                nav_ref_in="None",
                rssi_in=pd0_data["Wt"].rssi,
                rssi_units_in="Counts",
                excluded_bottom_in=excluded_bottom,
                excluded_bottom_type_in=excluded_bottom_type,
                excluded_top_in=excluded_top,
                excluded_top_type_in=excluded_top_type,
                cells_above_sl_in=cells_above_sl,
                sl_cutoff_per_in=sl_cutoff_per,
                sl_cutoff_num_in=0,
                sl_cutoff_type_in="Percent",
                sl_lag_effect_in=sl_lag_effect_m,
                sl_cutoff_m=sl_cutoff_m,
                wm_in=pd0_data["Cfg"].wm[0],
                blank_in=pd0_data["Cfg"].wf_cm[0] / 100,
                corr_in=pd0_data["Wt"].corr,
                ping_type=ensemble_ping_type,
            )

    def trdi_extrap(self, section, cfg):
        """Create extrap object.

        Parameters
        ----------
        section: dict
            Section information
        cfg: dict
            Playback configuration
        """

        # Create extrap object
        # --------------------
        # Determine top method
        top = "Power"
        bot = "Power"

        if section["DischargeMethod"] == "1":
            top = "Constant"
            bot = "No Slip"
        elif section["DischargeMethod"] == "2":
            top = "No Slip"
            bot = "No Slip"
        elif section["DischargeMethod"] == "3":
            top = "3-Point"
            bot = "No Slip"

        if "Power_Exponent" in section:
            exp = float(section["Power_Exponent"])
        else:
            exp = float(cfg["Discharge"]["Power_Curve_Coef"])
        self.extrap = Extrapolation()
        if top == "No Slip":
            ice_exp = 0.1667
        else:
            ice_exp = np.nan
        self.extrap.populate_data(top=top, bot=bot, exp=exp, ice_exp=ice_exp)

    def trdi_sensors(self, pd0_data, cfg):
        """Create sensors object.

        Parameters
        ----------
        pd0_data:
            pd0 data for this vertical
        cfg: dict
            Playback configuration
        """

        # Sensor Data
        self.sensors = Sensors()

        # Internal Heading
        # ----------------
        self.sensors.heading_deg.internal = HeadingData()
        if "MagneticVariation" in cfg["Offsets"]:
            magvar = float(cfg["Offsets"]["MagneticVariation"])
        else:
            magvar = 0.0
        if "Beam3_Misalignment" in cfg["Offsets"]:
            align = float(cfg["Offsets"]["Beam3_Misalignment"])
        else:
            align = 0.0
        self.sensors.heading_deg.internal.populate_data(
            data_in=pd0_data["Sensor"].heading_deg.T,
            source_in="internal",
            magvar=magvar,
            align=align,
        )
        self.sensors.heading_deg.selected = "internal"

        # Pitch
        # -----
        pitch = arctand(
            tand(pd0_data["Sensor"].pitch_deg) * cosd(pd0_data["Sensor"].roll_deg)
        )
        pitch_src = pd0_data["Cfg"].pitch_src[0]

        # Create pitch sensor
        self.sensors.pitch_deg.internal = SensorData()
        self.sensors.pitch_deg.internal.populate_data(
            data_in=pitch, source_in=pitch_src
        )
        self.sensors.pitch_deg.selected = "internal"

        # Roll
        # ----
        roll = pd0_data["Sensor"].roll_deg.T
        roll_src = pd0_data["Cfg"].roll_src[0]

        # Create Roll sensor
        self.sensors.roll_deg.internal = SensorData()
        self.sensors.roll_deg.internal.populate_data(data_in=roll, source_in=roll_src)
        self.sensors.roll_deg.selected = "internal"

        # Temperature
        # -----------
        temperature = pd0_data["Sensor"].temperature_deg_c.T
        temperature_src = pd0_data["Cfg"].temp_src[0]

        # Create temperature sensor
        self.sensors.temperature_deg_c.internal = SensorData()
        self.sensors.temperature_deg_c.internal.populate_data(
            data_in=temperature, source_in=temperature_src
        )
        self.sensors.temperature_deg_c.selected = "internal"

        # Salinity
        # --------
        pd0_salinity = pd0_data["Sensor"].salinity_ppt.T
        pd0_salinity_src = pd0_data["Cfg"].sal_src[0]

        # Create salinity sensor from pd0 data
        self.sensors.salinity_ppt.internal = SensorData()
        self.sensors.salinity_ppt.internal.populate_data(
            data_in=pd0_salinity, source_in=pd0_salinity_src
        )

        # Create salinity sensor from mmt data
        mmt_salinity = float(cfg["Processing"]["Salinity"])
        self.sensors.salinity_ppt.user = SensorData()
        self.sensors.salinity_ppt.user.populate_data(
            data_in=mmt_salinity, source_in="mmt"
        )

        # Set selected salinity
        self.sensors.salinity_ppt.selected = "internal"

        # Speed of Sound
        # --------------
        speed_of_sound = pd0_data["Sensor"].sos_mps.T
        speed_of_sound_src = pd0_data["Cfg"].sos_src[0]
        self.sensors.speed_of_sound_mps.internal = SensorData()
        self.sensors.speed_of_sound_mps.internal.populate_data(
            data_in=speed_of_sound, source_in=speed_of_sound_src
        )

        # Create speed of sound sensor from mmt data
        mmt_sos = float(cfg["Processing"]["Fixed_Speed_Of_Sound"])
        self.sensors.speed_of_sound_mps.user = SensorData()
        self.sensors.speed_of_sound_mps.user.populate_data(
            data_in=mmt_sos, source_in="mmt"
        )

        # The raw data are referenced to the internal SOS
        self.sensors.speed_of_sound_mps.selected = "internal"

        # Battery voltage
        self.sensors.battery_voltage.internal = SensorData()

        # Determine TRDI model
        num = float(pd0_data["Inst"].firm_ver[0])
        model_switch = np.floor(num)

        # Rio Grande voltage does not represent battery voltage and is set to nan
        if model_switch == 10:
            scale_factor = np.nan
        else:
            scale_factor = 0.1

        self.sensors.battery_voltage.internal.populate_data(
            data_in=pd0_data["Sensor"].xmit_voltage * scale_factor, source_in="internal"
        )

    @staticmethod
    def trdi_ping_type(pd0_data):
        """Determines if the ping is coherent on incoherent based on the lag near bottom.
        A coherent ping will have the lag near the bottom.

        Parameters
        ----------
        pd0_data: Pd0TRDI
            Raw data from pd0 file.

        Returns
        -------
        ping_type = np.array(str)
            Ping_type for each ensemble, C - coherent, I - incoherent
        """

        ping_type = np.array([])

        firmware = str(pd0_data["Inst"].firm_ver[0])
        # RiverRay, RiverPro, and RioPro
        if (firmware[:2] == "44") or (firmware[:2] == "56"):
            if hasattr(pd0_data["Cfg"], "lag_near_bottom"):
                ping_temp = pd0_data["Cfg"].lag_near_bottom > 0
                ping_type = np.tile(["U"], ping_temp.shape)
                ping_type[ping_temp == 0] = "I"
                ping_type[ping_temp == 1] = "C"

        # StreamPro
        elif firmware[:2] == "31":
            if pd0_data["Cfg"].wm[0] == 12:
                ping_type = np.tile(["I"], pd0_data["Wt"].vel_mps.shape[2])
            elif pd0_data["Cfg"].wm[0] == 13:
                ping_type = np.tile(["C"], pd0_data["Wt"].vel_mps.shape[2])
            else:
                ping_type = np.tile(["U"], pd0_data["Wt"].vel_mps.shape[2])

        # Rio Grande
        elif firmware[:2] == "10":
            if pd0_data["Cfg"].wm[0] == 1 or pd0_data["Cfg"].wm[0] == 12:
                ping_type = np.tile(["I"], pd0_data["Wt"].vel_mps.shape[2])
            elif pd0_data["Cfg"].wm[0] == 5 or pd0_data["Cfg"].wm[0] == 8:
                ping_type = np.tile(["C"], pd0_data["Wt"].vel_mps.shape[2])
            else:
                ping_type = np.tile(["U"], pd0_data["Wt"].vel_mps.shape[2])
        else:
            ping_type = np.tile(["U"], pd0_data["Wt"].vel_mps.shape[2])
        return ping_type

    @staticmethod
    def trdi_compute_cell_data(pd0):
        """Computes depth cell properties.

        Parameters
        ----------
        pd0: Pd0TRDI
            Raw data from pd0 file.

        returns
        -------
        cell_size_all: np.array(float)
            Array of cell sizes
        cell_depth: np.array(float)
            Array of cell depths to cell centerline
        sl_cutoff_per: float
            Percent to cutoff at bottom
        sl_lag_effect_m: np.array(float)
             The extra depth below the last depth cell that must be above the side lobe cutoff, in m.
        """

        # Number of ensembles
        num_ens = np.array(pd0["Wt"].vel_mps).shape[-1]

        # Retrieve and compute cell information
        reg_cell_size = pd0["Cfg"].ws_cm / 100
        reg_cell_size[reg_cell_size == 0] = np.nan
        dist_cell_1_m = pd0["Cfg"].dist_bin1_cm / 100
        num_reg_cells = pd0["Wt"].vel_mps.shape[1]

        # Surf data are to accommodate RiverRay and RiverPro.  pd0_read sets these
        # values to nan when reading Rio Grande or StreamPro data
        no_surf_cells = pd0["Surface"].no_cells
        no_surf_cells[np.isnan(no_surf_cells)] = 0
        max_surf_cells = np.nanmax(no_surf_cells)
        surf_cell_size = pd0["Surface"].cell_size_cm / 100
        surf_cell_dist = pd0["Surface"].dist_bin1_cm / 100

        # Compute maximum number of cells
        max_cells = int(max_surf_cells + num_reg_cells)

        # Combine cell size and cell range from transducer for both
        # surface and regular cells
        cell_depth = np.tile(np.nan, (max_cells, num_ens))
        cell_size_all = np.tile(np.nan, (max_cells, num_ens))
        for i in range(num_ens):
            # Determine number of cells to be treated as regular cells
            if np.nanmax(no_surf_cells) > 0:
                num_reg_cells = max_cells - no_surf_cells[i]
            else:
                num_reg_cells = max_cells

            # Compute cell depth
            if no_surf_cells[i] > 1e-5:
                cell_depth[: int(no_surf_cells[i]), i] = surf_cell_dist[i] + np.arange(
                    0,
                    (no_surf_cells[i] - 1) * surf_cell_size[i] + 0.001,
                    surf_cell_size[i],
                )
                cell_depth[int(no_surf_cells[i]) :, i] = (
                    cell_depth[int(no_surf_cells[i] - 1), i]
                    + (0.5 * surf_cell_size[i] + 0.5 * reg_cell_size[i])
                    + np.arange(
                        0,
                        (num_reg_cells - 1) * reg_cell_size[i] + 0.001,
                        reg_cell_size[i],
                    )
                )
                cell_size_all[0 : int(no_surf_cells[i]), i] = np.repeat(
                    surf_cell_size[i], int(no_surf_cells[i])
                )
                cell_size_all[int(no_surf_cells[i]) :, i] = np.repeat(
                    reg_cell_size[i], int(num_reg_cells)
                )
            else:
                cell_depth[: int(num_reg_cells), i] = (
                    dist_cell_1_m[i]
                    + np.linspace(0, int(num_reg_cells) - 1, int(num_reg_cells))
                    * reg_cell_size[i]
                )
                cell_size_all[:, i] = np.repeat(reg_cell_size[i], num_reg_cells)

        # Firmware is used to ID RiverRay data with variable modes and lags
        firmware = str(pd0["Inst"].firm_ver[0])

        # Compute sl_lag_effect
        lag = pd0["Cfg"].lag_cm / 100
        if firmware[0:2] == "44" or firmware[0:2] == "56":
            lag_near_bottom = np.array(pd0["Cfg"].lag_near_bottom)
            lag_near_bottom[lag_near_bottom == np.nan] = 0
            lag[lag_near_bottom != 0] = 0

        pulse_len = pd0["Cfg"].xmit_pulse_cm / 100
        sl_lag_effect_m = (lag + pulse_len + reg_cell_size) / 2
        sl_cutoff_per = (1 - (cosd(pd0["Inst"].beam_ang[0]))) * 100

        return cell_size_all, cell_depth, sl_cutoff_per, sl_lag_effect_m

    @staticmethod
    def valid_frequencies(frequency_in):
        """Create frequency time series for BT and WT with all valid frequencies.

        Parameters
        ----------
        frequency_in: nd.array()
            Frequency time series from raw data

        Returns
        -------
        frequency_out: nd.array()
            Frequency times series with np.nan filled with valid frequencies
        """

        # Initialize output
        frequency_out = np.copy(frequency_in)

        # Check for any invalid data
        invalid_freq = np.isnan(frequency_in)
        if np.any(invalid_freq):
            # Identify the first valid frequency
            valid = frequency_in[np.logical_not(invalid_freq)][0]
            # Forward fill for invalid frequencies beyond first valid, backfill until 1st valid
            for n in range(frequency_in.size):
                if invalid_freq[n]:
                    frequency_out[n] = valid
                else:
                    valid = frequency_in[n]

        return frequency_out

    @staticmethod
    def side_lobe_cutoff(depths, sl_lag_effect, slc_type="Percent", value=None):
        """Computes side lobe cutoff.

        The side lobe cutoff is based on shallowest beam from the slant beams adjusted
        for beam angle and the vertical beam if present. The side lobe cutoff is computed
        to ensure that the bin and any lag beyond the actual bin cutoff is
        above the side lobe cutoff.

        Parameters
        ----------
        depths: DepthStructure
            Object of DepthStructure class
        sl_lag_effect: np.array(float)
            The extra depth below the last depth cell that must be above the side lobe cutoff, in m.
        slc_type: str
            Method used for side lobe cutoff computation.
        value: float
            Value used in specified method to use for side lobe cutoff computation.

        Returns
        -------
        cells_above_sl: np.array(bool)
            Logical array of depth cells above the sidelobe cutoff based on BT
        cutoff: np.array(float)
            Bottom cutoff distance for depth cell centers measured from the water surface, in m.
        """

        # Compute minimum depths for each ensemble
        if slc_type == "Cells":
            selected_depth = getattr(depths, depths.selected)
            depth_values = selected_depth.depth_processed_m
            cell_size = np.nanmean(selected_depth.depth_cell_size_m)
            cell_depth = selected_depth.depth_cell_depth_m
            cutoff = depth_values - ((cell_size * value) + (cell_size * 0.5))

        else:
            # Adjust for transducer angle
            coeff = None
            if slc_type == "Percent":
                coeff = value
            elif slc_type == "Angle":
                coeff = np.cos(np.deg2rad(value))
            depth_array = depths.bt_depths.depth_orig_m

            if depths.vb_depths is not None:
                if depth_array.shape[0] > 0:
                    depth_array = np.vstack(
                        (depth_array, depths.vb_depths.depth_orig_m)
                    )

            min_depths = np.nanmin(depth_array, 0)
            draft = depths.bt_depths.draft_orig_m
            cell_depth = depths.bt_depths.depth_cell_depth_m

            # Compute range from transducer
            range_from_xducer = min_depths - draft

            # Compute sidelobe cutoff to centerline
            cutoff = np.array(range_from_xducer * coeff - sl_lag_effect + draft)

        # Compute boolean side lobe cutoff matrix
        cells_above_sl = nan_less(cell_depth, cutoff)

        return cells_above_sl, cutoff

    @staticmethod
    def adjust_side_lobe(
        depths, cells_above_sl_bt, sl_lag_effect_m, slc_type="Percent", value=None
    ):
        """Adjust the side lobe cutoff for vertical beam and interpolated depths.

        Parameters
        ----------
        depths: np.array
            Bottom track (all 4 beams) and vertical beam depths for each ensemble, in m.
        cells_above_sl_bt: np.array(bool)
            Logical array of depth cells above the sidelobe cutoff based on BT
        sl_lag_effect_m: np.array(float)
            The extra depth below the last depth cell that must be above the side lobe cutoff, in m.
        slc_type: str
            Method used for side lobe cutoff computation.
        value: float
            Value used in specified method to use for side lobe cutoff computation.
        """

        selected = depths.selected
        depth_selected = getattr(depths, depths.selected)
        cells_above_slbt = np.copy(cells_above_sl_bt)

        # Adjust for transducer angle
        coeff = None
        if slc_type == "Percent":
            coeff = value
        elif slc_type == "Angle":
            coeff = np.cos(np.deg2rad(value))

        # Compute cutoff for vertical beam depths
        if selected == "vb_depths":
            sl_cutoff_vb = (
                (depth_selected.depth_processed_m - depth_selected.draft_use_m) * coeff
                - sl_lag_effect_m
                + depth_selected.draft_use_m
            )
            cells_above_slvb = np.round(
                depth_selected.depth_cell_depth_m, 2
            ) <= np.round(sl_cutoff_vb, 2)
            idx = np.where(np.logical_not(depths.bt_depths.valid_data))
            cells_above_slbt[:, idx] = cells_above_slvb[:, idx]
            cells_above_sl = np.logical_and(cells_above_slbt, cells_above_slvb)
        else:
            cells_above_sl = cells_above_slbt

        # Compute cutoff from interpolated depths
        n_valid_beams = np.nansum(depth_selected.valid_beams, 0)

        # Find ensembles with no valid beam depths
        idx = np.where(depth_selected.depth_source_ens == "INT")[0]

        # Determine side lobe cutoff for ensembles with no valid beam depths
        if len(idx) > 0:
            if len(sl_lag_effect_m) > 1:
                sl_lag = sl_lag_effect_m[idx]
            else:
                sl_lag = sl_lag_effect_m

            sl_cutoff_int = (
                (depth_selected.depth_processed_m[idx] - depth_selected.draft_use_m)
                * coeff
                - sl_lag
                + depth_selected.draft_use_m
            )
            for i in range(len(idx)):
                cells_above_sl[:, idx[i]] = np.less(
                    depth_selected.depth_cell_depth_m[:, idx[i]], sl_cutoff_int[i]
                )

        # Find ensembles with at least 1 invalid beam depth
        if depth_selected.valid_beams.shape[1] > 1:
            idx = np.where(np.logical_and(n_valid_beams < 4, n_valid_beams > 0))[0]
            if len(idx) > 0:
                if len(sl_lag_effect_m) > 1:
                    sl_lag = sl_lag_effect_m[idx]
                else:
                    sl_lag = sl_lag_effect_m

                sl_cutoff_int = (
                    (depth_selected.depth_processed_m[idx] - depth_selected.draft_use_m)
                    * coeff
                    - sl_lag
                    + depth_selected.draft_use_m
                )
                cells_above_sl_int = np.tile(True, cells_above_sl.shape)

                for i in range(len(idx)):
                    cells_above_sl_int[:, idx[i]] = np.less(
                        depth_selected.depth_cell_depth_m[:, idx[i]], sl_cutoff_int[i]
                    )

                cells_above_sl[cells_above_sl_int == 0] = 0

        return cells_above_sl

    # matst
    # =====
    def from_matst(
        self,
        sontek_data,
        stations_item,
        data_indices,
        cell_start,
        excluded,
        snr_3beam_comp,
        date_format,
    ):
        """Coordinates assignment of data from RSSL Matlab output to object classes.

        Parameters
        ----------
        sontek_data: dict
            Dictionary of Matlab structures
        stations_item: mat_struct
            Station information
        data_indices: np.array(int)
            Array of indices of raw data for this vertical
        cell_start: float
            Top of 1st cell including draft
        excluded: dict
            Dictionary of excluded top distances for selected ADCPs
        snr_3beam_comp: bool
            Indicates if invalid data due to SNR filter should be computed using 3-beam
            solution
        date_format: str
            String defining date format
        """

        # ADCP instrument information
        self.inst = InstrumentData()
        self.inst.populate_data(manufacturer="matst", raw_data=sontek_data)

        # Date and time
        self.matst_date_time(sontek_data, data_indices, date_format)

        # Sensors
        self.matst_sensors(sontek_data, data_indices)

        # Boat velocity
        self.matst_boat(sontek_data, data_indices)

        # GPS data
        self.matst_gps(sontek_data, data_indices)

        # Depths
        self.matst_depths(sontek_data, data_indices, stations_item, cell_start)

        # Change coordinate system
        self.boat_vel.bt_vel.change_coord_sys("Ship", self.sensors, self.inst)

        # Water track
        self.matst_wt(
            sontek_data, data_indices, stations_item, excluded, snr_3beam_comp
        )

        # Extrap
        self.matst_extrap(stations_item)

        self.in_transect_idx = np.arange(self.w_vel.cells_above_sl.shape[1])

    def matst_date_time(self, sontek_data, data_indices, date_format):
        """Create date_time object.

        Parameters
        ----------
        sontek_data: dict
            Dictionary of Matlab structures
        data_indices: np.array(int)
            Array of indices of raw data for this vertical
        date_format: str
            String defining date format
        """

        # Get ensembles time for this vertical
        ensemble_times = sontek_data["RawSystem"].Time[data_indices]

        # Compute delta time
        ensemble_delta_time = np.append([0], np.diff(ensemble_times))

        # First ensemble counts for averaging velocity
        # Assume first ensemble time is equal to second
        ensemble_delta_time[0] = ensemble_delta_time[1]

        # Compute serial times
        start_serial_time = ensemble_times[0] + ((30 * 365) + 7) * 24 * 60 * 60
        end_serial_time = ensemble_times[-1] + (((30 * 365) + 7) * 24 * 60 * 60) + 1

        # Create object
        meas_date = datetime.strftime(
            datetime.fromtimestamp(start_serial_time), date_format
        )
        self.date_time = DateTime()
        self.date_time.populate_data(
            date_in=meas_date,
            start_in=start_serial_time,
            end_in=end_serial_time,
            ens_dur_in=ensemble_delta_time,
        )

    def matst_boat(self, sontek_data, data_indices):
        """Create boat_vel object.

        Parameters
        ----------
        sontek_data: dict
            Dictionary of Matlab structures
        data_indices: np.array(int)
            Array of indices of raw data for this vertical
        """

        # Create object
        self.boat_vel = BoatStructure()

        # Get data for this vertical
        vel_in = np.swapaxes(sontek_data["RawBottomTrack"].BT_Vel[data_indices], 0, 1)

        vel_in[np.round(vel_in, 3) == 32.767] = np.nan

        freq_in = sontek_data["RawBottomTrack"].BT_Frequency[data_indices]

        coord_sys_in = "Beam"

        # Add boat object
        self.boat_vel.add_boat_object(
            source="SonTek",
            vel_in=vel_in,
            freq_in=freq_in,
            coord_sys_in=coord_sys_in,
            nav_ref_in="BT",
        )

        # Set selected
        if sontek_data["Setup"].trackReference == 0:
            self.boat_vel.selected = "None"
        else:
            self.boat_vel.selected = "bt_vel"

    def matst_gps(self, sontek_data, data_indices):
        """Create gps object.

        Parameters
        ----------
        sontek_data: dict
            Dictionary of Matlab structures
        data_indices: np.array(int)
            Array of indices of raw data for this vertical
        """

        # Get data for this vertical
        gga_altitude = sontek_data["RawGPS"].Altitude[data_indices]
        gga_diff = sontek_data["RawGPS"].GPS_Quality[data_indices]
        gga_hdop = sontek_data["RawGPS"].HDOP[data_indices]
        gga_latitude = sontek_data["RawGPS"].Latitude[data_indices]
        gga_longitude = sontek_data["RawGPS"].Longitude[data_indices]
        gga_num_sats = sontek_data["RawGPS"].Satellites[data_indices]
        gga_utc = sontek_data["RawGPS"].Utc[data_indices]

        # Create object
        self.gps = GPSData()
        self.gps.populate_data(
            raw_gga_utc=None,
            raw_gga_lat=None,
            raw_gga_lon=None,
            raw_gga_alt=None,
            raw_gga_diff=None,
            raw_gga_hdop=None,
            raw_gga_num_sats=None,
            raw_gga_delta_time=None,
            raw_vtg_course=None,
            raw_vtg_speed=None,
            raw_vtg_delta_time=None,
            raw_vtg_mode_indicator=None,
            ext_gga_utc=gga_utc,
            ext_gga_lat=gga_latitude,
            ext_gga_lon=gga_longitude,
            ext_gga_alt=gga_altitude,
            ext_gga_diff=gga_diff,
            ext_gga_hdop=gga_hdop,
            ext_gga_num_sats=gga_num_sats,
            ext_vtg_course=None,
            ext_vtg_speed=None,
            gga_p_method="External",
            gga_v_method="External",
            vtg_method="External",
        )

    def matst_depths(self, sontek_data, data_indices, stations_item, cell_start):
        """Create depths object.

        Parameters
        ----------
        sontek_data: dict
            Dictionary of Matlab structures
        data_indices: np.array(int)
            Array of indices of raw data for this vertical
        stations_item: mat_struct
            Station information
        cell_start: float
            Top of 1st cell including draft
        """

        # Initialize depth data structure
        self.depths = DepthStructure()

        # Determine array rows and cols
        max_cells = sontek_data["RawWaterTrack"].Velocity.shape[0]
        num_ens = data_indices.shape[0]

        # Get draft
        draft = stations_item.SensorDepth / 10000.0

        # Compute cell sizes
        # TODO the cell start is wrong in the Matlab file
        cell_size = sontek_data["RawSystem"].Cell_Size[data_indices].reshape(1, num_ens)
        cell_size_all = np.tile(cell_size, (max_cells, 1))

        # Cell start in RawSystem of Matlab file is wrong with compute as
        # The cell start in StationSystem of the RSSL Matlab file only applies
        # to the first ensemble. The following code computes a correction
        # that is applied to RawSystem cell start for situations where the
        # frequency or water mode changed during the vertical. This allows
        # use of all data, rather than only using data that match the first
        # ensemble, which is what RSSL does.

        rawtops = np.unique(sontek_data["RawSystem"].Cell_Start[data_indices])
        if len(rawtops) > 1:
            top_correction = (
                sontek_data["RawSystem"].Cell_Start[data_indices[0]] - cell_start
            )
            top_of_cells = (
                sontek_data["RawSystem"].Cell_Start[data_indices] - top_correction
            )
        else:
            top_of_cells = np.tile(cell_start, num_ens)

        # Prepare bottom track depth variable
        depth = sontek_data["RawBottomTrack"].BT_Beam_Depth[data_indices].T
        freq = sontek_data["RawBottomTrack"].BT_Frequency[data_indices]

        # Zero depths are not valid
        depth[depth == 0] = np.nan

        # Add draft
        depth = depth + draft

        # Compute cell depth
        cell_depth = (
            (
                np.tile(
                    np.arange(1, max_cells + 1, 1).reshape(max_cells, 1), (1, num_ens)
                )
                - 0.5
            )
            * cell_size_all
        ) + np.tile(top_of_cells, (max_cells, 1))

        # Create depth object for bottom track beams
        self.depths.add_depth_object(
            depth_in=depth,
            source_in="BT",
            freq_in=freq,
            draft_in=draft,
            cell_depth_in=cell_depth,
            cell_size_in=cell_size_all,
        )

        # Prepare vertical beam depth variable
        depth_vb = np.tile(np.nan, (1, cell_depth.shape[1]))

        # Retrieve raw data
        depth_vb[0, :] = sontek_data["RawBottomTrack"].VB_Depth[data_indices]

        # Zero depths are not valid
        depth_vb[depth_vb == 0] = np.nan

        # Add draft
        depth_vb = depth_vb + draft

        freq = np.array(
            [sontek_data["Transformation_Matrices"].Frequency[1]] * depth.shape[-1]
        )

        # Create depth object for vertical beam
        self.depths.add_depth_object(
            depth_in=depth_vb,
            source_in="VB",
            freq_in=freq,
            draft_in=draft,
            cell_depth_in=cell_depth,
            cell_size_in=cell_size_all,
        )

        # Manual depth
        depth_in = np.array(
            [stations_item.UserWaterDepthInTNTHMM / 10000.0] * depth.shape[-1]
        ).reshape(1, depth.shape[-1])
        if np.nansum(depth_in) > 0:
            self.depths.add_depth_object(
                depth_in=depth_in,
                source_in="MAN",
                freq_in=np.array([np.nan] * depth.shape[-1]),
                draft_in=draft,
                cell_depth_in=cell_depth,
                cell_size_in=cell_size_all,
            )

        # Set depth reference
        if stations_item.UserWaterDepthInTNTHMM > 0.01:
            self.depths.selected = "man_depths"
            self.depths.composite_depths(transect=self, setting="Off")

        elif sontek_data["Setup"].depthReference < 0.5:
            self.depths.selected = "vb_depths"
            self.depths.composite_depths(transect=self, setting="On")

        else:
            self.depths.selected = "bt_depths"
            self.depths.composite_depths(transect=self, setting="On")

    def matst_sensors(self, sontek_data, data_indices):
        """Create sensors object.

        Parameters
        ----------
        sontek_data: dict
            Dictionary of Matlab structures
        data_indices: np.array(int)
            Array of indices of raw data for this vertical
        """

        # Sensor data
        # -----------
        self.sensors = Sensors()

        # Internal heading
        self.sensors.heading_deg.internal = HeadingData()

        # Check for firmware supporting G3 compass and associated data
        if "RawCompass" in sontek_data:
            # Retrieve data
            mag_error = sontek_data["RawCompass"].Magnetic_error[data_indices, 0]
            pitch_limit = np.array(
                (
                    sontek_data["RawCompass"].Maximum_Pitch[data_indices],
                    sontek_data["RawCompass"].Minimum_Pitch[data_indices],
                )
            ).T
            roll_limit = np.array(
                (
                    sontek_data["RawCompass"].Maximum_Roll[data_indices],
                    sontek_data["RawCompass"].Minimum_Roll[data_indices],
                )
            ).T
            heading = sontek_data["RawSystem"].Heading[data_indices]
            pitch = sontek_data["RawCompass"].Pitch[data_indices, 0]
            roll = sontek_data["RawCompass"].Roll[data_indices, 0]

            if np.any(np.greater_equal(np.abs(pitch_limit), 90)) or np.any(
                np.greater_equal(np.abs(roll_limit), 90)
            ):
                pitch_limit = None
                roll_limit = None
        else:
            heading = sontek_data["RawSystem"].Heading[data_indices]
            mag_error = None
            pitch_limit = None
            roll_limit = None
            pitch = None
            roll = None

        # Heading
        self.sensors.heading_deg.internal.populate_data(
            data_in=heading,
            source_in="internal",
            magvar=sontek_data["Setup"].magneticDeclination,
            mag_error=mag_error,
            pitch_limit=pitch_limit,
            roll_limit=roll_limit,
        )

        self.sensors.heading_deg.selected = "internal"

        # Pitch and roll
        self.sensors.pitch_deg.internal = SensorData()
        self.sensors.pitch_deg.internal.populate_data(
            data_in=pitch, source_in="internal"
        )
        self.sensors.pitch_deg.selected = "internal"
        self.sensors.roll_deg.internal = SensorData()
        self.sensors.roll_deg.internal.populate_data(data_in=roll, source_in="internal")
        self.sensors.roll_deg.selected = "internal"

        # Temperature
        temperature = sontek_data["RawSystem"].Temperature[data_indices]

        # Store measured temperature in degrees C
        if sontek_data["RawSystem"].Units.Temperature.find("C") == -1:
            temperature = (5.0 / 9.0) * (temperature - 32)
        self.sensors.temperature_deg_c.internal = SensorData()
        self.sensors.temperature_deg_c.internal.populate_data(
            data_in=temperature, source_in="internal"
        )

        # Store user temperature in degrees C
        temp = sontek_data["Setup"].userTemperature
        if sontek_data["Setup"].Units.userTemperature.find("C") == -1:
            temp = (5.0 / 9.0) * (temp - 32)
        user_temp = np.tile(temp, len(temperature))
        self.sensors.temperature_deg_c.user = SensorData()
        self.sensors.temperature_deg_c.user.populate_data(
            data_in=user_temp, source_in="Manual"
        )

        # Selected temperature source
        if sontek_data["Setup"].useMeasuredTemperature == 1:
            self.sensors.temperature_deg_c.selected = "internal"
        else:
            self.sensors.temperature_deg_c.selected = "user"

        # Salinity
        # Initial salinity is saved in both internal and user with initial selection
        # internal to support QA check for changes
        self.sensors.salinity_ppt.internal = SensorData()
        self.sensors.salinity_ppt.internal.populate_data(
            data_in=sontek_data["Setup"].userSalinity, source_in="Manual"
        )
        self.sensors.salinity_ppt.user = SensorData()
        self.sensors.salinity_ppt.user.populate_data(
            data_in=sontek_data["Setup"].userSalinity, source_in="Manual"
        )
        self.sensors.salinity_ppt.selected = "internal"

        # Computed speed of sound
        speed_of_sound = Sensors.speed_of_sound(
            temperature=temperature, salinity=sontek_data["Setup"].userSalinity
        )
        self.sensors.speed_of_sound_mps.internal = SensorData()
        self.sensors.speed_of_sound_mps.internal.populate_data(
            data_in=speed_of_sound, source_in="QRev"
        )

        # User speed of sound
        if hasattr(sontek_data["Setup"], "useFixedSoundSpeed"):
            user_sos = np.tile(
                sontek_data["Setup"].fixedSoundSpeed, len(speed_of_sound)
            )
            self.sensors.speed_of_sound_mps.user = SensorData()
            self.sensors.speed_of_sound_mps.user.populate_data(
                data_in=user_sos, source_in="Manual"
            )

            # Set selected speed of sound
            if sontek_data["Setup"].useFixedSoundSpeed == 0:
                self.sensors.speed_of_sound_mps.selected = "internal"
            else:
                self.sensors.speed_of_sound_mps.selected = "user"

        else:
            self.sensors.speed_of_sound_mps.selected = "internal"

        # Battery voltage
        self.sensors.battery_voltage.internal = SensorData()
        self.sensors.battery_voltage.internal.populate_data(
            data_in=sontek_data["RawSystem"].Voltage[data_indices],
            source_in="internal",
        )

    def matst_extrap(self, stations_item):
        """Create extrap object.

        Parameters
        ----------
        stations_item: mat_struct
            Station information
        """

        ice_exp = np.nan
        top = "Power"

        # Top method
        if stations_item.SurfaceType == 1:
            top = "Ice"
            ice_exp = stations_item.TopExponent
        elif stations_item.TopFitType == 0:
            top = "Constant"

        # SonTek allows Power at the top and no slip or power at the bottom with top and bottom having different
        # exponents. QRev does not support different exponents for open water situations.
        if top == "Power":
            bot = "Power"
            exp = stations_item.TopExponent
        else:
            bot = "No Slip"
            exp = stations_item.BottomExponent

        # Create extrap object
        self.extrap = Extrapolation()
        self.extrap.populate_data(top=top, bot=bot, exp=exp, ice_exp=ice_exp)

    def matst_wt(
        self, sontek_data, data_indices, stations_item, excluded, snr_3beam_comp
    ):
        """Create w_vel object.

        Parameters
        ----------
        sontek_data: dict
            Dictionary of Matlab structures
        data_indices: np.array(int)
            Array of indices of raw data for this vertical
        stations_item: mat_struct
            Station information
        excluded: dict
            Dictionary of excluded top distances for selected ADCPs
        snr_3beam_comp: bool
            Indicates if invalid data due to SNR filter should be computed using 3-beam
            solution
        """

        # Create valid frequency time series
        freq_ts = sontek_data["RawWaterTrack"].WT_Frequency[data_indices]

        # Rearrange arrays for consistency with WaterData class
        vel = np.swapaxes(
            sontek_data["RawWaterTrack"].Velocity[:, :, data_indices], 1, 0
        )
        snr = np.swapaxes(sontek_data["RawSystem"].SNR[:, :, data_indices], 1, 0)
        if hasattr(sontek_data["RawWaterTrack"], "Correlation"):
            corr = np.swapaxes(
                sontek_data["RawWaterTrack"].Correlation[:, :, data_indices], 1, 0
            )
        else:
            corr = np.array([])

        # Correct SonTek difference velocity for error in earlier transformation matrices.
        if abs(sontek_data["Transformation_Matrices"].Matrix[3, 0, 0]) < 0.5:
            vel[3, :, :] = vel[3, :, :] * 2

        # Apply TRDI scaling to SonTek difference velocity to convert to a TRDI compatible error velocity
        vel[3, :, :] = vel[3, :, :] / ((2**0.5) * np.tan(np.deg2rad(25)))

        # Coordinate system
        ref_coord = "Earth"
        if stations_item.CoordinateSystem == 0:
            # Rotate coordinates to standard ship coordinates
            vel[0, :, :], vel[1, :, :] = rotate_coordinates(
                vel[0, :, :], vel[1, :, :], 270
            )
            ref_coord = "Ship"

        ref_water = "None"

        # Transmit Length information is not availalbe in Matlab output, so it is assumed to be equal
        # to 1/2 depth_cell_size_m. The percent method is used for the side lobe cutoff computation.
        # Note: Sontek does not use the vertical beam for sidelobe cutoff but QRevIntMS does
        sl_cutoff_percent = stations_item.DiscardPercent
        sl_cutoff_number = stations_item.DiscardCells
        sl_lag_effect_m = np.copy(self.depths.bt_depths.depth_cell_size_m[0, :])
        sl_cutoff_type = "Percent"
        cells_above_sl, sl_cutoff_m = self.side_lobe_cutoff(
            depths=self.depths,
            sl_lag_effect=sl_lag_effect_m,
            slc_type=sl_cutoff_type,
            value=1 - sl_cutoff_percent / 100,
        )
        if stations_item.DiscardType == 0:
            cells_above_sl_2, sl_cutoff_m_2 = self.side_lobe_cutoff(
                depths=self.depths,
                sl_lag_effect=sl_lag_effect_m,
                slc_type="Cells",
                value=1 - sl_cutoff_percent / 100,
            )

            cutoff = np.vstack((sl_cutoff_m, sl_cutoff_m_2))
            sl_cutoff_m = np.nanmin(cutoff, axis=0)
            depth_selected = getattr(self.depths, self.depths.selected)
            cells_above_sl = nan_less(
                np.round(depth_selected.depth_cell_depth_m, 3), np.round(sl_cutoff_m, 3)
            )

        # Determine water mode
        if len(corr) > 0:
            corr_nan = np.isnan(corr)
            number_of_nan = np.count_nonzero(corr_nan)
            if number_of_nan == 0:
                wm = "HD"
            elif corr_nan.size == number_of_nan:
                wm = "IC"
            else:
                wm = "Variable"
        else:
            wm = "Unknown"

        # Determine excluded distance (Similar to SonTek's screening distance)
        excluded_bottom = 0
        excluded_bottom_type = "Distance"
        excluded_top_type = "Distance"
        excluded_top = (
            stations_item.ScreeningDistanceInTNTHMM - stations_item.SensorDepth
        ) / 10000.0

        # Set excluded distance for M9 to minimum default value
        if excluded_top < excluded["M9"] and sontek_data["SystemHW"].Frequency.F2 > 0:
            excluded_top = excluded["M9"]

        # Ping type
        ping_type = self.sontek_ping_type(corr=corr, freq=freq_ts)

        # Create water velocity object
        self.w_vel = WaterData()
        self.w_vel.populate_data(
            vel_in=vel,
            freq_in=freq_ts,
            coord_sys_in=ref_coord,
            nav_ref_in=ref_water,
            rssi_in=snr,
            rssi_units_in="SNR",
            excluded_bottom_in=excluded_bottom,
            excluded_bottom_type_in=excluded_bottom_type,
            excluded_top_in=excluded_top,
            excluded_top_type_in=excluded_top_type,
            cells_above_sl_in=cells_above_sl,
            sl_cutoff_per_in=sl_cutoff_percent,
            sl_cutoff_num_in=sl_cutoff_number,
            sl_cutoff_type_in=sl_cutoff_type,
            sl_lag_effect_in=sl_lag_effect_m,
            sl_cutoff_m=sl_cutoff_m,
            wm_in=wm,
            blank_in=excluded_top,
            corr_in=corr,
            ping_type=ping_type,
            snr_3beam_comp=snr_3beam_comp,
        )

    @staticmethod
    def sontek_ping_type(corr, freq, expected_std=None):
        """Determines ping type based on the fact that HD has correlation but incoherent does not.

        Parameters
        ----------
        corr: np.ndarray(int)
            Water track correlation
        freq:
            Frequency of ping in Hz
        expected_std: np.array(float)
            Array of expected velocity standard devations

        Returns
        -------
        ping_type: np.array(int)
            Ping_type for each ensemble, 3 - 1 MHz Incoherent, 4 - 1 MHz HD, 5 - 3 MHz Incoherent, 6 - 3 MHz HD
        """
        # Determine ping type

        if expected_std is None:
            # M9 or S5
            if corr.size > 0:
                corr_exists = np.nansum(np.nansum(corr, axis=1), axis=0)
                coherent = corr_exists > 0
            else:
                coherent = np.tile([False], freq.size)
            ping_type = []
            for n in range(len(coherent)):
                if coherent[n]:
                    if freq[n] == 3000:
                        ping_type.append("3C")
                    else:
                        ping_type.append("1C")
                else:
                    if freq[n] == 3000:
                        ping_type.append("3I")
                    else:
                        ping_type.append("1I")
            ping_type = np.array(ping_type)
        else:
            # RS5
            ves = []
            for n in range(4):
                ves.append(np.nanmean(expected_std[:, n, :], axis=0))

            ves = np.array(ves)

            ves_avg = np.nanmean(ves, axis=0)

            ping_type = np.tile(["PC/BB"], ves_avg.size)
            ping_type[ves_avg < 0.01] = "PC"
            ping_type[ves_avg > 0.025] = "BB"

        return ping_type

    @staticmethod
    def matst_xyz2enu(vel, heading):
        """If coordinates are xyz then the x velocity is in the direction of the heading. This function converts the
        velocities from xyz to ENU.

        Parameters
        ----------
        vel: np.array(float)
            Array of velocity components
        heading: np.array(float)
            Array of heading values

        Returns
        -------
        u_enu: np.array(float)
            Velocity east coordinate
        v_enu: np.array(float)
            Velocity north coordinate
        """

        u = vel[0, :, :]
        v = vel[1, :, :]

        # Convert to polar coordinates
        ang, mag = cart2pol(u, v)

        # Rotate coordinates

        vel_ang = heading + rad2azdeg(ang) - 90
        u_enu, v_enu = pol2cart(azdeg2rad(vel_ang), mag)

        return u_enu, v_enu

    # rsqst
    # =====
    def from_rsqst(
        self, adcp_data, station_data, system_configuration, snr_3beam_comp, utc_time_offset, date_format, draft
    ):
        """Coordinates assignment of data from RSQ raw data to object classes.

        Parameters
        ----------
        adcp_data: dict
            Raw data from a vertical
        station_data: dict
            Information about the vertical
        system_configuration: dict
            General measurement configuration
        snr_3beam_comp: bool
            Indicates if invalid data due to SNR filter should be computed using 3-beam
            solution
        utc_time_offset: str
            String containing time offset to get to local time.
        date_format: str
            String defining date format
        """

        # Meter
        self.inst = InstrumentData()
        self.inst.populate_data(manufacturer="rsqst", raw_data=adcp_data)

        # Extract samples for vertical from adcp_data
        (
            ens_time,
            bt,
            gps_ens,
            sensors_ens,
            vb,
            compass,
            wt,
        ) = VerticalData.rsqst_extract_samples(adcp_data)

        # Date and time
        self.rsqst_date_time(ens_time, utc_time_offset, date_format)

        # Bottom Track
        self.rsqst_boat(bt, system_configuration)

        # GPS
        self.rsqst_gps(gps_ens)

        # Depths
        self.rsqst_depths(bt, vb, wt, station_data, adcp_data, system_configuration, draft)

        # Sensors
        self.rsqst_sensors(compass, sensors_ens, adcp_data, system_configuration)

        # Water Track
        self.rsqst_wt(wt, station_data, snr_3beam_comp, draft)

        # Extrapolation
        self.rsqst_extrap(station_data)

        self.in_transect_idx = np.arange(self.w_vel.cells_above_sl.shape[1])

    @staticmethod
    def rsqst_extract_samples(adcp_data):
        """Extracts samples from raw data for a vertical.

        Parameters
        ----------
        adcp_data: dict
            Raw data from a vertical

        Returns
        -------
        ens_time: list
            List of ensemble times
        bt: dict
            Dictionary of bottom track sample data
        gps: dict
            Dictionary of gps sample data
        sensors: dict
            Dictionary of sensor sample data
        vb: dict
            Dictionary of vertical beam sample data
        compass: dict
            Dictionary of heading, pitch, roll, and magnetic error sample data
        wt: dict
            Dictionary of water track sample data
        """

        ens_time = []
        n_ensembles = len(adcp_data["Samples"])

        # Define dictionaries
        bt = {"ping_type": np.full([n_ensembles], "    "),
            "ping_count": np.full([n_ensembles], 0),
            "good_ping_count": np.full([n_ensembles], 0),
            "beam_set_id": np.full([n_ensembles], 0),
            "beam_rng": np.full([4, n_ensembles], np.nan),
            "beam_vel": np.full([4, n_ensembles], np.nan),
            "beam_vel_std": np.full([4, n_ensembles], np.nan),
            "contrast": np.full([4, n_ensembles], np.nan),
            "strength": np.full([4, n_ensembles], np.nan),
            "recovered_fraction": np.full([4, n_ensembles], np.nan),
            "frequency": np.full([n_ensembles], np.nan), }

        gps = {
            "gga_utc_time": np.full([n_ensembles], np.nan),
            "gga_latitude": np.full([n_ensembles], np.nan),
            "gga_longitude": np.full([n_ensembles], np.nan),
            "gga_quality": np.full([n_ensembles], np.nan),
            "gga_altitude": np.full([n_ensembles], np.nan),
            "gga_hdop": np.full([n_ensembles], np.nan),
            "gga_sats": np.full([n_ensembles], np.nan),
            "vtg_true_course": np.full([n_ensembles], np.nan),
            "vtg_speed": np.full([n_ensembles], np.nan),
            "vtg_mode": np.tile("", [n_ensembles])
        }

        sensors = {"temperature": [], "salinity": [], "battery": [], "sos": []}

        vb = {"rng": [], "rng_std": [], "contrast": [], "strength": []}

        compass = {
            "heading": [],
            "pitch": [],
            "roll": [],
            "heading_std": [],
            "pitch_std": [],
            "roll_std": [],
            "mag_error": [],
        }

        wt = {
            "snr": np.full([4, 128, n_ensembles], np.nan),
            "vel": np.full([4, 128, n_ensembles], np.nan),
            "vel_std": np.full([4, 128, n_ensembles], np.nan),
            "expected_std": np.full([4, 128, n_ensembles], np.nan),
            "corr": np.full([4, 128, n_ensembles], np.nan),
            "cell_size": np.full([128, n_ensembles], np.nan),
            "cell_start": np.full([n_ensembles], np.nan),
            "blanking_dist": np.full([n_ensembles], np.nan),
            "pulse_length": np.full([n_ensembles], np.nan),
            "pulse_lag": np.full([n_ensembles], np.nan),
            "code_length": np.full([n_ensembles], np.nan),
            "corr_lag": np.full([n_ensembles], np.nan),
            "freq": np.full([n_ensembles], np.nan),
            "mode": np.full([n_ensembles], "    "),
            "ping_count": np.full([n_ensembles], np.nan)
        }

        # Extract data from each sample and assign to appropriate dictionary and key
        for sample_n, sample in enumerate(adcp_data["Samples"]):
            # Ensemble times
            ens_time.append(sample["SampleTime"][0:-2])

            # Bottom Track
            bt["ping_count"][sample_n] = sample["Bt"]["PingCount"]
            bt["good_ping_count"][sample_n] = sample["Bt"]["GoodPingCount"]
            bt["beam_set_id"][sample_n] = sample["Bt"]["BeamSetId"]
            bt["beam_rng"][:, sample_n] = sample["Bt"]["Range (m)"]
            bt["beam_vel"][:, sample_n] = sample["Bt"]["Velocity (m/s)"]
            bt["beam_vel_std"][:, sample_n] = sample["Bt"]["VelocityStdDev (m/s)"]
            bt["contrast"][:, sample_n] = sample["Bt"]["Contrast (dB)"]
            bt["strength"][:, sample_n] = sample["Bt"]["Strength (dB)"]
            bt["recovered_fraction"][:, sample_n] = sample["Bt"]["RecoveredFraction"]
            freq = (
                adcp_data["SensorConfiguration"]["Info"]["beamSetInfo"][
                    str(sample["Bt"]["BeamSetId"])
                ]["systemFrequency (Hz)"]
            ) / 1000
            bt["frequency"][sample_n] = freq

            # GPS
            if len(sample["GpsRecords"]) > 0:


                tm_split = sample["GpsRecords"][-1]["GgaSatelliteTime"].split(":")
                utc = float(tm_split[0]) * 60 * 60 + float(
                    tm_split[1]) * 60 + float(tm_split[2])
                gps["gga_utc_time"][sample_n] = utc
                gps["gga_latitude"][sample_n] = sample["GpsRecords"][-1]["GgaLatitude"]
                gps["gga_longitude"][sample_n] = sample["GpsRecords"][-1]["GgaLongitude"]
                gps["gga_quality"][sample_n] = sample["GpsRecords"][-1]["GgaFixQuality"]
                gps["gga_altitude"][sample_n] = sample["GpsRecords"][-1]["GgaAltitude (m)"]
                gps["gga_hdop"][sample_n] = sample["Gga"]["Hdop"]
                gps["vtg_true_course"][sample_n] = sample["GpsRecords"][-1]["VtgTmgTrue (deg)"]
                gps["vtg_speed"][sample_n] = sample["GpsRecords"][-1]["VtgSpeed (m/s)"]
                gps["gga_sats"][sample_n] = sample["Gga"]["SatelliteCount"]

            # Sensors
            sensors["temperature"].append(sample["Sensors"]["Temperature (C)"])
            sensors["salinity"].append(sample["Sensors"]["Salinity (PSS-78)"])
            sensors["battery"].append(sample["Sensors"]["BatteryVoltage (V)"])
            sensors["sos"].append(sample["Sensors"]["SoundSpeed (m/s)"])

            # Vertical beam
            vb["rng"].append(sample["Vb"]["Range (m)"])
            vb["rng_std"].append(sample["Vb"]["RangeStdDev (m)"])
            vb["contrast"].append(sample["Vb"]["Contrast (dB)"])
            vb["strength"].append(sample["Vb"]["Strength (dB)"])

            # Compass
            compass["heading"].append(sample["Compass"]["Heading (deg)"])
            compass["pitch"].append(sample["Compass"]["Pitch (deg)"])
            compass["roll"].append(sample["Compass"]["Roll (deg)"])
            compass["heading_std"].append(sample["Compass"]["HeadingStdDev (deg)"])
            compass["pitch_std"].append(sample["Compass"]["PitchStdDev (deg)"])
            compass["roll_std"].append(sample["Compass"]["RollStdDev (deg)"])
            compass["mag_error"].append(sample["Compass"]["MagneticError"])

            # Water Track
            for beam_n, beam in enumerate(sample["ProfileBeams"]):
                n_cells = len(beam["CellVelocity (m/s)"])
                if n_cells > 0:
                    wt["snr"][beam_n, 0:n_cells, sample_n] = beam["CellSnr (dB)"]
                    wt["vel"][beam_n, 0:n_cells, sample_n] = beam["CellVelocity (m/s)"]
                    wt["vel_std"][beam_n, 0:n_cells, sample_n] = beam[
                        "CellVelocityStdDev (m/s)"]
                    wt["expected_std"][beam_n, 0:n_cells, sample_n] = beam[
                        "CellVelocityExpectedStdDev (m/s)"]
                    wt["corr"][beam_n, 0:n_cells, sample_n] = beam["CellCorrelationScore"]

            wt["cell_size"][0:n_cells, sample_n] = [sample["Adp"][
                                                        "CellSize (m)"]] * n_cells
            wt["cell_start"][sample_n] = sample["Adp"]["CellStart (m)"]
            wt["blanking_dist"][sample_n] = sample["Adp"]["BlankingDistance (m)"]
            wt["pulse_length"][sample_n] = sample["Adp"]["PulseLength (m)"]
            wt["pulse_lag"][sample_n] = sample["Adp"]["PulseLag (m)"]
            wt["corr_lag"][sample_n] = sample["Adp"]["CorrelationLag (m)"]
            wt["code_length"][sample_n] = sample["Adp"]["CodeLength"]
            freq = (
                adcp_data["SensorConfiguration"]["Info"]["beamSetInfo"][
                    str(sample["Adp"]["BeamSetId"])
                ]["systemFrequency (Hz)"]
            ) / 1000
            wt["freq"][sample_n] = freq
            wt["mode"][sample_n] = sample["Adp"]["ProfileType"]
            wt["ping_count"][sample_n] = sample["Adp"]["PingCount"]

        return ens_time, bt, gps, sensors, vb, compass, wt

    def rsqst_date_time(self, ens_time, utc_time_offset, date_format):
        """Create date_time object.

        Parameters
        ----------
        ens_time: list
            List of ensemble times
        utc_time_offset: str
            String containing time offset to get to local time.
        date_format: str
            String defining date format
        """

        # Create list of serial times in datetime format
        serial_time = []
        for tm in ens_time:
            time_local = local_time_from_iso(tm, utc_time_offset)
            serial_time.append(time_local)

        # Compute delta time
        diff = np.diff(serial_time)
        ensemble_delta_time = [0]
        for tm in diff:
            ensemble_delta_time.append(tm.total_seconds())

        # First ensemble counts for averaging velocity
        # Assume first ensemble time is equal to second
        ensemble_delta_time[0] = ensemble_delta_time[1]
        ens_dur_in = np.array(ensemble_delta_time)

        # Create object
        start_serial_time = serial_time[0].timestamp()
        end_serial_time = serial_time[-1].timestamp()
        meas_date = datetime.strftime(serial_time[0], date_format)
        self.date_time = DateTime()
        self.date_time.populate_data(
            date_in=meas_date,
            start_in=start_serial_time,
            end_in=end_serial_time,
            ens_dur_in=ens_dur_in,
            utc_time_offset=utc_time_offset
        )

    def rsqst_boat(self, bt, system_configuration):
        """Create boat_vel object.

        Parameters
        ----------
        bt: dict
            Dictionary of bottom track sample data
        system_configuration: dict
            General measurement configuration
        """

        # Create initial object
        self.boat_vel = BoatStructure()

        # Get raw data
        vel_in = bt["beam_vel"]
        vel_in[np.equal(vel_in, None)] = np.nan

        # Populate object
        self.boat_vel.add_boat_object(
            source="rsq",
            vel_in=vel_in.astype(float),
            freq_in=bt["frequency"],
            coord_sys_in="Beam",
            nav_ref_in="BT",
        )

        # Set track reference
        if system_configuration["TrackReference"] == "BottomTrack":
            self.boat_vel.selected = "bt_vel"
        else:
            self.boat_vel.selected = "None"

    def rsqst_gps(self, gps_ens):
        """Create gps object.

        Parameters
        ----------
        gps_ens: dict
            Dictionary of gps sample data
        """

        # Create object
        self.gps = GPSData()
        self.gps.populate_data(
            raw_gga_utc=None,
            raw_gga_lat=None,
            raw_gga_lon=None,
            raw_gga_alt=None,
            raw_gga_diff=None,
            raw_gga_hdop=None,
            raw_gga_num_sats=None,
            raw_gga_delta_time=None,
            raw_vtg_course=None,
            raw_vtg_speed=None,
            raw_vtg_delta_time=None,
            raw_vtg_mode_indicator=None,
            ext_gga_utc=gps_ens["gga_utc_time"],
            ext_gga_lat= gps_ens["gga_longitude"],
            ext_gga_lon= gps_ens["gga_latitude"],
            ext_gga_alt=gps_ens["gga_altitude"],
            ext_gga_diff=gps_ens["gga_quality"],
            ext_gga_hdop=gps_ens["gga_hdop"],
            ext_gga_num_sats=gps_ens["gga_sats"],
            ext_vtg_course=gps_ens["vtg_true_course"],
            ext_vtg_speed=gps_ens["vtg_speed"],
            gga_p_method="External",
            gga_v_method="External",
            vtg_method="External",
        )

    def rsqst_depths(self, bt, vb, wt, station_data, adcp_data, system_configuration, draft):
        """Create depths object.

        Parameters
        ----------
        bt: dict
            Dictionary of bottom track sample data
        vb: dict
            Dictionary of vertical beam sample data
        wt: dict
            Dictionary of water track sample data
        station_data: dict
            Information about the vertical
        adcp_data: dict
            Raw data from a vertical
        system_configuration: dict
            General measurement configuration
        """

        # Initialize depth data structure
        self.depths = DepthStructure()

        # Determine array rows and cols
        max_cells = 128
        num_ens = len(wt["vel"])

        # Compute cell sizes
        cell_size = np.array(wt["cell_size"])
        cell_size[np.equal(cell_size, None)] = np.nan
        cell_size = cell_size.astype(float)
        
        # Fill cell_size array, repeating last valid cell size
        for col in range(cell_size.shape[1]):
            size_idx = np.where(np.isnan(cell_size[:, col]))
            cell_size[size_idx[0], col] = cell_size[size_idx[0][0] - 1, col]
        top_of_cells = (np.array(wt["cell_start"]).astype(float) + draft)

        # Prepare bottom track depth variable
        depth = bt["beam_rng"]
        freq = bt["frequency"]

        # Zero depths are not valid
        depth[depth == 0] = np.nan

        # Draft
        depth = depth + draft

        # Compute cell depth
        # cell_depth = (
        #     (
        #         np.tile(
        #             np.arange(1, max_cells + 1, 1).reshape(max_cells, 1), (1, num_ens)
        #         )
        #         - 0.5
        #     )
        #     * cell_size_all
        # ) + np.tile(top_of_cells, (max_cells, 1))

        cell_depth = np.cumsum(cell_size, 0) - (0.5 * cell_size) + np.tile(top_of_cells,
                                                                           (max_cells, 1))
        # Create depth object for bottom track beams
        self.depths.add_depth_object(
            depth_in=depth,
            source_in="BT",
            freq_in=freq,
            draft_in=draft, 
            cell_depth_in=cell_depth, 
            cell_size_in=cell_size,
        )
        # Prepare vertical beam depth variable
        depth_vb = np.tile(np.nan, (1, cell_depth.shape[1]))

        # Retrieve raw data
        depth_vb[0, :] = vb["rng"]

        # Zero depths are not valid
        depth_vb[depth_vb == 0] = np.nan
        depth_vb = depth_vb + draft

        freq = (
            np.array(
                [
                    adcp_data["SensorConfiguration"]["Info"]["beamSetInfo"]["1"][
                        "systemFrequency (Hz)"
                    ]
                ]
                * depth.shape[-1]
            )
            / 1000
        )

        # Create depth object for vertical beam
        self.depths.add_depth_object(
            depth_in=depth_vb,
            source_in="VB",
            freq_in=freq,
            draft_in=draft,
            cell_depth_in=cell_depth,
            cell_size_in=cell_size,
        )

        # Manual depth
        depth_in = np.array(
            [station_data["UserWaterDepth (m)"]] * depth.shape[-1]
        ).reshape(1, depth.shape[-1])
        self.depths.add_depth_object(
            depth_in=depth_in,
            source_in="MAN",
            freq_in=np.array([np.nan] * depth.shape[-1]),
            draft_in=draft,
            cell_depth_in=cell_depth,
            cell_size_in=cell_size,
        )

        # Set depth reference
        if station_data["UserWaterDepth (m)"] > 0.01:
            self.depths.selected = "man_depths"
            self.depths.composite_depths(transect=self, setting="Off")

        elif system_configuration["DepthReference"] == "VerticalBeam":
            self.depths.selected = "vb_depths"
            self.depths.composite_depths(transect=self, setting="On")

        else:
            self.depths.selected = "bt_depths"
            self.depths.composite_depths(transect=self, setting="On")

    def rsqst_sensors(self, compass, sensors_ens, adcp_data, system_configuration):
        """Create sensors object.

        Parameters
        ----------
        compass: dict
            Dictionary of heading, pitch, roll, and magnetic error sample data
        sensors_ens: dict
            Dictionary of sensor sample data
        adcp_data: dict
            Raw data from a vertical
        system_configuration: dict
            General measurement configuration
        """

        # Sensor data
        # -----------
        self.sensors = Sensors()

        # Internal heading
        self.sensors.heading_deg.internal = HeadingData()

        # Assign data
        mag_error = np.array(compass["mag_error"]).astype(float) * 100.
        pitch_limit = np.array(
            (
                adcp_data["SensorConfiguration"]["CompassCalInfo"]["pitchMax (deg)"],
                adcp_data["SensorConfiguration"]["CompassCalInfo"]["pitchMin (deg)"],
            )
        ).T
        roll_limit = np.array(
            (
                adcp_data["SensorConfiguration"]["CompassCalInfo"]["rollMax (deg)"],
                adcp_data["SensorConfiguration"]["CompassCalInfo"]["rollMin (deg)"],
            )
        ).T
        heading = np.array(compass["heading"]).astype(float)
        pitch = np.array(compass["pitch"]).astype(float)
        roll = np.array(compass["roll"]).astype(float)

        # Populate object
        self.sensors.heading_deg.internal.populate_data(
            data_in=heading,
            source_in="internal",
            magvar=0,
            mag_error=mag_error,
            pitch_limit=pitch_limit,
            roll_limit=roll_limit,
        )
        self.sensors.heading_deg.selected = "internal"

        # Pitch and roll
        self.sensors.pitch_deg.internal = SensorData()
        self.sensors.pitch_deg.internal.populate_data(
            data_in=pitch, source_in="internal"
        )
        self.sensors.pitch_deg.selected = "internal"
        self.sensors.roll_deg.internal = SensorData()
        self.sensors.roll_deg.internal.populate_data(data_in=roll, source_in="internal")
        self.sensors.roll_deg.selected = "internal"

        # Temperature
        temperature = np.array(sensors_ens["temperature"]).astype(float)
        self.sensors.temperature_deg_c.internal = SensorData()
        self.sensors.temperature_deg_c.internal.populate_data(
            data_in=temperature, source_in="internal"
        )
        self.sensors.temperature_deg_c.selected = "internal"

        # Manual temperature
        if system_configuration["TemperatureOverride (C)"] is not None:
            temperature = [system_configuration["TemperatureOverride (C)"]] * len(
                sensors_ens["temperature"]
            )
            self.sensors.temperature_deg_c.user = SensorData()
            self.sensors.temperature_deg_c.user.populate_data(
                data_in=temperature, source_in="Manual"
            )
            self.sensors.temperature_deg_c.selected = "user"

        # Salinity
        # Initial salinity is saved in both internal and user with initial selection
        # internal to support QA check for changes
        salinity = np.array(
            [system_configuration["Salinity (PSS-78)"]]
            * len(sensors_ens["temperature"])
        )
        self.sensors.salinity_ppt.internal = SensorData()
        self.sensors.salinity_ppt.internal.populate_data(
            data_in=salinity, source_in="Manual"
        )
        self.sensors.salinity_ppt.user = SensorData()
        self.sensors.salinity_ppt.user.populate_data(
            data_in=salinity, source_in="Manual"
        )
        self.sensors.salinity_ppt.selected = "internal"

        # Speed of sound
        self.sensors.speed_of_sound_mps.internal = SensorData()
        self.sensors.speed_of_sound_mps.internal.populate_data(
            data_in=sensors_ens["sos"], source_in="ADCP"
        )
        # Set selected salinity
        self.sensors.speed_of_sound_mps.selected = "internal"

        if system_configuration["SoundSpeedOverride (m/s)"] is not None:
            sos = [system_configuration["SoundSpeedOverride (m/s)"]] * len(
                sensors_ens["temperature"]
            )
            self.sensors.speed_of_sound_mps.user = SensorData()
            self.sensors.speed_of_sound_mps.user.populate_data(
                data_in=sos, source_in="Manual"
            )
            self.sensors.speed_of_sound_mps.selected = "user"

        # Battery voltage
        self.sensors.battery_voltage.internal = SensorData()
        self.sensors.battery_voltage.internal.populate_data(
            data_in=sensors_ens["battery"], source_in="internal"
        )

    def rsqst_wt(self, wt, station_data, snr_3beam_comp, draft):
        """Create w_vel object.

        Parameters
        ----------
        wt: dict
            Dictionary of water track sample data
        station_data: dict
            Information about the vertical
        snr_3beam_comp: bool
            Indicates if invalid data due to SNR filter should be computed using 3-beam
            solution
        """

        # Apply TRDI scaling to SonTek difference velocity to convert to a TRDI compatible error velocity
        # vel[3, :, :] = vel[3, :, :] / ((2**0.5) * np.tan(np.deg2rad(25)))

        # Compute side lobe cutoff using Transmit Length information if available, if not it is assumed to be equal
        # to 1/2 depth_cell_size_m. The percent method is use for the side lobe cutoff computation.
        sl_cutoff_percent = station_data["ExtrapolationConfiguration"]["BottomDiscard"][
            "ProfileFraction"
        ]
        sl_cutoff_number = station_data["ExtrapolationConfiguration"]["BottomDiscard"][
            "NumberOfCells"
        ]

        min_depth = np.nanmin(self.depths.bt_depths.depth_beams_m, axis=0)

        blanking_plus_pulse_length = wt["blanking_dist"] + wt["pulse_length"]

        # Compute processing lag
        processing_lag = np.copy(wt["corr_lag"])
        idx = np.logical_and(wt["code_length"] > 1, wt["pulse_lag"] < 0)
        processing_lag[idx] = 2 * wt["corr_lag"][idx]
        processing_lag[wt["pulse_lag"] > 0] = 0

        # Account for rare occurance of None in pulse_length
        pulse_length = np.array(wt["pulse_length"])
        pulse_length[np.equal(pulse_length, None)] = np.nan
        pulse_length = pulse_length.astype(float)

        # Compute sidelobe cutoff
        sl_lag_effect_m = (processing_lag + wt["pulse_length"] + wt["cell_size"][0,
                                                                 :]) / 2
        
        sl_cutoff_type = "Percent"
        cells_above_sl, sl_cutoff_m = self.side_lobe_cutoff(
            depths=self.depths,
            sl_lag_effect=sl_lag_effect_m, 
            slc_type=sl_cutoff_type,
            value=1 - sl_cutoff_percent, )

        # sl_cutoff_type = "Percent"
        # pulse_length = np.array(wt["pulse_length"])
        # pulse_length[np.equal(pulse_length, None)] = np.nan
        # pulse_length = pulse_length.astype(float)
        # sl_lag_effect_m = (
        #     pulse_length + self.depths.bt_depths.depth_cell_size_m[0, :]
        # ) / 2.0
        # 
        # cells_above_sl, sl_cutoff_m = VerticalData.side_lobe_cutoff(
        #     depths=self.depths,
        #     sl_lag_effect=sl_lag_effect_m,
        #     slc_type=sl_cutoff_type,
        #     value=1 - sl_cutoff_percent,
        # )

        # snr_min_threshold = adcp_data["config_json"]["Setup"]["CalculationThresholds"][
        #     "MinBeamSnr (dB)"]
        # wt["vel"][wt["snr"] < snr_min_threshold] = np.nan

        # Determine excluded distance (Similar to SonTek's screening distance)
        excluded_bottom = 0
        excluded_bottom_type = "Distance"
        excluded_top = (
            station_data["ScreeningDistance (m)"] - draft
        )
        excluded_top_type = "Distance"
        if excluded_top < 0:
            excluded_top = 0

        ping_type = []
        for n in range(len(wt["mode"])):
            ping_type.append(str(wt["freq"][n])[0] + wt["mode"][n])

        # Create water velocity object
        self.w_vel = WaterData()
        blanking_dist = np.array(wt["blanking_dist"])
        blanking_dist[np.equal(blanking_dist, None)] = np.nan
        blanking_dist = blanking_dist.astype(float)
        # In some rare situations the blank is empty so it is set to the excluded_dist_in
        if blanking_dist.shape[0] == 0:
            if excluded_top_type == "Distance":
                blanking_dist = excluded_top
            else:
                blanking_dist = 0
        self.w_vel.populate_data(
            vel_in=wt["vel"],
            freq_in=wt["freq"],
            coord_sys_in="Beam",
            nav_ref_in="None",
            rssi_in=wt["snr"],
            rssi_units_in="SNR",
            excluded_bottom_in=excluded_bottom,
            excluded_bottom_type_in=excluded_bottom_type,
            excluded_top_in=excluded_top,
            excluded_top_type_in=excluded_top_type,
            cells_above_sl_in=cells_above_sl,
            sl_cutoff_per_in=sl_cutoff_percent,
            sl_cutoff_num_in=sl_cutoff_number,
            sl_cutoff_type_in=sl_cutoff_type,
            sl_lag_effect_in=sl_lag_effect_m,
            sl_cutoff_m=sl_cutoff_m,
            wm_in=wt["mode"],
            blank_in=blanking_dist,
            corr_in=wt["corr"],
            ping_type=np.array(ping_type),
            snr_3beam_comp=snr_3beam_comp,
            source="rsq"
        )

    def rsqst_extrap(self, station_data):
        """Create extrap object.

        Parameters
        ----------
        station_data: dict
            Information about the vertical
        """

        # Create translation dictionary for fit types
        method = {
            "PowerFit": "Power",
            "Constant": "Constant",
            "NoSlip": "No Slip",
            "ThreePointSlope": "3-Point",
        }

        # Determine QRev compatible fit and exponent
        top = station_data["ExtrapolationConfiguration"]["TopExtrapolation"]["Method"]

        # SonTek allows Power at the top and no slip or power at the bottom with top and bottom having different
        # exponents. QRev does not support different exponents for open water situations.
        if method[top] == "Power":
            bot = "PowerFit"
            exp = station_data["ExtrapolationConfiguration"]["TopExtrapolation"][
                "Coefficient"
            ]
        else:
            bot = "NoSlip"
            exp = station_data["ExtrapolationConfiguration"]["BottomExtrapolation"][
                "Coefficient"
            ]

        # Create extrap object
        self.extrap = Extrapolation()
        self.extrap.populate_data(top=method[top], bot=method[bot], exp=exp)

    @staticmethod
    def compute_excluded_distance(
        depths,
        cells_above_sl,
        excluded_top,
        excluded_top_type,
        excluded_bottom,
        excluded_bottom_type,
    ):
        """Returns the top and bottom excluded distances. If the excluded top and bottom are specified as
        number of cells, the distance based on the number of cells is computed.

        Parameters
        ----------
        depths: DepthStructure
            Object of DepthStructure
        cells_above_sl: np.array(bool)
            Bool array of depth cells above the sidelobe cutoff based on selected depth reference.
        excluded_bottom: int or float
            Number of cells or distance in m to exclude above side lobe cutoff
        excluded_bottom_type: str
            Type of exclusion (Cells, Distance)
        excluded_top: int or float
            Number of cells at the top or distance in m to exclude below transducer
        excluded_top_type: str
            Type of exclusion (Cells, Distance)

        Returns
        -------
        bottom_distance_m: float
            Maximum distance below transducer to bottom of last valid depth cell, in m
        top_distance_m: float
            Minimum distance below transducer to top of top valid depth cell, in m
        """

        depth = getattr(depths, depths.selected)
        top_distance = 0
        bottom_distance = 0

        # Compute top distance
        if excluded_top > 0:
            if excluded_top_type == "Distance":
                top_dist = excluded_top
            else:
                # Compute top distance from number of cells specified. Uses the first ensemble in series for
                # the computation.
                top_dist = (
                    depth.depth_cell_depth_m[0, int(excluded_top) - 1]
                    + 0.5 * depth.depth_cell_size_m[0, int(excluded_top) - 1]
                )

            top_distance = top_dist - depth.draft_use_m

        # Compute the bottom distance
        if excluded_bottom > 0:
            if excluded_bottom_type == "Distance":
                bottom_distance = excluded_bottom
            else:
                # Compute the bottom distance above the slide lobe if the number of cells are specified.
                # Uses the first ensemble in series for the computation.
                bottom_cell_number = np.nansum(cells_above_sl[:, 0]) - 1
                bottom_distance = depth.depth_cell_size_m[0, bottom_cell_number]

        return top_distance, bottom_distance

    def change_magvar(self, magvar):
        """Changes the magnetic variation to the specified value and applies the change to associated data.

        Parameter
        ---------
        magvar: float
            Magnetic variation to be used
        """

        # Update object
        if self.sensors.heading_deg.external is not None:
            self.sensors.heading_deg.external.set_mag_var(magvar, "external")

        if self.sensors.heading_deg.selected == "internal":
            heading_selected = getattr(
                self.sensors.heading_deg, self.sensors.heading_deg.selected
            )
            old_magvar = heading_selected.mag_var_deg
            magvar_change = magvar - old_magvar
            heading_selected.set_mag_var(magvar, "internal")
            self.boat_vel.bt_vel.change_heading(magvar_change)
            self.w_vel.change_heading(self.boat_vel, magvar_change)
        else:
            self.sensors.heading_deg.internal.set_mag_var(magvar, "internal")

        self.update_water()

    def change_heading_source(self, h_source):
        """Changes the heading source and applies the change to associated data.

        Parameters
        ----------
        h_source: str
            Heading source to be used
        """

        # If source is user, check to see if it was created, if not create it
        if h_source == "user":
            if self.sensors.heading_deg.user is None:
                self.sensors.heading_deg.user = HeadingData()
                self.sensors.heading_deg.user.populate_data(
                    data_in=np.zeros(self.boat_vel.bt_vel.u_processed_mps.shape),
                    source_in="user",
                    magvar=0,
                    align=0,
                )

        # Get new heading object
        new_heading_selection = getattr(self.sensors.heading_deg, h_source)

        # Change source to that requested
        if h_source is not None:
            old_heading_selection = getattr(
                self.sensors.heading_deg, self.sensors.heading_deg.selected
            )
            old_heading = old_heading_selection.data
            new_heading = new_heading_selection.data
            heading_change = new_heading - old_heading
            self.sensors.heading_deg.set_selected(h_source)
            self.boat_vel.bt_vel.change_heading(heading_change)
            self.w_vel.change_heading(self.boat_vel, heading_change)

        self.update_water()

    def update_water(self):
        """Method called from set_nav_reference, boat_interpolation and boat filters
        to ensure that changes in boatvel are reflected in the water data"""

        self.w_vel.set_nav_reference(self.boat_vel)

        # Reapply water filters and interpolations
        # Note wt_filters calls apply_filter which automatically calls
        # apply_interpolation so both filters and interpolations
        # are applied with this one call

        self.w_vel.apply_filter(transect=self)
        self.w_vel.apply_interpolation(transect=self)

    def change_sos(
        self, parameter=None, salinity=None, temperature=None, selected=None, speed=None
    ):
        """Changes the speed of sound based on provided parameters.

        Parameters
        ----------
        parameter: str
            Speed of sound parameter to be changed ('temperatureSrc', 'temperature', 'salinity', 'sosSrc')
        salinity: float
            Salinity in ppt
        temperature: float
            Temperature in deg C
        selected: str
            Selected speed of sound ('internal', 'computed', 'user') or temperature ('internal', 'user')
        speed: float
            Manually supplied speed of sound for 'user' source
        """

        # Change sos due to change in temperature source
        if parameter == "temperatureSrc":
            temperature_internal = getattr(self.sensors.temperature_deg_c, "internal")
            if selected == "user":
                if self.sensors.temperature_deg_c.user is None:
                    self.sensors.temperature_deg_c.user = SensorData()
                ens_temperature = np.tile(temperature, temperature_internal.data.shape)

                self.sensors.temperature_deg_c.user.change_data(data_in=ens_temperature)
                self.sensors.temperature_deg_c.user.set_source(source_in="Manual Input")

            # Set the temperature data to the selected source
            self.sensors.temperature_deg_c.set_selected(selected_name=selected)
            # Update the speed of sound
            self.update_sos()

        # Change sos due to setting of a manual temperature
        elif parameter == "temperature":
            adcp_temp = self.sensors.temperature_deg_c.internal.data
            new_user_temperature = np.tile(temperature, adcp_temp.shape)
            self.sensors.temperature_deg_c.user.change_data(
                data_in=new_user_temperature
            )
            self.sensors.temperature_deg_c.user.set_source(source_in="Manual Input")
            # Set the temperature data to the selected source
            self.sensors.temperature_deg_c.set_selected(selected_name="user")
            # Update the speed of sound
            self.update_sos()

        # Change sos due to change in salinity
        elif parameter == "salinity":
            if salinity is not None:
                self.sensors.salinity_ppt.user.change_data(data_in=salinity)
                if type(self.sensors.salinity_ppt.internal.data) is float:
                    salinity_internal = self.sensors.salinity_ppt.internal.data
                else:
                    salinity_internal = self.sensors.salinity_ppt.internal.data
                if np.all(
                    np.equal(self.sensors.salinity_ppt.user.data, salinity_internal)
                ):
                    self.sensors.salinity_ppt.set_selected(selected_name="internal")
                else:
                    self.sensors.salinity_ppt.set_selected(selected_name="user")
                self.update_sos()

        # Change sos due to change in sos source
        elif parameter == "sosSrc":
            if selected == "internal":
                self.update_sos()
            elif selected == "user":
                self.update_sos(speed=speed, selected="user", source="Manual Input")

    def update_sos(self, selected=None, source=None, speed=None):
        """Sets a new specified speed of sound.

        Parameters
        ----------
        self: obj
            Object of TransectData
        selected: str
             Selected speed of sound ('internal', 'computed', 'user')
        source: str
            Source of speed of sound (Computer, Calculated)
        speed: float
            Manually supplied speed of sound for 'user' source
        """

        # Get current speed of sound
        sos_selected = getattr(
            self.sensors.speed_of_sound_mps, self.sensors.speed_of_sound_mps.selected
        )
        old_sos = sos_selected.data
        new_sos = None

        # Manual input for speed of sound
        if selected == "user" and source == "Manual Input":
            self.sensors.speed_of_sound_mps.set_selected(selected_name=selected)
            self.sensors.speed_of_sound_mps.user = SensorData()
            self.sensors.speed_of_sound_mps.user.populate_data(speed, source)

        # If called with no input set source to internal and determine whether computed or calculated based on
        # availability of user supplied temperature or salinity
        elif selected is None and source is None:
            self.sensors.speed_of_sound_mps.set_selected("internal")
            # If temperature or salinity is set by the user the speed of sound is computed otherwise it is consider
            # calculated by the ADCP.
            if (self.sensors.temperature_deg_c.selected == "user") or (
                self.sensors.salinity_ppt.selected == "user"
            ):
                self.sensors.speed_of_sound_mps.internal.set_source("Computed")
            else:
                self.sensors.speed_of_sound_mps.internal.set_source("Calculated")

        # Determine new speed of sound
        if self.sensors.speed_of_sound_mps.selected == "internal":
            if self.sensors.speed_of_sound_mps.internal.source == "Calculated":
                # Internal: Calculated
                new_sos = self.sensors.speed_of_sound_mps.internal.data_orig
                self.sensors.speed_of_sound_mps.internal.change_data(data_in=new_sos)
                # Change temperature and salinity selected to internal
                self.sensors.temperature_deg_c.set_selected(selected_name="internal")
                self.sensors.salinity_ppt.set_selected(selected_name="internal")
            else:
                # Internal: Computed
                temperature_selected = getattr(
                    self.sensors.temperature_deg_c,
                    self.sensors.temperature_deg_c.selected,
                )
                temperature = temperature_selected.data
                salinity_selected = getattr(
                    self.sensors.salinity_ppt, self.sensors.salinity_ppt.selected
                )
                salinity = salinity_selected.data
                new_sos = Sensors.speed_of_sound(
                    temperature=temperature, salinity=salinity
                )
                self.sensors.speed_of_sound_mps.internal.change_data(data_in=new_sos)
        else:
            if speed is not None:
                new_sos = np.tile(
                    speed, len(self.sensors.speed_of_sound_mps.internal.data_orig)
                )
                self.sensors.speed_of_sound_mps.user.change_data(data_in=new_sos)

        self.apply_sos_change(old_sos=old_sos, new_sos=new_sos)

    def apply_sos_change(self, old_sos, new_sos):
        """Computes the ratio and calls methods in WaterData and BoatData to apply change.

        Parameters
        ----------
        old_sos: float
            Speed of sound on which the current data are based, in m/s
        new_sos: float
            Speed of sound on which the data need to be based, in m/s
        """

        ratio = new_sos / old_sos

        # RiverRay horizontal velocities are not affected by changes in speed of sound
        if self.inst.model != "RiverRay":
            # Apply speed of sound change to water and boat data
            self.w_vel.sos_correction(ratio=ratio)
            self.boat_vel.bt_vel.sos_correction(ratio=ratio)
        # Correct depths
        self.depths.sos_correction(ratio=ratio)
