import numpy as np
from Classes.ExtrapFit import ExtrapFit


class Extrapolation(object):
    """Stores both original and modified extrapolation settings.

    Attributes
    ----------
    bot_method: str
            Applied extrapolation method for bottom of profile (Power, No Slip)
    bot_method_orig: str
            Original extrapolation method for bottom of profile (Power, No Slip)
    exponent: float
        Exponent for bottom or entire profile
    exponent_orig: float
        Original exponent for power of no slip methods
    ice_exponent: float
        Exponent for ice method
    ice_exponent_orig: float or None
        Original exponent for ice method
    top_method: str
        Applied extrapolation method for bottom of profile (Power, Constant, 3-Point, Ice)
    top_method_orig: str
        Original extrapolation method for bottom of profile (Power, Constant, 3-Point, Ice)

    """

    def __init__(self):
        """Initialize class and set defaults."""
        self.top_method_orig = None
        self.bot_method_orig = None
        self.exponent_orig = np.nan
        self.top_method = ""
        self.bot_method = ""
        self.exponent = np.nan
        self.ice_exponent = np.nan
        self.ice_exponent_orig = np.nan
        self.extrap_fit = ExtrapFit()

    def populate_data(self, top, bot, exp, ice_exp=np.nan):
        """Store data in class variables.

        Parameters
        ----------
        top: str
            Original top method.
        bot: str
            Original bottom method.
        exp: float
            Original exponent for bottom or whole profile.
        ice_exp: float
            Top exponent for ice method
        """
        self.top_method_orig = top
        self.bot_method_orig = bot
        self.top_method = top
        self.bot_method = bot
        self.exponent_orig = float(exp)
        self.exponent = float(exp)
        if np.isnan(ice_exp):
            self.ice_exponent = ice_exp
            self.ice_exponent_orig = ice_exp
        else:
            self.ice_exponent = float(ice_exp)
            self.ice_exponent_orig = float(ice_exp)

    def set_extrap_data(self, top, bot, exp, ice_exp=None):
        """Store new extrapolation settings

        Parameters
        ----------
        top: str
            New top extrapolation method.
        bot: str
            New bottom extrapolation method.
        exp: float
            New bottom exponent.
        ice_exp: float or None
            New top exponent
        """
        self.top_method = top
        self.bot_method = bot
        self.exponent = exp
        self.ice_exponent = ice_exp
