import numpy as np


class Uncertainty(object):
    """Contains methods to compute the uncertainty using various methods.

    Attributes
    ----------
    default_values: dict
        Dictionary of default uncertainty values
    iso_num_verticals: np.array(float)
        Table of ISO uncertainty values based on number of verticals
    iso_num_cells_tbl: list
        List of ISO uncertainty values for number of cells 1-10
    method: str
        Identifies the uncertianty method used
    u_iso: dict
        Dictionary of computed ISO uncertainy values
    u_iso_contribution: dict
        Dictionary of the contribution of each ISO uncertainty to the total uncertainty
    u_ive: dict
        Dictionary of computed IVE uncertainty values
    u_ive_contribution: dict
        Dictionary of the contribution of each IVE uncertainty to the total uncertainty
    u_user: dict
        Dictionary of computed uncertainty using the selected method and user input
    u_user_contribution: dict
        Dictionary of the contribution of each user uncertainty to the total uncertainty
    user_values: dict
        User values overriding the default values
    """

    def __init__(self, method):
        # ISO table for number of verticals
        self.iso_num_verticals = np.array(
            [
                [5, 10, 15, 20, 25, 30, 35],
                [0.075, 0.045, 0.030, 0.025, 0.020, 0.015, 0.010],
            ]
        )

        # Expanded ISO table using linear interpolation
        self.iso_num_cells_tbl = [
            0.0750,
            0.0350,
            0.0317,
            0.0284,
            0.025,
            0.0210,
            0.0170,
            0.0130,
            0.0090,
            0.0050,
        ]

        self.default_values = {
            "u_so": 0.01,
            "u_sm": np.nan,
            "u_m": np.nan,
            "u_b": 0.005,
            "u_c": 0,
        }

        # ISO Method
        self.u_iso = {
            "u_s": np.nan,
            "u_c": np.nan,
            "u_d": np.nan,
            "u_v": np.nan,
            "u_b": np.nan,
            "u_m": np.nan,
            "u_p": np.nan,
            "u_q": np.nan,
            "u95_q": np.nan,
        }

        self.u_iso_contribution = {
            "u_s": np.nan,
            "u_c": np.nan,
            "u_d": np.nan,
            "u_v": np.nan,
            "u_b": np.nan,
            "u_m": np.nan,
            "u_p": np.nan,
        }

        # IVE Method
        self.u_ive = {
            "u_s": np.nan,
            "u_d": np.nan,
            "u_v": np.nan,
            "u_b": np.nan,
            "u_q": np.nan,
            "u95_q": np.nan,
        }

        self.u_ive_contribution = {
            "u_s": np.nan,
            "u_d": np.nan,
            "u_v": np.nan,
            "u_b": np.nan,
        }

        self.user_values = {
            "u_so": np.nan,
            "u_sm": np.nan,
            "u_m": np.nan,
            "u_b": np.nan,
            "u_c": np.nan,
        }

        self.u_user = {
            "u_s": np.nan,
            "u_c": np.nan,
            "u_d": np.nan,
            "u_v": np.nan,
            "u_b": np.nan,
            "u_m": np.nan,
            "u_p": np.nan,
            "u_q": np.nan,
            "u95_q": np.nan,
        }

        self.u_user_contribution = {
            "u_s": np.nan,
            "u_c": np.nan,
            "u_d": np.nan,
            "u_v": np.nan,
            "u_b": np.nan,
            "u_m": np.nan,
            "u_p": np.nan,
        }

        self.method = method

    def compute_uncertainty(self, meas, u_x="", value=np.nan):
        """Compute uncertainty.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        u_x: str
            String identifying uncertainty component
        value: float
            New value of uncertainty component

        Returns
        -------
        method: str
            Name of uncertainty method
        u: dict
            Dictionary of computed uncertainty values
        contributions: dict
            Dictionary of contributions of each component to the total uncertainty
        """

        if u_x == "u_so":
            self.user_values["u_so"] = value
        if np.isnan(self.user_values["u_so"]):
            u_so = self.default_values["u_so"]
        else:
            u_so = self.user_values["u_so"]

        if u_x == "u_sm":
            self.user_values["u_sm"] = value
        if np.isnan(self.user_values["u_sm"]):
            self.default_values["u_sm"] = self.instrument_uncertainty(meas)
            u_sm = self.default_values["u_sm"]
        else:
            u_sm = self.user_values["u_sm"]

        if u_x == "u_m":
            self.user_values["u_m"] = value
        if np.isnan(self.user_values["u_m"]):
            # Uncertainty due to number of verticals using linear interpolation from ISO table
            u_m = np.interp(
                len(meas.verticals_used_idx),
                self.iso_num_verticals[0, :],
                self.iso_num_verticals[1, :],
            )
            self.default_values["u_m"] = u_m
        else:
            u_m = self.user_values["u_m"]

        if u_x == "u_b":
            self.user_values["u_b"] = value
        if np.isnan(self.user_values["u_b"]):
            u_b = self.default_values["u_b"]
        else:
            u_b = self.user_values["u_b"]

        if u_x == "u_c":
            self.user_values["u_c"] = value
        if np.isnan(self.user_values["u_c"]):
            u_c = self.default_values["u_c"]
        else:
            u_c = self.user_values["u_c"]

        self.iso_method(meas=meas, u_m=u_m, u_so=u_so, u_sm=u_sm, u_c=u_c, u_b=u_b)
        self.ive_method(
            meas=meas,
            u_so=u_so,
            u_sm=u_sm,
            u_b=u_b,
        )

        self.compute_user_uncertainty()

    def compute_user_uncertainty(self, u_x="", value=np.nan):
        """Compute an uncertainty based on user specified values.

        Parameters
        ----------
        u_x: str
            String identifying uncertainty component
        value: float
            New value of uncertainty component

        Returns
        -------
        method: str
            Name of uncertainty method
        u: dict
            Dictionary of computed uncertainty values
        contributions: dict
            Dictionary of contributions of each component to the total uncertainty
        """
        self.method = self.method[0:3]

        # User method with IVE
        if self.method == "IVE":
            ive_user = {"u_s": np.nan, "u_b": np.nan, "u_d": np.nan, "u_v": np.nan}
            other_keys = ["u_m", "u_p", "u_c", "u_q", "u95_q"]

            # Use user values if available, if not use IVE values
            for key in ive_user.keys():
                if u_x == key:
                    self.u_user[key] = value
                if np.isnan(self.u_user[key]):
                    ive_user[key] = self.u_ive[key]
                else:
                    ive_user[key] = self.u_user[key]
                    self.method = "IVE_User"
            for key in other_keys:
                self.u_user[key] = np.nan

            # If user values available recompute uncertainty
            if self.method == "IVE_User":
                # Compute discharge uncertainty
                u2_q = (
                    ive_user["u_s"] ** 2
                    + ive_user["u_d"] ** 2
                    + ive_user["u_v"] ** 2
                    + ive_user["u_b"] ** 2
                )

                self.u_user["u_q"] = np.sqrt(u2_q)
                self.u_user["u95_q"] = 2 * np.sqrt(u2_q)

                for key in self.u_user_contribution.keys():
                    self.u_user_contribution[key] = np.nan

                self.u_user_contribution["u_d"] = ive_user["u_d"] ** 2 / u2_q
                self.u_user_contribution["u_v"] = ive_user["u_v"] ** 2 / u2_q
                self.u_user_contribution["u_b"] = ive_user["u_b"] ** 2 / u2_q
                self.u_user_contribution["u_s"] = ive_user["u_s"] ** 2 / u2_q

            # Clear user uncertainty as no values are set
            else:
                for key in self.u_user.keys():
                    self.u_user[key] = np.nan

                for key in self.u_user_contribution.keys():
                    self.u_user_contribution[key] = np.nan
        # User method with ISO
        else:
            iso_user = dict()
            self.u_user["u_q"] = np.nan
            self.u_user["u95_q"] = np.nan
            for key in self.u_user.keys():
                if u_x == key:
                    self.u_user[key] = value
                if np.isnan(self.u_user[key]):
                    iso_user[key] = self.u_iso[key]
                else:
                    iso_user[key] = self.u_user[key]
                    self.method = "ISO_User"

            if self.method == "ISO_User":
                u2_q = (
                    iso_user["u_s"] ** 2
                    + iso_user["u_m"] ** 2
                    + iso_user["u_b"] ** 2
                    + iso_user["u_d"] ** 2
                    + iso_user["u_p"] ** 2
                    + iso_user["u_v"] ** 2
                    + iso_user["u_c"] ** 2
                )

                self.u_user["u_q"] = np.sqrt(u2_q)
                self.u_user["u95_q"] = np.sqrt(u2_q) * 2

                self.u_user_contribution["u_b"] = iso_user["u_b"] ** 2 / u2_q
                self.u_user_contribution["u_d"] = iso_user["u_d"] ** 2 / u2_q
                self.u_user_contribution["u_p"] = iso_user["u_p"] ** 2 / u2_q
                self.u_user_contribution["u_v"] = iso_user["u_v"] ** 2 / u2_q
                self.u_user_contribution["u_c"] = iso_user["u_c"] ** 2 / u2_q
                self.u_user_contribution["u_s"] = iso_user["u_m"] ** 2 / u2_q
                self.u_user_contribution["u_m"] = iso_user["u_s"] ** 2 / u2_q

            # Clear user uncertainty as no values are set
            else:
                for key in self.u_user.keys():
                    self.u_user[key] = np.nan

                for key in self.u_user_contribution.keys():
                    self.u_user_contribution[key] = np.nan

    @staticmethod
    def instrument_uncertainty(meas):
        """Computes the system uncertainty based on the manufacturer specifications
        of the ADCP used.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement

        Returns
        -------
        u_inst: float
            Systematic uncertainty of instrument at 1 sigma
        """

        model = ""
        for idx in meas.verticals_used_idx:
            if meas.verticals[idx].data.inst is not None:
                model = meas.verticals[idx].data.inst.manufacturer
                break

        mean_vel = meas.summary["Measured Normal Velocity"][1:-4].mean()

        if model == "RS5" or model == "StreamPro":
            u_inst = 0.01 + 0.002 / mean_vel
        else:
            u_inst = 0.0025 + 0.002 / mean_vel

        return u_inst

    def iso_method(self, meas, u_m, u_so, u_sm, u_c, u_b, u_d=None):
        """Computes the uncertainty using the method defined in ISO 748.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        u_m: float
            Uncertainty due to number of verticals at 1 sigma
        u_so: float
            Systematic uncertainty for other sources at 1 sigma
        u_sm: float
            Systematic uncertainty for meter at 1 sigma
        u_c: float
            Instrument repeatability
        u_b: float
            Random uncertainty in width at 1 sigma
        u_d: float
            Random uncertainty in depth at 1 sigma
        """

        # Compute systematic uncertainty
        u_s = np.sqrt(u_so**2 + u_sm**2)

        # Compute uncertianty summations using each vertical
        u2_b_sum = 0
        u2_d_sum = 0
        u2_p_sum = 0
        u2_v_sum = 0
        u2_c_sum = 0

        q_idx = 0
        for idx in meas.verticals_used_idx[1:-1]:
            q_idx = q_idx + 1
            vertical = meas.verticals[idx]
            num_valid_cells = vertical.mean_profile["speed"].size - 2

            if u_d is None:
                # Depth from E3, ISO 748
                if vertical.depth_m > 0.3:
                    u_d = 0.005
                else:
                    u_d = 0.015

            # Number of measured points (cells)
            if num_valid_cells > 10:
                u_p = self.iso_num_cells_tbl[9]
            else:
                u_p = self.iso_num_cells_tbl[num_valid_cells - 1]

            # Computed instead of using exposure time table from ISO 748
            if np.abs(vertical.mean_measured_speed_mps) > 0:
                u_v = np.abs(
                    (
                        vertical.mean_measured_speed_cv
                        / np.sqrt(np.nanmax(vertical.mean_profile["number_ensembles"]))
                    )
                    / 100.0
                )
            else:
                u_v = 0

            if np.logical_not(
                np.any(
                    np.isnan([u_b, u_d, u_p, u_v, u_c, meas.discharge["q_cms"][q_idx]])
                )
            ):
                u2_b_sum = u2_b_sum + (meas.discharge["q_cms"][q_idx] ** 2) * (u_b**2)
                u2_d_sum = u2_d_sum + (meas.discharge["q_cms"][q_idx] ** 2) * (u_d**2)
                u2_p_sum = u2_p_sum + (meas.discharge["q_cms"][q_idx] ** 2) * (u_p**2)
                if num_valid_cells > 0:
                    u2_v_sum = u2_v_sum + (meas.discharge["q_cms"][q_idx] ** 2) * (
                        (u_v**2) / num_valid_cells
                    )
                    u2_c_sum = u2_c_sum + (meas.discharge["q_cms"][q_idx] ** 2) * (
                        (u_c**2) / num_valid_cells
                    )

        # Compute uncertainty by component
        q_total_sqr = np.nansum(meas.discharge["q_cms"]) ** 2
        u2_b = u2_b_sum / q_total_sqr
        u2_d = u2_d_sum / q_total_sqr
        u2_p = u2_p_sum / q_total_sqr
        u2_v = u2_v_sum / q_total_sqr
        u2_c = u2_c_sum / q_total_sqr

        u2_q = u_s**2 + u_m**2 + u2_b + u2_d + u2_p + u2_v + u2_c

        self.u_iso = {
            "u_q": np.sqrt(u2_q),
            "u95_q": np.sqrt(u2_q) * 2,
            "u_b": np.sqrt(u2_b),
            "u_d": np.sqrt(u2_d),
            "u_p": np.sqrt(u2_p),
            "u_v": np.sqrt(u2_v),
            "u_c": np.sqrt(u2_c),
            "u_s": u_s,
            "u_m": u_m,
        }

        self.u_iso_contribution = {
            "u_b": u2_b / u2_q,
            "u_d": u2_d / u2_q,
            "u_p": u2_p / u2_q,
            "u_v": u2_v / u2_q,
            "u_c": u2_c / u2_q,
            "u_s": u_s**2 / u2_q,
            "u_m": u_m**2 / u2_q,
        }

    def ive_method(self, meas, u_so, u_sm, u_b):
        """Computes the uncertainty using the IVE method as presented in
        Cohn, T.A, Kiang, J.E., and Mason, R.R., 2013, Estimating Discharge Measurement
        Uncertainty Using the Interpolated Variance Estimator, Journal of Hydraulic
        Engineering, Vol. 139, No. 5, May 1, 2013.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        u_so: float
            Systematic uncertainty of other sources at 1 sigma
        u_sm: float
            Systematic uncertainty of meter at 1 sigma
        u_b: float
            Random uncertainty in width at 1 sigma
        """

        # Compute systematic uncertainty
        u_s = np.sqrt(u_so**2 + u_sm**2)

        # Compute the sum of deltas for depth and velocity
        sum_delta_depth = 0
        sum_delta_vel = 0

        df = meas.summary[(np.logical_not(meas.summary["Station Number"].isna()))]
        # if meas.discharge_method == "Mean":
        #
        #     df = df[((df["Station Number"] % np.floor(df["Station Number"])) == 0.5)]
        #     start_idx = 1
        # else:
        start_idx = 2
        n_stations = len(df.index)

        for i in range(start_idx, n_stations - start_idx):
            if df.iloc[i]["Depth"] != 0 and df.iloc[i - 1]["Depth"] != 0 and df.iloc[i + 1]["Depth"] !=0:
                # Weighting
                wght = (df.iloc[i + 1]["Location"] - df.iloc[i]["Location"]) / (
                    df.iloc[i + 1]["Location"] - df.iloc[i - 1]["Location"]
                )

                # Depth
                depth_est = wght * (
                    df.iloc[i - 1]["Depth"] - df.iloc[i - 1]["Depth to Slush Bottom"]
                ) + (
                    (1 - wght)
                    * (df.iloc[i + 1]["Depth"] - df.iloc[i + 1]["Depth to Slush Bottom"])
                )
                delta_depth = (
                    df.iloc[i]["Depth"] - df.iloc[i]["Depth to Slush Bottom"] - depth_est
                )

                # Velocity
                vel_est = wght * df.iloc[i - 1]["Normal Velocity"] + (
                    (1 - wght) * df.iloc[i + 1]["Normal Velocity"]
                )
                delta_vel = df.iloc[i]["Normal Velocity"] - vel_est

                # Summations
                if np.logical_not(np.any(np.isnan([delta_depth, delta_vel, wght]))):
                    sum_delta_depth = sum_delta_depth + (
                        (delta_depth**2) / (2 * (1 - wght + wght**2))
                    )
                    sum_delta_vel = sum_delta_vel + (
                        (delta_vel**2) / (2 * (1 - wght + wght**2))
                    )

        # n_stations = len(meas.verticals_used_idx)
        # for i in range(2, n_stations - 2):
        #
        #     # Verticals
        #     vert_i = meas.verticals[meas.verticals_used_idx[i]]
        #     vert_i_minus = meas.verticals[meas.verticals_used_idx[i - 1]]
        #     vert_i_plus = meas.verticals[meas.verticals_used_idx[i + 1]]
        #
        #     # Weighting
        #     wght = (vert_i_plus.location_m - vert_i.location_m) / (
        #         vert_i_plus.location_m - vert_i_minus.location_m
        #     )
        #
        #     # Depth
        #     depth_est = wght * (
        #         vert_i_minus.depth_m - vert_i_minus.ws_to_slush_bottom_m
        #     ) + ((1 - wght) * (vert_i_plus.depth_m - vert_i_plus.ws_to_slush_bottom_m))
        #     delta_depth = vert_i.depth_m - vert_i.ws_to_slush_bottom_m - depth_est
        #
        #     # Velocity
        #     vel_est = wght * vert_i_minus.mean_speed_mps + (
        #         (1 - wght) * vert_i_plus.mean_speed_mps
        #     )
        #     delta_vel = vert_i.mean_speed_mps - vel_est
        #
        #     # Summations
        #     if np.logical_not(np.any(np.isnan([delta_depth, delta_vel, wght]))):
        #         sum_delta_depth = sum_delta_depth + (
        #             (delta_depth**2) / (2 * (1 - wght + wght**2))
        #         )
        #         sum_delta_vel = sum_delta_vel + (
        #             (delta_vel**2) / (2 * (1 - wght + wght**2))
        #         )

        # Compute the uncertainty due to depth and velocity
        if n_stations > 5:
            s_d = np.sqrt(sum_delta_depth / (n_stations - 5))
            s_v = np.sqrt(sum_delta_vel / (n_stations - 5))
        else:
            s_d = np.nan
            s_v = np.nan

        # Per Cohn, Kiang, and Mason the instrument resolution can be used as a lower
        # limit for s_v
        if np.isnan(s_v) or s_v < u_sm:
            s_v = u_sm

        # Compute component variations
        u2_d = 0
        u2_v = 0
        u2_b = 0
        q_total = meas.summary.loc["Total", "Q"]
        for row in range(1, n_stations - 1):
            q = meas.summary.loc[row, "Q"]
            v = meas.summary.loc[row, "Normal Velocity"]
            d = (meas.summary.loc[row, "Depth"]
                 - meas.summary.loc[row, "Depth to Slush Bottom"])
            if not np.isnan(q) and not np.isnan(v) and not np.isnan(d) and v != 0 and d != 0:
                u2_d = u2_d + ((q**2 * (s_d / d) ** 2) / q_total**2)
                u2_v = u2_v + ((q**2 * (s_v / v) ** 2) / q_total**2)
                u2_b = u2_b + ((q**2 * u_b**2) / q_total**2)

        # Compute discharge uncertainty
        u2_q = u_s**2 + u2_d + u2_v + u2_b
        self.u_ive = {
            "u_q": np.sqrt(u2_q),
            "u_d": np.sqrt(u2_d),
            "u_v": np.sqrt(u2_v),
            "u_b": np.sqrt(u2_b),
            "u_s": u_s,
            "u95_q": 2 * np.sqrt(u2_q),
        }

        self.u_ive_contribution = {
            "u_d": u2_d / u2_q,
            "u_v": u2_v / u2_q,
            "u_b": u2_b / u2_q,
            "u_s": u_s**2 / u2_q,
        }
