import copy
import numpy as np
from Modules.common_functions import iqr


class QAData(object):
    """Evaluates and stores quality assurance characteristics and messages.

    Attributes
    ----------
    boat: dict
        Dictionary of quality assurance checks on boat velocities
    bt_vel: dict
        Dictionary of quality assurance checks on bottom track velocities
    cells_caution_threshold: int
        Minimum number of cells recommended.
    cells_warning_threshold: int
        Minimum number of cells required.
    compass: dict
        Dictionary of quality assurance checks on compass calibration and evaluations
    depths: dict
        Dictionary of quality assurance checks on depths
    duration_caution_threshold: int
        Caution threshold for sampling duration in seconds
    duration_warning_threshold: int
        Warning threshold for sampling duration in seconds
    edges: dict
        Dictionary of quality assurance checks on edges
    extrapolation: dict
        Dictionary of quality assurance checks on extrapolation
    gga_vel: dict
        Dictionary of quality assurance checks on gga boat velocities
    main: dict
        Dictionary of quality assurance checks associated with the main tab
    percent_discharge_caution_threshold: float
        Caution threshold for discharge at a station
    percent_discharge_warning_threshold: float
        Warning threshold for discharge at a station
    percent_valid_caution_threshold: int
        Caution threshold for percent valid data, in percent.
    percent_valid_warning_threshold: int
        Warning threshold for percent valid data, in percent.
    rating: dict
        Dictionary of quality assurance checkes on the user rating
    stations: dict
        Dictionary of quality assurance checks on stations used, modified, or added
    settings_dict: dict
        Dictionary of changes to settings for each tab
    stations: dict
        Dictionary of quality assurance checks for station parameters
    system_tst: dict
        Dictionary of quality assurance checks on the system test(s)
    temperature: dict
        Dictionary of quality assurance checks on temperature comparions and variation
    tr: object
        Translation function
    user: dict
        Dictionary of quality assurance checks on user input data
    verticals: dict
        Dictionary of quality assurance checks on verticals
    vtg_vel: dict
        Dictionary of quality assurance checks on vtg boat velocities
    w_vel: dict
        Dictionary of quality assurance checks on water track velocities
    """

    def __init__(self, meas, compute=True, tr=None):
        """Checks the measurement for all quality assurance issues.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        compute: bool
            Indicates if the qa checks should be run
        """

        self.tr = tr

        # Initialize instance variables
        self.duration_caution_threshold = 40
        self.duration_warning_threshold = 20
        self.percent_valid_caution_threshold = 80
        self.percent_valid_warning_threshold = 50
        self.percent_discharge_warning_threshold = meas.station_q["warning"]
        self.percent_discharge_caution_threshold = meas.station_q["caution"]
        self.cells_caution_threshold = 3
        self.cells_warning_threshold = 0
        self.verticals = {"messages": []}
        self.system_tst = dict()
        self.compass = dict()
        self.temperature = dict()
        self.user = dict()
        self.stations = dict()
        self.main = dict()
        self.depths = dict()
        self.boat = dict()
        self.bt_vel = dict()
        self.gga_vel = dict()
        self.vtg_vel = dict()
        self.w_vel = dict()
        self.extrapolation = dict()
        self.edges = dict()
        self.rating = dict()
        self.settings_dict = dict()
        self.settings_dict["compasspr_tab"] = "Default"
        self.settings_dict["tempsal_tab"] = "Default"
        self.settings_dict["stations_tab"] = "Default"
        self.settings_dict["bt_tab"] = "Default"
        self.settings_dict["depth_tab"] = "Default"
        self.settings_dict["wt_tab"] = "Default"
        self.settings_dict["extrap_tab"] = "Default"
        self.settings_dict["edges_tab"] = "Default"
        self.settings_dict["main_premeasurement_tab"] = "Default"
        self.settings_dict["uncertainty_tab"] = "Default"

        if compute:
            # Apply QA checks
            self.verticals_qa(meas)
            self.system_tst_qa(meas)
            self.compass_qa(meas)
            self.temperature_qa(meas)
            self.user_qa(meas)
            self.stations_qa(meas)
            self.boat_qa(meas)
            self.depths_qa(meas)
            self.water_qa(meas)
            self.extrapolation_qa(meas)
            self.edges_qa(meas)
            self.rating_qa(meas)
            self.check_bt_setting(meas)
            self.check_wt_settings(meas)
            self.check_depth_settings(meas)
            self.check_edge_settings(meas)
            self.check_extrap_settings(meas)
            self.check_tempsal_settings(meas)
            self.check_compass_settings(meas)
            self.check_premeasurement_settings(meas)
            self.check_stations_settings(meas)
            self.check_uncertainty(meas)

    @staticmethod
    def guidance_prep(message_text, guidance_text):
        return message_text + "\n" + " -- " + guidance_text + "\n"

    def verticals_qa(self, meas):
        """Apply quality checks to transects

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        # Assume good results
        self.verticals["status"] = "good"
        self.main["status"] = "good"

        # Initialize keys
        self.main["messages"] = []
        self.main["guidance"] = []
        self.verticals["messages"] = []
        self.verticals["guidance"] = []
        self.verticals["duration_caution"] = []
        self.verticals["duration_warning"] = []
        self.verticals["valid_caution"] = []
        self.verticals["valid_warning"] = []
        self.verticals["no_valid_profile"] = []
        self.verticals["discharge_caution"] = []
        self.verticals["discharge_warning"] = []
        self.verticals["location_by_gps"] = []

        # Create a data frame having only measured verticals
        df = meas.summary[meas.summary["Duration"] > 0]

        if not df.empty:
            df = df.drop("Total")

            # Check duration
            self.verticals["duration_warning"] = df[
                df["Duration"] < self.duration_warning_threshold
            ]["Station Number"].tolist()
            self.verticals["duration_caution"] = df[
                (df["Duration"] < self.duration_caution_threshold)
                & (df["Duration"] >= self.duration_warning_threshold)
            ]["Station Number"].tolist()

            # Messages for duration
            if len(self.verticals["duration_warning"]) > 0:
                text = (
                    self.tr("STATIONS ")
                    + str(self.verticals["duration_warning"])
                    + self.tr(" have durations less than ")
                    + str(self.duration_warning_threshold)
                    + " secs"
                )
                self.verticals["status"] = "warning"
                self.verticals["messages"].append([text, 1, 0])
                guidance_text = self.tr("The short duration of data collection at a station could result in an inaccurate average velocity. It is generally recommended that at least 40 seconds of data be collected. Document the conditions preventing a longer duration measurement. If in the office, compare the average velocity to neighboring stations to ensure that it is reasonable.")
                self.verticals["guidance"].append(
                    self.guidance_prep(self.verticals["messages"][-1][0], guidance_text))
            elif len(self.verticals["duration_caution"]) > 0:
                text = (
                    self.tr("Stations ")
                    + str(self.verticals["duration_caution"])
                    + self.tr(" have durations less than ")
                    + str(self.duration_caution_threshold)
                    + " secs"
                )
                self.verticals["messages"].append([text, 2, 0])
                guidance_text = self.tr("It is generally recommended that at least 40 seconds of data be collected at each station to obtain the average velocity for the station. If that is not possible, document the conditions preventing collecting 40 seconds of data. If in the office, compare the average velocity to neighboring stations to ensure that it is reasonable.")
                self.verticals["guidance"].append(
                    self.guidance_prep(self.verticals["messages"][-1][0], guidance_text))
                

                if self.verticals["status"] != "warning":
                    self.verticals["status"] = "caution"

            # Percent valid profiles
            df["Number Profiles"] = df["Number Profiles"].replace([0], np.nan)
            df["Percent Valid"] = (
                df["Number Valid Profiles"] / df["Number Profiles"]
            ) * 100
            df["Percent Valid"] = df["Percent Valid"].replace([np.nan], 0)

            self.verticals["no_valid_profile"] = df[df["Percent Valid"] == 0][
                "Station Number"
            ].tolist()

            self.verticals["valid_warning"] = df[
                (df["Percent Valid"] < self.percent_valid_warning_threshold)
                & (df["Percent Valid"] > 0)
            ]["Station Number"].tolist()
            self.verticals["valid_caution"] = df[
                (df["Percent Valid"] < self.percent_valid_caution_threshold)
                & (df["Percent Valid"] >= self.percent_valid_warning_threshold)
            ]["Station Number"].tolist()

            # Remove warnings from caution list
            for item in self.verticals["valid_warning"]:
                if item in self.verticals["valid_caution"]:
                    self.verticals["valid_caution"].remove(item)

            self.verticals["batt_voltage"] = []
            batt_threshold = 10.5
            for n, idx in enumerate(meas.verticals_used_idx):
                if meas.verticals[idx].data.sensors is not None:
                    if meas.verticals[idx].data.inst.model == "RS5":
                        batt_threshold = 3.3

                    if (
                        np.nanmin(
                            meas.verticals[
                                idx
                            ].data.sensors.battery_voltage.internal.data
                        )
                        < batt_threshold
                    ):
                        self.verticals["batt_voltage"].append(n)

            # If some stations have low battery
            if len(self.verticals["batt_voltage"]) > 0:
                self.main["status"] = "caution"
                text = (
                    self.tr("Main ")
                    + str(self.verticals["batt_voltage"])
                    + self.tr(" have battery voltage less than ")
                    + str(batt_threshold)
                )
                self.main["messages"].append([text, 2, 0])
                guidance_text = self.tr("Low battery voltage may cause range issues with some ADCPs in some conditions. Evaluate the data carefully to ensure the data appear correct. If in the field, use a charged battery to recollect the data, if necessary. ")
                self.main["guidance"].append(
                    self.guidance_prep(self.main["messages"][-1][0], guidance_text))

            # Messages for percent valid
            if len(self.verticals["valid_warning"]) > 0:
                text = (
                    self.tr("STATIONS ")
                    + str(self.verticals["valid_warning"])
                    + self.tr(" have less than ")
                    + str(self.percent_valid_warning_threshold)
                    + self.tr("% valid ensembles")
                )
                self.verticals["status"] = "warning"
                self.verticals["messages"].append([text, 1, 0])
                guidance_text = self.tr(
                    "Less than 50% of the ensembles collected were valid. Review the data to ensure that there are sufficient data to provide an accurate velocity profile and mean velocity for the vertical.")
                self.verticals["guidance"].append(
                    self.guidance_prep(self.verticals["messages"][-1][0], guidance_text))

            if len(self.verticals["valid_caution"]) > 0:
                text = (
                    self.tr("Stations ")
                    + str(self.verticals["valid_caution"])
                    + self.tr(" have less than ")
                    + str(self.percent_valid_caution_threshold)
                    + self.tr("% valid ensembles")
                )
                self.verticals["messages"].append([text, 2, 0])
                guidance_text = self.tr(
                    "Less than 50% of the ensembles collected were valid. Review the data to ensure that there are sufficient data to provide an accurate velocity profile and mean velocity for the vertical.")
                self.verticals["guidance"].append(
                    self.guidance_prep(self.verticals["messages"][-1][0], guidance_text))
                if self.verticals["status"] != "warning":
                    self.verticals["status"] = "caution"

            if len(self.verticals["no_valid_profile"]) > 0:
                text = (self.tr("STATIONS ") 
                        + str(self.verticals["no_valid_profile"]) 
                        + self.tr(" have insufficient data for a valid profile "))
                self.verticals["messages"].append([text, 1, 0])
                guidance_text = self.tr("There are insufficient data to compute a valid profile. Review the data in the WT tab and BT tab (if using bottom track). Verify that the filters are set properly and working properly. It may be necessary to adjust the filters or to provide a manual estimate of the mean velocity.")
                self.verticals["guidance"].append(
                    self.guidance_prep(self.verticals["messages"][-1][0], guidance_text))
                self.verticals["status"] = "warning"
            
            # Percent discharge in vertical
            self.verticals["discharge_warning"] = df[
                df["Q%"] > self.percent_discharge_warning_threshold
            ]["Station Number"].tolist()
            self.verticals["discharge_caution"] = df[
                (df["Q%"] > self.percent_discharge_caution_threshold)
                & (df["Q%"] <= self.percent_discharge_warning_threshold)
            ]["Station Number"].tolist()

            # Messages for percent valid
            if len(self.verticals["discharge_warning"]) > 0:
                text = (
                    self.tr("MAIN ")
                    + str(self.verticals["discharge_warning"])
                    + self.tr(" have more than ")
                    + str(self.percent_discharge_warning_threshold)
                    + self.tr("% discharge")
                )
                self.main["status"] = "warning"
                self.main["messages"].append([text, 1, 0])
                guidance_text = self.tr("The spacing between stations is too large and could affect the total discharge. Reduce the spacing to reduce the percent discharge represented by each station.")
                self.main["guidance"].append(
                    self.guidance_prep(self.main["messages"][-1][0], guidance_text))

            if len(self.verticals["discharge_caution"]) > 0:
                text = (
                    self.tr("Main ")
                    + str(self.verticals["discharge_caution"])
                    + self.tr(" have more than ")
                    + str(self.percent_discharge_caution_threshold)
                    + self.tr("% discharge")
                )
                self.main["messages"].append([text, 2, 0])
                guidance_text = self.tr("The spacing between stations is too large, reduce the spacing to reduce the percent discharge represented by each station.")
                self.main["guidance"].append(
                    self.guidance_prep(self.main["messages"][-1][0], guidance_text))
                if self.main["status"] != "warning":
                    self.main["status"] = "caution"

            if "Location Source" in df:
                self.verticals["location_by_gps"] = df[df["Location Source"] == "GPS"][
                    "Station Number"
                ].tolist()
            if len(self.verticals["location_by_gps"]) > 0:
                text = (
                    self.tr("GPS Location: Stations ")
                    + str(self.verticals["location_by_gps"])
                    + self.tr("have locations determined by GPS.")
                )
                self.verticals["messages"].append([text, 2, 0])
                guidance_text = self.tr("The location of stations has been determined using GPS. Check locations to ensure that they are correct.")
                self.verticals["guidance"].append(
                    self.guidance_prep(self.verticals["messages"][-1][0], guidance_text))
                if self.verticals["status"] != "warning":
                    self.verticals["status"] = "caution"

    def system_tst_qa(self, meas):
        """Apply QA checks to system test.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.system_tst["messages"] = []
        self.system_tst["guidance"] = []
        self.system_tst["status"] = "good"

        # Determine if a system test was recorded
        if not meas.system_tst:
            # No system test data recorded
            self.system_tst["status"] = "warning"
            self.system_tst["messages"].append([self.tr("SYSTEM TEST: No system test") + ";", 1, 3])
            guidance_text = self.tr("A system test is recommended to be completed prior to every discharge measurement to ensure the ADCP is operating properly. If still in the field, complete a system test.")
            self.system_tst["guidance"].append(
                self.guidance_prep(self.system_tst["messages"][-1][0], guidance_text))
        else:
            pt3_fail = False
            num_tests_with_failure = 0

            for test in meas.system_tst:
                if hasattr(test, "result"):
                    # Check for presence of pt3 test
                    if "pt3" in test.result and test.result["pt3"] is not None:
                        # Check hard_limit, high gain, wide bandwidth
                        if "hard_limit" in test.result["pt3"]:
                            if "high_wide" in test.result["pt3"]["hard_limit"]:
                                corr_table = test.result["pt3"]["hard_limit"][
                                    "high_wide"
                                ]["corr_table"]
                                if len(corr_table) > 0:
                                    # All lags past lag 2 should be less than 50% of lag 0
                                    qa_threshold = corr_table[0, :] * 0.5
                                    all_lag_check = np.greater(
                                        corr_table[3::, :], qa_threshold
                                    )

                                    # Lag 7 should be less than 25% of lag 0
                                    lag_7_check = np.greater(
                                        corr_table[7, :], corr_table[0, :] * 0.25
                                    )

                                    # If either condition is met for any beam the test
                                    # fails
                                    if (
                                        np.sum(np.sum(all_lag_check))
                                        + np.sum(lag_7_check)
                                        > 1
                                    ):
                                        pt3_fail = True

                    if (
                        test.result["sysTest"]["n_failed"] is not None
                        and test.result["sysTest"]["n_failed"] > 0
                    ):
                        num_tests_with_failure += 1

            # pt3 test failure message
            if pt3_fail:
                self.system_tst["status"] = "caution"
                self.system_tst["messages"].append(
                    [
                        self.tr("System Test: One or more PT3 tests in the system test indicate potential EMI") + ";",
                        2,
                        3,
                    ]
                )
                guidance_text = self.tr("A failed PT3 test indicates there is potential electromagnetic interference. Errors in measured velocities caused by EMI tend to be a consistent bias (not related to true water velocity), so errors will be a greater percentage in lower velocities. EMI is more likely to occur on a StreamPro ADCP. To determine if EMI is affecting the measurement look for unusual patterns in the measured velocities, such as, higher velocities near the streambed. If EMI appears to be affecting the measurement a different measurement site should be selected or the measurement at this site should be made with a different instrument.")
                self.system_tst["guidance"].append(
                    self.guidance_prep(self.system_tst["messages"][-1][0], guidance_text))
                
            # Check for failed tests
            if num_tests_with_failure == len(meas.system_tst):
                # All tests had a failure
                self.system_tst["status"] = "warning"
                self.system_tst["messages"].append(
                    [
                        self.tr("SYSTEM TEST: All system test sets have at least one test that failed") + ";",
                        1,
                        3,
                    ]
                )
                guidance_text = self.tr(" If a system test fails, try repeating the test in calm water. If failures continue, proceed with the measurement and monitor the data closely. If the data appear valid, the measurement is probably OK. However, if this ADCP continues to fail system tests at other sites, the ADCP should be evaluated and potentially sent to the manufacturer for their evaluation and repair.")
                self.system_tst["guidance"].append(
                    self.guidance_prep(self.system_tst["messages"][-1][0], guidance_text))
            elif num_tests_with_failure > 0:
                self.system_tst["status"] = "caution"
                self.system_tst["messages"].append(
                    [
                        self.tr("System Test: One or more system test sets have at least one test that failed") + ";",
                        2,
                        3,
                    ]
                )
                guidance_text = self.tr("If a system test fails, try repeating the test in calm water. If at least one system test passed, the system is likely working properly. Always proceed with the measurement and monitor the data closely. If the data appear valid, the measurement is probably valid.")
                self.system_tst["guidance"].append(
                    self.guidance_prep(self.system_tst["messages"][-1][0], guidance_text))

        for idx in meas.verticals_used_idx:
            if meas.verticals[idx].data.inst is not None:
                if meas.verticals[idx].data.inst.t_matrix.source == "Nominal":
                    # This secondary check is necessary due to an earlier QRev bug
                    # that indicated the source as Nominal even though it was a
                    # custom matrix obtained from the ADCP. Thus loading a measurement
                    # saved from a earlier version could create a false alert.
                    # RiverRay matrices are always a standard matrix based on 30 degree
                    # beams, but other TRDI ADCPs should be different from their
                    # standard matrix based on 20 degree beams.
                    # This secondary check is not necessary for Sontek ADCPs.
                    if (
                        meas.verticals[idx].data.inst.model != "RiverRay"
                        and meas.verticals[idx].data.inst.manufacturer == "TRDI"
                    ):
                        nominal_matrix = [
                            [1.4619, -1.4619, 0, 0],
                            [0, 0, -1.4619, 1.4619],
                            [0.2661, 0.2661, 0.2661, 0.2661],
                            [1.0337, 1.0337, -1.0337, -1.0337],
                        ]
                        if np.allclose(
                            nominal_matrix,
                            meas.verticals[idx].data.inst.t_matrix.matrix,
                        ):
                            self.system_tst["status"] = "caution"
                            self.system_tst["messages"].append(
                                [
                                    self.tr("System Test: ADCP is using a nominal matrix rather than a custom matrix") + ";",
                                    2,
                                    3,
                                ]
                            )
                            guidance_text = self.tr("Most ADCPs have a custom transformation matrix (except for the RiverRay). If this ADCP has a nominal matrix, check the instruments history log to determine if it ever had a custom matrix. It may also be appropriate to contact the manufacturer to determine the transformation matrix for that ADCP serial number.")
                            self.system_tst["guidance"].append(
                                self.guidance_prep(self.system_tst["messages"][-1][0],
                                                   guidance_text))
                    break

    def compass_qa(self, meas):
        """Apply QA checks to compass calibration and evaluation.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        # Initialize variable as if ADCP has no compass
        self.compass["messages"] = []
        self.compass["guidance"] = []
        self.compass["status"] = "inactive"
        self.compass["status1"] = "good"
        self.compass["status2"] = "good"
        self.compass["magvar"] = 0
        self.compass["magvar_idx"] = []
        self.compass["mag_error_idx"] = []
        self.compass["pitch_mean_warning_idx"] = []
        self.compass["pitch_mean_caution_idx"] = []
        self.compass["pitch_std_caution_idx"] = []
        self.compass["roll_mean_warning_idx"] = []
        self.compass["roll_mean_caution_idx"] = []
        self.compass["roll_std_caution_idx"] = []

        # Create a data frame having only measured verticals
        df = meas.summary[meas.summary["Duration"] > 0]
        if not df.empty:
            df = df.drop("Total")

            # Determine if the ADCP has a compass
            compass_present = False
            heading_present_list = []
            for idx in meas.verticals_used_idx[1:-1]:
                if meas.verticals[idx].sampling_duration_sec > 0:
                    heading_data = getattr(
                        meas.verticals[idx].data.sensors.heading_deg,
                        "internal",
                    )
                    if np.any(heading_data.data != 0):
                        compass_present = True
                heading_present_list.append(compass_present)

            if len(set(heading_present_list)) > 1:
                idxs = np.where(np.array(heading_present_list) == False)[0]
                idxs = list(idxs + 1)
                self.compass["status2"] = "caution"
                self.compass["messages"].append(
                    [
                        self.tr("Compass: Stations {} do not have heading data") + ";".format(idxs),
                        2,
                        4,
                    ]
                )
                guidance_text = self.tr("Heading data are available for some but not all stations. If heading data are available for some verticals it should typically be available for all stations unless it was intentionally turned off. Check for instrument failure, communications issues, or configuration changes. ")
                self.compass["guidance"].append(
                    self.guidance_prep(self.compass["messages"][-1][0], guidance_text))

            if compass_present:
                # Determine manufacturer
                manufacturer = ""
                for idx in meas.verticals_used_idx:
                    if meas.verticals[idx].data.inst is not None:
                        manufacturer = meas.verticals[idx].data.inst.manufacturer
                        break

                # A compass calibration is required if the azimuth method is used
                new_df = df[df["Velocity Method"].str.contains("Azimuth")]
                if len(new_df.index) > 0:
                    # Compass calibration is required
                    if manufacturer == "SonTek":
                        # SonTek ADCP
                        if len(meas.compass_cal) == 0:
                            # No compass calibration
                            self.compass["status1"] = "warning"
                            self.compass["messages"].append(
                                [self.tr("COMPASS: No compass calibration") + ";", 1, 4]
                            )
                            guidance_text = self.tr("The azimuth method requires an accurate azimuth and flow direction at each station. Both requirements require accurate headings. A compass calibration should be completed prior to collecting the data. If in the field, calibrate the compass and recollect the data or provide justification as to why the compass was not calibrated.")
                            self.compass["guidance"].append(
                                self.guidance_prep(self.compass["messages"][-1][0],
                                                   guidance_text))
                        elif meas.compass_cal[-1].result["compass"]["error"] == "N/A":
                            # If the error cannot be decoded from the calibration
                            # assume the calibration is good
                            self.compass["status1"] = "good"
                        else:
                            if meas.compass_cal[-1].result["compass"]["error"] <= 0.2:
                                self.compass["status1"] = "good"
                            else:
                                self.compass["status1"] = "caution"
                                self.compass["messages"].append(
                                    [self.tr("Compass: Calibration result > 0.2 deg") + ";", 2, 4]
                                )
                                guidance_text = self.tr("The azimuth method requires an accurate azimuth and flow direction at each station. Both requirements require accurate headings. The compass calibration results do not meet the recommended minimum. Try recalibrating the compass (up to 3 times) near the measurement section but away from any magnetic interference. Your cell phone, keys, belt buckle are potential sources of interference if you are holding the ADCP. After 3 attempts that fail to be below 0.2 degree, document the results and proceed with the measurement.")
                                self.compass["guidance"].append(
                                    self.guidance_prep(self.compass["messages"][-1][0],
                                                       guidance_text))

                    elif manufacturer == "TRDI":
                        # TRDI ADCP
                        if len(meas.compass_cal) == 0:
                            # No compass calibration
                            if len(meas.compass_eval) == 0:
                                # No calibration or evaluation
                                self.compass["status1"] = "warning"
                                self.compass["messages"].append(
                                    [
                                        self.tr("COMPASS: No compass calibration or evaluation") + ";",
                                        1,
                                        4,
                                    ]
                                )
                                guidance_text = self.tr("The azimuth method requires an accurate azimuth and flow direction at each station. Both requirements require accurate headings. A compass calibration should be completed prior to collecting the data. If in the field, calibrate the compass and recollect the data or provide justification as to why the compass was not calibrated.")
                                self.compass["guidance"].append(
                                    self.guidance_prep(self.compass["messages"][-1][0],
                                                       guidance_text))
                            else:
                                # No calibration but an evaluation was completed
                                self.compass["status1"] = "caution"
                                self.compass["messages"].append(
                                    [self.tr("Compass: No compass calibration") + ";", 2, 4]
                                )
                                guidance_text = self.tr("If the evaluation result is < 1 degree a calibration is probably not necessary, and the headings should be accurate. However, if the evaluation is greater than 1 degree a compass calibration should be completed. In the office, carefully evaluate the measurement.")
                                self.compass["guidance"].append(
                                    self.guidance_prep(self.compass["messages"][-1][0],
                                                       guidance_text))
                        else:
                            # Compass was calibrated
                            if len(meas.compass_eval) == 0:
                                # No compass evaluation
                                self.compass["status1"] = "caution"
                                self.compass["messages"].append(
                                    [self.tr("Compass: No compass evaluation") + ";", 2, 4]
                                )
                                guidance_text = self.tr("A compass evaluation provides information on the quality of the calibration. If in the field, complete an evaluation, even if it is after the measurement. In the office, carefully evaluate the measurement.")
                                self.compass["guidance"].append(
                                    self.guidance_prep(self.compass["messages"][-1][0],
                                                       guidance_text))
                            else:
                                # Check results of evaluation
                                try:
                                    if (
                                        float(
                                            meas.compass_eval[-1].result["compass"][
                                                "error"
                                            ]
                                        )
                                        <= 1
                                    ):
                                        self.compass["status1"] = "good"
                                    else:
                                        self.compass["status1"] = "caution"
                                        self.compass["messages"].append(
                                            [
                                                self.tr("Compass: Evaluation result > 1 deg") + ";",
                                                2,
                                                4,
                                            ]
                                        )
                                        guidance_text = self.tr("If in the field, try to recalibrate the compass (up to 3 times) near the measurement section but away from any magnetic interference. Your cell phone, keys, belt buckle are potential sources of interference if you are holding the ADCP. After 3 attempts that fail to be below 1 degree, document the results and proceed with the measurement.")
                                        self.compass["guidance"].append(
                                            self.guidance_prep(
                                                self.compass["messages"][-1][0],
                                                guidance_text))
                                except ValueError:
                                    self.compass["status1"] = "good"
                else:
                    # Compass not required
                    if len(meas.compass_cal) == 0 and len(meas.compass_eval) == 0:
                        # No compass calibration or evaluation
                        self.compass["status1"] = "default"
                    else:
                        # Compass was calibrated and evaluated
                        self.compass["status1"] = "good"

                # Check for consistent magvar and pitch and roll mean and variation
                mag_error_exceeded = []
                pitch_exceeded = []
                roll_exceeded = []

                # SonTek G3 compass provides pitch, roll, and magnetic error
                # parameters that can be checked
                if manufacturer == "SonTek":
                    for n, idx in enumerate(meas.verticals_used_idx):
                        if meas.verticals[idx].sampling_duration_sec > 0:
                            heading_source_selected = getattr(
                                meas.verticals[idx].data.sensors.heading_deg,
                                meas.verticals[idx].data.sensors.heading_deg.selected,
                            )
                            pitch_source_selected = getattr(
                                meas.verticals[idx].data.sensors.pitch_deg,
                                meas.verticals[idx].data.sensors.pitch_deg.selected,
                            )
                            roll_source_selected = getattr(
                                meas.verticals[idx].data.sensors.roll_deg,
                                meas.verticals[idx].data.sensors.roll_deg.selected,
                            )

                            if heading_source_selected.pitch_limit is not None:
                                # Check for bug in SonTek data where pitch and roll
                                # was n x 3 use n x 1
                                if len(pitch_source_selected.data.shape) == 1:
                                    pitch_data = pitch_source_selected.data
                                else:
                                    pitch_data = pitch_source_selected.data[:, 0]
                                idx_max = np.where(
                                    pitch_data > heading_source_selected.pitch_limit[0]
                                )[0]
                                idx_min = np.where(
                                    pitch_data < heading_source_selected.pitch_limit[1]
                                )[0]
                                if len(idx_max) > 0 or len(idx_min) > 0:
                                    pitch_exceeded.append(
                                        meas.summary["Station Number"][n]
                                    )

                            if heading_source_selected.roll_limit is not None:
                                if len(roll_source_selected.data.shape) == 1:
                                    roll_data = roll_source_selected.data
                                else:
                                    roll_data = roll_source_selected.data[:, 0]
                                idx_max = np.where(
                                    roll_data > heading_source_selected.pitch_limit[0]
                                )[0]
                                idx_min = np.where(
                                    roll_data < heading_source_selected.pitch_limit[1]
                                )[0]
                                if len(idx_max) > 0 or len(idx_min) > 0:
                                    roll_exceeded.append(
                                        meas.summary["Station Number"][n]
                                    )

                            if heading_source_selected.mag_error is not None:
                                idx_max = np.where(
                                    heading_source_selected.mag_error > 2
                                )[0]
                                if len(idx_max) > 0:
                                    mag_error_exceeded.append(
                                        meas.summary["Station Number"][n]
                                    )

                # Check magvar consistency
                if len(np.unique(df["MagVar"])) > 1:
                    self.compass["status2"] = "caution"
                    self.compass["messages"].append(
                        [
                            self.tr("Compass: Magnetic variation is not consistent among stations") + ";",
                            2,
                            4,
                        ]
                    )
                    guidance_text = self.tr("The magnetic variation is site dependent and should be the same for all stations in a measurement. The magnetic variation should not be changed to account for compass errors. Using an app on your phone, site information, and/or an internet search enter and appropriate magnetic variation for this site.")
                    self.compass["guidance"].append(
                        self.guidance_prep(self.compass["messages"][-1][0],
                                           guidance_text))
                    self.compass["magvar"] = 1

                # Check magvar consistency
                if len(np.unique(df["Beam Misalignment"])) > 1:
                    self.compass["status2"] = "caution"
                    self.compass["messages"].append(
                        [
                            self.tr("Compass: Beam 3 misalignment is not consistent among stations") + ";",
                            2,
                            4,
                        ]
                    )
                    guidance_text = self.tr("The beam misalignment is used when the manufacturer specified reference beam is not pointed in the upstream direction for the fixed velocity method. Document the reason for the variation.")
                    self.compass["guidance"].append(
                        self.guidance_prep(self.compass["messages"][-1][0],
                                           guidance_text))
                    self.compass["align"] = 1

                # Check pitch mean
                self.compass["pitch_mean_warning_idx"] = df[df["Mean Pitch"].abs() > 8][
                    "Station Number"
                ].tolist()
                if len(self.compass["pitch_mean_warning_idx"]) > 0:
                    self.compass["status2"] = "warning"
                    self.compass["messages"].append(
                        [
                            self.tr("PITCH: Stations ")
                            + str(self.compass["pitch_mean_warning_idx"])
                            + self.tr(" have a mean pitch > 8 deg") + ";",
                            1,
                            4,
                        ]
                    )
                    guidance_text = self.tr("A consistent pitch is usually the result of a poor mount or the upward tension on the tether of a tethered boat. Adjust the mount to reduce the pitch or add a weight onto the tether near the tether boat to reduce the pitch.")
                    self.compass["guidance"].append(
                        self.guidance_prep(self.compass["messages"][-1][0],
                                           guidance_text))

                self.compass["pitch_mean_caution_idx"] = df[df["Mean Pitch"].abs() > 4][
                    "Station Number"
                ].tolist()
                if len(self.compass["pitch_mean_caution_idx"]) > 0:
                    if self.compass["status2"] == "good":
                        self.compass["status2"] = "caution"
                    self.compass["messages"].append(
                        [
                            self.tr("Pitch: Stations ")
                            + str(self.compass["pitch_mean_caution_idx"])
                            + self.tr(" have a mean pitch > 4 deg") + ";",
                            2,
                            4,
                        ]
                    )
                    guidance_text = self.tr("A consistent pitch is usually the result of a poor mount or the upward tension on the tether of a tethered boat. Adjust the mount to reduce the pitch or add a weight onto the tether near the tether boat to reduce the pitch.")
                    self.compass["guidance"].append(
                        self.guidance_prep(self.compass["messages"][-1][0],
                                           guidance_text))

                # Check roll mean
                self.compass["roll_mean_warning_idx"] = df[df["Mean Roll"].abs() > 8][
                    "Station Number"
                ].tolist()
                if len(self.compass["roll_mean_warning_idx"]) > 0:
                    self.compass["status2"] = "warning"
                    self.compass["messages"].append(
                        [
                            self.tr("ROLL: Stations ")
                            + str(self.compass["roll_mean_warning_idx"])
                            + self.tr(" have a mean roll > 8 deg") + ";",
                            1,
                            4,
                        ]
                    )
                    guidance_text = self.tr("A consistent roll is usually due to a poor mount or unevenly distributed weight on the boat (manned, tethered, or remote-control). Correct the mount or weight distribution.")
                    self.compass["guidance"].append(
                        self.guidance_prep(self.compass["messages"][-1][0],
                                           guidance_text))

                self.compass["roll_mean_caution_idx"] = df[df["Mean Roll"].abs() > 4][
                    "Station Number"
                ].tolist()
                if len(self.compass["roll_mean_caution_idx"]) > 0:
                    if self.compass["status2"] == "good":
                        self.compass["status2"] = "caution"
                    self.compass["messages"].append(
                        [
                            self.tr("Roll: Stations ")
                            + str(self.compass["roll_mean_caution_idx"])
                            + self.tr(" have a mean roll > 4 deg") + ";",
                            2,
                            4,
                        ]
                    )
                    guidance_text = self.tr("A consistent roll is usually due to a poor mount or unevenly distributed weight on the boat (manned, tethered, or remote-control). Correct the mount or weight distribution.")
                    self.compass["guidance"].append(
                        self.guidance_prep(self.compass["messages"][-1][0],
                                           guidance_text))

                # Check pitch standard deviation
                self.compass["pitch_std_caution_idx"] = df[
                    df["Pitch StdDev"].abs() > 5
                ]["Station Number"].tolist()
                if len(self.compass["pitch_std_caution_idx"]) > 0:
                    if self.compass["status2"] == "good":
                        self.compass["status2"] = "caution"
                    self.compass["messages"].append(
                        [
                            self.tr("Pitch: Stations ")
                            + str(self.compass["pitch_std_caution_idx"])
                            + self.tr(" have a pitch standard deviation > 5 deg") + ";",
                            2,
                            4,
                        ]
                    )
                    guidance_text = self.tr("Variable pitch can cause inaccuracies in the measured water and bottom track. Carefully review the water velocities. Check the uncertainty model for the uncertainty of the water velocity.")
                    self.compass["guidance"].append(
                        self.guidance_prep(self.compass["messages"][-1][0],
                                           guidance_text))

                # Check roll standard deviation
                self.compass["roll_std_caution_idx"] = df[df["Roll StdDev"].abs() > 5][
                    "Station Number"
                ].tolist()
                if len(self.compass["roll_std_caution_idx"]) > 0:
                    if self.compass["status2"] == "good":
                        self.compass["status2"] = "caution"
                    self.compass["messages"].append(
                        [
                            self.tr("Roll: Stations ")
                            + str(self.compass["roll_std_caution_idx"])
                            + self.tr(" have a roll standard deviation > 5 deg") + ";",
                            2,
                            4,
                        ]
                    )
                    guidance_text = self.tr("Variable roll can cause inaccuracies in the measured water and bottom track. Carefully review the water velocities. Check the uncertainty model for the uncertainty of the water velocity.")
                    self.compass["guidance"].append(
                        self.guidance_prep(self.compass["messages"][-1][0],
                                           guidance_text))

                # Additional checks for SonTek G3 compass
                if manufacturer == "SonTek":
                    # Check if pitch limits were exceeded
                    if any(pitch_exceeded):
                        if self.compass["status2"] == "good":
                            self.compass["status2"] = "caution"
                        self.compass["messages"].append(
                            [
                                self.tr("Compass: Stations ")
                                + str(pitch_exceeded)
                                + self.tr(" have pitch exceeding calibration limits") + ";",
                                2,
                                4,
                            ]
                        )
                        guidance_text = self.tr("Exceeding the pitch range from the compass calibration can result in inaccurate headings. If the exceedance is small the inaccuracies are likely small. If they are large and you are in the field, recalibrate the compass using an appropriate pitch range. If in the office, look at the Compass/P/R tab and see if there is an unexpected change in heading with a change in pitch beyond the limits.")
                        self.compass["guidance"].append(
                            self.guidance_prep(self.compass["messages"][-1][0],
                                               guidance_text))

                    # Check if roll limits were exceeded
                    if any(roll_exceeded):
                        if self.compass["status2"] == "good":
                            self.compass["status2"] = "caution"
                        self.compass["messages"].append(
                            [
                                self.tr("Compass: Stations ")
                                + str(roll_exceeded)
                                + self.tr(" have roll exceeding calibration limits") + ";",
                                2,
                                4,
                            ]
                        )
                        guidance_text = self.tr("Exceeding the roll range from the compass calibration can result in inaccurate headings. If the exceedance is small the inaccuracies are likely small. If they are large and you are in the field, recalibrate the compass using an appropriate roll range. If in the office, look at the Compass/P/R tab and see if there is an unexpected change in heading with a change in roll beyond the limits.")
                        self.compass["guidance"].append(
                            self.guidance_prep(self.compass["messages"][-1][0],
                                               guidance_text))

                    # Check if magnetic error was exceeded
                    self.compass["mag_error_idx"] = []
                    if len(mag_error_exceeded) > 0:
                        self.compass["mag_error_idx"] = np.array(mag_error_exceeded)
                        if self.compass["status2"] == "good":
                            self.compass["status2"] = "caution"
                        self.compass["messages"].append(
                            [
                                self.tr("Compass: Stations ")
                                + str(mag_error_exceeded)
                                + self.tr(" have a change in mag field exceeding 2%") + ";",
                                2,
                                4,
                            ]
                        )
                        guidance_text = self.tr("The G3 compass evaluates the strength of the magnetic field during calibration and during collection of station data. A change in magnetic field greater than 2% during a data collection indicates the magnetic field has changed from that measured during the compass calibration due to magnetic interference. Using the Compass/P/R tab look at the heading time series plot for changes in heading that correlate with changes in the magnetic field. If the interference is substantial, consider moving up or down stream away from the source of the interference.")
                        self.compass["guidance"].append(
                            self.guidance_prep(self.compass["messages"][-1][0],
                                               guidance_text))

                # Check for azimuth method compass reference
                for idx in meas.verticals_used_idx:
                    if meas.verticals[idx].velocity_method == "Azimuth":
                        if meas.verticals[idx].data.sensors is not None:
                            if np.abs(meas.verticals[idx].data.sensors.heading_deg.internal.mag_var_deg) > 0.0:
                                if self.compass["status2"] == "good":
                                    self.compass["status2"] = "caution"
                                self.compass["messages"].append(
                                    [
                                    self.tr("Compass: Headings are referenced to true north."), 2, 4
                                        ]
                                )
                                guidance_text = self.tr("The velocity data and the azimuth must have the same heading reference. The velocity data are referenced to true north. Therefore, the azimuth must also be referenced to true north. Make sure the azimuth is referenced to true north or remove the magnetic variation so that the measured velocities are referenced to magnetic north.")
                                self.compass["guidance"].append(
                                    self.guidance_prep(self.compass["messages"][-1][0], guidance_text))
                                break
                if (
                    self.compass["status1"] == "warning"
                    or self.compass["status2"] == "warning"
                ):
                    self.compass["status"] = "warning"
                elif (
                    self.compass["status1"] == "caution"
                    or self.compass["status2"] == "caution"
                ):
                    self.compass["status"] = "caution"
                else:
                    self.compass["status"] = "good"
            else:
                self.compass["status"] = "inactive"

    def temperature_qa(self, meas):
        """Apply QA checks to temperature.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.temperature["messages"] = []
        self.temperature["guidance"] = []
        check = [0, 0]

        # Create array of all temperatures
        temp, serial_time = meas.compute_time_series(meas, "Temperature")

        # Check temperature range
        if np.size(temp):
            temp_range = np.nanmax(temp) - np.nanmin(temp)
        else:
            temp_range = 0

        if temp_range > 2:
            check[0] = 3
            self.temperature["messages"].append(
                [
                    self.tr("TEMPERATURE: Temperature range is ")
                    + "{:3.1f}".format(temp_range)
                    + self.tr(" degrees C which is greater than 2 degrees") + ";",
                    1,
                    5,
                ]
            )
            guidance_text = self.tr("It is likely that the ADCP was not allowed to equilibrate to the water temperature prior to starting the measurement. However, it is also possible, though rare, that the water temperature is different from one side of the river to the other. If the temperature changes during the measurement and reaches an equilibrium value, then the ADCP was not given sufficient time to equilibrate. If the equilibrated ADCP temperature is close to the independent water temperature, change the water temperature source to user and enter either the independent temperature or the equilibrated ADCP temperature.")
            self.temperature["guidance"].append(
                self.guidance_prep(self.temperature["messages"][-1][0], guidance_text))
        elif temp_range > 1:
            check[0] = 2
            self.temperature["messages"].append(
                [
                    self.tr("Temperature: Temperature range is ")
                    + "{:3.1f}".format(temp_range)
                    + self.tr(" degrees C which is greater than 1 degree") + ";",
                    2,
                    5,
                ]
            )
            guidance_text = self.tr("It is likely that the ADCP was not allowed to equilibrate to the water temperature prior to starting the measurement. However, it is also possible, though rare, that the water temperature is different from one side of the river to the other. If the temperature changes during the measurement and reaches an equilibrium value, then the ADCP was not given sufficient time to equilibrate. If the equilibrated ADCP temperature is close to the independent water temperature, change the water temperature source to user and enter either the independent temperature or the equilibrated ADCP temperature.")
            self.temperature["guidance"].append(
                self.guidance_prep(self.temperature["messages"][-1][0], guidance_text))
        else:
            check[0] = 1

        # Check for independent temperature reading
        if "user" in meas.temp_chk:
            try:
                user = float(meas.temp_chk["user"])
            except (ValueError, TypeError):
                user = None
            if user is None or np.isnan(user):
                # No independent temperature reading
                check[1] = 2
                self.temperature["messages"].append(
                    [self.tr("Temperature: No independent temperature reading") + ";", 2, 5]
                )
                guidance_text = self.tr("The temperature measured by the ADCP cannot be verified without an independent temperature. The water temperature is critical to computing the speed of sound and the velocity from the Doppler shift. If still in the field, collect an independent water temperature. If in the office, look for other measurements using this ADCP and check that the ADCP temperature agrees with the water temperature in other measurements.")
                self.temperature["guidance"].append(
                    self.guidance_prep(self.temperature["messages"][-1][0],
                                       guidance_text))
            elif not np.isnan(meas.temp_chk["adcp"]):
                # Compare user to manually entered ADCP temperature
                diff = np.abs(user - meas.temp_chk["adcp"])
                if diff < 2:
                    check[1] = 1
                else:
                    check[1] = 3
                    self.temperature["messages"].append(
                        [
                            self.tr("TEMPERATURE: The difference between ADCP and reference is > 2") + ":  "
                            + "{:3.1f}".format(diff)
                            + " C;",
                            1,
                            5,
                        ]
                    )
                    guidance_text = self.tr("Ensure that the ADCP has had time to equilibrate to the water temperature and make another comparison. If the difference is still greater than 2 degrees, continue with the measurement. However, check the independent temperature source against another temperature source. If the independent temperature reading is correct, change the temperature source to user and enter the independent temperature source and reprocess the measurement.")
                    self.temperature["guidance"].append(
                        self.guidance_prep(self.temperature["messages"][-1][0],
                                           guidance_text))
            else:
                # Compare user to mean of all temperature data
                diff = np.abs(user - np.nanmean(temp))
                if diff < 2:
                    check[1] = 1
                else:
                    check[1] = 3
                    self.temperature["messages"].append(
                        [
                            self.tr("TEMPERATURE: The difference between ADCP and reference is > 2") + ":  "
                            + "{:3.1f}".format(diff)
                            + " C;",
                            1,
                            5,
                        ]
                    )
                    guidance_text = self.tr("Ensure that the ADCP has had time to equilibrate to the water temperature and make another comparison. If the difference is still greater than 2 degrees, continue with the measurement. However, check the independent temperature source against another temperature source. If the independent temperature reading is correct, change the temperature source to user and enter the independent temperature source and reprocess the measurement.")
                    self.temperature["guidance"].append(
                        self.guidance_prep(self.temperature["messages"][-1][0],
                                           guidance_text))

        # Assign temperature status
        max_check = max(check)
        if max_check == 1:
            self.temperature["status"] = "good"
        elif max_check == 2:
            self.temperature["status"] = "caution"
        elif max_check == 3:
            self.temperature["status"] = "warning"

    def user_qa(self, meas):
        """Apply quality checks to user input data.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.user["messages"] = []
        self.user["guidance"] = []
        self.user["status"] = "good"

        # Check for Station Name
        self.user["sta_name"] = False
        if meas.station_name is None or len(meas.station_name.strip()) < 1:
            self.user["messages"].append([self.tr("Site Info: Site name not entered") + ";", 2, 2])
            guidance_text = self.tr("Enter site name or description of the site location to identify the location of this measurement.")
            self.user["guidance"].append(
                self.guidance_prep(self.user["messages"][-1][0], guidance_text))
            self.user["status"] = "caution"
            self.user["sta_name"] = True

        # Check for Station Number
        self.user["sta_number"] = False
        try:
            if meas.station_number is None or len(meas.station_number.strip()) < 1:
                self.user["messages"].append(
                    [self.tr("Site Info: Site number not entered") + ";", 2, 2]
                )
                guidance_text = self.tr("If the measurement was made at numbered gauging site, enter the site number.")
                self.user["guidance"].append(
                    self.guidance_prep(self.user["messages"][-1][0], guidance_text))
                self.user["status"] = "caution"
                self.user["sta_number"] = True
        except AttributeError:
            self.user["messages"].append([self.tr("Site Info: Site number not entered") + ";", 2, 2])
            guidance_text = self.tr("If the measurement was made at numbered gauging site, enter the site number.")
            self.user["guidance"].append(
                self.guidance_prep(self.user["messages"][-1][0], guidance_text))
            self.user["status"] = "caution"
            self.user["sta_number"] = True

        # Time zone
        self.user["time_zone"] = False
        if meas.time_zone_required:
            if len(meas.time_zone) == 0:
                self.user["messages"].append([self.tr(
                    "Time Zone: Your agency requires the time zone to be entered") + ";",
                    2, 2, ])
                guidance_text = self.tr(
                    "Your agency requires the time zone to be entered. Select the appropriate time zone from the list on the Premeasurement tab.")
                self.user["guidance"].append(
                    self.guidance_prep(self.user["messages"][-1][0], guidance_text))
                self.user["status"] = "caution"
                self.user["time_zone"] = True

    def stations_qa(self, meas):
        """Apply quality checks to stations.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """
        
        self.stations["messages"] = []
        self.stations["guidance"] = []

        # Default
        if len(meas.stations_inserts) > 0:
            for message in meas.stations_inserts:
                self.stations["messages"].append(message)
                guidance_text = self.tr("A manual vertical has been inserted. Ensure the values are correct and document the reason for a manual vertical and justification for the estimated values.")
                self.stations["guidance"].append(
                    self.guidance_prep(self.stations["messages"][-1][0], guidance_text))
        if len(self.stations["messages"]) > 0:
            self.stations["status"] = "caution"
        else:
            self.stations["status"] = "good"

        # Find any stations not used
        not_used = []

        for vertical in meas.verticals:
            if not vertical.use:
                not_used.append(np.round(vertical.location_m, 2))

        # If some station aren't used report caution
        if len(not_used) > 0:
            self.stations["status"] = "caution"
            self.stations["messages"].append(
                [
                    self.tr("Stations: Some locations are not being used"),
                    2,
                    1,
                ]
            )
            guidance_text = self.tr("Document the reason that data were collected at some stations but not used to compute the discharge.")
            self.stations["guidance"].append(
                self.guidance_prep(self.stations["messages"][-1][0], guidance_text))

        # Warning if mixed velocity methods are used
        vel_method = []
        vel_method_idx = []
        for idx in meas.verticals_used_idx:
            method_current = meas.verticals[idx].velocity_method
            method_original = meas.verticals[idx].velocity_method_orig
            vel_method.append(method_current)
            if method_original != method_current:
                if method_original == "Fixed" or method_current == "Fixed":
                    vel_method_idx.append(idx)
        if len(vel_method_idx) > 0 and len(np.unique(vel_method[1:-1])) > 1:
            self.stations["status"] = "warning"
            self.stations["messages"].append(
                [
                    self.tr("STATIONS: Mixed velocity methods are being used"), 1, 2
                ]
            )
            guidance_text = self.tr("It is unusual for the velocity method to be different among the stations. If the velocity method is not consistent among the stations, provide justification for the changes. If the azimuth method is used the azimuth and the heading data for each vertical need to be accurate. If the fixed method is used, the reference beam of the ADCP must be pointed upstream. If the magnitude method is used, the user should evaluate the heading data, if available and determine if the heading data should be used or not.")
            self.stations["guidance"].append(
                self.guidance_prep(self.stations["messages"][-1][0], guidance_text))

    def depths_qa(self, meas):
        """Apply quality checks to depth data.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        # Initialize variables
        self.depths["total_caution"] = []
        self.depths["total_warning"] = []
        self.depths["all_invalid"] = []
        self.depths["draft_zero"] = []
        self.depths["messages"] = []
        self.depths["guidance"] = []
        self.depths["status"] = "good"
        self.depths["draft"] = 0
        self.depths["consistency"] = []
        ice = False
        drafts = []

        for n, idx in enumerate(meas.verticals_used_idx):
            vertical = meas.verticals[idx]
            if vertical.sampling_duration_sec > 0 and vertical.depth_source != "Manual":
                # Evaluate draft for open water and ice
                if vertical.ws_condition == "Open":
                    # Check for zero draft
                    if (
                        np.isnan(vertical.adcp_depth_below_ws_m)
                        or vertical.adcp_depth_below_ws_m < 0.01
                    ):
                        self.depths["draft_zero"].append(
                            meas.verticals_used_idx.index(idx) + 1
                        )
                elif vertical.ws_condition == "Ice":
                    if (
                        np.isnan(vertical.adcp_depth_below_ws_m)
                        or vertical.adcp_depth_below_ice_m < 0.01
                    ):
                        self.depths["draft_zero"].append(
                            meas.verticals_used_idx.index(idx) + 1
                        )
                    # Identify any verticals that have ice. If ice condition
                    # drafts allowed to be different.
                    ice = True

                # Get selected depths
                depths_selected = getattr(
                    vertical.data.depths, vertical.data.depths.selected
                )
                drafts.append(depths_selected.draft_use_m)

                # Determine valid measured depths
                if vertical.data.depths.composite == "On":
                    depth_na = depths_selected.depth_source_ens != "NA"
                    depth_in = depths_selected.depth_source_ens != "IN"
                    depth_valid = np.all(np.vstack((depth_na, depth_in)), 0)
                else:
                    depth_valid_temp = depths_selected.valid_data
                    depth_nan = depths_selected.depth_processed_m != np.nan
                    depth_valid = np.all(np.vstack((depth_nan, depth_valid_temp)), 0)

                # Compute percent valid
                number_valid_ens = np.nansum(depth_valid)
                percent_valid = (number_valid_ens / depth_valid.shape[0]) * 100

                if not np.any(depth_valid):
                    self.depths["all_invalid"].append(
                        meas.verticals_used_idx.index(idx) + 1
                    )

                # Apply total interpolated threshold
                if (percent_valid < self.percent_valid_warning_threshold) and (
                    percent_valid > 0
                ):
                    self.depths["total_warning"].append(
                        meas.verticals_used_idx.index(idx) + 1
                    )
                elif percent_valid < self.percent_valid_caution_threshold:
                    self.depths["total_caution"].append(
                        meas.verticals_used_idx.index(idx) + 1
                    )

        if not ice:
            # If all verticals are open water check for consistent draft
            # Create array of all unique draft values
            draft_check = np.unique(np.round(drafts, 2))

            # Check draft consistency
            if len(draft_check) > 1:
                self.depths["status"] = "caution"
                self.depths["draft"] = 1
                self.depths["messages"].append(
                    [self.tr("Depth: Transducer depth is not consistent among stations") + ";", 2, 10]
                )
                guidance_text = self.tr("For an open water measurement, the depth of the transducer is typically set at the beginning of the measurement and not changed. If the change in the depth of the transducer is correct, provide documentation, if not, set the depth of transducer to the correct value.")
                self.depths["guidance"].append(
                    self.guidance_prep(self.depths["messages"][-1][0], guidance_text))

        if len(self.depths["draft_zero"]) > 0:
            self.depths["messages"].append(
                [
                    self.tr("DEPTH: Stations") + " {}".format(str(self.depths["draft_zero"]))
                    + self.tr("have a transducer depth that is too shallow, likely 0"),
                    1,
                    10,
                ]
            )
            guidance_text = self.tr("The transducer must be submerged; thus, a value less than 0.01 m is not reasonable. Set the correct depth of transducer. If this is an ice measurement the transducer draft must be greater than the water surface to bottom of ice/slush.")
            self.depths["guidance"].append(
                self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
            self.depths["status"] = "warning"

        # Check if all depths are invalid
        if np.any(self.depths["all_invalid"]):
            self.depths["messages"].append(
                [
                    self.tr("DEPTH: There are no valid depths for stations") + " {}".format(
                        str(self.depths["all_invalid"])
                    ),
                    1,
                    10,
                ]
            )
            guidance_text = self.tr("Review the depth filters and measured depths to ensure that the filter settings are appropriate. If there are no valid depths, a manual depth may be entered. If a manual depth is used, provide documentation on how the manual depth was determined. If no depth is available, uncheck the station in the Stations tab.")
            self.depths["guidance"].append(
                self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
            self.depths["status"] = "warning"

        # Check total criteria
        if np.any(self.depths["total_warning"]):
            self.depths["messages"].append(
                [
                    self.tr("DEPTH: Percent valid ensembles in stations") + " {}".format(str(self.depths["total_warning"])) +
                    self.tr("are less than") + " {} %;".format(self.percent_valid_warning_threshold),
                    1,
                    10,
                ]
            )
            guidance_text = self.tr("A substantial number of ensemble depths have been marked invalid. Using the graphics on the Depth tab identify the ensembles that are invalid. If data that you believe are valid have been marked invalid by the filter, consider changing the filter setting. Ensure that the valid data provide a good average depth. Comparing the average depth for this station to neighboring stations is a good way to evaluate if the average depth is reasonable. If in the field, consider collecting data for a longer duration or try collecting the vertical at slightly different location.")
            self.depths["guidance"].append(
                self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
            self.depths["status"] = "warning"
        if np.any(self.depths["total_caution"]):
            self.depths["messages"].append(
                [
                    self.tr("Depth: Percent valid ensembles in stations")
                    + " {}".format(str(self.depths["total_caution"]))
                    + self.tr(" is less than") + " {} %;".format(
                        self.percent_valid_caution_threshold),
                    2,
                    10,
                ]
            )
            guidance_text = self.tr("More than 50% of the depths are invalid. Using the graphics on the Depth tab identify the ensembles that the filter has marked invalid. If data that you believe are valid have been marked invalid by the filter, consider changing the filter setting. Ensure that the valid data provide a good average depth. Comparing the average depth for this station to neighboring stations is a good way to evaluate if the average depth is reasonable. If in the field, consider collecting data for a longer duration or try collecting the vertical at slightly different location.")
            self.depths["guidance"].append(
                self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
            if self.depths["status"] != "warning":
                self.depths["status"] = "caution"

        # Check consecutive verticals for depth consistency
        df = meas.summary[(np.logical_not(meas.summary["Station Number"].isna()))]

        start_idx = 2
        n_stations = len(df.index)
        delta_depth = []
        for i in range(start_idx, n_stations - start_idx):
            # Weighting
            wght = (df.iloc[i + 1]["Location"] - df.iloc[i]["Location"]) / (
                df.iloc[i + 1]["Location"] - df.iloc[i - 1]["Location"]
            )

            # Depth
            depth_est = (wght * df.iloc[i - 1]["Depth"]) + (
                (1 - wght) * (df.iloc[i + 1]["Depth"])
            )
            delta_depth.append(df.iloc[i]["Depth"] - depth_est)
        delta_depth = np.array(delta_depth)

        # Threshold
        threshold = np.nanmax(df["Depth"]) * 0.2
        # threshold = iqr(delta_depth) * 5
        idx_invalid = np.where(np.abs(delta_depth) > threshold)[0]
        if idx_invalid.size > 0:
            self.depths["consistency"] = idx_invalid + 3
            self.depths["consistency"] = self.depths["consistency"].tolist()
            self.depths["messages"].append(
                [
                    self.tr("Depth: Depths appear inconsistent for stations") + " {} ;".format(
                        str(self.depths["consistency"])
                    ),
                    2,
                    10,
                ]
            )
            guidance_text = self.tr("The average depth at this station appears to be substantially different from the neighboring stations. Evaluate the shape of the cross section using the color contour plot on the Main tab and the depth data for this station using the Depth tab. Provide documentation to justify the observed depth or if necessary, uncheck the station on the Stations tab so that it won’t be used when computing the discharge.")
            self.depths["guidance"].append(
                self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
            if self.depths["status"] != "warning":
                self.depths["status"] = "caution"

    def boat_qa(self, meas):
        """Apply quality checks to boat data.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        # Initialize variables
        data_type = {
            "BT": {
                "class": "bt_vel",
                "warning": "BT-",
                "caution": "Bt-",
                "filter": [
                    (self.tr("All") + ": ", 0),
                    (self.tr("Original") + ": ", 1),
                    (self.tr("ErrorVel") + ": ", 2),
                    (self.tr("VertVel") + ": ", 3),
                    (self.tr("3Beams") + ": ", 5),
                ],
            }
        }
        boat = dict()
        dt_key = ""
        dt_value = dict()

        for dt_key, dt_value in data_type.items():
            boat = getattr(self, dt_value["class"])

            # Initialize dictionaries for each data type
            boat["total_caution"] = {}
            boat["total_warning"] = {}
            boat["all_invalid"] = {}
            boat["messages"] = []
            boat["guidance"] = []
            boat["status"] = "inactive"

            # Check the results of each filter
            for dt_filter in dt_value["filter"]:
                boat["total_caution"][dt_filter[0]] = []
                boat["total_warning"][dt_filter[0]] = []
                boat["all_invalid"][dt_filter[0]] = []
                if boat["status"] != "caution" and boat["status"] != "warning":
                    boat["status"] = "good"

                # Quality check each transect
                for idx in meas.verticals_used_idx:
                    if meas.verticals[idx].sampling_duration_sec > 0:
                        transect = meas.verticals[idx].data

                        # Check to see if data are available for the data_type
                        if getattr(transect.boat_vel, dt_value["class"]) is not None:
                            # Compute quality characteristics
                            valid = getattr(
                                transect.boat_vel, dt_value["class"]
                            ).valid_data[dt_filter[1], :]
                            number_valid_ens = np.nansum(valid)
                            number_valid_percent = (
                                number_valid_ens / valid.shape[0]
                            ) * 100

                            # Check if all invalid
                            if dt_filter[1] == 0 and not np.any(valid):
                                boat["all_invalid"][dt_filter[0]].append(
                                    meas.verticals_used_idx.index(idx) + 1
                                )
                            # Apply total interpolated discharge threshold
                            elif (
                                number_valid_percent
                                < self.percent_valid_warning_threshold
                            ):
                                boat["total_warning"][dt_filter[0]].append(
                                    meas.verticals_used_idx.index(idx) + 1
                                )
                            elif (
                                number_valid_percent
                                < self.percent_valid_caution_threshold
                            ):
                                boat["total_caution"][dt_filter[0]].append(
                                    meas.verticals_used_idx.index(idx) + 1
                                )

        # Create message for total invalid

        stations_warning = []
        stations_caution = []
        for key in boat["total_warning"].keys():
            stations_warning.extend(boat["total_warning"][key])
            stations_caution.extend(boat["total_caution"][key])
        stations_warning = set(stations_warning)
        stations_caution = set(stations_caution)

        stations_caution = list(stations_caution - stations_warning)
        stations_warning = list(stations_warning - set(boat["all_invalid"]["All: "]))

        if len(stations_warning) > 0:
            if dt_key == "BT":
                module_code = 7
            else:
                module_code = 8
            boat["messages"].append(
                [
                    dt_value["warning"]
                    + self.tr("Percent valid ensembles in stations")
                    + " {} ".format(str(stations_warning))
                    + self.tr("are less than")
                    + " {} %;".format(self.percent_valid_warning_threshold),
                    1,
                    module_code,
                ]
            )
            guidance_text = self.tr("The filter identified in the message has resulted in more than 50% of the data being marked invalid. The remaining valid data may not be sufficient to obtain an acceptable average velocity. Using the graphics on the BT tab identify the ensembles that the filter has marked invalid. Ensure that the valid data provide a good average velocity. Comparing the average velocity for this station to neighboring stations is a good way to evaluate if the average velocity is reasonable. If in the field, consider collecting data for a longer duration or try collecting the vertical at slightly different location.")
            boat["guidance"].append(
                self.guidance_prep(boat["messages"][-1][0], guidance_text))
            boat["status"] = "warning"
        if len(stations_caution) > 0:
            if dt_key == "BT":
                module_code = 7
            else:
                module_code = 8
            boat["messages"].append(
                [
                    dt_value["caution"]
                    + self.tr("Percent valid ensembles in stations")
                    + " {} ".format(str(stations_caution))
                    + self.tr("are less than")
                    + " {} %;".format(self.percent_valid_caution_threshold),
                    2,
                    module_code,
                ]
            )
            guidance_text = self.tr(" The filter identified in the message has resulted in a substantial number of ensembles being marked invalid. Using the graphics on the BT tab identify the ensembles that the filter has marked invalid. Ensure that the valid data provide a good average velocity. Comparing the average velocity for this station to neighboring stations is a good way to evaluate if the average velocity is reasonable. If in the field, consider collecting data for a longer duration or try collecting the vertical at slightly different location.")
            boat["guidance"].append(
                self.guidance_prep(boat["messages"][-1][0], guidance_text))
            if boat["status"] != "warning":
                boat["status"] = "caution"

        # Create message for all data invalid
        if len(boat["all_invalid"]["All: "]) > 0:
            boat["status"] = "warning"
            if dt_key == "BT":
                module_code = 7
            else:
                module_code = 8
            boat["messages"].append(
                [
                    dt_value["warning"]
                    + self.tr("There are no valid data for stations ")
                    + str(boat["all_invalid"]["All: "])
                    + ";",
                    1,
                    module_code,
                ]
            )
            guidance_text = self.tr("The bottom track data are all invalid. Consider changing the velocity reference to None. Using a velocity reference of None will compute the average velocity without considering the boat velocity or movement. Generally, using None is the preferred velocity reference for collecting stationary data.")
            boat["guidance"].append(
                self.guidance_prep(boat["messages"][-1][0], guidance_text))
            boat["status"] = "warning"

        setattr(self, dt_value["class"], boat)

    def water_qa(self, meas):
        """Apply quality checks to water data.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        # Initialize filter labels and indices
        prefix = [self.tr("All") + ": ", self.tr("Original") + ": ", self.tr("ErrorVel") + ": ", self.tr("VertVel") + ": ", self.tr("3Beams") + ": ", self.tr("SNR") + ": "]
        idx = 1
        for idx in meas.verticals_used_idx:
            if meas.verticals[idx].data.inst is not None:
                break
        if meas.verticals[idx].data.inst.manufacturer == "TRDI":
            filter_index = [0, 1, 2, 3, 5]
        else:
            filter_index = [0, 1, 2, 3, 5, 7]

        # Initialize dictionaries for each data type
        self.w_vel["total_caution"] = {}
        self.w_vel["total_warning"] = {}
        self.w_vel["all_invalid"] = {}
        self.w_vel["messages"] = []
        self.w_vel["guidance"] = []
        self.w_vel["status"] = "good"
        self.w_vel["cells_caution"] = []
        self.w_vel["cells_warning"] = []

        # Create a data frame having only measured verticals
        df = meas.summary[meas.summary["Duration"] > 0]

        self.w_vel["cells_warning"] = df[df["Number Cells"] == 0][
            "Station Number"
        ].tolist()
        self.w_vel["cells_caution"] = df[df["Number Cells"] < 3][
            "Station Number"
        ].tolist()

        if len(self.w_vel["cells_warning"]) > 0:
            text = (
                self.tr("WT-Profile: Stations ")
                + str(self.w_vel["cells_caution"])
                + self.tr(" have 0 valid cells in the mean profile")
            )
            self.w_vel["messages"].append([text, 1, 11])
            guidance_text = self.tr(
                "Review the water track filters results in the graphics on the WT tab to ensure that the filter settings are appropriate. If there a less than the minimum number of ensembles for all the depths in the profile, consider lowering the minimum number of ensembles and evaluate if the result seems reasonable. Alternatively, a manual velocity may be entered. If a manual velocity is used, provide documentation on how the manual velocity was determined. If no velocity is available, uncheck the station in the Stations tab.")
            self.w_vel["guidance"].append(
                self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
            self.w_vel["status"] = "warning"

        elif len(self.w_vel["cells_caution"]) > 0:
            text = (
                "Wt-Profile: Stations "
                + str(self.w_vel["cells_caution"])
                + " have less than 3 valid cells in the mean profile"
            )
            self.w_vel["messages"].append([text, 2, 11])
            guidance_text = self.tr("Few valid cells will result in a default power/power extrapolation due to lack of data. Review the water track filters results in the graphics on the WT tab to ensure that the filter settings are appropriate. If there a less than the minimum number of ensembles for some the depths in the profile, consider lowering the minimum number of ensembles and evaluate if the result seems reasonable. ")
            self.w_vel["guidance"].append(
                self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
            
            self.w_vel["status"] = "caution"

        # Loop through filters
        for prefix_idx, filter_idx in enumerate(filter_index):
            self.w_vel["total_caution"][prefix[prefix_idx]] = []
            self.w_vel["total_warning"][prefix[prefix_idx]] = []
            self.w_vel["all_invalid"][prefix[prefix_idx]] = []

            # Loop through transects
            for idx in meas.verticals_used_idx:
                if meas.verticals[idx].sampling_duration_sec > 0:
                    valid_data = meas.verticals[idx].data.w_vel.valid_data
                    # Data excluded are not counted as measured cells with potentially valid data
                    num_meas_cells = np.nansum(
                        np.nansum(meas.verticals[idx].data.w_vel.valid_data[6, :, :])
                    )

                    # Compute quality characteristics
                    valid = np.logical_and(
                        valid_data[filter_idx, :, :],
                        meas.verticals[idx].data.w_vel.valid_data[6, :, :],
                    )
                    number_valid_cells = np.nansum(np.nansum(valid))
                    number_valid_percent = (number_valid_cells / num_meas_cells) * 100

                    # Check if all invalid
                    if filter_idx == 0 and not np.any(valid):
                        self.w_vel["all_invalid"][prefix[prefix_idx]].append(
                            meas.verticals_used_idx.index(idx) + 1
                        )

                    # Apply total interpolated discharge threshold
                    elif number_valid_percent < self.percent_valid_warning_threshold:
                        self.w_vel["total_warning"][prefix[prefix_idx]].append(
                            meas.verticals_used_idx.index(idx) + 1
                        )
                    elif number_valid_percent < self.percent_valid_caution_threshold:
                        self.w_vel["total_caution"][prefix[prefix_idx]].append(
                            meas.verticals_used_idx.index(idx) + 1
                        )

        # Generate messages
        stations_warning = []
        stations_caution = []
        for key in self.w_vel["total_warning"].keys():
            stations_warning.extend(self.w_vel["total_warning"][key])
            stations_caution.extend(self.w_vel["total_caution"][key])
        stations_warning = set(stations_warning)
        stations_caution = set(stations_caution)

        stations_caution = list(stations_caution - stations_warning)
        stations_warning = list(
            stations_warning - set(self.w_vel["all_invalid"]["All: "])
        )

        if len(stations_warning) > 0:
            self.w_vel["messages"].append(
                [
                    self.tr("WT-The percent of valid cells in stations")
                    + " {} ".format(str(stations_warning))
                    + self.tr("is less than")
                    + " {} %;".format(self.percent_valid_warning_threshold),
                    1,
                    11,
                ]
            )
            guidance_text = self.tr("The filter identified in the message has resulted in more than 50% of the data being marked invalid. The remaining valid data may not be sufficient to obtain an acceptable average velocity. Using the graphics on the WT tab identify the ensembles that the filter has marked invalid. Ensure that the valid data provide a good average velocity. Comparing the average velocity for this station to neighboring stations is a good way to evaluate if the average velocity is reasonable. If in the field, consider collecting data for a longer duration or try collecting the vertical at slightly different location.")
            self.w_vel["guidance"].append(
                self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
            self.w_vel["status"] = "warning"
        if len(stations_caution) > 0:
            self.w_vel["messages"].append(
                [
                    self.tr("Wt-The percent of valid cells in stations")
                    + " {} ".format(str(stations_caution))
                    + self.tr("are less than")
                    + " {} %;".format(self.percent_valid_caution_threshold),
                    2,
                    11,
                ]
            )
            guidance_text = self.tr("The filter identified in the message has resulted in a substantial number of ensembles being marked invalid. Using the graphics on the WT tab identify the cells and ensembles that the filter has marked invalid. Ensure that the valid data provide a good average velocity. Comparing the average velocity for this station to neighboring stations is a good way to evaluate if the average velocity is reasonable. If in the field, consider collecting data for a longer duration or try collecting the vertical at slightly different location.")
            self.w_vel["guidance"].append(
                self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
            if self.w_vel["status"] != "warning":
                self.w_vel["status"] = "caution"

        # Generate message for all invalid
        if len(self.w_vel["all_invalid"]["All: "]) > 0:
            self.w_vel["messages"].append(
                [
                    self.tr("WT-There are no valid data for stations") + " {}.".format(
                        str(self.w_vel["all_invalid"]["All: "])
                    ),
                    1,
                    11,
                ]
            )
            guidance_text = self.tr("Review the water track filters results in the graphics on the WT tab to ensure that the filter settings are appropriate. If there are no valid water track data, a manual velocity may be entered. If a manual velocity is used, provide documentation on how the manual velocity was determined. If no velocity is available, uncheck the station in the Stations tab.")
            self.w_vel["guidance"].append(
                self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
            self.w_vel["status"] = "warning"

    def extrapolation_qa(self, meas):
        """Apply quality checks to extrapolation methods

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.extrapolation["messages"] = []
        self.extrapolation["guidance"] = []
        self.extrapolation["status"] = "good"

        stations = []
        for n, idx in enumerate(meas.verticals_used_idx):
            vertical = meas.verticals[idx]
            if vertical.sampling_duration_sec > 0:
                sensitivity = vertical.extrapolation.extrap_fit.sensitivity

                if vertical.ws_condition == "Ice":
                    v_possible = np.array(
                        [sensitivity.v_ice_ns, sensitivity.v_ice_ns_opt]
                    )
                    v_diff = np.round(np.abs(v_possible - vertical.mean_speed_mps), 5)

                    # Sort differences
                    percent_diff = (
                        np.sort(v_diff) / np.abs(np.round(vertical.mean_speed_mps, 5))
                    ) * 100.0

                    # Estimate the uncertainty as the average of the differences
                    extrap_uncertainty = np.nanmean(percent_diff[percent_diff > 0])

                else:
                    # Create array of mean velocities from the various extrapolation methods
                    v_possible = np.array(
                        [
                            sensitivity.v_pp,
                            sensitivity.v_pp_opt,
                            sensitivity.v_cns,
                            sensitivity.v_cns_opt,
                            sensitivity.v_3p_ns,
                            sensitivity.v_3p_ns_opt,
                        ]
                    )

                    v_diff = np.abs(v_possible - vertical.mean_speed_mps)

                    # Sort differences
                    percent_diff = (
                        np.sort(v_diff) / np.abs(vertical.mean_speed_mps)
                    ) * 100.0

                    # Estimate the uncertainty as the average of the 4 smallest differences
                    extrap_uncertainty = np.nanmean(percent_diff[1:5])

                # Record stations with uncertainty greater than 2%
                if np.abs(extrap_uncertainty) > 2:
                    stations.append(n + 1)

        if len(stations) > 0:
            self.extrapolation["messages"].append(
                [
                    self.tr("Extrapolation: The extrapolation uncertainty is more than 2 percent for stations")
                    + " {};".format(str(stations)),
                    2,
                    12,
                ]
            )
            guidance_text = self.tr("The extrapolation method will have a substantial effect on the average velocity. Carefully evaluate various extrapolation methods and choose the method that best fits the data. Provide justification for the selected method in the comments.")
            self.extrapolation["guidance"].append(
                self.guidance_prep(self.extrapolation["messages"][-1][0], guidance_text))

            self.extrapolation["status"] = "caution"

    def edges_qa(self, meas):
        """Apply quality checks to edge estimates

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """
        # Set default status to good
        self.edges["status"] = "good"
        self.edges["messages"] = []
        self.edges["guidance"] = []

        # Edge discharge are typically zero for mid-section, but non-zero for mean section
        # if meas.discharge_method == "Mean":

        # Initialize variables
        start_idx = 0
        end_idx = -1

        # Check start edge q > 5%
        if meas.discharge["q_%"][start_idx] > 5:
            self.edges["status"] = "caution"
            self.edges["messages"].append(
                ["Edges: Start edge Q is greater than 5%;", 1, 13]
            )
            guidance_text = self.tr("It is recommended that an edge contain a maximum of 5% of the total discharge. If an edge contains more than 5% consider using a different cross section, if possible. If it is not possible, provide documentation, including if the edge discharge appears reasonable.")
            self.edges["guidance"].append(
                self.guidance_prep(self.edges["messages"][-1][0], guidance_text))

        if meas.discharge["q_%"][end_idx] > 5:
            self.edges["status"] = "caution"
            self.edges["messages"].append(
                ["Edges: End edge Q is greater than 5%;", 1, 13]
            )
            guidance_text = self.tr("It is recommended that an edge contain a maximum of 5% of the total discharge. If an edge contains more than 5% consider using a different cross section, if possible. If it is not possible, provide documentation, including if the edge discharge appears reasonable.")
            self.edges["guidance"].append(
                self.guidance_prep(self.edges["messages"][-1][0], guidance_text))

        # Check for consistent sign
        total_q = meas.summary["Q"]["Total"]
        if total_q > 0:
            positive = True
        else:
            positive = False

        if np.not_equal(meas.discharge["q_cms"][start_idx] >= 0, positive):
            self.edges["status"] = "caution"
            self.edges["messages"].append(
                [
                    "Edges: Sign of start edge Q is not consistent with sign of total Q;",
                    2,
                    13,
                ]
            )
            guidance_text = self.tr("The sign of the discharge for an edge is not consistent with the sign of the total discharge. This may happen if the velocities are very low and there is flow fluctuation. This could also happen if there is a flow reversal on the edge. If there is negative flow in the edge that flow should be captured by the first or last station.")
            self.edges["guidance"].append(
                self.guidance_prep(self.edges["messages"][-1][0], guidance_text))

        if np.not_equal(meas.discharge["q_cms"][end_idx] >= 0, positive):
            self.edges["status"] = "caution"
            self.edges["messages"].append(
                [
                    "Edges: Sign of end edge Q is not consistent with sign of total Q;",
                    2,
                    13,
                ]
            )
            guidance_text = self.tr("The sign of the discharge for an edge is not consistent with the sign of the total discharge. This may happen if the velocities are very low and there is flow fluctuation. This could also happen if there is a flow reversal on the edge. If there is negative flow in the edge that flow should be captured by the first or last station.")
            self.edges["guidance"].append(
                self.guidance_prep(self.edges["messages"][-1][0], guidance_text))

        if meas.discharge_method == "Mean":
            # Check edges for zero discharge
            if np.round(meas.discharge["q_cms"][start_idx], 5) == 0:
                self.edges["status"] = "warning"
                self.edges["messages"].append(["EDGES: Start edge has 0 Q;", 1, 13])
                guidance_text = self.tr("Edges typically do not have a zero discharge. Check that the edge location has been entered correctly and there are sufficient valid ensembles to compute a mean depth and velocity in the neighboring station. Provide documentation for any needed changes.")
                self.edges["guidance"].append(
                    self.guidance_prep(self.edges["messages"][-1][0], guidance_text))

            if np.round(meas.discharge["q_cms"][end_idx], 5) == 0:
                self.edges["status"] = "warning"
                self.edges["messages"].append(["EDGES: End edge has 0 Q;", 1, 13])
                guidance_text = self.tr("Edges typically do not have a zero discharge. Check that the edge location has been entered correctly and there are sufficient valid ensembles to compute a mean depth and velocity in the neighboring station. Provide documentation for any needed changes.")
                self.edges["guidance"].append(
                    self.guidance_prep(self.edges["messages"][-1][0], guidance_text))

        # Check that measurement has a start edge
        self.edges["start_edge"] = True
        if not meas.verticals[meas.verticals_used_idx[0]].is_edge:
            self.edges["status"] = "warning"
            self.edges["messages"].append(
                [
                    "EDGES: Measurement has no start edge",
                    1,
                    13,
                ]
            )
            guidance_text = self.tr("None of the stations have been defined as a start edge. Review the data and identify a start edge.")
            self.edges["guidance"].append(
                self.guidance_prep(self.edges["messages"][-1][0], guidance_text))
            self.edges["start_edge"] = False

        # Check that measurement has an end edge
        self.edges["end_edge"] = True
        if not meas.verticals[meas.verticals_used_idx[-1]].is_edge:
            self.edges["status"] = "warning"
            self.edges["messages"].append(
                [
                    "EDGES: Measurement has no end edge",
                    1,
                    13,
                ]
            )
            guidance_text = self.tr("None of the stations have been defined as an end edge. Review the data and identify an end edge.")
            self.edges["guidance"].append(
                self.guidance_prep(self.edges["messages"][-1][0], guidance_text))
            self.edges["end_edge"] = False

    def rating_qa(self, meas):
        """Evaluate if user rating is better than rating suggested by uncertainty."""

        uncertainty = np.nan
        if meas.uncertainty.method == "IVE" or meas.uncertainty.method == "IVE_User":
            if meas.uncertainty.method == "IVE":
                uncertainty = meas.uncertainty.u_ive["u95_q"] * 100
            else:
                uncertainty = meas.uncertainty.u_user["u95_q"] * 100
        elif meas.uncertainty.method == "ISO" or meas.uncertainty.method == "ISO_User":
            if meas.uncertainty.method == "ISO":
                uncertainty = meas.uncertainty.u_iso["u95_q"] * 100
            else:
                uncertainty = meas.uncertainty.u_user["u95_q"] * 100

        if uncertainty < 3:
            auto_rating = 1
        elif uncertainty < 5.01:
            auto_rating = 2
        elif uncertainty < 8.01:
            auto_rating = 3
        else:
            auto_rating = 4

        user_rating = {"Not Rated": 5, "Excellent": 1, "Good": 2, "Fair": 3, "Poor": 4}

        self.rating["messages"] = []
        self.rating["guidance"] = []
        self.rating["status"] = "good"
        if user_rating[meas.user_rating] < auto_rating:
            text = "Rating: The user assigned rating is better than the uncertainty would suggest."
            self.rating["messages"].append(text)
            guidance_text = self.tr("Provide documentation to justify the rating.")
            self.rating["guidance"].append(
                self.guidance_prep(self.rating["messages"][-1][0], guidance_text))
            self.rating["status"] = "caution"

    def check_bt_setting(self, meas):
        """Checks the bt settings to see if they are still on the default
                        settings.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.settings_dict["bt_tab"] = "Default"
        beam_change = []
        error_change = []
        vert_change = []

        # Check each vertical used to compute discharge
        for idx in meas.verticals_used_idx:
            if meas.verticals[idx].sampling_duration_sec > 0:
                s = meas.verticals[idx].current_settings()
                d = meas.verticals[idx].qrev_default_settings()

                if s["BTbeamFilter"] != d["BTbeamFilter"]:
                    beam_change.append(meas.verticals_used_idx.index(idx) + 1)

                if s["BTdFilter"] != d["BTdFilter"]:
                    error_change.append(meas.verticals_used_idx.index(idx) + 1)

                if s["BTwFilter"] != d["BTwFilter"]:
                    vert_change.append(meas.verticals_used_idx.index(idx) + 1)

        if len(beam_change) > 0:
            self.bt_vel["messages"].append(
                [
                    "Bt: User modified default beam setting for stations {}".format(
                        str(beam_change)
                    ),
                    3,
                    8,
                ]
            )
            guidance_text = self.tr("This filter determines the use of 3-beam solutions for bottom track. Provide documentation on the logic for changing the default filter setting.")
            self.bt_vel["guidance"].append(
                self.guidance_prep(self.bt_vel["messages"][-1][0], guidance_text))
            self.settings_dict["bt_tab"] = "Custom"

        if len(error_change):
            self.bt_vel["messages"].append(
                [
                    "Bt: User modified default error velocity filter for stations {}".format(
                        str(error_change)
                    ),
                    3,
                    8,
                ]
            )
            guidance_text = self.tr("This filter assumes a random distribution of the error velocity. For SonTek data, each frequency is treated separately. Provide documentation on the logic for changing the default filter setting.")
            self.bt_vel["guidance"].append(
                self.guidance_prep(self.bt_vel["messages"][-1][0], guidance_text))
            self.settings_dict["bt_tab"] = "Custom"

        if len(vert_change) > 0:
            self.bt_vel["messages"].append(
                [
                    "Bt: User modified default vertical velocity filter for stations {}".format(
                        str(vert_change)
                    ),
                    3,
                    8,
                ]
            )
            guidance_text = self.tr("This filter assumes a random distribution of the vertical velocity. For SonTek data, each frequency is treated separately. Provide documentation on the logic for changing the default filter setting.")
            self.bt_vel["guidance"].append(
                self.guidance_prep(self.bt_vel["messages"][-1][0], guidance_text))
            self.settings_dict["bt_tab"] = "Custom"

    def check_wt_settings(self, meas):
        """Checks the wt settings to see if they are still on the default
                settings.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.settings_dict["wt_tab"] = "Default"
        source_change = []
        ensemble_change = []
        top_type_change = []
        top_change = []
        bottom_type_change = []
        bottom_change = []
        beam_change = []
        err_vel_change = []
        vert_vel_change = []
        snr_change = []
        for idx in meas.verticals_used_idx:
            if meas.verticals[idx].sampling_duration_sec > 0:
                default_settings = meas.verticals[idx].qrev_default_settings()

                if (
                    meas.verticals[idx].mean_speed_source
                    != meas.verticals[idx].mean_speed_source_orig
                ):
                    source_change.append(meas.verticals_used_idx.index(idx) + 1)

                if (
                    meas.verticals[idx].cell_minimum_valid_ensembles
                    != meas.cell_minimum_valid_ensembles_default
                ):
                    ensemble_change.append(meas.verticals_used_idx.index(idx) + 1)

                if (
                    meas.verticals[idx].data.w_vel.excluded_top_type
                    != meas.verticals[idx].data.w_vel.excluded_top_type_orig
                ):
                    top_type_change.append(meas.verticals_used_idx.index(idx) + 1)

                if (
                    meas.verticals[idx].data.w_vel.excluded_top
                    != meas.verticals[idx].data.w_vel.excluded_top_orig
                ):
                    top_change.append(meas.verticals_used_idx.index(idx) + 1)

                if (
                    meas.verticals[idx].data.w_vel.excluded_bottom_type
                    != meas.verticals[idx].data.w_vel.excluded_bottom_type_orig
                ):
                    bottom_type_change.append(meas.verticals_used_idx.index(idx) + 1)

                if (
                    meas.verticals[idx].data.w_vel.excluded_bottom
                    != meas.verticals[idx].data.w_vel.excluded_bottom_orig
                ):
                    bottom_change.append(meas.verticals_used_idx.index(idx) + 1)

                if (
                    meas.verticals[idx].data.w_vel.beam_filter
                    != default_settings["WTBeamFilter"]
                ):
                    beam_change.append(meas.verticals_used_idx.index(idx) + 1)

                if (
                    meas.verticals[idx].data.w_vel.d_filter
                    != default_settings["WTdFilter"]
                ):
                    err_vel_change.append(meas.verticals_used_idx.index(idx) + 1)

                if (
                    meas.verticals[idx].data.w_vel.w_filter
                    != default_settings["WTwFilter"]
                ):
                    vert_vel_change.append(meas.verticals_used_idx.index(idx) + 1)

                if (
                    meas.verticals[idx].data.w_vel.snr_filter
                    != default_settings["WTsnrFilter"]
                ):
                    snr_change.append(meas.verticals_used_idx.index(idx) + 1)

        if len(source_change) > 0:
            self.w_vel["messages"].append(
                [
                    "Wt: User modified velocity source setting for stations {}.".format(
                        str(source_change)
                    ),
                    3,
                    11,
                ]
            )
            guidance_text = self.tr("Provide documentation to support the change.")
            self.w_vel["guidance"].append(
                self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
            self.settings_dict["wt_tab"] = "Custom"

        if len(ensemble_change) > 0:
            self.w_vel["messages"].append(
                [
                    "Wt: User modified minimum number of ensembles for stations {}.".format(
                        str(ensemble_change)
                    ),
                    3,
                    11,
                ]
            )
            guidance_text = self.tr("Provide documentation to support the change.")
            self.w_vel["guidance"].append(
                self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
            self.settings_dict["wt_tab"] = "Custom"

        if len(top_type_change) > 0:
            self.w_vel["messages"].append(
                [
                    "Wt: User modified excluded below type for stations {}.".format(
                        str(top_type_change)
                    ),
                    3,
                    11,
                ]
            )
            guidance_text = self.tr("Generally, distance is preferred unless a particular layer of cells is identified as invalid. Provide documentation to support the change.")
            self.w_vel["guidance"].append(
                self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
            self.settings_dict["wt_tab"] = "Custom"

        if len(top_change) > 0:
            self.w_vel["messages"].append(
                [
                    "Wt: User modified excluded below value for stations {}.".format(
                        str(top_change)
                    ),
                    3,
                    11,
                ]
            )
            guidance_text = self.tr("Reducing the excluded distance below the transducer may result in a low bias in the depth cells near the transducer. Provide documentation to support the change.")
            self.w_vel["guidance"].append(
                self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
            self.settings_dict["wt_tab"] = "Custom"

        if len(bottom_type_change) > 0:
            self.w_vel["messages"].append(
                [
                    "Wt: User modified excluded above sidelobe type for stations {}.".format(
                        str(bottom_type_change)
                    ),
                    3,
                    11,
                ]
            )
            guidance_text = self.tr("Generally, distance is preferred unless a particular layer of cells is identified as invalid. Provide documentation to support the change.")
            self.w_vel["guidance"].append(
                self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
            self.settings_dict["wt_tab"] = "Custom"

        if len(bottom_change) > 0:
            self.w_vel["messages"].append(
                [
                    "Wt: User modified exclude above sidelobe value for stations {}.".format(
                        str(bottom_change)
                    ),
                    3,
                    11,
                ]
            )
            guidance_text = self.tr("Typically changing this value would only be appropriate if there was a known and observed issue with the measured velocities that the filters did not handle. Provide documentation to support the change.")
            self.w_vel["guidance"].append(
                self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
            self.settings_dict["wt_tab"] = "Custom"

        if len(beam_change) > 0:
            self.w_vel["messages"].append(
                [
                    "Wt: User modified beam setting for stations {}.".format(
                        str(beam_change)
                    ),
                    3,
                    11,
                ]
            )
            guidance_text = self.tr("This filter determines the use of 3-beam solutions for bottom track. Provide documentation on the logic for changing the default filter setting.")
            self.w_vel["guidance"].append(
                self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
            self.settings_dict["wt_tab"] = "Custom"

        if len(err_vel_change) > 0:
            self.w_vel["messages"].append(
                [
                    "Wt: User modified error velocity filter for stations {}.".format(
                        str(err_vel_change)
                    ),
                    3,
                    11,
                ]
            )
            guidance_text = self.tr("This filter assumes a random distribution of the error velocity. Each frequency and ping type are treated separately, when using the automatic mode. Provide documentation on the logic for changing the default filter setting.")
            self.w_vel["guidance"].append(
                self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
            self.settings_dict["wt_tab"] = "Custom"

        if len(vert_vel_change) > 0:
            self.w_vel["messages"].append(
                [
                    "Wt: User modified vertical velocity filter for stations {}.".format(
                        str(vert_vel_change)
                    ),
                    3,
                    11,
                ]
            )
            guidance_text = self.tr("This filter assumes a random distribution of the vertical velocity. Each frequency and ping type are treated separately, when using the automatic mode. Provide documentation on the logic for changing the default filter setting.")
            self.w_vel["guidance"].append(
                self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
            self.settings_dict["wt_tab"] = "Custom"

        if len(snr_change) > 0:
            self.w_vel["messages"].append(
                [
                    "Wt: User modified snr filter for stations {}.".format(
                        str(snr_change)
                    ),
                    3,
                    11,
                ]
            )
            guidance_text = self.tr("The SNR filter attempts to identify beams that may be affected by air bubbles under or on the transducer. Generally, invalid data due to the SNR filter can be reduced or eliminated by deploying the ADCP deeper into the water. If review data in the office, evaluate the amount and location of the invalid data. Evaluate the change in discharge between applying and not applying the filter. Document the logic used for the change.")
            self.w_vel["guidance"].append(
                self.guidance_prep(self.w_vel["messages"][-1][0], guidance_text))
            self.settings_dict["wt_tab"] = "Custom"

    def check_extrap_settings(self, meas):
        """Checks the extrap to see if they are still on the default
        settings.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.settings_dict["tab_extrap"] = "Default"
        modified_idx = []
        for idx in meas.verticals_used_idx:
            if meas.verticals[idx].sampling_duration_sec > 0:
                # Check fit parameters
                if (
                    meas.verticals[idx].extrapolation.extrap_fit.fit_method
                    != "Automatic"
                ):
                    self.settings_dict["extrap_tab"] = "Custom"
                    modified_idx.append(meas.verticals_used_idx.index(idx) + 1)

                    self.extrapolation["messages"].append(
                        [
                            "Extrapolation: User modified default automatic settings "
                            "for stations {}.".format(str(modified_idx)),
                            3,
                            12,
                        ]
                    )
                    guidance_text = self.tr("The extrapolation has been set manually. Provide justification for the selected method in the comments.")
                    self.extrapolation["guidance"].append(
                        self.guidance_prep(self.extrapolation["messages"][-1][0], guidance_text))

    def check_tempsal_settings(self, meas):
        """Checks the temp and salinity settings to see if they are still on
        the default settings.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.settings_dict["tempsal_tab"] = "Default"

        t_source_change = []
        salinity_change = []
        s_sound_change = []
        t_user_change = False
        t_adcp_change = False

        if not all(np.isnan([meas.temp_chk["user"], meas.temp_chk["user_orig"]])):
            if meas.temp_chk["user"] != meas.temp_chk["user_orig"]:
                t_user_change = True

        if not all(np.isnan([meas.temp_chk["adcp"], meas.temp_chk["adcp_orig"]])):
            if meas.temp_chk["adcp"] != meas.temp_chk["adcp_orig"]:
                t_adcp_change = True

        # Check each checked transect
        for idx in meas.verticals_used_idx:
            vertical = meas.verticals[idx]
            if vertical.sampling_duration_sec > 0:
                # Temperature source
                if vertical.data.sensors.temperature_deg_c.selected != "internal":
                    t_source_change.append(meas.verticals_used_idx.index(idx))

                if vertical.data.sensors.salinity_ppt.selected != "internal":
                    sal = getattr(
                        vertical.data.sensors.salinity_ppt,
                        vertical.data.sensors.salinity_ppt.selected,
                    )
                    if np.all(
                        np.equal(
                            sal.data, vertical.data.sensors.salinity_ppt.internal.data
                        )
                    ):
                        salinity_change.append(meas.verticals_used_idx.index(idx))

                # Speed of Sound
                if vertical.data.inst.manufacturer == "TRDI":
                    if vertical.data.sensors.speed_of_sound_mps.selected != "internal":
                        s_sound_change.append(meas.verticals_used_idx.index(idx))

        # Report condition and messages
        if any(
            [
                t_source_change,
                salinity_change,
                s_sound_change,
                t_adcp_change,
                t_user_change,
            ]
        ):
            self.settings_dict["tempsal_tab"] = "Custom"

            if len(t_source_change) > 0:
                self.temperature["messages"].append(
                    [
                        "Temperature: User modified temperature source for stations {}.".format(
                            str(t_source_change)
                        ),
                        3,
                        5,
                    ]
                )
                guidance_text = self.tr("The user has modified the temperature source. Validate the new data from field notes or other documentation.")
                self.temperature["guidance"].append(
                    self.guidance_prep(self.temperature["messages"][-1][0], guidance_text))

            if len(s_sound_change) > 0:
                self.temperature["messages"].append(
                    [
                        "Temperature: User modified speed of sound source for stations {}.".format(
                            str(s_sound_change)
                        ),
                        3,
                        5,
                    ]
                )
                guidance_text = self.tr("The user has modified the speed of sound source. Validate the new data from field notes or other documentation.")
                self.temperature["guidance"].append(
                    self.guidance_prep(self.temperature["messages"][-1][0],
                                       guidance_text))

            if t_user_change:
                self.temperature["messages"].append(
                    ["Temperature: User modified independent temperature.", 3, 5]
                )
                guidance_text = self.tr("The user has modified the independent temperature reading. Validate this reading from the field notes.")
                self.temperature["guidance"].append(
                    self.guidance_prep(self.temperature["messages"][-1][0],
                                       guidance_text))

            if t_adcp_change:
                self.temperature["messages"].append(
                    ["Temperature: User modified ADCP temperature.", 3, 5]
                )
                guidance_text = self.tr("The user has modified the ADCP temperature. Validate this reading from the field notes and with the ADCP temperature time series.")
                self.temperature["guidance"].append(
                    self.guidance_prep(self.temperature["messages"][-1][0],
                                       guidance_text))

    def check_gps_settings(self, meas):
        """Checks the gps settings to see if they are still on the default
        settings.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        gps = False
        self.settings_dict["tab_gps"] = "Default"

        # Check for transects with gga or vtg data
        for idx in meas.checked_transect_idx:
            transect = meas.transects[idx]
            if (
                transect.boat_vel.gga_vel is not None
                or transect.boat_vel.gga_vel is not None
            ):
                gps = True
                break

        # If gga or vtg data exist check settings
        if gps:
            s = meas.current_settings()
            d = meas.qrev_default_settings()

            if s["ggaDiffQualFilter"] != d["ggaDiffQualFilter"]:
                self.gga_vel["messages"].append(
                    ["GPS: User modified default quality setting.", 3, 8]
                )
                guidance_text = self.tr("If the quality setting is less than 2, evaluate the time series and shiptrack for large random errors. Provide documentation on the logic for changing the default filter setting.")
                self.gga_vel["guidance"].append(
                    self.guidance_prep(self.gga_vel["messages"][-1][0],
                                       guidance_text))
                self.settings_dict["tab_gps"] = "Custom"

            if s["ggaAltitudeFilter"] != d["ggaAltitudeFilter"]:
                self.gga_vel["messages"].append(
                    ["GPS: User modified default altitude filter.", 3, 8]
                )
                guidance_text = self.tr("The altitude filter is well correlated with the horizontal accuracy of the GGA data. The default setting is based on a target of sub-meter accuracy. Increasing the altitude filter would allow less accurate data to be used. Provide documentation on the logic for changing the default filter setting.")
                self.gga_vel["guidance"].append(
                    self.guidance_prep(self.gga_vel["messages"][-1][0], guidance_text))
                self.settings_dict["tab_gps"] = "Custom"

            if s["GPSHDOPFilter"] != d["GPSHDOPFilter"]:
                self.gga_vel["messages"].append(
                    ["GPS: User modified default HDOP filter.", 3, 8]
                )
                guidance_text = self.tr("The HDOP is a measure of accuracy based on satellite configuration. Increasing the value of HDOP could result in the acceptance of less accurate data. Provide documentation on the logic for changing the default filter setting.")
                self.gga_vel["guidance"].append(
                    self.guidance_prep(self.gga_vel["messages"][-1][0], guidance_text))
                self.settings_dict["tab_gps"] = "Custom"

            if s["GPSSmoothFilter"] != d["GPSSmoothFilter"]:
                self.gga_vel["messages"].append(
                    ["GPS: User modified default smooth filter.", 3, 8]
                )
                guidance_text = self.tr("The smooth filter is not used by default. It can be used when the other filters fail to mark obvious spikes in the boat velocity invalid. Provide documentation on the logic for changing the default filter setting.")
                self.gga_vel["guidance"].append(
                    self.guidance_prep(self.gga_vel["messages"][-1][0], guidance_text))
                self.settings_dict["tab_gps"] = "Custom"

    def check_depth_settings(self, meas):
        """Checks the depth settings to see if they are still on the default
                settings.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.settings_dict["tab_depth"] = "Default"
        ref_change = []
        comp_change = []
        avg_change = []
        filter_change = []
        draft_change = []
        draft_ice_change = []
        thick_change = []
        ice_bottom_change = []
        slush_bottom_change = []
        # Check each vertical used to compute discharge
        for idx in meas.verticals_used_idx:
            # Apply check to measure verticals only
            if meas.verticals[idx].sampling_duration_sec > 0:
                s = meas.verticals[idx].current_settings()
                d = meas.verticals[idx].qrev_default_settings()

                # Depth reference
                if s["depthReference"] != d["depthReference"]:
                    ref_change.append(meas.verticals_used_idx.index(idx) + 1)

                # Composite depths
                if s["depthComposite"] != d["depthComposite"]:
                    comp_change.append(meas.verticals_used_idx.index(idx) + 1)

                # Depth averaging method
                if s["depthAvgMethod"] != d["depthAvgMethod"]:
                    avg_change.append(meas.verticals_used_idx.index(idx) + 1)

                # Depth filter type
                if (s["depthReference"] != "man_depths") and (
                    s["depthFilterType"] != d["depthFilterType"]
                ):
                    filter_change.append(meas.verticals_used_idx.index(idx) + 1)

                # ADCP depth below ice
                if not np.allclose(
                    meas.verticals[idx].adcp_depth_below_ice_orig_m,
                    meas.verticals[idx].adcp_depth_below_ice_m,
                    equal_nan=True,
                ):
                    draft_ice_change.append(meas.verticals_used_idx.index(idx) + 1)

                # ADCP depth below water surface
                if not np.allclose(
                    meas.verticals[idx].adcp_depth_below_ws_orig_m,
                    meas.verticals[idx].adcp_depth_below_ws_m,
                    equal_nan=True,
                ):
                    draft_change.append(meas.verticals_used_idx.index(idx) + 1)

                # Ice thickness
                if not np.allclose(
                    meas.verticals[idx].ice_thickness_orig_m,
                    meas.verticals[idx].ice_thickness_m,
                    equal_nan=True,
                ):
                    thick_change.append(meas.verticals_used_idx.index(idx) + 1)

                # Water surface to bottom of ice
                if not np.allclose(
                    meas.verticals[idx].ws_to_ice_bottom_orig_m,
                    meas.verticals[idx].ws_to_ice_bottom_m,
                    equal_nan=True,
                ):
                    ice_bottom_change.append(meas.verticals_used_idx.index(idx) + 1)

                # Water surface to bottom of slush
                if not np.allclose(
                    meas.verticals[idx].ws_to_slush_bottom_orig_m,
                    meas.verticals[idx].ws_to_slush_bottom_m,
                    equal_nan=True,
                ):
                    slush_bottom_change.append(meas.verticals_used_idx.index(idx) + 1)

        # Depth reference
        if len(ref_change) > 0:
            self.depths["messages"].append(
                [
                    "Depths: User modified "
                    "depth reference for stations {}.".format(str(ref_change)),
                    3,
                    10,
                ]
            )
            guidance_text = self.tr("Provide documentation to justify the selected depth reference.")
            self.depths["guidance"].append(
                self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
            self.settings_dict["depth_tab"] = "Custom"

        # Composite depths
        if len(comp_change) > 0:
            self.depths["messages"].append(
                [
                    "Depths: User modified "
                    "depth reference for stations {}.".format(str(comp_change)),
                    3,
                    10,
                ]
            )
            guidance_text = self.tr("Provide documentation to justify the selected depth reference.")
            self.depths["guidance"].append(
                self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
            self.settings_dict["depth_tab"] = "Custom"

        # Depth averaging method
        if len(avg_change) > 0:
            self.depths["messages"].append(
                [
                    "Depths: User modified "
                    "averaging method for stations {}.".format(str(avg_change)),
                    3,
                    10,
                ]
            )
            guidance_text = self.tr("The default inverse distance weighting method was changed to a simple average. Provide documentation to justify the change.")
            self.depths["guidance"].append(
                self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
            self.settings_dict["depth_tab"] = "Custom"

        # Depth filter type
        if len(filter_change) > 0:
            self.depths["messages"].append(
                [
                    "Depths: User modified "
                    "filter type for stations {}.".format(str(filter_change)),
                    3,
                    10,
                ]
            )
            guidance_text = self.tr("The default filter was changed. Provide documentation to justify the change.")
            self.depths["guidance"].append(
                self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
            self.settings_dict["depth_tab"] = "Custom"

        # ADCP depth below ice
        if len(draft_ice_change) > 0:
            self.depths["messages"].append(
                [
                    "Depths: User modified ADCP depth below ice for stations {}.".format(
                        str(draft_ice_change)
                    ),
                    3,
                    10,
                ]
            )
            guidance_text = self.tr("The ADCP depth below ice has been changed from that entered in the field. Provide documentation to support the new depth.")
            self.depths["guidance"].append(
                self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
            self.settings_dict["depth_tab"] = "Custom"

        # ADCP depth below water surface
        if len(draft_change) > 0:
            self.depths["messages"].append(
                [
                    "Depths: User modified ADCP depth below water surface for stations {}.".format(
                        str(draft_change)
                    ),
                    3,
                    10,
                ]
            )
            guidance_text = self.tr("The ADCP depth below water surface has been changed from that entered in the field. Provide documentation to support the new depth.")
            self.depths["guidance"].append(
                self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
            self.settings_dict["depth_tab"] = "Custom"

        # Ice thickness
        if len(thick_change) > 0:
            self.depths["messages"].append(
                [
                    "Depths: User modified ice thickness for stations {}.".format(
                        str(thick_change)
                    ),
                    3,
                    10,
                ]
            )
            guidance_text = self.tr("The ice thickness has been changed from that entered in the field. Provide documentation to support the new thickness.")
            self.depths["guidance"].append(
                self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
            self.settings_dict["depth_tab"] = "Custom"

        # Water surface to bottom of ice
        if len(ice_bottom_change) > 0:
            self.depths["messages"].append(
                [
                    "Depths: User modified W.S. to ice bottom for stations {}.".format(
                        str(ice_bottom_change)
                    ),
                    3,
                    10,
                ]
            )
            guidance_text = self.tr("The water surface to ice bottom has been changed from that entered in the field. Provide documentation to support the new depth to ice bottom.")
            self.depths["guidance"].append(
                self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
            self.settings_dict["depth_tab"] = "Custom"

        # Water surface to bottom of slush
        if len(slush_bottom_change) > 0:
            self.depths["messages"].append(
                [
                    "Depths: User modified W.S. to slush bottom for stations {}.".format(
                        str(slush_bottom_change)
                    ),
                    3,
                    10,
                ]
            )
            guidance_text = self.tr("The water surface to slush bottom has been changed from that entered in the field. Provide documentation to support the new depth slush bottom.")
            self.depths["guidance"].append(
                self.guidance_prep(self.depths["messages"][-1][0], guidance_text))
            self.settings_dict["depth_tab"] = "Custom"

    def check_edge_settings(self, meas):
        """Checks the edge settings to see if they are still on the original
                settings.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        # Initialize variables
        start_idx = meas.verticals_used_idx[0]
        end_idx = meas.verticals_used_idx[-1]
        self.settings_dict["tab_edges"] = "Default"

        bank_change = []
        location_change = []
        depth_change = []
        velocity_correction_change = []
        edge_coef_change = []
        stage_change = []
        stage_time_change = []

        # Check each edge for changes
        for idx in [start_idx, end_idx]:
            vertical = meas.verticals[idx]
            if idx < end_idx:
                edge = "Start"
            else:
                edge = "End"
            if vertical.edge_loc != vertical.edge_loc_orig:
                bank_change.append(edge)
            if not np.allclose(
                vertical.location_m, vertical.location_orig_m, equal_nan=True
            ):
                location_change.append(edge)
            if not np.allclose(vertical.depth_m, vertical.depth_orig_m, equal_nan=True):
                depth_change.append(edge)
            if not np.allclose(
                vertical.velocity_correction,
                vertical.velocity_correction_orig,
                equal_nan=True,
            ):
                velocity_correction_change.append(edge)
            if not np.allclose(
                vertical.edge_coef, vertical.edge_coef_orig, equal_nan=True
            ):
                edge_coef_change.append(edge)
            if not np.allclose(vertical.gh_m, vertical.gh_orig_m, equal_nan=True):
                stage_change.append(edge)
            if vertical.gh_obs_time != vertical.gh_obs_time_orig:
                stage_time_change.append(edge)

        # Add messages for each change
        if len(bank_change) > 0:
            self.edges["messages"].append(
                [
                    "Edges: User modified bank for edges {} .".format(str(bank_change)),
                    3,
                    10,
                ]
            )
            guidance_text = self.tr("Provide documentation for the change.")
            self.edges["guidance"].append(
                self.guidance_prep(self.edges["messages"][-1][0], guidance_text))
            self.settings_dict["tab_edges"] = "Custom"

        if len(location_change) > 0:
            self.edges["messages"].append(
                [
                    "Edges: User modified location for edges {}.".format(
                        str(location_change)
                    ),
                    3,
                    10,
                ]
            )
            guidance_text = self.tr("Provide documentation for the change.")
            self.edges["guidance"].append(
                self.guidance_prep(self.edges["messages"][-1][0], guidance_text))
            self.settings_dict["tab_edges"] = "Custom"

        if len(depth_change) > 0:
            self.edges["messages"].append(
                [
                    "Edges: User modified depth for edges {}.".format(
                        str(depth_change)
                    ),
                    3,
                    10,
                ]
            )
            guidance_text = self.tr("Provide documentation for the change. Ensure that the velocity correction or edge coefficient is consistent with the new edge depth.")
            self.edges["guidance"].append(
                self.guidance_prep(self.edges["messages"][-1][0], guidance_text))
            self.settings_dict["tab_edges"] = "Custom"

        if len(velocity_correction_change) > 0:
            self.edges["messages"].append(
                [
                    "Edges: User modified velocity correction for edges {}.".format(
                        str(velocity_correction_change)
                    ),
                    3,
                    10,
                ]
            )
            guidance_text = self.tr("Provide documentation for the change.")
            self.edges["guidance"].append(
                self.guidance_prep(self.edges["messages"][-1][0], guidance_text))
            self.settings_dict["tab_edges"] = "Custom"

        if len(edge_coef_change) > 0:
            self.edges["messages"].append(
                [
                    "Edges: User modified edge coefficient for edges {}.".format(
                        str(edge_coef_change)
                    ),
                    3,
                    10,
                ]
            )
            guidance_text = self.tr("Provide documentation for the change.")
            self.edges["guidance"].append(
                self.guidance_prep(self.edges["messages"][-1][0], guidance_text))
            self.settings_dict["tab_edges"] = "Custom"

        if len(stage_change) > 0:
            self.edges["messages"].append(
                [
                    "Edges: User modified stage for edges {}.".format(
                        str(stage_change)
                    ),
                    3,
                    10,
                ]
            )
            guidance_text = self.tr("Provide documentation for the change.")
            self.edges["guidance"].append(
                self.guidance_prep(self.edges["messages"][-1][0], guidance_text))
            self.settings_dict["tab_edges"] = "Custom"

        if len(stage_time_change) > 0:
            self.edges["messages"].append(
                [
                    "Edges: User modified stage time for edges {}.".format(
                        str(stage_time_change)
                    ),
                    3,
                    10,
                ]
            )
            guidance_text = self.tr("Provide documentation for the change.")
            self.edges["guidance"].append(
                self.guidance_prep(self.edges["messages"][-1][0], guidance_text))
            self.settings_dict["tab_edges"] = "Custom"

    def check_compass_settings(self, meas):
        """Checks the compass settings for changes.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        self.settings_dict["tab_compass"] = "Default"

        magvar_change = []
        align_change = []

        # Check each checked transect
        for idx in meas.verticals_used_idx:
            vertical = meas.verticals[idx]
            if vertical.sampling_duration_sec > 0:
                # Magvar
                if (
                    vertical.data.sensors.heading_deg.internal.mag_var_deg
                    != vertical.data.sensors.heading_deg.internal.mag_var_orig_deg
                ):
                    magvar_change.append(meas.verticals_used_idx.index(idx))

                # Alignment
                if (
                    vertical.data.sensors.heading_deg.internal.align_correction_deg
                    != vertical.data.sensors.heading_deg.internal.align_correction_orig_deg
                ):
                    align_change.append(meas.verticals_used_idx.index(idx))

        # Report condition and messages
        if len(magvar_change) > 0:
            self.compass["messages"].append(
                [
                    "Compass: User modified magnetic variation for stations {}".format(
                        str(magvar_change)
                    ),
                    3,
                    4,
                ]
            )
            guidance_text = self.tr("If the user has modified the magnetic variation, check that the variation is reasonable for the site.")
            self.compass["guidance"].append(
                self.guidance_prep(self.compass["messages"][-1][0], guidance_text))
            self.settings_dict["compasspr_tab"] = "Custom"

        if len(align_change) > 0:
            self.compass["messages"].append(
                [
                    "Compass: User modified alignment for stations {}".format(
                        str(align_change)
                    ),
                    3,
                    4,
                ]
            )
            guidance_text = self.tr("The beam misalignment is used when the manufacturer specified reference beam is not pointed in the upstream direction for the fixed velocity method. Document the reason for the change.")
            self.compass["guidance"].append(
                self.guidance_prep(self.compass["messages"][-1][0], guidance_text))
            self.settings_dict["compasspr_tab"] = "Custom"

    def check_premeasurement_settings(self, meas):
        """Checks for user changes in azimuth and beam 3 misalignment

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        # Azimuth change
        if (not np.isnan(meas.tagline_azimuth_deg)) and round(
            meas.tagline_azimuth_deg, 2
        ) != round(meas.tagline_azimuth_orig_deg, 2):
            self.settings_dict["main_premeasurement_tab"] = "Custom"
            self.user["messages"].append(
                ["Premeasurement: User modified azimuth.", 3, 2]
            )
            guidance_text = self.tr("Verify that the azimuth angle is correct, and its reference (magnetic north or true north) is that same as the heading reference for the stations. Provide documentation to support the azimuth value.")
            self.user["guidance"].append(
                self.guidance_prep(self.user["messages"][-1][0], guidance_text))

        # Beam 3 misalignment change
        if (not np.isnan(meas.beam_3_misalignment_deg)) and round(
            meas.beam_3_misalignment_deg, 2
        ) != round(meas.beam_3_misalignment_orig_deg, 2):
            self.settings_dict["main_premeasurement_tab"] = "Custom"
            self.user["messages"].append(
                ["Premeasurement: User modified beam 3 misalignment.", 3, 2]
            )
            guidance_text = self.tr("Verify that the beam misalignment value is correct and provide supporting documentation.")
            self.user["guidance"].append(
                self.guidance_prep(self.user["messages"][-1][0], guidance_text))

    def check_stations_settings(self, meas):
        """Check for changes for settings on stations tab.

        Parameters
        ----------
        meas: Measurement
            Object of class Measurement
        """

        # Initialize lists
        loc_change = []
        stage_change = []
        stage_time_change = []
        ws_cond_change = []
        vel_ref_change = []
        vel_method_change = []
        use_change = []

        # Find changes
        for n, idx in enumerate(meas.verticals_used_idx):
            vertical = meas.verticals[idx]
            if np.round(vertical.location_m, 2) != np.round(
                vertical.location_orig_m, 2
            ):
                loc_change.append(n + 1)
            if (not np.isnan(vertical.gh_m)) and np.round(vertical.gh_m) != np.round(
                vertical.gh_orig_m
            ):
                stage_change.append(n + 1)
            if vertical.gh_obs_time != vertical.gh_obs_time_orig:
                stage_time_change.append(n + 1)
            if vertical.ws_condition != vertical.ws_condition_orig:
                ws_cond_change.append(n + 1)
            if vertical.velocity_reference != vertical.velocity_reference_orig:
                vel_ref_change.append(n + 1)
            if vertical.velocity_method != vertical.velocity_method_orig:
                vel_method_change.append(n + 1)
            if vertical.use != vertical.use_orig:
                use_change.append(n + 1)

        # Report changes
        if len(loc_change) > 0:
            self.settings_dict["stations_tab"] = "Custom"
            if len(loc_change) < len(meas.verticals_used_idx):
                self.stations["messages"].append(
                    "Stations: The location was changed for stations " + str(loc_change)
                )
                guidance_text = self.tr("Document the reason that the station location was changed.")
                self.stations["guidance"].append(
                    self.guidance_prep(self.stations["messages"][-1][0], guidance_text))
            else:
                self.stations["messages"].append(
                    "Stations: The location was changed for ALL stations."
                )
                guidance_text = self.tr("Document the reason that the locations of all stations were changed.")
                self.stations["guidance"].append(
                    self.guidance_prep(self.stations["messages"][-1][0], guidance_text))

        if len(stage_change) > 0:
            self.settings_dict["stations_tab"] = "Custom"
            if len(stage_change) < len(meas.verticals_used_idx):
                self.stations["messages"].append(
                    "Stations: The stage was changed for stations " + str(stage_change)
                )
                guidance_text = self.tr("Document why the stage was changed.")
                self.stations["guidance"].append(
                    self.guidance_prep(self.stations["messages"][-1][0], guidance_text))
            else:
                self.stations["messages"].append(
                    "Stations: The stage was changed for ALL stations."
                )
                guidance_text = self.tr("Document why the stage was changed for all stations.")
                self.stations["guidance"].append(
                    self.guidance_prep(self.stations["messages"][-1][0], guidance_text))

        if len(stage_time_change) > 0:
            self.settings_dict["stations_tab"] = "Custom"
            if len(stage_time_change) < len(meas.verticals_used_idx):
                self.stations["messages"].append(
                    "Stations: The stage time was changed for stations "
                    + str(stage_time_change)
                )
                guidance_text = self.tr("Document the reason for stage time change(s).")
                self.stations["guidance"].append(
                    self.guidance_prep(self.stations["messages"][-1][0], guidance_text))
            else:
                self.stations["messages"].append(
                    "Stations: The stage time was changed for ALL stations."
                )
                guidance_text = self.tr("Document the reason for the change in stage time for all stations.")
                self.stations["guidance"].append(
                    self.guidance_prep(self.stations["messages"][-1][0], guidance_text))

        if len(ws_cond_change) > 0:
            self.settings_dict["stations_tab"] = "Custom"
            if len(ws_cond_change) < len(meas.verticals_used_idx) - 2:
                self.stations["messages"].append(
                    "Stations: The water surface condition was changed for stations "
                    + str(ws_cond_change)
                )
                guidance_text = self.tr("Document the reason for the change.")
                self.stations["guidance"].append(
                    self.guidance_prep(self.stations["messages"][-1][0], guidance_text))
            else:
                self.stations["messages"].append(
                    "Stations: The water surface condition was changed for ALL stations."
                )
                guidance_text = self.tr("Document the reason for the change.")
                self.stations["guidance"].append(
                    self.guidance_prep(self.stations["messages"][-1][0], guidance_text))

        if len(vel_ref_change) > 0:
            self.settings_dict["stations_tab"] = "Custom"
            if len(vel_ref_change) < len(meas.verticals_used_idx) - 2:
                self.stations["messages"].append(
                    "Stations: The velocity reference was changed for stations "
                    + str(vel_ref_change)
                )
                guidance_text = self.tr("Generally, the same velocity reference is used for all stations. Unusual conditions could cause the best velocity reference to be different. However, if bottom track is selected, the user should ensure that there is not a moving-bed conditions. Document the reason for the change.")
                self.stations["guidance"].append(
                    self.guidance_prep(self.stations["messages"][-1][0], guidance_text))
            else:
                self.stations["messages"].append(
                    "Stations: The velocity reference was changed for ALL stations."
                )
                guidance_text = self.tr("If bottom track is selected, the user should ensure that there is not a moving-bed conditions. Document the reason for the change.")
                self.stations["guidance"].append(
                    self.guidance_prep(self.stations["messages"][-1][0], guidance_text))

        if len(vel_method_change) > 0:
            self.settings_dict["stations_tab"] = "Custom"
            if len(vel_method_change) < len(meas.verticals_used_idx) - 2:
                self.stations["messages"].append(
                    "Stations: The velocity method was changed for stations "
                    + str(vel_method_change)
                )
                guidance_text = self.tr(" It is unusual for the velocity method to be different among the stations. If the velocity method is not consistent among the stations, provide justification for the changes. If the azimuth method is used the azimuth and the heading data for each vertical need to be accurate. If the fixed method is used, the reference beam of the ADCP must be pointed upstream. If the magnitude method is used, the user should evaluate the heading data, if available and determine if the heading data should be used or not.")
                self.stations["guidance"].append(
                    self.guidance_prep(self.stations["messages"][-1][0], guidance_text))
            else:
                self.stations["messages"].append(
                    "Stations: The velocity method was changed for ALL stations."
                )
                guidance_text = self.tr("Document the reason that the velocity method was changed from that used to collect the data. If the azimuth method is used the azimuth and the heading data for each vertical need to be accurate. If the fixed method is used, the reference beam of the ADCP must be pointed upstream. If the magnitude method is used, the user should evaluate the heading data, if available and determine if the heading data should be used or not.")
                self.stations["guidance"].append(
                    self.guidance_prep(self.stations["messages"][-1][0], guidance_text))

        if len(use_change) > 0:
            self.settings_dict["stations_tab"] = "Custom"
            if len(use_change) < len(meas.verticals_used_idx) - 2:
                self.stations["messages"].append(
                    "Stations: The use status was changed for stations "
                    + str(use_change)
                )
                guidance_text = self.tr("Document the reason for the change.")
                self.stations["guidance"].append(
                    self.guidance_prep(self.stations["messages"][-1][0], guidance_text))
            else:
                self.stations["messages"].append(
                    "Stations: The use status was changed for ALL stations."
                )
                guidance_text = self.tr("Document the reason for the change.")
                self.stations["guidance"].append(
                    self.guidance_prep(self.stations["messages"][-1][0], guidance_text))

    def check_uncertainty(self, meas):
        if len(meas.uncertainty.method) > 3:
            self.settings_dict["uncertainty_tab"] = "Custom"
        else:
            self.settings_dict["uncertainty_tab"] = "Default"
